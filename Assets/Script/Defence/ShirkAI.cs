﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class ShirkAI : Charctor {
	//キャラの高さ
	float m_y=1.5f;
	//移動のステート
	int _movestate;
	//アニメーションの値
	float _anim=0;
	// アニメーション
	bool isanim=false;
	//色
	Color _initcolor;
	//初期のHP
	int _hp;
	// 現在の回転数
	int _angle;
	//HPのSprite
	GameObject _hpgauge;
	GameObject _hptext;

	BlackBoard bb;

	//タイトルシーンのでもかどうか
	bool istitle;

	// Use this for initialization
	void Start () {
		par = null;
		movePar = null;
		endPar = new GameObject[6];
		if (SceneManager.GetActiveScene ().name == "Title") {
			_chardata = GetCharData (0);
			this.gameObject.AddComponent<Rigidbody> ();
			istitle = true;
		}

		this.transform.localPosition = m_pos;

		ismove = false;

		_hp = _chardata._hp;


		//_initcolor = this.gameObject.transform.FindChild ("Box001").gameObject.GetComponent<Renderer> ().material.color;
		_initcolor = this.GetComponent<Renderer>().material.color;

		//DelayMethodを0.5秒後に呼び出す
		//Invoke("SearchBullet", 0.1f / gameSpeed);

		StartParticle ();

	}

	void Awake(){
		m_pos = new Vector3 (0, m_y, 0);

		_movestate = 0;

		gameSpeed = 1.0f;

		_angle = 3;

	}

	public void InitBlacBoard(BlackBoard blackboard){
		bb = blackboard;
		bb.AddInteger ("hitflag", 0);
		bb.AddInteger ("ctflag", 0);
		bb.AddInteger("angle",_angle);
		bb.AddInteger("movestate",0);
		bb.AddVector3 ("position", m_pos);
		bb.AddFraction ("spd",_chardata._spd);
		bb.AddFraction ("gameSpeed",gameSpeed);
		bb.AddJude ("ismove",false);
	}

	public virtual void SetGameSpeed(float speed){
		gameSpeed = speed;
		bb.setFraction ("gameSpeed",gameSpeed);
	}

	public void SetHP(GameObject hpgaug,GameObject hptext){
		_hp = _chardata._hp;

		_hpgauge = hpgaug;
		_hptext = hptext;
		FindObjectOfType<HPManager> ().init (_chardata._hp);
	}
	
	// Update is called once per frame
	void Update () {
		if(ismove){
			Move ();
		}

		if(isanim)
			Flashing ();
	}

	void FixedUpdate(){

	}

	public int GetHP(){
		return _hp;
	}

	void SearchBullet(){
		// 使ってない？
		//Vector3 p = this.gameObject.transform.position;

		//レイヤーマスクの指定
		//int layerMask = 1 << LayerMask.NameToLayer("Bullet");
		//9番のPlayerのみに当たるようにする
		//layerMask = ~layerMask;
		RaycastHit hit;

		int hitflag = 0;
		int ctflag = 0;

		Vector3 startpos = new Vector3 (transform.position.x, 1.2f, transform.position.z);

		if (_angle != 0) {
			if (Physics.Raycast (startpos, Vector3.left,out hit, 5)) {
				hitflag += 1;
				ctflag += 2;
			}
		}
		if (_angle != 1) {
			if (Physics.Raycast (startpos, Vector3.right,out hit, 5)) {
				hitflag += 2;
				ctflag += 1;
			}
		}
		if (_angle != 2) {
			if (Physics.Raycast (startpos, Vector3.back,out hit , 5)) {
				hitflag += 4;
				ctflag += 8;
			}
		}
		if (_angle != 3) {
			if (Physics.Raycast (startpos, Vector3.forward,out hit, 5)) {
				hitflag += 8;
				ctflag += 4;
			}
		}
			
		if (hitflag != 0) {
			ismove = true;
			MoveDirection (hitflag, ctflag);
		} else {
			//DelayMethodを0.5秒後に呼び出す
			Invoke("SearchBullet", 0.1f / gameSpeed);

		}
	}

	void MoveDirection(int hitflag,int ctflag){
		//方向ビット
		const int eastbit = 1;
		const int westbit = 2;
		const int southbit = 4;
		const int northit = 8;
		//当たりを検出したかどうか
		bool ism = false;
		//真ん中
		int center=0;
		//移動方向
		int s = 1;
		//端っこ判定
		int dnm = 0;
		//真ん中から遠い方検出
		if (m_pos.x < 0) {
			center += westbit;
			//端っこかどうか
			if (m_pos.x == -8)
				dnm += eastbit;
		}else if (m_pos.x > 0) {
			center += eastbit;
			//端っこかどうか
			if(m_pos.x == 8)
				dnm += westbit;
		}
		if (m_pos.z < 0) {
			//center += southbit;
			//端っこかどうか
			if (m_pos.z == -8)
				dnm += southbit;
		}else if (m_pos.z > 0) {
			//center += northit;
			//端っこかどうか
			if(m_pos.z == 8)
				dnm += northit;
		}
			
		int angle = 0;

		//移動できる方向検出
		if (FlagJude(hitflag , eastbit) && FlagJude(dnm , eastbit)) {
			_movestate = 1;
			_chardata._spd *= -1;
			s = -1;
			angle = 0;
			if (FlagJude(ctflag , eastbit)) {
				ism = true;
			}
		} 

		if (FlagJude(hitflag , westbit) && FlagJude(dnm , westbit)) {

			if ((FlagJude (ctflag, westbit) && FlagJude (center, westbit)) && !ism) {
				_movestate = 1;
				s = 1;
				angle = 1;
				if (_chardata._spd < 0)
					_chardata._spd *= -1;

				if (FlagJude (ctflag, westbit)) {
					ism = true;
				}
			}
		}

		if (FlagJude(hitflag , southbit) && FlagJude(dnm , southbit)) {
			if ((FlagJude (ctflag, southbit) && FlagJude (center, southbit)) && !ism) {
				_movestate = 2;
				s = -1;
				angle = 2;
				if (_chardata._spd > 0)
					_chardata._spd *= -1;
				
				if (FlagJude (ctflag, southbit))
					ism = true;
			}
		} 

		if (FlagJude(hitflag , northit) && FlagJude(dnm , northit)) {
			if ((FlagJude (ctflag, northit) && FlagJude (center, northit)) && !ism) {
				_movestate = 2;
				s = 1;
				angle = 3;
				if (_chardata._spd < 0)
					_chardata._spd *= -1;

				if (FlagJude (ctflag, northit))
					ism = true;
			}
		}

		PlayerRotatin (angle);

		//移動量
		const int movenum = 4;
		//移動場所検知
		switch (_movestate) {
		case 0:
			break;

		case 1:
			m_pos.x += movenum * s;
			break;

		case 2:
			m_pos.z += movenum * s;
			break;

		default:
			print ("default");
			break;
		}
		
	}

	bool FlagJude(int flag1,int flag2){
		return (flag1 & flag2) != flag2;
	}

	public override void MoveStart (){
		ismove = false;
	}

	protected override void Move (){

		Vector3 pos = this.gameObject.transform.position;

		//bool isfinish = false;

		float p = 0;

		switch (_movestate) {
		case 1:
			this.gameObject.transform.position = new Vector3 (pos.x + _chardata._spd * gameSpeed, pos.y, pos.z);
			p = m_pos.x - (pos.x + _chardata._spd * gameSpeed);
			break;
		case 2:
			this.gameObject.transform.position = new Vector3 (pos.x, pos.y, pos.z + _chardata._spd * gameSpeed);
			p = m_pos.z - (pos.z + _chardata._spd * gameSpeed);
			break;

		default:
			break;
		}

		if ((p > _chardata._spd | p > _chardata._spd * -1) && (p < _chardata._spd | p < _chardata._spd * -1)) {
			this.gameObject.transform.position = m_pos;
			ismove = false;
			if (_chardata._spd < 0)
				_chardata._spd *= -1;
			//DelayMethodを3.5秒後に呼び出す
			Invoke("SearchBullet", 0.5f / gameSpeed);
		}
			
	}

	void PlayerRotatin(int angle){
		float roll = 90;
		_angle = angle;
		switch (angle) {
		case 0:
			this.transform.eulerAngles = new Vector3 (0, -roll, 0);
			break;
		case 1:
			this.transform.eulerAngles = new Vector3 (0, roll, 0);
			break;
		case 2:
			this.transform.eulerAngles = new Vector3 (0, roll * 2, 0);
			break;
		case 3:
			this.transform.eulerAngles = Vector3.zero;
			break;
			
		}
	}

	private void Flashing(){
		if (_anim > 30) {
			//this.gameObject.transform.FindChild ("Box001").gameObject.GetComponent<Renderer> ().material.color = _initcolor;
			this.gameObject.GetComponent<Renderer> ().material.color = _initcolor;
			isanim = false;
		} else
			//this.gameObject.transform.FindChild ("Box001").gameObject.GetComponent<Renderer> ().material.color = new Color (1.0f, 0.2f, 0.2f, 0.0f);
			this.gameObject.GetComponent<Renderer>().material.color = new Color (1.0f, 0.2f, 0.2f, 0.0f);
		_anim++;
	}

	public override void HitBullet(int damage,Direction direction){
		DamegeParticle ();
		PlayerRotatin ((int)direction);
		_hp -= damage;
		isanim = true;
		_anim = 0;
		if (istitle)
			return;{
			if (_hp <= 0) {
				_hp = 0;
				this.GetComponentInParent<DefenceManager> ().GameOver ();
			}
			FindObjectOfType<HPManager> ().Damage (damage);
		}
	}

	void ChangeHP(){
		int num = (_hp*100) / _chardata._hp;
		_hptext.GetComponent<Text> ().text = num.ToString () + "%";
		float numx = 3.5f * num;
		_hpgauge.GetComponent<RectTransform> ().sizeDelta = new Vector2 (numx, 50.0f);
	}

	void OnDrawGizmos()
	{
		RaycastHit hit;
		//var scale = transform.lossyScale.x * 0.5f;

		Vector3 startpos = new Vector3 (transform.position.x, 1.2f, transform.position.z);
		var isHit = Physics.Raycast (startpos, Vector3.back, out hit, 5);
		if (isHit) {
			Gizmos.DrawRay (startpos, Vector3.back * hit.distance);
		} else {
			Gizmos.DrawRay (startpos, Vector3.back * 5);
		}
	}

	public override void StartParticle(){
		if(par)
			FindObjectOfType<ParticleManager> ().ParticleStop (par);
		par = FindObjectOfType<ParticleManager> ().CharChangeParticleStart (this.transform.position);
		Invoke("StopPar",par.GetComponent<ParticleSystem>().duration);
	}

	void DamegeParticle(){
		if(par)
			FindObjectOfType<ParticleManager> ().ParticleStop (par);
		par = FindObjectOfType<ParticleManager> ().DamegeParticleStart (this.transform.position);
		Invoke("StopPar",par.GetComponent<ParticleSystem>().duration);
	}
}
