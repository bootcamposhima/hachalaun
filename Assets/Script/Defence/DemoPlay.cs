﻿using UnityEngine;
using System.Collections;
using GodTouches;

public class DemoPlay : Player {

	bool isdemoplay;

	// Use this for initialization
	void Start () {
	
	}

	void Awake(){
		ismove = false;
		_movestate = 0;
		isplay = false;
		isdemoplay = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (isdemoplay) {
			// こう書ける
			GodPhase info = GodTouch.GetPhase ();
			if (info == GodPhase.Began && !ismove) {
				// タッチ開始
				Vector2 toppos=GodTouch.GetPosition();
				toppos.x=GodTouch.GetPosition().x-Screen.width/2;
				if (toppos.x >= 0) {
					toppos.x*=2;
					TopScreen (toppos);
				}
			}
		} 
		if (ismove) {
			Move ();
		}
	}

	public void StopDemoPlay(){
		isdemoplay = false;
	}

	public void PlayStart(){
		Vector3 ppos = this.gameObject.transform.localPosition;
		if (ppos.x != 0 || ppos.z != 0) {
			StartParticle ();
			this.gameObject.SetActive (false);
			Invoke ("AddPlayerComporment", 0.3f);
		}
	}

	public void AddPlayerComporment(){
		this.gameObject.SetActive (true);
		this.gameObject.transform.localPosition = new Vector3 (0.0f, 1.5f, 0.0f);
		StartParticle ();
		Destroy (this);
	}

}
