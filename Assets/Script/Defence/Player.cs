﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using GodTouches;
using SocketIO;
using LitJson;

public class Player : Charctor {
	
	//キャラの高さ
	float m_y=1.5f;
	//移動のステート
	protected int _movestate;
	//アニメーションの値
	float _anim=0;
	// アニメーション
	bool isanim=false;

	//Fightモードかどうか
	bool isfight;
	//FightモードでPlayerかどうか
	bool isPlayer;
	//このゲームの開始時間
	float _time;

	//色
	Color _initcolor;
	//初期のHP
	int _hp;
	//回復値
	int _recoverylife;
	//HPのSprite
	GameObject _hpgauge;
	GameObject _hptext;

	//Debug
	int calldelaymove;

	// Use this for initialization
	void Start () {
		par = null;
		movePar = null;
		endPar = new GameObject[6];
		isfight = (SceneManager.GetActiveScene ().name == "FightRound") ? true : false;
		m_pos = new Vector3 (0, m_y, 0);

		this.transform.localPosition = m_pos;

		StartParticle ();

		//_initcolor = this.gameObject.transform.FindChild ("Box001").gameObject.GetComponent<Renderer> ().material.color;
		_initcolor = this.GetComponent<Renderer>().material.color;
	}
		
	void Awake(){
		_time = 0;
		isfight = false;
		ismove = false;
		_movestate = 0;
		isplay = false;
		isPlayer = false;

		calldelaymove = 0;
	}

	public bool FightPlayer{
		set{
			isPlayer = value;
		}
	}

	public float StartTime{
		set{
			_time = value;
		}
	}

	public bool Play{
		set{
			isplay = value;
		}
	}

	public void SetHP(GameObject hpgaug,GameObject hptext){
		_hp = _chardata._hp;

		_recoverylife = _chardata._hp / 10 * 3;

		_hpgauge = hpgaug;
		_hptext = hptext;
		FindObjectOfType<HPManager> ().init (_chardata._hp);
	}
	
	// Update is called once per frame
	void Update () {

		if (isplay && isPlayer) {
			// こう書ける
			GodPhase info = GodTouch.GetPhase ();
			if (info == GodPhase.Began && !ismove) {
				// タッチ開始
				Vector2 sc = GodTouch.GetPosition ();
				TopScreen (sc);
			}
			if (isanim)
				Flashing ();
		} 
		if (ismove) {
			Move ();
		}
	}

	protected void TopScreen(Vector2 top){
		// タッチ開始
		Vector2 _sc = new Vector2 (Screen.width, Screen.height);
		if (TriangleCollision (new Vector2 (0, _sc.y), _sc / 2, _sc, top)) {
			if (m_pos.z != 8) {
				m_pos.z += 4;
				_movestate = 2;
				ismove = true;
				this.transform.eulerAngles = Vector3.zero;
			}
		} else if (TriangleCollision (_sc, new Vector2 (_sc.x, 0), _sc / 2, top)) {
			if (m_pos.x != 8) {
				m_pos.x += 4;
				_movestate = 1;
				ismove = true;
				this.transform.eulerAngles = new Vector3 (0, 90, 0);
			}
		} else if (TriangleCollision (_sc / 2, new Vector2 (_sc.x, 0), Vector2.zero, top)) {
			if (m_pos.z != -8) {
				m_pos.z -= 4;
				_movestate = 2;
				_chardata._spd *= -1;
				ismove = true;
				this.transform.eulerAngles = new Vector3 (0, 180, 0);
			}
		} else if (TriangleCollision (Vector2.zero, _sc / 2, new Vector2 (0, _sc.y), top)) {
			if (m_pos.x != -8) {
				m_pos.x -= 4;
				_movestate = 1;
				_chardata._spd *= -1;
				ismove = true;
				this.transform.eulerAngles = new Vector3 (0, -90, 0);
			}
		}

		if (ismove) {
			if (isfight&&isplay) {
				PlayerJson pj = new PlayerJson (new MoveJson (this.transform.localPosition, m_pos, _chardata._spd, _movestate,Time.time-_time));
				SummarizeJson sj = new SummarizeJson (2, pj);
				MessageJson mj = new MessageJson (sj);
				JSONObject job = JSONObject.Create (LitJson.JsonMapper.ToJson (mj));
				FindObjectOfType<FightSocket> ().EmitMassage (job);
			}
		}

	}

	protected bool TriangleCollision(Vector2 a,Vector2 b,Vector2 c,Vector2 p)
	{
		//外積の値
		float[] cp = new float[3];
		//ベクトルA
		Vector2 av=b-a;
		//ベクトルB
		Vector2 bv=p-b;
		//外積の計算
		cp[0]=av.x*bv.y-av.y*bv.x;
		//ベクトルA
		av=c-b;
		//ベクトルB
		bv=p-c;
		//外積の計算
		cp[1]=av.x*bv.y-av.y*bv.x;
		//ベクトルA
		av=a-c;
		//ベクトルB
		bv=p-a;
		//外積の計算
		cp[2]=av.x*bv.y-av.y*bv.x;

		if ((cp [0] > 0 && cp [1] > 0 && cp [2] > 0)||(cp [0] < 0 && cp [1] < 0 && cp [2] < 0)) {
			return true;
		}
		return false;
	}

	public override void MoveStart (){
		ismove = false;
	}

	public void FightMoveStart(){
		Debug.Log ("FightMoveStart");
		ismove = true;
	}

	//通信の遅延によっての移動距離を計算し移動する
	bool DelayMove(MoveJson mj){
		Debug.Log ("calldelaymove"+calldelaymove);
		calldelaymove++;

		//現在の経過時間を出す
		float t = Time.time - _time;
		//遅延情報
		float t2 = t - float.Parse (mj.time);
		//移動しきるのにかかる時間
		float movetime = 4 / _chardata._spd;
		Debug.Log (movetime);
		Debug.Log (t2);


		if (t2 < 0) {
			//Hostがタップした時間より時間が経過していない場合
			Debug.Log ("Stay");
			Debug.Log (t2);
			Invoke ("FightMoveStart", t2 * -1);
			return false;
		} else if (t2 > movetime) {
			//移動しきるほど時間が経過している場合
			Debug.Log ("changepos");
			this.gameObject.transform.position = new Vector3 ((float)mj.movepos.x, (float)mj.movepos.y, (float)mj.movepos.z);
			return true;
		} else {
			//移動しきるほどではないが時間が経過しいている場合
			Debug.Log ("DelayMove");
			Vector3 pos = this.gameObject.transform.position;

			switch (_movestate) {
			case 1:
				this.gameObject.transform.position = new Vector3 (pos.x + (t2*float.Parse(mj.charspd)), pos.y, pos.z);
				break;
			case 2:
				this.gameObject.transform.position = new Vector3 (pos.x, pos.y, pos.z + (t2*float.Parse(mj.charspd)));
				break;

			default:
				break;
			}
			ismove = true;
			return false;
		}
	}

	protected override void Move (){

		Vector3 pos = this.gameObject.transform.position;

		//bool isfinish = false;

		float p = 0;

		switch (_movestate) {
		case 1:
			this.gameObject.transform.position = new Vector3 (pos.x + _chardata._spd, pos.y, pos.z);
			p = m_pos.x - (pos.x + _chardata._spd);
			break;
		case 2:
			this.gameObject.transform.position = new Vector3 (pos.x, pos.y, pos.z + _chardata._spd);
			p = m_pos.z - (pos.z + _chardata._spd);
			break;

		default:
			break;
		}

		if ((p > _chardata._spd | p > _chardata._spd * -1) && (p < _chardata._spd | p < _chardata._spd * -1)) {
			this.gameObject.transform.position = m_pos;
			ismove = false;
			if (_chardata._spd < 0)
				_chardata._spd *= -1;
		}

		// pの位置に表示
		MoveParticle();
	}

	private void Flashing(){
		if (_anim > 30) {
			//this.gameObject.transform.FindChild ("Box001").gameObject.GetComponent<Renderer> ().material.color = _initcolor;
			this.gameObject.GetComponent<Renderer> ().material.color = _initcolor;
			isanim = false;
		} else
			//this.gameObject.transform.FindChild ("Box001").gameObject.GetComponent<Renderer> ().material.color = new Color (1.0f, 0.2f, 0.2f, 0.0f);
			this.gameObject.GetComponent<Renderer>().material.color = new Color (1.0f, 0.2f, 0.2f, 0.0f);
			_anim++;
	}

	public void RecoveryRound(){
		if (_hp + _recoverylife > _chardata._hp) {
			_hp = _chardata._hp;
		} else {
			_hp += _recoverylife;
		}

		//FindObjectOfType<HPManager>().
	}

	void ChangeHP(){
		int num = (_hp*100) / _chardata._hp;
		_hptext.GetComponent<Text> ().text = num.ToString () + "%";
		float numx = 3.5f * num;
		_hpgauge.GetComponent<RectTransform> ().sizeDelta = new Vector2 (numx, 50.0f);
	}

	void CreateCharctorData(CharData ca,string str){
		TextLodaer txl = TextLodaer.Instance;
		txl.WriteTextFile (str, JsonUtility.ToJson (ca));
	}

	public int GetHP(){
		return (_hp*100) / _chardata._hp;
	}

	public override void HitBullet(int damage,Direction direction){
		if (isfight && isPlayer) {
			PlayerJson pj = new PlayerJson (new HitJson (damage, direction));
			SummarizeJson sj = new SummarizeJson (2, pj);
			MessageJson mj = new MessageJson (sj);
			JSONObject job = JSONObject.Create (LitJson.JsonMapper.ToJson (mj));
			FindObjectOfType<FightSocket> ().EmitMassage (job);
		} else if (isfight && !isPlayer)
			return;

		_hp -= damage;
		isanim = true;
		DamegeParticle ();
		_anim = 0;

		if (_hp <= 0) {
			_hp = 0;
			if (isplay) {
				this.GetComponentInParent<DefenceManager> ().GameOver ();
				isplay = false;
			}
		}
		FindObjectOfType<HPManager> ().Damage (damage);
	}

	public override void StartParticle(){
		if(par)
			FindObjectOfType<ParticleManager> ().ParticleStop (par);
		par = FindObjectOfType<ParticleManager> ().CharChangeParticleStart (this.transform.position);
		Invoke("StopPar",par.GetComponent<ParticleSystem>().duration);
	}

	void DamegeParticle(){
		if(par)
			FindObjectOfType<ParticleManager> ().ParticleStop (par);
		par = FindObjectOfType<ParticleManager> ().DamegeParticleStart (this.transform.position);
		Invoke("StopPar",par.GetComponent<ParticleSystem>().duration);
	}

	//送られてきた敵の操作メッセージ
	public void Massage(SocketIOEvent e){
		PlayerJson pj = LitJson.JsonMapper.ToObject<SummarizeJson> (e.data.GetField ("value").ToString()).pjson;
		if (pj.num==0) {
			MoveJson mj = pj.mj;
			this.transform.localPosition = new Vector3 ((float)mj.position.x, (float)mj.position.y, (float)mj.position.z);
			if (!DelayMove (mj)) {
				m_pos = new Vector3 ((float)mj.movepos.x, (float)mj.movepos.y, (float)mj.movepos.z);
				_chardata._spd = float.Parse(mj.charspd);
				_movestate = mj.movestate;
			}
		} else {
			HitJson hj = pj.hj;

			_hp -= hj.damage;
			isanim = true;
			DamegeParticle ();
			_anim = 0;

			if (_hp <= 0) {
				_hp = 0;
				this.GetComponentInParent<DefenceManager> ().GameOver ();
			}
			FindObjectOfType<HPManager> ().Damage (hj.damage);
		}
	}
		
	public void MoveParticle(){
		if (movePar) 
			FindObjectOfType<ParticleManager> ().ParticleStop (movePar);
		movePar = FindObjectOfType<ParticleManager> ().CharChangeParticleStart (this.gameObject.transform.position);
		Invoke ("StopMovePar", movePar.GetComponent<ParticleSystem> ().duration);
	}
}

public struct PlayerJson{
	public int num;
	public MoveJson mj;
	public HitJson hj;

	public PlayerJson(MoveJson m){
		num = 0;
		mj = m;
		hj = new HitJson (0);
	}

	public PlayerJson(HitJson h){
		num = 1;
		mj = new MoveJson(0);
		hj = h;
	}

	public PlayerJson(int n){
		num = 0;
		mj = new MoveJson(0);
		hj = new HitJson (0);
	}

}

public struct MoveJson{
	public JsonVector3 position;
	public JsonVector3 movepos;
	public int movestate;
	public string charspd;
	public string time;

	public MoveJson(Vector3 pos,Vector3 mp,float cspd,int ms,float t){
		position = new JsonVector3 (pos);
		movepos = new JsonVector3 (mp);
		charspd = cspd.ToString();
		movestate = ms;
		time = t.ToString ();;
	}

	public MoveJson(int num){
		position = new JsonVector3(0);
		movepos = new JsonVector3(0);
		charspd = "0";
		movestate = 0;
		time = Time.time.ToString();
	}

}

public struct HitJson{
	public int damage;
	public Direction direction;

	public HitJson(int d,Direction di){
		damage = d;
		direction = di;
	}

	public HitJson(int num){
		damage = 0;
		direction = Direction.East;
	}
		
}

public struct JsonVector3{
	public int x;
	public int y;
	public int z;

	public JsonVector3(Vector3 v){
		x = (int)v.x;
		y = (int)v.y;
		z = (int)v.z;
	}

	public JsonVector3(int num){
		x = num;
		y = num;
		z = num;
	}
}