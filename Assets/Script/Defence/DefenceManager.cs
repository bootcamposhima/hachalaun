﻿using UnityEngine;
using System.Collections;

public class DefenceManager : MonoBehaviour {

	[SerializeField]
	GameObject _hpgauge;
	[SerializeField]
	GameObject _hptext;

	[SerializeField]
	GameObject _AI;

	[SerializeField]
	GameObject round;

	GameObject ob;

	bool _isAI;

	// Use this for initialization
	void Start () {
		_isAI = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public GameObject CreatDefenceCharctor(int charNum,bool isAI){
		DestroyCharacter ();
		ob = Instantiate (Factory.Instance.CreateCharactor(charNum));
		ob.AddComponent<Rigidbody> ();
		ob.GetComponent<Rigidbody> ().constraints = RigidbodyConstraints.FreezeRotation;
		// スクリプトをつける
		if (isAI) {
			GameObject _object = Instantiate (_AI);
			_object.transform.parent = ob.transform;
			_object.GetComponent<BlackBoard> ().setParent (ob);
			//print (_object.GetComponent<BlackBoard> ().getParent());
			ShirkAI ai = ob.AddComponent<ShirkAI> ();
			ai.setCharData (charNum);
			ai.SetHP (_hpgauge,_hptext);
			ai.InitBlacBoard (_object.GetComponent<BlackBoard> ());
			_object.GetComponent<BehaviorTree> ().Play ();
		} else {
			Player pl = ob.AddComponent<Player> ();
			pl.setCharData (charNum);
			pl.SetHP (_hpgauge,_hptext);
		}

		// 子供にする
		ob.transform.parent = this.transform;
		// レイヤーをプレイヤーにする
		ob.layer = LayerMask.NameToLayer("Player");
		_isAI = isAI;

		return ob;
	}

	public void PlayerStart(){
		if(ob)
			ob.GetComponent<Charctor> ().IsPlay = true;
	}

	public void MoveStop(){
		if(ob)
			ob.GetComponent<Charctor>().MoveStop();
	}

	public void MoveReStart(){
		if(ob)
			ob.GetComponent<Charctor> ().MoveReStart ();
	}

	public int GetHP(){
		if (_isAI)
			return this.GetComponentInChildren<ShirkAI> ().GetHP ();
		else
			return this.GetComponentInChildren<Player> ().GetHP ();
	}

	public void DestroyCharacter(){
		foreach (Transform child in this.transform) {
			Destroy (child.gameObject);
		}
		ob = null;
	}

	public void EndAction(){
		ob.GetComponent<Charctor> ().EndAction ();
	}

	public void SetGameSpeed(float speed){
		if(_isAI)
			this.GetComponentInChildren<Charctor>().SetGameSpeed(speed);
	}

	public void GameOver(){
		round.GetComponent<Round> ().OnFinishBattle ();
	}
}
