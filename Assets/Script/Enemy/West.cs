﻿using UnityEngine;
using System.Collections;

public class West : Enemy {

	// Use this for initialization
	void Start () {
	}

	void Awake (){
		m_pos = new Vector3 (11,20, 0);
		//初期座標に移動
		this.gameObject.transform.position = m_pos;
		//現在実行しているAIの番号
		_ainum = 0;
		//移動フラグ
		ismove=false;
		//スタートフラグ
		isStart = true;
		shotPar = null;
		isEnd = false;
		//ファクトリースクリプト
		f = Factory.Instance;

		gameSpeed = 1.0f;

		_bullettechnich = 0;

		float y = this.transform.localPosition.y;
		// 60が時間
		hayasa = y / 60;

		this.gameObject.layer = 13;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void FixedUpdate(){
		if (isStart)
			StartAction ();
		if(ismove && !isEnd)
			Move ();
	}

	public override void MoveSatrtPosition (){
		_ainum = 0;
		ActionManager.Instance.StartMoveAction (this.gameObject, new Vector3 (11.0f, 1.0f, 0), 1.0f);
	}
		
	public override void MoveStart (){
		if (_aidata._position [0] < m_pos.z)
			_chardata._spd *= -1;

		m_pos.z = _aidata._position [0]*4;
		ismove = true;
		isEnd = false;
		Factory.Instance.BulletisStop (false);
	}

	protected override void ChangeMove (){
		if (_aidata._position.Length - 1 == _ainum) {
			ismove = false;
			_ainum = 0;
			this.GetComponentInParent<EnemyManager> ().AIEndCount ();
		} else {
			_ainum++;

			if (_chardata._spd < 0)
				_chardata._spd *= -1;
		
			if (_aidata._position [_ainum]*4 < m_pos.z)
				_chardata._spd *= -1;

			m_pos.z = _aidata._position [_ainum]*4;

			Invoke ("setMove", _bullettechnich - _chardata._technique / gameSpeed);
			_bullettechnich = _chardata._technique * 2;
		}
	}

	protected override void setCoordinate(){
	}

	protected override void Shot (){
		// パーティクルとサウンド
		SoundManager.Instance.SEShotPlay ();
		ShotParticle ();

		GameObject bullet = f.CreateBullet (_aidata._bullet [_ainum]);
		bullet.layer = this.gameObject.layer;
		bullet.transform.localPosition = m_pos;
		bullet.GetComponent<Bullet> ().Shot ();
		bullet.GetComponent<Bullet> ().direction = Direction.West;
		bullet.GetComponent<Bullet> ().SetGameSpeed (gameSpeed);
		_bullettechnich = bullet.GetComponent<Bullet> ().technique;
		ChangeMove ();
	}

	protected override void Move(){
		Vector3 p = this.transform.localPosition;

		bool isshot = false;
		if (_chardata._spd >= 0) {
			if (m_pos.z <= p.z) {
				isshot = true;
			}
		} else {
			if (m_pos.z >= p.z) {
				isshot = true;
			}
		}

		if (isshot) {
			Invoke("Shot", _chardata._technique / gameSpeed);
			FindObjectOfType<StagePanelManager> ().LightAction ((int)Direction.West, m_pos.z, _chardata._technique / gameSpeed);
			IsMove = false;
			return;
		}
		this.transform.localPosition = new Vector3 (p.x, p.y, p.z + _chardata._spd * gameSpeed);
	}

	public void ShotParticle(){
		if(shotPar)
			FindObjectOfType<ParticleManager> ().ParticleStop (shotPar);
		shotPar = FindObjectOfType<ParticleManager> ().ShotParticleStart (new Vector3(9.5f,m_pos.y,m_pos.z));
		Invoke("StopShotPar",shotPar.GetComponent<ParticleSystem>().duration);
	}
}
