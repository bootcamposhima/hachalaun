﻿using UnityEngine;
using System.Collections;
using LitJson;

public class EnemyManager : MonoBehaviour {

	[SerializeField]
	GameObject Round;

	protected int AIEndNum;

	AIDatas _ai;
	float time;
	bool isDemo;
	bool isPlay;

	// Use this for initialization
	void Start () {
		AIEndNum = 0;
		time = 0;
		isDemo = false;
		isPlay = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (AIEndNum >= 4) {
			time += Time.deltaTime;
			if (time >= 1.5f) {
				if (isDemo) {
					isDemo = false;
					MoveStop ();
					Factory.Instance.BulletFlyweight (false);
				} else {
					isPlay = false;
					Round.GetComponent<Round> ().OnFinishBattle ();
				}
				AIEndNum = 0;
				time = 0;
			}
		}
	}

	public AIDatas aidatas{
		get{
			return _ai;
		}
	}

	public void DemoStart(){
		if (!isPlay) {
			isDemo = true;
			MoveStart ();
		}
	}

	// 敵の生成
	public void CreateEnemies(AIDatas aidatas,bool isDemoPlay){
		_ai = aidatas;
		Factory fc = Factory.Instance;
		isPlay = false;
		for (int i = 0; i < 4; i++) {
			GameObject ob = Instantiate (fc.CreateCharactor(aidatas._dataList[i]._charactornumber));

			setScript (ob, aidatas._dataList [i]._direction);

			ob.GetComponent<Charctor> ().setCharData (aidatas._dataList [i]._charactornumber);

			ob.GetComponent<Enemy> ().Aidata = aidatas._dataList[i];

			// 配置スタートを呼ぶ<-これやる
			ob.GetComponent<Enemy>().StartParticle();

			ob.transform.parent = this.transform;
			if (isDemoPlay)
				Invoke ("DemoStart", 1.5f);
			else
				isPlay = true;
		}
	}
		

	public void DestroyEnemies(){
		for (int i = 0; i < 4; i++) {
			if (this.transform.childCount > 0)
				this.GetComponentsInChildren<Enemy> () [i].DestroyMe ();
		}
	}

	public void EndAction(){
		for (int i = 0; i < 4; i++) {
			if (this.transform.childCount > 0)
				this.GetComponentsInChildren<Enemy> () [i].EndAction ();
		}
	}

	public void MoveSatrtPosition(){
		isPlay = true;
		for (int i = 0; i < 4; i++) {
			if (this.transform.childCount > 0)
				this.GetComponentsInChildren<Enemy> () [i].MoveSatrtPosition ();
		}
		isDemo = false;
	}

	public void MoveStart(){
		for (int i = 0; i < 4; i++) {
			if (this.transform.childCount > 0)
				this.GetComponentsInChildren<Enemy>()[i].MoveStart ();
		}
	}

	public void MoveStop(){
		for (int i = 0; i < 4; i++) {
			if (this.transform.childCount > 0)
				this.GetComponentsInChildren<Enemy> () [i].MoveStop ();
		}
	}

	public void MoveReStart(){
		for (int i = 0; i < 4; i++) {
			if (this.transform.childCount > 0)
				this.GetComponentsInChildren<Enemy> () [i].MoveReStart ();
		}
	}

	public void SetGameSpeed(float speed){
		for (int i = 0; i < 4; i++) {
			if (this.transform.childCount > 0)
				this.GetComponentsInChildren<Enemy> () [i].SetGameSpeed (speed);
		}
	}

	public virtual void AIEndCount(){
		AIEndNum++;
	}
		
	void setScript(GameObject _enemy,int num){
		switch (num) {
		case 0:
			_enemy.AddComponent <East>();
			break;
		case 1:
			_enemy.AddComponent <West>();
			break;
		case 2:
			_enemy.AddComponent <South>();
			break;
		case 3:
			_enemy.AddComponent <North>();
			break;
		}
	}
}
