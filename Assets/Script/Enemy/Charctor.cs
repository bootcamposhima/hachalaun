﻿using UnityEngine;
using System.Collections;

public abstract class Charctor : MonoBehaviour {

	//移動座標
	protected Vector3 m_pos;
	//移動フラグ
	protected bool ismove;
	//キャラクターのデータ
	protected CharData _chardata;

	protected float gameSpeed;

	protected bool isplay;

	protected GameObject par;

	protected GameObject[] endPar;

	protected GameObject movePar;

	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {
	}

	public virtual void setCharData(int num){
		_chardata = GetCharData(num);
	}

	protected bool IsMove{
		get{
			return ismove;
		}
		set{
			ismove = value;
		}
	}
	public bool IsPlay{
		get{
			return isplay;
		}
		set{
			isplay = value;
		}
	}

	protected virtual void setMove()
	{
		IsMove = !ismove;
	}

	protected virtual CharData GetCharData(int datanum){

		CharData cd;

		string _fn;

		switch (datanum) {
		case 0:
			_fn="CharactorData/Charactor1";
			break;

		case 1:
			_fn="CharactorData/Charactor2";
			break;

		case 2:
			_fn="CharactorData/Charactor3";
			break;

		case 3:
			_fn="CharactorData/Charactor4";
			break;

		default :
			_fn="CharactorData/Charactor1";
			break;
		}

		TextLodaer txl = TextLodaer.Instance;
		string jstr = txl.ReadTextFile (_fn);
		cd = JsonUtility.FromJson<CharData> (jstr);

		return cd;
	}

	public abstract void MoveStart ();

	public virtual void HitBullet (int damage,Direction direction){
	}

	public virtual void MoveStop(){
		ismove = false;
		isplay = false;
	}

	public virtual void MoveReStart(){
		ismove = true;
		isplay = true;
	}

	protected abstract void Move ();

	public virtual void SetGameSpeed(float speed){
		gameSpeed = speed;
	}

	public virtual void DestroyMe(){
		if(par)
			FindObjectOfType<ParticleManager> ().ParticleStop (par);
		if(movePar)
			FindObjectOfType<ParticleManager> ().ParticleStop (movePar);
		
		Destroy (this.gameObject);
	}

	public abstract void StartParticle ();

	protected void StopPar(){
		FindObjectOfType<ParticleManager> ().ParticleStop (par);
		par = null;
	}

	protected void StopEndPar(int num){
		FindObjectOfType<ParticleManager> ().ParticleStop (endPar[num]);
		endPar [num] = null;
	}

	protected void StopMovePar(){
		FindObjectOfType<ParticleManager> ().ParticleStop (movePar);
		movePar = null;
	}

	public virtual void EndAction(){
		print ("Action");
		StartCoroutine ("EndActionCol");
	}

	IEnumerator EndActionCol(){
		// 1個目
		EndParticle (0);
		EndParticle2 (3);
		yield return new WaitForSeconds (endPar[0].GetComponent<ParticleSystem>().duration/2);
		// 2個目
		EndParticle (1);
		EndParticle2 (4);
		yield return new WaitForSeconds (endPar[1].GetComponent<ParticleSystem>().duration/2);
		// １個目止める
		StopEndPar (0);
		StopEndPar (3);
		// 3個目
		EndParticle (2);
		EndParticle2 (5);
		yield return new WaitForSeconds (endPar[2].GetComponent<ParticleSystem>().duration);
		// 2個目止める
		StopEndPar (1);
		StopEndPar (4);
		yield return new WaitForSeconds (endPar[2].GetComponent<ParticleSystem>().duration);
		// 3個目止める
		StopEndPar (2);
		StopEndPar (5);
		// 消す
		DestroyMe ();
	}

	protected virtual void EndParticle(int num){
		SoundManager.Instance.SEBomPlay ();
		if(endPar[num])
			FindObjectOfType<ParticleManager> ().ParticleStop (endPar[num]);
		float x;
		if (num == 0 || num == 2)
			x = 2.0f;
		else
			x = -2.0f;
		endPar[num] = FindObjectOfType<ParticleManager> ().BloodParticleStart (new Vector3(transform.position.x + x,transform.position.y + 5.0f,transform.position.z));
	}

	protected virtual void EndParticle2(int num){
		if(endPar[num])
			FindObjectOfType<ParticleManager> ().ParticleStop (endPar[num]);
		float x;
		if (num == 3 || num == 5)
			x = 2.0f;
		else
			x = -2.0f;
		endPar[num] = FindObjectOfType<ParticleManager> ().ExplosionsParticleStart (new Vector3(x,16.0f,-8.0f));
	}
}
	
public struct CharData{
	//キャラのヘルスポイント
	public int _hp;
	//移動スピード
	public float _spd;
	//テクニック
	public float _technique;

	public CharData(int hp, float spd, float technique){
		_hp = hp;
		_spd = spd;
		_technique = technique;
	}
}