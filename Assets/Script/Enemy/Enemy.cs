﻿using UnityEngine;
using System.Collections;

public abstract class Enemy : Charctor {

	//キャラのオブジェクト
	//protected GameObject m_enemy;
	//AIのデータ
	protected AIData _aidata;
	//実行しているAIの番数
	protected int _ainum;
	//ファクトリークラス
	protected Factory f;
	// スタート中か
	protected bool isStart;
	//
	protected float hayasa;

	protected float _bullettechnich;

	protected GameObject shotPar;

	protected bool isEnd;

	public AIData Aidata{
		set{
			_aidata = value;
		}
	}

	protected abstract void ChangeMove ();

	protected abstract void setCoordinate();

	protected abstract void Shot ();

	public abstract void MoveSatrtPosition ();

	public virtual void StartAction (){
		Vector3 p = this.transform.localPosition;
		this.transform.localPosition = new Vector3 (p.x, p.y - hayasa, p.z);
		if (this.transform.localPosition.y <= 1) {
			isStart = false;
			m_pos.y = this.transform.localPosition.y;
		}
	}

	public override void StartParticle(){
		SoundManager.Instance.SEFiringPlay ();
		if(par)
			FindObjectOfType<ParticleManager> ().ParticleStop (par);
		par = FindObjectOfType<ParticleManager> ().AdmissionParticleStart (new Vector3(this.transform.position.x,1.0f,this.transform.position.z));
		Invoke("StopPar",par.GetComponent<ParticleSystem>().duration);
	}
		
	IEnumerator EndActionCol(){
		SoundManager.Instance.SEFiringPlay ();
		while (this.transform.localPosition.y <= 20) {
			Vector3 p = this.transform.localPosition;
			this.transform.localPosition = new Vector3 (p.x, p.y + hayasa, p.z);
			yield return null;
		}

		DestroyMe ();
	}

	protected override void EndParticle(int nun){
		if(par)
			FindObjectOfType<ParticleManager> ().ParticleStop (par);
		par = FindObjectOfType<ParticleManager> ().SoriteParticleStart (new Vector3(transform.position.x,transform.position.y + 5.0f,transform.position.z));
		Invoke("StopPar",par.GetComponent<ParticleSystem>().duration);
	}

	protected void StopShotPar(){
		FindObjectOfType<ParticleManager> ().ParticleStop (shotPar);
		shotPar = null;
	}

	public override void MoveStop(){
		ismove = false;
		isEnd = true;
		Factory.Instance.BulletisStop (true);
		CancelInvoke ();
	}

	public override void MoveReStart(){
		ismove = true;
		isEnd = false;
		Factory.Instance.BulletisStop (false);
	}

	public override void EndAction(){
		ismove = false;
		isEnd = true;
		EndParticle (0);
		StartCoroutine ("EndActionCol");
	}
}

public enum Direction{
	None=-1,
	East=0,
	West=1,
	South=2,
	North=3
}