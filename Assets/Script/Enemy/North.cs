﻿using UnityEngine;
using System.Collections;

public class North : Enemy {

	// Use this for initialization
	void Start () {
	}

	void Awake (){
		m_pos = new Vector3 (0, 20, 11);
		//初期座標に移動
		this.gameObject.transform.position = m_pos;
		//現在実行しているAIの番号
		_ainum = 0;
		//移動フラグ
		ismove=false;
		//スタートフラグ
		isStart = true;
		shotPar = null;
		isEnd = false;
		//ファクトリースクリプト
		f = Factory.Instance;

		gameSpeed = 1.0f;

		_bullettechnich = 0;

		float y = this.transform.localPosition.y;
		// 60が時間
		hayasa = y / 60;

		this.gameObject.layer = 15;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void FixedUpdate(){
		if (isStart)
			StartAction ();
		if(ismove && !isEnd)
			Move ();
	}

	public override void MoveSatrtPosition (){
		_ainum = 0;
		ActionManager.Instance.StartMoveAction (this.gameObject, new Vector3 (0, 1.0f, 11.0f), 1.0f);
	}
		
	public override void MoveStart (){
		if (_aidata._position [0] < m_pos.x)
			_chardata._spd *= -1;

		m_pos.x = _aidata._position [0]*4;
		ismove = true;
		isEnd = false;
		Factory.Instance.BulletisStop (false);
	}

	protected override void ChangeMove (){
		if (_aidata._position.Length - 1 == _ainum) {
			ismove = false;
			_ainum = 0;
			this.GetComponentInParent<EnemyManager> ().AIEndCount ();
		} else {
			_ainum++;

			if (_chardata._spd < 0)
				_chardata._spd *= -1;

			if (_aidata._position [_ainum]*4 < m_pos.x)
				_chardata._spd *= -1;

			m_pos.x = _aidata._position [_ainum]*4;
			Invoke ("setMove", _chardata._technique / gameSpeed);
			_bullettechnich = _chardata._technique * 2;
		}
	}

	protected override void setCoordinate(){
	}

	protected override void Shot (){
		// パーティクルとサウンド
		SoundManager.Instance.SEShotPlay ();
		ShotParticle ();
		GameObject bullet = f.CreateBullet (_aidata._bullet [_ainum]);
		bullet.layer = this.gameObject.layer;
		bullet.transform.localPosition = m_pos;
		bullet.GetComponent<Bullet> ().direction = Direction.North;
		bullet.GetComponent<Bullet> ().SetGameSpeed (gameSpeed);
		bullet.GetComponent<Bullet> ().Shot ();
		_bullettechnich = bullet.GetComponent<Bullet> ().technique;
		ChangeMove ();

	}

	protected override void Move(){
		Vector3 p = this.transform.localPosition;

		bool isshot = false;
		if (_chardata._spd >= 0) {
			if (m_pos.x <= p.x) {
				isshot = true;
			}
		} else {
			if (m_pos.x >= p.x) {
				isshot = true;
			}
		}

		if (isshot) {
			
			//DelayMethodを3.5秒後に呼び出す
			Invoke("Shot",_chardata._technique / gameSpeed);
			FindObjectOfType<StagePanelManager> ().LightAction ((int)Direction.North, m_pos.x, _chardata._technique / gameSpeed);
			IsMove = false;
			return;
		}

		this.transform.localPosition = new Vector3 (p.x + _chardata._spd * gameSpeed, p.y, p.z);
	}

	public void ShotParticle(){
		if(shotPar)
			FindObjectOfType<ParticleManager> ().ParticleStop (shotPar);
		shotPar = FindObjectOfType<ParticleManager> ().ShotParticleStart (new Vector3(m_pos.x,m_pos.y,9.5f));
		Invoke("StopShotPar",shotPar.GetComponent<ParticleSystem>().duration);
	}
}
