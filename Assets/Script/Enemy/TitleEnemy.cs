﻿using UnityEngine;
using System.Collections;

public class TitleEnemy : EnemyManager {

	// Use this for initialization
	void Start () {
		AIDatas ai = AI.LoadAIData ("AI/AI1.text");
		CreateEnemies (ai,false);
		Invoke ("MoveStart",2.0f);
		AIEndNum = 0;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public override void AIEndCount ()
	{
		AIEndNum++;
		if (AIEndNum >= 4) {
			Invoke ("MoveStart", 2.0f);
			AIEndNum = 0;
		}
	}
}
