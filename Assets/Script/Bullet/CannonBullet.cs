﻿using UnityEngine;
using System.Collections;

public class CannonBullet : Bullet {

	// Use this for initialization
	void Start () {
		_spd = 0.4f;
		_power = 30;
		isflyweight = true;
		gameSpeed = 1.0f;
		_technique = 1.5f;
	}
	
	// Update is called once per frame
	void Update () {
		Move ();
	}
}
