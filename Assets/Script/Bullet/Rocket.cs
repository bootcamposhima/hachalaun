﻿using UnityEngine;
using System.Collections;

public class Rocket : Bullet {

	// Use this for initialization
	void Start () {
		_spd = 1.0f;
		_power = 50;
		isflyweight = true;
		gameSpeed = 1.0f;
		_technique = 3.0f;
	}
	
	// Update is called once per frame
	void Update () {
		Move ();
	}
}
