﻿using UnityEngine;
using System.Collections;

public abstract class Bullet : MonoBehaviour {

	//弾の移動スピード
	protected float _spd;
	//弾の威力
	protected int _power;
	//フライウェイト管理フラグ
	protected bool isflyweight;
	//打つ方角
	protected Direction _direction;
	//ゲームの速さ
	protected float gameSpeed;
	//
	protected float _technique;

	protected bool isStop = false;

	public virtual bool Flyweight{
		get{
			return isflyweight;
		}

		set{
			isflyweight = value;
			this.gameObject.SetActive (value);
		}
	}

	public virtual bool Stop{
		get{
			return isStop;
		}

		set{
			isStop = value;
		}
	}

	public virtual float technique{
		get{
			return _technique;
		}
	}

	public virtual Direction direction{
		set{
			_direction = value;
		}
	}

	public virtual void Shot(){
		Flyweight = true;
	}

	protected virtual void Move (){
		if (isStop) {
			return;
		}

		MoveDirection ();
		Vector3 p = this.gameObject.transform.position;

		if (p.x >= 30 || p.x <= -30 || p.z >= 30 || p.z <= -30) {
			Flyweight = false;
		}
	}

	protected virtual void MoveDirection(){

		Vector3 p = this.gameObject.transform.position;

		switch(_direction)
		{
		case Direction.East:
			this.gameObject.transform.localPosition = new Vector3 (p.x + _spd * gameSpeed, p.y, p.z);
			this.gameObject.transform.eulerAngles = new Vector3 (0, 90, 0);
			break;

		case Direction.North:
			this.gameObject.transform.localPosition = new Vector3 (p.x, p.y, p.z - _spd * gameSpeed);
			this.gameObject.transform.eulerAngles = Vector3.zero;
			break;

		case Direction.South:
			this.gameObject.transform.localPosition = new Vector3 (p.x, p.y, p.z+_spd * gameSpeed);
			this.gameObject.transform.eulerAngles = Vector3.zero;
			break;

		case Direction.West:
			this.gameObject.transform.localPosition = new Vector3 (p.x-_spd * gameSpeed, p.y, p.z);
			this.gameObject.transform.eulerAngles = new Vector3 (0, 90, 0);
			break;
		}
	}

//	protected virtual Vector3 VecDirection(){
//		print (_direction);
//		switch(_direction)
//		{
//		case Direction.East:
//			return Vector3.right;
//
//		case Direction.North:
//			return Vector3.back;
//
//		case Direction.South:
//			return Vector3.forward;
//
//		case Direction.West:
//			return Vector3.left;
//
//		case Direction.None:
//			return Vector3.zero;
//		}
//
//		return Vector3.zero;
//	}

	public void SetGameSpeed(float speed){
		gameSpeed = speed;
	}

	void OnTriggerEnter(Collider other) {
		other.gameObject.GetComponent<Charctor> ().HitBullet (_power,_direction);
		Flyweight = false;
	}

}

public enum BulletNumber{
	TestBullet=0,
	CannonBullet=1,
	BigballBullet=2,
	RiceBullet=3,
	Rocket=4
}