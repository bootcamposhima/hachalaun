﻿using UnityEngine;
using System.Collections;

public class TestBullet : Bullet {

	// Use this for initialization
	void Start () {
		_spd = 0.5f;
		_power = 10;
		isflyweight = true;
		gameSpeed = 1.0f;
		_technique = 1.0f;
	}
	
	// Update is called once per frame
	void Update () {
		Move ();
	}

}
