﻿using UnityEngine;
using System.Collections;

public class BigballBullet : Bullet {

	// Use this for initialization
	void Start () {
		_spd = 0.6f;
		_power = 25;
		isflyweight = true;
		gameSpeed = 1.0f;
		_technique = 2.0f;
	}
	
	// Update is called once per frame
	void Update () {
		Move ();
	}
}
