﻿using UnityEngine;
using System.Collections;

public class RiceBullet : Bullet {

	// Use this for initialization
	void Start () {
		_spd = 1.0f;
		_power = 7;
		isflyweight = true;
		gameSpeed = 1.0f;
		_technique = 0.5f;
	}
	
	// Update is called once per frame
	void Update () {
		Move ();
	}
}
