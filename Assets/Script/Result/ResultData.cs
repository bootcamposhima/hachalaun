﻿using UnityEngine;
using System.Collections;

public class ResultData : MonoBehaviour {

	/// <summary>
	/// 記憶するデータ
	/// </summary>
	int[] _playerRemainderHPs;
	int[] _enemyRemainderHPs;
	// プレイヤーの中間リザルト
	PlayerResult[] _playerIntermediateResults;
	//相手をLostしたかどうか
	bool _isOpponentRetired;
	//サーバーに切断されたかどうか
	bool _isDisconnected;
	// 最大ラウンド数
	int  _maxRound;

	public enum PlayerResult{WIN,LOSE,DRAW}

	/// <summary>
	/// 初期化
	/// </summary>
	void Awake(){
		// シーン遷移しても消されないように
		DontDestroyOnLoad (this);

		// 初期化
		_playerRemainderHPs        = new int[]{100,100,100};
		_enemyRemainderHPs         = new int[]{100,100,100};
		_playerIntermediateResults = new PlayerResult[]{PlayerResult.DRAW,PlayerResult.DRAW,PlayerResult.DRAW};
		_isOpponentRetired         = false;
		_isDisconnected            = false;
		_maxRound 				   = 3;
	}

	/// <summary>
	/// 相手がリタイアしたか
	/// </summary>
	public bool IsOpponentRetired{
		get{
			return _isOpponentRetired;
		}
		set{
			_isOpponentRetired = value;
		}
	}

	/// <summary>
	/// 通信が切断したか
	/// </summary>
	public bool IsDisconnected{
		get{
			return _isDisconnected;
		}
		set{
			_isDisconnected = value;
		}
	}

	/// <summary>
	/// プレイヤーの中間リザルト
	/// </summary>
	public PlayerResult[] PlayerIntermediateResults{
		get{
			return _playerIntermediateResults;
		}
	}

	/// <summary>
	/// 最大ラウンド数
	/// </summary>
	public int MaxRound{
		get{
			return _maxRound;
		}
	}

	/// <summary>
	/// 残りHPの取得
	/// </summary>
	public int[] GetRemainderHP(bool isPlayer){
		if (isPlayer)
			return _playerRemainderHPs;
		else
			return _enemyRemainderHPs;
	}

	/// <summary>
	/// 現在のラウンドの残りHPの記憶
	/// </summary>
	/// remainderHP 残りHP currentRound 現在のラウンド
	/// playerRemainderHP プレイヤーの残りHP enemyRemainderHP 敵の残りHP

	/// <summary>
	/// プレイヤーのみ
	/// </summary>
	public void SetPlayerRemainderHP(int remainderHP,int currentRound){
		_playerRemainderHPs [currentRound - 1] = remainderHP;
	}

	/// <summary>
	/// 敵のみ
	/// </summary>
	public void SetEnemyRemainderHP(int remainderHP,int currentRound){
		_enemyRemainderHPs  [currentRound - 1] = remainderHP;
	}

	/// <summary>
	/// 両方
	/// </summary>
	public void SetRemainderHPs(int playerRemainderHP,int enemyRemainderHP,int currentRound){
		_playerRemainderHPs [currentRound - 1] = playerRemainderHP;
		_enemyRemainderHPs  [currentRound - 1] = enemyRemainderHP;
	}

	/// <summary>
	/// プレイヤーの中間リザルトの記憶
	/// </summary>
	public void SetPlayerIntermediateResult(PlayerResult playerIntermediateResult,int currentRound){
		_playerIntermediateResults [currentRound - 1] = playerIntermediateResult;
	}

	/// <summary>
	/// リザルトデータを消す
	/// </summary>
	public void MyDestroy(){
		Destroy (this.gameObject);
	}
}