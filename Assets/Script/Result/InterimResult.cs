﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InterimResult : MonoBehaviour {
	// 現在のラウンドテキスト
	[SerializeField]
	GameObject _currentRoundText;

	// リザルト
	[SerializeField]
	GameObject _playerResultUIRoot;
	[SerializeField]
	GameObject _enemyResultUIRoot;

	// カメラ
	[SerializeField]
	GameObject _otherUICamera;
	[SerializeField]
	GameObject _resultCamera;

	// リザルト
	ResultData.PlayerResult _result;

	/// <summary>
	/// 中間リザルトの表示設定
	/// </summary>
	public void SettingShowInterimResult(int playerHP,int enemyHP,int currentRound){
		// 現在のラウンドテキスト
		_currentRoundText.GetComponent<Text> ().text = currentRound.ToString();

		// 結果判定
		if (playerHP == enemyHP)
			_result = ResultData.PlayerResult.DRAW;
		else if (playerHP > enemyHP)
			_result = ResultData.PlayerResult.WIN;
		else
			_result = ResultData.PlayerResult.LOSE;

		// UIの表示設定
		SettingUIs (_playerResultUIRoot, playerHP, _result == ResultData.PlayerResult.WIN);
		SettingUIs (_enemyResultUIRoot , enemyHP , _result == ResultData.PlayerResult.LOSE);

		// リザルトの記憶
		FindObjectOfType<ResultData> ().SetPlayerIntermediateResult (_result, currentRound);

		// 3.0f後に表示するよう設定
		Invoke ("ShowInterimResult", 3.0f);
	}

	/// <summary>
	/// リザルトUIの表示設定
	/// </summary>
	void SettingUIs(GameObject targetUIRoot,int hp,bool isLose){
		if(isLose)
			targetUIRoot.transform.localScale = new Vector3 (0.5f, 0.5f, 1);
		
		targetUIRoot.transform.FindChild ("HPText").gameObject.GetComponent<Text>          ().text      = hp.ToString () + "%";
		targetUIRoot.transform.FindChild ("HP")    .gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (hp * 3.0f, 50.0f);
	}


	/// <summary>
	/// リザルトを表示する
	/// </summary>
	void ShowInterimResult(){
		// カメラの切り替え
		_resultCamera .SetActive (true);
		_otherUICamera.SetActive (false);

		// サウンド
		switch (_result) {
		case ResultData.PlayerResult.WIN:
			SoundManager.Instance.SEWinPlay  ();
			break;
		case ResultData.PlayerResult.LOSE:
			SoundManager.Instance.SELossPlay ();
			break;
		}

		// 3.0f後に非表示するよう設定
		Invoke ("HideInterimResult", 3.0f);
	}

	/// <summary>
	/// リザルトを非表示にする
	/// </summary>
	void HideInterimResult(){
		_resultCamera .SetActive (false);
		_otherUICamera.SetActive (true);
	}
}
