﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class Result : MonoBehaviour {
	
	/// <summary>
	/// 中間リザルト用
	/// </summary>
	[SerializeField]
	GameObject[] _playerIntermediateResultUIRoots;
	[SerializeField]
	GameObject[] _enemyIntermediateResultUIRoots;

	/// <summary>
	/// 全体リザルト用
	/// </summary>
	[SerializeField]
	GameObject _wholeResultCanvas;
	[SerializeField]
	GameObject _wholeResultUI;
	[SerializeField]
	Sprite[]   _wholeResultUISprites;

	/// <summary>
	/// 全体リザルトを出す為のデータ
	/// </summary>
	int _playerWinCount;
	int _enemyWinCount;
	int _playerTotalRemainderHP;
	int _enemyTotalRemainderHP;

	ResultData _resultData;

	/// <summary>
	/// 初期化
	/// </summary>
	void Start () {
		// 初期化
		_playerWinCount         = 0;
		_enemyWinCount          = 0;
		_playerTotalRemainderHP = 0;
		_enemyTotalRemainderHP  = 0;
		_resultData = FindObjectOfType<ResultData> ();

		// ファイトモードの場合:通信終了
		if (SceneManager.GetActiveScene ().name == "FightRound") {
			FindObjectOfType<FightSocket> ().EmitEndnotification ();
			FindObjectOfType<Fight> ().Des ();
		}

		// 中間リザルトの表示
		SettingIntermediateResult ();

		// 3.0f後に全体リザルトを表示するよう設定
		Invoke ("ShowWholeResult", 3.0f);
	}

	/// <summary>
	/// 中間リザルトの表示
	/// </summary>
	void SettingIntermediateResult(){
		// 中間リザルトのデータの取得
		int[] playerHPs = _resultData.GetRemainderHP (true);
		int[] enemyHPs  = _resultData.GetRemainderHP (false);

		// 表示設定
		for (int i = 0; i < _resultData.MaxRound; i++) {
			ResultData.PlayerResult result = DecideIntermediateResult (i);

			// UIの表示設定
			SettingUIs (_playerIntermediateResultUIRoots [i], playerHPs[i], result == ResultData.PlayerResult.LOSE);
			SettingUIs (_enemyIntermediateResultUIRoots  [i], enemyHPs [i], result == ResultData.PlayerResult.WIN);
		}
	}

	/// <summary>
	/// リザルトUIの表示設定
	/// </summary>
	void SettingUIs(GameObject targetUIRoot,int hp,bool isLose){
		if(isLose)
			targetUIRoot .transform.localScale = new Vector3 (0.5f, 0.5f, 1);

		targetUIRoot.transform.FindChild ("HPText").gameObject.GetComponent<Text>          ().text      = hp.ToString () + "%";
		targetUIRoot.transform.FindChild ("HP")    .gameObject.GetComponent<RectTransform> ().sizeDelta = new Vector2 (hp * 3.0f, 50.0f);
	}

	/// <summary>
	/// 結果判定
	/// </summary>
	ResultData.PlayerResult DecideIntermediateResult(int roundNumber){
		// 中間リザルトのデータの取得
		ResultData.PlayerResult[] playerIntermediateResults = _resultData.PlayerIntermediateResults;

		// 結果判定
		// 勝ち
		if (playerIntermediateResults [roundNumber] != ResultData.PlayerResult.DRAW)
			return playerIntermediateResults [roundNumber];
		// 相手がリタイアした(勝ち)
		if (_resultData.IsOpponentRetired)
			return ResultData.PlayerResult.WIN;
		// 通信が切れた(負け)
		if (_resultData.IsDisconnected)
			return ResultData.PlayerResult.LOSE;
		
		return ResultData.PlayerResult.DRAW;
	}

	/// <summary>
	/// 全体リザルトの表示
	/// </summary>
	void ShowWholeResult(){
		// 結果の取得
		ResultData.PlayerResult result = DecideWholeResult ();
		Image resultSprite = _wholeResultUI.GetComponent<Image> ();

		// 結果に応じた表示設定
		switch (result) {
		case ResultData.PlayerResult.WIN:
			resultSprite.sprite = _wholeResultUISprites[0];
			SoundManager.Instance.StartWinSound ();
			SoundManager.Instance.SEWinPlay ();
			ParticleManager.Instance.WinParticleStart (Vector3.zero);
			break;
		case ResultData.PlayerResult.LOSE:
			resultSprite.sprite = _wholeResultUISprites[1];
			SoundManager.Instance.StartLossSound ();
			SoundManager.Instance.SELossPlay ();
			ParticleManager.Instance.LossParticleStart (Vector3.zero);
			break;
		case ResultData.PlayerResult.DRAW:
			resultSprite.sprite = _wholeResultUISprites[2];
			break;
		}

		// 表示
		_wholeResultCanvas.SetActive (true);
	}

	/// <summary>
	/// 結果判定
	/// </summary>
	ResultData.PlayerResult DecideWholeResult(){
		// リザルトデータの取得
		PrepareDecideWholeResult ();
		// 結果判定
		if (_playerWinCount > _enemyWinCount || _resultData.IsOpponentRetired)
			return ResultData.PlayerResult.WIN;
		if (_playerWinCount < _enemyWinCount || _resultData.IsDisconnected)
			return ResultData.PlayerResult.LOSE;
		// ^の結果が引き分けだった場合:HPの差で勝敗
		if (_playerTotalRemainderHP > _enemyTotalRemainderHP)
			return ResultData.PlayerResult.WIN;
		if (_playerTotalRemainderHP < _enemyTotalRemainderHP)
			return ResultData.PlayerResult.LOSE;
		
		return ResultData.PlayerResult.DRAW;
	}

	/// <summary>
	/// 全体判定の準備
	/// </summary>
	void PrepareDecideWholeResult(){
		int[] playerHPs = _resultData.GetRemainderHP (true);
		int[] enemyHPs  = _resultData.GetRemainderHP (false);

		for (int i = 0; i < _resultData.MaxRound; i++) {
			// 勝ち数のカウント
			switch (DecideIntermediateResult (i)) {
			case ResultData.PlayerResult.WIN:
				_playerWinCount++;
				break;
			case ResultData.PlayerResult.LOSE:
				_enemyWinCount++;
				break;
			}
			// 総残りHPの計算
			_playerTotalRemainderHP += playerHPs [i];
			_enemyTotalRemainderHP  += enemyHPs  [i];
		}
	}

	/// <summary>
	/// シーン終了時
	/// </summary>
	void OnDestroy(){
		// リザルトデータの削除
		_resultData.MyDestroy ();
	}

	/// <summary>
	/// セレクトシーンへ遷移
	/// </summary>
	public void OnClickSelectButton(){
		// SEの再生
		SoundManager.Instance.SEButtonPlay ();

		StageManager.Instance.OnClickStageButton ("Select");
	}

	/// <summary>
	/// リトライする処理
	/// ファイトモード:マッチング ノーマルモード:難易度選択 の状態に戻る
	/// </summary>
	public void OnClickRetryButton(){
		// SEの再生
		SoundManager.Instance.SEButtonPlay ();

		string nextScene = (SceneManager.GetActiveScene ().name == "FightRound") ? "Matching" : "yk_test";
		StageManager.Instance.OnClickStageButton (nextScene);
	}
}
