﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SkipSpeed : MonoBehaviour {
	/// <summary>
	/// キャラクターマネジャー
	/// </summary>
	[SerializeField]
	GameObject _attackManager;
	[SerializeField]
	GameObject _defenceManager;

	/// <summary>
	/// UI
	/// </summary>
	[SerializeField]
	Sprite _normalSpeedButtonImg;
	[SerializeField]
	Sprite _fastSpeedButtonImg;

	// アタック中のゲームスピードのステータス
	int _speedState;

	/// <summary>
	/// 通常再生と倍速の切り替え
	/// </summary>
	public void SwitchingSpeedState(){
		// SEの再生
		SoundManager.Instance.SEButtonPlay ();
		// ステートとスプライトの変更
		switch (_speedState) {
		case 1://現状Normal->変更Fast
			_speedState = 2;
			this.GetComponent<Image> ().sprite = _fastSpeedButtonImg;
			break;
		case 2://現状Fast->変更Normal
			_speedState = 1;
			this.GetComponent<Image> ().sprite = _normalSpeedButtonImg;
			break;
		}
		// スピードの変更
		_defenceManager.GetComponent<DefenceManager> ().SetGameSpeed (_speedState);
		_attackManager .GetComponent<EnemyManager>   ().SetGameSpeed (_speedState);
	}

	/// <summary>
	/// 非表示にする
	/// </summary>
	public void HideSpeedButton(){
		this.gameObject.SetActive (false);
	}

	/// <summary>
	/// 表示する
	/// </summary>
	public void ShowSpeedButton(){
		_speedState = 1;
		this.GetComponent<Image> ().sprite = _normalSpeedButtonImg;
		this.gameObject.SetActive (true);
	}
}