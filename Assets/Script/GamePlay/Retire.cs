﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Retire : MonoBehaviour {
	/// <summary>
	/// UI
	/// </summary>
	[SerializeField]
	GameObject _retireCanvas;
	[SerializeField]
	GameObject _retireImage;

	/// <summary>
	/// フラグ
	/// </summary>
	bool _isFightMode;
	bool _isPlaying;
	bool _isSelected;

	/// <summary>
	/// 初期化
	/// </summary>
	void Start() {
		_isFightMode = (SceneManager.GetActiveScene ().name == "FightRound");
		_isSelected = false;
		_isPlaying  = false;
	}

	/// <summary>
	/// バトル中か
	/// </summary>
	public bool IsPlaying{
		set{
			_isPlaying = value;
		}
	}

	/// <summary>
	/// リタイア選択を表示する
	/// </summary>
	public void ShowRetireSelect(){
		// ノーマルモード:バトルの動きを止める
		if (!_isFightMode) {
			FindObjectOfType<DefenceManager> ().MoveStop ();
			FindObjectOfType<PlaySelect>     ().StopCol  ();
			FindObjectOfType<EnemyManager>   ().MoveStop ();
		}
		SoundManager .Instance.SEButtonPlay ();
		_retireCanvas.SetActive(true);
	}

	/// <summary>
	/// リタイア選択を消してバトルを再スタートする
	/// </summary>
	public void ReturnBattle(){
		if (!_isSelected) {
			_isSelected = true;
			SoundManager.Instance.SEButtonPlay ();
			_retireCanvas.SetActive (false);
			//バトル中なら再スタート
			if (_isPlaying) {
				FindObjectOfType<PlaySelect> ().ShowStart ();
			}
		}
		_isSelected = false;
	}

	/// <summary>
	/// リタイア処理
	/// </summary>
	public void OnClickRetireYesButton(){
		if (!_isSelected) {
			_isSelected = true;
			SoundManager.Instance.SEButtonPlay ();
			_retireCanvas.SetActive (false);
			// リタイア表示
			StartCoroutine (ShowRetireUI ());
			// リザルトデータの削除
			FindObjectOfType<ResultData> ().MyDestroy ();
			// ファイトモードの場合:通信終了処理
			if (_isFightMode) {
				FindObjectOfType<FightSocket> ().EmitEndnotification ();
				FindObjectOfType<FightSocket> ().EmitDisconnect ();
			}
		}
	}

	/// <summary>
	/// リタイアを表示後シーン遷移する
	/// </summary>
	IEnumerator ShowRetireUI(){
		// SE再生と表示
		SoundManager.Instance.SELossPlay ();
		_retireImage.SetActive (true);

		// RetireUIが上から降りてくるように表示する
		float remainTime  =  1.0f;
		Vector3 pos;
		while (remainTime >= 0.0f) {
			pos = _retireImage.transform.position;
			_retireImage.transform.position = new Vector3 (pos.x, pos.y - remainTime / 0.5f, pos.z);
			remainTime -= Time.deltaTime;
			yield return null;
		}
		// 0.8f後にセレクトシーンに遷移
		yield return new WaitForSeconds (0.8f);
		StageManager.Instance.OnClickStageButton ("Select");
	}
}