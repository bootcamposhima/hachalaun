﻿using UnityEngine;
using System.Collections;

public abstract class UIManager : MonoBehaviour {

	protected Vector3 _firstposbuttonpos;
	protected Vector3 _secoundbuttonpos;
	//目的地
	protected Vector3 _destination;
	//移動フラグ
	protected bool _ismove;
	//移動スピード
	protected float _time;


	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public abstract void EntranceAction();

	public abstract void LeavingAction ();

	public abstract void UIStart ();

	public abstract void UIEnd ();

	protected virtual bool UIMove (GameObject _ob,Vector3 destination,ref float spd){
		float x = _ob.gameObject.transform.localPosition.x - destination.x;

		float num;
		if (spd < 0)
			num = spd / 2 * -1;
		else
			num = spd / 2;
		
		if (x > -num && x < num) {
			_ob.gameObject.transform.localPosition = destination;

			if (spd < 0) 
				spd *= -1;
			
			return false;
		} else {
			Vector3 pos = _ob.gameObject.transform.localPosition;
			_ob.gameObject.transform.localPosition = new Vector3 (pos.x + spd, pos.y, pos.z);
			return true;
		}
	}

}
