﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DirectionManager : UIManager {

	//押された時の座表
	Vector3 _thirdbuttonpos;

	float _rotate;
	float _rotatespd;

	[SerializeField]
	GameObject _DirectionStageCanvas;

	[SerializeField]
	Sprite[] _DirectionImage;
	// Use this for initialization
	void Start () {
	}

	void Awake(){
		_firstposbuttonpos = this.transform.localPosition;
		_secoundbuttonpos = new Vector3 (59.0f, -33.0f, 0);
		_thirdbuttonpos = new Vector3 (403, -33, 0);
		_destination = Vector3.zero;
		_time = (_secoundbuttonpos.x - _firstposbuttonpos.x) / 20.0f;
	}
	
	// Update is called once per frame
	void Update () {

	}

	public bool DirectionButtonActive{
		set{
			this.transform.FindChild ("Direction").gameObject.GetComponent<Button> ().interactable = value;
		}
	}

	public void UIChange (){
		if (_destination == _thirdbuttonpos) {
			ActionManager.Instance.StartMoveAction (this.gameObject, _secoundbuttonpos, _time);
			_destination = _secoundbuttonpos;
		} else {
			ActionManager.Instance.StartMoveAction (this.gameObject, _thirdbuttonpos, _time);
			_destination = _thirdbuttonpos;
		}
	}

	public override void EntranceAction(){
	}

	public override void LeavingAction (){
		if (_destination == _thirdbuttonpos) {
			ActionManager.Instance.StartMoveAction (this.gameObject, _secoundbuttonpos, _time);
			_destination = _secoundbuttonpos;
		}
	}

	public override void UIStart (){
		ActionManager.Instance.StartMoveAction (this.gameObject, _secoundbuttonpos, _time);
		_destination = _secoundbuttonpos;
	}

	public override void UIEnd (){
		ActionManager.Instance.StartMoveAction (this.gameObject, _firstposbuttonpos, _time);
		_destination = _firstposbuttonpos;
	}

	public void ChangeButtonColor(int num){

		int length = 4;
		Color[] colors = new Color[length];

		for (int i = 0; i < length; i++) {
			if (i == num)
				colors [i] = Color.yellow;
			else
				colors [i] = Color.white;
		}

		this.transform.FindChild ("East").GetComponent<Button> ().interactable = true;
		this.transform.FindChild ("West").GetComponent<Button> ().interactable = true;
		this.transform.FindChild ("South").GetComponent<Button> ().interactable=true;
		this.transform.FindChild ("North").GetComponent<Button> ().interactable=true;

		switch (num) {
		case 0:
			this.transform.FindChild ("East").GetComponent<Button> ().interactable = false;
			break;

		case 1:
			this.transform.FindChild ("West").GetComponent<Button> ().interactable = false;
			break;
		case 2:
			this.transform.FindChild ("South").GetComponent<Button> ().interactable = false;
			break;
		case 3:
			this.transform.FindChild ("North").GetComponent<Button> ().interactable = false;
			break;
		}
		// ステージキャンパス側の方角表示変更
		_DirectionStageCanvas.GetComponent<Image> ().sprite = _DirectionImage[num];

		//this.transform.FindChild ("Direction").transform.FindChild ("Text").GetComponent<Text> ().text = str.ToString ();
		//Eastの色変更
		ColorBlock cb=this.transform.FindChild ("East").GetComponent<Button> ().colors;
		cb.disabledColor = colors [0];
		this.transform.FindChild ("East").GetComponent<Button> ().colors = cb;
		//Westの色変更
		cb=this.transform.FindChild ("West").GetComponent<Button> ().colors;
		cb.disabledColor = colors [1];
		this.transform.FindChild ("West").GetComponent<Button> ().colors = cb;
		//Southの色変更
		cb=this.transform.FindChild ("South").GetComponent<Button> ().colors;
		cb.disabledColor = colors [2];
		this.transform.FindChild ("South").GetComponent<Button> ().colors = cb;
		//Northの色変更
		cb=this.transform.FindChild ("North").GetComponent<Button> ().colors;
		cb.disabledColor = colors [3];
		this.transform.FindChild ("North").GetComponent<Button> ().colors = cb;
	}

	public void init(){
		ColorBlock cb=this.transform.FindChild ("East").GetComponent<Button> ().colors;
		cb.disabledColor = new Color(0.75f,0.3f,0.3f);
		this.transform.FindChild ("East").GetComponent<Button> ().colors = cb;
		//Westの色変更
		this.transform.FindChild ("West").GetComponent<Button> ().colors = cb;
		//Southの色変更
		this.transform.FindChild ("South").GetComponent<Button> ().colors = cb;
		//Northの色変更
		this.transform.FindChild ("North").GetComponent<Button> ().colors = cb;

		this.transform.FindChild ("East").GetComponent<Button> ().interactable = false;
		this.transform.FindChild ("West").GetComponent<Button> ().interactable = false;
		this.transform.FindChild ("South").GetComponent<Button> ().interactable=false;
		this.transform.FindChild ("North").GetComponent<Button> ().interactable=false;
	}

}
