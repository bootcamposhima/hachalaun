﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class PracticeManager : MonoBehaviour {

	//AIの情報
	List<AIDatas> _list;
	//UIMainCamera
	[SerializeField]
	Camera _uimaincamera;
	//ファイル名
	List<string> fs;
	//選ばれているAI番号
	int _top;

	//Application.persistentDataPath

	// Use this for initialization
	void Start () {
		this.transform.FindChild ("AIChoice").gameObject.SetActive (true);

		string _aifilepath;

		_list = new List<AIDatas> ();
		fs = new List<string> ();

		_aifilepath=Application.persistentDataPath;
		Debug.Log (_aifilepath);
		string[] str = System.IO.Directory.GetFiles (_aifilepath);
		if (str.Length < 1) {
			string strfilpath = Application.streamingAssetsPath + "/AI";
			string[] strs = System.IO.Directory.GetFiles (strfilpath);
			int num = 1;
			for (int i = 0; i < strs.Length; i++) {
				if (strs [i].IndexOf ("meta") < 1 && strs [i].IndexOf ("Store") < 1) {
					AI.FileWrite2 (_aifilepath + "/AI" + num.ToString () + ".text", AI.LoadAIData2 (strs [i]));
					fs.Add (_aifilepath +"/AI" + num.ToString () + ".text");
					_list.Add (AI.LoadAIData2 (strs [i]));
					num++;
				}
			}
		} else {
			for (int i = 0; i < str.Length; i++) {
				if (str [i].IndexOf ("meta") < 1 && str [i].IndexOf ("Store") < 1) {
					fs.Add (str [i]);
					_list.Add (AI.LoadAIData2 (str [i]));
				}
			}
		}

		if(this.transform.FindChild ("AIChoice").gameObject.activeSelf)
			this.transform.FindChild ("AIChoice").GetComponent<AIChoiceManager> ().setAIDatas (_list);

	}

	void Awake(){
		_top = 0;
	}

	// Update is called once per frame
	void Update () {
	
	}

	public void TopAIChoiceButton(int num){
		_top = num;
		this.transform.FindChild ("AIChoice").gameObject.SetActive (false);
		_uimaincamera.gameObject.SetActive (false);
		this.transform.FindChild ("AICreater").gameObject.SetActive (true);

		if (_top == 20) {
			_list.Add (AI.CreateAIData ());
			fs.Add (Application.persistentDataPath + "/AI" + (fs.Count + 1).ToString () + ".text");
			ChangeSave (_list [_list.Count-1]);
			this.transform.FindChild ("AICreater").GetComponent<AICreaterManager> ().Start (_list [_list.Count - 1]);
		} else {
			this.transform.FindChild ("AICreater").GetComponent<AICreaterManager> ().Start (_list [num]);
		}
	}

	public void FinishButton(){
		Invoke ("AIChoiceActive", 0.5f);
		_uimaincamera.gameObject.SetActive (true);
		this.transform.FindChild ("AICreater").GetComponent<AICreaterManager> ().End();
		this.transform.FindChild ("AIChoice").GetComponent<AIChoiceManager> ().setAIDatas (_list);
		this.transform.FindChild ("AIChoice").GetComponent<AIChoiceManager> ().initAIButton ();
	}

	public void AIChoiceActive(){
		this.transform.FindChild ("AIChoice").gameObject.SetActive (!this.transform.FindChild ("AIChoice").gameObject.activeSelf);
		this.transform.FindChild ("AICreater").gameObject.SetActive (false);
	}

	public void ChangeSave(AIDatas aidatas){
		if (_top != 20) {
			_list [_top] = aidatas;
			AI.FileWrite2 (fs [_top], aidatas);
		} else {
			print (fs [fs.Count-1]);
			_list [_list.Count-1]= aidatas;
			AI.FileWrite2 (fs[fs.Count-1], aidatas);
		}
	}
}
