﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class AIChoiceManager : MonoBehaviour {

	//スクロールビュー
	[SerializeField]
	GameObject _scroll;
	//スクロールビュー
	[SerializeField]
	GameObject _AIscroll;
	//決定ボタン
	[SerializeField]
	GameObject _okbutton;
	//戻るボタン
	[SerializeField]
	GameObject _backbutton;
	[SerializeField]
	GameObject _directionobject;

	[SerializeField]
	AIChoiceAction _aichoiceaction;

	//AIの情報
	List<AIDatas> _list;
	//ファクトリークラス
	Factory fc;
	//方角
	int _direction;
	//ノード
	[SerializeField]
	GameObject _node;
	//タップされている番号
	int _top;

	//

	// Use this for initialization
	void Start () {
		initAIButton ();
		_directionobject.GetComponent<DirectionManager> ().init ();
	}

	void Awake(){
		fc = Factory.Instance;
		_direction = 0;
		_top = -1;
		_okbutton.GetComponent<Button> ().interactable = false;	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public int Direction{
		get{
			return _direction;
		}
	}

	public void initAIButton(){
		foreach (Transform child in _AIscroll.transform)
		{
			child.gameObject.SetActive (true);
			child.transform.FindChild ("bullet").gameObject.SetActive (false);
			child.transform.FindChild ("position").gameObject.SetActive (false);
		}
		_directionobject.GetComponent<DirectionManager> ().init ();
		_aichoiceaction.ActionChoice (0);
		_top = -1;
	}

	public void setAIDatas(List<AIDatas> list){
		_list = list;
		int num = 1;
		int c = _list.Count;
		foreach (Transform child in _scroll.transform)
		{
			if (c > num - 1) {
				child.transform.FindChild ("Text").gameObject.GetComponent<Text> ().text = "AI" + num;
				for (int i = 0; i < 4; i++) {
					switch (_list [num - 1]._dataList [i]._direction) {
					case 0:
						child.transform.FindChild ("East").gameObject.GetComponent<Image> ().sprite = fc.GetCharactorSprite (_list [num - 1]._dataList [i]._charactornumber);
						break;
					case 1:
						child.transform.FindChild ("West").gameObject.GetComponent<Image> ().sprite = fc.GetCharactorSprite (_list [num - 1]._dataList [i]._charactornumber);
						break;
					case 2:
						child.transform.FindChild ("South").gameObject.GetComponent<Image> ().sprite = fc.GetCharactorSprite (_list [num - 1]._dataList [i]._charactornumber);
						break;
					case 3:
						child.transform.FindChild ("Noth").gameObject.GetComponent<Image> ().sprite = fc.GetCharactorSprite (_list [num - 1]._dataList [i]._charactornumber);
						break;

					default:
						break;
					}
				}

				num++;
				child.gameObject.SetActive (true);

			} else {
				if(child.name!="New AI")
					child.gameObject.SetActive (false);
			}
		}
	}

	public void TopAIButton(int num){
		_top = num;
		if (_top == -1)
			return;
		
		int i = 0;
		_directionobject.GetComponent<DirectionManager> ().ChangeButtonColor (_direction);
		for(int j=0; j<4; j++){
			if(_list [num]._dataList[j]._direction==_direction){
				int count = _list [num]._dataList[j]._bullet.Length;
				foreach (Transform child in _AIscroll.transform)
				{
					if (count > i) {
						child.transform.FindChild ("bullet").gameObject.SetActive (true);
						child.transform.FindChild ("position").gameObject.SetActive (true);
						child.transform.FindChild ("bullet").GetComponent<Image> ().sprite = fc.GetBulletSprite(_list [num]._dataList [j]._bullet[i]);
						child.transform.FindChild ("position").gameObject.GetComponent<Text> ().text = (_list [num]._dataList [j]._position [i] + 2).ToString ();
						child.gameObject.SetActive (true);
						i++;
					} else {
						child.gameObject.SetActive (false);
					}
				}
				_okbutton.GetComponent<Button> ().interactable = true;
			}
		}

		_aichoiceaction.ActionChoice (_direction+1);

	}

	public void TopOKButton(){
		//親オブジェクトを取得
		_node.GetComponent<PracticeManager>().TopAIChoiceButton(_top);
		_top = 0;
		_okbutton.GetComponent<Button> ().interactable = false;	
		_aichoiceaction.ActionStop ();
	}

	public void TopAddButton(){
		//親オブジェクトを取得
		_node.GetComponent<PracticeManager>().TopAIChoiceButton(20);
		_aichoiceaction.ActionStop ();
	}

	public void setDirection(int num){
		if (_top != -1) {
			_direction = num;
			TopAIButton (_top);
			_aichoiceaction.ActionChoice (_direction + 1);
		}
	}

}
