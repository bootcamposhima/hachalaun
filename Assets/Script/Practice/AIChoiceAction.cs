﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AIChoiceAction : MonoBehaviour {

	[SerializeField]
	GameObject _createAIbutton;
	[SerializeField]
	GameObject _editingbutton;
	[SerializeField]
	GameObject _east;
	[SerializeField]
	GameObject _weat;
	[SerializeField]
	GameObject _south;
	[SerializeField]
	GameObject _north;

	TransitionAnimation _ta;

	int _topnum;


	void Awake(){
		_topnum = 0;
	}

	// Use this for initialization
	void Start () {
		_ta = TransitionAnimation.Instance;
		_ta.ClearAllScaleAnimationList ();
		ActionChoice (_topnum);
		_ta.StartAllScaleAnimation (1.0f, 1.3f,0.01f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ActionChoice(int num){
		_ta.ClearAllScaleAnimationList ();
		switch(num){
		case 0:
			_ta.AddScaleAnimationGameObject (_createAIbutton);
			break;

		case 1:
			_ta.AddScaleAnimationGameObject  (_editingbutton);
			_ta.AddScaleAnimationGameObject  (_weat);
			_ta.AddScaleAnimationGameObject  (_south);
			_ta.AddScaleAnimationGameObject  (_north);
			break;

		case 2:
			_ta.AddScaleAnimationGameObject  (_editingbutton);
			_ta.AddScaleAnimationGameObject  (_east);
			_ta.AddScaleAnimationGameObject  (_south);
			_ta.AddScaleAnimationGameObject  (_north);
			break;

		case 3:
			_ta.AddScaleAnimationGameObject  (_editingbutton);
			_ta.AddScaleAnimationGameObject  (_east);
			_ta.AddScaleAnimationGameObject  (_weat);
			_ta.AddScaleAnimationGameObject  (_north);
			break;

		case 4:
			_ta.AddScaleAnimationGameObject  (_editingbutton);
			_ta.AddScaleAnimationGameObject  (_east);
			_ta.AddScaleAnimationGameObject  (_weat);
			_ta.AddScaleAnimationGameObject  (_south);
			break;
		}
		Defoult (num);
	}

	public void ActionStart(){
		Defoult (_topnum);
		_ta.StartAllScaleAnimation (1.0f, 1.3f,0.01f);
	}

	public void ActionStop (){
		_ta.StopAllScaleAnimations ();
	}

	public void Defoult(int num){
		if ((num == 0 && _topnum != 0) || (num != 0 && _topnum == 0)) {
			_ta.StopAllScaleAnimations ();
			_ta.StartAllScaleAnimation (1.0f, 1.3f,0.01f);
		}
		_topnum = num;
		Defoultpos ();
	}

	void Defoultpos(){
		_editingbutton.transform.localScale = Vector3.one;
		_createAIbutton.transform.localScale = Vector3.one;
		_east.transform.localScale = Vector3.one;
		_weat.transform.localScale = Vector3.one;
		_south.transform.localScale = Vector3.one;
		_north.transform.localScale = Vector3.one;
	}


}
