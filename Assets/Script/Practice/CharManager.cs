﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CharManager : UIManager {

	//ボタン
	[SerializeField]
	GameObject _button;
	//スクロールビュー
	[SerializeField]
	GameObject _scroll;
	[SerializeField]
	GameObject _content;
	//スクロールビューの初期座標
	Vector3 _scrollfirstpos;
	//スクロールビューの移動先の座標
	Vector3 _scrollsecpundpos;
	//スクロールビューの移動先の座標
	Vector3 _scrolldestination;

	float _scrollspd;

	bool _scrollismove;

	bool istop;

	// Use this for initialization
	void Start () {
	
	}

	void Awake(){
		_firstposbuttonpos = _button.transform.localPosition;
		_secoundbuttonpos = new Vector3 (-177.0f, 50.0f, 0);
		_scrollfirstpos = _scroll.transform.localPosition;
		_scrollsecpundpos = new Vector3 (26.0f, 29.0f, 0);
		_destination = Vector3.zero;
		_scrolldestination= Vector3.zero;
		_time = 20.0f;
		_scrollspd = 40.0f;
		_ismove = false;
		istop = false;
		_scrollismove = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (_ismove) 
			_ismove = UIMove (_button, _destination, ref _time);

		if (_scrollismove) {
			
			_scrollismove = UIMove (_scroll, _scrolldestination, ref _scrollspd);
		}
	}

	void setScrollMove(){
		_scrollismove = !_scrollismove;

	}

	void seTop(){
		istop = !istop;
	}


	public override void EntranceAction(){
		if (!_scrollismove&&!istop) {
			istop = true;
			_scrollspd *= -1;
			Invoke ("setScrollMove", 0.5f);
			Invoke ("seTop", 1.2f);
			_scrolldestination = _scrollsecpundpos;
			_button.GetComponent<Button> ().interactable = false;
		}
	}

	public override void LeavingAction (){
		if(!_scrollismove&&!istop){
			istop = true;
			Invoke ("seTop", 1.2f);
			_scrollismove = true;
			_scrolldestination = _scrollfirstpos;
			_button.GetComponent<Button> ().interactable = true;
		}
	}

	public void StopTop(){
		istop = true;
		Invoke ("seTop", 1.2f);
	}

//	public override void UIChange (){
//		_scrollismove = true;
//		if (_scrollismove == _scrollfirstpos) {
//			_scrolldestination = _scrollsecpundpos;
//		} else {
//			_scrollismove = _scrollfirstpos;
//		}
//	}

	public override void UIStart (){
		_ismove = true;
		_destination = _secoundbuttonpos;
		_scrollismove = false;
		_scrolldestination = _scrollfirstpos;
	}

	public override void UIEnd (){
		_time *= -1;
		_ismove = true;
		_destination = _firstposbuttonpos;
		_scrollismove = true;
		_scrolldestination = _scrollfirstpos;
		_button.GetComponent<Button> ().interactable = true;
	}

}
