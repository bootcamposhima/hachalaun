﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AIManager : UIManager {

	//ボタン
	[SerializeField]
	GameObject _button;
	//スクロールビュー
	[SerializeField]
	GameObject _scroll;
	[SerializeField]
	GameObject _scrollbullet;
	//スクロールビューの初期座標
	Vector3 _scrollfirstpos;
	//スクロールビューの移動先の座標
	Vector3 _scrollsecpundpos;
	//スクロールビューの移動先の座標
	Vector3 _scrolldestination;

	GameObject _moveobjrct;

	float _scrollspd;

	bool _scrollismove;
	//is Top
	bool istop;
	//is Reload
	bool isre;
	//is move scrollbukket
	bool ismsb;

	// Use this for initialization
	void Start () {
	
	}

	void Awake(){
		_firstposbuttonpos = _button.transform.localPosition;
		_secoundbuttonpos = new Vector3 (-177.0f, -96.0f, 0);
		_scrollfirstpos = _scroll.transform.localPosition;
		_scrollsecpundpos = new Vector3 (26.0f, 29.0f, 0);
		_destination = Vector3.zero;
		_scrolldestination= Vector3.zero;
		_moveobjrct = null;
		_time = 20.0f;
		_scrollspd = 40.0f;
		_ismove = false;
		istop = false;
		isre = false;
		ismsb = false;
		_scrollismove = false;
	}

	// Update is called once per frame
	void Update () {
		if (_ismove) {
			_ismove = UIMove (_button, _destination,ref _time);
		}
		if (_scrollismove) {
			_scrollismove = UIMove (_moveobjrct, _scrolldestination,ref _scrollspd);
		}
	}

	void setScrollMove(){
		_scrollismove = !_scrollismove;
	}


	void seTop(){
		istop = !istop;
	}

//	public override void ChangeButton(){
//		_scrolldestination = false;
//		_scrollismove = _scrollfirstpos;
//	}

	public override void EntranceAction(){
		if (!_scrollismove&&!istop||isre) {
			if (!isre) {
				Invoke ("setScrollMove", 0.5f);
				Invoke ("seTop", 1.2f);
			} else _scrollismove = true;
			_moveobjrct = _scroll;
			istop = true;
			_scrollspd *= -1;
			_scrolldestination = _scrollsecpundpos;
			isre = false;
			_button.GetComponent<Button> ().interactable = false;
		}
	}

	public void BulletEntranceAction(){
		if (!_scrollismove&&!istop||isre) {
			_scrollismove = true;
			_moveobjrct = _scrollbullet;
			istop = true;
			_scrollspd *= -1;
			_scrolldestination = _scrollsecpundpos;
			isre = false;
		}
	}

	public override void LeavingAction (){
		if (!_scrollismove&&!istop||isre) {
			if (!isre)
				Invoke ("seTop", 1.2f);
			_moveobjrct = _scroll;
			istop = true;
			_scrollismove = true;
			_scrolldestination = _scrollfirstpos;
			_button.GetComponent<Button> ().interactable = true;
		}
	}

	public void BulletLeavingAction (){
		if (!_scrollismove&&!istop||isre) {
			if(!isre)
				Invoke ("seTop", 1.2f);
			_moveobjrct = _scrollbullet;
			istop = true;
			_scrollismove = true;
			_scrolldestination = _scrollfirstpos;
		}
	}

	public void ChangeBulletScroll(int num){
		BulletLeavingAction ();
		isre = true;
		if (num == 0)
			Invoke ("EntranceAction", 0.5f);
		else
			Invoke ("LeavingAction", 0.5f);
		Invoke ("seTop", 1.2f);
	}

	public void ReGoBack(){
		if (!_scrollismove&&!istop) {
			_moveobjrct = _scroll;
			isre = true;
			LeavingAction ();
			Invoke ("EntranceAction", 0.5f);
			Invoke ("seTop", 1.2f);
		}
	}

	public void ChangeBulletScroll(){
		isre = true;
		if (ismsb) {
			BulletLeavingAction ();
			Invoke ("EntranceAction", 0.5f);
		} else {
			LeavingAction ();
			Invoke ("BulletEntranceAction", 0.5f);
		}
		Invoke ("seTop", 1.2f);
		ismsb = !ismsb;
	}
		
	public override void UIStart (){
		_moveobjrct = _scroll;
		_ismove = true;
		//ActionManager.Instance.StartMoveAction(_button,_secoundbuttonpos,(_secoundbuttonpos.x-_firstposbuttonpos.x)/20.0f);
		_destination = _secoundbuttonpos;
		_scrollspd *= -1;
		_scrollismove = true;
		//ActionManager.Instance.StartMoveAction(_moveobjrct,_scrollsecpundpos,(_secoundbuttonpos.x-_firstposbuttonpos.x)/20.0f);
		_scrolldestination = _scrollsecpundpos;
		_button.GetComponent<Button> ().interactable = false;
	}

	public override void UIEnd (){
		_moveobjrct = _scroll;
		_time *= -1;
		_ismove = true;
		_destination = _firstposbuttonpos;
		_scrollismove = true;
		_scrolldestination = _scrollfirstpos;
	}

}