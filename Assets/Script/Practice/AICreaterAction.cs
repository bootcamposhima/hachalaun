﻿using UnityEngine;
using System.Collections;

public class AICreaterAction : MonoBehaviour {

	//編集、保存ボタン
	[SerializeField]
	GameObject _formationButton;
	[SerializeField]
	GameObject _arrows;
	//アクションを行うScript
	TransitionAnimation _ta;
	//アクションを行うScript
	ActionManager _am;

	// Use this for initialization
	void Start () {
		_ta = TransitionAnimation.Instance;
		_am = ActionManager.Instance;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ActionChoice(int num){
		_ta.ClearAllScaleAnimationList ();
		switch(num){
		case 0:
			_ta.StopAllScaleAnimations ();
			_am.StopMoveForeverAction ();
			Defoultpos();
			break;

		case 1:
			_ta.AddScaleAnimationGameObject (_formationButton);
			if(!_ta.IsAllScaleAnimationExecuting())
				ActionStart ();
			break;

		case 2:
			Vector3 pos = _arrows.transform.localPosition;
			pos.y = 1.7f;
			_am.StartMoveForeverAction (_arrows, pos, 15.0f, 0.2f);
			break;

		case 3:
			
			break;

		case 4:
			
			break;
		}
	}

	public void ActionStart(){
		_ta.StartAllScaleAnimation (1.0f, 1.3f,0.01f);
	}

	public void ActionStop (){
		_ta.StopAllScaleAnimations ();
	}

	void Defoultpos(){
		_formationButton.transform.localScale = Vector3.one;
		Vector3 pos = _arrows.transform.localPosition;
		pos.y = 3.5f;
		_arrows.transform.localPosition = pos;
	}

}
