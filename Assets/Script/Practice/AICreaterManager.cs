﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AICreaterManager : MonoBehaviour {

	//作成するAIDatas
	AIDatas _aidatas;
	//AIのスクロールビュー
	[SerializeField]
	GameObject _aiscroll;
	//弾のスクロールビュー
	[SerializeField]
	GameObject _bulletscroll;
	//キャラクターのスクロールビュー
	[SerializeField]
	GameObject _charactorscroll;
	//編集・保存ボタン
	[SerializeField]
	GameObject _addbutton;
	//弾の発射位置を決めるための地面
	[SerializeField]
	GameObject _panels;
	//パネルが押せるようになったことを知らせる矢印
	[SerializeField]
	GameObject _arrows;
	//ゲームのメインカメラ
	[SerializeField]
	Camera _maincamera;
	//方角のオブジェクト
	[SerializeField]
	GameObject _directionobject;
	//ノード
	[SerializeField]
	GameObject _node;
	[SerializeField]
	AICreaterAction _aicreateaction;

	//作成する方角
	int _direction;
	//キャラクターナンバー
	int _charactornum;
	//ファクトリークラス
	Factory _fc;
	//タップされているAI番号
	public int _topainum;
	//タップされているbullet番号
	public int _topbulletnum;
	//タップされているposition番号
	public int _toppositionnum;
	//編成されているかどうかのフラグ
	bool _formation;
	//
	int _count;
	bool _istop;

	//Enemyのオブジェクト
	GameObject _north;
	GameObject _south;
	GameObject _east;
	GameObject _west;


	// Use this for initialization
	void Start () {
		_fc = Factory.Instance;
		_addbutton.GetComponent<Button> ().interactable = false;
		_formation = false;
	}

	void Awake(){
		_direction = 0;
		_charactornum = -1;
		_topainum = -1;
		_toppositionnum = 0;
		_toppositionnum = 0;
		_count = 0;
	}

	public void Start(AIDatas aidatas){
		SetAidatas (aidatas);

		this.transform.FindChild ("Direction").GetComponent<DirectionManager> ().UIStart();
		this.transform.FindChild ("Char").GetComponent<CharManager> ().UIStart ();
		this.transform.FindChild ("AI").GetComponent<AIManager> ().UIStart ();
		_charactornum = -1;
		_topainum = -1;
		_panels.SetActive (true);
		for (int i = 0; i < _count; i++) {
			if (_aidatas._dataList [i]._direction == _direction) {
				TidyScrollView (_aidatas._dataList [i]._charactornumber, _charactorscroll);
				_charactornum = _aidatas._dataList [i]._charactornumber;
				break;
			}
		}
	}

	public void End(){
		this.transform.FindChild ("Direction").GetComponent<DirectionManager> ().UIEnd();
		this.transform.FindChild ("Char").GetComponent<CharManager> ().UIEnd ();
		this.transform.FindChild ("AI").GetComponent<AIManager> ().UIEnd ();
		setButtonAction (false);
		_panels.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	//bullet追加ボタンを押したら
	public void AddButton(){
		TopPositionNum (0);
		_formation = true;
		this.transform.FindChild ("AI").GetComponent<AIManager> ().ChangeBulletScroll ();
		_addbutton.transform.FindChild ("Text").GetComponent<Text> ().text = "保存";
	}

	//配列の連結関数
	int[] ArrayLinking(int[] origin,int link){
		int[] memo = new int[origin.Length];
		int[] l = new int[]{ link };
		origin.CopyTo (memo, 0);
		int[] modori = new int[memo.Length + 1];
		memo.CopyTo (modori, 0);
		l.CopyTo (modori, memo.Length);
		return modori;
	}

	public void ChangeCharNum(int num){
		for (int i = 0; i < _count; i++) {
			if (_aidatas._dataList [i]._direction == _direction) {
				_charactornum = num;
				AI.Write (num, _direction, _aidatas);
				_node.GetComponent<PracticeManager> ().ChangeSave (_aidatas);
				CreateCharactor ();
				ChangeCharactorPosition ();
				TidyScrollView (num, _charactorscroll);
				break;
			}
		}
	}
		
	//キャラクターの座標移動
	private void ChangeCharactorPosition(){
		//移動量
		float movenum = 4;
		//移動しない時の座標
		float pos = 11;
		//補正
		float hosei=7;

		//現在編集している方角とそのAIdataが記憶している方角が一致したら
		switch (_direction) {
		case 0:
			_east.transform.localPosition = new Vector3 (-pos, 2, movenum * _toppositionnum-hosei);
			break;
		case 1:
			_west.transform.localPosition = new Vector3 (pos, 2, movenum * _toppositionnum-hosei);
			break;
		case 2:
			_south.transform.localPosition = new Vector3 (movenum * _toppositionnum, 2, -pos-hosei);
			break;
		case 3:
			_north.transform.localPosition = new Vector3 (movenum * _toppositionnum, 2, pos-hosei);
			break;
		}
	}

	void CreateCharactor(){
		GameObject _object = _fc.CreateCharactor (_charactornum);
		switch (_direction) {
		case 0:
			if (_east)
				Destroy (_east);
			_east = Instantiate(_object);
			break;
		case 1:
			if (_west)
				Destroy (_west);
			_west = Instantiate (_object);
			break;
		case 2:
			if (_south)
				Destroy (_south);
			_south = Instantiate (_object);
			break;
		case 3:
			if (_north)
				Destroy (_north);
			_north = Instantiate (_object);
			break;
		}
	}

	//変更を保存する
	void ChangeSave(){
		for (int i = 0; i < _count; i++) {
			if (_aidatas._dataList [i]._direction == _direction) {
				if (_topainum == 20) {
					_topainum = _aidatas._dataList [i]._bullet.Length;
					int[] bullet = ArrayLinking (_aidatas._dataList [i]._bullet, _topbulletnum);
					int[] position = ArrayLinking (_aidatas._dataList [i]._position, _toppositionnum);
					WriteAI (bullet, position);
				} else {
					_aidatas._dataList [i]._bullet [_topainum] = _topbulletnum;
					_aidatas._dataList [i]._position [_topainum] = _toppositionnum;
				}
				break;
			}
		}

		_node.GetComponent<PracticeManager> ().ChangeSave (_aidatas);
		LoadAiData ();
	}

	public void FinishButton(){
		if (!_istop) {
			_istop = true;
			Invoke ("setTop", 1.2f);
			_aicreateaction.ActionStop ();
			if (_formation) {
				this.transform.FindChild ("Char").GetComponent<CharManager> ().StopTop ();
				this.transform.FindChild ("AI").GetComponent<AIManager> ().ChangeBulletScroll ();
				_addbutton.transform.FindChild ("Text").GetComponent<Text> ().text = "編集";
				_directionobject.GetComponent<DirectionManager> ().DirectionButtonActive = true;
				_formation = false;
				setArrowConfiguration (false);
				_addbutton.GetComponent<Button> ().interactable = true;
				ChangeCharactorPosition ();

				for (int i = 0; i < _count; i++) {
					if (_aidatas._dataList [i]._direction == _direction) {
						_toppositionnum = _aidatas._dataList [i]._position [_topainum];
						ChangeCharactorPosition ();
						break;
					}
				}

			} else {
				_node.GetComponent<PracticeManager> ().FinishButton ();
			}
		}
	}

	//キャラクターの座標移動
	private void InitCharactorPosition(){
		//移動量
		float movenum = 4;
		//移動しない時の座標
		float pos = 11;
		//補正
		float hosei=7;

		for(int i =0; i<_count; i++){
			switch (_aidatas._dataList[i]._direction) {
			case 0:
				_east.transform.localPosition = new Vector3 (-pos, 2, movenum * _toppositionnum-hosei);
				break;
			case 1:
				_west.transform.localPosition = new Vector3 (pos, 2, movenum * _toppositionnum-hosei);
				break;
			case 2:
				_south.transform.localPosition = new Vector3 (movenum * _toppositionnum, 2, -pos-hosei);
				break;
			case 3:
				_north.transform.localPosition = new Vector3 (movenum * _toppositionnum, 2, pos-hosei);
				break;
			}
		}
	}


	void initCreateCharactor(){
		for (int i = 0; i < _count; i++) {
			GameObject _object = _fc.CreateCharactor (_aidatas._dataList [i]._charactornumber);
			switch (_aidatas._dataList [i]._direction) {
			case 0:
				if (_east)
					Destroy (_east);
				_east = Instantiate(_object);
				break;
			case 1:
				if (_west)
					Destroy (_west);
				_west = Instantiate (_object);
				break;
			case 2:
				if (_south)
					Destroy (_south);
				_south = Instantiate (_object);
				break;
			case 3:
				if (_north)
					Destroy (_north);
				_north = Instantiate (_object);
				break;
			}
		}
	}

	void LoadAiData(){
		//foreachで回した回数記憶する変数
		int i = 0;
		for (int j = 0; j < 4; j++) {
			if (_aidatas._dataList [j]._direction == _direction) {
				//バレット類の長さ
				int bulletcount = _aidatas._dataList[j]._bullet.Length;
				//foreach
				foreach (Transform child in _aiscroll.transform)
				{
					if(bulletcount>i){
						child.transform.FindChild ("bullet").GetComponent<Image> ().sprite = _fc.GetBulletSprite(_aidatas._dataList [j]._bullet[i]);
						child.transform.FindChild ("position").GetComponent<Text> ().text = (_aidatas._dataList [j]._position [i] + 2).ToString();
						child.gameObject.SetActive (true);
					}else{
						if (child.gameObject.name != "AddBullet" || i <= 19)
							child.gameObject.SetActive (false);
					}
					i++;
				} 
			}
		}

	}

	//AIのデータをセットする
	public void SetAidatas(AIDatas aidatas){
		//もしファクトリークラスを保持していなかったら
		if (!_fc) 
			_fc = Factory.Instance;

		_count = aidatas._dataList.Count;

		//AIデータを記憶
		_aidatas = aidatas;

		_directionobject.GetComponent<DirectionManager> ().ChangeButtonColor (_direction);

		LoadAiData ();
		SetCameraPosition ();
		initCreateCharactor ();
		InitCharactorPosition ();
	}

	//Arrowの表示非表示とアクション
	void setArrowConfiguration(bool flag){
		_aicreateaction.ActionChoice ((flag == true) ? 2 : 0);
		_arrows.SetActive (flag);
	}

	//Arrowの表示非表示とアクション
	void setArrowPosition(){
		int j = 0;
		Transform[] _gob = new Transform[4];
		foreach(Transform child in _arrows.transform){
			_gob [j] = child;
			j++;
		}
		j = 0;
		for (int i = 0; i < 5; i++) {
			if (i != _toppositionnum+2) {
				Vector3 pos = _gob [j].localPosition;
				float p = -8.0f + i * 4;
				pos.x = p;
				_gob [j].localPosition = pos;
				j++;
			}
		}
	}

	void setButtonAction(bool flag){
		_addbutton.GetComponent<Button> ().interactable = flag;
		_aicreateaction.ActionChoice ((flag == true) ? 1 : 0);
	}
		
	//カメラの座標移動
	public void SetCameraPosition(){
		float pos = 35;
		float pos2 = 0;
		float y = 12;
		float py = 1;
		float rotation = 90.0f;
		float _dppos = -7;
		float _mpos = 11;

		if (_direction <= 1) {
			_maincamera.transform.localPosition = new Vector3 ((_direction == 0) ? -28 : 28, y, -7);
			_maincamera.transform.eulerAngles = new Vector3 (20, (_direction == 0) ? rotation : -rotation, 0);
			_panels.transform.transform.localPosition = new Vector3 ((_direction == 0) ? -_mpos : _mpos, py, _dppos);
			_panels.transform.eulerAngles = new Vector3 (0, -rotation, 0);
			_arrows.transform.eulerAngles = new Vector3 (0, -rotation, 0);
		}else{
			_maincamera.transform.localPosition = new Vector3 (pos2, y, (_direction == 2) ? -pos : 22);
			_maincamera.transform.eulerAngles = new Vector3 (20, (_direction == 2) ? 0 : rotation * 2, 0);
			Vector3 p = new Vector3 (0, py, (_direction == 2) ? _dppos - _mpos : _dppos + _mpos);
			_panels.transform.localPosition = p;
			_panels.transform.eulerAngles = Vector3.zero;
			_arrows.transform.eulerAngles = Vector3.zero;
		}

	}

	//方角変更ボタンを押したら
	public void setDirection(int num){
		if (!_istop&&!_formation) {
			_charactornum = -1;
			_topainum = -1;
			TidyScrollView(_topainum,_aiscroll);
			setButtonAction (false);
			_directionobject.GetComponent<DirectionManager> ().ChangeButtonColor (num);
			_istop = true;
			Invoke ("setTop", 1.2f);
			_direction = num;
			InitCharactorPosition ();
			SetCameraPosition ();
			LoadAiData ();
			this.transform.FindChild ("Direction").GetComponent<DirectionManager> ().LeavingAction ();
			this.transform.FindChild ("Char").GetComponent<CharManager> ().LeavingAction ();
			this.transform.FindChild ("AI").GetComponent<AIManager> ().ReGoBack ();

			for(int i=0; i<_aidatas._dataList.Count; i++){
				if (_aidatas._dataList [i]._direction == num) {
					TidyScrollView (_aidatas._dataList [i]._charactornumber, _charactorscroll);
					break;
				}
			}
		}
	}

	void setTop(){
		_istop = !_istop;
	}

	//AIのボタンを押したら
	public void TopAIButton(){
		if (!_istop&&!_formation) {
			_istop = true;
			Invoke ("setTop", 1.2f);
			this.transform.FindChild ("Direction").GetComponent<DirectionManager> ().LeavingAction ();
			this.transform.FindChild ("Char").GetComponent<CharManager> ().LeavingAction ();
			setButtonAction ((_topainum == -1) ? false : true);
			if (_formation)
				this.transform.FindChild ("AI").GetComponent<AIManager> ().ChangeBulletScroll (0);
			else
				this.transform.FindChild ("AI").GetComponent<AIManager> ().EntranceAction ();
		}
	}


	//AIボタンがタップされたら
	public void TopAIButton(int num){
		_topainum = num;
		if (_topainum != 20) {
			for (int i = 0; i < _count; i++) {
				if (_aidatas._dataList [i]._direction == _direction) {
					_toppositionnum = _aidatas._dataList [i]._position [_topainum];
					ChangeCharactorPosition ();
				}
				TidyScrollView (num, _aiscroll);
			}
		}
		setButtonAction (true);
		this.transform.FindChild ("Direction").GetComponent<DirectionManager> ().LeavingAction ();
	}

	//bulletのボタンを押したら
	public void TopBulletNum(int num){
		_topbulletnum = num;
		TidyScrollView (num, _bulletscroll.transform.FindChild("Viewport").FindChild("Content").gameObject);
		setButtonAction (true);
	}

	//キャラのボタンを押したら
	public void TopCharaButton(){
		if (!_istop&&!_formation) {
			_istop = true;
			Invoke ("setTop", 1.2f);
			this.transform.FindChild ("Direction").GetComponent<DirectionManager> ().LeavingAction ();
			this.transform.FindChild ("Char").GetComponent<CharManager> ().EntranceAction ();
			setButtonAction (false);
			if (_formation)
				this.transform.FindChild ("AI").GetComponent<AIManager> ().ChangeBulletScroll (1);
			else
				this.transform.FindChild ("AI").GetComponent<AIManager> ().LeavingAction ();
		}
	}

	//編集ボタンを押したら
	public void TopFormationButton(){
		if (!_istop) {
			_istop = true;
			Invoke ("setTop", 1.2f);
			this.transform.FindChild ("AI").GetComponent<AIManager> ().ChangeBulletScroll ();
			if (_formation) {
				_formation = false;
				_addbutton.transform.FindChild ("Text").GetComponent<Text> ().text = "編集";
				ChangeSave ();
				_directionobject.GetComponent<DirectionManager> ().DirectionButtonActive = true;
				_addbutton.GetComponent<Button> ().interactable = true;
				setArrowConfiguration (false);
			} else {
				_formation = true;
				_addbutton.transform.FindChild ("Text").GetComponent<Text> ().text = "保存";
				_directionobject.GetComponent<DirectionManager> ().DirectionButtonActive = false;
				this.transform.FindChild ("Direction").GetComponent<DirectionManager> ().LeavingAction ();
				_addbutton.GetComponent<Button> ().interactable = false;
				setArrowConfiguration (true);
				setArrowPosition ();
				for (int i = 0; i < _count; i++) {
					if (_aidatas._dataList [i]._direction == _direction) {
						TidyScrollView (_aidatas._dataList [i]._bullet[_topainum], _bulletscroll.transform.FindChild ("Viewport").FindChild ("Content").gameObject);
						break;
					}
				}
			}
		}
	}

	//座標のボタンを押したた
	public void TopPositionNum(int num){
		if (!_formation)
			return;

		_toppositionnum = num;

		ChangeCharactorPosition ();

		setArrowPosition ();

		setButtonAction (true);
	}

	//num:タップ番号
	void TidyScrollView(int num,GameObject sc){
		int j = 0;

		//foreach
		foreach (Transform child in sc.transform) {
			if (j != num) {
				child.GetComponent<Button> ().interactable = true;
			} else {
				ColorBlock cb = child.GetComponent<Button> ().colors;
				cb.disabledColor = Color.yellow;
				child.GetComponent<Button> ().colors = cb;
				child.GetComponent<Button> ().interactable = false;
			}
			j++;
		} 
	}

	void WriteAI(int[] bullet,int[] position){
		for (int i = 0; i < _count; i++) {
			if (_aidatas._dataList [i]._direction == _direction) {
				_aidatas = AI.Write (_charactornum, _direction, bullet, position,_aidatas);
			}
		}
	}

}
