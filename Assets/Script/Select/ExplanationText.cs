﻿using UnityEngine;
using System;
using UnityEngine.UI;
using System.Collections;

public class ExplanationText : MonoBehaviour {
	[SerializeField]
	GameObject _goButton;
	[SerializeField]
	GameObject _modeImageUI;
	Mode       _mode;

	enum Mode{NORMAL,FIGHT,EDIT}

	void Start () {
		this.GetComponent<Text> ().text = "好きなモードを選んでね";
		_mode = Mode.NORMAL;
	}

	/// <summary>
	/// ノーマルモード選択
	/// </summary>
	public void OnClickNormalMode(){
		selectedMode (() => {
			this.GetComponent<Text> ().text = "ＡＩと戦うモードです";
			_mode = Mode.NORMAL;
		});
	}

	/// <summary>
	/// 対戦モード選択
	/// </summary>
	public void OnClickFightMode(){
		selectedMode (() => {
			this.GetComponent<Text> ().text = "他プレイヤーと戦うモードです";
			_mode = Mode.FIGHT;
		});
	}

	/// <summary>
	/// 編集モード選択
	/// </summary>
	public void OnClickEditMode(){
		selectedMode (() => {
			this.GetComponent<Text> ().text = "行動パターンを作成・編集＆確認ができます";
			_mode = Mode.EDIT;
		});
	}

	/// <summary>
	/// モード選択時
	/// </summary>
	void selectedMode(Action modeProcess){
		SoundManager.Instance.SEButtonPlay ();
		StartAnimationGoButton ();
		modeProcess();
		_modeImageUI.GetComponent<SCImage> ().ChangeSelectmode ((int)_mode);
	}

	/// <summary>
	/// スタートボタンのアニメーション
	/// </summary>
	void StartAnimationGoButton(){
		TransitionAnimation.Instance.StopAllScaleAnimations ();
		TransitionAnimation.Instance.ClearAllScaleAnimationList ();
		TransitionAnimation.Instance.StartScaleAnimation (_goButton, 1.2f);
	}

	/// <summary>
	/// 次のシーンに遷移する
	/// </summary>
	public void OnClickGoButton(){
		SoundManager.Instance.SEButtonPlay ();
		TransitionAnimation.Instance.StopScaleAnimation (_goButton);
		string nextScene = "";
		switch (_mode) {
		case Mode.NORMAL:
			nextScene = "yk_test";
			break;
		case Mode.FIGHT:
			nextScene = "Matching";
			break;
		case Mode.EDIT:
			nextScene = "Practice";
			break;
		}
		FindObjectOfType<StageManager> ().OnClickStageButton (nextScene);
	}
}