﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class PlaySelect : MonoBehaviour {

	[SerializeField]
	GameObject _SelectUICamera;
	[SerializeField]
	GameObject _MainCamera;
	[SerializeField]
	GameObject _UICamera;

	[SerializeField]
	GameObject _EnemyManager;
	[SerializeField]
	GameObject _DefenceManager;

	[SerializeField]
	GameObject _round;

	[SerializeField]
	GameObject _StartText;
	[SerializeField]
	GameObject _SkipSpeed;

	[SerializeField]
	GameObject _NotSelectImage;

	[SerializeField]
	GameObject _AIScroll;

	[SerializeField]
	GameObject _Go;

	[SerializeField]
	GameObject waku1;
	[SerializeField]
	GameObject waku2;

	GameObject _demoobject;


	//AIdaras
	List<AIDatas> list;
	//タップされているAI
	int _ai;
	int _pl;

	bool _isAttack;

	bool _isFight;

	bool _isStartOK;

	bool _isStart;

	bool _isChangeGameStart;
	bool _isChangeShow;

	float rectX_s;
	float rectX_m;

	float s_rectX_m;
	float s_rectX_s;
	float s_view_m;

	bool _isFarst;

	IEnumerator ie;

	// Use this for initialization
	void Start () {
		_ai = -1;
		_pl = -1;
		_isAttack = false;
		_isStartOK = false;
		_isStart = false;
		_isFarst = true;
		rectX_s = 0.0f;
		rectX_m = 0.5f;
		s_rectX_m = 0.5f / 60.0f;
		s_rectX_s = -1.0f / 60.0f;
		s_view_m = 20.0f / 60.0f;
		_isChangeGameStart = false;
		_isChangeShow = false;
		_isAttack = false;
		ShowAIDatas ();
		ie = null;
		_isFight = (SceneManager.GetActiveScene ().name == "FightRound") ? true : false;
	}
	
	// Update is called once per frame
	void Update () {
		if (_isChangeGameStart) {
			// 0.5f ~ 0
			_MainCamera.GetComponent<Camera> ().rect = new Rect (_MainCamera.GetComponent<Camera> ().rect.x - s_rectX_m, 0, 1, 1);
			// 80 ~ 60
			_MainCamera.GetComponent<Camera> ().fieldOfView -= s_view_m;
			// 0 ~ 1
			_SelectUICamera.GetComponent<Camera> ().rect = new Rect (_SelectUICamera.GetComponent<Camera> ().rect.x + s_rectX_s, 0, 1, 1);
			if (_MainCamera.GetComponent<Camera> ().rect.x <= 0.0f) {
				GameStart ();
			}
		}
		if (_isChangeShow) {
			// 0.5f ~ 0
			_MainCamera.GetComponent<Camera> ().rect = new Rect (_MainCamera.GetComponent<Camera> ().rect.x + s_rectX_m, 0, 1, 1);
			// 80 ~ 60
			_MainCamera.GetComponent<Camera> ().fieldOfView += s_view_m;
			// 0 ~ 1
			_SelectUICamera.GetComponent<Camera> ().rect = new Rect (_SelectUICamera.GetComponent<Camera> ().rect.x - s_rectX_s, 0, 1, 1);
			if (_MainCamera.GetComponent<Camera> ().rect.x >= 0.5f) {
				_isChangeShow = false;
				_MainCamera.GetComponent<Camera> ().rect = new Rect (rectX_m, 0, 1, 1);
				_SelectUICamera.GetComponent<Camera> ().rect = new Rect (rectX_s, 0, 1, 1);
				if (_isAttack) {
					TransitionAnimation.Instance.StartScaleAnimation (waku1, 1.1f,1,0.005f);
				} else {
					TransitionAnimation.Instance.StartScaleAnimation (waku2, 1.1f,1,0.005f);
				}
			}
		}
			
	}

	public bool Attack{
		set{
			_isAttack = value;
		}
	}

	public int CharaNum{
		get{
			return _pl;
		}
	}

	public void Show(bool isAttack){
		_ai = -1;
		_pl = -1;
		_isChangeShow = true;
		this.gameObject.SetActive (true);
		_SelectUICamera.SetActive (true);
		_UICamera.SetActive (false);
		_isStart = false;
		_Go.SetActive (true);
		if (isAttack) {
			_NotSelectImage.GetComponent<Image> ().rectTransform.anchorMax = new Vector2 (0.5f, 0.5f);
			_NotSelectImage.GetComponent<Image> ().rectTransform.anchorMin = new Vector2 (0.0f, 0.0f);
			waku1.SetActive (true);
		} else {
			_NotSelectImage.GetComponent<Image> ().rectTransform.anchorMax = new Vector2 (0.5f, 1.0f);
			_NotSelectImage.GetComponent<Image> ().rectTransform.anchorMin = new Vector2 (0.0f, 0.5f);
			waku2.SetActive (true);
		}
		_isAttack = isAttack;
		_isFarst = true;
	}

	void ShowAIDatas(){
		//AIフォルダーのパス
		//AiDatasを格納する変数
		list = AI.LoadAllAidatas();

		int num = 0;
		int count = list.Count;
		Factory fc = Factory.Instance;

		//表示
		foreach (Transform child in _AIScroll.transform) {
			if (num < count) {
				child.transform.FindChild ("Text").gameObject.GetComponent<Text> ().text = "AI" + (num + 1);

				for (int i = 0; i < 4; i++) {
					switch (list [num]._dataList [i]._direction) {
					case 0:
						child.transform.FindChild ("Noth").gameObject.GetComponent<Image> ().sprite = fc.GetCharactorSprite (list [num]._dataList [i]._charactornumber);
						break;
					case 1:
						child.transform.FindChild ("South").gameObject.GetComponent<Image> ().sprite = fc.GetCharactorSprite (list [num]._dataList [i]._charactornumber);
						break;
					case 2:
						child.transform.FindChild ("West").gameObject.GetComponent<Image> ().sprite = fc.GetCharactorSprite (list [num]._dataList [i]._charactornumber);
						break;
					case 3:
						child.transform.FindChild ("East").gameObject.GetComponent<Image> ().sprite = fc.GetCharactorSprite (list [num]._dataList [i]._charactornumber);
						break;

					default:
						break;
					}
				}
				num++;
			} else {
				child.gameObject.SetActive (false);
			}
		}
	}

	public void SelectAI(int num){
		if ((_isAttack & !_isStart)&&(_ai!=num)) {
			if (_isStartOK)
				_EnemyManager.GetComponent<EnemyManager> ().DestroyEnemies ();
			_isStartOK = true;
			_EnemyManager.GetComponent<EnemyManager> ().CreateEnemies (list[num],true);
			_ai = num;
			TransitionAnimation.Instance.StartScaleAnimation (_Go, 1.2f);
			SoundManager.Instance.SEButtonPlay ();
		}
	}

	public void CreateEnemy(AIDatas ai){
		_EnemyManager.GetComponent<EnemyManager> ().CreateEnemies (ai,false);
	}


	public void SelectCharactor(int num){
		if ((!_isAttack & !_isStart)&&(_pl!=num)) {
			_isStartOK = true;
			_demoobject = _DefenceManager.GetComponent<DefenceManager> ().CreatDefenceCharctor (num, false);
			_demoobject.AddComponent<DemoPlay> ();
			_demoobject.GetComponent<DemoPlay> ().setCharData (num);
			_pl = num;
			TransitionAnimation.Instance.StartScaleAnimation (_Go, 1.2f);
			SoundManager.Instance.SEButtonPlay ();
		}
	}

	public GameObject CreateCharactor(int num){
		return _DefenceManager.GetComponent<DefenceManager> ().CreatDefenceCharctor (num, false);
	}

	public void ChangeGame(){
		if (_isStartOK) {
			SoundManager.Instance.SEButtonPlay ();
			_isStartOK = false;
			_isStart = true;
			waku1.SetActive (false);
			waku2.SetActive (false);
			TransitionAnimation.Instance.StopAllScaleAnimations ();
			TransitionAnimation.Instance.ClearAllScaleAnimationList ();
			ChangeGameStart ();
		}
	}

	public void ChangeGameStart(){
		_isChangeGameStart = true;
		_isChangeShow = false;
		if (!_isAttack)
			_demoobject.GetComponent<DemoPlay> ().PlayStart ();
		else {
			_EnemyManager.GetComponent<EnemyManager> ().MoveStop ();
			Factory.Instance.BulletFlyweight (false);
			_EnemyManager.GetComponent<EnemyManager> ().MoveSatrtPosition ();
		}
	}

	void GameStart(){
		_isChangeGameStart = false;
		_SelectUICamera.SetActive (false);
		_UICamera.SetActive (true);
		_MainCamera.GetComponent<Camera> ().rect = new Rect (0, 0, 1, 1);

		_round.GetComponent<Round> ().StartBattle ();
	}

	public void ShowStart(){
		ie = ShowStartCol ();
		StartCoroutine(ie);
	}

	IEnumerator ShowStartCol(){
		_StartText.SetActive (true);
		FindObjectOfType<Retire> ().IsPlaying = true;
		float time = 0;
		// アニメーション
		while (time <= 1.5f) {
			_StartText.transform.localScale = new Vector3(0.5f + time/3.0f,0.5f + time/3.0f,1.0f);
			time += Time.deltaTime;
			yield return null;
		}
		yield return new WaitForSeconds (0.5f);
		if (_isAttack && !_isFight) {
			_SkipSpeed.GetComponent<SkipSpeed> ().ShowSpeedButton ();
		}
		_DefenceManager.GetComponent<DefenceManager> ().PlayerStart ();
		_StartText.SetActive (false);
		yield return new WaitForSeconds (1.0f);
		if (_isFarst) {
			_EnemyManager.GetComponent<EnemyManager> ().MoveStart ();
			_isFarst = false;
		}
		else
			_EnemyManager.GetComponent<EnemyManager> ().MoveReStart ();
		ie = null;
	}

	public void StopCol(){
		if(ie != null)
			StopCoroutine (ie);
	}
}
