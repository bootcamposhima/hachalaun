﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SCImage : MonoBehaviour {

	// イメージ
	[SerializeField]
	Sprite[] _Normal;
	[SerializeField]
	Sprite[] _Fight;
	[SerializeField]
	Sprite[] _Edit;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ChangeSelectmode(int mode){
		StopAllCoroutines ();
		switch (mode) {
		case 1:
			StartCoroutine (Imagecol (_Normal));
			break;
		case 2:
			StartCoroutine (Imagecol (_Fight));
			break;
		case 3:
			StartCoroutine (Imagecol (_Edit));
			break;
		default:
			StopAllCoroutines ();
			this.GetComponent<Image> ().sprite = null;
			break;
		}
	}

	IEnumerator Imagecol(Sprite[] image){
		float time = 0;
		float a = 255 / 60.0f;
		Image im = this.GetComponent<Image> ();
		while (true) {
			// イメージ数繰り返す
			for (int i = 0; i < image.Length; i++) {
				// 表示(フェードイン)
				im.sprite = image [i];
				time = 0;
				while (time <= 1.5f) {
					im.color = new Color (255, 255, 255, 0 + time / 1.0f);
					time += Time.deltaTime;
					yield return null;
				}
				yield return new WaitForSeconds (1.0f);
				// 非表示(フェードアウト)
				time = 0;
				while (time <= 1.5f) {
					im.color = new Color (255, 255, 255, 255 - time / 1.0f);
					time += Time.deltaTime;
					yield return null;
				}
			}
		}
	}
}
