﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;

public struct AIData{
	public int _charactornumber;
	public int _direction;
	public int[] _bullet;
	public int[] _position;

	public AIData(int charactornumber,int direction,int[] bullet,int[] position)
	{
		_charactornumber = charactornumber;
		_direction = direction;
		_bullet = bullet;
		_position = position;
	}
}

public struct AIDatas{
	public List<AIData> _dataList;

	public AIDatas(List<AIData> dataList)
	{
		_dataList = dataList;
	}

	public AIDatas(int num)
	{
		_dataList = new List<AIData> (num);
	}

}

public class AI : MonoBehaviour {

	//保存しているAIDataを読み込む
	public static AIDatas LoadAIData(string filename){

		TextLodaer txl = TextLodaer.Instance;
		string jstr = txl.ReadTextFile (filename);
		AIDatas _aidatas = LitJson.JsonMapper.ToObject<AIDatas> (jstr);
		return _aidatas;
	}

	//保存しているAIDataを読み込む
	public static AIDatas LoadAIData2(string filename){

		TextLodaer txl = TextLodaer.Instance;
		string jstr = txl.ReadTextFile2 (filename);
		AIDatas _aidatas = LitJson.JsonMapper.ToObject<AIDatas> (jstr);
		return _aidatas;
	}

	public static List<AIDatas> LoadAllAidatas(){
		List<AIDatas> _list = new List<AIDatas> ();

		string aifilepath=Application.persistentDataPath;
		string[] str = System.IO.Directory.GetFiles (aifilepath);
		if (str.Length < 1) {
			string strfilpath = Application.streamingAssetsPath + "/AI";
			string[] strs = System.IO.Directory.GetFiles (strfilpath);
			int num = 1;
			for (int i = 0; i < strs.Length; i++) {
				if (strs [i].IndexOf ("meta") < 1 && strs [i].IndexOf ("Store") < 1) {
					AI.FileWrite2 (aifilepath + "/AI" + num.ToString () + ".text", AI.LoadAIData2 (strs [i]));
					_list.Add (AI.LoadAIData2 (strs [i]));
					num++;
				}
			}
		} else {
			for (int i = 0; i < str.Length; i++) {
				if (str [i].IndexOf ("meta") < 1 && str [i].IndexOf ("Store") < 1) {
					_list.Add (AI.LoadAIData2 (str [i]));
				}
			}
		}

		return _list;
	}

	public static List<string> LoadAllAidatasPath(){
		string _aifilepath;
	
		List<string> fs = new List<string> ();

		_aifilepath=Application.persistentDataPath;
		string[] str = System.IO.Directory.GetFiles (_aifilepath);
		if (str.Length < 1) {
			string strfilpath = Application.streamingAssetsPath + "/AI";
			string[] strs = System.IO.Directory.GetFiles (strfilpath);
			int num = 1;
			for (int i = 0; i < strs.Length; i++) {
				if (strs [i].IndexOf ("meta") < 1 && strs [i].IndexOf ("Store") < 1) {
					AI.FileWrite2 (_aifilepath + "/AI" + num.ToString () + ".text", AI.LoadAIData2 (strs [i]));
					fs.Add (_aifilepath +"/AI" + num.ToString () + ".text");
					num++;
				}
			}
		} else {
			for (int i = 0; i < str.Length; i++) {
				if (str [i].IndexOf ("meta") < 1 && str [i].IndexOf ("Store") < 1) {
					fs.Add (str [i]);
				}
			}
		}

		return fs;
	}

	//AIDataを新しく作る
	public static AIDatas CreateAIData(){

		List<AIData> dataList = new List<AIData> ();

		for (int i = 0; i < 4; i++) {
			int[] bullet = new int[]{ 0 };
			int[] position = new int[]{ 0 };
			dataList.Add (new AIData (0,i, bullet, position));
		}

		AIDatas _aidatas = new AIDatas(dataList);

		return _aidatas;
	}

	// jsonからAIDataを生成
	public static AIDatas CreateJsonAIData(string json){

		AIDatas _aidatas = LitJson.JsonMapper.ToObject<AIDatas> (json);

		return _aidatas;
	}

	//AIDataの上書き
	public static AIDatas Write(int charactornumber,int direction,int[] bullet,int[] position,AIDatas ads)
	{
		AIDatas	aidatas = ads;
		int num = aidatas._dataList.Count;
		for (int i = 0; i < num; i++) {
			if (direction == aidatas._dataList [i]._direction) {
				aidatas._dataList.Remove (aidatas._dataList [i]);
				break;
			}
		}
		aidatas._dataList.Add(new AIData (charactornumber,direction, bullet, position));
		Debug.Log (charactornumber);
		Debug.Log (LitJson.JsonMapper.ToJson (aidatas));

		return aidatas;
	}

	//AIDataの上書き
	public static AIDatas Write(int charactornumber,int direction,AIDatas ads)
	{
		AIDatas	aidatas = ads;
		int num = aidatas._dataList.Count;
		int[] bullet = null;
		int[] position = null;
		for (int i = 0; i < num; i++) {
			if (direction == aidatas._dataList [i]._direction) {
				bullet=new int[aidatas._dataList[i]._bullet.Length];
				aidatas._dataList [i]._bullet.CopyTo (bullet, 0);
				position=new int[aidatas._dataList[i]._position.Length];
				aidatas._dataList [i]._position.CopyTo (position, 0);
				aidatas._dataList.Remove (aidatas._dataList [i]);
				break;
			}
		}
		if(bullet!=null&&position!=null)
			aidatas._dataList.Add(new AIData (charactornumber,direction, bullet, position));

		return aidatas;
	}

	//AIDataを保存
	public static void FileWrite(string name,AIDatas ads)
	{
		TextLodaer txl = TextLodaer.Instance;
		txl.WriteTextFile (name, LitJson.JsonMapper.ToJson (ads));
	}

	//AIDataを保存
	public static void FileWrite2(string name,AIDatas ads)
	{
		TextLodaer txl = TextLodaer.Instance;
		txl.WriteTextFile2 (name, LitJson.JsonMapper.ToJson (ads));
	}
		
}