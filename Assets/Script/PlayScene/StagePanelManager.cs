﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StagePanelManager : MonoBehaviour {
	[SerializeField]
	GameObject _stagepanel;

	List<GameObject> _listobjects;

	List<IEnumerator> _listienumerator;

	bool isstop;

	// Use this for initialization
	void Start () {
		_listobjects = new List<GameObject> ();
		_listienumerator = new List<IEnumerator> ();
		isstop = false;
	}

	// Update is called once per frame
	void Update () {
		if (!isstop && Time.timeScale == 0) {
			isstop = true;
			int count = _listienumerator.Count;
			for (int i = 0; i < count; i++) {
				StopCoroutine (_listienumerator [i]);
			}
		} else if(isstop==true){
			isstop = false;
			int count = _listienumerator.Count;
			for (int i = 0; i < count; i++) {
				StartCoroutine (_listienumerator [i]);
			}
		}
	}

	GameObject CreateObject(){
		GameObject ob=Instantiate (_stagepanel);
		_listobjects.Add (ob);
		return ob;
	}

	GameObject GetObject(){
		int count = _listobjects.Count;
		for (int i = 0; i < count; i++) {
			if (!_listobjects [i].activeSelf)
				return _listobjects [i];
		}
		return CreateObject ();
	}

	public void LightAction(int direction,float position,float time){
		IEnumerator ie = StagePanelLightAction (GetObject (), direction, position, time);
		_listienumerator.Add (ie);
		StartCoroutine (ie);
	}

	IEnumerator StagePanelLightAction(GameObject ob,int direction,float position,float time){
		ob.SetActive (true);

		if (direction == 0||direction == 1){
			ob.transform.localScale = new Vector3 (2.0f, 0.1f, 0.4f);
			ob.transform.localPosition = new Vector3 (0, 0.05f, position);

		}else{
			ob.transform.localScale = new Vector3 (0.4f, 0.1f, 2.0f);
			ob.transform.localPosition = new Vector3 (position, 0.05f, 0);
		}

		yield return new WaitForSeconds (time);

		ob.SetActive (false);

		yield break;
	}
}
