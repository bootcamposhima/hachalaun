﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HPManager : MonoBehaviour {

	int _defaulthpnum;
	int _hpnum;
	//float 

	//HPのSprite
	[SerializeField]
	GameObject _hpgauge;
	[SerializeField]
	GameObject _redhpgauge;
	[SerializeField]
	GameObject _hphert;
	[SerializeField]
	GameObject _redhphert;
	[SerializeField]
	GameObject _hptext;

	IEnumerator _ie;
	IEnumerator _ie2;

	GameObject[] _bomPar;

	// Use this for initialization
	void Start () {
		_defaulthpnum = 100;
		_hpnum = 100;
		_ie = null;
		_ie2 = null;
		_bomPar = new GameObject[2];
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void init(int num){
		_defaulthpnum = num;
		_hpnum = num;
		SetHPActive (true);
		ChangeObjects (100);
		float numx = 3.5f * num;
		_redhpgauge.GetComponent<RectTransform> ().sizeDelta = new Vector2 (numx, 50.0f);
	}

	public void Damage(int damage){
		_hpnum -= damage;
		if (_hpnum <= 0)
			_hpnum = 0;
		int num = (_hpnum * 100) / _defaulthpnum;
		SetHPActive (false);
		ChangeObjects (num);
		_hptext.GetComponent<Text> ().color=Color.white;
		bomParticle (0, new Vector3 (-2.5f, 14.2f, -2.3f));
		if (_ie == null) {
			_ie = DamagetAnimation ();
			StartCoroutine (_ie);
		}
		if (_ie2 != null)
			StopCoroutine (_ie2);
		_ie2 = DamagetAnimation2 ();
		StartCoroutine (_ie2);
	}

	public void SetHPActive(bool flag){
		_hphert.SetActive (flag);
	}

	public void ChangeObjects(int num){
		_hptext.GetComponent<Text> ().text = num.ToString () + "%";
		float numx = 3.5f * num;
		_hpgauge.GetComponent<RectTransform> ().sizeDelta = new Vector2 (numx, 50.0f);
	}

	IEnumerator DamagetAnimation(){
		Vector2 size = _redhpgauge.GetComponent<RectTransform> ().sizeDelta;

		yield return new WaitForSecondsRealtime (0.7f);

		for (;;) {
			if (Time.timeScale == 0)
				yield return null;
			size.x -= 1.5f;
			_redhpgauge.GetComponent<RectTransform> ().sizeDelta = size;
			Vector2 s = _hpgauge.GetComponent<RectTransform> ().sizeDelta;
			if (s.x >= size.x - 0.5f && s.x <= size.x + 0.5f) {
				_hptext.GetComponent<Text> ().color = new Color (0.2f, 0.2f, 0.2f);
				SetHPActive (true);
				_ie = null;
				yield break;
			}
			yield return null;
		}
	}

	IEnumerator DamagetAnimation2(){
		_hptext.transform.localScale = new Vector3 (1.5f, 1.5f, 1.0f);
		yield return new WaitForSecondsRealtime (0.5f);
		bomParticle (1, new Vector3 (-7.5f, 13.0f, -2.3f));
		_hptext.transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
		_ie2 = null;
	}

	void bomParticle(int num,Vector3 parpos){
		string a = "StopBomPar" + num.ToString();
		if (_bomPar [num]) {
			ParticleManager.Instance.ParticleStop (_bomPar [num]);
			CancelInvoke (a);
		}// xを変更　-7から-2.5の範囲
		_bomPar[num] = FindObjectOfType<ParticleManager> ().ShotParticleStart (parpos);
		Invoke(a,_bomPar[num].GetComponent<ParticleSystem>().duration);
	}

	void StopBomPar0(){
		if(_bomPar [0])
			ParticleManager.Instance.ParticleStop (_bomPar[0]);
		_bomPar[0] = null;
	}

	void StopBomPar1(){
		if(_bomPar [1])
			ParticleManager.Instance.ParticleStop (_bomPar[1]);
		_bomPar[1] = null;
	}

}
