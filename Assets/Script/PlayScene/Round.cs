﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public abstract class Round : MonoBehaviour {
	/// <summary>
	/// UI
	/// </summary>
	[SerializeField]
	protected GameObject _defencePlayerHPGaugeUI;
	[SerializeField]
	protected GameObject _currentRoundNumberTextUI;
	[SerializeField]
	protected GameObject _firstDefenceOrAttackTextUI;

	/// <summary>
	/// その他
	/// </summary>
	protected int  _currentRound;
	protected bool _isAttackTurnPlaying;

	// 次のラウンドに移行する
	public    abstract void ChangeNextRound ();
	// バトル部分を始める
	public    abstract void StartBattle();
	// バトルが終了して次に移る
	public    abstract void OnFinishBattle();
	// 選択開始
	protected abstract void ShowCharacterSelectUIs();
	// 選択終了
	public    virtual  void HideCharacterSelectUIs(){}

	/// <summary>
	/// シーン遷移
	/// </summary>
	protected virtual void TransitionScene(){
		FindObjectOfType<StageManager> ().OnClickStageButton ("Result");
	}

	/// <summary>
	/// Roundスタート時のUI表示を呼び出す
	/// </summary>
	protected virtual void SettingShowUIsForStartRound(){
		Vector3 textUIsScaleSize = new Vector3 (0.0f, 0.0f, 1.0f);
		// サイズ変更
		_currentRoundNumberTextUI  .transform.localScale = textUIsScaleSize;
		_firstDefenceOrAttackTextUI.transform.localScale = textUIsScaleSize;

		// 文字の設定
		_currentRoundNumberTextUI  .GetComponent<Text> ().text = "Round" + _currentRound.ToString ();
		_firstDefenceOrAttackTextUI.GetComponent<Text> ().text = (_isAttackTurnPlaying) ? "先攻":"後攻";

		// 表示呼び出し
		StartCoroutine(ShowUIsForStartRound());
	}

	/// <summary>
	/// Roundスタート時のUI表示
	/// </summary>
	IEnumerator ShowUIsForStartRound(){
		// 表示
		_currentRoundNumberTextUI  .SetActive (true);
		_firstDefenceOrAttackTextUI.SetActive (true);

		// UIの表示アニメーション
		float elapsedTime = 0;
		Vector3 textUIsScaleSize;
		while (elapsedTime <= 1.0f) {
			textUIsScaleSize = new Vector3(0.5f + elapsedTime / 2.0f, 0.5f + elapsedTime / 2.0f, 1.0f);
			_currentRoundNumberTextUI  .transform.localScale = textUIsScaleSize;
			_firstDefenceOrAttackTextUI.transform.localScale = textUIsScaleSize;
			elapsedTime += Time.deltaTime;
			yield return null;
		}

		yield return new WaitForSeconds (2.0f);
		// 表示を消す
		_currentRoundNumberTextUI  .SetActive (false);
		_firstDefenceOrAttackTextUI.SetActive (false);

		// キャラクター選択を始める
		ShowCharacterSelectUIs ();
	}
}