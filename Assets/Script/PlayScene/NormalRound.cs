﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NormalRound : Round {
	int  _gameCountOfCurrentRound;
	int  _firstDefenseRemainderHP;
	bool _isPlaying;
	// 難易度
	int  _playLevel;
	DefenceManager _defenceManager;
	EnemyManager   _enemyManager;

	// 定数
	int MAX_GAMECOUNT = 2;
	int MAX_AIDATA    = 3;
	int MAX_ROUND     = 3;

	/// <summary>
	/// 初期化
	/// </summary>
	void Start () {
		_gameCountOfCurrentRound = 0;
		_firstDefenseRemainderHP = 0;
		_currentRound            = 0;
		_isPlaying = false;
		// 弾のリセット
		Factory.Instance.Reset ();
		_defenceManager = FindObjectOfType<DefenceManager> ();
		_enemyManager   = FindObjectOfType<EnemyManager>   ();
	}

	/// <summary>
	/// 先行後攻を決めて勝負を始める
	/// </summary>
	public void GameStart(int level){
		_playLevel = level;
		// 先攻後攻を決める
		_isAttackTurnPlaying = (Random.Range (0, 2) == 1);
		// ラウンドの開始
		ChangeNextRound ();
	}

	/// <summary>
	/// 次のラウンドに移行して開始
	/// </summary>
	public override void ChangeNextRound(){
		_currentRound++;
		_gameCountOfCurrentRound = 0;
		// 不必要な表示を消す
		PrepareForNextGame ();
		// 1.0f後にラウンドを開始
		Invoke ("SettingShowUIsForStartRound", 1.0f);
	}

	/// <summary>
	/// 不必要な表示を消してキャラクター選択画面を表示
	/// </summary>
	protected override void ShowCharacterSelectUIs(){
		PrepareForNextGame ();
		FindObjectOfType<PlaySelect> ().Show(_isAttackTurnPlaying);
	}

	/// <summary>
	/// 不要な表示を消す
	/// </summary>
	void PrepareForNextGame(){
		_enemyManager  .DestroyEnemies   ();
		_defenceManager.DestroyCharacter ();
		_defencePlayerHPGaugeUI.SetActive (false);
		Factory.Instance.BulletFlyweight  (false);
	}

	/// <summary>
	/// バトル部分を始める
	/// </summary>
	public override void StartBattle(){
		_gameCountOfCurrentRound++;
		_isPlaying = true;
		// 敵の生成
		if (_isAttackTurnPlaying) {
			_defenceManager.CreatDefenceCharctor(SelectDefenceCharacter(), true);
		} else {
			_enemyManager.CreateEnemies(LoadEnemyAIData (), false);
			FindObjectOfType<Player> ().FightPlayer = true;
		}
		// スタートの表示
		FindObjectOfType<PlaySelect> ().ShowStart ();
	}

	/// <summary>
	/// バトル終了時
	/// </summary>
	public override void OnFinishBattle(){
		// すでにプレイ中じゃない場合
		if (!_isPlaying)
			return;

		// フラグの変更
		_isPlaying = false;
		FindObjectOfType<Retire>().IsPlaying = false;
		Factory.Instance.BulletFlyweight (false);
		// プレイヤーが攻撃だった場合:Speedボタンの表示を消す
		if (_isAttackTurnPlaying)
			FindObjectOfType<SkipSpeed> ().HideSpeedButton ();

		// 終了アクション
		int remainderHP = _defenceManager.GetHP();
		if (remainderHP <= 0) {
			_defenceManager.EndAction ();
			_enemyManager  .MoveStop  ();
		} else {
			_enemyManager.EndAction ();
		}

		// 次のゲームに移行する
		ChangeNextGame ();
		// HPデータの保存
		SaveRemainderHP (remainderHP);

		if (_gameCountOfCurrentRound < MAX_GAMECOUNT)
			return;

		// リザルト表示
		ShowResult (remainderHP);
	}

	/// <summary>
	/// 次のゲームに移行する
	/// </summary>
	void ChangeNextGame() {
		// 次のゲームに移行する
		if (_gameCountOfCurrentRound < MAX_GAMECOUNT) {
			_isAttackTurnPlaying = !_isAttackTurnPlaying;
			Invoke ("ShowCharacterSelectUIs", 3.3f);
			return;
		}
		if(_currentRound < MAX_ROUND)
			// 次のラウンドに移行する
			Invoke ("ChangeNextRound", 6.3f);
		else
			// ラウンド終了
			Invoke ("TransitionScene", 3.3f);
	}

	/// <summary>
	/// HPデータの保存
	/// </summary>
	void SaveRemainderHP(int remainderHP){
		if (_gameCountOfCurrentRound < MAX_GAMECOUNT) {
			// 一時保存
			_firstDefenseRemainderHP = remainderHP;
		} else {
			// 保存
			int playerRemainderHP = (_isAttackTurnPlaying)  ? _firstDefenseRemainderHP : remainderHP;
			int enemyRemainderHP  = (!_isAttackTurnPlaying) ? _firstDefenseRemainderHP : remainderHP;

			FindObjectOfType<ResultData> ().SetRemainderHPs (playerRemainderHP, enemyRemainderHP, _currentRound);
		}
	}

	/// <summary>
	/// リザルト表示
	/// </summary>
	void ShowResult(int remainderHP){
		int playerRemainderHP = (_isAttackTurnPlaying)  ? _firstDefenseRemainderHP : remainderHP;
		int enemyRemainderHP  = (!_isAttackTurnPlaying) ? _firstDefenseRemainderHP : remainderHP;

		FindObjectOfType<InterimResult> ().SettingShowInterimResult (playerRemainderHP, enemyRemainderHP, _currentRound);
	}

	/// <summary>
	/// EnemyのAIを読み込み
	/// </summary>
	AIDatas LoadEnemyAIData(){
		// ランダムで選択して読み込む
		switch (Random.Range(0,MAX_AIDATA)) {
		case 1:
			return AI.LoadAIData ("EnemyAI/AI2.text");
		case 2:
			return AI.LoadAIData ("EnemyAI/AI3.text");
		case 3:
			return AI.LoadAIData ("EnemyAI/AI4.text");
		default:
			return AI.LoadAIData ("EnemyAI/AI1.text");
		}
	}

	/// <summary>
	/// EnemyのDefenceChar選択
	/// </summary>
	int SelectDefenceCharacter(){
		switch (_playLevel) {
		case 1:
			return Random.Range (0, 2);
		case 2:
			return Random.Range (0, 3);
		default:
			return 1;
		}
	}
}