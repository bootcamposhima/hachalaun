﻿using UnityEngine;
using System.Collections;

public class Fight : MonoBehaviour {

	string _roomid;
	FixedID _enemyfid;
	bool _attack;

	// Use this for initialization
	void Start () {
	
	}

	void Awake()
	{
		// 新しいシーンを読み込んでもオブジェクトが自動で破壊されないように設定
		DontDestroyOnLoad(this.gameObject);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public bool Attack{
		get{
			return _attack;
		}

		set{
			_attack = value;
		}
	}

	public string Room{
		get{
			return _roomid;
		}

		set{
			_roomid = value;
		}
	}

	//敵のID
	public FixedID EnemyFID{
		get{
			return _enemyfid;	
		}

		set{
			_enemyfid = value;
		}
	}

	public string EnemyID{
		get{
			return _enemyfid.id;	
		}
	}

	public string Enemyname{
		get{
			return _enemyfid.name;	
		}
	}

	public void Des(){
		Destroy (this.gameObject);
	}

}
