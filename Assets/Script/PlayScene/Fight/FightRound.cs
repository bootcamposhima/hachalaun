﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SocketIO;

public class FightRound : Round {

	[SerializeField]
	GameObject _stopbutton;
	[SerializeField]
	GameObject _retiretext;
	//秒数
	[SerializeField]
	Text _text;

	//試合結果
	int decision;
	//ゲームのカウント
	int GameCount;

	//サーバーに切断されたかどうか
	bool isdisconnection;
	//相手をLostしたかどうか
	bool isretire;
	//次のゲームへ移行するかのフラグ
	bool isNextGame;
	//相手のAIDatasの情報を受信したかどうかのフラグ
	bool _receptionAI;
	//相手のスコア情報を受信したかどうかのフラグ
	bool _receptionscore;
	//AIDatasを送信したかどうかのフラグ
	bool _sendAI;
	//スコア情報を送信したかどうかのフラグ
	bool _sendscore;
	//Startしているかどうか
	bool isstart;
	//相手のAIDatas
	AIDatas _enemyAi;
	//敵のキャラクター
	int _charnum;
	//ファイトモードのマネージャー
	FightSocket _fm;

	// Use this for initialization
	void Start () {
		_fm = FindObjectOfType<FightSocket> ();
		Factory.Instance.Reset ();
	}

	void Awake(){
		_currentRound = 0;
		isretire = false;
		isdisconnection = false;
		isstart = false;
		_charnum = 0;
		Fightinit ();
	}

	void init(){
		_isAttackTurnPlaying = FindObjectOfType<Fight> ().Attack;
		GameCount = 0;
	}

	void Fightinit(){
		_receptionAI = false;
		_receptionscore = false;
		_sendAI = false;
		_sendscore = false;
		isNextGame = false;
	}

	public override void HideCharacterSelectUIs(){
		//AIDatasもしくはCharaNumを追加
		SummarizeJson mj;
		if (_isAttackTurnPlaying) {
			AIDatas ai=FindObjectOfType<EnemyManager> ().aidatas;
			FightJson fj = new FightJson (0,ai);
			mj = new SummarizeJson (0, fj);
		} else {
			FightJson fj = new FightJson (1,FindObjectOfType<PlaySelect>().CharaNum);
			mj = new SummarizeJson (1, fj);
		}

		MessageJson m = new MessageJson (mj);

		string str = LitJson.JsonMapper.ToJson (m);
		JSONObject jsonobject = JSONObject.Create (str);

		//送信
		_fm.EmitMassage (jsonobject);

		//ゲームスタート
		FindObjectOfType<PlaySelect>().ChangeGameStart ();
		StopCoroutine ("RoundTimeAction");
		_sendAI = true;
	}

	protected override void ShowCharacterSelectUIs(){
		Debug.Log (GameCount);
		FindObjectOfType<EnemyManager> ().DestroyEnemies ();
		FindObjectOfType<DefenceManager> ().DestroyCharacter ();
		Factory.Instance.BulletFlyweight (false);
		Fightinit ();
		GameCount++;
		isNextGame = false;
		isstart = false;
		// 敵を配置する
		FindObjectOfType<PlaySelect>().Show(_isAttackTurnPlaying);
		if (_receptionAI) {
			if (_isAttackTurnPlaying) {
				FindObjectOfType<PlaySelect> ().CreateEnemy (_enemyAi);
			} else {
				GameObject g = FindObjectOfType<PlaySelect> ().CreateCharactor (_charnum);
				g.GetComponent<Player> ().FightPlayer = false;
			}
		} 
		StartCoroutine ("RoundTimeAction");
	}

	public void DissconnectMessage(SocketIOEvent e){
		Debug.Log ("DissconnectMessage");
		//サーバーに切断された通知
		FindObjectOfType<ResultData>().IsDisconnected=true;
		//リタイアボタンを押させないようにする
		_stopbutton.GetComponent<Button> ().interactable = false;
		//切断を表示
		_retiretext.GetComponent<Text>().text="サーバーから切断されました";
		IEnumerator ie = ShowRetire (_retiretext);
		StartCoroutine (ie);
	}

	public override void StartBattle(){
		if (_sendAI && _receptionAI && !_isAttackTurnPlaying) {
			isstart = true;
			Player p = FindObjectOfType<Player> ();
			p.FightPlayer = true;
			p.StartTime = Time.time;
			//JsonObject作成
			FightJson fj = new FightJson (3);
			string str=LitJson.JsonMapper.ToJson (new MessageJson(new SummarizeJson(1,fj)));
			JSONObject jsonObject = JSONObject.Create (str);
			_fm.EmitMassage (jsonObject);
			FindObjectOfType<PlaySelect>().GetComponent<PlaySelect> ().ShowStart();
		}
	}

	public void RetireMassage(SocketIOEvent e){
		Debug.Log ("RetireMassage");
		//相手がリタイアしたメッセージ
		FindObjectOfType<ResultData>().IsOpponentRetired=true;
		//リタイアボタンを押させないようにする
		_stopbutton.GetComponent<Button> ().interactable = false;
		//リタイア表示
		_retiretext.GetComponent<Text>().text="相手がリタイアしました";
		IEnumerator ie = ShowRetire (_retiretext);
		StartCoroutine (ie);
	}

	public void Message(SocketIOEvent e){
		int num = LitJson.JsonMapper.ToObject<SummarizeJson> (e.data.GetField("value").ToString()).fjson.num;
		Debug.Log (num.ToString ());
		if (num == 0)
			ReceptionAI (e);
		else if (num == 1)
			ReceptionChar (e);
		else if (num == 2)
			ReceptionScore (e);
		else if (num == 3)
			ReceptoionStart ();

	}

	public override void OnFinishBattle(){

		DefenceManager de = FindObjectOfType<DefenceManager> ();

		// 終了アクション
		if (de.GetHP () <= 0) {
			de.EndAction ();
			FindObjectOfType<EnemyManager> ().MoveStop ();
		}
		else
			FindObjectOfType<EnemyManager> ().EndAction ();

		if (!_isAttackTurnPlaying) {
			if (!_sendscore) {
				print ("送信");
				//結果送信
				_sendscore = true;
				//JsonObject作成
				FightJson fj = new FightJson (2, FindObjectOfType<DefenceManager> ().GetHP ());
				string str = LitJson.JsonMapper.ToJson (new MessageJson (new SummarizeJson (1, fj)));
				JSONObject jsonObject = JSONObject.Create (str);
				_fm.EmitMassage (jsonObject);

				FindObjectOfType<ResultData> ().SetPlayerRemainderHP (de.GetHP (), _currentRound);
				_receptionscore = true;
			}
		} else {
			_sendscore = true;
		}

		NextGameStart ();

	}

	public void NextGameStart(){
		if (!isNextGame&&_sendscore&&_receptionscore) {
			//Debug.Log (FindObjectOfType<ResultData> ().);
			isNextGame = true;
			_isAttackTurnPlaying = !_isAttackTurnPlaying;
			if (GameCount >= 2) {
				if (_currentRound >= 3) {
					Invoke ("TransitionScene", 3.3f);
				} else {
					ResultData rd = FindObjectOfType<ResultData> ();
					FindObjectOfType<InterimResult> ().SettingShowInterimResult (rd.GetRemainderHP(true)[_currentRound-1], rd.GetRemainderHP(false)[_currentRound-1],_currentRound);
					Invoke ("ChangeNextRound", 6.3f);
				}
			} else {
				Invoke ("ShowCharacterSelectUIs", 3.3f);
			}
		}

	}

	// ラウンドを始める
	public override void ChangeNextRound(){
		init ();
		_currentRound++;
		if (_currentRound != 0) {
			_defencePlayerHPGaugeUI.SetActive (false);
		}
		Invoke ("SettingShowUIsForStartRound", 1.0f);
	}

	public void ReceptionAI(SocketIOEvent e){
		Debug.Log ("ReceptionAI");
		_receptionAI = true;
		SummarizeJson mj = LitJson.JsonMapper.ToObject<SummarizeJson> (e.data.GetField ("value").ToString());
		_enemyAi = mj.fjson.aidatas;
		if (!isstart) {
			FindObjectOfType<PlaySelect>().CreateEnemy (_enemyAi);
			StartBattle ();
		}
	}

	public void ReceptionChar(SocketIOEvent e){
		Debug.Log ("ReceptionChar");
		_receptionAI = true;
		SummarizeJson mj = LitJson.JsonMapper.ToObject<SummarizeJson> (e.data.GetField ("value").ToString());
		if (isstart) {
			_charnum = mj.fjson.hp;
		} else {
			GameObject g = FindObjectOfType<PlaySelect> ().CreateCharactor (mj.fjson.hp);
			g.GetComponent<Player> ().FightPlayer = false;
		}
	}

	public void ReceptionScore(SocketIOEvent e){
		Debug.Log ("ReceptionScore");
		_receptionscore = true;
		SummarizeJson mj = LitJson.JsonMapper.ToObject<SummarizeJson> (e.data.GetField ("value").ToString());
		decision = mj.fjson.hp;
		FindObjectOfType<ResultData> ().SetEnemyRemainderHP (decision, _currentRound);
		NextGameStart ();
	}

	public void ReceptoionStart(){
		FindObjectOfType<PlaySelect>().ShowStart();
	}
		
	//敵選択のアクション
	IEnumerator RoundTimeAction(){
		float time = 1800;
		for (;;) {
			time--;
			int t = (int)(time / 60 + 1);
			_text.text = t.ToString();
			if (time <= 0) {
				//ファイトシーンに飛ぶ
				if (_isAttackTurnPlaying) {
					FindObjectOfType<PlaySelect>().SelectAI (0);
				} else {
					FindObjectOfType<PlaySelect>().SelectCharactor (0);
				}
				HideCharacterSelectUIs ();
				break;
			} 
			yield return null;
		}
	}

	//雪乃のリタイア表示するのをパクった
	IEnumerator ShowRetire(GameObject _go){
		_go.SetActive (true);
		float time = 0;
		while (time <= 1.0f) {
			Vector3 p = _go.transform.position;
			_go.transform.position = new Vector3 (p.x, p.y - time / 0.5f, p.z);
			time += Time.deltaTime;
			yield return null;
		}
		yield return new WaitForSeconds (0.8f);
		// シーン遷移
		StageManager.Instance.OnClickStageButton ("Result");
	}

}

public struct FightJson{
	public int num;
	public int hp;
	public AIDatas aidatas;

	public FightJson(int n,AIDatas ad){
		aidatas = ad;
		num = n;
		hp = -1;
	}

	public FightJson(int n,int h){
		aidatas = new AIDatas(0);
		num = n;
		hp = h;
	}

	public FightJson(int n){
		aidatas = new AIDatas(0);
		num = n;
		hp = n;
	}

}