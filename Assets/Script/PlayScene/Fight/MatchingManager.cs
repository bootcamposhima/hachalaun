﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SocketIO;
using LitJson;

public class MatchingManager : SocketManager {

	int createcount;
	int deletecount;
	int time=0;
	
	[SerializeField]
	GameObject _button;
	[SerializeField]
	GameObject[] par;
	[SerializeField]
	Text _text;


	// Use this for initialization
	void Start () {
		createcount = 0;
		deletecount = 0;
		par = new GameObject[2];
		_text.text="接続中です。\nしばらくお待ちください";

		_button.SetActive (false);
		
		socket = FindObjectOfType<FightSocket> ();
		socket.EmitConnection ();
	}
	
	// Update is called once per frame
	void Update () {

	}

	public override void Online (SocketIOEvent e){
		_text.text="部屋を探しています。\nしばらくお待ちください";
		socket.EmitJoin ();	
	}

	public override void Join(SocketIOEvent e){
		FindObjectOfType<Fight> ().Room = e.data.GetField ("room").ToString();

		//部屋に入室した人のID取得
		string id = e.data.GetField ("id").str;

		//取得したIDが自分だった場合他の人が入ってくるまで待つ
		if (id==IDManager.Instance.ID) {
			_text.text="対戦相手を探しています。\nしばらくお待ちください";
			return;
		} else {
			//入っていた人のIDを記憶
			FindObjectOfType<Fight> ().EnemyFID = LitJson.JsonMapper.ToObject<FixedID> (e.data.ToString());
			//先攻後攻を決める
			int i = Random.Range (0, 2);
			FindObjectOfType<Fight> ().Attack = (i != 0) ? true : false;
			//FindObjectOfType<Fight> ().Attack = true;
			//先行後攻を決める
			JSONObject jsonObject = new JSONObject(JSONObject.Type.OBJECT);
			//ID
			jsonObject.AddField("id", IDManager.Instance.FID.id);
			//先攻後攻の情報
			jsonObject.AddField ("value", (i == 0) ? true : false);
			//jsonObject.AddField ("value", false);
			//送信
			socket.EmitMassage (jsonObject);
		}
		//ゲーム開始
		_text.text="対戦相手が見つかりました\n間も無く対戦が開始されます\n5秒前";
		//Invoke ("MoveStag", 5.0f);
		StartCoroutine("MatingTimeAction");

	}

	public override void Offline (SocketIOEvent e){
	}

	public override void Message(SocketIOEvent e){
		if (IDManager.Instance.ID == e.data.GetField ("id").str)
			return;
		//自分より先に人がいる場合
		FindObjectOfType<Fight> ().EnemyFID = LitJson.JsonMapper.ToObject<FixedID> (e.data.ToString());
		//入っていた人のIDを記憶
		bool b=e.data.GetField("value").b;
		FindObjectOfType<Fight> ().Attack = b;

		_text.text="対戦相手が見つかりました\n間も無く対戦が開始されます\n5秒前";
		StartCoroutine("MatingTimeAction");
	}

	public override void Onlinenotification(SocketIOEvent e){
		Debug.Log ("online");
	}

	public override void Endnotification(SocketIOEvent e){
		//部屋に入室した人のID取得
		string id = e.data.GetField ("id").str;
		if (id != IDManager.Instance.ID) {
			_text.text = "対戦相手が退出しました\n対戦相手を探しています。\nしばらくお待ちください";
			StopCoroutine ("MatingTimeAction");
		}
	}

	public override void Error(SocketIOEvent e){
		_text.text="接続に失敗しました";
		_button.GetComponent<Button> ().interactable = true;	
		_button.SetActive (true);
	}

	IEnumerator MatingTimeAction(){
		float time = 5;
		for (;;) {
			time-=Time.deltaTime;
			int t = (int)time;
			_text.text="対戦相手が見つかりました\n間も無く対戦を始めます\n"+t.ToString();
			if (time <= 1) {
				MoveStag ();
				yield break;
			} 
			yield return null;
		}
	}

	public void MoveStag(){
		//ファイトシーンに飛ぶ
		StageManager.Instance.OnClickStageButton ("FightRound");
	}

	public void StartParticle(){
		par[createcount] = FindObjectOfType<ParticleManager> ().AdmissionParticleStart (new Vector3(this.transform.position.x,1.0f,this.transform.position.z));
		Invoke("StopPar",par[createcount].GetComponent<ParticleSystem>().duration);
		createcount++;
	}

	private void StopPar(){
		FindObjectOfType<ParticleManager> ().ParticleStop (par[deletecount]);
		deletecount++;
	}

	public void TopReturnButton(){
		socket.EmitEndnotification ();
		socket.EmitDisconnect ();
		StageManager.Instance.OnClickStageButton ("Select");
	}

}
