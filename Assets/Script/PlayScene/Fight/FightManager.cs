﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SocketIO;
using LitJson;

public class FightManager : SocketManager {

	[SerializeField]
	FightRound _fm;

	// Use this for initialization
	void Start () {
		socket = FindObjectOfType<FightSocket> ();
		if (socket.isConnect) {
			_fm.ChangeNextRound ();
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public override void Online (SocketIOEvent e){
		
	}

	public override void Join(SocketIOEvent e){
	}

	public override void Offline (SocketIOEvent e){

		if (IDManager.Instance.ID == e.data.GetField ("id").str) {
			//通信が切断されました
			//敗北
			_fm.DissconnectMessage (e);
		} 

	}
		
	public override void Message(SocketIOEvent e){
		if (IDManager.Instance.ID == e.data.GetField ("id").str) {
			return;
		}

		//データの受信
		int num = LitJson.JsonMapper.ToObject<SummarizeJson>(e.data.GetField("value").ToString()).num;
		if (num!=2) {
			_fm.Message (e);
		} else {
			FindObjectOfType<Player> ().Massage (e);
		}
	}

	public override void Onlinenotification(SocketIOEvent e){
		
	}

	public override void Endnotification(SocketIOEvent e){
		if (IDManager.Instance.ID != e.data.GetField ("id").str) {
			//通信が切断されました
			//敗北
			_fm.RetireMassage(e);
		} 
	}

	public override void Error(SocketIOEvent e){
		
	}

}

public struct MessageJson{
	public string id;
	public SummarizeJson value;

	public MessageJson(SummarizeJson j){
		id = IDManager.Instance.ID;
		value = j;
	}
	
}

public struct SummarizeJson{
	public int num;
	public FightJson fjson;
	public PlayerJson pjson;

	public SummarizeJson(int n,FightJson j){
		num = n;
		fjson = j;
		pjson=new PlayerJson(0);
	}

	public SummarizeJson(int n,PlayerJson j){
		num = n;
		fjson = new FightJson(0);
		pjson = j;
	}
}