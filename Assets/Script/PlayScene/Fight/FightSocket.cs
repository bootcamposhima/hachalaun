﻿#region License
/*
 * TestSocketIO.cs
 *
 * The MIT License
 *
 * Copyright (c) 2014 Fabio Panettieri
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#endregion


using UnityEngine;
using System.Collections;

using SocketIO;
using LitJson;

public class FightSocket : SingletonMonoBehaviour<FightSocket> {

	private SocketIOComponent socket;
	private FixedID fid;

	// Use this for initialization
	void Start () {

//		// 新しいシーンを読み込んでもオブジェクトが自動で破壊されないように設定
//		DontDestroyOnLoad(this.gameObject);
	
		fid = IDManager.Instance.FID;
	
		socket.On("open", TestOpen);
		socket.On("error", TestError);
		socket.On("close", TestClose);
		socket.On("online", Online);
		socket.On("join", Join);
		socket.On("offline", Offline);
		socket.On("message", Message);
		socket.On("onlinenotification", Onlinenotification);
		socket.On("endnotification", Endnotification);

	}

	void Awake()
	{
		if (this != Instance)
		{
			Destroy(this);
			return;
		}

		// 新しいシーンを読み込んでもオブジェクトが自動で破壊されないように設定
		DontDestroyOnLoad(this.gameObject);

		socket = this.GetComponent<SocketIOComponent>();

	}

	// Update is called once per frame
	void Update () {

	}

	public bool isConnect{
		get{
			return socket.IsConnected;
		}
	}

	//========================================
	//::
	//::    サーバーへリクエストを送る関数類
	//::
	//========================================

	//サーバーに接続開始
	public bool EmitConnection(){
		if (!socket.IsConnected) {
			//サーバーに接続開始
			socket.Connect ();
			return false;
		}
		return true;
	}

	//サーバーに接続開始
	public bool EmitReConnection(){
		if (!socket.IsConnected) {
			//サーバーに接続開始
			socket.Connect ();
			return false;
		}
		return true;
	}

	//入室
	public void EmitJoin(){
		JSONObject jsonObject = new JSONObject(JSONObject.Type.OBJECT);
		jsonObject.AddField("id", fid.id);
		jsonObject.AddField("name", fid.name);

		socket.Emit ("join",jsonObject);
	}

	//データの送信
	public void EmitMassage(JSONObject jsonObject){

		socket.Emit ("message", jsonObject);
	}

	//データの送信
	public void EmitMassage(string json){

		socket.Emit ("message", json);
	}

	//受信できなかったメッセージを取得
	public void EmitOldmessagerequest(JSONObject jsonObject){

		socket.Emit ("message", jsonObject);
	}

	//切断申請
	public void EmitDisconnect(){
		if (isConnect) {
			socket.Emit ("disconnect");
		}
		socket.Close ();
	}

	//オンライン通知
	public void EmitOnlinenotification(){
		JSONObject jsonObject = new JSONObject(JSONObject.Type.OBJECT);
		jsonObject.AddField("id", fid.id);

		socket.Emit ("onlinenotification",jsonObject);
	}

	//通信終了通知
	public void EmitEndnotification(){
		JSONObject jsonObject = new JSONObject(JSONObject.Type.OBJECT);
		jsonObject.AddField("id", fid.id);

		socket.Emit ("endnotification",jsonObject);
	}

	//========================================
	//::
	//::    サーバーからのレスポンスを受ける関数類
	//::
	//========================================

	//サーバー接続完了
	//部屋に入室
	public void Online(SocketIOEvent e)
	{
		//Debug.Log("接続完了");
		//Debug.Log("部屋検出開始");
		FindObjectOfType<SocketManager> ().Online (e);
	}

	//部屋に入室完了
	//マッチング開始
	public void Join(SocketIOEvent e)
	{
		//Debug.Log("検出完了");
		//Debug.Log("マッチング開始");
		FindObjectOfType<SocketManager> ().Join (e);
	}

	//切断通知
	public void Offline(SocketIOEvent e)
	{
		FindObjectOfType<SocketManager> ().Offline (e);
	}
		
	//データ受信
	public void Message(SocketIOEvent e)
	{
		FindObjectOfType<SocketManager> ().Message (e);
	}

	//オンライン通知
	public void Onlinenotification(SocketIOEvent e)
	{
		FindObjectOfType<SocketManager> ().Onlinenotification (e);
	}

	//通信終了通知
	public void Endnotification(SocketIOEvent e)
	{
		Debug.Log ("通信終了");
		FindObjectOfType<SocketManager> ().Endnotification (e);
	}

	//========================================
	//::
	//::   デバッグ用関数
	//::
	//========================================

	public void TestOpen(SocketIOEvent e)
	{
		
	}

	public void TestError(SocketIOEvent e)
	{
		Debug.Log("[SocketIO] Error received: " + e.name + " " + e.data);
		Debug.Log (socket.IsConnected);
		FindObjectOfType<SocketManager> ().Error (e);
	}

	public void TestClose(SocketIOEvent e)
	{	
		Debug.Log("[SocketIO] Close received: " + e.name + " " + e.data);
	}
		
}
