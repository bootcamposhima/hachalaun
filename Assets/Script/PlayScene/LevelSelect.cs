﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelect : MonoBehaviour {
	[SerializeField]
	GameObject _levelSelectCanvas;
	[SerializeField]
	GameObject _gameStartButton;

	int _playLevel;

	void Start () {
		_playLevel = -1;
	}

	/// <summary>
	/// 難易度の選択
	/// </summary>
	public void SelectedLevel(int level){
		// SEの再生
		SoundManager.Instance.SEButtonPlay ();

		_playLevel = level;
		// 表示アニメーションの変更
		TransitionAnimation.Instance.StopAllScaleAnimations ();
		TransitionAnimation.Instance.ClearAllScaleAnimationList ();
		TransitionAnimation.Instance.StartScaleAnimation (_gameStartButton, 1.2f);
	}

	/// <summary>
	/// 難易度の選択終了
	/// </summary>
	public void OnClickGameStartButton(){
		SoundManager.Instance.SEButtonPlay ();
		TransitionAnimation.Instance.StopScaleAnimation (_gameStartButton);
		// 画面全体でフェイドをかけている間にCanvasを消す
		TransitionAnimation.Instance.HideObjectBetweenFade (_levelSelectCanvas,0.5f);
		Invoke ("GameStart", 0.6f);
	}

	/// <summary>
	/// ゲームを始める
	/// </summary>
	void GameStart(){
		FindObjectOfType<NormalRound> ().GameStart (_playLevel);
	}
}