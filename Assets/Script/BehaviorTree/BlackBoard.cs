﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BlackBoard : MonoBehaviour {

	GameObject _parent;

	[SerializeField]
	Dictionary<string,GameObject> _gameobject;
	[SerializeField]
	Dictionary<string,Vector3> _vector3;
	[SerializeField]
	Dictionary<string,int> _integer;
	[SerializeField]
	Dictionary<string,float> _fraction;
	[SerializeField]
	Dictionary<string,bool> _jude;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void Awake(){
		_gameobject = new Dictionary<string, GameObject> ();
		_vector3 = new Dictionary<string, Vector3> ();
		_integer = new Dictionary<string, int> ();
		_fraction = new Dictionary<string, float> ();
		_jude = new Dictionary<string, bool> ();
	}

	//ビヘイビアツリーが対象としているオブジェクトの取得
	public GameObject getParent(){
		return _parent;
	}

	//ビヘイビアツリーが対象とするオブジェクトの挿入
	public void setParent(GameObject gameobject){
		_parent = gameobject;
	}

	//ブラックボードに記載されているオブジェクトを取得
	public GameObject getGameObject(string str){
		return _gameobject[str];
	}

	//ブラックボードに記載されているオブジェクトの書き換え
	public void setGameObject(string str,GameObject gameobject){
		_gameobject[str] = gameobject;
	}

	//ブラックボードに新しくオブジェクトを記載する
	public void AddGameObject(string str,GameObject gameobject){
		_gameobject.Add (str, gameobject);
	}

	//ブラックボードに記載されている整数を取得
	public int getInteger(string str){
		return _integer [str];
	}

	//ブラックボードに記載されている整数の書き換え
	public void setInteger(string str,int num){
		_integer [str] = num;
	}

	//ブラックボードに新しく整数を記載する
	public void AddInteger(string str,int num){
		_integer.Add (str, num);
	}

	//ブラックボードに記載されている3次元ベクトルを取得
	public Vector3 getVector3(string str){
		return _vector3 [str];
	}

	//ブラックボードに記載されている3次元ベクトルの書き換え
	public void setVector3(string str,Vector3 num){
		_vector3 [str] = num;
	}

	//ブラックボードに新しく3次元ベクトルを記載する
	public void AddVector3(string str,Vector3 num){
		_vector3.Add (str, num);
	}

	//ブラックボードに記載されている小数を取得
	public float getFraction(string str){
		return _fraction [str];
	}

	//ブラックボードに記載されている小数の書き換え
	public void setFraction(string str,float num){
		_fraction [str] = num;
	}

	//ブラックボードに新しく小数を記載する
	public void AddFraction(string str,float num){
		_fraction.Add(str,num);
	}

	//ブラックボードに記載されているフラグを取得
	public bool getJude(string str){
		return _jude [str];
	}

	//ブラックボードに記載されているフラグの書き換え
	public void setJude(string str,bool flag){
		_jude [str] = flag;
	}

	//ブラックボードに新しくフラグを記載する
	public void AddJude(string str,bool flag){
		_jude.Add (str, flag);
	}


}
