﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TaskOrder : MonoBehaviour {

	public int _taskorder;
	public Condition _condition;
	public List<Task> _tasks;
	public bool issequence = false;

	bool iscoroutines;
	BlackBoard bb;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public bool coroutines{
		get{
			return iscoroutines;
		}
	}

	public void Play(){
		StartCoroutine ("TaskStart");
	}

	public void Stop(){
		StopCoroutine ("TaskStart");
	}

	public BlackBoard blackboard{
		set{
			bb = value;
		}
	}


	IEnumerator TaskStart(){
		//コールチンを開始
		iscoroutines = true;
		//タスクの数
		int count = _tasks.Count;
		//タスクが終わるまで回す
		for (;;) {
			if(Time.timeScale==0)
				yield return null;
				
			//順次呼び出しかどうか
			if (!issequence) {
				//一括呼び出し
				for (int j = 0; j < count; j++) {
					if (_tasks [j]) {
						_tasks [j].PlayTask (bb);
					}
				}
			} else {
				//順次呼び出し
				for (int j = 0; j < count; j++) {
					if (_tasks [j]) {
						for (;;) {
							//処理が終わってなかったら次のフレーム
							if (!_tasks [j].PlayTask (bb)) {
								yield return null;
							} else {
								yield return null;
								break;
							}
						}
					}
				}
			}

			iscoroutines = false;
			yield break;
		}
	}



}
