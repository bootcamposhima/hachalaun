﻿using UnityEngine;
using System.Collections;

public class BulletSarch : Condition {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public override bool PlayCondition (BlackBoard blackboard){

		if (blackboard.getJude ("ismove"))
			return false;

		Vector3 p = blackboard.getVector3 ("position");
		//レイヤーマスクの指定
		int layerMask ;
		RaycastHit hit;

		int hitflag = 0;
		int ctflag = 0;

		Vector3 startpos = new Vector3 (p.x, 1.2f, p.z);

		if (blackboard.getInteger("angle") != 0) {
			//レイヤーマスクのを全て当たる状態を
			//一つだけ当たるようにシフトする
			layerMask = 1 << 12;
			if (Physics.Raycast (startpos, Vector3.left,out hit, 5,layerMask)) {
				hitflag += 1;
				ctflag += 2;
			}
		}
		if (blackboard.getInteger("angle") != 1) {
			//レイヤーマスクのを全て当たる状態を
			//一つだけ当たるようにシフトする
			layerMask = 1 << 13;
			if (Physics.Raycast (startpos, Vector3.right,out hit, 5,layerMask)) {
				hitflag += 2;
				ctflag += 1;
			}
		}
		if (blackboard.getInteger("angle") != 2) {
			//レイヤーマスクのを全て当たる状態を
			//一つだけ当たるようにシフトする
			layerMask = 1 << 14;
			if (Physics.Raycast (startpos, Vector3.back,out hit , 5,layerMask)) {
				hitflag += 4;
				ctflag += 8;
			}
		}
		if (blackboard.getInteger("angle") != 0) {
			//レイヤーマスクのを全て当たる状態を
			//一つだけ当たるようにシフトする
			layerMask = 1 << 15;
			if (Physics.Raycast (startpos, Vector3.forward,out hit, 5,layerMask)) {
				hitflag += 8;
				ctflag += 4;
			}
		}

		if (hitflag != 0) {
			blackboard.setInteger ("hitflag", hitflag);
			blackboard.setInteger ("ctflag", ctflag);
			return true;
		}

		return false;
	}

}
