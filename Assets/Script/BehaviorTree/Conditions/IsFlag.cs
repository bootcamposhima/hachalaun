﻿using UnityEngine;
using System.Collections;

public class IsFlag : Condition {

	[SerializeField]
	string str;

	[SerializeField]
	bool flag;

	// Use this for initialization
	void Start () {
	
	}

	// Update is called once per frame
	void Update () {
	
	}

	public override bool PlayCondition (BlackBoard blackboard){
		if (str.Length == 0)
			return true;
		if (blackboard.getJude (str) == flag)
			return true;

		return false;
	}

}
