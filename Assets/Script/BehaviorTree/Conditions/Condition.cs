﻿using UnityEngine;
using System.Collections;

public abstract class Condition : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public abstract bool PlayCondition (BlackBoard blackboard);

}
