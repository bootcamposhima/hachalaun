﻿using UnityEngine;
using System.Collections;

public class Move : Task {

	bool istart=false;

	GameObject movePar;

	// Use this for initialization
	void Start () {
		movePar = null;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public override bool PlayTask (BlackBoard blackboard){
		return MoveAction (blackboard);
	}

	bool MoveAction(BlackBoard blackboard){
		Vector3 pos = blackboard.getParent().gameObject.transform.localPosition;
		Vector3 position = blackboard.getVector3 ("position");

		float p = 0;

		int movestate =  blackboard.getInteger ("movestate");

		float spd = blackboard.getFraction ("spd");

		float gameSpeed = blackboard.getFraction ("gameSpeed");

		if (movestate==1) {
			if (position.x < pos.x)
				spd *= -1;
		} else if(movestate==2){
			if (position.z < pos.z)
				spd *= -1;
		}

		pos = blackboard.getParent().gameObject.transform.localPosition;
		gameSpeed = blackboard.getFraction ("gameSpeed"); 
		switch (movestate) {
		case 1:
			blackboard.getParent().gameObject.transform.localPosition = new Vector3 (pos.x + spd * gameSpeed, pos.y, pos.z);
			p = position.x - (pos.x + spd * gameSpeed);
			break;
		case 2:
			blackboard.getParent().gameObject.transform.localPosition = new Vector3 (pos.x, pos.y, pos.z + spd * gameSpeed);
			p = position.z - (pos.z + spd * gameSpeed);
			break;

		default:
			break;
		}

		MoveParticle (blackboard);

		if ((p > spd | p > spd * -1) && (p < spd | p < spd * -1)) {
			blackboard.getParent().gameObject.transform.position = position;
			blackboard.setJude("ismove",false);	
			return true;
		}
		return false;
	}

	public void MoveParticle(BlackBoard blackboard){
		if (movePar) 
			FindObjectOfType<ParticleManager> ().ParticleStop (movePar);
		movePar = FindObjectOfType<ParticleManager> ().CharChangeParticleStart (blackboard.getParent().gameObject.transform.position);
		Invoke ("StopMovePar", movePar.GetComponent<ParticleSystem> ().duration);
	}

	public void StopMovePar(){
		FindObjectOfType<ParticleManager> ().ParticleStop (movePar);
		movePar = null;
	}
}
