﻿using UnityEngine;
using System.Collections;

public class CenterMove : Task {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
		
	public override bool PlayTask (BlackBoard blackboard){
		return CenterMovePlace (blackboard);
	}

	bool CenterMovePlace(BlackBoard blackboard){
		Vector3 pos = blackboard.getParent().gameObject.transform.localPosition;

		blackboard.setJude("ismove",true);	

		int movestate = 0;

		if (pos.x == -8.0f) {
			movestate = 1;
			blackboard.setVector3 ("position", new Vector3 (pos.x + 4, pos.y, pos.z));
		} else if(pos.x == 8.0f){
			movestate = 1;
			blackboard.setVector3 ("position", new Vector3 (pos.x - 4, pos.y, pos.z));
		}

		if (pos.z == -8.0f) {
			movestate = 2;
			blackboard.setVector3 ("position", new Vector3 (pos.x, pos.y, pos.z + 4));
		} else if(pos.z == 8.0f){
			movestate = 2;
			blackboard.setVector3 ("position", new Vector3 (pos.x, pos.y, pos.z - 4));
		}

		blackboard.setInteger ("movestate",movestate);

		return true;
	}

//	IEnumerator CenterMoveAction(BlackBoard blackboard){		
//		Vector3 pos = blackboard.getParent().gameObject.transform.localPosition;
//
//		float spd = blackboard.getFraction ("spd");
//
//		float gameSpeed = blackboard.getFraction ("gameSpeed");
//
//		int movestate = 0;
//
//		if (pos.x == -8.0f) {
//			movestate = 1;
//		} else if(pos.x == 8.0f){
//			movestate = 1;
//			spd *= -1;
//		}
//
//		if (pos.z == -8.0f) {
//			movestate = 2;
//		} else if(pos.z == 8.0f){
//			movestate = 2;
//			spd *= -1;
//		}
//
//		if (movestate == 1) {
//			blackboard.setVector3 ("position", new Vector3 (pos.x + 4 * spd, 2.0f, pos.z));
//		} else if (movestate == 2) {
//			blackboard.setVector3 ("position", new Vector3 (pos.x, 2.0f, pos.z + 4 * spd));
//		} else {
//			yield break;
//		}
//
//		Vector3 position = blackboard.getVector3 ("position");
//
//		float p = 0;
//
//
//		for (;;) {
//			pos = blackboard.getParent().gameObject.transform.localPosition;
//			gameSpeed = blackboard.getFraction ("gameSpeed"); 
//			switch (movestate) {
//			case 1:
//				blackboard.getParent().gameObject.transform.localPosition = new Vector3 (pos.x + spd * gameSpeed, pos.y, pos.z);
//				p = position.x - (pos.x + spd * gameSpeed);
//				break;
//			case 2:
//				blackboard.getParent().gameObject.transform.localPosition = new Vector3 (pos.x, pos.y, pos.z + spd * gameSpeed);
//				p = position.z - (pos.z + spd * gameSpeed);
//				break;
//
//			default:
//				break;
//			}
//
//			if ((p > spd | p > spd * -1) && (p < spd | p < spd * -1)) {
//				blackboard.getParent().gameObject.transform.position = position;
//				break;
//			}
//			yield return null;
//		}
//
//
//	}


}
