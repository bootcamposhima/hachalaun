﻿using UnityEngine;
using System.Collections;

public class SearchMoveTask : Task {

	bool test=false;

	//方向ビット
	const int eastbit = 1;
	const int westbit = 2;
	const int southbit = 4;
	const int northit = 8;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public override bool PlayTask (BlackBoard blackboard){

		blackboard.setJude("ismove",true);	

		Vector3 position = blackboard.getVector3 ("position");

		//当たりを検出したかどうか
		bool ism = false;
		//真ん中
		int center=0;
		//移動方向
		int s = 1;
		//端っこ判定
		int dnm = 0;
		//真ん中から遠い方検出

		getDnm (position,ref center,ref dnm);

		int angle = 0;

		int _movestate = 0;

		int hitflag = blackboard.getInteger ("hitflag");
		int ctflag = blackboard.getInteger ("ctflag");

		//移動できる方向検出
		if (FlagJude(hitflag , eastbit) && FlagJude(dnm , eastbit)) {
			_movestate = 1;
			s = -1;
			angle = 0;
			if (FlagJude(ctflag , eastbit)) {
				ism = true;
			}
		} 

		if (FlagJude(hitflag , westbit) && FlagJude(dnm , westbit)) {

			if ((FlagJude (ctflag, westbit) && FlagJude (center, westbit)) && !ism) {
				_movestate = 1;
				s = 1;
				angle = 1;

				if (FlagJude (ctflag, westbit)) {
					ism = true;
				}
			}
		}

		if (FlagJude(hitflag , southbit) && FlagJude(dnm , southbit)) {
			if (!ism) {
				_movestate = 2;
				s = -1;
				angle = 2;
				if (FlagJude (ctflag, southbit)) {
					ism = true;
				}
			} else {
				if (FlagJude (ctflag, southbit)&&FlagJude (center, southbit)) {
					_movestate = 2;
					s = -1;
					angle = 2;
					ism = true;
				}
			}
		} 

		if (FlagJude(hitflag , northit) && FlagJude(dnm , northit)) {
			if (!ism) {
				_movestate = 2;
				s = 1;
				angle = 3;
				if (FlagJude (ctflag, northit)) {
					ism = true;
				}
			} else {
				if (FlagJude (ctflag, northit)&&FlagJude (center, northit)) {
					_movestate = 2;
					s = 1;
					angle = 3;
					ism = true;
				}
			}
		}
			
		blackboard.setInteger ("movestate",_movestate);

		PlayerRotatin (angle,blackboard);

		//移動量
		const int movenum = 4;

		//移動場所検知
		switch (_movestate) {
		case 0:
			break;

		case 1:
			blackboard.setVector3 ("position", new Vector3 (position.x + movenum * s, position.y, position.z));
			break;

		case 2:
			blackboard.setVector3 ("position", new Vector3 (position.x, position.y, position.z + movenum * s));
			break;

		default:
			break;
		}

		return true;
	}

	bool FlagJude(int flag1,int flag2){
		return (flag1 & flag2) != flag2;
	}


	void getDnm(Vector3 pos, ref int center, ref int dnm){
		//真ん中から遠い方検出
		if (pos.x < 0) {
			center += westbit;
			//端っこかどうか
			if (pos.x <= -5)
				dnm += eastbit;
		}else if (pos.x > 0) {
			center += eastbit;
			//端っこかどうか
			if(pos.x >= 5)
				dnm += westbit;
		}
		if (pos.z < 0) {
			center += southbit;
			//端っこかどうか
			if (pos.z <= -5)
				dnm += southbit;
		}else if (pos.z > 0) {
			center += northit;
			//端っこかどうか
			if(pos.z >= 5)
				dnm += northit;
		}
	}

	void PlayerRotatin(int angle,BlackBoard blackboard){
		float roll = 90;
		blackboard.setInteger("angle",angle);
		switch (angle) {
		case 0:
			blackboard.getParent().transform.eulerAngles = new Vector3 (0, -roll, 0);
			break;
		case 1:
			blackboard.getParent().transform.eulerAngles = new Vector3 (0, roll, 0);
			break;
		case 2:
			blackboard.getParent().transform.eulerAngles = new Vector3 (0, roll * 2, 0);
			break;
		case 3:
			blackboard.getParent().transform.eulerAngles = Vector3.zero;
			break;

		}
	}


}
