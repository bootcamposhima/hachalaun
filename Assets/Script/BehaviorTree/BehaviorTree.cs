﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class BehaviorTree : MonoBehaviour {

	[SerializeField]
	List<TaskOrder> _taskorder;

	List<TaskOrder> _cleantaskorder;

	int _count;

	// Use this for initialization
	void Start () {

		//Play ();

	}

	void Awake(){
		_cleantaskorder = new List<TaskOrder> ();
		_count = _taskorder.Count;

		for (int i=0; i<_count; i++) {

			for (int j = 0; j < _count; j++) {
				if (_taskorder [j]._taskorder == i) {
					_cleantaskorder.Add (_taskorder [j]);
					_taskorder [j].blackboard = this.GetComponent<BlackBoard> ();
				}
			}

			if (_cleantaskorder.Count == _count)
				break;
		}
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void Play(){
		StartCoroutine ("AIPlay");
	}

	public void Stop(){
		StopCoroutine ("AIPlay");  
	}

	IEnumerator AIPlay(){
		for (;;) {
			//タイムスケールが0なら回さない
			if(Time.timeScale==0)
				yield return null;
			//TaskOrderのぶん回す
			for (int i = 0; i < _count; i++) {
				//条件にあったら
				if (_cleantaskorder [i]._condition.PlayCondition (this.GetComponent<BlackBoard> ())) {
					//検出したところから最後まで
					for (int j = i; j < _count; j++) {
						//最初のやつでコールチンが動いていないなら
						if (j == i && !_cleantaskorder [j].coroutines) {
							_cleantaskorder [j].Play ();
						} else if (_cleantaskorder [j].coroutines) {
							//その番号から後ろのやつが動いていたら
							_cleantaskorder [j].Stop ();
						}
					}
					break;
				}
			}
	
			yield return null;
		}
	}

}