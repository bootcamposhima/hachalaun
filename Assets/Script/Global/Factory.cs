﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Factory : SingletonMonoBehaviour<Factory> {

	[SerializeField]
	GameObject[] _bullets;
	[SerializeField]
	GameObject[] _charactors;

	[SerializeField]
	Sprite[] _charactorspritedatas;
	[SerializeField]
	Sprite[] _bulletspritedatas;

	List<List<GameObject>> _bullet = new List<List<GameObject>> ();

	bool isbulletstop;

	// Use this for initialization
	void Start () {
		isbulletstop = false;
		int bulletnum = _bullets.Length;
		for (int i = 0; i < bulletnum; i++) {
			_bullet.Add (new List<GameObject> ());
		}
	}

	void Awake()
	{
		if (this != Instance)
		{
			Destroy(this);
			return;
		}

		// 新しいシーンを読み込んでもオブジェクトが自動で破壊されないように設定
		DontDestroyOnLoad(this.gameObject);
	}


	// Update is called once per frame
	void Update () {
	
	}

	public GameObject CreateBullet(int num)
	{
		GameObject b = getBullet (_bullet[num]);

		if (!b) {
			b = Instantiate (_bullets [num]);
			_bullet[num].Add (b);
			b.GetComponent<Bullet> ().Stop = isbulletstop;
		}
		return b;
	}

	public GameObject CreateCharactor(int num)
	{
		return _charactors[num];
	}


	private GameObject getBullet(List<GameObject> objects)
	{
		int num = objects.Count;
		for (int i = 0; i < num; i++) {
			if (objects [i] != null) {
				if (!objects [i].GetComponent<Bullet> ().Flyweight)
					return objects [i];
			} else {
				objects.Remove (objects [i]);
			}
		}
		return null;
	}

	public Sprite GetCharactorSprite(int num){
		return _charactorspritedatas [num];
	}

	public Sprite GetBulletSprite(int num){
		return _bulletspritedatas [num];
	}

	public void BulletisStop(bool stop){
		isbulletstop = stop;
		int n = _bullet.Count;
		for (int j = 0; j < n; j++) {
			List<GameObject> bob = _bullet [j];
			for (int i = 0; i < bob.Count; i++) {
				if (bob [i] != null) {
					bob [i].GetComponent<Bullet> ().Stop = isbulletstop;
				}
			}
		}
	}

	public void BulletFlyweight(bool flag){
		int n = _bullet.Count;
		for (int j = 0; j < n; j++) {
			List<GameObject> bob = _bullet [j];
			for (int i = 0; i < bob.Count; i++) {
				if (bob [i] != null) {
					bob [i].GetComponent<Bullet> ().Flyweight = flag;
				}
			}
		}
	}

	public void Reset(){
		for (int i = 0; i < _bullet.Count; i++) {
			_bullet [i].Clear ();
		}
	}
}