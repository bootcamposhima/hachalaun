﻿using UnityEngine;
using System.Collections;
using System.IO;

public class TextLodaer : SingletonMonoBehaviour<TextLodaer> {

	//ファイルの位置
	string m_filepas;

	// Use this for initialization
	void Start () {
	}

	void Awake()
	{
		if (this != Instance)
		{
			Destroy(this);
			return;
		}

		// 新しいシーンを読み込んでもオブジェクトが自動で破壊されないように設定
		DontDestroyOnLoad(this.gameObject);

		m_filepas=Application.streamingAssetsPath+"/";
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public string ReadTextFile(string filename){
		//ファイルを開く
		StreamReader strre = new StreamReader (m_filepas + filename);	
		//末尾まで読み込み
		string memstr = strre.ReadToEnd ();
		//ファイルを閉じる
		strre.Close ();
		//読み込んだ文字列を返す
		return memstr;
	}

	public string ReadTextFile2(string filename){
		//ファイルを開く
		StreamReader strre = new StreamReader (filename);	
		//末尾まで読み込み
		string memstr = strre.ReadToEnd ();
		//ファイルを閉じる
		strre.Close ();
		//読み込んだ文字列を返す
		return memstr;
	}
		
	public void WriteTextFile(string filename,string jstr){
		//ファイルを開く
		StreamWriter strwr = new StreamWriter (m_filepas + filename,false);	
		//書き込み
		strwr.Write(jstr);
		//ファイルを閉じる
		strwr.Close();
	}

	public void WriteTextFile2(string filename,string jstr){
		//ファイルを開く
		StreamWriter strwr = new StreamWriter (filename,false);	
		//書き込み
		strwr.Write(jstr);
		//ファイルを閉じる
		strwr.Close();
	}
		
}
