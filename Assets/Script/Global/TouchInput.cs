﻿// タッチマネジャー
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class TouchInput : MonoBehaviour {

	// シングルトン
	private static TouchInput _instance;

	public bool active = true;

	//string _scene;

	// インスタンスの取得
	public static TouchInput GetInstance
	{
		get {
			// インスタンスが存在しない場合
			if (_instance == null) {
				// 生成
				_instance = GameObject.FindObjectOfType<TouchInput> ();
			}

			return _instance;
		}
	}

	// スタートする前に最初に呼び出される
	void Awake()
	{
		if (_instance == null) 
		{
			// 最初のインスタンスの場合、シングルトンを行う
			_instance = this;

			this.name = this.GetType ().ToString ();
		}
		else
		{
			// シングルトンがすでに存在していて、シーンないの別の参照を見つけた場合
			// それを破壊する
			if (this != _instance)
				Destroy (this.gameObject);
		}
	}

	// 初期化
	void Start()
	{
		//_scene = SceneManager.GetActiveScene ().name;
	}

	// 更新処理
	void Update()
	{
		#if UNITY_EDITOR
		// PC用
		// マウスの左クリック
		if(Input.GetMouseButtonDown(0))
		{
			RaycastDetectionPC();
		}
		#endif

		#if UNITY_STANDALONE
		if(Input.GetMouseButtonDown(0))
		{
			if (SceneManager.GetActiveScene ().name == "Practice")
				RaycastDetectionPC();
		}
		#endif

		#if UNITY_IOS
		if(Input.touchCount > 0)
		{
			Touch touch = Input.GetTouch(0);
			if(touch.phase == TouchPhase.Began)
			{
				// タッチ開始
				RaycastDetectionMobile();
			}
		}
		#endif 

		#if UNITY_ANDROID
		// android用
		// タッチの検出

		#endif
	}

	public void SetTouchActive(bool active)
	{
		this.active = active;
	}

	public void SetTouchNotActive(bool active)
	{
		this.active = false;
	}

	void SceneTouchInput()
	{
		//GetComponent<AudioSource> ().PlayOneShot (m_se);
		//Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition + Camera.main.transform.forward * 10);

//		switch (_scene) {
//		case "AtelierFarm":
//			RaycastDetectionPC ();
//			break;
//
//		case "fusion_room":
//			break;
//		}
	}

	// PC用
	void RaycastDetectionPC()
	{
		// 現在のマウスの位置にRayを作成
		Ray theRay = Camera.main.ScreenPointToRay (Input.mousePosition);
		// オブジェクトデータを格納する
		RaycastHit hitInfo = new RaycastHit ();
		//レイヤーマスク
		int layerMask = 1 << LayerMask.NameToLayer("Panel");

		if (Physics.Raycast (theRay, out hitInfo, Mathf.Infinity,layerMask)) {
			ButtonEvent bt = hitInfo.collider.GetComponent<ButtonEvent> ();
			bt.EventObject.GetComponent<AICreaterManager> ().TopPositionNum (bt.EventNum);
		}
	}

	// Android用
	void RaycastDetectionMobile()
	{
		// 現在のタッチの位置にRayを作成
		Ray theRay = Camera.main.ScreenPointToRay (Input.GetTouch(0).position);

		// オブジェクトデータを格納する
		RaycastHit hitInfo = new RaycastHit ();

		//レイヤーマスク
		int layerMask = 1 << LayerMask.NameToLayer("Panel");

		if (Physics.Raycast (theRay, out hitInfo, Mathf.Infinity,layerMask)) {
			ButtonEvent bt = hitInfo.collider.GetComponent<ButtonEvent> ();
			bt.EventObject.GetComponent<AICreaterManager> ().TopPositionNum (bt.EventNum);
		}
	}

	// android用　レイヤーマスクver
	void RaycastDetectionMobileWithLayerMask()
	{
		// 現在のタッチの位置にRayを作成
		Ray theRay = Camera.main.ScreenPointToRay (Input.GetTouch(0).position);

		// オブジェクトデータを格納する
		RaycastHit hitInfo = new RaycastHit ();

		// 範囲なしでゲーム内にレイキャストを送る
		if (Physics.Raycast (theRay, out hitInfo, Mathf.Infinity)) 
		{
			
		}
	}

}