﻿using UnityEngine;
using System.Collections;

public class SoundManager : SingletonMonoBehaviour<SoundManager>{
	// BGMdeta
	[SerializeField]
	AudioClip PlaySound;
	[SerializeField]
	AudioClip GameSound;
	[SerializeField]
	AudioClip WinSound;
	[SerializeField]
	AudioClip LossSound;

	// SEdata
	[SerializeField]
	AudioClip SE1;
	[SerializeField]
	AudioClip SE2;
	[SerializeField]
	AudioClip LossSE;
	[SerializeField]
	AudioClip WinSE;
	[SerializeField]
	AudioClip BomSE;
	[SerializeField]
	AudioClip FlySE;

	AudioSource[] _audios;

	// ******シングルトン******
	void Awake()
	{
		if (this != Instance)
		{
			Destroy(this);
			return;
		}

		// 新しいシーンを読み込んでもオブジェクトが自動で破壊されないように設定
		DontDestroyOnLoad(this.gameObject);
	}


	// 初期化
	void Start () 
	{
		_audios = this.GetComponents<AudioSource> ();
		_audios[0].clip = GameSound;
		_audios [0].playOnAwake = true;
		_audios [0].loop = true;
		_audios [0].volume = 0.5f;
		_audios[0].Play ();
		_audios [1].playOnAwake = false;
		_audios [1].loop = false;
	}

	public void StartGameSound(){
		_audios[0].clip = GameSound;
		_audios[0].Play ();
	}

	public void StartPlaySound(){
		_audios[0].clip = PlaySound;
		_audios[0].Play ();
	}

	public void StartWinSound(){
		_audios[0].clip = WinSound;
		_audios[0].Play ();
	}

	public void StartLossSound(){
		_audios[0].clip = LossSound;
		_audios[0].Play ();
	}

	public void SEShotPlay(){
		_audios [1].clip = SE1;
		_audios[1].PlayOneShot (SE1);
	}

	public void SEButtonPlay(){
		_audios [1].clip = SE2;
		_audios[1].PlayOneShot (SE2);
	}

	public void SELossPlay(){
		_audios [1].clip = LossSE;
		_audios[1].PlayOneShot (LossSE);
	}

	public void SEWinPlay(){
		_audios [1].clip = WinSE;
		_audios[1].PlayOneShot (WinSE);
	}

	public void SEBomPlay(){
		_audios [1].clip = BomSE;
		_audios[1].PlayOneShot (BomSE);
	}

	public void SEFiringPlay(){
		_audios [1].clip = FlySE;
		_audios[1].PlayOneShot (FlySE);
	}
}
