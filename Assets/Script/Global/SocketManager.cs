﻿using UnityEngine;
using System.Collections;
using SocketIO;

public abstract class SocketManager : MonoBehaviour {

	protected FightSocket socket;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
		
	public abstract void Online (SocketIOEvent e);
		
	public abstract void Join(SocketIOEvent e);

	public abstract void Offline (SocketIOEvent e);

	public abstract void Message(SocketIOEvent e);

	public abstract void Onlinenotification(SocketIOEvent e);

	public abstract void Endnotification(SocketIOEvent e);

	public abstract void Error(SocketIOEvent e);

}
