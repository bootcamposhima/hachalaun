﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class StageManager : SingletonMonoBehaviour<StageManager>{

	bool _isBattleScenePlaying;

	/// <summary>
	/// 初期化
	/// </summary>
	void Start () {
		_isBattleScenePlaying = false;
	}

	/// <summary>
	/// Awake
	/// </summary>
	void Awake()
	{
		if (this != Instance)
		{
			Destroy(this);
			return;
		}

		// 新しいシーンを読み込んでもオブジェクトが自動で破壊されないように設定
		DontDestroyOnLoad(this.gameObject);
	}

	/// <summary>
	/// Scene遷移ボタンが押された処理
	/// </summary>
	/// <param name="str">String.</param>
	public void OnClickStageButton(string str)
	{
		if (str == "yk_test") {
			FindObjectOfType<SoundManager> ().StartPlaySound ();
			_isBattleScenePlaying = true;
		} else if (_isBattleScenePlaying == true) {
			FindObjectOfType<SoundManager> ().StartGameSound ();
			_isBattleScenePlaying = false;
		}
		FindObjectOfType<TransitionAnimation> ().StartSceneTransition (str, 0.3f);
	}

}
