﻿using UnityEngine;
using System.Collections;

public class ActionManager : SingletonMonoBehaviour<ActionManager> {

	IEnumerator _moveforever;

	void Awake()
	{
		if (this != Instance)
		{
			Destroy(this);
			return;
		}

		// 新しいシーンを読み込んでもオブジェクトが自動で破壊されないように設定
		DontDestroyOnLoad(this.gameObject);
	}


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	//移動アクション開始
	public void StartMoveAction(GameObject _ob,Vector3 destination,float deltatime){
		StartCoroutine (MoveAction (_ob, destination, deltatime));
	}

	public void StartMoveForeverAction(GameObject _ob,Vector3 destination,float deltatime,float waittime=0){
		_moveforever = ForeverMoveAction (_ob, destination, deltatime, waittime);
		StartCoroutine (_moveforever);
	}

	public void StopMoveForeverAction(){
		if(_moveforever!=null)
			StopCoroutine (_moveforever);
	}

	IEnumerator ForeverMoveAction(GameObject _ob,Vector3 destination,float deltatime,float waittime=0){
		Vector2 defaultpos = _ob.transform.localPosition;
		for (;;) {
			yield return StartCoroutine (MoveAction (_ob, destination, deltatime));
			yield return new WaitForSeconds (waittime);
			yield return StartCoroutine (MoveAction (_ob, defaultpos, deltatime));
			yield return new WaitForSeconds (waittime);
		}
	}

	//移動アクション
	public static IEnumerator MoveAction(GameObject _ob,Vector3 destination,float deltatime){
		Vector3 p =destination- _ob.gameObject.transform.localPosition;

		float spdx = p.x / deltatime;
		float spdy = p.y / deltatime;
		float spdz = p.z / deltatime;

		Vector3 mp;

		for (;;) {

			p = destination - _ob.transform.localPosition;
			mp = Vector3.zero;

			if (!(p.x >= -spdx && p.x <= spdx||
				p.x >= spdx && p.x <= -spdx))
				mp.x = spdx;
			if (!(p.y >= -spdy && p.y <= spdy||
				p.y >= spdy && p.y <= -spdy))
				mp.y = spdy;
			if (!(p.z >= -spdz && p.z <= spdz||
				p.z >= spdz && p.z <= -spdz))
				mp.z = spdz;

			if (mp == Vector3.zero) {
				_ob.transform.localPosition = destination;
				yield break;
			} else {
				_ob.transform.localPosition += mp;
			}
			yield return null;
		}
	}

	//回転アクション開始
	public void StartRotateAction(GameObject _ob,Vector3 rotate,float deltatime){
		StartCoroutine (RotateAction (_ob, rotate, deltatime));
	}

	//回転アクション
	public static IEnumerator RotateAction(GameObject _ob,Vector3 rotate,float deltatime){
		
		Vector3 p = rotate - _ob.transform.localEulerAngles;

		float spdx = p.x / deltatime;
		float spdy = p.y / deltatime;
		float spdz = p.z / deltatime;

		Vector3 mp;

		for (;;) {
			
			p = rotate - _ob.transform.localEulerAngles;
			mp = Vector3.zero;

			if (!(p.x >= -spdx && p.x <= spdx||
				p.x >= spdx && p.x <= -spdx))
				mp.x = spdx;
			if (!(p.y >= -spdy && p.y <= spdy||
				p.y >= spdy && p.y <= -spdy))
				mp.y = spdy;
			if (!(p.z >= -spdz && p.z <= spdz||
				p.z >= spdz && p.z <= -spdz))
				mp.z = spdz;
		
			if (mp == Vector3.zero) {
				_ob.transform.localEulerAngles = rotate;
				yield break;
			} else {
				_ob.transform.localEulerAngles += mp;
			}
			yield return null;
		}
	}

	public bool UIMove (GameObject _ob,Vector3 destination,ref float spd){
		float x = _ob.gameObject.transform.localPosition.x - destination.x;

		float num;
		if (spd < 0)
			num = spd / 2 * -1;
		else
			num = spd / 2;

		if (x > -num && x < num) {
			_ob.gameObject.transform.localPosition = destination;
			return false;
		} else {
			Vector3 pos = _ob.gameObject.transform.localPosition;
			_ob.gameObject.transform.localPosition = new Vector3 (pos.x + spd, pos.y, pos.z);
			return true;
		}
	}
}
