﻿using UnityEngine;
using System.Collections;

public class StartScaleAnimation : MonoBehaviour {

	// Use this for initialization
	void Start () {
		TransitionAnimation.Instance.StartScaleAnimation (this.gameObject, 1.2f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
