﻿using UnityEngine;
using System.Collections;
using LitJson;

public class IDManager : SingletonMonoBehaviour<IDManager> {

	FixedID fid;

	// Use this for initialization
	void Start () {
		
	}

	void Awake()
	{
		if (this != Instance)
		{
			Destroy(this);
			return;
		}

		// 新しいシーンを読み込んでもオブジェクトが自動で破壊されないように設定
		DontDestroyOnLoad(this.gameObject);

//		//ID読み込みまたは作成
//		TextLodaer txl = TextLodaer.Instance;
//		string filepath = Application.persistentDataPath + "/Hidamari2ID";
//		if (System.IO.File.Exists (filepath)) {
//			string jstr = txl.ReadTextFile2 (filepath);
//			fid = LitJson.JsonMapper.ToObject<FixedID> (jstr);
//		} else {
//			string id = Random.Range (0, 9).ToString();
//
//			for (int i = 0; i < 7; i++) {
//				id=id.ToString()+Random.Range (0, 9).ToString();
//			}
//
//			fid = new FixedID (id.ToString (), "p");
//			txl.WriteTextFile2 (filepath, LitJson.JsonMapper.ToJson (fid));
//		}

		string id = Random.Range (0, 9).ToString();

		for (int i = 0; i < 7; i++) {
			id=id.ToString()+Random.Range (0, 9).ToString();
		}

		fid = new FixedID (id.ToString (), "p");
	}

	
	// Update is called once per frame
	void Update () {
	
	}

	//自分のID
	public FixedID FID{
		get{
			return fid;
		}
	}

	public string ID{
		get{
			return fid.id;	
		}
	}

	public string playername{
		get{
			return fid.name;	
		}
	}

}

public struct FixedID{
	public string id;
	public string name;
	//public AIDatas value;

	public FixedID(string i, string n){
		id=i;
		name="Player";
	}
}