﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

// ********** ********** ********** ********** **********
// フェードアニメーション
// ********** ********** ********** ********** **********

public class TransitionAnimation : SingletonMonoBehaviour<TransitionAnimation>
{
	/// <summary>
	/// シーン遷移で使う
	/// </summary>
	[SerializeField]
	Texture2D _sceneTransitionTex;
	float     _sceneTransitionAlpha;
	Vector3   _sceneTransitionColor;
	bool      _isPlayingAnimation;

	/// <summary>
	/// 拡大縮小アニメーションで使う
	/// </summary>
	List<GameObject>  _scaleObjects;
	List<IEnumerator> _scaleIEnumerators;
	IEnumerator       _allScaleAnimationIEnumerator;

	void Awake()
	{
		// ゲーム全体で使うアニメーション関連なので消えないようにする
		if (this != Instance)
		{
			Destroy(this);
			return;
		}
		DontDestroyOnLoad(this.gameObject);

		_sceneTransitionAlpha         = 0;
		_sceneTransitionColor         = Vector3.zero;
		_isPlayingAnimation           = false;
		_scaleObjects                 = new List<GameObject>  ();
		_scaleIEnumerators            = new List<IEnumerator> ();
		_allScaleAnimationIEnumerator = null;
	}

    /// <summary>
    /// GUI event.
    /// </summary>
    public void OnGUI()
    {
		if (!_isPlayingAnimation)
			return;
		// 透明度を更新、テクスチャを描画
		GUI.color = new Color(_sceneTransitionColor.x, _sceneTransitionColor.y, _sceneTransitionColor.z, _sceneTransitionAlpha);
		GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), _sceneTransitionTex);
	}

    /// <summary>
	/// シーン遷移を始める
    /// </summary>
	public void StartSceneTransition(string nextScene, float interval)
	{
		if (SceneManager.GetActiveScene ().name != nextScene)
			StartCoroutine (StartFadeAnimation (() => {
				SceneManager.LoadScene (nextScene);
			}, interval));
    }

    /// <summary>
	/// フェードアニメーション中にゲームオブジェクトを消す
	/// </summary>
	public void HideObjectBetweenFade(GameObject targetGameObject, float interval)
	{
		StartCoroutine (StartFadeAnimation (() => {
			targetGameObject.SetActive (false);
		}, interval));
    }

	/// <summary>
	/// 画面全体でフェードインフェードアウト中に固有処理をする
	IEnumerator StartFadeAnimation(Action OnFadeProcess,float interval)
	{
		_isPlayingAnimation = true;
		_sceneTransitionColor = Vector3.one;
		// フェードアウト
		yield return StartCoroutine(FadeScreen (interval, false));
		// 固有処理
		OnFadeProcess();
		// フェードイン
		yield return StartCoroutine(FadeScreen (interval, true));
		_isPlayingAnimation = false;
	}

	/// <summary>
	/// フェードアニメーション
	/// </summary>
	public IEnumerator FadeScreen(float interval, bool isFadeIn){
		// 値の設定
		float minimum = (isFadeIn)  ? 1.0f : 0.0f;
		float maximum = (!isFadeIn) ? 1.0f : 0.0f;

		// フェードアニメーション
		float elapsedTime = 0;
		while (elapsedTime <= interval)
		{
			_sceneTransitionAlpha = Mathf.Lerp(minimum, maximum, elapsedTime / interval);
			elapsedTime          += Time.deltaTime;
			yield return 0;
		}
	}

// ********** ********** ********** ********** **********
// 拡縮アニメーション(ALL)
// ********** ********** ********** ********** **********

	/// <summary>
	/// 登録された全てのオブジェクトの拡縮アニメーションを始める
	/// </summary>
	public void StartAllScaleAnimation(float maxScale,float minScale=1.0f,float diffScale=0.01f){
		// アニメーションを止める
		StopAllScaleAnimations ();
		// アニメーションを始める
		_allScaleAnimationIEnumerator = AllScaleAction (maxScale, minScale, diffScale);
		StartCoroutine (_allScaleAnimationIEnumerator);
	}

	/// <summary>
	/// 登録された全てのオブジェクトの拡縮アニメーションを止める
	/// </summary>
	public void StopAllScaleAnimations(){
		for (int i = 0; i < _scaleIEnumerators.Count; i++) {
			StopCoroutine (_scaleIEnumerators [i]);
		}
		_scaleIEnumerators.Clear ();

		if (_allScaleAnimationIEnumerator != null) {
			StopCoroutine (_allScaleAnimationIEnumerator);
			_allScaleAnimationIEnumerator = null;
		}
	}

	/// <summary>
	/// 登録された全てのオブジェクトの拡縮アニメーションを実行
	/// </summary>
	IEnumerator AllScaleAction(float maxScale,float minScale,float diffScale){
		// アニメーションの実行
		for (int i = 0; i < _scaleObjects.Count; i++) {
			StartScaleAnimation(_scaleObjects [i], maxScale, minScale, diffScale);
		}

		// 全ての登録されたオブジェクトがnullだったらやめる
		while(true) {
			if (_scaleObjects.TrueForAll(null)) {
				ClearAllScaleAnimationList ();
				_allScaleAnimationIEnumerator = null;
				break;
			}

			yield return null;
		}
	}

	/// <summary>
	/// 登録された全ての拡縮アニメーションが現在動いているか
	/// </summary>
	public bool IsAllScaleAnimationExecuting(){
		return _allScaleAnimationIEnumerator != null;
	}

// ********** ********** ********** ********** **********
// 拡縮アニメーション(単独)
// ********** ********** ********** ********** **********

	/// <summary>
	///　指定のオブジェクトの拡縮アニメーションを始める
	/// </summary>
	public void StartScaleAnimation(GameObject targetGameObject,float maxScale,float minScale=1.0f,float diffScale=0.01f){
		// ターゲットが登録されていない時:登録
		AddScaleAnimationGameObject(targetGameObject);
		IEnumerator scaleActionProcess = ScaleAction (targetGameObject, maxScale, minScale, diffScale);
		_scaleIEnumerators.Add (scaleActionProcess);
		StartCoroutine (scaleActionProcess);
	}

	/// <summary>
	/// 指定のオブジェクトの拡縮アニメーションを止める
	/// </summary>
	public void StopScaleAnimation(GameObject targetGameObject){
		if (!_scaleObjects.Contains (targetGameObject))
			return;
		// アニメーションを止める
		int indexTarget = _scaleObjects.IndexOf (targetGameObject);
		StopCoroutine (_scaleIEnumerators[indexTarget]);
		// listから削除
		RemoveScaleAnimationGameObject(targetGameObject);
	}

	/// <summary>
	/// 拡縮アニメーション
	/// </summary>
	IEnumerator ScaleAction(GameObject targetGameObject,float maxScale,float minScale,float diffScale){
		float currentScale = minScale;
		bool  isScaleUp    = true;

		while(true) {
			if (targetGameObject == null) {
				break;
			}

			if (isScaleUp) {
				currentScale += diffScale;
				isScaleUp = currentScale <= maxScale;
			} else {
				currentScale -= diffScale;
				isScaleUp = currentScale <= minScale;
			}

			targetGameObject.transform.localScale = Vector3.one * currentScale;

			yield return null;
		}
	}

// ********** ********** ********** ********** **********
// 拡縮アニメーション(リスト処理)
// ********** ********** ********** ********** **********

	/// <summary>
	/// スケールのアニメーションリストに登録する
	/// </summary>
	public void AddScaleAnimationGameObject(GameObject targetGameObject){
		if (!_scaleObjects.Contains (targetGameObject))
			_scaleObjects.Add (targetGameObject);
	}

	/// <summary>
	/// スケールのアニメーションリストから削除する
	/// </summary>
	public void RemoveScaleAnimationGameObject(GameObject targetGameObject){
		if (_scaleObjects.Contains (targetGameObject)) {
			int indexTarget = _scaleObjects.IndexOf (targetGameObject);
			_scaleObjects     .RemoveAt (indexTarget);
			_scaleIEnumerators.RemoveAt (indexTarget);
		}
	}

	/// <summary>
	/// 全ての登録されているアニメーションリストを消す
	/// </summary>
	public void ClearAllScaleAnimationList(){
		_scaleObjects     .Clear ();
		_scaleIEnumerators.Clear ();
	}
}