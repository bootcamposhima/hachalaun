﻿using UnityEngine;
using System.Collections;

public class ParticleManager : SingletonMonoBehaviour<ParticleManager> {

	[SerializeField]
	GameObject p_sortiePar;
	[SerializeField]
	GameObject p_admissionPar;
	[SerializeField]
	GameObject p_lossPar;
	[SerializeField]
	GameObject p_winPar;
	[SerializeField]
	GameObject p_changePar;
	[SerializeField]
	GameObject p_damegePar;
	[SerializeField]
	GameObject p_bloodPar;
	[SerializeField]
	GameObject p_shotPar;
	[SerializeField]
	GameObject p_explosionsPar;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void Awake()
	{
		if (this != Instance)
		{
			Destroy(this);
			return;
		}

		// 新しいシーンを読み込んでもオブジェクトが自動で破壊されないように設定
		DontDestroyOnLoad(this.gameObject);
	}

	public GameObject SoriteParticleStart(Vector3 pos){
		GameObject par = Instantiate (p_sortiePar);
		par.transform.position = pos;
		par.GetComponent<ParticleSystem> ().Play ();

		return par;
	}

	public GameObject AdmissionParticleStart(Vector3 pos){
		GameObject par = Instantiate (p_admissionPar);
		par.transform.position = pos;
		par.GetComponent<ParticleSystem> ().Play ();

		return par;
	}

	public GameObject LossParticleStart(Vector3 pos){
		GameObject par = Instantiate (p_lossPar);
		par.transform.position = pos;
		par.GetComponent<ParticleSystem> ().Play ();

		return par;
	}

	public GameObject WinParticleStart(Vector3 pos){
		GameObject par = Instantiate (p_winPar);
		par.transform.position = pos;
		par.GetComponent<ParticleSystem> ().Play ();

		return par;
	}

	public GameObject CharChangeParticleStart(Vector3 pos){
		GameObject par = Instantiate (p_changePar);
		par.transform.position = pos;
		par.GetComponent<ParticleSystem> ().Play ();

		return par;
	}

	public GameObject DamegeParticleStart(Vector3 pos){
		GameObject par = Instantiate (p_damegePar);
		par.transform.position = pos;
		par.GetComponent<ParticleSystem> ().Play ();

		return par;
	}

	public GameObject BloodParticleStart(Vector3 pos){
		GameObject par = Instantiate (p_bloodPar);
		par.transform.position = pos;
		par.GetComponent<ParticleSystem> ().Play ();

		return par;
	}

	public GameObject ShotParticleStart(Vector3 pos){
		GameObject par = Instantiate (p_shotPar);
		par.transform.position = pos;
		par.GetComponent<ParticleSystem> ().Play ();

		return par;
	}

	public GameObject ExplosionsParticleStart(Vector3 pos){
		GameObject par = Instantiate (p_explosionsPar);
		par.transform.position = pos;
		par.GetComponent<ParticleSystem> ().Play ();

		return par;
	}

	public void ParticleStop(GameObject par){
		if (par) {
			par.GetComponent<ParticleSystem> ().Stop ();
			Destroy (par);
		}
	}
}
