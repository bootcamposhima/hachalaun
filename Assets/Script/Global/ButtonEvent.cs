﻿using UnityEngine;
using System.Collections;

public class ButtonEvent : MonoBehaviour {

	[SerializeField]
	GameObject _object;
	[SerializeField]
	int num;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public GameObject EventObject{
		get{
			return _object;
		}
	}

	public int EventNum{
		get{
			return num;
		}
	}
}
