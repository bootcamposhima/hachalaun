﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


/// <summary>
/// Android blue tooth adapter.
/// Author Kazuki Ito
/// </summary>

public class AndroidBlueToothAdapter : MonoBehaviour {
	
	// datatype
	const int INT = 0;
	const int FLOAT = 1;
	const int STRING = 2;

	//	dataIndex
	const int DATATYPE = 0;
	const int DATA = 1;
	const int TAG = 2;


	// ClickTag
	const int CONNECT = 0;
	const int DISCONNECT = 1;

	AndroidJavaObject cls;
	bool serverFlag = false;
	bool retryRequest = false;

	void Update ()
	{
		if (!isConnected ()) 
		{
			serverFlag = false;
		}
	}



	public void Create () {
#if UNITY_ANDROID && !UNITY_EDITOR
		cls = new AndroidJavaObject("com.framework.bluetooth.P2P");
		cls.Call ("setGameObject", gameObject.name);
		serverFlag = false;

		//cls.Call ("BlueToothEnable");
#endif
	}
	
	public void BlueToothEnable()
	{
#if UNITY_ANDROID && !UNITY_EDITOR
		cls.Call ("BlueToothEnable");
#endif
	}

	//サーバーを生成
	public string StartServer()
	{
#if UNITY_ANDROID && !UNITY_EDITOR
		string address = cls.Call<string> ("StartServer");
		serverFlag = true;
		return address;
#endif
		return "This function is only valid for Android";
	}

	//サーバーに接続する
	public void Connect(string address)
	{
#if UNITY_ANDROID && !UNITY_EDITOR
		if(!serverFlag)
		{
			cls.Call("cancel");
			cls.Call("connect",address);
		}
#endif
	}

	public bool isConnected()
	{
		bool connected = false;
#if UNITY_ANDROID && !UNITY_EDITOR
		connected = cls.Call<bool>("isConnected");
#endif
		return connected;
	}

	public void Cancel()
	{
#if UNITY_ANDROID && !UNITY_EDITOR
		cls.Call("cancel");
#endif
	}

	public void disConnect()
	{
#if UNITY_ANDROID && !UNITY_EDITOR
		cls.Call("Disconnect");
#endif
	}

	public void SendIntergerData(int data ,Tag tag)
	{

#if UNITY_ANDROID && !UNITY_EDITOR
		string sendData = INT + "," + data + "," + (int)tag;
		cls.Call("sendData",sendData);
#endif
	}

	public void SendFloatData(float data ,Tag tag)
	{
#if UNITY_ANDROID && !UNITY_EDITOR
		string sendData = FLOAT + "," + data.ToString() + "," + (int)tag;
		cls.Call("sendData",sendData);
#endif
	}

	public void SendStringData(string data ,Tag tag)
	{
#if UNITY_ANDROID && !UNITY_EDITOR
		string sendData = STRING + "," + data + "," + (int)tag;
		cls.Call("sendData",sendData);
#endif
	}

	void onCallReceiveData(string data)
	{
		string[] receiveData = data.Split(',');
		int dataType = int.Parse (receiveData [DATATYPE]);
		int tag = int.Parse (receiveData [TAG]);
		switch (dataType) {
		case INT:
			int intData = int.Parse(receiveData [DATA]);
			onCallReceiveIntegerData(intData ,tag);
			break;

		case FLOAT:
			float floatData = float.Parse(receiveData [DATA]);
			onCallReceiveFloatData(floatData ,tag);
			break;

		case STRING:
			onCallReceiveStringData(receiveData [DATA] ,tag);
			break;
		}

		return;
	}

	void onCallReceiveIntegerData(int intData ,int tag)
	{
		//	Write the necessary processing
	}

	void onCallReceiveFloatData(float floatData ,int tag)
	{
		// Write the necessary processing
	}

	void onCallReceiveStringData(string stringData ,int tag)
	{
		// Write the necessary processing
	}

	void onCallClickOK(string tagstr)
	{
		int tag = int.Parse (tagstr);
		// Write the necessary processing
	}
	

	void onCallDisConnect(string dummydata)
	{
		// Write the necessary processing
	}
}
