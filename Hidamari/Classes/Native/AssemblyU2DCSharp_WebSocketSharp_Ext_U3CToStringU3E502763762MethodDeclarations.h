﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.Ext/<ToString>c__AnonStorey23`1<System.Object>
struct U3CToStringU3Ec__AnonStorey23_1_t502763762;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocketSharp.Ext/<ToString>c__AnonStorey23`1<System.Object>::.ctor()
extern "C"  void U3CToStringU3Ec__AnonStorey23_1__ctor_m2071422745_gshared (U3CToStringU3Ec__AnonStorey23_1_t502763762 * __this, const MethodInfo* method);
#define U3CToStringU3Ec__AnonStorey23_1__ctor_m2071422745(__this, method) ((  void (*) (U3CToStringU3Ec__AnonStorey23_1_t502763762 *, const MethodInfo*))U3CToStringU3Ec__AnonStorey23_1__ctor_m2071422745_gshared)(__this, method)
// System.Void WebSocketSharp.Ext/<ToString>c__AnonStorey23`1<System.Object>::<>m__3(System.Int32)
extern "C"  void U3CToStringU3Ec__AnonStorey23_1_U3CU3Em__3_m1972716164_gshared (U3CToStringU3Ec__AnonStorey23_1_t502763762 * __this, int32_t ___i0, const MethodInfo* method);
#define U3CToStringU3Ec__AnonStorey23_1_U3CU3Em__3_m1972716164(__this, ___i0, method) ((  void (*) (U3CToStringU3Ec__AnonStorey23_1_t502763762 *, int32_t, const MethodInfo*))U3CToStringU3Ec__AnonStorey23_1_U3CU3Em__3_m1972716164_gshared)(__this, ___i0, method)
