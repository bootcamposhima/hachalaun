﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TaskOrder
struct TaskOrder_t2170039465;
// BlackBoard
struct BlackBoard_t328561223;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BlackBoard328561223.h"

// System.Void TaskOrder::.ctor()
extern "C"  void TaskOrder__ctor_m3803191010 (TaskOrder_t2170039465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TaskOrder::Start()
extern "C"  void TaskOrder_Start_m2750328802 (TaskOrder_t2170039465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TaskOrder::Update()
extern "C"  void TaskOrder_Update_m3661666411 (TaskOrder_t2170039465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TaskOrder::get_coroutines()
extern "C"  bool TaskOrder_get_coroutines_m1823149490 (TaskOrder_t2170039465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TaskOrder::Play()
extern "C"  void TaskOrder_Play_m411091030 (TaskOrder_t2170039465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TaskOrder::Stop()
extern "C"  void TaskOrder_Stop_m504775076 (TaskOrder_t2170039465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TaskOrder::set_blackboard(BlackBoard)
extern "C"  void TaskOrder_set_blackboard_m3845524575 (TaskOrder_t2170039465 * __this, BlackBoard_t328561223 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TaskOrder::TaskStart()
extern "C"  Il2CppObject * TaskOrder_TaskStart_m2890089317 (TaskOrder_t2170039465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
