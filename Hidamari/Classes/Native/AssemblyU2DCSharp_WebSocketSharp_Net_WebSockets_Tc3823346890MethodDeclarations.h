﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<>c__Iterator1D
struct U3CU3Ec__Iterator1D_t3823346890;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.String>
struct IEnumerator_1_t1919096606;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<>c__Iterator1D::.ctor()
extern "C"  void U3CU3Ec__Iterator1D__ctor_m3257398241 (U3CU3Ec__Iterator1D_t3823346890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<>c__Iterator1D::System.Collections.Generic.IEnumerator<string>.get_Current()
extern "C"  String_t* U3CU3Ec__Iterator1D_System_Collections_Generic_IEnumeratorU3CstringU3E_get_Current_m3706070033 (U3CU3Ec__Iterator1D_t3823346890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<>c__Iterator1D::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CU3Ec__Iterator1D_System_Collections_IEnumerator_get_Current_m742685157 (U3CU3Ec__Iterator1D_t3823346890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<>c__Iterator1D::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CU3Ec__Iterator1D_System_Collections_IEnumerable_GetEnumerator_m1712162400 (U3CU3Ec__Iterator1D_t3823346890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.String> WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<>c__Iterator1D::System.Collections.Generic.IEnumerable<string>.GetEnumerator()
extern "C"  Il2CppObject* U3CU3Ec__Iterator1D_System_Collections_Generic_IEnumerableU3CstringU3E_GetEnumerator_m3264289290 (U3CU3Ec__Iterator1D_t3823346890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<>c__Iterator1D::MoveNext()
extern "C"  bool U3CU3Ec__Iterator1D_MoveNext_m2055721843 (U3CU3Ec__Iterator1D_t3823346890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<>c__Iterator1D::Dispose()
extern "C"  void U3CU3Ec__Iterator1D_Dispose_m3521801182 (U3CU3Ec__Iterator1D_t3823346890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<>c__Iterator1D::Reset()
extern "C"  void U3CU3Ec__Iterator1D_Reset_m903831182 (U3CU3Ec__Iterator1D_t3823346890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
