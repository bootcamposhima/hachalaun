﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TransitionAnimation/<AllScaleAction>c__IteratorD
struct U3CAllScaleActionU3Ec__IteratorD_t4063999446;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void TransitionAnimation/<AllScaleAction>c__IteratorD::.ctor()
extern "C"  void U3CAllScaleActionU3Ec__IteratorD__ctor_m890978693 (U3CAllScaleActionU3Ec__IteratorD_t4063999446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TransitionAnimation/<AllScaleAction>c__IteratorD::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CAllScaleActionU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1424865143 (U3CAllScaleActionU3Ec__IteratorD_t4063999446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TransitionAnimation/<AllScaleAction>c__IteratorD::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CAllScaleActionU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m1423149323 (U3CAllScaleActionU3Ec__IteratorD_t4063999446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TransitionAnimation/<AllScaleAction>c__IteratorD::MoveNext()
extern "C"  bool U3CAllScaleActionU3Ec__IteratorD_MoveNext_m2514227767 (U3CAllScaleActionU3Ec__IteratorD_t4063999446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransitionAnimation/<AllScaleAction>c__IteratorD::Dispose()
extern "C"  void U3CAllScaleActionU3Ec__IteratorD_Dispose_m1430315138 (U3CAllScaleActionU3Ec__IteratorD_t4063999446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransitionAnimation/<AllScaleAction>c__IteratorD::Reset()
extern "C"  void U3CAllScaleActionU3Ec__IteratorD_Reset_m2832378930 (U3CAllScaleActionU3Ec__IteratorD_t4063999446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
