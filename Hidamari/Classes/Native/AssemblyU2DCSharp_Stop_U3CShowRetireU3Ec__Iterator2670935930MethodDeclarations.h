﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Stop/<ShowRetire>c__Iterator4
struct U3CShowRetireU3Ec__Iterator4_t2670935930;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Stop/<ShowRetire>c__Iterator4::.ctor()
extern "C"  void U3CShowRetireU3Ec__Iterator4__ctor_m503257905 (U3CShowRetireU3Ec__Iterator4_t2670935930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Stop/<ShowRetire>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CShowRetireU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m378196865 (U3CShowRetireU3Ec__Iterator4_t2670935930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Stop/<ShowRetire>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CShowRetireU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m2460875029 (U3CShowRetireU3Ec__Iterator4_t2670935930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Stop/<ShowRetire>c__Iterator4::MoveNext()
extern "C"  bool U3CShowRetireU3Ec__Iterator4_MoveNext_m2699635747 (U3CShowRetireU3Ec__Iterator4_t2670935930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Stop/<ShowRetire>c__Iterator4::Dispose()
extern "C"  void U3CShowRetireU3Ec__Iterator4_Dispose_m2492792622 (U3CShowRetireU3Ec__Iterator4_t2670935930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Stop/<ShowRetire>c__Iterator4::Reset()
extern "C"  void U3CShowRetireU3Ec__Iterator4_Reset_m2444658142 (U3CShowRetireU3Ec__Iterator4_t2670935930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
