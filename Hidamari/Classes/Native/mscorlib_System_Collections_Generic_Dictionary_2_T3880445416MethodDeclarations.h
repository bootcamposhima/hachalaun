﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Tr827029284MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m3679548637(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t3880445416 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m2052388693_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m4190707003(__this, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Transform_1_t3880445416 *, HttpConnection_t602292776 *, HttpConnection_t602292776 *, const MethodInfo*))Transform_1_Invoke_m757436355_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m3550485734(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t3880445416 *, HttpConnection_t602292776 *, HttpConnection_t602292776 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m397518190_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m787382383(__this, ___result0, method) ((  DictionaryEntry_t1751606614  (*) (Transform_1_t3880445416 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m3155601639_gshared)(__this, ___result0, method)
