﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TestBullet
struct TestBullet_t3235265620;

#include "codegen/il2cpp-codegen.h"

// System.Void TestBullet::.ctor()
extern "C"  void TestBullet__ctor_m3394370247 (TestBullet_t3235265620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestBullet::Start()
extern "C"  void TestBullet_Start_m2341508039 (TestBullet_t3235265620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestBullet::Update()
extern "C"  void TestBullet_Update_m3873124646 (TestBullet_t3235265620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
