﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.WebSocket/<sendAsync>c__AnonStorey29
struct U3CsendAsyncU3Ec__AnonStorey29_t861700891;
// System.IAsyncResult
struct IAsyncResult_t2754620036;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocketSharp.WebSocket/<sendAsync>c__AnonStorey29::.ctor()
extern "C"  void U3CsendAsyncU3Ec__AnonStorey29__ctor_m2579381936 (U3CsendAsyncU3Ec__AnonStorey29_t861700891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket/<sendAsync>c__AnonStorey29::<>m__F(System.IAsyncResult)
extern "C"  void U3CsendAsyncU3Ec__AnonStorey29_U3CU3Em__F_m2428161332 (U3CsendAsyncU3Ec__AnonStorey29_t861700891 * __this, Il2CppObject * ___ar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
