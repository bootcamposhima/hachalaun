﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.Net.HttpBasicIdentity
struct HttpBasicIdentity_t3688512078;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void WebSocketSharp.Net.HttpBasicIdentity::.ctor(System.String,System.String)
extern "C"  void HttpBasicIdentity__ctor_m3075037707 (HttpBasicIdentity_t3688512078 * __this, String_t* ___username0, String_t* ___password1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.Net.HttpBasicIdentity::get_Password()
extern "C"  String_t* HttpBasicIdentity_get_Password_m3036521496 (HttpBasicIdentity_t3688512078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
