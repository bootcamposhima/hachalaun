﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3672647722MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m3739670980(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t2976322626 *, Dictionary_2_t1349563175 *, const MethodInfo*))KeyCollection__ctor_m3432069128_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3930486034(__this, ___item0, method) ((  void (*) (KeyCollection_t2976322626 *, HttpListenerContext_t3744659101 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3101899854_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2573907721(__this, method) ((  void (*) (KeyCollection_t2976322626 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m164109637_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3760482744(__this, ___item0, method) ((  bool (*) (KeyCollection_t2976322626 *, HttpListenerContext_t3744659101 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2402136956_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2641357085(__this, ___item0, method) ((  bool (*) (KeyCollection_t2976322626 *, HttpListenerContext_t3744659101 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1325978593_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2075823237(__this, method) ((  Il2CppObject* (*) (KeyCollection_t2976322626 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3150060033_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m2534233595(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2976322626 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m2134327863_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3916688566(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2976322626 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3067601266_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m342512409(__this, method) ((  bool (*) (KeyCollection_t2976322626 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m642268125_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3991648011(__this, method) ((  bool (*) (KeyCollection_t2976322626 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1412236495_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m1218793655(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2976322626 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3575527099_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m3767169894(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2976322626 *, HttpListenerContextU5BU5D_t838488656*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2803941053_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::GetEnumerator()
#define KeyCollection_GetEnumerator_m3231960860(__this, method) ((  Enumerator_t1964499229  (*) (KeyCollection_t2976322626 *, const MethodInfo*))KeyCollection_GetEnumerator_m2980864032_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::get_Count()
#define KeyCollection_get_Count_m295039752(__this, method) ((  int32_t (*) (KeyCollection_t2976322626 *, const MethodInfo*))KeyCollection_get_Count_m1374340501_gshared)(__this, method)
