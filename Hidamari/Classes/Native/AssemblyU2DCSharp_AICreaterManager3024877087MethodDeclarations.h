﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AICreaterManager
struct AICreaterManager_t3024877087;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AIDatas4008896193.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void AICreaterManager::.ctor()
extern "C"  void AICreaterManager__ctor_m172892 (AICreaterManager_t3024877087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICreaterManager::Start()
extern "C"  void AICreaterManager_Start_m3242277980 (AICreaterManager_t3024877087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICreaterManager::Awake()
extern "C"  void AICreaterManager_Awake_m237778111 (AICreaterManager_t3024877087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICreaterManager::Start(AIDatas)
extern "C"  void AICreaterManager_Start_m3949536757 (AICreaterManager_t3024877087 * __this, AIDatas_t4008896193  ___aidatas0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICreaterManager::End()
extern "C"  void AICreaterManager_End_m1585795285 (AICreaterManager_t3024877087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICreaterManager::Update()
extern "C"  void AICreaterManager_Update_m1732221745 (AICreaterManager_t3024877087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICreaterManager::AddButton()
extern "C"  void AICreaterManager_AddButton_m3425202797 (AICreaterManager_t3024877087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] AICreaterManager::ArrayLinking(System.Int32[],System.Int32)
extern "C"  Int32U5BU5D_t3230847821* AICreaterManager_ArrayLinking_m4024009989 (AICreaterManager_t3024877087 * __this, Int32U5BU5D_t3230847821* ___origin0, int32_t ___link1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICreaterManager::ChangeCharNum(System.Int32)
extern "C"  void AICreaterManager_ChangeCharNum_m4281091147 (AICreaterManager_t3024877087 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICreaterManager::ChangeCharactorPosition()
extern "C"  void AICreaterManager_ChangeCharactorPosition_m854496018 (AICreaterManager_t3024877087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICreaterManager::CreateCharactor()
extern "C"  void AICreaterManager_CreateCharactor_m4150507325 (AICreaterManager_t3024877087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICreaterManager::ChangeSave()
extern "C"  void AICreaterManager_ChangeSave_m3118733109 (AICreaterManager_t3024877087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICreaterManager::FinishButton()
extern "C"  void AICreaterManager_FinishButton_m1253550029 (AICreaterManager_t3024877087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICreaterManager::InitCharactorPosition()
extern "C"  void AICreaterManager_InitCharactorPosition_m3616505842 (AICreaterManager_t3024877087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICreaterManager::initCreateCharactor()
extern "C"  void AICreaterManager_initCreateCharactor_m911032237 (AICreaterManager_t3024877087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICreaterManager::LoadAiData()
extern "C"  void AICreaterManager_LoadAiData_m2504658848 (AICreaterManager_t3024877087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICreaterManager::SetAidatas(AIDatas)
extern "C"  void AICreaterManager_SetAidatas_m2650289770 (AICreaterManager_t3024877087 * __this, AIDatas_t4008896193  ___aidatas0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICreaterManager::setArrowConfiguration(System.Boolean)
extern "C"  void AICreaterManager_setArrowConfiguration_m2397907968 (AICreaterManager_t3024877087 * __this, bool ___flag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICreaterManager::setArrowPosition()
extern "C"  void AICreaterManager_setArrowPosition_m2919139032 (AICreaterManager_t3024877087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICreaterManager::setButtonAction(System.Boolean)
extern "C"  void AICreaterManager_setButtonAction_m624947931 (AICreaterManager_t3024877087 * __this, bool ___flag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICreaterManager::SetCameraPosition()
extern "C"  void AICreaterManager_SetCameraPosition_m532627306 (AICreaterManager_t3024877087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICreaterManager::setDirection(System.Int32)
extern "C"  void AICreaterManager_setDirection_m3940081430 (AICreaterManager_t3024877087 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICreaterManager::setTop()
extern "C"  void AICreaterManager_setTop_m1750240603 (AICreaterManager_t3024877087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICreaterManager::TopAIButton()
extern "C"  void AICreaterManager_TopAIButton_m3938880457 (AICreaterManager_t3024877087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICreaterManager::TopAIButton(System.Int32)
extern "C"  void AICreaterManager_TopAIButton_m1908136858 (AICreaterManager_t3024877087 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICreaterManager::TopBulletNum(System.Int32)
extern "C"  void AICreaterManager_TopBulletNum_m3088539592 (AICreaterManager_t3024877087 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICreaterManager::TopCharaButton()
extern "C"  void AICreaterManager_TopCharaButton_m239549936 (AICreaterManager_t3024877087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICreaterManager::TopFormationButton()
extern "C"  void AICreaterManager_TopFormationButton_m2742726614 (AICreaterManager_t3024877087 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICreaterManager::TopPositionNum(System.Int32)
extern "C"  void AICreaterManager_TopPositionNum_m2666574337 (AICreaterManager_t3024877087 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICreaterManager::TidyScrollView(System.Int32,UnityEngine.GameObject)
extern "C"  void AICreaterManager_TidyScrollView_m3313731945 (AICreaterManager_t3024877087 * __this, int32_t ___num0, GameObject_t3674682005 * ___sc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICreaterManager::WriteAI(System.Int32[],System.Int32[])
extern "C"  void AICreaterManager_WriteAI_m2438185221 (AICreaterManager_t3024877087 * __this, Int32U5BU5D_t3230847821* ___bullet0, Int32U5BU5D_t3230847821* ___position1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
