﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Condition
struct Condition_t1142656251;

#include "codegen/il2cpp-codegen.h"

// System.Void Condition::.ctor()
extern "C"  void Condition__ctor_m1430188752 (Condition_t1142656251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Condition::Start()
extern "C"  void Condition_Start_m377326544 (Condition_t1142656251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Condition::Update()
extern "C"  void Condition_Update_m3113040445 (Condition_t1142656251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
