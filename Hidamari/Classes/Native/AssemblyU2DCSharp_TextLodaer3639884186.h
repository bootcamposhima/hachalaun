﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen2970059039.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextLodaer
struct  TextLodaer_t3639884186  : public SingletonMonoBehaviour_1_t2970059039
{
public:
	// System.String TextLodaer::m_filepas
	String_t* ___m_filepas_3;

public:
	inline static int32_t get_offset_of_m_filepas_3() { return static_cast<int32_t>(offsetof(TextLodaer_t3639884186, ___m_filepas_3)); }
	inline String_t* get_m_filepas_3() const { return ___m_filepas_3; }
	inline String_t** get_address_of_m_filepas_3() { return &___m_filepas_3; }
	inline void set_m_filepas_3(String_t* value)
	{
		___m_filepas_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_filepas_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
