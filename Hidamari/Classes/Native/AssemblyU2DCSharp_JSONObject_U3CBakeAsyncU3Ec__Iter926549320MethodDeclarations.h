﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSONObject/<BakeAsync>c__Iterator17
struct U3CBakeAsyncU3Ec__Iterator17_t926549320;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;

#include "codegen/il2cpp-codegen.h"

// System.Void JSONObject/<BakeAsync>c__Iterator17::.ctor()
extern "C"  void U3CBakeAsyncU3Ec__Iterator17__ctor_m4120817443 (U3CBakeAsyncU3Ec__Iterator17_t926549320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JSONObject/<BakeAsync>c__Iterator17::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CBakeAsyncU3Ec__Iterator17_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m836847247 (U3CBakeAsyncU3Ec__Iterator17_t926549320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JSONObject/<BakeAsync>c__Iterator17::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CBakeAsyncU3Ec__Iterator17_System_Collections_IEnumerator_get_Current_m758663203 (U3CBakeAsyncU3Ec__Iterator17_t926549320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator JSONObject/<BakeAsync>c__Iterator17::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CBakeAsyncU3Ec__Iterator17_System_Collections_IEnumerable_GetEnumerator_m1059704542 (U3CBakeAsyncU3Ec__Iterator17_t926549320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Object> JSONObject/<BakeAsync>c__Iterator17::System.Collections.Generic.IEnumerable<object>.GetEnumerator()
extern "C"  Il2CppObject* U3CBakeAsyncU3Ec__Iterator17_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m1720955564 (U3CBakeAsyncU3Ec__Iterator17_t926549320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSONObject/<BakeAsync>c__Iterator17::MoveNext()
extern "C"  bool U3CBakeAsyncU3Ec__Iterator17_MoveNext_m525709809 (U3CBakeAsyncU3Ec__Iterator17_t926549320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject/<BakeAsync>c__Iterator17::Dispose()
extern "C"  void U3CBakeAsyncU3Ec__Iterator17_Dispose_m43998880 (U3CBakeAsyncU3Ec__Iterator17_t926549320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject/<BakeAsync>c__Iterator17::Reset()
extern "C"  void U3CBakeAsyncU3Ec__Iterator17_Reset_m1767250384 (U3CBakeAsyncU3Ec__Iterator17_t926549320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
