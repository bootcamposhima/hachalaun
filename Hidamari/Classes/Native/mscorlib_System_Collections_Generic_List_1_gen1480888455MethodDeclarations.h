﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1244034627MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::.ctor()
#define List_1__ctor_m2534021403(__this, method) ((  void (*) (List_1_t1480888455 *, const MethodInfo*))List_1__ctor_m3048469268_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m1162742349(__this, ___collection0, method) ((  void (*) (List_1_t1480888455 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1160795371_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::.ctor(System.Int32)
#define List_1__ctor_m3605371203(__this, ___capacity0, method) ((  void (*) (List_1_t1480888455 *, int32_t, const MethodInfo*))List_1__ctor_m3643386469_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::.cctor()
#define List_1__cctor_m1387457275(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3826137881_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m92848836(__this, method) ((  Il2CppObject* (*) (List_1_t1480888455 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2808422246_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m2179857042(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1480888455 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m4034025648_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3496345741(__this, method) ((  Il2CppObject * (*) (List_1_t1480888455 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1841330603_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m4236592900(__this, ___item0, method) ((  int32_t (*) (List_1_t1480888455 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3794749222_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m1288400840(__this, ___item0, method) ((  bool (*) (List_1_t1480888455 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2659633254_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m3389152092(__this, ___item0, method) ((  int32_t (*) (List_1_t1480888455 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3431692926_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m3168220807(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1480888455 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2067529129_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m3515048769(__this, ___item0, method) ((  void (*) (List_1_t1480888455 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1644145887_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m652494281(__this, method) ((  bool (*) (List_1_t1480888455 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1299706087_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m2916643476(__this, method) ((  bool (*) (List_1_t1480888455 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3867536694_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m2478627136(__this, method) ((  Il2CppObject * (*) (List_1_t1480888455 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m4244374434_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m1693750583(__this, method) ((  bool (*) (List_1_t1480888455 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m432946261_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m1755571938(__this, method) ((  bool (*) (List_1_t1480888455 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2961826820_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m3614407367(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1480888455 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3985478825_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m142335134(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1480888455 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3234554688_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::Add(T)
#define List_1_Add_m2228382219(__this, ___item0, method) ((  void (*) (List_1_t1480888455 *, Action_1_t112702903 *, const MethodInfo*))List_1_Add_m642669291_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m2358976776(__this, ___newCount0, method) ((  void (*) (List_1_t1480888455 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m4122600870_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m1267030022(__this, ___collection0, method) ((  void (*) (List_1_t1480888455 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2478449828_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m528002246(__this, ___enumerable0, method) ((  void (*) (List_1_t1480888455 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1739422052_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m102030865(__this, ___collection0, method) ((  void (*) (List_1_t1480888455 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2229151411_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::AsReadOnly()
#define List_1_AsReadOnly_m1398868600(__this, method) ((  ReadOnlyCollection_1_t1669780439 * (*) (List_1_t1480888455 *, const MethodInfo*))List_1_AsReadOnly_m769820182_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::Clear()
#define List_1_Clear_m2869787421(__this, method) ((  void (*) (List_1_t1480888455 *, const MethodInfo*))List_1_Clear_m454602559_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::Contains(T)
#define List_1_Contains_m3058564045(__this, ___item0, method) ((  bool (*) (List_1_t1480888455 *, Action_1_t112702903 *, const MethodInfo*))List_1_Contains_m4186092781_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m3910543869(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1480888455 *, Action_1U5BU5D_t2027700622*, int32_t, const MethodInfo*))List_1_CopyTo_m3988356635_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::Find(System.Predicate`1<T>)
#define List_1_Find_m356727179(__this, ___match0, method) ((  Action_1_t112702903 * (*) (List_1_t1480888455 *, Predicate_1_t4018727082 *, const MethodInfo*))List_1_Find_m3379773421_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m2375999878(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t4018727082 *, const MethodInfo*))List_1_CheckMatch_m3390394152_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m1536182699(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1480888455 *, int32_t, int32_t, Predicate_1_t4018727082 *, const MethodInfo*))List_1_GetIndex_m4275988045_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::GetEnumerator()
#define List_1_GetEnumerator_m4267029534(__this, method) ((  Enumerator_t1500561225  (*) (List_1_t1480888455 *, const MethodInfo*))List_1_GetEnumerator_m2326457258_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::IndexOf(T)
#define List_1_IndexOf_m541563713(__this, ___item0, method) ((  int32_t (*) (List_1_t1480888455 *, Action_1_t112702903 *, const MethodInfo*))List_1_IndexOf_m1752303327_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m1557574484(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1480888455 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3807054194_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m3656911053(__this, ___index0, method) ((  void (*) (List_1_t1480888455 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3734723819_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::Insert(System.Int32,T)
#define List_1_Insert_m490773428(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1480888455 *, int32_t, Action_1_t112702903 *, const MethodInfo*))List_1_Insert_m3427163986_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m2666774825(__this, ___collection0, method) ((  void (*) (List_1_t1480888455 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2905071175_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::Remove(T)
#define List_1_Remove_m3623269042(__this, ___item0, method) ((  bool (*) (List_1_t1480888455 *, Action_1_t112702903 *, const MethodInfo*))List_1_Remove_m2747911208_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m3162770500(__this, ___match0, method) ((  int32_t (*) (List_1_t1480888455 *, Predicate_1_t4018727082 *, const MethodInfo*))List_1_RemoveAll_m2933443938_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m2659593594(__this, ___index0, method) ((  void (*) (List_1_t1480888455 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1301016856_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::Reverse()
#define List_1_Reverse_m2159394482(__this, method) ((  void (*) (List_1_t1480888455 *, const MethodInfo*))List_1_Reverse_m449081940_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::Sort()
#define List_1_Sort_m2216382000(__this, method) ((  void (*) (List_1_t1480888455 *, const MethodInfo*))List_1_Sort_m1168641486_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m1566545539(__this, ___comparison0, method) ((  void (*) (List_1_t1480888455 *, Comparison_1_t3124031386 *, const MethodInfo*))List_1_Sort_m4192185249_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::ToArray()
#define List_1_ToArray_m2734735857(__this, method) ((  Action_1U5BU5D_t2027700622* (*) (List_1_t1480888455 *, const MethodInfo*))List_1_ToArray_m238588755_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::TrimExcess()
#define List_1_TrimExcess_m3175287241(__this, method) ((  void (*) (List_1_t1480888455 *, const MethodInfo*))List_1_TrimExcess_m2451380967_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::get_Capacity()
#define List_1_get_Capacity_m280365809(__this, method) ((  int32_t (*) (List_1_t1480888455 *, const MethodInfo*))List_1_get_Capacity_m543520655_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m3864132890(__this, ___value0, method) ((  void (*) (List_1_t1480888455 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1332789688_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::get_Count()
#define List_1_get_Count_m455457233(__this, method) ((  int32_t (*) (List_1_t1480888455 *, const MethodInfo*))List_1_get_Count_m2599103100_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::get_Item(System.Int32)
#define List_1_get_Item_m223753790(__this, ___index0, method) ((  Action_1_t112702903 * (*) (List_1_t1480888455 *, int32_t, const MethodInfo*))List_1_get_Item_m2771401372_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>::set_Item(System.Int32,T)
#define List_1_set_Item_m996458379(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1480888455 *, int32_t, Action_1_t112702903 *, const MethodInfo*))List_1_set_Item_m1074271145_gshared)(__this, ___index0, ___value1, method)
