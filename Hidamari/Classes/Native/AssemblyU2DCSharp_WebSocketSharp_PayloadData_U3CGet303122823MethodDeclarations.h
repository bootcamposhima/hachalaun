﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator1E
struct U3CGetEnumeratorU3Ec__Iterator1E_t303122823;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator1E::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator1E__ctor_m1103379908 (U3CGetEnumeratorU3Ec__Iterator1E_t303122823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator1E::System.Collections.Generic.IEnumerator<byte>.get_Current()
extern "C"  uint8_t U3CGetEnumeratorU3Ec__Iterator1E_System_Collections_Generic_IEnumeratorU3CbyteU3E_get_Current_m3060147566 (U3CGetEnumeratorU3Ec__Iterator1E_t303122823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator1E::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator1E_System_Collections_IEnumerator_get_Current_m3228423842 (U3CGetEnumeratorU3Ec__Iterator1E_t303122823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator1E::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator1E_MoveNext_m2976200112 (U3CGetEnumeratorU3Ec__Iterator1E_t303122823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator1E::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator1E_Dispose_m3684419841 (U3CGetEnumeratorU3Ec__Iterator1E_t303122823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator1E::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator1E_Reset_m3044780145 (U3CGetEnumeratorU3Ec__Iterator1E_t303122823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
