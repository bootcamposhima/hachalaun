﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType1744280289.h"
#include "AssemblyU2DCSharp_FightJson649175288.h"
#include "AssemblyU2DCSharp_PlayerJson4089020297.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SummarizeJson
struct  SummarizeJson_t970273193 
{
public:
	// System.Int32 SummarizeJson::num
	int32_t ___num_0;
	// FightJson SummarizeJson::fjson
	FightJson_t649175288  ___fjson_1;
	// PlayerJson SummarizeJson::pjson
	PlayerJson_t4089020297  ___pjson_2;

public:
	inline static int32_t get_offset_of_num_0() { return static_cast<int32_t>(offsetof(SummarizeJson_t970273193, ___num_0)); }
	inline int32_t get_num_0() const { return ___num_0; }
	inline int32_t* get_address_of_num_0() { return &___num_0; }
	inline void set_num_0(int32_t value)
	{
		___num_0 = value;
	}

	inline static int32_t get_offset_of_fjson_1() { return static_cast<int32_t>(offsetof(SummarizeJson_t970273193, ___fjson_1)); }
	inline FightJson_t649175288  get_fjson_1() const { return ___fjson_1; }
	inline FightJson_t649175288 * get_address_of_fjson_1() { return &___fjson_1; }
	inline void set_fjson_1(FightJson_t649175288  value)
	{
		___fjson_1 = value;
	}

	inline static int32_t get_offset_of_pjson_2() { return static_cast<int32_t>(offsetof(SummarizeJson_t970273193, ___pjson_2)); }
	inline PlayerJson_t4089020297  get_pjson_2() const { return ___pjson_2; }
	inline PlayerJson_t4089020297 * get_address_of_pjson_2() { return &___pjson_2; }
	inline void set_pjson_2(PlayerJson_t4089020297  value)
	{
		___pjson_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: SummarizeJson
struct SummarizeJson_t970273193_marshaled_pinvoke
{
	int32_t ___num_0;
	FightJson_t649175288_marshaled_pinvoke ___fjson_1;
	PlayerJson_t4089020297_marshaled_pinvoke ___pjson_2;
};
// Native definition for marshalling of: SummarizeJson
struct SummarizeJson_t970273193_marshaled_com
{
	int32_t ___num_0;
	FightJson_t649175288_marshaled_com ___fjson_1;
	PlayerJson_t4089020297_marshaled_com ___pjson_2;
};
