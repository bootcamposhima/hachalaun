﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShirkAI
struct ShirkAI_t3726263989;
// BlackBoard
struct BlackBoard_t328561223;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BlackBoard328561223.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_Direction1041377119.h"

// System.Void ShirkAI::.ctor()
extern "C"  void ShirkAI__ctor_m3727336534 (ShirkAI_t3726263989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShirkAI::Start()
extern "C"  void ShirkAI_Start_m2674474326 (ShirkAI_t3726263989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShirkAI::Awake()
extern "C"  void ShirkAI_Awake_m3964941753 (ShirkAI_t3726263989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShirkAI::InitBlacBoard(BlackBoard)
extern "C"  void ShirkAI_InitBlacBoard_m2375031 (ShirkAI_t3726263989 * __this, BlackBoard_t328561223 * ___blackboard0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShirkAI::SetGameSpeed(System.Single)
extern "C"  void ShirkAI_SetGameSpeed_m509225386 (ShirkAI_t3726263989 * __this, float ___speed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShirkAI::SetHP(UnityEngine.GameObject,UnityEngine.GameObject)
extern "C"  void ShirkAI_SetHP_m3428996584 (ShirkAI_t3726263989 * __this, GameObject_t3674682005 * ___hpgaug0, GameObject_t3674682005 * ___hptext1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShirkAI::Update()
extern "C"  void ShirkAI_Update_m1310177655 (ShirkAI_t3726263989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShirkAI::FixedUpdate()
extern "C"  void ShirkAI_FixedUpdate_m805398865 (ShirkAI_t3726263989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ShirkAI::GetHP()
extern "C"  int32_t ShirkAI_GetHP_m4274465504 (ShirkAI_t3726263989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShirkAI::SearchBullet()
extern "C"  void ShirkAI_SearchBullet_m1694398328 (ShirkAI_t3726263989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShirkAI::MoveDirection(System.Int32,System.Int32)
extern "C"  void ShirkAI_MoveDirection_m827683780 (ShirkAI_t3726263989 * __this, int32_t ___hitflag0, int32_t ___ctflag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ShirkAI::FlagJude(System.Int32,System.Int32)
extern "C"  bool ShirkAI_FlagJude_m4105645140 (ShirkAI_t3726263989 * __this, int32_t ___flag10, int32_t ___flag21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShirkAI::MoveStart()
extern "C"  void ShirkAI_MoveStart_m1995336389 (ShirkAI_t3726263989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShirkAI::Move()
extern "C"  void ShirkAI_Move_m3097080255 (ShirkAI_t3726263989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShirkAI::PlayerRotatin(System.Int32)
extern "C"  void ShirkAI_PlayerRotatin_m2487486995 (ShirkAI_t3726263989 * __this, int32_t ___angle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShirkAI::Flashing()
extern "C"  void ShirkAI_Flashing_m4061885728 (ShirkAI_t3726263989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShirkAI::HitBullet(System.Int32,Direction)
extern "C"  void ShirkAI_HitBullet_m2596021607 (ShirkAI_t3726263989 * __this, int32_t ___damage0, int32_t ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShirkAI::ChangeHP()
extern "C"  void ShirkAI_ChangeHP_m3115280998 (ShirkAI_t3726263989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShirkAI::OnDrawGizmos()
extern "C"  void ShirkAI_OnDrawGizmos_m3439591434 (ShirkAI_t3726263989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShirkAI::StartParticle()
extern "C"  void ShirkAI_StartParticle_m3518421372 (ShirkAI_t3726263989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShirkAI::DamegeParticle()
extern "C"  void ShirkAI_DamegeParticle_m1027832263 (ShirkAI_t3726263989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
