﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StageManager
struct StageManager_t3194912495;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void StageManager::.ctor()
extern "C"  void StageManager__ctor_m1532481676 (StageManager_t3194912495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StageManager::Start()
extern "C"  void StageManager_Start_m479619468 (StageManager_t3194912495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StageManager::Awake()
extern "C"  void StageManager_Awake_m1770086895 (StageManager_t3194912495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StageManager::Update()
extern "C"  void StageManager_Update_m1989153793 (StageManager_t3194912495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StageManager::PushStageBuuton(System.String)
extern "C"  void StageManager_PushStageBuuton_m2536803203 (StageManager_t3194912495 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
