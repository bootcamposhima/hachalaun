﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "mscorlib_System_EventArgs2540831021.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Opcode3782140426.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.MessageEventArgs
struct  MessageEventArgs_t36945740  : public EventArgs_t2540831021
{
public:
	// System.String WebSocketSharp.MessageEventArgs::_data
	String_t* ____data_1;
	// WebSocketSharp.Opcode WebSocketSharp.MessageEventArgs::_opcode
	uint8_t ____opcode_2;
	// System.Byte[] WebSocketSharp.MessageEventArgs::_rawData
	ByteU5BU5D_t4260760469* ____rawData_3;

public:
	inline static int32_t get_offset_of__data_1() { return static_cast<int32_t>(offsetof(MessageEventArgs_t36945740, ____data_1)); }
	inline String_t* get__data_1() const { return ____data_1; }
	inline String_t** get_address_of__data_1() { return &____data_1; }
	inline void set__data_1(String_t* value)
	{
		____data_1 = value;
		Il2CppCodeGenWriteBarrier(&____data_1, value);
	}

	inline static int32_t get_offset_of__opcode_2() { return static_cast<int32_t>(offsetof(MessageEventArgs_t36945740, ____opcode_2)); }
	inline uint8_t get__opcode_2() const { return ____opcode_2; }
	inline uint8_t* get_address_of__opcode_2() { return &____opcode_2; }
	inline void set__opcode_2(uint8_t value)
	{
		____opcode_2 = value;
	}

	inline static int32_t get_offset_of__rawData_3() { return static_cast<int32_t>(offsetof(MessageEventArgs_t36945740, ____rawData_3)); }
	inline ByteU5BU5D_t4260760469* get__rawData_3() const { return ____rawData_3; }
	inline ByteU5BU5D_t4260760469** get_address_of__rawData_3() { return &____rawData_3; }
	inline void set__rawData_3(ByteU5BU5D_t4260760469* value)
	{
		____rawData_3 = value;
		Il2CppCodeGenWriteBarrier(&____rawData_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
