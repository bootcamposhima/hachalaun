﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<JSONObject>
struct Action_1_t2148193039;

#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_DateTime4283661327.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SocketIO.Ack
struct  Ack_t4240269559  : public Il2CppObject
{
public:
	// System.Int32 SocketIO.Ack::packetId
	int32_t ___packetId_0;
	// System.DateTime SocketIO.Ack::time
	DateTime_t4283661327  ___time_1;
	// System.Action`1<JSONObject> SocketIO.Ack::action
	Action_1_t2148193039 * ___action_2;

public:
	inline static int32_t get_offset_of_packetId_0() { return static_cast<int32_t>(offsetof(Ack_t4240269559, ___packetId_0)); }
	inline int32_t get_packetId_0() const { return ___packetId_0; }
	inline int32_t* get_address_of_packetId_0() { return &___packetId_0; }
	inline void set_packetId_0(int32_t value)
	{
		___packetId_0 = value;
	}

	inline static int32_t get_offset_of_time_1() { return static_cast<int32_t>(offsetof(Ack_t4240269559, ___time_1)); }
	inline DateTime_t4283661327  get_time_1() const { return ___time_1; }
	inline DateTime_t4283661327 * get_address_of_time_1() { return &___time_1; }
	inline void set_time_1(DateTime_t4283661327  value)
	{
		___time_1 = value;
	}

	inline static int32_t get_offset_of_action_2() { return static_cast<int32_t>(offsetof(Ack_t4240269559, ___action_2)); }
	inline Action_1_t2148193039 * get_action_2() const { return ___action_2; }
	inline Action_1_t2148193039 ** get_address_of_action_2() { return &___action_2; }
	inline void set_action_2(Action_1_t2148193039 * value)
	{
		___action_2 = value;
		Il2CppCodeGenWriteBarrier(&___action_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
