﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.WebSocket/<sendAsync>c__AnonStorey2A
struct U3CsendAsyncU3Ec__AnonStorey2A_t861700899;
// System.IAsyncResult
struct IAsyncResult_t2754620036;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocketSharp.WebSocket/<sendAsync>c__AnonStorey2A::.ctor()
extern "C"  void U3CsendAsyncU3Ec__AnonStorey2A__ctor_m1007273896 (U3CsendAsyncU3Ec__AnonStorey2A_t861700899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket/<sendAsync>c__AnonStorey2A::<>m__10(System.IAsyncResult)
extern "C"  void U3CsendAsyncU3Ec__AnonStorey2A_U3CU3Em__10_m112891155 (U3CsendAsyncU3Ec__AnonStorey2A_t861700899 * __this, Il2CppObject * ___ar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
