﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2662109048;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_CharData1499709504.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Charctor
struct  Charctor_t1500651146  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Vector3 Charctor::m_pos
	Vector3_t4282066566  ___m_pos_2;
	// System.Boolean Charctor::ismove
	bool ___ismove_3;
	// CharData Charctor::_chardata
	CharData_t1499709504  ____chardata_4;
	// System.Single Charctor::gameSpeed
	float ___gameSpeed_5;
	// System.Boolean Charctor::isplay
	bool ___isplay_6;
	// UnityEngine.GameObject Charctor::par
	GameObject_t3674682005 * ___par_7;
	// UnityEngine.GameObject[] Charctor::endPar
	GameObjectU5BU5D_t2662109048* ___endPar_8;
	// UnityEngine.GameObject Charctor::movePar
	GameObject_t3674682005 * ___movePar_9;

public:
	inline static int32_t get_offset_of_m_pos_2() { return static_cast<int32_t>(offsetof(Charctor_t1500651146, ___m_pos_2)); }
	inline Vector3_t4282066566  get_m_pos_2() const { return ___m_pos_2; }
	inline Vector3_t4282066566 * get_address_of_m_pos_2() { return &___m_pos_2; }
	inline void set_m_pos_2(Vector3_t4282066566  value)
	{
		___m_pos_2 = value;
	}

	inline static int32_t get_offset_of_ismove_3() { return static_cast<int32_t>(offsetof(Charctor_t1500651146, ___ismove_3)); }
	inline bool get_ismove_3() const { return ___ismove_3; }
	inline bool* get_address_of_ismove_3() { return &___ismove_3; }
	inline void set_ismove_3(bool value)
	{
		___ismove_3 = value;
	}

	inline static int32_t get_offset_of__chardata_4() { return static_cast<int32_t>(offsetof(Charctor_t1500651146, ____chardata_4)); }
	inline CharData_t1499709504  get__chardata_4() const { return ____chardata_4; }
	inline CharData_t1499709504 * get_address_of__chardata_4() { return &____chardata_4; }
	inline void set__chardata_4(CharData_t1499709504  value)
	{
		____chardata_4 = value;
	}

	inline static int32_t get_offset_of_gameSpeed_5() { return static_cast<int32_t>(offsetof(Charctor_t1500651146, ___gameSpeed_5)); }
	inline float get_gameSpeed_5() const { return ___gameSpeed_5; }
	inline float* get_address_of_gameSpeed_5() { return &___gameSpeed_5; }
	inline void set_gameSpeed_5(float value)
	{
		___gameSpeed_5 = value;
	}

	inline static int32_t get_offset_of_isplay_6() { return static_cast<int32_t>(offsetof(Charctor_t1500651146, ___isplay_6)); }
	inline bool get_isplay_6() const { return ___isplay_6; }
	inline bool* get_address_of_isplay_6() { return &___isplay_6; }
	inline void set_isplay_6(bool value)
	{
		___isplay_6 = value;
	}

	inline static int32_t get_offset_of_par_7() { return static_cast<int32_t>(offsetof(Charctor_t1500651146, ___par_7)); }
	inline GameObject_t3674682005 * get_par_7() const { return ___par_7; }
	inline GameObject_t3674682005 ** get_address_of_par_7() { return &___par_7; }
	inline void set_par_7(GameObject_t3674682005 * value)
	{
		___par_7 = value;
		Il2CppCodeGenWriteBarrier(&___par_7, value);
	}

	inline static int32_t get_offset_of_endPar_8() { return static_cast<int32_t>(offsetof(Charctor_t1500651146, ___endPar_8)); }
	inline GameObjectU5BU5D_t2662109048* get_endPar_8() const { return ___endPar_8; }
	inline GameObjectU5BU5D_t2662109048** get_address_of_endPar_8() { return &___endPar_8; }
	inline void set_endPar_8(GameObjectU5BU5D_t2662109048* value)
	{
		___endPar_8 = value;
		Il2CppCodeGenWriteBarrier(&___endPar_8, value);
	}

	inline static int32_t get_offset_of_movePar_9() { return static_cast<int32_t>(offsetof(Charctor_t1500651146, ___movePar_9)); }
	inline GameObject_t3674682005 * get_movePar_9() const { return ___movePar_9; }
	inline GameObject_t3674682005 ** get_address_of_movePar_9() { return &___movePar_9; }
	inline void set_movePar_9(GameObject_t3674682005 * value)
	{
		___movePar_9 = value;
		Il2CppCodeGenWriteBarrier(&___movePar_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
