﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen184564025MethodDeclarations.h"

// System.Void System.Func`2<System.Security.Principal.IIdentity,WebSocketSharp.Net.NetworkCredential>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m3897635121(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t4138449219 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m3944524044_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.Security.Principal.IIdentity,WebSocketSharp.Net.NetworkCredential>::Invoke(T)
#define Func_2_Invoke_m848298127(__this, ___arg10, method) ((  NetworkCredential_t1204099087 * (*) (Func_2_t4138449219 *, Il2CppObject *, const MethodInfo*))Func_2_Invoke_m1924616534_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.Security.Principal.IIdentity,WebSocketSharp.Net.NetworkCredential>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m1053804888(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t4138449219 *, Il2CppObject *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m4281703301_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.Security.Principal.IIdentity,WebSocketSharp.Net.NetworkCredential>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m3595489487(__this, ___result0, method) ((  NetworkCredential_t1204099087 * (*) (Func_2_t4138449219 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m4118168638_gshared)(__this, ___result0, method)
