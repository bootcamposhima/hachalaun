﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TaskOrder/<TaskStart>c__Iterator1
struct U3CTaskStartU3Ec__Iterator1_t3806606775;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void TaskOrder/<TaskStart>c__Iterator1::.ctor()
extern "C"  void U3CTaskStartU3Ec__Iterator1__ctor_m2508465556 (U3CTaskStartU3Ec__Iterator1_t3806606775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TaskOrder/<TaskStart>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTaskStartU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m650572670 (U3CTaskStartU3Ec__Iterator1_t3806606775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TaskOrder/<TaskStart>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTaskStartU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2744890130 (U3CTaskStartU3Ec__Iterator1_t3806606775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TaskOrder/<TaskStart>c__Iterator1::MoveNext()
extern "C"  bool U3CTaskStartU3Ec__Iterator1_MoveNext_m933895392 (U3CTaskStartU3Ec__Iterator1_t3806606775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TaskOrder/<TaskStart>c__Iterator1::Dispose()
extern "C"  void U3CTaskStartU3Ec__Iterator1_Dispose_m1057029329 (U3CTaskStartU3Ec__Iterator1_t3806606775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TaskOrder/<TaskStart>c__Iterator1::Reset()
extern "C"  void U3CTaskStartU3Ec__Iterator1_Reset_m154898497 (U3CTaskStartU3Ec__Iterator1_t3806606775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
