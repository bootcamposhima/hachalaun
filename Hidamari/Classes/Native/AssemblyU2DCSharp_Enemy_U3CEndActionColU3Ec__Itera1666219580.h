﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// Enemy
struct Enemy_t67100520;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Enemy/<EndActionCol>c__Iterator3
struct  U3CEndActionColU3Ec__Iterator3_t1666219580  : public Il2CppObject
{
public:
	// UnityEngine.Vector3 Enemy/<EndActionCol>c__Iterator3::<p>__0
	Vector3_t4282066566  ___U3CpU3E__0_0;
	// System.Int32 Enemy/<EndActionCol>c__Iterator3::$PC
	int32_t ___U24PC_1;
	// System.Object Enemy/<EndActionCol>c__Iterator3::$current
	Il2CppObject * ___U24current_2;
	// Enemy Enemy/<EndActionCol>c__Iterator3::<>f__this
	Enemy_t67100520 * ___U3CU3Ef__this_3;

public:
	inline static int32_t get_offset_of_U3CpU3E__0_0() { return static_cast<int32_t>(offsetof(U3CEndActionColU3Ec__Iterator3_t1666219580, ___U3CpU3E__0_0)); }
	inline Vector3_t4282066566  get_U3CpU3E__0_0() const { return ___U3CpU3E__0_0; }
	inline Vector3_t4282066566 * get_address_of_U3CpU3E__0_0() { return &___U3CpU3E__0_0; }
	inline void set_U3CpU3E__0_0(Vector3_t4282066566  value)
	{
		___U3CpU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24PC_1() { return static_cast<int32_t>(offsetof(U3CEndActionColU3Ec__Iterator3_t1666219580, ___U24PC_1)); }
	inline int32_t get_U24PC_1() const { return ___U24PC_1; }
	inline int32_t* get_address_of_U24PC_1() { return &___U24PC_1; }
	inline void set_U24PC_1(int32_t value)
	{
		___U24PC_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CEndActionColU3Ec__Iterator3_t1666219580, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_3() { return static_cast<int32_t>(offsetof(U3CEndActionColU3Ec__Iterator3_t1666219580, ___U3CU3Ef__this_3)); }
	inline Enemy_t67100520 * get_U3CU3Ef__this_3() const { return ___U3CU3Ef__this_3; }
	inline Enemy_t67100520 ** get_address_of_U3CU3Ef__this_3() { return &___U3CU3Ef__this_3; }
	inline void set_U3CU3Ef__this_3(Enemy_t67100520 * value)
	{
		___U3CU3Ef__this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
