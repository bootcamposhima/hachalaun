﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<AIDatas>
struct DefaultComparer_t2517141970;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AIDatas4008896193.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<AIDatas>::.ctor()
extern "C"  void DefaultComparer__ctor_m801173925_gshared (DefaultComparer_t2517141970 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m801173925(__this, method) ((  void (*) (DefaultComparer_t2517141970 *, const MethodInfo*))DefaultComparer__ctor_m801173925_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<AIDatas>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m50953898_gshared (DefaultComparer_t2517141970 * __this, AIDatas_t4008896193  ___x0, AIDatas_t4008896193  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m50953898(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t2517141970 *, AIDatas_t4008896193 , AIDatas_t4008896193 , const MethodInfo*))DefaultComparer_Compare_m50953898_gshared)(__this, ___x0, ___y1, method)
