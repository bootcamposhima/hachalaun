﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "AssemblyU2DCSharp_Round79151470.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NormalRound
struct  NormalRound_t1394582727  : public Round_t79151470
{
public:
	// System.Int32 NormalRound::GameCount
	int32_t ___GameCount_11;
	// UnityEngine.GameObject NormalRound::_SkipSpeed
	GameObject_t3674682005 * ____SkipSpeed_12;
	// UnityEngine.GameObject NormalRound::_ModeCanvas
	GameObject_t3674682005 * ____ModeCanvas_13;
	// UnityEngine.GameObject NormalRound::_Go
	GameObject_t3674682005 * ____Go_14;
	// System.Boolean NormalRound::isNextGame
	bool ___isNextGame_15;
	// System.Int32 NormalRound::senkoHP
	int32_t ___senkoHP_16;
	// System.Int32 NormalRound::mode
	int32_t ___mode_17;

public:
	inline static int32_t get_offset_of_GameCount_11() { return static_cast<int32_t>(offsetof(NormalRound_t1394582727, ___GameCount_11)); }
	inline int32_t get_GameCount_11() const { return ___GameCount_11; }
	inline int32_t* get_address_of_GameCount_11() { return &___GameCount_11; }
	inline void set_GameCount_11(int32_t value)
	{
		___GameCount_11 = value;
	}

	inline static int32_t get_offset_of__SkipSpeed_12() { return static_cast<int32_t>(offsetof(NormalRound_t1394582727, ____SkipSpeed_12)); }
	inline GameObject_t3674682005 * get__SkipSpeed_12() const { return ____SkipSpeed_12; }
	inline GameObject_t3674682005 ** get_address_of__SkipSpeed_12() { return &____SkipSpeed_12; }
	inline void set__SkipSpeed_12(GameObject_t3674682005 * value)
	{
		____SkipSpeed_12 = value;
		Il2CppCodeGenWriteBarrier(&____SkipSpeed_12, value);
	}

	inline static int32_t get_offset_of__ModeCanvas_13() { return static_cast<int32_t>(offsetof(NormalRound_t1394582727, ____ModeCanvas_13)); }
	inline GameObject_t3674682005 * get__ModeCanvas_13() const { return ____ModeCanvas_13; }
	inline GameObject_t3674682005 ** get_address_of__ModeCanvas_13() { return &____ModeCanvas_13; }
	inline void set__ModeCanvas_13(GameObject_t3674682005 * value)
	{
		____ModeCanvas_13 = value;
		Il2CppCodeGenWriteBarrier(&____ModeCanvas_13, value);
	}

	inline static int32_t get_offset_of__Go_14() { return static_cast<int32_t>(offsetof(NormalRound_t1394582727, ____Go_14)); }
	inline GameObject_t3674682005 * get__Go_14() const { return ____Go_14; }
	inline GameObject_t3674682005 ** get_address_of__Go_14() { return &____Go_14; }
	inline void set__Go_14(GameObject_t3674682005 * value)
	{
		____Go_14 = value;
		Il2CppCodeGenWriteBarrier(&____Go_14, value);
	}

	inline static int32_t get_offset_of_isNextGame_15() { return static_cast<int32_t>(offsetof(NormalRound_t1394582727, ___isNextGame_15)); }
	inline bool get_isNextGame_15() const { return ___isNextGame_15; }
	inline bool* get_address_of_isNextGame_15() { return &___isNextGame_15; }
	inline void set_isNextGame_15(bool value)
	{
		___isNextGame_15 = value;
	}

	inline static int32_t get_offset_of_senkoHP_16() { return static_cast<int32_t>(offsetof(NormalRound_t1394582727, ___senkoHP_16)); }
	inline int32_t get_senkoHP_16() const { return ___senkoHP_16; }
	inline int32_t* get_address_of_senkoHP_16() { return &___senkoHP_16; }
	inline void set_senkoHP_16(int32_t value)
	{
		___senkoHP_16 = value;
	}

	inline static int32_t get_offset_of_mode_17() { return static_cast<int32_t>(offsetof(NormalRound_t1394582727, ___mode_17)); }
	inline int32_t get_mode_17() const { return ___mode_17; }
	inline int32_t* get_address_of_mode_17() { return &___mode_17; }
	inline void set_mode_17(int32_t value)
	{
		___mode_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
