﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen1281917335.h"
#include "AssemblyU2DCSharp_FixedID820738927.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IDManager
struct  IDManager_t1951742482  : public SingletonMonoBehaviour_1_t1281917335
{
public:
	// FixedID IDManager::fid
	FixedID_t820738927  ___fid_3;

public:
	inline static int32_t get_offset_of_fid_3() { return static_cast<int32_t>(offsetof(IDManager_t1951742482, ___fid_3)); }
	inline FixedID_t820738927  get_fid_3() const { return ___fid_3; }
	inline FixedID_t820738927 * get_address_of_fid_3() { return &___fid_3; }
	inline void set_fid_3(FixedID_t820738927  value)
	{
		___fid_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
