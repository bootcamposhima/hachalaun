﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CharManager
struct CharManager_t988180311;

#include "codegen/il2cpp-codegen.h"

// System.Void CharManager::.ctor()
extern "C"  void CharManager__ctor_m3624361460 (CharManager_t988180311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharManager::Start()
extern "C"  void CharManager_Start_m2571499252 (CharManager_t988180311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharManager::Awake()
extern "C"  void CharManager_Awake_m3861966679 (CharManager_t988180311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharManager::Update()
extern "C"  void CharManager_Update_m2412917657 (CharManager_t988180311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharManager::setScrollMove()
extern "C"  void CharManager_setScrollMove_m1332234354 (CharManager_t988180311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharManager::seTop()
extern "C"  void CharManager_seTop_m465309717 (CharManager_t988180311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharManager::EntranceAction()
extern "C"  void CharManager_EntranceAction_m1552296860 (CharManager_t988180311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharManager::LeavingAction()
extern "C"  void CharManager_LeavingAction_m4125684892 (CharManager_t988180311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharManager::StopTop()
extern "C"  void CharManager_StopTop_m1060109317 (CharManager_t988180311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharManager::UIStart()
extern "C"  void CharManager_UIStart_m860083616 (CharManager_t988180311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharManager::UIEnd()
extern "C"  void CharManager_UIEnd_m3089459993 (CharManager_t988180311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
