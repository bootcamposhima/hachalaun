﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StagePanelManager/<StagePanelLightAction>c__Iterator14
struct U3CStagePanelLightActionU3Ec__Iterator14_t3565141640;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void StagePanelManager/<StagePanelLightAction>c__Iterator14::.ctor()
extern "C"  void U3CStagePanelLightActionU3Ec__Iterator14__ctor_m3974098707 (U3CStagePanelLightActionU3Ec__Iterator14_t3565141640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object StagePanelManager/<StagePanelLightAction>c__Iterator14::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStagePanelLightActionU3Ec__Iterator14_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m386325225 (U3CStagePanelLightActionU3Ec__Iterator14_t3565141640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object StagePanelManager/<StagePanelLightAction>c__Iterator14::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStagePanelLightActionU3Ec__Iterator14_System_Collections_IEnumerator_get_Current_m313905277 (U3CStagePanelLightActionU3Ec__Iterator14_t3565141640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean StagePanelManager/<StagePanelLightAction>c__Iterator14::MoveNext()
extern "C"  bool U3CStagePanelLightActionU3Ec__Iterator14_MoveNext_m753433321 (U3CStagePanelLightActionU3Ec__Iterator14_t3565141640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StagePanelManager/<StagePanelLightAction>c__Iterator14::Dispose()
extern "C"  void U3CStagePanelLightActionU3Ec__Iterator14_Dispose_m781214352 (U3CStagePanelLightActionU3Ec__Iterator14_t3565141640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StagePanelManager/<StagePanelLightAction>c__Iterator14::Reset()
extern "C"  void U3CStagePanelLightActionU3Ec__Iterator14_Reset_m1620531648 (U3CStagePanelLightActionU3Ec__Iterator14_t3565141640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
