﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3733267447MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Char>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m1853360963(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2383646621 *, Dictionary_2_t3683040908 *, const MethodInfo*))ValueCollection__ctor_m1935621361_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Char>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m4085660815(__this, ___item0, method) ((  void (*) (ValueCollection_t2383646621 *, Il2CppChar, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m624631073_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Char>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3002517720(__this, method) ((  void (*) (ValueCollection_t2383646621 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3880174186_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Char>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3022014583(__this, ___item0, method) ((  bool (*) (ValueCollection_t2383646621 *, Il2CppChar, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2845504293_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Char>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3566146268(__this, ___item0, method) ((  bool (*) (ValueCollection_t2383646621 *, Il2CppChar, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1014010122_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Char>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m520654758(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2383646621 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3008812856_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Char>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m2420784092(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2383646621 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1243932398_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Char>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1397975639(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2383646621 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m360125929_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Char>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1297190442(__this, method) ((  bool (*) (ValueCollection_t2383646621 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1120680152_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Char>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3865854730(__this, method) ((  bool (*) (ValueCollection_t2383646621 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3355378360_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Char>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1582005558(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2383646621 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2518357860_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Char>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m650368714(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2383646621 *, CharU5BU5D_t3324145743*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m2788789496_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Char>::GetEnumerator()
#define ValueCollection_GetEnumerator_m3600790637(__this, method) ((  Enumerator_t1614874316  (*) (ValueCollection_t2383646621 *, const MethodInfo*))ValueCollection_GetEnumerator_m533181211_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Char>::get_Count()
#define ValueCollection_get_Count_m4076411600(__this, method) ((  int32_t (*) (ValueCollection_t2383646621 *, const MethodInfo*))ValueCollection_get_Count_m1062707326_gshared)(__this, method)
