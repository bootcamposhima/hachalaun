﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<AIData>
struct List_1_t3298620098;

#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AIDatas
struct  AIDatas_t4008896193 
{
public:
	// System.Collections.Generic.List`1<AIData> AIDatas::_dataList
	List_1_t3298620098 * ____dataList_0;

public:
	inline static int32_t get_offset_of__dataList_0() { return static_cast<int32_t>(offsetof(AIDatas_t4008896193, ____dataList_0)); }
	inline List_1_t3298620098 * get__dataList_0() const { return ____dataList_0; }
	inline List_1_t3298620098 ** get_address_of__dataList_0() { return &____dataList_0; }
	inline void set__dataList_0(List_1_t3298620098 * value)
	{
		____dataList_0 = value;
		Il2CppCodeGenWriteBarrier(&____dataList_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: AIDatas
struct AIDatas_t4008896193_marshaled_pinvoke
{
	List_1_t3298620098 * ____dataList_0;
};
// Native definition for marshalling of: AIDatas
struct AIDatas_t4008896193_marshaled_com
{
	List_1_t3298620098 * ____dataList_0;
};
