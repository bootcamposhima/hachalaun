﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// AIChoiceAction
struct AIChoiceAction_t410758015;
// System.Collections.Generic.List`1<AIDatas>
struct List_1_t1082114449;
// Factory
struct Factory_t572770538;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AIChoiceManager
struct  AIChoiceManager_t1845660548  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject AIChoiceManager::_scroll
	GameObject_t3674682005 * ____scroll_2;
	// UnityEngine.GameObject AIChoiceManager::_AIscroll
	GameObject_t3674682005 * ____AIscroll_3;
	// UnityEngine.GameObject AIChoiceManager::_okbutton
	GameObject_t3674682005 * ____okbutton_4;
	// UnityEngine.GameObject AIChoiceManager::_backbutton
	GameObject_t3674682005 * ____backbutton_5;
	// UnityEngine.GameObject AIChoiceManager::_directionobject
	GameObject_t3674682005 * ____directionobject_6;
	// AIChoiceAction AIChoiceManager::_aichoiceaction
	AIChoiceAction_t410758015 * ____aichoiceaction_7;
	// System.Collections.Generic.List`1<AIDatas> AIChoiceManager::_list
	List_1_t1082114449 * ____list_8;
	// Factory AIChoiceManager::fc
	Factory_t572770538 * ___fc_9;
	// System.Int32 AIChoiceManager::_direction
	int32_t ____direction_10;
	// UnityEngine.GameObject AIChoiceManager::_node
	GameObject_t3674682005 * ____node_11;
	// System.Int32 AIChoiceManager::_top
	int32_t ____top_12;

public:
	inline static int32_t get_offset_of__scroll_2() { return static_cast<int32_t>(offsetof(AIChoiceManager_t1845660548, ____scroll_2)); }
	inline GameObject_t3674682005 * get__scroll_2() const { return ____scroll_2; }
	inline GameObject_t3674682005 ** get_address_of__scroll_2() { return &____scroll_2; }
	inline void set__scroll_2(GameObject_t3674682005 * value)
	{
		____scroll_2 = value;
		Il2CppCodeGenWriteBarrier(&____scroll_2, value);
	}

	inline static int32_t get_offset_of__AIscroll_3() { return static_cast<int32_t>(offsetof(AIChoiceManager_t1845660548, ____AIscroll_3)); }
	inline GameObject_t3674682005 * get__AIscroll_3() const { return ____AIscroll_3; }
	inline GameObject_t3674682005 ** get_address_of__AIscroll_3() { return &____AIscroll_3; }
	inline void set__AIscroll_3(GameObject_t3674682005 * value)
	{
		____AIscroll_3 = value;
		Il2CppCodeGenWriteBarrier(&____AIscroll_3, value);
	}

	inline static int32_t get_offset_of__okbutton_4() { return static_cast<int32_t>(offsetof(AIChoiceManager_t1845660548, ____okbutton_4)); }
	inline GameObject_t3674682005 * get__okbutton_4() const { return ____okbutton_4; }
	inline GameObject_t3674682005 ** get_address_of__okbutton_4() { return &____okbutton_4; }
	inline void set__okbutton_4(GameObject_t3674682005 * value)
	{
		____okbutton_4 = value;
		Il2CppCodeGenWriteBarrier(&____okbutton_4, value);
	}

	inline static int32_t get_offset_of__backbutton_5() { return static_cast<int32_t>(offsetof(AIChoiceManager_t1845660548, ____backbutton_5)); }
	inline GameObject_t3674682005 * get__backbutton_5() const { return ____backbutton_5; }
	inline GameObject_t3674682005 ** get_address_of__backbutton_5() { return &____backbutton_5; }
	inline void set__backbutton_5(GameObject_t3674682005 * value)
	{
		____backbutton_5 = value;
		Il2CppCodeGenWriteBarrier(&____backbutton_5, value);
	}

	inline static int32_t get_offset_of__directionobject_6() { return static_cast<int32_t>(offsetof(AIChoiceManager_t1845660548, ____directionobject_6)); }
	inline GameObject_t3674682005 * get__directionobject_6() const { return ____directionobject_6; }
	inline GameObject_t3674682005 ** get_address_of__directionobject_6() { return &____directionobject_6; }
	inline void set__directionobject_6(GameObject_t3674682005 * value)
	{
		____directionobject_6 = value;
		Il2CppCodeGenWriteBarrier(&____directionobject_6, value);
	}

	inline static int32_t get_offset_of__aichoiceaction_7() { return static_cast<int32_t>(offsetof(AIChoiceManager_t1845660548, ____aichoiceaction_7)); }
	inline AIChoiceAction_t410758015 * get__aichoiceaction_7() const { return ____aichoiceaction_7; }
	inline AIChoiceAction_t410758015 ** get_address_of__aichoiceaction_7() { return &____aichoiceaction_7; }
	inline void set__aichoiceaction_7(AIChoiceAction_t410758015 * value)
	{
		____aichoiceaction_7 = value;
		Il2CppCodeGenWriteBarrier(&____aichoiceaction_7, value);
	}

	inline static int32_t get_offset_of__list_8() { return static_cast<int32_t>(offsetof(AIChoiceManager_t1845660548, ____list_8)); }
	inline List_1_t1082114449 * get__list_8() const { return ____list_8; }
	inline List_1_t1082114449 ** get_address_of__list_8() { return &____list_8; }
	inline void set__list_8(List_1_t1082114449 * value)
	{
		____list_8 = value;
		Il2CppCodeGenWriteBarrier(&____list_8, value);
	}

	inline static int32_t get_offset_of_fc_9() { return static_cast<int32_t>(offsetof(AIChoiceManager_t1845660548, ___fc_9)); }
	inline Factory_t572770538 * get_fc_9() const { return ___fc_9; }
	inline Factory_t572770538 ** get_address_of_fc_9() { return &___fc_9; }
	inline void set_fc_9(Factory_t572770538 * value)
	{
		___fc_9 = value;
		Il2CppCodeGenWriteBarrier(&___fc_9, value);
	}

	inline static int32_t get_offset_of__direction_10() { return static_cast<int32_t>(offsetof(AIChoiceManager_t1845660548, ____direction_10)); }
	inline int32_t get__direction_10() const { return ____direction_10; }
	inline int32_t* get_address_of__direction_10() { return &____direction_10; }
	inline void set__direction_10(int32_t value)
	{
		____direction_10 = value;
	}

	inline static int32_t get_offset_of__node_11() { return static_cast<int32_t>(offsetof(AIChoiceManager_t1845660548, ____node_11)); }
	inline GameObject_t3674682005 * get__node_11() const { return ____node_11; }
	inline GameObject_t3674682005 ** get_address_of__node_11() { return &____node_11; }
	inline void set__node_11(GameObject_t3674682005 * value)
	{
		____node_11 = value;
		Il2CppCodeGenWriteBarrier(&____node_11, value);
	}

	inline static int32_t get_offset_of__top_12() { return static_cast<int32_t>(offsetof(AIChoiceManager_t1845660548, ____top_12)); }
	inline int32_t get__top_12() const { return ____top_12; }
	inline int32_t* get_address_of__top_12() { return &____top_12; }
	inline void set__top_12(int32_t value)
	{
		____top_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
