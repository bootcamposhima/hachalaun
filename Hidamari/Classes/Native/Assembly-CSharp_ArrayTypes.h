﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// TaskOrder
struct TaskOrder_t2170039465;
// Task
struct Task_t2599333;
// Enemy
struct Enemy_t67100520;
// JSONObject
struct JSONObject_t1752376903;
// SocketIO.Ack
struct Ack_t4240269559;
// SocketIO.SocketIOEvent
struct SocketIOEvent_t4011854063;
// SocketIO.Packet
struct Packet_t1660110912;
// WebSocketSharp.Net.Chunk
struct Chunk_t1399190359;
// WebSocketSharp.Net.Cookie
struct Cookie_t2077085446;
// WebSocketSharp.Net.ListenerPrefix
struct ListenerPrefix_t2663314696;
// WebSocketSharp.Net.HttpListener
struct HttpListener_t398944510;
// WebSocketSharp.Net.HttpConnection
struct HttpConnection_t602292776;
// WebSocketSharp.Net.EndPointListener
struct EndPointListener_t3188089579;
// WebSocketSharp.Net.HttpListenerContext
struct HttpListenerContext_t3744659101;
// WebSocketSharp.Net.ListenerAsyncResult
struct ListenerAsyncResult_t609216079;
// WebSocketSharp.Net.HttpHeaderInfo
struct HttpHeaderInfo_t3355144229;
// WebSocketSharp.MessageEventArgs
struct MessageEventArgs_t36945740;

#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_AIData1930434546.h"
#include "AssemblyU2DCSharp_AIDatas4008896193.h"
#include "AssemblyU2DCSharp_TaskOrder2170039465.h"
#include "AssemblyU2DCSharp_Task2599333.h"
#include "AssemblyU2DCSharp_Enemy67100520.h"
#include "AssemblyU2DCSharp_JSONObject1752376903.h"
#include "AssemblyU2DCSharp_SocketIO_Ack4240269559.h"
#include "AssemblyU2DCSharp_SocketIO_SocketIOEvent4011854063.h"
#include "AssemblyU2DCSharp_SocketIO_Packet1660110912.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_Chunk1399190359.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_Cookie2077085446.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_ListenerPrefi2663314696.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_HttpListener398944510.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_HttpConnection602292776.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_EndPointListe3188089579.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_HttpListenerC3744659101.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_ListenerAsyncR609216079.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_HttpHeaderInf3355144229.h"
#include "AssemblyU2DCSharp_WebSocketSharp_MessageEventArgs36945740.h"
#include "AssemblyU2DCSharp_WebSocketSharp_CompressionMethod2226596781.h"

#pragma once
// AIData[]
struct AIDataU5BU5D_t2017435655  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) AIData_t1930434546  m_Items[1];

public:
	inline AIData_t1930434546  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline AIData_t1930434546 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, AIData_t1930434546  value)
	{
		m_Items[index] = value;
	}
};
// AIDatas[]
struct AIDatasU5BU5D_t2487978268  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) AIDatas_t4008896193  m_Items[1];

public:
	inline AIDatas_t4008896193  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline AIDatas_t4008896193 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, AIDatas_t4008896193  value)
	{
		m_Items[index] = value;
	}
};
// TaskOrder[]
struct TaskOrderU5BU5D_t2344298132  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TaskOrder_t2170039465 * m_Items[1];

public:
	inline TaskOrder_t2170039465 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TaskOrder_t2170039465 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TaskOrder_t2170039465 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Task[]
struct TaskU5BU5D_t4067068456  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Task_t2599333 * m_Items[1];

public:
	inline Task_t2599333 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Task_t2599333 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Task_t2599333 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Enemy[]
struct EnemyU5BU5D_t3805539833  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Enemy_t67100520 * m_Items[1];

public:
	inline Enemy_t67100520 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Enemy_t67100520 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Enemy_t67100520 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// JSONObject[]
struct JSONObjectU5BU5D_t1578151102  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) JSONObject_t1752376903 * m_Items[1];

public:
	inline JSONObject_t1752376903 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JSONObject_t1752376903 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JSONObject_t1752376903 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SocketIO.Ack[]
struct AckU5BU5D_t3264495950  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Ack_t4240269559 * m_Items[1];

public:
	inline Ack_t4240269559 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Ack_t4240269559 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Ack_t4240269559 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SocketIO.SocketIOEvent[]
struct SocketIOEventU5BU5D_t3010781814  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SocketIOEvent_t4011854063 * m_Items[1];

public:
	inline SocketIOEvent_t4011854063 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SocketIOEvent_t4011854063 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SocketIOEvent_t4011854063 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SocketIO.Packet[]
struct PacketU5BU5D_t676810433  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Packet_t1660110912 * m_Items[1];

public:
	inline Packet_t1660110912 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Packet_t1660110912 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Packet_t1660110912 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// WebSocketSharp.Net.Chunk[]
struct ChunkU5BU5D_t53098862  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Chunk_t1399190359 * m_Items[1];

public:
	inline Chunk_t1399190359 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Chunk_t1399190359 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Chunk_t1399190359 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// WebSocketSharp.Net.Cookie[]
struct CookieU5BU5D_t4193117731  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Cookie_t2077085446 * m_Items[1];

public:
	inline Cookie_t2077085446 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Cookie_t2077085446 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Cookie_t2077085446 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// WebSocketSharp.Net.ListenerPrefix[]
struct ListenerPrefixU5BU5D_t3459952089  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ListenerPrefix_t2663314696 * m_Items[1];

public:
	inline ListenerPrefix_t2663314696 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ListenerPrefix_t2663314696 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ListenerPrefix_t2663314696 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// WebSocketSharp.Net.HttpListener[]
struct HttpListenerU5BU5D_t3625160011  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) HttpListener_t398944510 * m_Items[1];

public:
	inline HttpListener_t398944510 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline HttpListener_t398944510 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, HttpListener_t398944510 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// WebSocketSharp.Net.HttpConnection[]
struct HttpConnectionU5BU5D_t1726474297  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) HttpConnection_t602292776 * m_Items[1];

public:
	inline HttpConnection_t602292776 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline HttpConnection_t602292776 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, HttpConnection_t602292776 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// WebSocketSharp.Net.EndPointListener[]
struct EndPointListenerU5BU5D_t3884799242  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) EndPointListener_t3188089579 * m_Items[1];

public:
	inline EndPointListener_t3188089579 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline EndPointListener_t3188089579 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, EndPointListener_t3188089579 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// WebSocketSharp.Net.HttpListenerContext[]
struct HttpListenerContextU5BU5D_t838488656  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) HttpListenerContext_t3744659101 * m_Items[1];

public:
	inline HttpListenerContext_t3744659101 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline HttpListenerContext_t3744659101 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, HttpListenerContext_t3744659101 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// WebSocketSharp.Net.ListenerAsyncResult[]
struct ListenerAsyncResultU5BU5D_t759038870  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ListenerAsyncResult_t609216079 * m_Items[1];

public:
	inline ListenerAsyncResult_t609216079 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ListenerAsyncResult_t609216079 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ListenerAsyncResult_t609216079 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// WebSocketSharp.Net.HttpHeaderInfo[]
struct HttpHeaderInfoU5BU5D_t3241302440  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) HttpHeaderInfo_t3355144229 * m_Items[1];

public:
	inline HttpHeaderInfo_t3355144229 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline HttpHeaderInfo_t3355144229 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, HttpHeaderInfo_t3355144229 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// WebSocketSharp.MessageEventArgs[]
struct MessageEventArgsU5BU5D_t3247095301  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MessageEventArgs_t36945740 * m_Items[1];

public:
	inline MessageEventArgs_t36945740 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MessageEventArgs_t36945740 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MessageEventArgs_t36945740 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// WebSocketSharp.CompressionMethod[]
struct CompressionMethodU5BU5D_t88594176  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
