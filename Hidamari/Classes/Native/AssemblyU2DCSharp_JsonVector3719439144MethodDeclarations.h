﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JsonVector3
struct JsonVector3_t719439144;
struct JsonVector3_t719439144_marshaled_pinvoke;
struct JsonVector3_t719439144_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JsonVector3719439144.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void JsonVector3::.ctor(UnityEngine.Vector3)
extern "C"  void JsonVector3__ctor_m564422582 (JsonVector3_t719439144 * __this, Vector3_t4282066566  ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JsonVector3::.ctor(System.Int32)
extern "C"  void JsonVector3__ctor_m2942102548 (JsonVector3_t719439144 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct JsonVector3_t719439144;
struct JsonVector3_t719439144_marshaled_pinvoke;

extern "C" void JsonVector3_t719439144_marshal_pinvoke(const JsonVector3_t719439144& unmarshaled, JsonVector3_t719439144_marshaled_pinvoke& marshaled);
extern "C" void JsonVector3_t719439144_marshal_pinvoke_back(const JsonVector3_t719439144_marshaled_pinvoke& marshaled, JsonVector3_t719439144& unmarshaled);
extern "C" void JsonVector3_t719439144_marshal_pinvoke_cleanup(JsonVector3_t719439144_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct JsonVector3_t719439144;
struct JsonVector3_t719439144_marshaled_com;

extern "C" void JsonVector3_t719439144_marshal_com(const JsonVector3_t719439144& unmarshaled, JsonVector3_t719439144_marshaled_com& marshaled);
extern "C" void JsonVector3_t719439144_marshal_com_back(const JsonVector3_t719439144_marshaled_com& marshaled, JsonVector3_t719439144& unmarshaled);
extern "C" void JsonVector3_t719439144_marshal_com_cleanup(JsonVector3_t719439144_marshaled_com& marshaled);
