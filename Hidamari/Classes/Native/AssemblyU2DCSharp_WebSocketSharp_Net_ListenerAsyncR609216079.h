﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.AsyncCallback
struct AsyncCallback_t1369114871;
// WebSocketSharp.Net.HttpListenerContext
struct HttpListenerContext_t3744659101;
// System.Exception
struct Exception_t3991598821;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t924017833;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.ListenerAsyncResult
struct  ListenerAsyncResult_t609216079  : public Il2CppObject
{
public:
	// System.AsyncCallback WebSocketSharp.Net.ListenerAsyncResult::_callback
	AsyncCallback_t1369114871 * ____callback_0;
	// System.Boolean WebSocketSharp.Net.ListenerAsyncResult::_completed
	bool ____completed_1;
	// WebSocketSharp.Net.HttpListenerContext WebSocketSharp.Net.ListenerAsyncResult::_context
	HttpListenerContext_t3744659101 * ____context_2;
	// System.Exception WebSocketSharp.Net.ListenerAsyncResult::_exception
	Exception_t3991598821 * ____exception_3;
	// System.Threading.ManualResetEvent WebSocketSharp.Net.ListenerAsyncResult::_waitHandle
	ManualResetEvent_t924017833 * ____waitHandle_4;
	// System.Object WebSocketSharp.Net.ListenerAsyncResult::_state
	Il2CppObject * ____state_5;
	// System.Object WebSocketSharp.Net.ListenerAsyncResult::_sync
	Il2CppObject * ____sync_6;
	// System.Boolean WebSocketSharp.Net.ListenerAsyncResult::_syncCompleted
	bool ____syncCompleted_7;
	// System.Boolean WebSocketSharp.Net.ListenerAsyncResult::EndCalled
	bool ___EndCalled_8;
	// System.Boolean WebSocketSharp.Net.ListenerAsyncResult::InGet
	bool ___InGet_9;

public:
	inline static int32_t get_offset_of__callback_0() { return static_cast<int32_t>(offsetof(ListenerAsyncResult_t609216079, ____callback_0)); }
	inline AsyncCallback_t1369114871 * get__callback_0() const { return ____callback_0; }
	inline AsyncCallback_t1369114871 ** get_address_of__callback_0() { return &____callback_0; }
	inline void set__callback_0(AsyncCallback_t1369114871 * value)
	{
		____callback_0 = value;
		Il2CppCodeGenWriteBarrier(&____callback_0, value);
	}

	inline static int32_t get_offset_of__completed_1() { return static_cast<int32_t>(offsetof(ListenerAsyncResult_t609216079, ____completed_1)); }
	inline bool get__completed_1() const { return ____completed_1; }
	inline bool* get_address_of__completed_1() { return &____completed_1; }
	inline void set__completed_1(bool value)
	{
		____completed_1 = value;
	}

	inline static int32_t get_offset_of__context_2() { return static_cast<int32_t>(offsetof(ListenerAsyncResult_t609216079, ____context_2)); }
	inline HttpListenerContext_t3744659101 * get__context_2() const { return ____context_2; }
	inline HttpListenerContext_t3744659101 ** get_address_of__context_2() { return &____context_2; }
	inline void set__context_2(HttpListenerContext_t3744659101 * value)
	{
		____context_2 = value;
		Il2CppCodeGenWriteBarrier(&____context_2, value);
	}

	inline static int32_t get_offset_of__exception_3() { return static_cast<int32_t>(offsetof(ListenerAsyncResult_t609216079, ____exception_3)); }
	inline Exception_t3991598821 * get__exception_3() const { return ____exception_3; }
	inline Exception_t3991598821 ** get_address_of__exception_3() { return &____exception_3; }
	inline void set__exception_3(Exception_t3991598821 * value)
	{
		____exception_3 = value;
		Il2CppCodeGenWriteBarrier(&____exception_3, value);
	}

	inline static int32_t get_offset_of__waitHandle_4() { return static_cast<int32_t>(offsetof(ListenerAsyncResult_t609216079, ____waitHandle_4)); }
	inline ManualResetEvent_t924017833 * get__waitHandle_4() const { return ____waitHandle_4; }
	inline ManualResetEvent_t924017833 ** get_address_of__waitHandle_4() { return &____waitHandle_4; }
	inline void set__waitHandle_4(ManualResetEvent_t924017833 * value)
	{
		____waitHandle_4 = value;
		Il2CppCodeGenWriteBarrier(&____waitHandle_4, value);
	}

	inline static int32_t get_offset_of__state_5() { return static_cast<int32_t>(offsetof(ListenerAsyncResult_t609216079, ____state_5)); }
	inline Il2CppObject * get__state_5() const { return ____state_5; }
	inline Il2CppObject ** get_address_of__state_5() { return &____state_5; }
	inline void set__state_5(Il2CppObject * value)
	{
		____state_5 = value;
		Il2CppCodeGenWriteBarrier(&____state_5, value);
	}

	inline static int32_t get_offset_of__sync_6() { return static_cast<int32_t>(offsetof(ListenerAsyncResult_t609216079, ____sync_6)); }
	inline Il2CppObject * get__sync_6() const { return ____sync_6; }
	inline Il2CppObject ** get_address_of__sync_6() { return &____sync_6; }
	inline void set__sync_6(Il2CppObject * value)
	{
		____sync_6 = value;
		Il2CppCodeGenWriteBarrier(&____sync_6, value);
	}

	inline static int32_t get_offset_of__syncCompleted_7() { return static_cast<int32_t>(offsetof(ListenerAsyncResult_t609216079, ____syncCompleted_7)); }
	inline bool get__syncCompleted_7() const { return ____syncCompleted_7; }
	inline bool* get_address_of__syncCompleted_7() { return &____syncCompleted_7; }
	inline void set__syncCompleted_7(bool value)
	{
		____syncCompleted_7 = value;
	}

	inline static int32_t get_offset_of_EndCalled_8() { return static_cast<int32_t>(offsetof(ListenerAsyncResult_t609216079, ___EndCalled_8)); }
	inline bool get_EndCalled_8() const { return ___EndCalled_8; }
	inline bool* get_address_of_EndCalled_8() { return &___EndCalled_8; }
	inline void set_EndCalled_8(bool value)
	{
		___EndCalled_8 = value;
	}

	inline static int32_t get_offset_of_InGet_9() { return static_cast<int32_t>(offsetof(ListenerAsyncResult_t609216079, ___InGet_9)); }
	inline bool get_InGet_9() const { return ___InGet_9; }
	inline bool* get_address_of_InGet_9() { return &___InGet_9; }
	inline void set_InGet_9(bool value)
	{
		___InGet_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
