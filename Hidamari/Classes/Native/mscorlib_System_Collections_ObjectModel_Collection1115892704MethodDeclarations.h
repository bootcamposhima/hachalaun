﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<AIData>
struct Collection_1_t1115892704;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// AIData[]
struct AIDataU5BU5D_t2017435655;
// System.Collections.Generic.IEnumerator`1<AIData>
struct IEnumerator_1_t3842299595;
// System.Collections.Generic.IList`1<AIData>
struct IList_1_t330114453;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_AIData1930434546.h"

// System.Void System.Collections.ObjectModel.Collection`1<AIData>::.ctor()
extern "C"  void Collection_1__ctor_m386015225_gshared (Collection_1_t1115892704 * __this, const MethodInfo* method);
#define Collection_1__ctor_m386015225(__this, method) ((  void (*) (Collection_1_t1115892704 *, const MethodInfo*))Collection_1__ctor_m386015225_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AIData>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1539316894_gshared (Collection_1_t1115892704 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1539316894(__this, method) ((  bool (*) (Collection_1_t1115892704 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1539316894_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<AIData>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m2331204459_gshared (Collection_1_t1115892704 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m2331204459(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1115892704 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m2331204459_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<AIData>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m3061892154_gshared (Collection_1_t1115892704 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m3061892154(__this, method) ((  Il2CppObject * (*) (Collection_1_t1115892704 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m3061892154_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<AIData>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m1261398083_gshared (Collection_1_t1115892704 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m1261398083(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1115892704 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1261398083_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AIData>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m3540573405_gshared (Collection_1_t1115892704 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m3540573405(__this, ___value0, method) ((  bool (*) (Collection_1_t1115892704 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m3540573405_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<AIData>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m2284569883_gshared (Collection_1_t1115892704 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m2284569883(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1115892704 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m2284569883_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AIData>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m3717001742_gshared (Collection_1_t1115892704 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m3717001742(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1115892704 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m3717001742_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<AIData>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m1478522970_gshared (Collection_1_t1115892704 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m1478522970(__this, ___value0, method) ((  void (*) (Collection_1_t1115892704 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1478522970_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AIData>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m1410429279_gshared (Collection_1_t1115892704 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m1410429279(__this, method) ((  bool (*) (Collection_1_t1115892704 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1410429279_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<AIData>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m2102524689_gshared (Collection_1_t1115892704 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m2102524689(__this, method) ((  Il2CppObject * (*) (Collection_1_t1115892704 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m2102524689_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AIData>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m2361046540_gshared (Collection_1_t1115892704 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m2361046540(__this, method) ((  bool (*) (Collection_1_t1115892704 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m2361046540_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AIData>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m668718957_gshared (Collection_1_t1115892704 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m668718957(__this, method) ((  bool (*) (Collection_1_t1115892704 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m668718957_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<AIData>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m2957208664_gshared (Collection_1_t1115892704 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m2957208664(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t1115892704 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m2957208664_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AIData>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m3534803557_gshared (Collection_1_t1115892704 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m3534803557(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1115892704 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m3534803557_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<AIData>::Add(T)
extern "C"  void Collection_1_Add_m1942256998_gshared (Collection_1_t1115892704 * __this, AIData_t1930434546  ___item0, const MethodInfo* method);
#define Collection_1_Add_m1942256998(__this, ___item0, method) ((  void (*) (Collection_1_t1115892704 *, AIData_t1930434546 , const MethodInfo*))Collection_1_Add_m1942256998_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AIData>::Clear()
extern "C"  void Collection_1_Clear_m2087115812_gshared (Collection_1_t1115892704 * __this, const MethodInfo* method);
#define Collection_1_Clear_m2087115812(__this, method) ((  void (*) (Collection_1_t1115892704 *, const MethodInfo*))Collection_1_Clear_m2087115812_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<AIData>::ClearItems()
extern "C"  void Collection_1_ClearItems_m1381808990_gshared (Collection_1_t1115892704 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m1381808990(__this, method) ((  void (*) (Collection_1_t1115892704 *, const MethodInfo*))Collection_1_ClearItems_m1381808990_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AIData>::Contains(T)
extern "C"  bool Collection_1_Contains_m3009173014_gshared (Collection_1_t1115892704 * __this, AIData_t1930434546  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m3009173014(__this, ___item0, method) ((  bool (*) (Collection_1_t1115892704 *, AIData_t1930434546 , const MethodInfo*))Collection_1_Contains_m3009173014_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AIData>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m2072516566_gshared (Collection_1_t1115892704 * __this, AIDataU5BU5D_t2017435655* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m2072516566(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1115892704 *, AIDataU5BU5D_t2017435655*, int32_t, const MethodInfo*))Collection_1_CopyTo_m2072516566_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<AIData>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m549411181_gshared (Collection_1_t1115892704 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m549411181(__this, method) ((  Il2CppObject* (*) (Collection_1_t1115892704 *, const MethodInfo*))Collection_1_GetEnumerator_m549411181_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<AIData>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m2854263330_gshared (Collection_1_t1115892704 * __this, AIData_t1930434546  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m2854263330(__this, ___item0, method) ((  int32_t (*) (Collection_1_t1115892704 *, AIData_t1930434546 , const MethodInfo*))Collection_1_IndexOf_m2854263330_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AIData>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m3201706957_gshared (Collection_1_t1115892704 * __this, int32_t ___index0, AIData_t1930434546  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m3201706957(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1115892704 *, int32_t, AIData_t1930434546 , const MethodInfo*))Collection_1_Insert_m3201706957_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<AIData>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m784903040_gshared (Collection_1_t1115892704 * __this, int32_t ___index0, AIData_t1930434546  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m784903040(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1115892704 *, int32_t, AIData_t1930434546 , const MethodInfo*))Collection_1_InsertItem_m784903040_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AIData>::Remove(T)
extern "C"  bool Collection_1_Remove_m1388028817_gshared (Collection_1_t1115892704 * __this, AIData_t1930434546  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m1388028817(__this, ___item0, method) ((  bool (*) (Collection_1_t1115892704 *, AIData_t1930434546 , const MethodInfo*))Collection_1_Remove_m1388028817_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AIData>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m1075559827_gshared (Collection_1_t1115892704 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m1075559827(__this, ___index0, method) ((  void (*) (Collection_1_t1115892704 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1075559827_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AIData>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m1425500403_gshared (Collection_1_t1115892704 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m1425500403(__this, ___index0, method) ((  void (*) (Collection_1_t1115892704 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1425500403_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<AIData>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m2400124825_gshared (Collection_1_t1115892704 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m2400124825(__this, method) ((  int32_t (*) (Collection_1_t1115892704 *, const MethodInfo*))Collection_1_get_Count_m2400124825_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<AIData>::get_Item(System.Int32)
extern "C"  AIData_t1930434546  Collection_1_get_Item_m3429737273_gshared (Collection_1_t1115892704 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m3429737273(__this, ___index0, method) ((  AIData_t1930434546  (*) (Collection_1_t1115892704 *, int32_t, const MethodInfo*))Collection_1_get_Item_m3429737273_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AIData>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m3453398372_gshared (Collection_1_t1115892704 * __this, int32_t ___index0, AIData_t1930434546  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m3453398372(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1115892704 *, int32_t, AIData_t1930434546 , const MethodInfo*))Collection_1_set_Item_m3453398372_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<AIData>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m1785257621_gshared (Collection_1_t1115892704 * __this, int32_t ___index0, AIData_t1930434546  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m1785257621(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1115892704 *, int32_t, AIData_t1930434546 , const MethodInfo*))Collection_1_SetItem_m1785257621_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AIData>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m3212209494_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m3212209494(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m3212209494_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<AIData>::ConvertItem(System.Object)
extern "C"  AIData_t1930434546  Collection_1_ConvertItem_m490802584_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m490802584(__this /* static, unused */, ___item0, method) ((  AIData_t1930434546  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m490802584_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AIData>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m2977211542_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m2977211542(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m2977211542_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AIData>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m3024924590_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m3024924590(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m3024924590_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AIData>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m987850161_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m987850161(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m987850161_gshared)(__this /* static, unused */, ___list0, method)
