﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2662109048;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HPManager
struct  HPManager_t3198084485  : public MonoBehaviour_t667441552
{
public:
	// System.Int32 HPManager::_defaulthpnum
	int32_t ____defaulthpnum_2;
	// System.Int32 HPManager::_hpnum
	int32_t ____hpnum_3;
	// UnityEngine.GameObject HPManager::_hpgauge
	GameObject_t3674682005 * ____hpgauge_4;
	// UnityEngine.GameObject HPManager::_redhpgauge
	GameObject_t3674682005 * ____redhpgauge_5;
	// UnityEngine.GameObject HPManager::_hphert
	GameObject_t3674682005 * ____hphert_6;
	// UnityEngine.GameObject HPManager::_redhphert
	GameObject_t3674682005 * ____redhphert_7;
	// UnityEngine.GameObject HPManager::_hptext
	GameObject_t3674682005 * ____hptext_8;
	// System.Collections.IEnumerator HPManager::_ie
	Il2CppObject * ____ie_9;
	// System.Collections.IEnumerator HPManager::_ie2
	Il2CppObject * ____ie2_10;
	// UnityEngine.GameObject[] HPManager::_bomPar
	GameObjectU5BU5D_t2662109048* ____bomPar_11;

public:
	inline static int32_t get_offset_of__defaulthpnum_2() { return static_cast<int32_t>(offsetof(HPManager_t3198084485, ____defaulthpnum_2)); }
	inline int32_t get__defaulthpnum_2() const { return ____defaulthpnum_2; }
	inline int32_t* get_address_of__defaulthpnum_2() { return &____defaulthpnum_2; }
	inline void set__defaulthpnum_2(int32_t value)
	{
		____defaulthpnum_2 = value;
	}

	inline static int32_t get_offset_of__hpnum_3() { return static_cast<int32_t>(offsetof(HPManager_t3198084485, ____hpnum_3)); }
	inline int32_t get__hpnum_3() const { return ____hpnum_3; }
	inline int32_t* get_address_of__hpnum_3() { return &____hpnum_3; }
	inline void set__hpnum_3(int32_t value)
	{
		____hpnum_3 = value;
	}

	inline static int32_t get_offset_of__hpgauge_4() { return static_cast<int32_t>(offsetof(HPManager_t3198084485, ____hpgauge_4)); }
	inline GameObject_t3674682005 * get__hpgauge_4() const { return ____hpgauge_4; }
	inline GameObject_t3674682005 ** get_address_of__hpgauge_4() { return &____hpgauge_4; }
	inline void set__hpgauge_4(GameObject_t3674682005 * value)
	{
		____hpgauge_4 = value;
		Il2CppCodeGenWriteBarrier(&____hpgauge_4, value);
	}

	inline static int32_t get_offset_of__redhpgauge_5() { return static_cast<int32_t>(offsetof(HPManager_t3198084485, ____redhpgauge_5)); }
	inline GameObject_t3674682005 * get__redhpgauge_5() const { return ____redhpgauge_5; }
	inline GameObject_t3674682005 ** get_address_of__redhpgauge_5() { return &____redhpgauge_5; }
	inline void set__redhpgauge_5(GameObject_t3674682005 * value)
	{
		____redhpgauge_5 = value;
		Il2CppCodeGenWriteBarrier(&____redhpgauge_5, value);
	}

	inline static int32_t get_offset_of__hphert_6() { return static_cast<int32_t>(offsetof(HPManager_t3198084485, ____hphert_6)); }
	inline GameObject_t3674682005 * get__hphert_6() const { return ____hphert_6; }
	inline GameObject_t3674682005 ** get_address_of__hphert_6() { return &____hphert_6; }
	inline void set__hphert_6(GameObject_t3674682005 * value)
	{
		____hphert_6 = value;
		Il2CppCodeGenWriteBarrier(&____hphert_6, value);
	}

	inline static int32_t get_offset_of__redhphert_7() { return static_cast<int32_t>(offsetof(HPManager_t3198084485, ____redhphert_7)); }
	inline GameObject_t3674682005 * get__redhphert_7() const { return ____redhphert_7; }
	inline GameObject_t3674682005 ** get_address_of__redhphert_7() { return &____redhphert_7; }
	inline void set__redhphert_7(GameObject_t3674682005 * value)
	{
		____redhphert_7 = value;
		Il2CppCodeGenWriteBarrier(&____redhphert_7, value);
	}

	inline static int32_t get_offset_of__hptext_8() { return static_cast<int32_t>(offsetof(HPManager_t3198084485, ____hptext_8)); }
	inline GameObject_t3674682005 * get__hptext_8() const { return ____hptext_8; }
	inline GameObject_t3674682005 ** get_address_of__hptext_8() { return &____hptext_8; }
	inline void set__hptext_8(GameObject_t3674682005 * value)
	{
		____hptext_8 = value;
		Il2CppCodeGenWriteBarrier(&____hptext_8, value);
	}

	inline static int32_t get_offset_of__ie_9() { return static_cast<int32_t>(offsetof(HPManager_t3198084485, ____ie_9)); }
	inline Il2CppObject * get__ie_9() const { return ____ie_9; }
	inline Il2CppObject ** get_address_of__ie_9() { return &____ie_9; }
	inline void set__ie_9(Il2CppObject * value)
	{
		____ie_9 = value;
		Il2CppCodeGenWriteBarrier(&____ie_9, value);
	}

	inline static int32_t get_offset_of__ie2_10() { return static_cast<int32_t>(offsetof(HPManager_t3198084485, ____ie2_10)); }
	inline Il2CppObject * get__ie2_10() const { return ____ie2_10; }
	inline Il2CppObject ** get_address_of__ie2_10() { return &____ie2_10; }
	inline void set__ie2_10(Il2CppObject * value)
	{
		____ie2_10 = value;
		Il2CppCodeGenWriteBarrier(&____ie2_10, value);
	}

	inline static int32_t get_offset_of__bomPar_11() { return static_cast<int32_t>(offsetof(HPManager_t3198084485, ____bomPar_11)); }
	inline GameObjectU5BU5D_t2662109048* get__bomPar_11() const { return ____bomPar_11; }
	inline GameObjectU5BU5D_t2662109048** get_address_of__bomPar_11() { return &____bomPar_11; }
	inline void set__bomPar_11(GameObjectU5BU5D_t2662109048* value)
	{
		____bomPar_11 = value;
		Il2CppCodeGenWriteBarrier(&____bomPar_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
