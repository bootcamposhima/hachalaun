﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BlackBoard
struct BlackBoard_t328561223;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void BlackBoard::.ctor()
extern "C"  void BlackBoard__ctor_m882165300 (BlackBoard_t328561223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlackBoard::Start()
extern "C"  void BlackBoard_Start_m4124270388 (BlackBoard_t328561223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlackBoard::Update()
extern "C"  void BlackBoard_Update_m3304182617 (BlackBoard_t328561223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlackBoard::Awake()
extern "C"  void BlackBoard_Awake_m1119770519 (BlackBoard_t328561223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject BlackBoard::getParent()
extern "C"  GameObject_t3674682005 * BlackBoard_getParent_m1406525501 (BlackBoard_t328561223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlackBoard::setParent(UnityEngine.GameObject)
extern "C"  void BlackBoard_setParent_m137813142 (BlackBoard_t328561223 * __this, GameObject_t3674682005 * ___gameobject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject BlackBoard::getGameObject(System.String)
extern "C"  GameObject_t3674682005 * BlackBoard_getGameObject_m1374591710 (BlackBoard_t328561223 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlackBoard::setGameObject(System.String,UnityEngine.GameObject)
extern "C"  void BlackBoard_setGameObject_m1891710273 (BlackBoard_t328561223 * __this, String_t* ___str0, GameObject_t3674682005 * ___gameobject1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlackBoard::AddGameObject(System.String,UnityEngine.GameObject)
extern "C"  void BlackBoard_AddGameObject_m1431445344 (BlackBoard_t328561223 * __this, String_t* ___str0, GameObject_t3674682005 * ___gameobject1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BlackBoard::getInteger(System.String)
extern "C"  int32_t BlackBoard_getInteger_m2773874812 (BlackBoard_t328561223 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlackBoard::setInteger(System.String,System.Int32)
extern "C"  void BlackBoard_setInteger_m1236984513 (BlackBoard_t328561223 * __this, String_t* ___str0, int32_t ___num1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlackBoard::AddInteger(System.String,System.Int32)
extern "C"  void BlackBoard_AddInteger_m4026331714 (BlackBoard_t328561223 * __this, String_t* ___str0, int32_t ___num1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 BlackBoard::getVector3(System.String)
extern "C"  Vector3_t4282066566  BlackBoard_getVector3_m2155162686 (BlackBoard_t328561223 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlackBoard::setVector3(System.String,UnityEngine.Vector3)
extern "C"  void BlackBoard_setVector3_m68187415 (BlackBoard_t328561223 * __this, String_t* ___str0, Vector3_t4282066566  ___num1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlackBoard::AddVector3(System.String,UnityEngine.Vector3)
extern "C"  void BlackBoard_AddVector3_m3114738294 (BlackBoard_t328561223 * __this, String_t* ___str0, Vector3_t4282066566  ___num1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BlackBoard::getFraction(System.String)
extern "C"  float BlackBoard_getFraction_m2652510948 (BlackBoard_t328561223 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlackBoard::setFraction(System.String,System.Single)
extern "C"  void BlackBoard_setFraction_m606765681 (BlackBoard_t328561223 * __this, String_t* ___str0, float ___num1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlackBoard::AddFraction(System.String,System.Single)
extern "C"  void BlackBoard_AddFraction_m1109833138 (BlackBoard_t328561223 * __this, String_t* ___str0, float ___num1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BlackBoard::getJude(System.String)
extern "C"  bool BlackBoard_getJude_m3665975042 (BlackBoard_t328561223 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlackBoard::setJude(System.String,System.Boolean)
extern "C"  void BlackBoard_setJude_m2310939259 (BlackBoard_t328561223 * __this, String_t* ___str0, bool ___flag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BlackBoard::AddJude(System.String,System.Boolean)
extern "C"  void BlackBoard_AddJude_m3509296858 (BlackBoard_t328561223 * __this, String_t* ___str0, bool ___flag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
