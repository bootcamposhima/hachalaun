﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlaySelect
struct PlaySelect_t3562688880;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AIDatas4008896193.h"

// System.Void PlaySelect::.ctor()
extern "C"  void PlaySelect__ctor_m698213675 (PlaySelect_t3562688880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlaySelect::Start()
extern "C"  void PlaySelect_Start_m3940318763 (PlaySelect_t3562688880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlaySelect::Update()
extern "C"  void PlaySelect_Update_m1896649538 (PlaySelect_t3562688880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlaySelect::set_Attack(System.Boolean)
extern "C"  void PlaySelect_set_Attack_m1771572533 (PlaySelect_t3562688880 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlaySelect::get_CharaNum()
extern "C"  int32_t PlaySelect_get_CharaNum_m2536607947 (PlaySelect_t3562688880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlaySelect::Show(System.Boolean)
extern "C"  void PlaySelect_Show_m2615096685 (PlaySelect_t3562688880 * __this, bool ___isAttack0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlaySelect::ShowAIDatas()
extern "C"  void PlaySelect_ShowAIDatas_m840081517 (PlaySelect_t3562688880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlaySelect::SelectAI(System.Int32)
extern "C"  void PlaySelect_SelectAI_m870064558 (PlaySelect_t3562688880 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlaySelect::CreateEnemy(AIDatas)
extern "C"  void PlaySelect_CreateEnemy_m4006479612 (PlaySelect_t3562688880 * __this, AIDatas_t4008896193  ___ai0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlaySelect::SelectCharactor(System.Int32)
extern "C"  void PlaySelect_SelectCharactor_m1234096541 (PlaySelect_t3562688880 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject PlaySelect::CreateCharactor(System.Int32)
extern "C"  GameObject_t3674682005 * PlaySelect_CreateCharactor_m686103176 (PlaySelect_t3562688880 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlaySelect::ChangeGame()
extern "C"  void PlaySelect_ChangeGame_m2431016955 (PlaySelect_t3562688880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlaySelect::ChangeGameStart()
extern "C"  void PlaySelect_ChangeGameStart_m1069629961 (PlaySelect_t3562688880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlaySelect::GameStart()
extern "C"  void PlaySelect_GameStart_m1387459033 (PlaySelect_t3562688880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlaySelect::ShowStart()
extern "C"  void PlaySelect_ShowStart_m1909157998 (PlaySelect_t3562688880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PlaySelect::ShowStartCol()
extern "C"  Il2CppObject * PlaySelect_ShowStartCol_m119942172 (PlaySelect_t3562688880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlaySelect::StopCol()
extern "C"  void PlaySelect_StopCol_m2219963111 (PlaySelect_t3562688880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
