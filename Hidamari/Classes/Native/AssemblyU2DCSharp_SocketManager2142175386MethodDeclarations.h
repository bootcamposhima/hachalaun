﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SocketManager
struct SocketManager_t2142175386;

#include "codegen/il2cpp-codegen.h"

// System.Void SocketManager::.ctor()
extern "C"  void SocketManager__ctor_m2417585681 (SocketManager_t2142175386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketManager::Start()
extern "C"  void SocketManager_Start_m1364723473 (SocketManager_t2142175386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketManager::Update()
extern "C"  void SocketManager_Update_m3657574172 (SocketManager_t2142175386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
