﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Player2393081601.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DemoPlay
struct  DemoPlay_t921155543  : public Player_t2393081601
{
public:
	// System.Boolean DemoPlay::isdemoplay
	bool ___isdemoplay_23;

public:
	inline static int32_t get_offset_of_isdemoplay_23() { return static_cast<int32_t>(offsetof(DemoPlay_t921155543, ___isdemoplay_23)); }
	inline bool get_isdemoplay_23() const { return ___isdemoplay_23; }
	inline bool* get_address_of_isdemoplay_23() { return &___isdemoplay_23; }
	inline void set_isdemoplay_23(bool value)
	{
		___isdemoplay_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
