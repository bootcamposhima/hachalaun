﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GodTouches_GodPhase1840992135.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void GodTouches.GodTouch::.cctor()
extern "C"  void GodTouch__cctor_m2990440194 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GodTouches.GodPhase GodTouches.GodTouch::GetPhase()
extern "C"  int32_t GodTouch_GetPhase_m713777415 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 GodTouches.GodTouch::GetPosition()
extern "C"  Vector3_t4282066566  GodTouch_GetPosition_m3176068866 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 GodTouches.GodTouch::GetDeltaPosition()
extern "C"  Vector3_t4282066566  GodTouch_GetDeltaPosition_m3181640106 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
