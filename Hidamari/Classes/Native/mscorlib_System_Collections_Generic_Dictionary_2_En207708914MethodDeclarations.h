﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1190435706MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,WebSocketSharp.Net.EndPointListener>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1751537815(__this, ___dictionary0, method) ((  void (*) (Enumerator_t207708914 *, Dictionary_2_t3185352818 *, const MethodInfo*))Enumerator__ctor_m2377115088_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,WebSocketSharp.Net.EndPointListener>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2976255092(__this, method) ((  Il2CppObject * (*) (Enumerator_t207708914 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1037642267_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,WebSocketSharp.Net.EndPointListener>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2538809278(__this, method) ((  void (*) (Enumerator_t207708914 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2809374949_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,WebSocketSharp.Net.EndPointListener>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1797327541(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t207708914 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2434214620_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,WebSocketSharp.Net.EndPointListener>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m495919696(__this, method) ((  Il2CppObject * (*) (Enumerator_t207708914 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3735627447_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,WebSocketSharp.Net.EndPointListener>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m885894370(__this, method) ((  Il2CppObject * (*) (Enumerator_t207708914 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m393753481_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,WebSocketSharp.Net.EndPointListener>::MoveNext()
#define Enumerator_MoveNext_m1417804014(__this, method) ((  bool (*) (Enumerator_t207708914 *, const MethodInfo*))Enumerator_MoveNext_m1213995029_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,WebSocketSharp.Net.EndPointListener>::get_Current()
#define Enumerator_get_Current_m1454018702(__this, method) ((  KeyValuePair_2_t3084133524  (*) (Enumerator_t207708914 *, const MethodInfo*))Enumerator_get_Current_m1399860359_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,WebSocketSharp.Net.EndPointListener>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3532968567(__this, method) ((  int32_t (*) (Enumerator_t207708914 *, const MethodInfo*))Enumerator_get_CurrentKey_m1767398110_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,WebSocketSharp.Net.EndPointListener>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3715532919(__this, method) ((  EndPointListener_t3188089579 * (*) (Enumerator_t207708914 *, const MethodInfo*))Enumerator_get_CurrentValue_m3384846750_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,WebSocketSharp.Net.EndPointListener>::Reset()
#define Enumerator_Reset_m1618739305(__this, method) ((  void (*) (Enumerator_t207708914 *, const MethodInfo*))Enumerator_Reset_m1080084514_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,WebSocketSharp.Net.EndPointListener>::VerifyState()
#define Enumerator_VerifyState_m4160593330(__this, method) ((  void (*) (Enumerator_t207708914 *, const MethodInfo*))Enumerator_VerifyState_m2404513451_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,WebSocketSharp.Net.EndPointListener>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m2460509338(__this, method) ((  void (*) (Enumerator_t207708914 *, const MethodInfo*))Enumerator_VerifyCurrent_m2789892947_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,WebSocketSharp.Net.EndPointListener>::Dispose()
#define Enumerator_Dispose_m3353740025(__this, method) ((  void (*) (Enumerator_t207708914 *, const MethodInfo*))Enumerator_Dispose_m1102561394_gshared)(__this, method)
