﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// WebSocketSharp.Net.WebHeaderCollection
struct WebHeaderCollection_t288332393;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.WebHeaderCollection/<GetObjectData>c__AnonStorey26
struct  U3CGetObjectDataU3Ec__AnonStorey26_t1884298844  : public Il2CppObject
{
public:
	// System.Runtime.Serialization.SerializationInfo WebSocketSharp.Net.WebHeaderCollection/<GetObjectData>c__AnonStorey26::serializationInfo
	SerializationInfo_t2185721892 * ___serializationInfo_0;
	// System.Int32 WebSocketSharp.Net.WebHeaderCollection/<GetObjectData>c__AnonStorey26::count
	int32_t ___count_1;
	// WebSocketSharp.Net.WebHeaderCollection WebSocketSharp.Net.WebHeaderCollection/<GetObjectData>c__AnonStorey26::<>f__this
	WebHeaderCollection_t288332393 * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_serializationInfo_0() { return static_cast<int32_t>(offsetof(U3CGetObjectDataU3Ec__AnonStorey26_t1884298844, ___serializationInfo_0)); }
	inline SerializationInfo_t2185721892 * get_serializationInfo_0() const { return ___serializationInfo_0; }
	inline SerializationInfo_t2185721892 ** get_address_of_serializationInfo_0() { return &___serializationInfo_0; }
	inline void set_serializationInfo_0(SerializationInfo_t2185721892 * value)
	{
		___serializationInfo_0 = value;
		Il2CppCodeGenWriteBarrier(&___serializationInfo_0, value);
	}

	inline static int32_t get_offset_of_count_1() { return static_cast<int32_t>(offsetof(U3CGetObjectDataU3Ec__AnonStorey26_t1884298844, ___count_1)); }
	inline int32_t get_count_1() const { return ___count_1; }
	inline int32_t* get_address_of_count_1() { return &___count_1; }
	inline void set_count_1(int32_t value)
	{
		___count_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3CGetObjectDataU3Ec__AnonStorey26_t1884298844, ___U3CU3Ef__this_2)); }
	inline WebHeaderCollection_t288332393 * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline WebHeaderCollection_t288332393 ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(WebHeaderCollection_t288332393 * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
