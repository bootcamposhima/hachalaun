﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HitJson
struct HitJson_t2591263483;
struct HitJson_t2591263483_marshaled_pinvoke;
struct HitJson_t2591263483_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_HitJson2591263483.h"
#include "AssemblyU2DCSharp_Direction1041377119.h"

// System.Void HitJson::.ctor(System.Int32,Direction)
extern "C"  void HitJson__ctor_m1478341582 (HitJson_t2591263483 * __this, int32_t ___d0, int32_t ___di1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HitJson::.ctor(System.Int32)
extern "C"  void HitJson__ctor_m156290081 (HitJson_t2591263483 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct HitJson_t2591263483;
struct HitJson_t2591263483_marshaled_pinvoke;

extern "C" void HitJson_t2591263483_marshal_pinvoke(const HitJson_t2591263483& unmarshaled, HitJson_t2591263483_marshaled_pinvoke& marshaled);
extern "C" void HitJson_t2591263483_marshal_pinvoke_back(const HitJson_t2591263483_marshaled_pinvoke& marshaled, HitJson_t2591263483& unmarshaled);
extern "C" void HitJson_t2591263483_marshal_pinvoke_cleanup(HitJson_t2591263483_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct HitJson_t2591263483;
struct HitJson_t2591263483_marshaled_com;

extern "C" void HitJson_t2591263483_marshal_com(const HitJson_t2591263483& unmarshaled, HitJson_t2591263483_marshaled_com& marshaled);
extern "C" void HitJson_t2591263483_marshal_com_back(const HitJson_t2591263483_marshaled_com& marshaled, HitJson_t2591263483& unmarshaled);
extern "C" void HitJson_t2591263483_marshal_com_cleanup(HitJson_t2591263483_marshaled_com& marshaled);
