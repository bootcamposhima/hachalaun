﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.Net.ChunkStream
struct ChunkStream_t4213968439;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// WebSocketSharp.Net.WebHeaderCollection
struct WebHeaderCollection_t288332393;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_WebHeaderColle288332393.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_InputChunkSta4273164664.h"
#include "mscorlib_System_String7231557.h"

// System.Void WebSocketSharp.Net.ChunkStream::.ctor(System.Byte[],System.Int32,System.Int32,WebSocketSharp.Net.WebHeaderCollection)
extern "C"  void ChunkStream__ctor_m2255280580 (ChunkStream_t4213968439 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___size2, WebHeaderCollection_t288332393 * ___headers3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.ChunkStream::.ctor(WebSocketSharp.Net.WebHeaderCollection)
extern "C"  void ChunkStream__ctor_m2968231879 (ChunkStream_t4213968439 * __this, WebHeaderCollection_t288332393 * ___headers0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.Net.WebHeaderCollection WebSocketSharp.Net.ChunkStream::get_Headers()
extern "C"  WebHeaderCollection_t288332393 * ChunkStream_get_Headers_m2902828043 (ChunkStream_t4213968439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 WebSocketSharp.Net.ChunkStream::get_ChunkLeft()
extern "C"  int32_t ChunkStream_get_ChunkLeft_m1129758725 (ChunkStream_t4213968439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.Net.ChunkStream::get_WantMore()
extern "C"  bool ChunkStream_get_WantMore_m2363430268 (ChunkStream_t4213968439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.Net.InputChunkState WebSocketSharp.Net.ChunkStream::readCRLF(System.Byte[],System.Int32&,System.Int32)
extern "C"  int32_t ChunkStream_readCRLF_m1068544541 (ChunkStream_t4213968439 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t* ___offset1, int32_t ___size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 WebSocketSharp.Net.ChunkStream::readFromChunks(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t ChunkStream_readFromChunks_m1449653979 (ChunkStream_t4213968439 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.Net.InputChunkState WebSocketSharp.Net.ChunkStream::readTrailer(System.Byte[],System.Int32&,System.Int32)
extern "C"  int32_t ChunkStream_readTrailer_m1082306387 (ChunkStream_t4213968439 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t* ___offset1, int32_t ___size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.Net.ChunkStream::removeChunkExtension(System.String)
extern "C"  String_t* ChunkStream_removeChunkExtension_m2118000559 (Il2CppObject * __this /* static, unused */, String_t* ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.Net.InputChunkState WebSocketSharp.Net.ChunkStream::setChunkSize(System.Byte[],System.Int32&,System.Int32)
extern "C"  int32_t ChunkStream_setChunkSize_m1455200490 (ChunkStream_t4213968439 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t* ___offset1, int32_t ___size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.ChunkStream::throwProtocolViolation(System.String)
extern "C"  void ChunkStream_throwProtocolViolation_m1183337889 (Il2CppObject * __this /* static, unused */, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.ChunkStream::write(System.Byte[],System.Int32&,System.Int32)
extern "C"  void ChunkStream_write_m3224357920 (ChunkStream_t4213968439 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t* ___offset1, int32_t ___size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.Net.InputChunkState WebSocketSharp.Net.ChunkStream::writeBody(System.Byte[],System.Int32&,System.Int32)
extern "C"  int32_t ChunkStream_writeBody_m1771096247 (ChunkStream_t4213968439 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t* ___offset1, int32_t ___size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 WebSocketSharp.Net.ChunkStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t ChunkStream_Read_m3517347979 (ChunkStream_t4213968439 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.ChunkStream::ResetBuffer()
extern "C"  void ChunkStream_ResetBuffer_m1857756311 (ChunkStream_t4213968439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.ChunkStream::Write(System.Byte[],System.Int32,System.Int32)
extern "C"  void ChunkStream_Write_m4013588162 (ChunkStream_t4213968439 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.ChunkStream::WriteAndReadBack(System.Byte[],System.Int32,System.Int32,System.Int32&)
extern "C"  void ChunkStream_WriteAndReadBack_m1867035887 (ChunkStream_t4213968439 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___size2, int32_t* ___read3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
