﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Round/<RoundTextAnimation>c__IteratorE
struct U3CRoundTextAnimationU3Ec__IteratorE_t2209827310;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Round/<RoundTextAnimation>c__IteratorE::.ctor()
extern "C"  void U3CRoundTextAnimationU3Ec__IteratorE__ctor_m3586328173 (U3CRoundTextAnimationU3Ec__IteratorE_t2209827310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Round/<RoundTextAnimation>c__IteratorE::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CRoundTextAnimationU3Ec__IteratorE_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1193259471 (U3CRoundTextAnimationU3Ec__IteratorE_t2209827310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Round/<RoundTextAnimation>c__IteratorE::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CRoundTextAnimationU3Ec__IteratorE_System_Collections_IEnumerator_get_Current_m2704618339 (U3CRoundTextAnimationU3Ec__IteratorE_t2209827310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Round/<RoundTextAnimation>c__IteratorE::MoveNext()
extern "C"  bool U3CRoundTextAnimationU3Ec__IteratorE_MoveNext_m4028945743 (U3CRoundTextAnimationU3Ec__IteratorE_t2209827310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Round/<RoundTextAnimation>c__IteratorE::Dispose()
extern "C"  void U3CRoundTextAnimationU3Ec__IteratorE_Dispose_m1795885930 (U3CRoundTextAnimationU3Ec__IteratorE_t2209827310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Round/<RoundTextAnimation>c__IteratorE::Reset()
extern "C"  void U3CRoundTextAnimationU3Ec__IteratorE_Reset_m1232761114 (U3CRoundTextAnimationU3Ec__IteratorE_t2209827310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
