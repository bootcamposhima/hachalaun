﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IDManager
struct IDManager_t1951742482;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FixedID820738927.h"

// System.Void IDManager::.ctor()
extern "C"  void IDManager__ctor_m3912306585 (IDManager_t1951742482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IDManager::Start()
extern "C"  void IDManager_Start_m2859444377 (IDManager_t1951742482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IDManager::Awake()
extern "C"  void IDManager_Awake_m4149911804 (IDManager_t1951742482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IDManager::Update()
extern "C"  void IDManager_Update_m2749281940 (IDManager_t1951742482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FixedID IDManager::get_FID()
extern "C"  FixedID_t820738927  IDManager_get_FID_m3291668051 (IDManager_t1951742482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String IDManager::get_ID()
extern "C"  String_t* IDManager_get_ID_m2480470668 (IDManager_t1951742482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String IDManager::get_playername()
extern "C"  String_t* IDManager_get_playername_m2597375517 (IDManager_t1951742482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
