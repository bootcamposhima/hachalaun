﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AudioClip
struct AudioClip_t794140988;
// UnityEngine.AudioSource[]
struct AudioSourceU5BU5D_t44861630;

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen1774517059.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundManager
struct  SoundManager_t2444342206  : public SingletonMonoBehaviour_1_t1774517059
{
public:
	// UnityEngine.AudioClip SoundManager::PlaySound
	AudioClip_t794140988 * ___PlaySound_3;
	// UnityEngine.AudioClip SoundManager::GameSound
	AudioClip_t794140988 * ___GameSound_4;
	// UnityEngine.AudioClip SoundManager::WinSound
	AudioClip_t794140988 * ___WinSound_5;
	// UnityEngine.AudioClip SoundManager::LossSound
	AudioClip_t794140988 * ___LossSound_6;
	// UnityEngine.AudioClip SoundManager::SE1
	AudioClip_t794140988 * ___SE1_7;
	// UnityEngine.AudioClip SoundManager::SE2
	AudioClip_t794140988 * ___SE2_8;
	// UnityEngine.AudioClip SoundManager::LossSE
	AudioClip_t794140988 * ___LossSE_9;
	// UnityEngine.AudioClip SoundManager::WinSE
	AudioClip_t794140988 * ___WinSE_10;
	// UnityEngine.AudioClip SoundManager::BomSE
	AudioClip_t794140988 * ___BomSE_11;
	// UnityEngine.AudioClip SoundManager::FlySE
	AudioClip_t794140988 * ___FlySE_12;
	// UnityEngine.AudioSource[] SoundManager::_audios
	AudioSourceU5BU5D_t44861630* ____audios_13;

public:
	inline static int32_t get_offset_of_PlaySound_3() { return static_cast<int32_t>(offsetof(SoundManager_t2444342206, ___PlaySound_3)); }
	inline AudioClip_t794140988 * get_PlaySound_3() const { return ___PlaySound_3; }
	inline AudioClip_t794140988 ** get_address_of_PlaySound_3() { return &___PlaySound_3; }
	inline void set_PlaySound_3(AudioClip_t794140988 * value)
	{
		___PlaySound_3 = value;
		Il2CppCodeGenWriteBarrier(&___PlaySound_3, value);
	}

	inline static int32_t get_offset_of_GameSound_4() { return static_cast<int32_t>(offsetof(SoundManager_t2444342206, ___GameSound_4)); }
	inline AudioClip_t794140988 * get_GameSound_4() const { return ___GameSound_4; }
	inline AudioClip_t794140988 ** get_address_of_GameSound_4() { return &___GameSound_4; }
	inline void set_GameSound_4(AudioClip_t794140988 * value)
	{
		___GameSound_4 = value;
		Il2CppCodeGenWriteBarrier(&___GameSound_4, value);
	}

	inline static int32_t get_offset_of_WinSound_5() { return static_cast<int32_t>(offsetof(SoundManager_t2444342206, ___WinSound_5)); }
	inline AudioClip_t794140988 * get_WinSound_5() const { return ___WinSound_5; }
	inline AudioClip_t794140988 ** get_address_of_WinSound_5() { return &___WinSound_5; }
	inline void set_WinSound_5(AudioClip_t794140988 * value)
	{
		___WinSound_5 = value;
		Il2CppCodeGenWriteBarrier(&___WinSound_5, value);
	}

	inline static int32_t get_offset_of_LossSound_6() { return static_cast<int32_t>(offsetof(SoundManager_t2444342206, ___LossSound_6)); }
	inline AudioClip_t794140988 * get_LossSound_6() const { return ___LossSound_6; }
	inline AudioClip_t794140988 ** get_address_of_LossSound_6() { return &___LossSound_6; }
	inline void set_LossSound_6(AudioClip_t794140988 * value)
	{
		___LossSound_6 = value;
		Il2CppCodeGenWriteBarrier(&___LossSound_6, value);
	}

	inline static int32_t get_offset_of_SE1_7() { return static_cast<int32_t>(offsetof(SoundManager_t2444342206, ___SE1_7)); }
	inline AudioClip_t794140988 * get_SE1_7() const { return ___SE1_7; }
	inline AudioClip_t794140988 ** get_address_of_SE1_7() { return &___SE1_7; }
	inline void set_SE1_7(AudioClip_t794140988 * value)
	{
		___SE1_7 = value;
		Il2CppCodeGenWriteBarrier(&___SE1_7, value);
	}

	inline static int32_t get_offset_of_SE2_8() { return static_cast<int32_t>(offsetof(SoundManager_t2444342206, ___SE2_8)); }
	inline AudioClip_t794140988 * get_SE2_8() const { return ___SE2_8; }
	inline AudioClip_t794140988 ** get_address_of_SE2_8() { return &___SE2_8; }
	inline void set_SE2_8(AudioClip_t794140988 * value)
	{
		___SE2_8 = value;
		Il2CppCodeGenWriteBarrier(&___SE2_8, value);
	}

	inline static int32_t get_offset_of_LossSE_9() { return static_cast<int32_t>(offsetof(SoundManager_t2444342206, ___LossSE_9)); }
	inline AudioClip_t794140988 * get_LossSE_9() const { return ___LossSE_9; }
	inline AudioClip_t794140988 ** get_address_of_LossSE_9() { return &___LossSE_9; }
	inline void set_LossSE_9(AudioClip_t794140988 * value)
	{
		___LossSE_9 = value;
		Il2CppCodeGenWriteBarrier(&___LossSE_9, value);
	}

	inline static int32_t get_offset_of_WinSE_10() { return static_cast<int32_t>(offsetof(SoundManager_t2444342206, ___WinSE_10)); }
	inline AudioClip_t794140988 * get_WinSE_10() const { return ___WinSE_10; }
	inline AudioClip_t794140988 ** get_address_of_WinSE_10() { return &___WinSE_10; }
	inline void set_WinSE_10(AudioClip_t794140988 * value)
	{
		___WinSE_10 = value;
		Il2CppCodeGenWriteBarrier(&___WinSE_10, value);
	}

	inline static int32_t get_offset_of_BomSE_11() { return static_cast<int32_t>(offsetof(SoundManager_t2444342206, ___BomSE_11)); }
	inline AudioClip_t794140988 * get_BomSE_11() const { return ___BomSE_11; }
	inline AudioClip_t794140988 ** get_address_of_BomSE_11() { return &___BomSE_11; }
	inline void set_BomSE_11(AudioClip_t794140988 * value)
	{
		___BomSE_11 = value;
		Il2CppCodeGenWriteBarrier(&___BomSE_11, value);
	}

	inline static int32_t get_offset_of_FlySE_12() { return static_cast<int32_t>(offsetof(SoundManager_t2444342206, ___FlySE_12)); }
	inline AudioClip_t794140988 * get_FlySE_12() const { return ___FlySE_12; }
	inline AudioClip_t794140988 ** get_address_of_FlySE_12() { return &___FlySE_12; }
	inline void set_FlySE_12(AudioClip_t794140988 * value)
	{
		___FlySE_12 = value;
		Il2CppCodeGenWriteBarrier(&___FlySE_12, value);
	}

	inline static int32_t get_offset_of__audios_13() { return static_cast<int32_t>(offsetof(SoundManager_t2444342206, ____audios_13)); }
	inline AudioSourceU5BU5D_t44861630* get__audios_13() const { return ____audios_13; }
	inline AudioSourceU5BU5D_t44861630** get_address_of__audios_13() { return &____audios_13; }
	inline void set__audios_13(AudioSourceU5BU5D_t44861630* value)
	{
		____audios_13 = value;
		Il2CppCodeGenWriteBarrier(&____audios_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
