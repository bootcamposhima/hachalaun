﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Test
struct Test_t2603186;

#include "codegen/il2cpp-codegen.h"

// System.Void Test::.ctor()
extern "C"  void Test__ctor_m3717691945 (Test_t2603186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Test::Start()
extern "C"  void Test_Start_m2664829737 (Test_t2603186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Test::Update()
extern "C"  void Test_Update_m1011195396 (Test_t2603186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
