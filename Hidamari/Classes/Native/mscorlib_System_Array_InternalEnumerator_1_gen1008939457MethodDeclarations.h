﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1008939457.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_WebSocketSharp_CompressionMethod2226596781.h"

// System.Void System.Array/InternalEnumerator`1<WebSocketSharp.CompressionMethod>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3090750733_gshared (InternalEnumerator_1_t1008939457 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3090750733(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1008939457 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3090750733_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<WebSocketSharp.CompressionMethod>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1207294835_gshared (InternalEnumerator_1_t1008939457 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1207294835(__this, method) ((  void (*) (InternalEnumerator_1_t1008939457 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1207294835_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<WebSocketSharp.CompressionMethod>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2710118239_gshared (InternalEnumerator_1_t1008939457 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2710118239(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1008939457 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2710118239_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<WebSocketSharp.CompressionMethod>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3136824100_gshared (InternalEnumerator_1_t1008939457 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3136824100(__this, method) ((  void (*) (InternalEnumerator_1_t1008939457 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3136824100_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<WebSocketSharp.CompressionMethod>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m352315359_gshared (InternalEnumerator_1_t1008939457 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m352315359(__this, method) ((  bool (*) (InternalEnumerator_1_t1008939457 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m352315359_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<WebSocketSharp.CompressionMethod>::get_Current()
extern "C"  uint8_t InternalEnumerator_1_get_Current_m2623789396_gshared (InternalEnumerator_1_t1008939457 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2623789396(__this, method) ((  uint8_t (*) (InternalEnumerator_1_t1008939457 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2623789396_gshared)(__this, method)
