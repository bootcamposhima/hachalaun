﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SoundManager
struct SoundManager_t2444342206;

#include "codegen/il2cpp-codegen.h"

// System.Void SoundManager::.ctor()
extern "C"  void SoundManager__ctor_m4124152989 (SoundManager_t2444342206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::Awake()
extern "C"  void SoundManager_Awake_m66790912 (SoundManager_t2444342206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::Start()
extern "C"  void SoundManager_Start_m3071290781 (SoundManager_t2444342206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::StartGameSound()
extern "C"  void SoundManager_StartGameSound_m1951173826 (SoundManager_t2444342206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::StartPlaySound()
extern "C"  void SoundManager_StartPlaySound_m1507917632 (SoundManager_t2444342206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::StartWinSound()
extern "C"  void SoundManager_StartWinSound_m345467792 (SoundManager_t2444342206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::StartLossSound()
extern "C"  void SoundManager_StartLossSound_m3769845873 (SoundManager_t2444342206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::SEShotPlay()
extern "C"  void SoundManager_SEShotPlay_m2178939431 (SoundManager_t2444342206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::SEButtonPlay()
extern "C"  void SoundManager_SEButtonPlay_m1014810207 (SoundManager_t2444342206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::SELossPlay()
extern "C"  void SoundManager_SELossPlay_m1281057680 (SoundManager_t2444342206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::SEWinPlay()
extern "C"  void SoundManager_SEWinPlay_m4051560409 (SoundManager_t2444342206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::SEBomPlay()
extern "C"  void SoundManager_SEBomPlay_m37854909 (SoundManager_t2444342206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::SEFiringPlay()
extern "C"  void SoundManager_SEFiringPlay_m1054509024 (SoundManager_t2444342206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
