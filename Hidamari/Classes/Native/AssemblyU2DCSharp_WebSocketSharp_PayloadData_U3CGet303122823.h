﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t4260760469;
// WebSocketSharp.PayloadData
struct PayloadData_t39926750;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator1E
struct  U3CGetEnumeratorU3Ec__Iterator1E_t303122823  : public Il2CppObject
{
public:
	// System.Byte[] WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator1E::<$s_140>__0
	ByteU5BU5D_t4260760469* ___U3CU24s_140U3E__0_0;
	// System.Int32 WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator1E::<$s_141>__1
	int32_t ___U3CU24s_141U3E__1_1;
	// System.Byte WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator1E::<b>__2
	uint8_t ___U3CbU3E__2_2;
	// System.Byte[] WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator1E::<$s_142>__3
	ByteU5BU5D_t4260760469* ___U3CU24s_142U3E__3_3;
	// System.Int32 WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator1E::<$s_143>__4
	int32_t ___U3CU24s_143U3E__4_4;
	// System.Byte WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator1E::<b>__5
	uint8_t ___U3CbU3E__5_5;
	// System.Int32 WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator1E::$PC
	int32_t ___U24PC_6;
	// System.Byte WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator1E::$current
	uint8_t ___U24current_7;
	// WebSocketSharp.PayloadData WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator1E::<>f__this
	PayloadData_t39926750 * ___U3CU3Ef__this_8;

public:
	inline static int32_t get_offset_of_U3CU24s_140U3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1E_t303122823, ___U3CU24s_140U3E__0_0)); }
	inline ByteU5BU5D_t4260760469* get_U3CU24s_140U3E__0_0() const { return ___U3CU24s_140U3E__0_0; }
	inline ByteU5BU5D_t4260760469** get_address_of_U3CU24s_140U3E__0_0() { return &___U3CU24s_140U3E__0_0; }
	inline void set_U3CU24s_140U3E__0_0(ByteU5BU5D_t4260760469* value)
	{
		___U3CU24s_140U3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_140U3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CU24s_141U3E__1_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1E_t303122823, ___U3CU24s_141U3E__1_1)); }
	inline int32_t get_U3CU24s_141U3E__1_1() const { return ___U3CU24s_141U3E__1_1; }
	inline int32_t* get_address_of_U3CU24s_141U3E__1_1() { return &___U3CU24s_141U3E__1_1; }
	inline void set_U3CU24s_141U3E__1_1(int32_t value)
	{
		___U3CU24s_141U3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CbU3E__2_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1E_t303122823, ___U3CbU3E__2_2)); }
	inline uint8_t get_U3CbU3E__2_2() const { return ___U3CbU3E__2_2; }
	inline uint8_t* get_address_of_U3CbU3E__2_2() { return &___U3CbU3E__2_2; }
	inline void set_U3CbU3E__2_2(uint8_t value)
	{
		___U3CbU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U3CU24s_142U3E__3_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1E_t303122823, ___U3CU24s_142U3E__3_3)); }
	inline ByteU5BU5D_t4260760469* get_U3CU24s_142U3E__3_3() const { return ___U3CU24s_142U3E__3_3; }
	inline ByteU5BU5D_t4260760469** get_address_of_U3CU24s_142U3E__3_3() { return &___U3CU24s_142U3E__3_3; }
	inline void set_U3CU24s_142U3E__3_3(ByteU5BU5D_t4260760469* value)
	{
		___U3CU24s_142U3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_142U3E__3_3, value);
	}

	inline static int32_t get_offset_of_U3CU24s_143U3E__4_4() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1E_t303122823, ___U3CU24s_143U3E__4_4)); }
	inline int32_t get_U3CU24s_143U3E__4_4() const { return ___U3CU24s_143U3E__4_4; }
	inline int32_t* get_address_of_U3CU24s_143U3E__4_4() { return &___U3CU24s_143U3E__4_4; }
	inline void set_U3CU24s_143U3E__4_4(int32_t value)
	{
		___U3CU24s_143U3E__4_4 = value;
	}

	inline static int32_t get_offset_of_U3CbU3E__5_5() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1E_t303122823, ___U3CbU3E__5_5)); }
	inline uint8_t get_U3CbU3E__5_5() const { return ___U3CbU3E__5_5; }
	inline uint8_t* get_address_of_U3CbU3E__5_5() { return &___U3CbU3E__5_5; }
	inline void set_U3CbU3E__5_5(uint8_t value)
	{
		___U3CbU3E__5_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1E_t303122823, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1E_t303122823, ___U24current_7)); }
	inline uint8_t get_U24current_7() const { return ___U24current_7; }
	inline uint8_t* get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(uint8_t value)
	{
		___U24current_7 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_8() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1E_t303122823, ___U3CU3Ef__this_8)); }
	inline PayloadData_t39926750 * get_U3CU3Ef__this_8() const { return ___U3CU3Ef__this_8; }
	inline PayloadData_t39926750 ** get_address_of_U3CU3Ef__this_8() { return &___U3CU3Ef__this_8; }
	inline void set_U3CU3Ef__this_8(PayloadData_t39926750 * value)
	{
		___U3CU3Ef__this_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
