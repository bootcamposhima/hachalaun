﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StableAspect
struct StableAspect_t1871202195;

#include "codegen/il2cpp-codegen.h"

// System.Void StableAspect::.ctor()
extern "C"  void StableAspect__ctor_m3050012776 (StableAspect_t1871202195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StableAspect::Awake()
extern "C"  void StableAspect_Awake_m3287617995 (StableAspect_t1871202195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StableAspect::Start()
extern "C"  void StableAspect_Start_m1997150568 (StableAspect_t1871202195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StableAspect::Update()
extern "C"  void StableAspect_Update_m1787977637 (StableAspect_t1871202195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
