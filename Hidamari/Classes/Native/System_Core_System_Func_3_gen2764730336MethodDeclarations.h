﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_3_gen1235414321MethodDeclarations.h"

// System.Void System.Func`3<WebSocketSharp.Opcode,System.IO.Stream,System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Func_3__ctor_m979856946(__this, ___object0, ___method1, method) ((  void (*) (Func_3_t2764730336 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_3__ctor_m23952469_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`3<WebSocketSharp.Opcode,System.IO.Stream,System.Boolean>::Invoke(T1,T2)
#define Func_3_Invoke_m3358544583(__this, ___arg10, ___arg21, method) ((  bool (*) (Func_3_t2764730336 *, uint8_t, Stream_t1561764144 *, const MethodInfo*))Func_3_Invoke_m2075810392_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Func`3<WebSocketSharp.Opcode,System.IO.Stream,System.Boolean>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Func_3_BeginInvoke_m249269144(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Func_3_t2764730336 *, uint8_t, Stream_t1561764144 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Func_3_BeginInvoke_m3367373661_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// TResult System.Func`3<WebSocketSharp.Opcode,System.IO.Stream,System.Boolean>::EndInvoke(System.IAsyncResult)
#define Func_3_EndInvoke_m3777894830(__this, ___result0, method) ((  bool (*) (Func_3_t2764730336 *, Il2CppObject *, const MethodInfo*))Func_3_EndInvoke_m1137194883_gshared)(__this, ___result0, method)
