﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SocketIO.SocketIOComponent
struct SocketIOComponent_t3095476818;
// WebSocketSharp.WebSocket
struct WebSocket_t1342580397;
// System.String
struct String_t;
// System.Action`1<SocketIO.SocketIOEvent>
struct Action_1_t112702903;
// System.Action`1<JSONObject>
struct Action_1_t2148193039;
// JSONObject
struct JSONObject_t1752376903;
// System.Object
struct Il2CppObject;
// SocketIO.Packet
struct Packet_t1660110912;
// System.EventArgs
struct EventArgs_t2540831021;
// WebSocketSharp.MessageEventArgs
struct MessageEventArgs_t36945740;
// WebSocketSharp.ErrorEventArgs
struct ErrorEventArgs_t424195371;
// WebSocketSharp.CloseEventArgs
struct CloseEventArgs_t2470319931;
// SocketIO.SocketIOEvent
struct SocketIOEvent_t4011854063;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSONObject1752376903.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_SocketIO_Packet1660110912.h"
#include "mscorlib_System_EventArgs2540831021.h"
#include "AssemblyU2DCSharp_WebSocketSharp_MessageEventArgs36945740.h"
#include "AssemblyU2DCSharp_WebSocketSharp_ErrorEventArgs424195371.h"
#include "AssemblyU2DCSharp_WebSocketSharp_CloseEventArgs2470319931.h"
#include "AssemblyU2DCSharp_SocketIO_SocketIOEvent4011854063.h"

// System.Void SocketIO.SocketIOComponent::.ctor()
extern "C"  void SocketIOComponent__ctor_m3753768748 (SocketIOComponent_t3095476818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.WebSocket SocketIO.SocketIOComponent::get_socket()
extern "C"  WebSocket_t1342580397 * SocketIOComponent_get_socket_m3688457717 (SocketIOComponent_t3095476818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SocketIO.SocketIOComponent::get_sid()
extern "C"  String_t* SocketIOComponent_get_sid_m3009206540 (SocketIOComponent_t3095476818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIO.SocketIOComponent::set_sid(System.String)
extern "C"  void SocketIOComponent_set_sid_m1524583815 (SocketIOComponent_t3095476818 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SocketIO.SocketIOComponent::get_IsConnected()
extern "C"  bool SocketIOComponent_get_IsConnected_m3215437548 (SocketIOComponent_t3095476818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIO.SocketIOComponent::Awake()
extern "C"  void SocketIOComponent_Awake_m3991373967 (SocketIOComponent_t3095476818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIO.SocketIOComponent::Start()
extern "C"  void SocketIOComponent_Start_m2700906540 (SocketIOComponent_t3095476818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIO.SocketIOComponent::Update()
extern "C"  void SocketIOComponent_Update_m2129576289 (SocketIOComponent_t3095476818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIO.SocketIOComponent::OnDestroy()
extern "C"  void SocketIOComponent_OnDestroy_m3827848741 (SocketIOComponent_t3095476818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIO.SocketIOComponent::OnApplicationQuit()
extern "C"  void SocketIOComponent_OnApplicationQuit_m2198930090 (SocketIOComponent_t3095476818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIO.SocketIOComponent::SetHeader(System.String,System.String)
extern "C"  void SocketIOComponent_SetHeader_m1876198021 (SocketIOComponent_t3095476818 * __this, String_t* ___header0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIO.SocketIOComponent::Connect()
extern "C"  void SocketIOComponent_Connect_m2962396276 (SocketIOComponent_t3095476818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIO.SocketIOComponent::Close()
extern "C"  void SocketIOComponent_Close_m1169660994 (SocketIOComponent_t3095476818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIO.SocketIOComponent::On(System.String,System.Action`1<SocketIO.SocketIOEvent>)
extern "C"  void SocketIOComponent_On_m2458640667 (SocketIOComponent_t3095476818 * __this, String_t* ___ev0, Action_1_t112702903 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIO.SocketIOComponent::Off(System.String,System.Action`1<SocketIO.SocketIOEvent>)
extern "C"  void SocketIOComponent_Off_m3647093401 (SocketIOComponent_t3095476818 * __this, String_t* ___ev0, Action_1_t112702903 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIO.SocketIOComponent::Emit(System.String)
extern "C"  void SocketIOComponent_Emit_m980264247 (SocketIOComponent_t3095476818 * __this, String_t* ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIO.SocketIOComponent::Emit(System.String,System.Action`1<JSONObject>)
extern "C"  void SocketIOComponent_Emit_m3744016642 (SocketIOComponent_t3095476818 * __this, String_t* ___ev0, Action_1_t2148193039 * ___action1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIO.SocketIOComponent::Emit(System.String,System.String)
extern "C"  void SocketIOComponent_Emit_m3250970931 (SocketIOComponent_t3095476818 * __this, String_t* ___ev0, String_t* ___str1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIO.SocketIOComponent::Emit(System.String,JSONObject)
extern "C"  void SocketIOComponent_Emit_m3543815496 (SocketIOComponent_t3095476818 * __this, String_t* ___ev0, JSONObject_t1752376903 * ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIO.SocketIOComponent::Emit(System.String,JSONObject,System.Action`1<JSONObject>)
extern "C"  void SocketIOComponent_Emit_m962332051 (SocketIOComponent_t3095476818 * __this, String_t* ___ev0, JSONObject_t1752376903 * ___data1, Action_1_t2148193039 * ___action2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIO.SocketIOComponent::RunSocketThread(System.Object)
extern "C"  void SocketIOComponent_RunSocketThread_m1848335938 (SocketIOComponent_t3095476818 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIO.SocketIOComponent::RunPingThread(System.Object)
extern "C"  void SocketIOComponent_RunPingThread_m1872516259 (SocketIOComponent_t3095476818 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIO.SocketIOComponent::EmitMessage(System.Int32,System.String)
extern "C"  void SocketIOComponent_EmitMessage_m125653899 (SocketIOComponent_t3095476818 * __this, int32_t ___id0, String_t* ___raw1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIO.SocketIOComponent::EmitClose()
extern "C"  void SocketIOComponent_EmitClose_m2852844655 (SocketIOComponent_t3095476818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIO.SocketIOComponent::EmitPacket(SocketIO.Packet)
extern "C"  void SocketIOComponent_EmitPacket_m1422682018 (SocketIOComponent_t3095476818 * __this, Packet_t1660110912 * ___packet0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIO.SocketIOComponent::OnOpen(System.Object,System.EventArgs)
extern "C"  void SocketIOComponent_OnOpen_m3247990395 (SocketIOComponent_t3095476818 * __this, Il2CppObject * ___sender0, EventArgs_t2540831021 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIO.SocketIOComponent::OnMessage(System.Object,WebSocketSharp.MessageEventArgs)
extern "C"  void SocketIOComponent_OnMessage_m1700610771 (SocketIOComponent_t3095476818 * __this, Il2CppObject * ___sender0, MessageEventArgs_t36945740 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIO.SocketIOComponent::HandleOpen(SocketIO.Packet)
extern "C"  void SocketIOComponent_HandleOpen_m3853953579 (SocketIOComponent_t3095476818 * __this, Packet_t1660110912 * ___packet0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIO.SocketIOComponent::HandlePing()
extern "C"  void SocketIOComponent_HandlePing_m808377490 (SocketIOComponent_t3095476818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIO.SocketIOComponent::HandlePong()
extern "C"  void SocketIOComponent_HandlePong_m813918616 (SocketIOComponent_t3095476818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIO.SocketIOComponent::HandleMessage(SocketIO.Packet)
extern "C"  void SocketIOComponent_HandleMessage_m126836588 (SocketIOComponent_t3095476818 * __this, Packet_t1660110912 * ___packet0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIO.SocketIOComponent::OnError(System.Object,WebSocketSharp.ErrorEventArgs)
extern "C"  void SocketIOComponent_OnError_m953960243 (SocketIOComponent_t3095476818 * __this, Il2CppObject * ___sender0, ErrorEventArgs_t424195371 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIO.SocketIOComponent::OnClose(System.Object,WebSocketSharp.CloseEventArgs)
extern "C"  void SocketIOComponent_OnClose_m3665609011 (SocketIOComponent_t3095476818 * __this, Il2CppObject * ___sender0, CloseEventArgs_t2470319931 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIO.SocketIOComponent::EmitEvent(System.String)
extern "C"  void SocketIOComponent_EmitEvent_m1851845137 (SocketIOComponent_t3095476818 * __this, String_t* ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIO.SocketIOComponent::EmitEvent(SocketIO.SocketIOEvent)
extern "C"  void SocketIOComponent_EmitEvent_m3441286949 (SocketIOComponent_t3095476818 * __this, SocketIOEvent_t4011854063 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIO.SocketIOComponent::InvokeAck(SocketIO.Packet)
extern "C"  void SocketIOComponent_InvokeAck_m326865146 (SocketIOComponent_t3095476818 * __this, Packet_t1660110912 * ___packet0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
