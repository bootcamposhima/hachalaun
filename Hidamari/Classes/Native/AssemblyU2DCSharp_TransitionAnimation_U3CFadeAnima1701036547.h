﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object
struct Il2CppObject;
// TransitionAnimation
struct TransitionAnimation_t4286760335;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TransitionAnimation/<FadeAnimation>c__IteratorB
struct  U3CFadeAnimationU3Ec__IteratorB_t1701036547  : public Il2CppObject
{
public:
	// System.Single TransitionAnimation/<FadeAnimation>c__IteratorB::<time>__0
	float ___U3CtimeU3E__0_0;
	// System.Single TransitionAnimation/<FadeAnimation>c__IteratorB::interval
	float ___interval_1;
	// UnityEngine.GameObject TransitionAnimation/<FadeAnimation>c__IteratorB::gb
	GameObject_t3674682005 * ___gb_2;
	// System.Int32 TransitionAnimation/<FadeAnimation>c__IteratorB::$PC
	int32_t ___U24PC_3;
	// System.Object TransitionAnimation/<FadeAnimation>c__IteratorB::$current
	Il2CppObject * ___U24current_4;
	// System.Single TransitionAnimation/<FadeAnimation>c__IteratorB::<$>interval
	float ___U3CU24U3Einterval_5;
	// UnityEngine.GameObject TransitionAnimation/<FadeAnimation>c__IteratorB::<$>gb
	GameObject_t3674682005 * ___U3CU24U3Egb_6;
	// TransitionAnimation TransitionAnimation/<FadeAnimation>c__IteratorB::<>f__this
	TransitionAnimation_t4286760335 * ___U3CU3Ef__this_7;

public:
	inline static int32_t get_offset_of_U3CtimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CFadeAnimationU3Ec__IteratorB_t1701036547, ___U3CtimeU3E__0_0)); }
	inline float get_U3CtimeU3E__0_0() const { return ___U3CtimeU3E__0_0; }
	inline float* get_address_of_U3CtimeU3E__0_0() { return &___U3CtimeU3E__0_0; }
	inline void set_U3CtimeU3E__0_0(float value)
	{
		___U3CtimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_interval_1() { return static_cast<int32_t>(offsetof(U3CFadeAnimationU3Ec__IteratorB_t1701036547, ___interval_1)); }
	inline float get_interval_1() const { return ___interval_1; }
	inline float* get_address_of_interval_1() { return &___interval_1; }
	inline void set_interval_1(float value)
	{
		___interval_1 = value;
	}

	inline static int32_t get_offset_of_gb_2() { return static_cast<int32_t>(offsetof(U3CFadeAnimationU3Ec__IteratorB_t1701036547, ___gb_2)); }
	inline GameObject_t3674682005 * get_gb_2() const { return ___gb_2; }
	inline GameObject_t3674682005 ** get_address_of_gb_2() { return &___gb_2; }
	inline void set_gb_2(GameObject_t3674682005 * value)
	{
		___gb_2 = value;
		Il2CppCodeGenWriteBarrier(&___gb_2, value);
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CFadeAnimationU3Ec__IteratorB_t1701036547, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CFadeAnimationU3Ec__IteratorB_t1701036547, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Einterval_5() { return static_cast<int32_t>(offsetof(U3CFadeAnimationU3Ec__IteratorB_t1701036547, ___U3CU24U3Einterval_5)); }
	inline float get_U3CU24U3Einterval_5() const { return ___U3CU24U3Einterval_5; }
	inline float* get_address_of_U3CU24U3Einterval_5() { return &___U3CU24U3Einterval_5; }
	inline void set_U3CU24U3Einterval_5(float value)
	{
		___U3CU24U3Einterval_5 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Egb_6() { return static_cast<int32_t>(offsetof(U3CFadeAnimationU3Ec__IteratorB_t1701036547, ___U3CU24U3Egb_6)); }
	inline GameObject_t3674682005 * get_U3CU24U3Egb_6() const { return ___U3CU24U3Egb_6; }
	inline GameObject_t3674682005 ** get_address_of_U3CU24U3Egb_6() { return &___U3CU24U3Egb_6; }
	inline void set_U3CU24U3Egb_6(GameObject_t3674682005 * value)
	{
		___U3CU24U3Egb_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Egb_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_7() { return static_cast<int32_t>(offsetof(U3CFadeAnimationU3Ec__IteratorB_t1701036547, ___U3CU3Ef__this_7)); }
	inline TransitionAnimation_t4286760335 * get_U3CU3Ef__this_7() const { return ___U3CU3Ef__this_7; }
	inline TransitionAnimation_t4286760335 ** get_address_of_U3CU3Ef__this_7() { return &___U3CU3Ef__this_7; }
	inline void set_U3CU3Ef__this_7(TransitionAnimation_t4286760335 * value)
	{
		___U3CU3Ef__this_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
