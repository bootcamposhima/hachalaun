﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V4272688975MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,WebSocketSharp.Net.HttpHeaderInfo>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m651760948(__this, ___host0, method) ((  void (*) (Enumerator_t2107396007 *, Dictionary_2_t4175562599 *, const MethodInfo*))Enumerator__ctor_m76754913_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,WebSocketSharp.Net.HttpHeaderInfo>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2882200173(__this, method) ((  Il2CppObject * (*) (Enumerator_t2107396007 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3118196448_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,WebSocketSharp.Net.HttpHeaderInfo>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3527090945(__this, method) ((  void (*) (Enumerator_t2107396007 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3702199860_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,WebSocketSharp.Net.HttpHeaderInfo>::Dispose()
#define Enumerator_Dispose_m2534232278(__this, method) ((  void (*) (Enumerator_t2107396007 *, const MethodInfo*))Enumerator_Dispose_m1628348611_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,WebSocketSharp.Net.HttpHeaderInfo>::MoveNext()
#define Enumerator_MoveNext_m2797866788(__this, method) ((  bool (*) (Enumerator_t2107396007 *, const MethodInfo*))Enumerator_MoveNext_m3556422944_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,WebSocketSharp.Net.HttpHeaderInfo>::get_Current()
#define Enumerator_get_Current_m931475147(__this, method) ((  HttpHeaderInfo_t3355144229 * (*) (Enumerator_t2107396007 *, const MethodInfo*))Enumerator_get_Current_m841474402_gshared)(__this, method)
