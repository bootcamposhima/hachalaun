﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Char>
struct KeyCollection_t2364453889;
// System.Collections.Generic.Dictionary`2<System.Object,System.Char>
struct Dictionary_2_t737694438;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1352630492.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Char>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m1505796127_gshared (KeyCollection_t2364453889 * __this, Dictionary_2_t737694438 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m1505796127(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t2364453889 *, Dictionary_2_t737694438 *, const MethodInfo*))KeyCollection__ctor_m1505796127_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Char>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2876957143_gshared (KeyCollection_t2364453889 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2876957143(__this, ___item0, method) ((  void (*) (KeyCollection_t2364453889 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2876957143_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Char>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m4025323790_gshared (KeyCollection_t2364453889 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m4025323790(__this, method) ((  void (*) (KeyCollection_t2364453889 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m4025323790_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Char>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3078173459_gshared (KeyCollection_t2364453889 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3078173459(__this, ___item0, method) ((  bool (*) (KeyCollection_t2364453889 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3078173459_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Char>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1183665464_gshared (KeyCollection_t2364453889 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1183665464(__this, ___item0, method) ((  bool (*) (KeyCollection_t2364453889 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1183665464_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Char>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3189926410_gshared (KeyCollection_t2364453889 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3189926410(__this, method) ((  Il2CppObject* (*) (KeyCollection_t2364453889 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3189926410_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Char>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m1990742144_gshared (KeyCollection_t2364453889 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m1990742144(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2364453889 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1990742144_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Char>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m481029499_gshared (KeyCollection_t2364453889 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m481029499(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2364453889 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m481029499_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Char>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1773285812_gshared (KeyCollection_t2364453889 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1773285812(__this, method) ((  bool (*) (KeyCollection_t2364453889 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1773285812_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Char>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m25234278_gshared (KeyCollection_t2364453889 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m25234278(__this, method) ((  bool (*) (KeyCollection_t2364453889 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m25234278_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Char>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m1228509650_gshared (KeyCollection_t2364453889 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m1228509650(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2364453889 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1228509650_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Char>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m1560167124_gshared (KeyCollection_t2364453889 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m1560167124(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2364453889 *, ObjectU5BU5D_t1108656482*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m1560167124_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Char>::GetEnumerator()
extern "C"  Enumerator_t1352630492  KeyCollection_GetEnumerator_m3766311287_gshared (KeyCollection_t2364453889 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m3766311287(__this, method) ((  Enumerator_t1352630492  (*) (KeyCollection_t2364453889 *, const MethodInfo*))KeyCollection_GetEnumerator_m3766311287_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Char>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m962994348_gshared (KeyCollection_t2364453889 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m962994348(__this, method) ((  int32_t (*) (KeyCollection_t2364453889 *, const MethodInfo*))KeyCollection_get_Count_m962994348_gshared)(__this, method)
