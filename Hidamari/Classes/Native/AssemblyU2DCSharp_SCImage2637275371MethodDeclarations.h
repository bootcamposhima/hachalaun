﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SCImage
struct SCImage_t2637275371;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2761310900;

#include "codegen/il2cpp-codegen.h"

// System.Void SCImage::.ctor()
extern "C"  void SCImage__ctor_m2184603360 (SCImage_t2637275371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCImage::Start()
extern "C"  void SCImage_Start_m1131741152 (SCImage_t2637275371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCImage::Update()
extern "C"  void SCImage_Update_m730089517 (SCImage_t2637275371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCImage::ChangeSelectmode(System.Int32)
extern "C"  void SCImage_ChangeSelectmode_m2621875044 (SCImage_t2637275371 * __this, int32_t ___mode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SCImage::Imagecol(UnityEngine.Sprite[])
extern "C"  Il2CppObject * SCImage_Imagecol_m3091778371 (SCImage_t2637275371 * __this, SpriteU5BU5D_t2761310900* ___image0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
