﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.Net.EndPointManager
struct EndPointManager_t3612541026;
// System.String
struct String_t;
// WebSocketSharp.Net.HttpListener
struct HttpListener_t398944510;
// WebSocketSharp.Net.EndPointListener
struct EndPointListener_t3188089579;
// System.Net.IPAddress
struct IPAddress_t3525271463;
// System.Net.IPEndPoint
struct IPEndPoint_t2123960758;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_HttpListener398944510.h"
#include "System_System_Net_IPAddress3525271463.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_EndPointListe3188089579.h"
#include "System_System_Net_IPEndPoint2123960758.h"

// System.Void WebSocketSharp.Net.EndPointManager::.ctor()
extern "C"  void EndPointManager__ctor_m3262278719 (EndPointManager_t3612541026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.EndPointManager::.cctor()
extern "C"  void EndPointManager__cctor_m1864296270 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.EndPointManager::addPrefix(System.String,WebSocketSharp.Net.HttpListener)
extern "C"  void EndPointManager_addPrefix_m487406182 (Il2CppObject * __this /* static, unused */, String_t* ___uriPrefix0, HttpListener_t398944510 * ___httpListener1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.Net.EndPointListener WebSocketSharp.Net.EndPointManager::getEndPointListener(System.Net.IPAddress,System.Int32,WebSocketSharp.Net.HttpListener,System.Boolean)
extern "C"  EndPointListener_t3188089579 * EndPointManager_getEndPointListener_m2196734051 (Il2CppObject * __this /* static, unused */, IPAddress_t3525271463 * ___address0, int32_t ___port1, HttpListener_t398944510 * ___httpListener2, bool ___secure3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.EndPointManager::removePrefix(System.String,WebSocketSharp.Net.HttpListener)
extern "C"  void EndPointManager_removePrefix_m3525430747 (Il2CppObject * __this /* static, unused */, String_t* ___uriPrefix0, HttpListener_t398944510 * ___httpListener1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.EndPointManager::AddListener(WebSocketSharp.Net.HttpListener)
extern "C"  void EndPointManager_AddListener_m4252482088 (Il2CppObject * __this /* static, unused */, HttpListener_t398944510 * ___httpListener0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.EndPointManager::AddPrefix(System.String,WebSocketSharp.Net.HttpListener)
extern "C"  void EndPointManager_AddPrefix_m4131577478 (Il2CppObject * __this /* static, unused */, String_t* ___uriPrefix0, HttpListener_t398944510 * ___httpListener1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.EndPointManager::RemoveEndPoint(WebSocketSharp.Net.EndPointListener,System.Net.IPEndPoint)
extern "C"  void EndPointManager_RemoveEndPoint_m1474066511 (Il2CppObject * __this /* static, unused */, EndPointListener_t3188089579 * ___epListener0, IPEndPoint_t2123960758 * ___endpoint1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.EndPointManager::RemoveListener(WebSocketSharp.Net.HttpListener)
extern "C"  void EndPointManager_RemoveListener_m1982232797 (Il2CppObject * __this /* static, unused */, HttpListener_t398944510 * ___httpListener0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.EndPointManager::RemovePrefix(System.String,WebSocketSharp.Net.HttpListener)
extern "C"  void EndPointManager_RemovePrefix_m3144168891 (Il2CppObject * __this /* static, unused */, String_t* ___uriPrefix0, HttpListener_t398944510 * ___httpListener1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
