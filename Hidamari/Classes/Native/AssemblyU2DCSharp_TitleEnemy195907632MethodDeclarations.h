﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TitleEnemy
struct TitleEnemy_t195907632;

#include "codegen/il2cpp-codegen.h"

// System.Void TitleEnemy::.ctor()
extern "C"  void TitleEnemy__ctor_m601718379 (TitleEnemy_t195907632 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TitleEnemy::Start()
extern "C"  void TitleEnemy_Start_m3843823467 (TitleEnemy_t195907632 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TitleEnemy::Update()
extern "C"  void TitleEnemy_Update_m3200262658 (TitleEnemy_t195907632 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TitleEnemy::AIEndCount()
extern "C"  void TitleEnemy_AIEndCount_m3770430869 (TitleEnemy_t195907632 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
