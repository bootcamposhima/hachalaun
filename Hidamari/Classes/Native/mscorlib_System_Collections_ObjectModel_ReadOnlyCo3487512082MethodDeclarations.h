﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<AIData>
struct ReadOnlyCollection_1_t3487512082;
// System.Collections.Generic.IList`1<AIData>
struct IList_1_t330114453;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// AIData[]
struct AIDataU5BU5D_t2017435655;
// System.Collections.Generic.IEnumerator`1<AIData>
struct IEnumerator_1_t3842299595;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AIData1930434546.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AIData>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m391598526_gshared (ReadOnlyCollection_1_t3487512082 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m391598526(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t3487512082 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m391598526_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AIData>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m161510568_gshared (ReadOnlyCollection_1_t3487512082 * __this, AIData_t1930434546  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m161510568(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t3487512082 *, AIData_t1930434546 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m161510568_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AIData>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2718551330_gshared (ReadOnlyCollection_1_t3487512082 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2718551330(__this, method) ((  void (*) (ReadOnlyCollection_1_t3487512082 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2718551330_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AIData>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2050049423_gshared (ReadOnlyCollection_1_t3487512082 * __this, int32_t ___index0, AIData_t1930434546  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2050049423(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t3487512082 *, int32_t, AIData_t1930434546 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2050049423_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AIData>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m903063695_gshared (ReadOnlyCollection_1_t3487512082 * __this, AIData_t1930434546  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m903063695(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t3487512082 *, AIData_t1930434546 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m903063695_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AIData>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m4218869589_gshared (ReadOnlyCollection_1_t3487512082 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m4218869589(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3487512082 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m4218869589_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<AIData>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  AIData_t1930434546  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3842322299_gshared (ReadOnlyCollection_1_t3487512082 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3842322299(__this, ___index0, method) ((  AIData_t1930434546  (*) (ReadOnlyCollection_1_t3487512082 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3842322299_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AIData>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m517103270_gshared (ReadOnlyCollection_1_t3487512082 * __this, int32_t ___index0, AIData_t1930434546  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m517103270(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3487512082 *, int32_t, AIData_t1930434546 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m517103270_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AIData>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1610556960_gshared (ReadOnlyCollection_1_t3487512082 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1610556960(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3487512082 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1610556960_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AIData>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3779909997_gshared (ReadOnlyCollection_1_t3487512082 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3779909997(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3487512082 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3779909997_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<AIData>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1956762812_gshared (ReadOnlyCollection_1_t3487512082 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1956762812(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3487512082 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1956762812_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<AIData>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m2237731073_gshared (ReadOnlyCollection_1_t3487512082 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m2237731073(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3487512082 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m2237731073_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AIData>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m2538790011_gshared (ReadOnlyCollection_1_t3487512082 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m2538790011(__this, method) ((  void (*) (ReadOnlyCollection_1_t3487512082 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m2538790011_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AIData>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m293306591_gshared (ReadOnlyCollection_1_t3487512082 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m293306591(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3487512082 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m293306591_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<AIData>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2344541913_gshared (ReadOnlyCollection_1_t3487512082 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2344541913(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3487512082 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2344541913_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AIData>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m936983244_gshared (ReadOnlyCollection_1_t3487512082 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m936983244(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3487512082 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m936983244_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AIData>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m1763103452_gshared (ReadOnlyCollection_1_t3487512082 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1763103452(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t3487512082 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m1763103452_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AIData>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m406987868_gshared (ReadOnlyCollection_1_t3487512082 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m406987868(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3487512082 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m406987868_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AIData>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1928148509_gshared (ReadOnlyCollection_1_t3487512082 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1928148509(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3487512082 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1928148509_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<AIData>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3049734991_gshared (ReadOnlyCollection_1_t3487512082 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3049734991(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3487512082 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3049734991_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AIData>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2935030670_gshared (ReadOnlyCollection_1_t3487512082 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2935030670(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3487512082 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2935030670_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AIData>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1657065899_gshared (ReadOnlyCollection_1_t3487512082 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1657065899(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3487512082 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1657065899_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<AIData>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m2693242134_gshared (ReadOnlyCollection_1_t3487512082 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m2693242134(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3487512082 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m2693242134_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AIData>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m3406685091_gshared (ReadOnlyCollection_1_t3487512082 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m3406685091(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3487512082 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m3406685091_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AIData>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m1874473172_gshared (ReadOnlyCollection_1_t3487512082 * __this, AIData_t1930434546  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m1874473172(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3487512082 *, AIData_t1930434546 , const MethodInfo*))ReadOnlyCollection_1_Contains_m1874473172_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AIData>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m1726562264_gshared (ReadOnlyCollection_1_t3487512082 * __this, AIDataU5BU5D_t2017435655* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m1726562264(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3487512082 *, AIDataU5BU5D_t2017435655*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m1726562264_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<AIData>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m3700812587_gshared (ReadOnlyCollection_1_t3487512082 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m3700812587(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t3487512082 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m3700812587_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<AIData>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m2185652644_gshared (ReadOnlyCollection_1_t3487512082 * __this, AIData_t1930434546  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m2185652644(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3487512082 *, AIData_t1930434546 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m2185652644_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<AIData>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m3148030039_gshared (ReadOnlyCollection_1_t3487512082 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m3148030039(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t3487512082 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m3148030039_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<AIData>::get_Item(System.Int32)
extern "C"  AIData_t1930434546  ReadOnlyCollection_1_get_Item_m294351547_gshared (ReadOnlyCollection_1_t3487512082 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m294351547(__this, ___index0, method) ((  AIData_t1930434546  (*) (ReadOnlyCollection_1_t3487512082 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m294351547_gshared)(__this, ___index0, method)
