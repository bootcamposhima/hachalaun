﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2652585416MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.IO.Stream>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m2130483441(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t43533189 *, Dictionary_2_t2711741034 *, const MethodInfo*))KeyCollection__ctor_m293092086_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.IO.Stream>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1633103301(__this, ___item0, method) ((  void (*) (KeyCollection_t43533189 *, uint8_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1919261856_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.IO.Stream>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1789395068(__this, method) ((  void (*) (KeyCollection_t43533189 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1923770903_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.IO.Stream>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1368117733(__this, ___item0, method) ((  bool (*) (KeyCollection_t43533189 *, uint8_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2539460654_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.IO.Stream>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2080209034(__this, ___item0, method) ((  bool (*) (KeyCollection_t43533189 *, uint8_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3033382163_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.IO.Stream>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1666821240(__this, method) ((  Il2CppObject* (*) (KeyCollection_t43533189 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1243973865_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.IO.Stream>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m4135652078(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t43533189 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m847438857_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.IO.Stream>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m4250871145(__this, method) ((  Il2CppObject * (*) (KeyCollection_t43533189 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1790171608_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.IO.Stream>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3382207494(__this, method) ((  bool (*) (KeyCollection_t43533189 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3761323023_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.IO.Stream>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1811979320(__this, method) ((  bool (*) (KeyCollection_t43533189 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m89996161_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.IO.Stream>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m794702116(__this, method) ((  Il2CppObject * (*) (KeyCollection_t43533189 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1701827571_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.IO.Stream>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m1178731430(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t43533189 *, CompressionMethodU5BU5D_t88594176*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2229205419_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.IO.Stream>::GetEnumerator()
#define KeyCollection_GetEnumerator_m2749506633(__this, method) ((  Enumerator_t3326677088  (*) (KeyCollection_t43533189 *, const MethodInfo*))KeyCollection_GetEnumerator_m1444326392_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.IO.Stream>::get_Count()
#define KeyCollection_get_Count_m221043070(__this, method) ((  int32_t (*) (KeyCollection_t43533189 *, const MethodInfo*))KeyCollection_get_Count_m3976517947_gshared)(__this, method)
