﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SocketIO.SocketIOComponent
struct SocketIOComponent_t3095476818;

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen670979848.h"
#include "AssemblyU2DCSharp_FixedID820738927.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FightSocket
struct  FightSocket_t1340804995  : public SingletonMonoBehaviour_1_t670979848
{
public:
	// SocketIO.SocketIOComponent FightSocket::socket
	SocketIOComponent_t3095476818 * ___socket_3;
	// FixedID FightSocket::fid
	FixedID_t820738927  ___fid_4;

public:
	inline static int32_t get_offset_of_socket_3() { return static_cast<int32_t>(offsetof(FightSocket_t1340804995, ___socket_3)); }
	inline SocketIOComponent_t3095476818 * get_socket_3() const { return ___socket_3; }
	inline SocketIOComponent_t3095476818 ** get_address_of_socket_3() { return &___socket_3; }
	inline void set_socket_3(SocketIOComponent_t3095476818 * value)
	{
		___socket_3 = value;
		Il2CppCodeGenWriteBarrier(&___socket_3, value);
	}

	inline static int32_t get_offset_of_fid_4() { return static_cast<int32_t>(offsetof(FightSocket_t1340804995, ___fid_4)); }
	inline FixedID_t820738927  get_fid_4() const { return ___fid_4; }
	inline FixedID_t820738927 * get_address_of_fid_4() { return &___fid_4; }
	inline void set_fid_4(FixedID_t820738927  value)
	{
		___fid_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
