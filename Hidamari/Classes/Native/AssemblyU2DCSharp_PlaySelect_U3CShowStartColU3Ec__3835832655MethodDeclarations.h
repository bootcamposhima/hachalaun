﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlaySelect/<ShowStartCol>c__Iterator15
struct U3CShowStartColU3Ec__Iterator15_t3835832655;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PlaySelect/<ShowStartCol>c__Iterator15::.ctor()
extern "C"  void U3CShowStartColU3Ec__Iterator15__ctor_m1702290476 (U3CShowStartColU3Ec__Iterator15_t3835832655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlaySelect/<ShowStartCol>c__Iterator15::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CShowStartColU3Ec__Iterator15_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3625938288 (U3CShowStartColU3Ec__Iterator15_t3835832655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlaySelect/<ShowStartCol>c__Iterator15::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CShowStartColU3Ec__Iterator15_System_Collections_IEnumerator_get_Current_m3812249860 (U3CShowStartColU3Ec__Iterator15_t3835832655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlaySelect/<ShowStartCol>c__Iterator15::MoveNext()
extern "C"  bool U3CShowStartColU3Ec__Iterator15_MoveNext_m3294538544 (U3CShowStartColU3Ec__Iterator15_t3835832655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlaySelect/<ShowStartCol>c__Iterator15::Dispose()
extern "C"  void U3CShowStartColU3Ec__Iterator15_Dispose_m3711858025 (U3CShowStartColU3Ec__Iterator15_t3835832655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlaySelect/<ShowStartCol>c__Iterator15::Reset()
extern "C"  void U3CShowStartColU3Ec__Iterator15_Reset_m3643690713 (U3CShowStartColU3Ec__Iterator15_t3835832655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
