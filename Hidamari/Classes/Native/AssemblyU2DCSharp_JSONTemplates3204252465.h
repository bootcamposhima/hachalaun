﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t3325245147;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1974256870;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSONTemplates
struct  JSONTemplates_t3204252465  : public Il2CppObject
{
public:

public:
};

struct JSONTemplates_t3204252465_StaticFields
{
public:
	// System.Collections.Generic.HashSet`1<System.Object> JSONTemplates::touched
	HashSet_1_t3325245147 * ___touched_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> JSONTemplates::<>f__switch$map0
	Dictionary_2_t1974256870 * ___U3CU3Ef__switchU24map0_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> JSONTemplates::<>f__switch$map1
	Dictionary_2_t1974256870 * ___U3CU3Ef__switchU24map1_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> JSONTemplates::<>f__switch$map2
	Dictionary_2_t1974256870 * ___U3CU3Ef__switchU24map2_3;

public:
	inline static int32_t get_offset_of_touched_0() { return static_cast<int32_t>(offsetof(JSONTemplates_t3204252465_StaticFields, ___touched_0)); }
	inline HashSet_1_t3325245147 * get_touched_0() const { return ___touched_0; }
	inline HashSet_1_t3325245147 ** get_address_of_touched_0() { return &___touched_0; }
	inline void set_touched_0(HashSet_1_t3325245147 * value)
	{
		___touched_0 = value;
		Il2CppCodeGenWriteBarrier(&___touched_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_1() { return static_cast<int32_t>(offsetof(JSONTemplates_t3204252465_StaticFields, ___U3CU3Ef__switchU24map0_1)); }
	inline Dictionary_2_t1974256870 * get_U3CU3Ef__switchU24map0_1() const { return ___U3CU3Ef__switchU24map0_1; }
	inline Dictionary_2_t1974256870 ** get_address_of_U3CU3Ef__switchU24map0_1() { return &___U3CU3Ef__switchU24map0_1; }
	inline void set_U3CU3Ef__switchU24map0_1(Dictionary_2_t1974256870 * value)
	{
		___U3CU3Ef__switchU24map0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map0_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1_2() { return static_cast<int32_t>(offsetof(JSONTemplates_t3204252465_StaticFields, ___U3CU3Ef__switchU24map1_2)); }
	inline Dictionary_2_t1974256870 * get_U3CU3Ef__switchU24map1_2() const { return ___U3CU3Ef__switchU24map1_2; }
	inline Dictionary_2_t1974256870 ** get_address_of_U3CU3Ef__switchU24map1_2() { return &___U3CU3Ef__switchU24map1_2; }
	inline void set_U3CU3Ef__switchU24map1_2(Dictionary_2_t1974256870 * value)
	{
		___U3CU3Ef__switchU24map1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2_3() { return static_cast<int32_t>(offsetof(JSONTemplates_t3204252465_StaticFields, ___U3CU3Ef__switchU24map2_3)); }
	inline Dictionary_2_t1974256870 * get_U3CU3Ef__switchU24map2_3() const { return ___U3CU3Ef__switchU24map2_3; }
	inline Dictionary_2_t1974256870 ** get_address_of_U3CU3Ef__switchU24map2_3() { return &___U3CU3Ef__switchU24map2_3; }
	inline void set_U3CU3Ef__switchU24map2_3(Dictionary_2_t1974256870 * value)
	{
		___U3CU3Ef__switchU24map2_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map2_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
