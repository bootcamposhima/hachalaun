﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Char>
struct Dictionary_2_t737694438;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t666883479;
// System.Collections.Generic.IDictionary`2<System.Object,System.Char>
struct IDictionary_2_t315567783;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Char>[]
struct KeyValuePair_2U5BU5D_t403702745;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Char>>
struct IEnumerator_1_t2548340193;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Char>
struct KeyCollection_t2364453889;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Char>
struct ValueCollection_t3733267447;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_636475144.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2055017830.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Char>::.ctor()
extern "C"  void Dictionary_2__ctor_m313542742_gshared (Dictionary_2_t737694438 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m313542742(__this, method) ((  void (*) (Dictionary_2_t737694438 *, const MethodInfo*))Dictionary_2__ctor_m313542742_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Char>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3817275341_gshared (Dictionary_2_t737694438 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m3817275341(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t737694438 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3817275341_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Char>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m2387144450_gshared (Dictionary_2_t737694438 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m2387144450(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t737694438 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2387144450_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Char>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m2601921703_gshared (Dictionary_2_t737694438 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m2601921703(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t737694438 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m2601921703_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Char>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1880190523_gshared (Dictionary_2_t737694438 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m1880190523(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t737694438 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1880190523_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Char>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2856906166_gshared (Dictionary_2_t737694438 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m2856906166(__this, ___capacity0, ___comparer1, method) ((  void (*) (Dictionary_2_t737694438 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2856906166_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Char>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m3930922135_gshared (Dictionary_2_t737694438 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m3930922135(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t737694438 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m3930922135_gshared)(__this, ___info0, ___context1, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Char>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m2215609522_gshared (Dictionary_2_t737694438 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m2215609522(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t737694438 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m2215609522_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Char>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m2021527703_gshared (Dictionary_2_t737694438 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m2021527703(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t737694438 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m2021527703_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Char>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m1067969370_gshared (Dictionary_2_t737694438 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m1067969370(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t737694438 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m1067969370_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Char>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m150640853_gshared (Dictionary_2_t737694438 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m150640853(__this, ___key0, method) ((  void (*) (Dictionary_2_t737694438 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m150640853_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Char>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3645772792_gshared (Dictionary_2_t737694438 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3645772792(__this, method) ((  bool (*) (Dictionary_2_t737694438 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3645772792_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Char>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2228064932_gshared (Dictionary_2_t737694438 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2228064932(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t737694438 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2228064932_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Char>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1895999036_gshared (Dictionary_2_t737694438 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1895999036(__this, method) ((  bool (*) (Dictionary_2_t737694438 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1895999036_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Char>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3275757291_gshared (Dictionary_2_t737694438 * __this, KeyValuePair_2_t636475144  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3275757291(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t737694438 *, KeyValuePair_2_t636475144 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3275757291_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Char>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2260319927_gshared (Dictionary_2_t737694438 * __this, KeyValuePair_2_t636475144  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2260319927(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t737694438 *, KeyValuePair_2_t636475144 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2260319927_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Char>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2955624975_gshared (Dictionary_2_t737694438 * __this, KeyValuePair_2U5BU5D_t403702745* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2955624975(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t737694438 *, KeyValuePair_2U5BU5D_t403702745*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2955624975_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Char>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3360040860_gshared (Dictionary_2_t737694438 * __this, KeyValuePair_2_t636475144  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3360040860(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t737694438 *, KeyValuePair_2_t636475144 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3360040860_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Char>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m3838024110_gshared (Dictionary_2_t737694438 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m3838024110(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t737694438 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m3838024110_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Char>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m969270441_gshared (Dictionary_2_t737694438 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m969270441(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t737694438 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m969270441_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Char>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3294974118_gshared (Dictionary_2_t737694438 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3294974118(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t737694438 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3294974118_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Char>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2205446849_gshared (Dictionary_2_t737694438 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2205446849(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t737694438 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2205446849_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Char>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m3090218430_gshared (Dictionary_2_t737694438 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m3090218430(__this, method) ((  int32_t (*) (Dictionary_2_t737694438 *, const MethodInfo*))Dictionary_2_get_Count_m3090218430_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Char>::get_Item(TKey)
extern "C"  Il2CppChar Dictionary_2_get_Item_m1982276205_gshared (Dictionary_2_t737694438 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m1982276205(__this, ___key0, method) ((  Il2CppChar (*) (Dictionary_2_t737694438 *, Il2CppObject *, const MethodInfo*))Dictionary_2_get_Item_m1982276205_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Char>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m1681665942_gshared (Dictionary_2_t737694438 * __this, Il2CppObject * ___key0, Il2CppChar ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m1681665942(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t737694438 *, Il2CppObject *, Il2CppChar, const MethodInfo*))Dictionary_2_set_Item_m1681665942_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Char>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m4009193422_gshared (Dictionary_2_t737694438 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m4009193422(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t737694438 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m4009193422_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Char>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m979683753_gshared (Dictionary_2_t737694438 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m979683753(__this, ___size0, method) ((  void (*) (Dictionary_2_t737694438 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m979683753_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Char>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m2670528997_gshared (Dictionary_2_t737694438 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m2670528997(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t737694438 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m2670528997_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Char>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t636475144  Dictionary_2_make_pair_m3263101489_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, Il2CppChar ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m3263101489(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t636475144  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Il2CppChar, const MethodInfo*))Dictionary_2_make_pair_m3263101489_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Char>::pick_key(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_key_m449361573_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, Il2CppChar ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m449361573(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Il2CppChar, const MethodInfo*))Dictionary_2_pick_key_m449361573_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Char>::pick_value(TKey,TValue)
extern "C"  Il2CppChar Dictionary_2_pick_value_m1641689253_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, Il2CppChar ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m1641689253(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppChar (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Il2CppChar, const MethodInfo*))Dictionary_2_pick_value_m1641689253_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Char>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m3444384330_gshared (Dictionary_2_t737694438 * __this, KeyValuePair_2U5BU5D_t403702745* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m3444384330(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t737694438 *, KeyValuePair_2U5BU5D_t403702745*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m3444384330_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Char>::Resize()
extern "C"  void Dictionary_2_Resize_m1187687074_gshared (Dictionary_2_t737694438 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m1187687074(__this, method) ((  void (*) (Dictionary_2_t737694438 *, const MethodInfo*))Dictionary_2_Resize_m1187687074_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Char>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m399764767_gshared (Dictionary_2_t737694438 * __this, Il2CppObject * ___key0, Il2CppChar ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m399764767(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t737694438 *, Il2CppObject *, Il2CppChar, const MethodInfo*))Dictionary_2_Add_m399764767_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Char>::Clear()
extern "C"  void Dictionary_2_Clear_m2014643329_gshared (Dictionary_2_t737694438 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m2014643329(__this, method) ((  void (*) (Dictionary_2_t737694438 *, const MethodInfo*))Dictionary_2_Clear_m2014643329_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Char>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m2245780391_gshared (Dictionary_2_t737694438 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m2245780391(__this, ___key0, method) ((  bool (*) (Dictionary_2_t737694438 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsKey_m2245780391_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Char>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m2629766183_gshared (Dictionary_2_t737694438 * __this, Il2CppChar ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m2629766183(__this, ___value0, method) ((  bool (*) (Dictionary_2_t737694438 *, Il2CppChar, const MethodInfo*))Dictionary_2_ContainsValue_m2629766183_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Char>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m3188710644_gshared (Dictionary_2_t737694438 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m3188710644(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t737694438 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m3188710644_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Char>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m2914420208_gshared (Dictionary_2_t737694438 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m2914420208(__this, ___sender0, method) ((  void (*) (Dictionary_2_t737694438 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m2914420208_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Char>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m3006294249_gshared (Dictionary_2_t737694438 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m3006294249(__this, ___key0, method) ((  bool (*) (Dictionary_2_t737694438 *, Il2CppObject *, const MethodInfo*))Dictionary_2_Remove_m3006294249_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Char>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m4051281152_gshared (Dictionary_2_t737694438 * __this, Il2CppObject * ___key0, Il2CppChar* ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m4051281152(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t737694438 *, Il2CppObject *, Il2CppChar*, const MethodInfo*))Dictionary_2_TryGetValue_m4051281152_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Char>::get_Keys()
extern "C"  KeyCollection_t2364453889 * Dictionary_2_get_Keys_m3260392959_gshared (Dictionary_2_t737694438 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m3260392959(__this, method) ((  KeyCollection_t2364453889 * (*) (Dictionary_2_t737694438 *, const MethodInfo*))Dictionary_2_get_Keys_m3260392959_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Char>::get_Values()
extern "C"  ValueCollection_t3733267447 * Dictionary_2_get_Values_m2476484415_gshared (Dictionary_2_t737694438 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m2476484415(__this, method) ((  ValueCollection_t3733267447 * (*) (Dictionary_2_t737694438 *, const MethodInfo*))Dictionary_2_get_Values_m2476484415_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Char>::ToTKey(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTKey_m4194187776_gshared (Dictionary_2_t737694438 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m4194187776(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t737694438 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m4194187776_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Char>::ToTValue(System.Object)
extern "C"  Il2CppChar Dictionary_2_ToTValue_m4248970688_gshared (Dictionary_2_t737694438 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m4248970688(__this, ___value0, method) ((  Il2CppChar (*) (Dictionary_2_t737694438 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m4248970688_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Char>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m22400716_gshared (Dictionary_2_t737694438 * __this, KeyValuePair_2_t636475144  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m22400716(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t737694438 *, KeyValuePair_2_t636475144 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m22400716_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Char>::GetEnumerator()
extern "C"  Enumerator_t2055017830  Dictionary_2_GetEnumerator_m2426966427_gshared (Dictionary_2_t737694438 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m2426966427(__this, method) ((  Enumerator_t2055017830  (*) (Dictionary_2_t737694438 *, const MethodInfo*))Dictionary_2_GetEnumerator_m2426966427_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Char>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__0_m1659248426_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, Il2CppChar ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m1659248426(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Il2CppChar, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m1659248426_gshared)(__this /* static, unused */, ___key0, ___value1, method)
