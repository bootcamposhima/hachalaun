﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TransitionAnimation/<FadeAnimation>c__IteratorB
struct U3CFadeAnimationU3Ec__IteratorB_t1701036547;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void TransitionAnimation/<FadeAnimation>c__IteratorB::.ctor()
extern "C"  void U3CFadeAnimationU3Ec__IteratorB__ctor_m1740961992 (U3CFadeAnimationU3Ec__IteratorB_t1701036547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TransitionAnimation/<FadeAnimation>c__IteratorB::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFadeAnimationU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m84849034 (U3CFadeAnimationU3Ec__IteratorB_t1701036547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TransitionAnimation/<FadeAnimation>c__IteratorB::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFadeAnimationU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m3277392158 (U3CFadeAnimationU3Ec__IteratorB_t1701036547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TransitionAnimation/<FadeAnimation>c__IteratorB::MoveNext()
extern "C"  bool U3CFadeAnimationU3Ec__IteratorB_MoveNext_m3274700844 (U3CFadeAnimationU3Ec__IteratorB_t1701036547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransitionAnimation/<FadeAnimation>c__IteratorB::Dispose()
extern "C"  void U3CFadeAnimationU3Ec__IteratorB_Dispose_m2220479237 (U3CFadeAnimationU3Ec__IteratorB_t1701036547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransitionAnimation/<FadeAnimation>c__IteratorB::Reset()
extern "C"  void U3CFadeAnimationU3Ec__IteratorB_Reset_m3682362229 (U3CFadeAnimationU3Ec__IteratorB_t1701036547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
