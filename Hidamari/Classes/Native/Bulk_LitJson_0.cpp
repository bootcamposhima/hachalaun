﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Type
struct Type_t;
// LitJson.ExporterFunc
struct ExporterFunc_t3330360473;
// System.Object
struct Il2CppObject;
// LitJson.JsonWriter
struct JsonWriter_t1165300239;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;
// LitJson.FsmContext
struct FsmContext_t3936467683;
// LitJson.ImporterFunc
struct ImporterFunc_t2138319818;
// LitJson.JsonData
struct JsonData_t1715015430;
// System.String
struct String_t;
// System.Array
struct Il2CppArray;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Collections.IDictionary
struct IDictionary_t537317817;
// System.Collections.IList
struct IList_t1751339649;
// LitJson.IJsonWrapper
struct IJsonWrapper_t2026182966;
// LitJson.JsonException
struct JsonException_t3617621405;
// System.Exception
struct Exception_t3991598821;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// LitJson.JsonReader
struct JsonReader_t1009895007;
// LitJson.WrapperFactory
struct WrapperFactory_t3264289803;
// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>>
struct IDictionary_2_t1504911478;
// LitJson.JsonMockWrapper
struct JsonMockWrapper_t721699511;
// System.IO.TextReader
struct TextReader_t2148718976;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.IO.TextWriter
struct TextWriter_t2304124208;
// System.Char[]
struct CharU5BU5D_t3324145743;
// LitJson.Lexer
struct Lexer_t3664372066;
// LitJson.Lexer/StateHandler
struct StateHandler_t3261942315;
// System.Collections.Generic.IDictionary`2<System.String,LitJson.PropertyMetadata>
struct IDictionary_2_t169959035;
// LitJson.OrderedDictionaryEnumerator
struct OrderedDictionaryEnumerator_t3526912157;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>
struct IEnumerator_1_t51112259;
// LitJson.WriterContext
struct WriterContext_t3060158226;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "LitJson_U3CModuleU3E86524790.h"
#include "LitJson_U3CModuleU3E86524790MethodDeclarations.h"
#include "LitJson_U3CPrivateImplementationDetailsU3EU7Bd7db52637124422.h"
#include "LitJson_U3CPrivateImplementationDetailsU3EU7Bd7db52637124422MethodDeclarations.h"
#include "LitJson_U3CPrivateImplementationDetailsU3EU7Bd7db52216643725.h"
#include "LitJson_U3CPrivateImplementationDetailsU3EU7Bd7db52216643725MethodDeclarations.h"
#include "LitJson_U3CPrivateImplementationDetailsU3EU7Bd7db52565356614.h"
#include "LitJson_U3CPrivateImplementationDetailsU3EU7Bd7db52565356614MethodDeclarations.h"
#include "LitJson_LitJson_ArrayMetadata4058342910.h"
#include "LitJson_LitJson_ArrayMetadata4058342910MethodDeclarations.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Type2863145774MethodDeclarations.h"
#include "mscorlib_System_RuntimeTypeHandle2669177232.h"
#include "mscorlib_System_Void2863195528.h"
#include "mscorlib_System_Boolean476798718.h"
#include "LitJson_LitJson_Condition853519089.h"
#include "LitJson_LitJson_Condition853519089MethodDeclarations.h"
#include "LitJson_LitJson_ExporterFunc3330360473.h"
#include "LitJson_LitJson_ExporterFunc3330360473MethodDeclarations.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "LitJson_LitJson_JsonWriter1165300239.h"
#include "mscorlib_System_AsyncCallback1369114871.h"
#include "LitJson_LitJson_FsmContext3936467683.h"
#include "LitJson_LitJson_FsmContext3936467683MethodDeclarations.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "LitJson_LitJson_ImporterFunc2138319818.h"
#include "LitJson_LitJson_ImporterFunc2138319818MethodDeclarations.h"
#include "LitJson_LitJson_JsonData1715015430.h"
#include "LitJson_LitJson_JsonData1715015430MethodDeclarations.h"
#include "mscorlib_System_Int321153838500.h"
#include "mscorlib_System_ArgumentException928607144MethodDeclarations.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_ArgumentException928607144.h"
#include "LitJson_LitJson_JsonType1715515030.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22434214506.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22434214506MethodDeclarations.h"
#include "mscorlib_System_Double3868226565.h"
#include "mscorlib_System_Int641153838595.h"
#include "mscorlib_System_InvalidOperationException1589641621MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException1589641621.h"
#include "LitJson_LitJson_OrderedDictionaryEnumerator3526912157MethodDeclarations.h"
#include "LitJson_LitJson_OrderedDictionaryEnumerator3526912157.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2535433800MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3802400058MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2535433800.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3802400058.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3083200982MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3083200982.h"
#include "LitJson_LitJson_JsonWriter1165300239MethodDeclarations.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614MethodDeclarations.h"
#include "mscorlib_System_Int321153838500MethodDeclarations.h"
#include "mscorlib_System_Int641153838595MethodDeclarations.h"
#include "mscorlib_System_Double3868226565MethodDeclarations.h"
#include "mscorlib_System_Boolean476798718MethodDeclarations.h"
#include "mscorlib_System_IO_StringWriter4216882900MethodDeclarations.h"
#include "mscorlib_System_IO_StringWriter4216882900.h"
#include "mscorlib_System_IO_TextWriter2304124208.h"
#include "LitJson_LitJson_JsonException3617621405.h"
#include "LitJson_LitJson_JsonException3617621405MethodDeclarations.h"
#include "LitJson_LitJson_ParserToken608116400.h"
#include "mscorlib_System_Exception3991598821.h"
#include "mscorlib_System_ApplicationException3128023763MethodDeclarations.h"
#include "mscorlib_System_Char2862622538.h"
#include "LitJson_LitJson_JsonMapper863513565.h"
#include "LitJson_LitJson_JsonMapper863513565MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g4163765395MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge107454380MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2114716983MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2571737008MethodDeclarations.h"
#include "mscorlib_System_Globalization_DateTimeFormatInfo2490955586MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3435782958MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1927038133MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g4163765395.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge107454380.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2114716983.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2571737008.h"
#include "mscorlib_System_Globalization_DateTimeFormatInfo2490955586.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3435782958.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1927038133.h"
#include "mscorlib_System_Threading_Monitor2734674376MethodDeclarations.h"
#include "mscorlib_System_Reflection_PropertyInfo924268725.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Reflection_ParameterInfo2235474049.h"
#include "mscorlib_System_Reflection_MemberInfo3995515898MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo3995515898.h"
#include "mscorlib_System_Reflection_PropertyInfo924268725MethodDeclarations.h"
#include "mscorlib_System_Reflection_ParameterInfo2235474049MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge592085690MethodDeclarations.h"
#include "LitJson_LitJson_ObjectMetadata2009294498.h"
#include "LitJson_LitJson_PropertyMetadata4066634616.h"
#include "mscorlib_System_Reflection_FieldInfo3973053266.h"
#include "LitJson_LitJson_ObjectMetadata2009294498MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge592085690.h"
#include "mscorlib_System_Reflection_FieldInfo3973053266MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1139852872MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1139852872.h"
#include "mscorlib_System_Reflection_MethodInfo318736065.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge424158550MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge424158550.h"
#include "LitJson_LitJson_JsonReader1009895007.h"
#include "LitJson_LitJson_JsonReader1009895007MethodDeclarations.h"
#include "mscorlib_System_Nullable584944597MethodDeclarations.h"
#include "mscorlib_System_Enum2862688501MethodDeclarations.h"
#include "mscorlib_System_Reflection_MethodBase318515428MethodDeclarations.h"
#include "mscorlib_System_Activator2714366379MethodDeclarations.h"
#include "mscorlib_System_Collections_ArrayList3948406897MethodDeclarations.h"
#include "mscorlib_System_Array1146569071MethodDeclarations.h"
#include "LitJson_LitJson_JsonToken621800391.h"
#include "mscorlib_System_Reflection_MethodBase318515428.h"
#include "mscorlib_System_Collections_ArrayList3948406897.h"
#include "LitJson_LitJson_WrapperFactory3264289803.h"
#include "LitJson_LitJson_WrapperFactory3264289803MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2243742303MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2243742303.h"
#include "mscorlib_System_IO_TextWriter2304124208MethodDeclarations.h"
#include "mscorlib_System_Enum2862688501.h"
#include "mscorlib_System_UInt6424668076.h"
#include "LitJson_LitJson_JsonMockWrapper721699511MethodDeclarations.h"
#include "LitJson_LitJson_JsonMockWrapper721699511.h"
#include "mscorlib_System_Convert1363677321MethodDeclarations.h"
#include "mscorlib_System_Byte2862609660.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "mscorlib_System_Decimal1954350631.h"
#include "mscorlib_System_SByte1161769777.h"
#include "mscorlib_System_Int161153838442.h"
#include "mscorlib_System_UInt1624667923.h"
#include "mscorlib_System_UInt3224667981.h"
#include "mscorlib_System_Single4291918972.h"
#include "mscorlib_System_IO_StringReader4061477668MethodDeclarations.h"
#include "mscorlib_System_IO_StringReader4061477668.h"
#include "mscorlib_System_IO_TextReader2148718976.h"
#include "mscorlib_System_ArgumentNullException3573189601MethodDeclarations.h"
#include "System_System_Collections_Generic_Stack_1_gen4252399424MethodDeclarations.h"
#include "LitJson_LitJson_Lexer3664372066MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException3573189601.h"
#include "System_System_Collections_Generic_Stack_1_gen4252399424.h"
#include "LitJson_LitJson_Lexer3664372066.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2803247644MethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_RuntimeHe1593496111MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2803247644.h"
#include "mscorlib_System_RuntimeFieldHandle2347752062.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3228111060MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3228111060.h"
#include "mscorlib_System_UInt6424668076MethodDeclarations.h"
#include "mscorlib_System_IO_TextReader2148718976MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyNotFoundExce876339989.h"
#include "LitJson_LitJson_JsonToken621800391MethodDeclarations.h"
#include "LitJson_LitJson_JsonType1715515030MethodDeclarations.h"
#include "mscorlib_System_Globalization_NumberFormatInfo1637637232MethodDeclarations.h"
#include "mscorlib_System_Globalization_NumberFormatInfo1637637232.h"
#include "mscorlib_System_Text_StringBuilder243639308MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"
#include "LitJson_LitJson_WriterContext3060158226.h"
#include "System_System_Collections_Generic_Stack_1_gen1863751854MethodDeclarations.h"
#include "LitJson_LitJson_WriterContext3060158226MethodDeclarations.h"
#include "System_System_Collections_Generic_Stack_1_gen1863751854.h"
#include "LitJson_LitJson_Lexer_StateHandler3261942315MethodDeclarations.h"
#include "LitJson_ArrayTypes.h"
#include "LitJson_LitJson_Lexer_StateHandler3261942315.h"
#include "LitJson_LitJson_ParserToken608116400MethodDeclarations.h"
#include "LitJson_LitJson_PropertyMetadata4066634616MethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: <PrivateImplementationDetails>{d7db51ae-d199-4277-a2a4-4bdb68cd3ac9}/$ArrayType=112
extern "C" void U24ArrayTypeU3D112_t2216643725_marshal_pinvoke(const U24ArrayTypeU3D112_t2216643725& unmarshaled, U24ArrayTypeU3D112_t2216643725_marshaled_pinvoke& marshaled)
{
}
extern "C" void U24ArrayTypeU3D112_t2216643725_marshal_pinvoke_back(const U24ArrayTypeU3D112_t2216643725_marshaled_pinvoke& marshaled, U24ArrayTypeU3D112_t2216643725& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>{d7db51ae-d199-4277-a2a4-4bdb68cd3ac9}/$ArrayType=112
extern "C" void U24ArrayTypeU3D112_t2216643725_marshal_pinvoke_cleanup(U24ArrayTypeU3D112_t2216643725_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>{d7db51ae-d199-4277-a2a4-4bdb68cd3ac9}/$ArrayType=112
extern "C" void U24ArrayTypeU3D112_t2216643725_marshal_com(const U24ArrayTypeU3D112_t2216643725& unmarshaled, U24ArrayTypeU3D112_t2216643725_marshaled_com& marshaled)
{
}
extern "C" void U24ArrayTypeU3D112_t2216643725_marshal_com_back(const U24ArrayTypeU3D112_t2216643725_marshaled_com& marshaled, U24ArrayTypeU3D112_t2216643725& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>{d7db51ae-d199-4277-a2a4-4bdb68cd3ac9}/$ArrayType=112
extern "C" void U24ArrayTypeU3D112_t2216643725_marshal_com_cleanup(U24ArrayTypeU3D112_t2216643725_marshaled_com& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>{d7db51ae-d199-4277-a2a4-4bdb68cd3ac9}/$ArrayType=12
extern "C" void U24ArrayTypeU3D12_t2565356614_marshal_pinvoke(const U24ArrayTypeU3D12_t2565356614& unmarshaled, U24ArrayTypeU3D12_t2565356614_marshaled_pinvoke& marshaled)
{
}
extern "C" void U24ArrayTypeU3D12_t2565356614_marshal_pinvoke_back(const U24ArrayTypeU3D12_t2565356614_marshaled_pinvoke& marshaled, U24ArrayTypeU3D12_t2565356614& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>{d7db51ae-d199-4277-a2a4-4bdb68cd3ac9}/$ArrayType=12
extern "C" void U24ArrayTypeU3D12_t2565356614_marshal_pinvoke_cleanup(U24ArrayTypeU3D12_t2565356614_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>{d7db51ae-d199-4277-a2a4-4bdb68cd3ac9}/$ArrayType=12
extern "C" void U24ArrayTypeU3D12_t2565356614_marshal_com(const U24ArrayTypeU3D12_t2565356614& unmarshaled, U24ArrayTypeU3D12_t2565356614_marshaled_com& marshaled)
{
}
extern "C" void U24ArrayTypeU3D12_t2565356614_marshal_com_back(const U24ArrayTypeU3D12_t2565356614_marshaled_com& marshaled, U24ArrayTypeU3D12_t2565356614& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>{d7db51ae-d199-4277-a2a4-4bdb68cd3ac9}/$ArrayType=12
extern "C" void U24ArrayTypeU3D12_t2565356614_marshal_com_cleanup(U24ArrayTypeU3D12_t2565356614_marshaled_com& marshaled)
{
}
// System.Type LitJson.ArrayMetadata::get_ElementType()
extern const Il2CppType* JsonData_t1715015430_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t ArrayMetadata_get_ElementType_m1471053318_MetadataUsageId;
extern "C"  Type_t * ArrayMetadata_get_ElementType_m1471053318 (ArrayMetadata_t4058342910 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayMetadata_get_ElementType_m1471053318_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = __this->get_element_type_0();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(JsonData_t1715015430_0_0_0_var), /*hidden argument*/NULL);
		return L_1;
	}

IL_0016:
	{
		Type_t * L_2 = __this->get_element_type_0();
		return L_2;
	}
}
extern "C"  Type_t * ArrayMetadata_get_ElementType_m1471053318_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ArrayMetadata_t4058342910 * _thisAdjusted = reinterpret_cast<ArrayMetadata_t4058342910 *>(__this + 1);
	return ArrayMetadata_get_ElementType_m1471053318(_thisAdjusted, method);
}
// System.Void LitJson.ArrayMetadata::set_ElementType(System.Type)
extern "C"  void ArrayMetadata_set_ElementType_m478552961 (ArrayMetadata_t4058342910 * __this, Type_t * ___value0, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___value0;
		__this->set_element_type_0(L_0);
		return;
	}
}
extern "C"  void ArrayMetadata_set_ElementType_m478552961_AdjustorThunk (Il2CppObject * __this, Type_t * ___value0, const MethodInfo* method)
{
	ArrayMetadata_t4058342910 * _thisAdjusted = reinterpret_cast<ArrayMetadata_t4058342910 *>(__this + 1);
	ArrayMetadata_set_ElementType_m478552961(_thisAdjusted, ___value0, method);
}
// System.Boolean LitJson.ArrayMetadata::get_IsArray()
extern "C"  bool ArrayMetadata_get_IsArray_m858588955 (ArrayMetadata_t4058342910 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_is_array_1();
		return L_0;
	}
}
extern "C"  bool ArrayMetadata_get_IsArray_m858588955_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ArrayMetadata_t4058342910 * _thisAdjusted = reinterpret_cast<ArrayMetadata_t4058342910 *>(__this + 1);
	return ArrayMetadata_get_IsArray_m858588955(_thisAdjusted, method);
}
// System.Void LitJson.ArrayMetadata::set_IsArray(System.Boolean)
extern "C"  void ArrayMetadata_set_IsArray_m3904919016 (ArrayMetadata_t4058342910 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_is_array_1(L_0);
		return;
	}
}
extern "C"  void ArrayMetadata_set_IsArray_m3904919016_AdjustorThunk (Il2CppObject * __this, bool ___value0, const MethodInfo* method)
{
	ArrayMetadata_t4058342910 * _thisAdjusted = reinterpret_cast<ArrayMetadata_t4058342910 *>(__this + 1);
	ArrayMetadata_set_IsArray_m3904919016(_thisAdjusted, ___value0, method);
}
// System.Boolean LitJson.ArrayMetadata::get_IsList()
extern "C"  bool ArrayMetadata_get_IsList_m1858371582 (ArrayMetadata_t4058342910 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_is_list_2();
		return L_0;
	}
}
extern "C"  bool ArrayMetadata_get_IsList_m1858371582_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ArrayMetadata_t4058342910 * _thisAdjusted = reinterpret_cast<ArrayMetadata_t4058342910 *>(__this + 1);
	return ArrayMetadata_get_IsList_m1858371582(_thisAdjusted, method);
}
// System.Void LitJson.ArrayMetadata::set_IsList(System.Boolean)
extern "C"  void ArrayMetadata_set_IsList_m962711967 (ArrayMetadata_t4058342910 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_is_list_2(L_0);
		return;
	}
}
extern "C"  void ArrayMetadata_set_IsList_m962711967_AdjustorThunk (Il2CppObject * __this, bool ___value0, const MethodInfo* method)
{
	ArrayMetadata_t4058342910 * _thisAdjusted = reinterpret_cast<ArrayMetadata_t4058342910 *>(__this + 1);
	ArrayMetadata_set_IsList_m962711967(_thisAdjusted, ___value0, method);
}
// Conversion methods for marshalling of: LitJson.ArrayMetadata
extern "C" void ArrayMetadata_t4058342910_marshal_pinvoke(const ArrayMetadata_t4058342910& unmarshaled, ArrayMetadata_t4058342910_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___element_type_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'element_type' of type 'ArrayMetadata': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___element_type_0Exception);
}
extern "C" void ArrayMetadata_t4058342910_marshal_pinvoke_back(const ArrayMetadata_t4058342910_marshaled_pinvoke& marshaled, ArrayMetadata_t4058342910& unmarshaled)
{
	Il2CppCodeGenException* ___element_type_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'element_type' of type 'ArrayMetadata': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___element_type_0Exception);
}
// Conversion method for clean up from marshalling of: LitJson.ArrayMetadata
extern "C" void ArrayMetadata_t4058342910_marshal_pinvoke_cleanup(ArrayMetadata_t4058342910_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: LitJson.ArrayMetadata
extern "C" void ArrayMetadata_t4058342910_marshal_com(const ArrayMetadata_t4058342910& unmarshaled, ArrayMetadata_t4058342910_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___element_type_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'element_type' of type 'ArrayMetadata': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___element_type_0Exception);
}
extern "C" void ArrayMetadata_t4058342910_marshal_com_back(const ArrayMetadata_t4058342910_marshaled_com& marshaled, ArrayMetadata_t4058342910& unmarshaled)
{
	Il2CppCodeGenException* ___element_type_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'element_type' of type 'ArrayMetadata': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___element_type_0Exception);
}
// Conversion method for clean up from marshalling of: LitJson.ArrayMetadata
extern "C" void ArrayMetadata_t4058342910_marshal_com_cleanup(ArrayMetadata_t4058342910_marshaled_com& marshaled)
{
}
// System.Void LitJson.ExporterFunc::.ctor(System.Object,System.IntPtr)
extern "C"  void ExporterFunc__ctor_m657802314 (ExporterFunc_t3330360473 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void LitJson.ExporterFunc::Invoke(System.Object,LitJson.JsonWriter)
extern "C"  void ExporterFunc_Invoke_m4274113196 (ExporterFunc_t3330360473 * __this, Il2CppObject * ___obj0, JsonWriter_t1165300239 * ___writer1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ExporterFunc_Invoke_m4274113196((ExporterFunc_t3330360473 *)__this->get_prev_9(),___obj0, ___writer1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___obj0, JsonWriter_t1165300239 * ___writer1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0, ___writer1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___obj0, JsonWriter_t1165300239 * ___writer1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0, ___writer1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, JsonWriter_t1165300239 * ___writer1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___obj0, ___writer1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult LitJson.ExporterFunc::BeginInvoke(System.Object,LitJson.JsonWriter,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ExporterFunc_BeginInvoke_m641377751 (ExporterFunc_t3330360473 * __this, Il2CppObject * ___obj0, JsonWriter_t1165300239 * ___writer1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___obj0;
	__d_args[1] = ___writer1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void LitJson.ExporterFunc::EndInvoke(System.IAsyncResult)
extern "C"  void ExporterFunc_EndInvoke_m1771727706 (ExporterFunc_t3330360473 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void LitJson.FsmContext::.ctor()
extern "C"  void FsmContext__ctor_m4002008078 (FsmContext_t3936467683 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.ImporterFunc::.ctor(System.Object,System.IntPtr)
extern "C"  void ImporterFunc__ctor_m35661563 (ImporterFunc_t2138319818 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Object LitJson.ImporterFunc::Invoke(System.Object)
extern "C"  Il2CppObject * ImporterFunc_Invoke_m2708021386 (ImporterFunc_t2138319818 * __this, Il2CppObject * ___input0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ImporterFunc_Invoke_m2708021386((ImporterFunc_t2138319818 *)__this->get_prev_9(),___input0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___input0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___input0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___input0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___input0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___input0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult LitJson.ImporterFunc::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ImporterFunc_BeginInvoke_m1227913920 (ImporterFunc_t2138319818 * __this, Il2CppObject * ___input0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___input0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Object LitJson.ImporterFunc::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * ImporterFunc_EndInvoke_m472251136 (ImporterFunc_t2138319818 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Int32 LitJson.JsonData::System.Collections.ICollection.get_Count()
extern "C"  int32_t JsonData_System_Collections_ICollection_get_Count_m3055399888 (JsonData_t1715015430 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = JsonData_get_Count_m412158079(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean LitJson.JsonData::System.Collections.ICollection.get_IsSynchronized()
extern Il2CppClass* ICollection_t2643922881_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_System_Collections_ICollection_get_IsSynchronized_m856228537_MetadataUsageId;
extern "C"  bool JsonData_System_Collections_ICollection_get_IsSynchronized_m856228537 (JsonData_t1715015430 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_ICollection_get_IsSynchronized_m856228537_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureCollection_m467105989(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.ICollection::get_IsSynchronized() */, ICollection_t2643922881_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Object LitJson.JsonData::System.Collections.ICollection.get_SyncRoot()
extern Il2CppClass* ICollection_t2643922881_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_System_Collections_ICollection_get_SyncRoot_m620571225_MetadataUsageId;
extern "C"  Il2CppObject * JsonData_System_Collections_ICollection_get_SyncRoot_m620571225 (JsonData_t1715015430 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_ICollection_get_SyncRoot_m620571225_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureCollection_m467105989(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t2643922881_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Boolean LitJson.JsonData::LitJson.IJsonWrapper.get_IsArray()
extern "C"  bool JsonData_LitJson_IJsonWrapper_get_IsArray_m2686901208 (JsonData_t1715015430 * __this, const MethodInfo* method)
{
	{
		bool L_0 = JsonData_get_IsArray_m2198308313(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean LitJson.JsonData::LitJson.IJsonWrapper.get_IsBoolean()
extern "C"  bool JsonData_LitJson_IJsonWrapper_get_IsBoolean_m26469991 (JsonData_t1715015430 * __this, const MethodInfo* method)
{
	{
		bool L_0 = JsonData_get_IsBoolean_m2935100456(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean LitJson.JsonData::LitJson.IJsonWrapper.get_IsDouble()
extern "C"  bool JsonData_LitJson_IJsonWrapper_get_IsDouble_m46993812 (JsonData_t1715015430 * __this, const MethodInfo* method)
{
	{
		bool L_0 = JsonData_get_IsDouble_m2080483251(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean LitJson.JsonData::LitJson.IJsonWrapper.get_IsInt()
extern "C"  bool JsonData_LitJson_IJsonWrapper_get_IsInt_m2705034094 (JsonData_t1715015430 * __this, const MethodInfo* method)
{
	{
		bool L_0 = JsonData_get_IsInt_m76595631(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean LitJson.JsonData::LitJson.IJsonWrapper.get_IsLong()
extern "C"  bool JsonData_LitJson_IJsonWrapper_get_IsLong_m2338371071 (JsonData_t1715015430 * __this, const MethodInfo* method)
{
	{
		bool L_0 = JsonData_get_IsLong_m2461157342(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean LitJson.JsonData::LitJson.IJsonWrapper.get_IsObject()
extern "C"  bool JsonData_LitJson_IJsonWrapper_get_IsObject_m3072988546 (JsonData_t1715015430 * __this, const MethodInfo* method)
{
	{
		bool L_0 = JsonData_get_IsObject_m811510689(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean LitJson.JsonData::LitJson.IJsonWrapper.get_IsString()
extern "C"  bool JsonData_LitJson_IJsonWrapper_get_IsString_m482534868 (JsonData_t1715015430 * __this, const MethodInfo* method)
{
	{
		bool L_0 = JsonData_get_IsString_m2516024307(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean LitJson.JsonData::System.Collections.IList.get_IsFixedSize()
extern Il2CppClass* IList_t1751339649_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_System_Collections_IList_get_IsFixedSize_m824924018_MetadataUsageId;
extern "C"  bool JsonData_System_Collections_IList_get_IsFixedSize_m824924018 (JsonData_t1715015430 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IList_get_IsFixedSize_m824924018_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureList_m2914899205(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IList::get_IsFixedSize() */, IList_t1751339649_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Boolean LitJson.JsonData::System.Collections.IList.get_IsReadOnly()
extern Il2CppClass* IList_t1751339649_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_System_Collections_IList_get_IsReadOnly_m2143187271_MetadataUsageId;
extern "C"  bool JsonData_System_Collections_IList_get_IsReadOnly_m2143187271 (JsonData_t1715015430 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IList_get_IsReadOnly_m2143187271_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureList_m2914899205(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IList::get_IsReadOnly() */, IList_t1751339649_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Object LitJson.JsonData::System.Collections.IDictionary.get_Item(System.Object)
extern Il2CppClass* IDictionary_t537317817_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_System_Collections_IDictionary_get_Item_m2993080477_MetadataUsageId;
extern "C"  Il2CppObject * JsonData_System_Collections_IDictionary_get_Item_m2993080477 (JsonData_t1715015430 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IDictionary_get_Item_m2993080477_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureDictionary_m4167775685(__this, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___key0;
		NullCheck(L_0);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t537317817_il2cpp_TypeInfo_var, L_0, L_1);
		return L_2;
	}
}
// System.Void LitJson.JsonData::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3336764446;
extern const uint32_t JsonData_System_Collections_IDictionary_set_Item_m1606342530_MetadataUsageId;
extern "C"  void JsonData_System_Collections_IDictionary_set_Item_m1606342530 (JsonData_t1715015430 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IDictionary_set_Item_m1606342530_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JsonData_t1715015430 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___key0;
		if (((String_t*)IsInstSealed(L_0, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_0016;
		}
	}
	{
		ArgumentException_t928607144 * L_1 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_1, _stringLiteral3336764446, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Il2CppObject * L_2 = ___value1;
		JsonData_t1715015430 * L_3 = JsonData_ToJsonData_m3725295152(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Il2CppObject * L_4 = ___key0;
		JsonData_t1715015430 * L_5 = V_0;
		JsonData_set_Item_m3307919212(__this, ((String_t*)CastclassSealed(L_4, String_t_il2cpp_TypeInfo_var)), L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Object LitJson.JsonData::System.Collections.IList.get_Item(System.Int32)
extern Il2CppClass* IList_t1751339649_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_System_Collections_IList_get_Item_m434104736_MetadataUsageId;
extern "C"  Il2CppObject * JsonData_System_Collections_IList_get_Item_m434104736 (JsonData_t1715015430 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IList_get_Item_m434104736_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureList_m2914899205(__this, /*hidden argument*/NULL);
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(2 /* System.Object System.Collections.IList::get_Item(System.Int32) */, IList_t1751339649_il2cpp_TypeInfo_var, L_0, L_1);
		return L_2;
	}
}
// System.Void LitJson.JsonData::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void JsonData_System_Collections_IList_set_Item_m1995460215 (JsonData_t1715015430 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	JsonData_t1715015430 * V_0 = NULL;
	{
		JsonData_EnsureList_m2914899205(__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___value1;
		JsonData_t1715015430 * L_1 = JsonData_ToJsonData_m3725295152(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = ___index0;
		JsonData_t1715015430 * L_3 = V_0;
		JsonData_set_Item_m1400309003(__this, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 LitJson.JsonData::get_Count()
extern Il2CppClass* ICollection_t2643922881_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_get_Count_m412158079_MetadataUsageId;
extern "C"  int32_t JsonData_get_Count_m412158079 (JsonData_t1715015430 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonData_get_Count_m412158079_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureCollection_m467105989(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.ICollection::get_Count() */, ICollection_t2643922881_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Boolean LitJson.JsonData::get_IsArray()
extern "C"  bool JsonData_get_IsArray_m2198308313 (JsonData_t1715015430 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_type_8();
		return (bool)((((int32_t)L_0) == ((int32_t)2))? 1 : 0);
	}
}
// System.Boolean LitJson.JsonData::get_IsBoolean()
extern "C"  bool JsonData_get_IsBoolean_m2935100456 (JsonData_t1715015430 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_type_8();
		return (bool)((((int32_t)L_0) == ((int32_t)7))? 1 : 0);
	}
}
// System.Boolean LitJson.JsonData::get_IsDouble()
extern "C"  bool JsonData_get_IsDouble_m2080483251 (JsonData_t1715015430 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_type_8();
		return (bool)((((int32_t)L_0) == ((int32_t)6))? 1 : 0);
	}
}
// System.Boolean LitJson.JsonData::get_IsInt()
extern "C"  bool JsonData_get_IsInt_m76595631 (JsonData_t1715015430 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_type_8();
		return (bool)((((int32_t)L_0) == ((int32_t)4))? 1 : 0);
	}
}
// System.Boolean LitJson.JsonData::get_IsLong()
extern "C"  bool JsonData_get_IsLong_m2461157342 (JsonData_t1715015430 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_type_8();
		return (bool)((((int32_t)L_0) == ((int32_t)5))? 1 : 0);
	}
}
// System.Boolean LitJson.JsonData::get_IsObject()
extern "C"  bool JsonData_get_IsObject_m811510689 (JsonData_t1715015430 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_type_8();
		return (bool)((((int32_t)L_0) == ((int32_t)1))? 1 : 0);
	}
}
// System.Boolean LitJson.JsonData::get_IsString()
extern "C"  bool JsonData_get_IsString_m2516024307 (JsonData_t1715015430 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_type_8();
		return (bool)((((int32_t)L_0) == ((int32_t)3))? 1 : 0);
	}
}
// System.Void LitJson.JsonData::set_Item(System.String,LitJson.JsonData)
extern Il2CppClass* IDictionary_2_t2113307145_il2cpp_TypeInfo_var;
extern Il2CppClass* IList_1_t833894413_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ICollection_1_t3328804493_il2cpp_TypeInfo_var;
extern const MethodInfo* KeyValuePair_2__ctor_m1796039453_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m3350570668_MethodInfo_var;
extern const uint32_t JsonData_set_Item_m3307919212_MetadataUsageId;
extern "C"  void JsonData_set_Item_m3307919212 (JsonData_t1715015430 * __this, String_t* ___prop_name0, JsonData_t1715015430 * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonData_set_Item_m3307919212_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2_t2434214506  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	KeyValuePair_2_t2434214506  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		JsonData_EnsureDictionary_m4167775685(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___prop_name0;
		JsonData_t1715015430 * L_1 = ___value1;
		KeyValuePair_2__ctor_m1796039453((&V_0), L_0, L_1, /*hidden argument*/KeyValuePair_2__ctor_m1796039453_MethodInfo_var);
		Il2CppObject* L_2 = __this->get_inst_object_5();
		String_t* L_3 = ___prop_name0;
		NullCheck(L_2);
		bool L_4 = InterfaceFuncInvoker1< bool, String_t* >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,LitJson.JsonData>::ContainsKey(!0) */, IDictionary_2_t2113307145_il2cpp_TypeInfo_var, L_2, L_3);
		if (!L_4)
		{
			goto IL_0073;
		}
	}
	{
		V_1 = 0;
		goto IL_005d;
	}

IL_0028:
	{
		Il2CppObject* L_5 = __this->get_object_list_9();
		int32_t L_6 = V_1;
		NullCheck(L_5);
		KeyValuePair_2_t2434214506  L_7 = InterfaceFuncInvoker1< KeyValuePair_2_t2434214506 , int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>::get_Item(System.Int32) */, IList_1_t833894413_il2cpp_TypeInfo_var, L_5, L_6);
		V_2 = L_7;
		String_t* L_8 = KeyValuePair_2_get_Key_m3350570668((&V_2), /*hidden argument*/KeyValuePair_2_get_Key_m3350570668_MethodInfo_var);
		String_t* L_9 = ___prop_name0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0059;
		}
	}
	{
		Il2CppObject* L_11 = __this->get_object_list_9();
		int32_t L_12 = V_1;
		KeyValuePair_2_t2434214506  L_13 = V_0;
		NullCheck(L_11);
		InterfaceActionInvoker2< int32_t, KeyValuePair_2_t2434214506  >::Invoke(4 /* System.Void System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>::set_Item(System.Int32,!0) */, IList_1_t833894413_il2cpp_TypeInfo_var, L_11, L_12, L_13);
		goto IL_006e;
	}

IL_0059:
	{
		int32_t L_14 = V_1;
		V_1 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_005d:
	{
		int32_t L_15 = V_1;
		Il2CppObject* L_16 = __this->get_object_list_9();
		NullCheck(L_16);
		int32_t L_17 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>::get_Count() */, ICollection_1_t3328804493_il2cpp_TypeInfo_var, L_16);
		if ((((int32_t)L_15) < ((int32_t)L_17)))
		{
			goto IL_0028;
		}
	}

IL_006e:
	{
		goto IL_007f;
	}

IL_0073:
	{
		Il2CppObject* L_18 = __this->get_object_list_9();
		KeyValuePair_2_t2434214506  L_19 = V_0;
		NullCheck(L_18);
		InterfaceActionInvoker1< KeyValuePair_2_t2434214506  >::Invoke(2 /* System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>::Add(!0) */, ICollection_1_t3328804493_il2cpp_TypeInfo_var, L_18, L_19);
	}

IL_007f:
	{
		Il2CppObject* L_20 = __this->get_inst_object_5();
		String_t* L_21 = ___prop_name0;
		JsonData_t1715015430 * L_22 = ___value1;
		NullCheck(L_20);
		InterfaceActionInvoker2< String_t*, JsonData_t1715015430 * >::Invoke(3 /* System.Void System.Collections.Generic.IDictionary`2<System.String,LitJson.JsonData>::set_Item(!0,!1) */, IDictionary_2_t2113307145_il2cpp_TypeInfo_var, L_20, L_21, L_22);
		__this->set_json_7((String_t*)NULL);
		return;
	}
}
// System.Void LitJson.JsonData::set_Item(System.Int32,LitJson.JsonData)
extern Il2CppClass* IList_1_t114695337_il2cpp_TypeInfo_var;
extern Il2CppClass* IList_1_t833894413_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t2113307145_il2cpp_TypeInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m3350570668_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2__ctor_m1796039453_MethodInfo_var;
extern const uint32_t JsonData_set_Item_m1400309003_MetadataUsageId;
extern "C"  void JsonData_set_Item_m1400309003 (JsonData_t1715015430 * __this, int32_t ___index0, JsonData_t1715015430 * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonData_set_Item_m1400309003_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2_t2434214506  V_0;
	memset(&V_0, 0, sizeof(V_0));
	KeyValuePair_2_t2434214506  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		JsonData_EnsureCollection_m467105989(__this, /*hidden argument*/NULL);
		int32_t L_0 = __this->get_type_8();
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_0025;
		}
	}
	{
		Il2CppObject* L_1 = __this->get_inst_array_0();
		int32_t L_2 = ___index0;
		JsonData_t1715015430 * L_3 = ___value1;
		NullCheck(L_1);
		InterfaceActionInvoker2< int32_t, JsonData_t1715015430 * >::Invoke(4 /* System.Void System.Collections.Generic.IList`1<LitJson.JsonData>::set_Item(System.Int32,!0) */, IList_1_t114695337_il2cpp_TypeInfo_var, L_1, L_2, L_3);
		goto IL_0061;
	}

IL_0025:
	{
		Il2CppObject* L_4 = __this->get_object_list_9();
		int32_t L_5 = ___index0;
		NullCheck(L_4);
		KeyValuePair_2_t2434214506  L_6 = InterfaceFuncInvoker1< KeyValuePair_2_t2434214506 , int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>::get_Item(System.Int32) */, IList_1_t833894413_il2cpp_TypeInfo_var, L_4, L_5);
		V_0 = L_6;
		String_t* L_7 = KeyValuePair_2_get_Key_m3350570668((&V_0), /*hidden argument*/KeyValuePair_2_get_Key_m3350570668_MethodInfo_var);
		JsonData_t1715015430 * L_8 = ___value1;
		KeyValuePair_2__ctor_m1796039453((&V_1), L_7, L_8, /*hidden argument*/KeyValuePair_2__ctor_m1796039453_MethodInfo_var);
		Il2CppObject* L_9 = __this->get_object_list_9();
		int32_t L_10 = ___index0;
		KeyValuePair_2_t2434214506  L_11 = V_1;
		NullCheck(L_9);
		InterfaceActionInvoker2< int32_t, KeyValuePair_2_t2434214506  >::Invoke(4 /* System.Void System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>::set_Item(System.Int32,!0) */, IList_1_t833894413_il2cpp_TypeInfo_var, L_9, L_10, L_11);
		Il2CppObject* L_12 = __this->get_inst_object_5();
		String_t* L_13 = KeyValuePair_2_get_Key_m3350570668((&V_0), /*hidden argument*/KeyValuePair_2_get_Key_m3350570668_MethodInfo_var);
		JsonData_t1715015430 * L_14 = ___value1;
		NullCheck(L_12);
		InterfaceActionInvoker2< String_t*, JsonData_t1715015430 * >::Invoke(3 /* System.Void System.Collections.Generic.IDictionary`2<System.String,LitJson.JsonData>::set_Item(!0,!1) */, IDictionary_2_t2113307145_il2cpp_TypeInfo_var, L_12, L_13, L_14);
	}

IL_0061:
	{
		__this->set_json_7((String_t*)NULL);
		return;
	}
}
// System.Void LitJson.JsonData::.ctor(System.Object)
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t1153838595_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral413459003;
extern const uint32_t JsonData__ctor_m2642779625_MetadataUsageId;
extern "C"  void JsonData__ctor_m2642779625 (JsonData_t1715015430 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonData__ctor_m2642779625_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___obj0;
		if (!((Il2CppObject *)IsInstSealed(L_0, Boolean_t476798718_il2cpp_TypeInfo_var)))
		{
			goto IL_0025;
		}
	}
	{
		__this->set_type_8(7);
		Il2CppObject * L_1 = ___obj0;
		__this->set_inst_boolean_1(((*(bool*)((bool*)UnBox (L_1, Boolean_t476798718_il2cpp_TypeInfo_var)))));
		return;
	}

IL_0025:
	{
		Il2CppObject * L_2 = ___obj0;
		if (!((Il2CppObject *)IsInstSealed(L_2, Double_t3868226565_il2cpp_TypeInfo_var)))
		{
			goto IL_0044;
		}
	}
	{
		__this->set_type_8(6);
		Il2CppObject * L_3 = ___obj0;
		__this->set_inst_double_2(((*(double*)((double*)UnBox (L_3, Double_t3868226565_il2cpp_TypeInfo_var)))));
		return;
	}

IL_0044:
	{
		Il2CppObject * L_4 = ___obj0;
		if (!((Il2CppObject *)IsInstSealed(L_4, Int32_t1153838500_il2cpp_TypeInfo_var)))
		{
			goto IL_0063;
		}
	}
	{
		__this->set_type_8(4);
		Il2CppObject * L_5 = ___obj0;
		__this->set_inst_int_3(((*(int32_t*)((int32_t*)UnBox (L_5, Int32_t1153838500_il2cpp_TypeInfo_var)))));
		return;
	}

IL_0063:
	{
		Il2CppObject * L_6 = ___obj0;
		if (!((Il2CppObject *)IsInstSealed(L_6, Int64_t1153838595_il2cpp_TypeInfo_var)))
		{
			goto IL_0082;
		}
	}
	{
		__this->set_type_8(5);
		Il2CppObject * L_7 = ___obj0;
		__this->set_inst_long_4(((*(int64_t*)((int64_t*)UnBox (L_7, Int64_t1153838595_il2cpp_TypeInfo_var)))));
		return;
	}

IL_0082:
	{
		Il2CppObject * L_8 = ___obj0;
		if (!((String_t*)IsInstSealed(L_8, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_00a1;
		}
	}
	{
		__this->set_type_8(3);
		Il2CppObject * L_9 = ___obj0;
		__this->set_inst_string_6(((String_t*)CastclassSealed(L_9, String_t_il2cpp_TypeInfo_var)));
		return;
	}

IL_00a1:
	{
		ArgumentException_t928607144 * L_10 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_10, _stringLiteral413459003, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
	}
}
// System.Void LitJson.JsonData::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern Il2CppClass* ICollection_t2643922881_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_System_Collections_ICollection_CopyTo_m1119522329_MetadataUsageId;
extern "C"  void JsonData_System_Collections_ICollection_CopyTo_m1119522329 (JsonData_t1715015430 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_ICollection_CopyTo_m1119522329_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureCollection_m467105989(__this, /*hidden argument*/NULL);
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck(L_0);
		InterfaceActionInvoker2< Il2CppArray *, int32_t >::Invoke(3 /* System.Void System.Collections.ICollection::CopyTo(System.Array,System.Int32) */, ICollection_t2643922881_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return;
	}
}
// System.Void LitJson.JsonData::System.Collections.IDictionary.Add(System.Object,System.Object)
extern Il2CppClass* IDictionary_t537317817_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ICollection_1_t3328804493_il2cpp_TypeInfo_var;
extern const MethodInfo* KeyValuePair_2__ctor_m1796039453_MethodInfo_var;
extern const uint32_t JsonData_System_Collections_IDictionary_Add_m4028317071_MetadataUsageId;
extern "C"  void JsonData_System_Collections_IDictionary_Add_m4028317071 (JsonData_t1715015430 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IDictionary_Add_m4028317071_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JsonData_t1715015430 * V_0 = NULL;
	KeyValuePair_2_t2434214506  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Il2CppObject * L_0 = ___value1;
		JsonData_t1715015430 * L_1 = JsonData_ToJsonData_m3725295152(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Il2CppObject * L_2 = JsonData_EnsureDictionary_m4167775685(__this, /*hidden argument*/NULL);
		Il2CppObject * L_3 = ___key0;
		JsonData_t1715015430 * L_4 = V_0;
		NullCheck(L_2);
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Void System.Collections.IDictionary::Add(System.Object,System.Object) */, IDictionary_t537317817_il2cpp_TypeInfo_var, L_2, L_3, L_4);
		Il2CppObject * L_5 = ___key0;
		JsonData_t1715015430 * L_6 = V_0;
		KeyValuePair_2__ctor_m1796039453((&V_1), ((String_t*)CastclassSealed(L_5, String_t_il2cpp_TypeInfo_var)), L_6, /*hidden argument*/KeyValuePair_2__ctor_m1796039453_MethodInfo_var);
		Il2CppObject* L_7 = __this->get_object_list_9();
		KeyValuePair_2_t2434214506  L_8 = V_1;
		NullCheck(L_7);
		InterfaceActionInvoker1< KeyValuePair_2_t2434214506  >::Invoke(2 /* System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>::Add(!0) */, ICollection_1_t3328804493_il2cpp_TypeInfo_var, L_7, L_8);
		__this->set_json_7((String_t*)NULL);
		return;
	}
}
// System.Collections.IDictionaryEnumerator LitJson.JsonData::System.Collections.IDictionary.GetEnumerator()
extern Il2CppClass* IOrderedDictionary_t3308796194_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_System_Collections_IDictionary_GetEnumerator_m2594797152_MetadataUsageId;
extern "C"  Il2CppObject * JsonData_System_Collections_IDictionary_GetEnumerator_m2594797152 (JsonData_t1715015430 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IDictionary_GetEnumerator_m2594797152_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IDictionaryEnumerator System.Collections.Specialized.IOrderedDictionary::GetEnumerator() */, IOrderedDictionary_t3308796194_il2cpp_TypeInfo_var, __this);
		return L_0;
	}
}
// System.Void LitJson.JsonData::System.Collections.IDictionary.Remove(System.Object)
extern Il2CppClass* IDictionary_t537317817_il2cpp_TypeInfo_var;
extern Il2CppClass* IList_1_t833894413_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ICollection_1_t3328804493_il2cpp_TypeInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m3350570668_MethodInfo_var;
extern const uint32_t JsonData_System_Collections_IDictionary_Remove_m1606718912_MetadataUsageId;
extern "C"  void JsonData_System_Collections_IDictionary_Remove_m1606718912 (JsonData_t1715015430 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IDictionary_Remove_m1606718912_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2_t2434214506  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Il2CppObject * L_0 = JsonData_EnsureDictionary_m4167775685(__this, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___key0;
		NullCheck(L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(4 /* System.Void System.Collections.IDictionary::Remove(System.Object) */, IDictionary_t537317817_il2cpp_TypeInfo_var, L_0, L_1);
		V_0 = 0;
		goto IL_004c;
	}

IL_0013:
	{
		Il2CppObject* L_2 = __this->get_object_list_9();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		KeyValuePair_2_t2434214506  L_4 = InterfaceFuncInvoker1< KeyValuePair_2_t2434214506 , int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>::get_Item(System.Int32) */, IList_1_t833894413_il2cpp_TypeInfo_var, L_2, L_3);
		V_1 = L_4;
		String_t* L_5 = KeyValuePair_2_get_Key_m3350570668((&V_1), /*hidden argument*/KeyValuePair_2_get_Key_m3350570668_MethodInfo_var);
		Il2CppObject * L_6 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_5, ((String_t*)CastclassSealed(L_6, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0048;
		}
	}
	{
		Il2CppObject* L_8 = __this->get_object_list_9();
		int32_t L_9 = V_0;
		NullCheck(L_8);
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>::RemoveAt(System.Int32) */, IList_1_t833894413_il2cpp_TypeInfo_var, L_8, L_9);
		goto IL_005d;
	}

IL_0048:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_004c:
	{
		int32_t L_11 = V_0;
		Il2CppObject* L_12 = __this->get_object_list_9();
		NullCheck(L_12);
		int32_t L_13 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>::get_Count() */, ICollection_1_t3328804493_il2cpp_TypeInfo_var, L_12);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_0013;
		}
	}

IL_005d:
	{
		__this->set_json_7((String_t*)NULL);
		return;
	}
}
// System.Collections.IEnumerator LitJson.JsonData::System.Collections.IEnumerable.GetEnumerator()
extern Il2CppClass* IEnumerable_t3464557803_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_System_Collections_IEnumerable_GetEnumerator_m1548196424_MetadataUsageId;
extern "C"  Il2CppObject * JsonData_System_Collections_IEnumerable_GetEnumerator_m1548196424 (JsonData_t1715015430 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IEnumerable_GetEnumerator_m1548196424_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureCollection_m467105989(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t3464557803_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Boolean LitJson.JsonData::LitJson.IJsonWrapper.GetBoolean()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2253496515;
extern const uint32_t JsonData_LitJson_IJsonWrapper_GetBoolean_m2538048866_MetadataUsageId;
extern "C"  bool JsonData_LitJson_IJsonWrapper_GetBoolean_m2538048866 (JsonData_t1715015430 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonData_LitJson_IJsonWrapper_GetBoolean_m2538048866_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = __this->get_type_8();
		if ((((int32_t)L_0) == ((int32_t)7)))
		{
			goto IL_0017;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, _stringLiteral2253496515, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		bool L_2 = __this->get_inst_boolean_1();
		return L_2;
	}
}
// System.Double LitJson.JsonData::LitJson.IJsonWrapper.GetDouble()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2901067734;
extern const uint32_t JsonData_LitJson_IJsonWrapper_GetDouble_m1436906150_MetadataUsageId;
extern "C"  double JsonData_LitJson_IJsonWrapper_GetDouble_m1436906150 (JsonData_t1715015430 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonData_LitJson_IJsonWrapper_GetDouble_m1436906150_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = __this->get_type_8();
		if ((((int32_t)L_0) == ((int32_t)6)))
		{
			goto IL_0017;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, _stringLiteral2901067734, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		double L_2 = __this->get_inst_double_2();
		return L_2;
	}
}
// System.Int32 LitJson.JsonData::LitJson.IJsonWrapper.GetInt()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3125532258;
extern const uint32_t JsonData_LitJson_IJsonWrapper_GetInt_m3414412559_MetadataUsageId;
extern "C"  int32_t JsonData_LitJson_IJsonWrapper_GetInt_m3414412559 (JsonData_t1715015430 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonData_LitJson_IJsonWrapper_GetInt_m3414412559_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = __this->get_type_8();
		if ((((int32_t)L_0) == ((int32_t)4)))
		{
			goto IL_0017;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, _stringLiteral3125532258, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		int32_t L_2 = __this->get_inst_int_3();
		return L_2;
	}
}
// System.Int64 LitJson.JsonData::LitJson.IJsonWrapper.GetLong()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3055767489;
extern const uint32_t JsonData_LitJson_IJsonWrapper_GetLong_m1477791231_MetadataUsageId;
extern "C"  int64_t JsonData_LitJson_IJsonWrapper_GetLong_m1477791231 (JsonData_t1715015430 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonData_LitJson_IJsonWrapper_GetLong_m1477791231_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = __this->get_type_8();
		if ((((int32_t)L_0) == ((int32_t)5)))
		{
			goto IL_0017;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, _stringLiteral3055767489, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		int64_t L_2 = __this->get_inst_long_4();
		return L_2;
	}
}
// System.String LitJson.JsonData::LitJson.IJsonWrapper.GetString()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3335040022;
extern const uint32_t JsonData_LitJson_IJsonWrapper_GetString_m1514240678_MetadataUsageId;
extern "C"  String_t* JsonData_LitJson_IJsonWrapper_GetString_m1514240678 (JsonData_t1715015430 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonData_LitJson_IJsonWrapper_GetString_m1514240678_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = __this->get_type_8();
		if ((((int32_t)L_0) == ((int32_t)3)))
		{
			goto IL_0017;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, _stringLiteral3335040022, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		String_t* L_2 = __this->get_inst_string_6();
		return L_2;
	}
}
// System.Void LitJson.JsonData::LitJson.IJsonWrapper.SetBoolean(System.Boolean)
extern "C"  void JsonData_LitJson_IJsonWrapper_SetBoolean_m1679717379 (JsonData_t1715015430 * __this, bool ___val0, const MethodInfo* method)
{
	{
		__this->set_type_8(7);
		bool L_0 = ___val0;
		__this->set_inst_boolean_1(L_0);
		__this->set_json_7((String_t*)NULL);
		return;
	}
}
// System.Void LitJson.JsonData::LitJson.IJsonWrapper.SetDouble(System.Double)
extern "C"  void JsonData_LitJson_IJsonWrapper_SetDouble_m3476916179 (JsonData_t1715015430 * __this, double ___val0, const MethodInfo* method)
{
	{
		__this->set_type_8(6);
		double L_0 = ___val0;
		__this->set_inst_double_2(L_0);
		__this->set_json_7((String_t*)NULL);
		return;
	}
}
// System.Void LitJson.JsonData::LitJson.IJsonWrapper.SetInt(System.Int32)
extern "C"  void JsonData_LitJson_IJsonWrapper_SetInt_m1914934244 (JsonData_t1715015430 * __this, int32_t ___val0, const MethodInfo* method)
{
	{
		__this->set_type_8(4);
		int32_t L_0 = ___val0;
		__this->set_inst_int_3(L_0);
		__this->set_json_7((String_t*)NULL);
		return;
	}
}
// System.Void LitJson.JsonData::LitJson.IJsonWrapper.SetLong(System.Int64)
extern "C"  void JsonData_LitJson_IJsonWrapper_SetLong_m4039470284 (JsonData_t1715015430 * __this, int64_t ___val0, const MethodInfo* method)
{
	{
		__this->set_type_8(5);
		int64_t L_0 = ___val0;
		__this->set_inst_long_4(L_0);
		__this->set_json_7((String_t*)NULL);
		return;
	}
}
// System.Void LitJson.JsonData::LitJson.IJsonWrapper.SetString(System.String)
extern "C"  void JsonData_LitJson_IJsonWrapper_SetString_m2410757971 (JsonData_t1715015430 * __this, String_t* ___val0, const MethodInfo* method)
{
	{
		__this->set_type_8(3);
		String_t* L_0 = ___val0;
		__this->set_inst_string_6(L_0);
		__this->set_json_7((String_t*)NULL);
		return;
	}
}
// System.String LitJson.JsonData::LitJson.IJsonWrapper.ToJson()
extern "C"  String_t* JsonData_LitJson_IJsonWrapper_ToJson_m2808725158 (JsonData_t1715015430 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = JsonData_ToJson_m2780988991(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void LitJson.JsonData::LitJson.IJsonWrapper.ToJson(LitJson.JsonWriter)
extern "C"  void JsonData_LitJson_IJsonWrapper_ToJson_m4202450879 (JsonData_t1715015430 * __this, JsonWriter_t1165300239 * ___writer0, const MethodInfo* method)
{
	{
		JsonWriter_t1165300239 * L_0 = ___writer0;
		JsonData_ToJson_m4047182642(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 LitJson.JsonData::System.Collections.IList.Add(System.Object)
extern "C"  int32_t JsonData_System_Collections_IList_Add_m442784041 (JsonData_t1715015430 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		int32_t L_1 = JsonData_Add_m2309120602(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void LitJson.JsonData::System.Collections.IList.Clear()
extern Il2CppClass* IList_t1751339649_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_System_Collections_IList_Clear_m308166951_MetadataUsageId;
extern "C"  void JsonData_System_Collections_IList_Clear_m308166951 (JsonData_t1715015430 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IList_Clear_m308166951_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureList_m2914899205(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(5 /* System.Void System.Collections.IList::Clear() */, IList_t1751339649_il2cpp_TypeInfo_var, L_0);
		__this->set_json_7((String_t*)NULL);
		return;
	}
}
// System.Boolean LitJson.JsonData::System.Collections.IList.Contains(System.Object)
extern Il2CppClass* IList_t1751339649_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_System_Collections_IList_Contains_m1115183555_MetadataUsageId;
extern "C"  bool JsonData_System_Collections_IList_Contains_m1115183555 (JsonData_t1715015430 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IList_Contains_m1115183555_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureList_m2914899205(__this, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___value0;
		NullCheck(L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(6 /* System.Boolean System.Collections.IList::Contains(System.Object) */, IList_t1751339649_il2cpp_TypeInfo_var, L_0, L_1);
		return L_2;
	}
}
// System.Int32 LitJson.JsonData::System.Collections.IList.IndexOf(System.Object)
extern Il2CppClass* IList_t1751339649_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_System_Collections_IList_IndexOf_m3759264513_MetadataUsageId;
extern "C"  int32_t JsonData_System_Collections_IList_IndexOf_m3759264513 (JsonData_t1715015430 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IList_IndexOf_m3759264513_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureList_m2914899205(__this, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___value0;
		NullCheck(L_0);
		int32_t L_2 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(7 /* System.Int32 System.Collections.IList::IndexOf(System.Object) */, IList_t1751339649_il2cpp_TypeInfo_var, L_0, L_1);
		return L_2;
	}
}
// System.Void LitJson.JsonData::System.Collections.IList.Insert(System.Int32,System.Object)
extern Il2CppClass* IList_t1751339649_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_System_Collections_IList_Insert_m2705345184_MetadataUsageId;
extern "C"  void JsonData_System_Collections_IList_Insert_m2705345184 (JsonData_t1715015430 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IList_Insert_m2705345184_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureList_m2914899205(__this, /*hidden argument*/NULL);
		int32_t L_1 = ___index0;
		Il2CppObject * L_2 = ___value1;
		NullCheck(L_0);
		InterfaceActionInvoker2< int32_t, Il2CppObject * >::Invoke(8 /* System.Void System.Collections.IList::Insert(System.Int32,System.Object) */, IList_t1751339649_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		__this->set_json_7((String_t*)NULL);
		return;
	}
}
// System.Void LitJson.JsonData::System.Collections.IList.Remove(System.Object)
extern Il2CppClass* IList_t1751339649_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_System_Collections_IList_Remove_m1238276744_MetadataUsageId;
extern "C"  void JsonData_System_Collections_IList_Remove_m1238276744 (JsonData_t1715015430 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IList_Remove_m1238276744_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureList_m2914899205(__this, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___value0;
		NullCheck(L_0);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(9 /* System.Void System.Collections.IList::Remove(System.Object) */, IList_t1751339649_il2cpp_TypeInfo_var, L_0, L_1);
		__this->set_json_7((String_t*)NULL);
		return;
	}
}
// System.Void LitJson.JsonData::System.Collections.IList.RemoveAt(System.Int32)
extern Il2CppClass* IList_t1751339649_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_System_Collections_IList_RemoveAt_m1317229104_MetadataUsageId;
extern "C"  void JsonData_System_Collections_IList_RemoveAt_m1317229104 (JsonData_t1715015430 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_IList_RemoveAt_m1317229104_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = JsonData_EnsureList_m2914899205(__this, /*hidden argument*/NULL);
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		InterfaceActionInvoker1< int32_t >::Invoke(10 /* System.Void System.Collections.IList::RemoveAt(System.Int32) */, IList_t1751339649_il2cpp_TypeInfo_var, L_0, L_1);
		__this->set_json_7((String_t*)NULL);
		return;
	}
}
// System.Collections.IDictionaryEnumerator LitJson.JsonData::System.Collections.Specialized.IOrderedDictionary.GetEnumerator()
extern Il2CppClass* IEnumerable_1_t1440160167_il2cpp_TypeInfo_var;
extern Il2CppClass* OrderedDictionaryEnumerator_t3526912157_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_System_Collections_Specialized_IOrderedDictionary_GetEnumerator_m282942264_MetadataUsageId;
extern "C"  Il2CppObject * JsonData_System_Collections_Specialized_IOrderedDictionary_GetEnumerator_m282942264 (JsonData_t1715015430 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonData_System_Collections_Specialized_IOrderedDictionary_GetEnumerator_m282942264_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		JsonData_EnsureDictionary_m4167775685(__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = __this->get_object_list_9();
		NullCheck(L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>::GetEnumerator() */, IEnumerable_1_t1440160167_il2cpp_TypeInfo_var, L_0);
		OrderedDictionaryEnumerator_t3526912157 * L_2 = (OrderedDictionaryEnumerator_t3526912157 *)il2cpp_codegen_object_new(OrderedDictionaryEnumerator_t3526912157_il2cpp_TypeInfo_var);
		OrderedDictionaryEnumerator__ctor_m1595318391(L_2, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Collections.ICollection LitJson.JsonData::EnsureCollection()
extern Il2CppClass* ICollection_t2643922881_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1527009466;
extern const uint32_t JsonData_EnsureCollection_m467105989_MetadataUsageId;
extern "C"  Il2CppObject * JsonData_EnsureCollection_m467105989 (JsonData_t1715015430 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonData_EnsureCollection_m467105989_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = __this->get_type_8();
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject* L_1 = __this->get_inst_array_0();
		return ((Il2CppObject *)Castclass(L_1, ICollection_t2643922881_il2cpp_TypeInfo_var));
	}

IL_0018:
	{
		int32_t L_2 = __this->get_type_8();
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_0030;
		}
	}
	{
		Il2CppObject* L_3 = __this->get_inst_object_5();
		return ((Il2CppObject *)Castclass(L_3, ICollection_t2643922881_il2cpp_TypeInfo_var));
	}

IL_0030:
	{
		InvalidOperationException_t1589641621 * L_4 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_4, _stringLiteral1527009466, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}
}
// System.Collections.IDictionary LitJson.JsonData::EnsureDictionary()
extern Il2CppClass* IDictionary_t537317817_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t2535433800_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t3802400058_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m3448825566_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m88530562_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4278929416;
extern const uint32_t JsonData_EnsureDictionary_m4167775685_MetadataUsageId;
extern "C"  Il2CppObject * JsonData_EnsureDictionary_m4167775685 (JsonData_t1715015430 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonData_EnsureDictionary_m4167775685_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = __this->get_type_8();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject* L_1 = __this->get_inst_object_5();
		return ((Il2CppObject *)Castclass(L_1, IDictionary_t537317817_il2cpp_TypeInfo_var));
	}

IL_0018:
	{
		int32_t L_2 = __this->get_type_8();
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, _stringLiteral4278929416, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002e:
	{
		__this->set_type_8(1);
		Dictionary_2_t2535433800 * L_4 = (Dictionary_2_t2535433800 *)il2cpp_codegen_object_new(Dictionary_2_t2535433800_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3448825566(L_4, /*hidden argument*/Dictionary_2__ctor_m3448825566_MethodInfo_var);
		__this->set_inst_object_5(L_4);
		List_1_t3802400058 * L_5 = (List_1_t3802400058 *)il2cpp_codegen_object_new(List_1_t3802400058_il2cpp_TypeInfo_var);
		List_1__ctor_m88530562(L_5, /*hidden argument*/List_1__ctor_m88530562_MethodInfo_var);
		__this->set_object_list_9(L_5);
		Il2CppObject* L_6 = __this->get_inst_object_5();
		return ((Il2CppObject *)Castclass(L_6, IDictionary_t537317817_il2cpp_TypeInfo_var));
	}
}
// System.Collections.IList LitJson.JsonData::EnsureList()
extern Il2CppClass* IList_t1751339649_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t3083200982_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1930419451_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1038321040;
extern const uint32_t JsonData_EnsureList_m2914899205_MetadataUsageId;
extern "C"  Il2CppObject * JsonData_EnsureList_m2914899205 (JsonData_t1715015430 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonData_EnsureList_m2914899205_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = __this->get_type_8();
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject* L_1 = __this->get_inst_array_0();
		return ((Il2CppObject *)Castclass(L_1, IList_t1751339649_il2cpp_TypeInfo_var));
	}

IL_0018:
	{
		int32_t L_2 = __this->get_type_8();
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, _stringLiteral1038321040, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002e:
	{
		__this->set_type_8(2);
		List_1_t3083200982 * L_4 = (List_1_t3083200982 *)il2cpp_codegen_object_new(List_1_t3083200982_il2cpp_TypeInfo_var);
		List_1__ctor_m1930419451(L_4, /*hidden argument*/List_1__ctor_m1930419451_MethodInfo_var);
		__this->set_inst_array_0(L_4);
		Il2CppObject* L_5 = __this->get_inst_array_0();
		return ((Il2CppObject *)Castclass(L_5, IList_t1751339649_il2cpp_TypeInfo_var));
	}
}
// LitJson.JsonData LitJson.JsonData::ToJsonData(System.Object)
extern Il2CppClass* JsonData_t1715015430_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_ToJsonData_m3725295152_MetadataUsageId;
extern "C"  JsonData_t1715015430 * JsonData_ToJsonData_m3725295152 (JsonData_t1715015430 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonData_ToJsonData_m3725295152_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (JsonData_t1715015430 *)NULL;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___obj0;
		if (!((JsonData_t1715015430 *)IsInstClass(L_1, JsonData_t1715015430_il2cpp_TypeInfo_var)))
		{
			goto IL_001a;
		}
	}
	{
		Il2CppObject * L_2 = ___obj0;
		return ((JsonData_t1715015430 *)CastclassClass(L_2, JsonData_t1715015430_il2cpp_TypeInfo_var));
	}

IL_001a:
	{
		Il2CppObject * L_3 = ___obj0;
		JsonData_t1715015430 * L_4 = (JsonData_t1715015430 *)il2cpp_codegen_object_new(JsonData_t1715015430_il2cpp_TypeInfo_var);
		JsonData__ctor_m2642779625(L_4, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void LitJson.JsonData::WriteJson(LitJson.IJsonWrapper,LitJson.JsonWriter)
extern Il2CppClass* IJsonWrapper_t2026182966_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_t3464557803_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonData_t1715015430_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_t537317817_il2cpp_TypeInfo_var;
extern Il2CppClass* DictionaryEntry_t1751606614_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_WriteJson_m1691040349_MetadataUsageId;
extern "C"  void JsonData_WriteJson_m1691040349 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, JsonWriter_t1165300239 * ___writer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonData_WriteJson_m1691040349_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	DictionaryEntry_t1751606614  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Il2CppObject * V_4 = NULL;
	Il2CppObject * V_5 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		JsonWriter_t1165300239 * L_1 = ___writer1;
		NullCheck(L_1);
		JsonWriter_Write_m1040069059(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}

IL_000e:
	{
		Il2CppObject * L_2 = ___obj0;
		NullCheck(L_2);
		bool L_3 = InterfaceFuncInvoker0< bool >::Invoke(6 /* System.Boolean LitJson.IJsonWrapper::get_IsString() */, IJsonWrapper_t2026182966_il2cpp_TypeInfo_var, L_2);
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		JsonWriter_t1165300239 * L_4 = ___writer1;
		Il2CppObject * L_5 = ___obj0;
		NullCheck(L_5);
		String_t* L_6 = InterfaceFuncInvoker0< String_t* >::Invoke(11 /* System.String LitJson.IJsonWrapper::GetString() */, IJsonWrapper_t2026182966_il2cpp_TypeInfo_var, L_5);
		NullCheck(L_4);
		JsonWriter_Write_m1040069059(L_4, L_6, /*hidden argument*/NULL);
		return;
	}

IL_0026:
	{
		Il2CppObject * L_7 = ___obj0;
		NullCheck(L_7);
		bool L_8 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean LitJson.IJsonWrapper::get_IsBoolean() */, IJsonWrapper_t2026182966_il2cpp_TypeInfo_var, L_7);
		if (!L_8)
		{
			goto IL_003e;
		}
	}
	{
		JsonWriter_t1165300239 * L_9 = ___writer1;
		Il2CppObject * L_10 = ___obj0;
		NullCheck(L_10);
		bool L_11 = InterfaceFuncInvoker0< bool >::Invoke(7 /* System.Boolean LitJson.IJsonWrapper::GetBoolean() */, IJsonWrapper_t2026182966_il2cpp_TypeInfo_var, L_10);
		NullCheck(L_9);
		JsonWriter_Write_m2388425430(L_9, L_11, /*hidden argument*/NULL);
		return;
	}

IL_003e:
	{
		Il2CppObject * L_12 = ___obj0;
		NullCheck(L_12);
		bool L_13 = InterfaceFuncInvoker0< bool >::Invoke(2 /* System.Boolean LitJson.IJsonWrapper::get_IsDouble() */, IJsonWrapper_t2026182966_il2cpp_TypeInfo_var, L_12);
		if (!L_13)
		{
			goto IL_0056;
		}
	}
	{
		JsonWriter_t1165300239 * L_14 = ___writer1;
		Il2CppObject * L_15 = ___obj0;
		NullCheck(L_15);
		double L_16 = InterfaceFuncInvoker0< double >::Invoke(8 /* System.Double LitJson.IJsonWrapper::GetDouble() */, IJsonWrapper_t2026182966_il2cpp_TypeInfo_var, L_15);
		NullCheck(L_14);
		JsonWriter_Write_m471830019(L_14, L_16, /*hidden argument*/NULL);
		return;
	}

IL_0056:
	{
		Il2CppObject * L_17 = ___obj0;
		NullCheck(L_17);
		bool L_18 = InterfaceFuncInvoker0< bool >::Invoke(3 /* System.Boolean LitJson.IJsonWrapper::get_IsInt() */, IJsonWrapper_t2026182966_il2cpp_TypeInfo_var, L_17);
		if (!L_18)
		{
			goto IL_006e;
		}
	}
	{
		JsonWriter_t1165300239 * L_19 = ___writer1;
		Il2CppObject * L_20 = ___obj0;
		NullCheck(L_20);
		int32_t L_21 = InterfaceFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 LitJson.IJsonWrapper::GetInt() */, IJsonWrapper_t2026182966_il2cpp_TypeInfo_var, L_20);
		NullCheck(L_19);
		JsonWriter_Write_m295913072(L_19, L_21, /*hidden argument*/NULL);
		return;
	}

IL_006e:
	{
		Il2CppObject * L_22 = ___obj0;
		NullCheck(L_22);
		bool L_23 = InterfaceFuncInvoker0< bool >::Invoke(4 /* System.Boolean LitJson.IJsonWrapper::get_IsLong() */, IJsonWrapper_t2026182966_il2cpp_TypeInfo_var, L_22);
		if (!L_23)
		{
			goto IL_0086;
		}
	}
	{
		JsonWriter_t1165300239 * L_24 = ___writer1;
		Il2CppObject * L_25 = ___obj0;
		NullCheck(L_25);
		int64_t L_26 = InterfaceFuncInvoker0< int64_t >::Invoke(10 /* System.Int64 LitJson.IJsonWrapper::GetLong() */, IJsonWrapper_t2026182966_il2cpp_TypeInfo_var, L_25);
		NullCheck(L_24);
		JsonWriter_Write_m295916017(L_24, L_26, /*hidden argument*/NULL);
		return;
	}

IL_0086:
	{
		Il2CppObject * L_27 = ___obj0;
		NullCheck(L_27);
		bool L_28 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean LitJson.IJsonWrapper::get_IsArray() */, IJsonWrapper_t2026182966_il2cpp_TypeInfo_var, L_27);
		if (!L_28)
		{
			goto IL_00e1;
		}
	}
	{
		JsonWriter_t1165300239 * L_29 = ___writer1;
		NullCheck(L_29);
		JsonWriter_WriteArrayStart_m721019272(L_29, /*hidden argument*/NULL);
		Il2CppObject * L_30 = ___obj0;
		NullCheck(L_30);
		Il2CppObject * L_31 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t3464557803_il2cpp_TypeInfo_var, L_30);
		V_1 = L_31;
	}

IL_009e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00b6;
		}

IL_00a3:
		{
			Il2CppObject * L_32 = V_1;
			NullCheck(L_32);
			Il2CppObject * L_33 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_32);
			V_0 = L_33;
			Il2CppObject * L_34 = V_0;
			JsonWriter_t1165300239 * L_35 = ___writer1;
			JsonData_WriteJson_m1691040349(NULL /*static, unused*/, ((JsonData_t1715015430 *)CastclassClass(L_34, JsonData_t1715015430_il2cpp_TypeInfo_var)), L_35, /*hidden argument*/NULL);
		}

IL_00b6:
		{
			Il2CppObject * L_36 = V_1;
			NullCheck(L_36);
			bool L_37 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_36);
			if (L_37)
			{
				goto IL_00a3;
			}
		}

IL_00c1:
		{
			IL2CPP_LEAVE(0xDA, FINALLY_00c6);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_00c6;
	}

FINALLY_00c6:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_38 = V_1;
			Il2CppObject * L_39 = ((Il2CppObject *)IsInst(L_38, IDisposable_t1423340799_il2cpp_TypeInfo_var));
			V_2 = L_39;
			if (!L_39)
			{
				goto IL_00d9;
			}
		}

IL_00d3:
		{
			Il2CppObject * L_40 = V_2;
			NullCheck(L_40);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_40);
		}

IL_00d9:
		{
			IL2CPP_END_FINALLY(198)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(198)
	{
		IL2CPP_JUMP_TBL(0xDA, IL_00da)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_00da:
	{
		JsonWriter_t1165300239 * L_41 = ___writer1;
		NullCheck(L_41);
		JsonWriter_WriteArrayEnd_m3594342657(L_41, /*hidden argument*/NULL);
		return;
	}

IL_00e1:
	{
		Il2CppObject * L_42 = ___obj0;
		NullCheck(L_42);
		bool L_43 = InterfaceFuncInvoker0< bool >::Invoke(5 /* System.Boolean LitJson.IJsonWrapper::get_IsObject() */, IJsonWrapper_t2026182966_il2cpp_TypeInfo_var, L_42);
		if (!L_43)
		{
			goto IL_015f;
		}
	}
	{
		JsonWriter_t1165300239 * L_44 = ___writer1;
		NullCheck(L_44);
		JsonWriter_WriteObjectStart_m3796293414(L_44, /*hidden argument*/NULL);
		Il2CppObject * L_45 = ___obj0;
		NullCheck(L_45);
		Il2CppObject * L_46 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(3 /* System.Collections.IDictionaryEnumerator System.Collections.IDictionary::GetEnumerator() */, IDictionary_t537317817_il2cpp_TypeInfo_var, L_45);
		V_4 = L_46;
	}

IL_00fa:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0130;
		}

IL_00ff:
		{
			Il2CppObject * L_47 = V_4;
			NullCheck(L_47);
			Il2CppObject * L_48 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_47);
			V_3 = ((*(DictionaryEntry_t1751606614 *)((DictionaryEntry_t1751606614 *)UnBox (L_48, DictionaryEntry_t1751606614_il2cpp_TypeInfo_var))));
			JsonWriter_t1165300239 * L_49 = ___writer1;
			Il2CppObject * L_50 = DictionaryEntry_get_Key_m3516209325((&V_3), /*hidden argument*/NULL);
			NullCheck(L_49);
			JsonWriter_WritePropertyName_m1552473251(L_49, ((String_t*)CastclassSealed(L_50, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
			Il2CppObject * L_51 = DictionaryEntry_get_Value_m4281303039((&V_3), /*hidden argument*/NULL);
			JsonWriter_t1165300239 * L_52 = ___writer1;
			JsonData_WriteJson_m1691040349(NULL /*static, unused*/, ((JsonData_t1715015430 *)CastclassClass(L_51, JsonData_t1715015430_il2cpp_TypeInfo_var)), L_52, /*hidden argument*/NULL);
		}

IL_0130:
		{
			Il2CppObject * L_53 = V_4;
			NullCheck(L_53);
			bool L_54 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_53);
			if (L_54)
			{
				goto IL_00ff;
			}
		}

IL_013c:
		{
			IL2CPP_LEAVE(0x158, FINALLY_0141);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0141;
	}

FINALLY_0141:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_55 = V_4;
			Il2CppObject * L_56 = ((Il2CppObject *)IsInst(L_55, IDisposable_t1423340799_il2cpp_TypeInfo_var));
			V_5 = L_56;
			if (!L_56)
			{
				goto IL_0157;
			}
		}

IL_0150:
		{
			Il2CppObject * L_57 = V_5;
			NullCheck(L_57);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_57);
		}

IL_0157:
		{
			IL2CPP_END_FINALLY(321)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(321)
	{
		IL2CPP_JUMP_TBL(0x158, IL_0158)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0158:
	{
		JsonWriter_t1165300239 * L_58 = ___writer1;
		NullCheck(L_58);
		JsonWriter_WriteObjectEnd_m2431063583(L_58, /*hidden argument*/NULL);
		return;
	}

IL_015f:
	{
		return;
	}
}
// System.Int32 LitJson.JsonData::Add(System.Object)
extern Il2CppClass* IList_t1751339649_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_Add_m2309120602_MetadataUsageId;
extern "C"  int32_t JsonData_Add_m2309120602 (JsonData_t1715015430 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonData_Add_m2309120602_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JsonData_t1715015430 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___value0;
		JsonData_t1715015430 * L_1 = JsonData_ToJsonData_m3725295152(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		__this->set_json_7((String_t*)NULL);
		Il2CppObject * L_2 = JsonData_EnsureList_m2914899205(__this, /*hidden argument*/NULL);
		JsonData_t1715015430 * L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(4 /* System.Int32 System.Collections.IList::Add(System.Object) */, IList_t1751339649_il2cpp_TypeInfo_var, L_2, L_3);
		return L_4;
	}
}
// System.Boolean LitJson.JsonData::Equals(LitJson.JsonData)
extern "C"  bool JsonData_Equals_m1775900429 (JsonData_t1715015430 * __this, JsonData_t1715015430 * ___x0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		JsonData_t1715015430 * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (bool)0;
	}

IL_0008:
	{
		JsonData_t1715015430 * L_1 = ___x0;
		NullCheck(L_1);
		int32_t L_2 = L_1->get_type_8();
		int32_t L_3 = __this->get_type_8();
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_001b;
		}
	}
	{
		return (bool)0;
	}

IL_001b:
	{
		int32_t L_4 = __this->get_type_8();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if (L_5 == 0)
		{
			goto IL_004d;
		}
		if (L_5 == 1)
		{
			goto IL_004f;
		}
		if (L_5 == 2)
		{
			goto IL_0061;
		}
		if (L_5 == 3)
		{
			goto IL_0073;
		}
		if (L_5 == 4)
		{
			goto IL_0085;
		}
		if (L_5 == 5)
		{
			goto IL_0097;
		}
		if (L_5 == 6)
		{
			goto IL_00a9;
		}
		if (L_5 == 7)
		{
			goto IL_00bb;
		}
	}
	{
		goto IL_00cd;
	}

IL_004d:
	{
		return (bool)1;
	}

IL_004f:
	{
		Il2CppObject* L_6 = __this->get_inst_object_5();
		JsonData_t1715015430 * L_7 = ___x0;
		NullCheck(L_7);
		Il2CppObject* L_8 = L_7->get_inst_object_5();
		NullCheck(L_6);
		bool L_9 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_6, L_8);
		return L_9;
	}

IL_0061:
	{
		Il2CppObject* L_10 = __this->get_inst_array_0();
		JsonData_t1715015430 * L_11 = ___x0;
		NullCheck(L_11);
		Il2CppObject* L_12 = L_11->get_inst_array_0();
		NullCheck(L_10);
		bool L_13 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_10, L_12);
		return L_13;
	}

IL_0073:
	{
		String_t* L_14 = __this->get_inst_string_6();
		JsonData_t1715015430 * L_15 = ___x0;
		NullCheck(L_15);
		String_t* L_16 = L_15->get_inst_string_6();
		NullCheck(L_14);
		bool L_17 = String_Equals_m3541721061(L_14, L_16, /*hidden argument*/NULL);
		return L_17;
	}

IL_0085:
	{
		int32_t* L_18 = __this->get_address_of_inst_int_3();
		JsonData_t1715015430 * L_19 = ___x0;
		NullCheck(L_19);
		int32_t L_20 = L_19->get_inst_int_3();
		bool L_21 = Int32_Equals_m3849884467(L_18, L_20, /*hidden argument*/NULL);
		return L_21;
	}

IL_0097:
	{
		int64_t* L_22 = __this->get_address_of_inst_long_4();
		JsonData_t1715015430 * L_23 = ___x0;
		NullCheck(L_23);
		int64_t L_24 = L_23->get_inst_long_4();
		bool L_25 = Int64_Equals_m1843428819(L_22, L_24, /*hidden argument*/NULL);
		return L_25;
	}

IL_00a9:
	{
		double* L_26 = __this->get_address_of_inst_double_2();
		JsonData_t1715015430 * L_27 = ___x0;
		NullCheck(L_27);
		double L_28 = L_27->get_inst_double_2();
		bool L_29 = Double_Equals_m806774885(L_26, L_28, /*hidden argument*/NULL);
		return L_29;
	}

IL_00bb:
	{
		bool* L_30 = __this->get_address_of_inst_boolean_1();
		JsonData_t1715015430 * L_31 = ___x0;
		NullCheck(L_31);
		bool L_32 = L_31->get_inst_boolean_1();
		bool L_33 = Boolean_Equals_m4087985523(L_30, L_32, /*hidden argument*/NULL);
		return L_33;
	}

IL_00cd:
	{
		return (bool)0;
	}
}
// System.Void LitJson.JsonData::SetJsonType(LitJson.JsonType)
extern Il2CppClass* Dictionary_2_t2535433800_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t3802400058_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t3083200982_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m3448825566_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m88530562_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1930419451_MethodInfo_var;
extern const uint32_t JsonData_SetJsonType_m2477325084_MetadataUsageId;
extern "C"  void JsonData_SetJsonType_m2477325084 (JsonData_t1715015430 * __this, int32_t ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonData_SetJsonType_m2477325084_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = __this->get_type_8();
		int32_t L_1 = ___type0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		int32_t L_2 = ___type0;
		if (L_2 == 0)
		{
			goto IL_0038;
		}
		if (L_2 == 1)
		{
			goto IL_003d;
		}
		if (L_2 == 2)
		{
			goto IL_0058;
		}
		if (L_2 == 3)
		{
			goto IL_0068;
		}
		if (L_2 == 4)
		{
			goto IL_0074;
		}
		if (L_2 == 5)
		{
			goto IL_0080;
		}
		if (L_2 == 6)
		{
			goto IL_008d;
		}
		if (L_2 == 7)
		{
			goto IL_00a1;
		}
	}
	{
		goto IL_00ad;
	}

IL_0038:
	{
		goto IL_00ad;
	}

IL_003d:
	{
		Dictionary_2_t2535433800 * L_3 = (Dictionary_2_t2535433800 *)il2cpp_codegen_object_new(Dictionary_2_t2535433800_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3448825566(L_3, /*hidden argument*/Dictionary_2__ctor_m3448825566_MethodInfo_var);
		__this->set_inst_object_5(L_3);
		List_1_t3802400058 * L_4 = (List_1_t3802400058 *)il2cpp_codegen_object_new(List_1_t3802400058_il2cpp_TypeInfo_var);
		List_1__ctor_m88530562(L_4, /*hidden argument*/List_1__ctor_m88530562_MethodInfo_var);
		__this->set_object_list_9(L_4);
		goto IL_00ad;
	}

IL_0058:
	{
		List_1_t3083200982 * L_5 = (List_1_t3083200982 *)il2cpp_codegen_object_new(List_1_t3083200982_il2cpp_TypeInfo_var);
		List_1__ctor_m1930419451(L_5, /*hidden argument*/List_1__ctor_m1930419451_MethodInfo_var);
		__this->set_inst_array_0(L_5);
		goto IL_00ad;
	}

IL_0068:
	{
		__this->set_inst_string_6((String_t*)NULL);
		goto IL_00ad;
	}

IL_0074:
	{
		__this->set_inst_int_3(0);
		goto IL_00ad;
	}

IL_0080:
	{
		__this->set_inst_long_4((((int64_t)((int64_t)0))));
		goto IL_00ad;
	}

IL_008d:
	{
		__this->set_inst_double_2((0.0));
		goto IL_00ad;
	}

IL_00a1:
	{
		__this->set_inst_boolean_1((bool)0);
		goto IL_00ad;
	}

IL_00ad:
	{
		int32_t L_6 = ___type0;
		__this->set_type_8(L_6);
		return;
	}
}
// System.String LitJson.JsonData::ToJson()
extern Il2CppClass* StringWriter_t4216882900_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonWriter_t1165300239_il2cpp_TypeInfo_var;
extern const uint32_t JsonData_ToJson_m2780988991_MetadataUsageId;
extern "C"  String_t* JsonData_ToJson_m2780988991 (JsonData_t1715015430 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonData_ToJson_m2780988991_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringWriter_t4216882900 * V_0 = NULL;
	JsonWriter_t1165300239 * V_1 = NULL;
	{
		String_t* L_0 = __this->get_json_7();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_1 = __this->get_json_7();
		return L_1;
	}

IL_0012:
	{
		StringWriter_t4216882900 * L_2 = (StringWriter_t4216882900 *)il2cpp_codegen_object_new(StringWriter_t4216882900_il2cpp_TypeInfo_var);
		StringWriter__ctor_m3428223077(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		StringWriter_t4216882900 * L_3 = V_0;
		JsonWriter_t1165300239 * L_4 = (JsonWriter_t1165300239 *)il2cpp_codegen_object_new(JsonWriter_t1165300239_il2cpp_TypeInfo_var);
		JsonWriter__ctor_m89997433(L_4, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		JsonWriter_t1165300239 * L_5 = V_1;
		NullCheck(L_5);
		JsonWriter_set_Validate_m2559375852(L_5, (bool)0, /*hidden argument*/NULL);
		JsonWriter_t1165300239 * L_6 = V_1;
		JsonData_WriteJson_m1691040349(NULL /*static, unused*/, __this, L_6, /*hidden argument*/NULL);
		StringWriter_t4216882900 * L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_7);
		__this->set_json_7(L_8);
		String_t* L_9 = __this->get_json_7();
		return L_9;
	}
}
// System.Void LitJson.JsonData::ToJson(LitJson.JsonWriter)
extern "C"  void JsonData_ToJson_m4047182642 (JsonData_t1715015430 * __this, JsonWriter_t1165300239 * ___writer0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		JsonWriter_t1165300239 * L_0 = ___writer0;
		NullCheck(L_0);
		bool L_1 = JsonWriter_get_Validate_m637559351(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		JsonWriter_t1165300239 * L_2 = ___writer0;
		NullCheck(L_2);
		JsonWriter_set_Validate_m2559375852(L_2, (bool)0, /*hidden argument*/NULL);
		JsonWriter_t1165300239 * L_3 = ___writer0;
		JsonData_WriteJson_m1691040349(NULL /*static, unused*/, __this, L_3, /*hidden argument*/NULL);
		JsonWriter_t1165300239 * L_4 = ___writer0;
		bool L_5 = V_0;
		NullCheck(L_4);
		JsonWriter_set_Validate_m2559375852(L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.String LitJson.JsonData::ToString()
extern Il2CppCodeGenString* _stringLiteral706748811;
extern Il2CppCodeGenString* _stringLiteral820173389;
extern Il2CppCodeGenString* _stringLiteral1007230167;
extern const uint32_t JsonData_ToString_m537105832_MetadataUsageId;
extern "C"  String_t* JsonData_ToString_m537105832 (JsonData_t1715015430 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonData_ToString_m537105832_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_type_8();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 0)
		{
			goto IL_007e;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 1)
		{
			goto IL_0030;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 2)
		{
			goto IL_0084;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 3)
		{
			goto IL_005a;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 4)
		{
			goto IL_006c;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 5)
		{
			goto IL_0048;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 6)
		{
			goto IL_0036;
		}
	}
	{
		goto IL_008b;
	}

IL_0030:
	{
		return _stringLiteral706748811;
	}

IL_0036:
	{
		bool* L_2 = __this->get_address_of_inst_boolean_1();
		String_t* L_3 = Boolean_ToString_m2512358154(L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0048:
	{
		double* L_4 = __this->get_address_of_inst_double_2();
		String_t* L_5 = Double_ToString_m3380246633(L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_005a:
	{
		int32_t* L_6 = __this->get_address_of_inst_int_3();
		String_t* L_7 = Int32_ToString_m1286526384(L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_006c:
	{
		int64_t* L_8 = __this->get_address_of_inst_long_4();
		String_t* L_9 = Int64_ToString_m3478011791(L_8, /*hidden argument*/NULL);
		return L_9;
	}

IL_007e:
	{
		return _stringLiteral820173389;
	}

IL_0084:
	{
		String_t* L_10 = __this->get_inst_string_6();
		return L_10;
	}

IL_008b:
	{
		return _stringLiteral1007230167;
	}
}
// System.Void LitJson.JsonException::.ctor(LitJson.ParserToken,System.Exception)
extern Il2CppClass* ParserToken_t608116400_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3817532472;
extern const uint32_t JsonException__ctor_m1122554673_MetadataUsageId;
extern "C"  void JsonException__ctor_m1122554673 (JsonException_t3617621405 * __this, int32_t ___token0, Exception_t3991598821 * ___inner_exception1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonException__ctor_m1122554673_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___token0;
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(ParserToken_t608116400_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral3817532472, L_2, /*hidden argument*/NULL);
		Exception_t3991598821 * L_4 = ___inner_exception1;
		ApplicationException__ctor_m1208591594(__this, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonException::.ctor(System.Int32)
extern Il2CppClass* Char_t2862622538_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4087772648;
extern const uint32_t JsonException__ctor_m3681649971_MetadataUsageId;
extern "C"  void JsonException__ctor_m3681649971 (JsonException_t3617621405 * __this, int32_t ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonException__ctor_m3681649971_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___c0;
		Il2CppChar L_1 = ((Il2CppChar)(((int32_t)((uint16_t)L_0))));
		Il2CppObject * L_2 = Box(Char_t2862622538_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral4087772648, L_2, /*hidden argument*/NULL);
		ApplicationException__ctor_m1727689164(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonException::.ctor(System.String)
extern "C"  void JsonException__ctor_m2918697824 (JsonException_t3617621405 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		ApplicationException__ctor_m1727689164(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonMapper::.cctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonMapper_t863513565_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t4163765395_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t107454380_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t2114716983_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t2571737008_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonWriter_t1165300239_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTimeFormatInfo_t2490955586_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t3435782958_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t1927038133_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1147775567_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1954564562_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m3763033777_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2393057868_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m4274551720_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m89709152_MethodInfo_var;
extern const uint32_t JsonMapper__cctor_m4040458713_MetadataUsageId;
extern "C"  void JsonMapper__cctor_m4040458713 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper__cctor_m4040458713_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->set_array_metadata_lock_7(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_1, /*hidden argument*/NULL);
		((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->set_conv_ops_lock_9(L_1);
		Il2CppObject * L_2 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_2, /*hidden argument*/NULL);
		((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->set_object_metadata_lock_11(L_2);
		Il2CppObject * L_3 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_3, /*hidden argument*/NULL);
		((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->set_type_properties_lock_13(L_3);
		Il2CppObject * L_4 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_4, /*hidden argument*/NULL);
		((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->set_static_writer_lock_15(L_4);
		((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->set_max_nesting_depth_0(((int32_t)100));
		Dictionary_2_t4163765395 * L_5 = (Dictionary_2_t4163765395 *)il2cpp_codegen_object_new(Dictionary_2_t4163765395_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1147775567(L_5, /*hidden argument*/Dictionary_2__ctor_m1147775567_MethodInfo_var);
		((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->set_array_metadata_6(L_5);
		Dictionary_2_t107454380 * L_6 = (Dictionary_2_t107454380 *)il2cpp_codegen_object_new(Dictionary_2_t107454380_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1954564562(L_6, /*hidden argument*/Dictionary_2__ctor_m1954564562_MethodInfo_var);
		((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->set_conv_ops_8(L_6);
		Dictionary_2_t2114716983 * L_7 = (Dictionary_2_t2114716983 *)il2cpp_codegen_object_new(Dictionary_2_t2114716983_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3763033777(L_7, /*hidden argument*/Dictionary_2__ctor_m3763033777_MethodInfo_var);
		((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->set_object_metadata_10(L_7);
		Dictionary_2_t2571737008 * L_8 = (Dictionary_2_t2571737008 *)il2cpp_codegen_object_new(Dictionary_2_t2571737008_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2393057868(L_8, /*hidden argument*/Dictionary_2__ctor_m2393057868_MethodInfo_var);
		((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->set_type_properties_12(L_8);
		JsonWriter_t1165300239 * L_9 = (JsonWriter_t1165300239 *)il2cpp_codegen_object_new(JsonWriter_t1165300239_il2cpp_TypeInfo_var);
		JsonWriter__ctor_m52938338(L_9, /*hidden argument*/NULL);
		((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->set_static_writer_14(L_9);
		IL2CPP_RUNTIME_CLASS_INIT(DateTimeFormatInfo_t2490955586_il2cpp_TypeInfo_var);
		DateTimeFormatInfo_t2490955586 * L_10 = DateTimeFormatInfo_get_InvariantInfo_m1430381298(NULL /*static, unused*/, /*hidden argument*/NULL);
		((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->set_datetime_format_1(L_10);
		Dictionary_2_t3435782958 * L_11 = (Dictionary_2_t3435782958 *)il2cpp_codegen_object_new(Dictionary_2_t3435782958_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m4274551720(L_11, /*hidden argument*/Dictionary_2__ctor_m4274551720_MethodInfo_var);
		((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->set_base_exporters_table_2(L_11);
		Dictionary_2_t3435782958 * L_12 = (Dictionary_2_t3435782958 *)il2cpp_codegen_object_new(Dictionary_2_t3435782958_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m4274551720(L_12, /*hidden argument*/Dictionary_2__ctor_m4274551720_MethodInfo_var);
		((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->set_custom_exporters_table_3(L_12);
		Dictionary_2_t1927038133 * L_13 = (Dictionary_2_t1927038133 *)il2cpp_codegen_object_new(Dictionary_2_t1927038133_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m89709152(L_13, /*hidden argument*/Dictionary_2__ctor_m89709152_MethodInfo_var);
		((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->set_base_importers_table_4(L_13);
		Dictionary_2_t1927038133 * L_14 = (Dictionary_2_t1927038133 *)il2cpp_codegen_object_new(Dictionary_2_t1927038133_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m89709152(L_14, /*hidden argument*/Dictionary_2__ctor_m89709152_MethodInfo_var);
		((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->set_custom_importers_table_5(L_14);
		JsonMapper_RegisterBaseExporters_m1541476208(NULL /*static, unused*/, /*hidden argument*/NULL);
		JsonMapper_RegisterBaseImporters_m810286623(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonMapper::AddArrayMetadata(System.Type)
extern const Il2CppType* Int32_t1153838500_0_0_0_var;
extern Il2CppClass* JsonMapper_t863513565_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t3741638740_il2cpp_TypeInfo_var;
extern Il2CppClass* ArrayMetadata_t4058342910_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral208507119;
extern Il2CppCodeGenString* _stringLiteral2289459;
extern const uint32_t JsonMapper_AddArrayMetadata_m50814818_MetadataUsageId;
extern "C"  void JsonMapper_AddArrayMetadata_m50814818 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_AddArrayMetadata_m50814818_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ArrayMetadata_t4058342910  V_0;
	memset(&V_0, 0, sizeof(V_0));
	PropertyInfo_t * V_1 = NULL;
	PropertyInfoU5BU5D_t4286713048* V_2 = NULL;
	int32_t V_3 = 0;
	ParameterInfoU5BU5D_t2015293532* V_4 = NULL;
	Il2CppObject * V_5 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_array_metadata_6();
		Type_t * L_1 = ___type0;
		NullCheck(L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, Type_t * >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Type,LitJson.ArrayMetadata>::ContainsKey(!0) */, IDictionary_2_t3741638740_il2cpp_TypeInfo_var, L_0, L_1);
		if (!L_2)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		Initobj (ArrayMetadata_t4058342910_il2cpp_TypeInfo_var, (&V_0));
		Type_t * L_3 = ___type0;
		NullCheck(L_3);
		bool L_4 = Type_get_IsArray_m837983873(L_3, /*hidden argument*/NULL);
		ArrayMetadata_set_IsArray_m3904919016((&V_0), L_4, /*hidden argument*/NULL);
		Type_t * L_5 = ___type0;
		NullCheck(L_5);
		Type_t * L_6 = Type_GetInterface_m1133348080(L_5, _stringLiteral208507119, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003e;
		}
	}
	{
		ArrayMetadata_set_IsList_m962711967((&V_0), (bool)1, /*hidden argument*/NULL);
	}

IL_003e:
	{
		Type_t * L_7 = ___type0;
		NullCheck(L_7);
		PropertyInfoU5BU5D_t4286713048* L_8 = Type_GetProperties_m3853308260(L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		V_3 = 0;
		goto IL_00aa;
	}

IL_004c:
	{
		PropertyInfoU5BU5D_t4286713048* L_9 = V_2;
		int32_t L_10 = V_3;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		PropertyInfo_t * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		V_1 = L_12;
		PropertyInfo_t * L_13 = V_1;
		NullCheck(L_13);
		String_t* L_14 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_13);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_15 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_14, _stringLiteral2289459, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_006a;
		}
	}
	{
		goto IL_00a6;
	}

IL_006a:
	{
		PropertyInfo_t * L_16 = V_1;
		NullCheck(L_16);
		ParameterInfoU5BU5D_t2015293532* L_17 = VirtFuncInvoker0< ParameterInfoU5BU5D_t2015293532* >::Invoke(20 /* System.Reflection.ParameterInfo[] System.Reflection.PropertyInfo::GetIndexParameters() */, L_16);
		V_4 = L_17;
		ParameterInfoU5BU5D_t2015293532* L_18 = V_4;
		NullCheck(L_18);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_18)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0081;
		}
	}
	{
		goto IL_00a6;
	}

IL_0081:
	{
		ParameterInfoU5BU5D_t2015293532* L_19 = V_4;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 0);
		int32_t L_20 = 0;
		ParameterInfo_t2235474049 * L_21 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		NullCheck(L_21);
		Type_t * L_22 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, L_21);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_23 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int32_t1153838500_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_22) == ((Il2CppObject*)(Type_t *)L_23))))
		{
			goto IL_00a6;
		}
	}
	{
		PropertyInfo_t * L_24 = V_1;
		NullCheck(L_24);
		Type_t * L_25 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Reflection.PropertyInfo::get_PropertyType() */, L_24);
		ArrayMetadata_set_ElementType_m478552961((&V_0), L_25, /*hidden argument*/NULL);
	}

IL_00a6:
	{
		int32_t L_26 = V_3;
		V_3 = ((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_00aa:
	{
		int32_t L_27 = V_3;
		PropertyInfoU5BU5D_t4286713048* L_28 = V_2;
		NullCheck(L_28);
		if ((((int32_t)L_27) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_28)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		Il2CppObject * L_29 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_array_metadata_lock_7();
		V_5 = L_29;
		Il2CppObject * L_30 = V_5;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
	}

IL_00c1:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
			Il2CppObject* L_31 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_array_metadata_6();
			Type_t * L_32 = ___type0;
			ArrayMetadata_t4058342910  L_33 = V_0;
			NullCheck(L_31);
			InterfaceActionInvoker2< Type_t *, ArrayMetadata_t4058342910  >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,LitJson.ArrayMetadata>::Add(!0,!1) */, IDictionary_2_t3741638740_il2cpp_TypeInfo_var, L_31, L_32, L_33);
			goto IL_00d8;
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t3991598821 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (ArgumentException_t928607144_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_00d2;
			throw e;
		}

CATCH_00d2:
		{ // begin catch(System.ArgumentException)
			IL2CPP_LEAVE(0xE5, FINALLY_00dd);
		} // end catch (depth: 2)

IL_00d8:
		{
			IL2CPP_LEAVE(0xE5, FINALLY_00dd);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_00dd;
	}

FINALLY_00dd:
	{ // begin finally (depth: 1)
		Il2CppObject * L_34 = V_5;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(221)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(221)
	{
		IL2CPP_JUMP_TBL(0xE5, IL_00e5)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_00e5:
	{
		return;
	}
}
// System.Void LitJson.JsonMapper::AddObjectMetadata(System.Type)
extern const Il2CppType* String_t_0_0_0_var;
extern Il2CppClass* JsonMapper_t863513565_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t1692590328_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectMetadata_t2009294498_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t592085690_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* PropertyMetadata_t4066634616_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t169959035_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m4065633488_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3289452583;
extern Il2CppCodeGenString* _stringLiteral2289459;
extern const uint32_t JsonMapper_AddObjectMetadata_m902222360_MetadataUsageId;
extern "C"  void JsonMapper_AddObjectMetadata_m902222360 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_AddObjectMetadata_m902222360_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ObjectMetadata_t2009294498  V_0;
	memset(&V_0, 0, sizeof(V_0));
	PropertyInfo_t * V_1 = NULL;
	PropertyInfoU5BU5D_t4286713048* V_2 = NULL;
	int32_t V_3 = 0;
	ParameterInfoU5BU5D_t2015293532* V_4 = NULL;
	PropertyMetadata_t4066634616  V_5;
	memset(&V_5, 0, sizeof(V_5));
	FieldInfo_t * V_6 = NULL;
	FieldInfoU5BU5D_t2567562023* V_7 = NULL;
	int32_t V_8 = 0;
	PropertyMetadata_t4066634616  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Il2CppObject * V_10 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_object_metadata_10();
		Type_t * L_1 = ___type0;
		NullCheck(L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, Type_t * >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Type,LitJson.ObjectMetadata>::ContainsKey(!0) */, IDictionary_2_t1692590328_il2cpp_TypeInfo_var, L_0, L_1);
		if (!L_2)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		Initobj (ObjectMetadata_t2009294498_il2cpp_TypeInfo_var, (&V_0));
		Type_t * L_3 = ___type0;
		NullCheck(L_3);
		Type_t * L_4 = Type_GetInterface_m1133348080(L_3, _stringLiteral3289452583, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0031;
		}
	}
	{
		ObjectMetadata_set_IsDictionary_m1761380233((&V_0), (bool)1, /*hidden argument*/NULL);
	}

IL_0031:
	{
		Dictionary_2_t592085690 * L_5 = (Dictionary_2_t592085690 *)il2cpp_codegen_object_new(Dictionary_2_t592085690_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m4065633488(L_5, /*hidden argument*/Dictionary_2__ctor_m4065633488_MethodInfo_var);
		ObjectMetadata_set_Properties_m2278608048((&V_0), L_5, /*hidden argument*/NULL);
		Type_t * L_6 = ___type0;
		NullCheck(L_6);
		PropertyInfoU5BU5D_t4286713048* L_7 = Type_GetProperties_m3853308260(L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		V_3 = 0;
		goto IL_00da;
	}

IL_004b:
	{
		PropertyInfoU5BU5D_t4286713048* L_8 = V_2;
		int32_t L_9 = V_3;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int32_t L_10 = L_9;
		PropertyInfo_t * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		V_1 = L_11;
		PropertyInfo_t * L_12 = V_1;
		NullCheck(L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_12);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_14 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_13, _stringLiteral2289459, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00a5;
		}
	}
	{
		PropertyInfo_t * L_15 = V_1;
		NullCheck(L_15);
		ParameterInfoU5BU5D_t2015293532* L_16 = VirtFuncInvoker0< ParameterInfoU5BU5D_t2015293532* >::Invoke(20 /* System.Reflection.ParameterInfo[] System.Reflection.PropertyInfo::GetIndexParameters() */, L_15);
		V_4 = L_16;
		ParameterInfoU5BU5D_t2015293532* L_17 = V_4;
		NullCheck(L_17);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_17)->max_length))))) == ((int32_t)1)))
		{
			goto IL_007b;
		}
	}
	{
		goto IL_00d6;
	}

IL_007b:
	{
		ParameterInfoU5BU5D_t2015293532* L_18 = V_4;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 0);
		int32_t L_19 = 0;
		ParameterInfo_t2235474049 * L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		NullCheck(L_20);
		Type_t * L_21 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, L_20);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_22 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_21) == ((Il2CppObject*)(Type_t *)L_22))))
		{
			goto IL_00a0;
		}
	}
	{
		PropertyInfo_t * L_23 = V_1;
		NullCheck(L_23);
		Type_t * L_24 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Reflection.PropertyInfo::get_PropertyType() */, L_23);
		ObjectMetadata_set_ElementType_m3476049203((&V_0), L_24, /*hidden argument*/NULL);
	}

IL_00a0:
	{
		goto IL_00d6;
	}

IL_00a5:
	{
		Initobj (PropertyMetadata_t4066634616_il2cpp_TypeInfo_var, (&V_5));
		PropertyInfo_t * L_25 = V_1;
		(&V_5)->set_Info_0(L_25);
		PropertyInfo_t * L_26 = V_1;
		NullCheck(L_26);
		Type_t * L_27 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Reflection.PropertyInfo::get_PropertyType() */, L_26);
		(&V_5)->set_Type_2(L_27);
		Il2CppObject* L_28 = ObjectMetadata_get_Properties_m1175853819((&V_0), /*hidden argument*/NULL);
		PropertyInfo_t * L_29 = V_1;
		NullCheck(L_29);
		String_t* L_30 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_29);
		PropertyMetadata_t4066634616  L_31 = V_5;
		NullCheck(L_28);
		InterfaceActionInvoker2< String_t*, PropertyMetadata_t4066634616  >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.String,LitJson.PropertyMetadata>::Add(!0,!1) */, IDictionary_2_t169959035_il2cpp_TypeInfo_var, L_28, L_30, L_31);
	}

IL_00d6:
	{
		int32_t L_32 = V_3;
		V_3 = ((int32_t)((int32_t)L_32+(int32_t)1));
	}

IL_00da:
	{
		int32_t L_33 = V_3;
		PropertyInfoU5BU5D_t4286713048* L_34 = V_2;
		NullCheck(L_34);
		if ((((int32_t)L_33) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_34)->max_length)))))))
		{
			goto IL_004b;
		}
	}
	{
		Type_t * L_35 = ___type0;
		NullCheck(L_35);
		FieldInfoU5BU5D_t2567562023* L_36 = Type_GetFields_m3137302773(L_35, /*hidden argument*/NULL);
		V_7 = L_36;
		V_8 = 0;
		goto IL_013c;
	}

IL_00f3:
	{
		FieldInfoU5BU5D_t2567562023* L_37 = V_7;
		int32_t L_38 = V_8;
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, L_38);
		int32_t L_39 = L_38;
		FieldInfo_t * L_40 = (L_37)->GetAt(static_cast<il2cpp_array_size_t>(L_39));
		V_6 = L_40;
		Initobj (PropertyMetadata_t4066634616_il2cpp_TypeInfo_var, (&V_9));
		FieldInfo_t * L_41 = V_6;
		(&V_9)->set_Info_0(L_41);
		(&V_9)->set_IsField_1((bool)1);
		FieldInfo_t * L_42 = V_6;
		NullCheck(L_42);
		Type_t * L_43 = VirtFuncInvoker0< Type_t * >::Invoke(16 /* System.Type System.Reflection.FieldInfo::get_FieldType() */, L_42);
		(&V_9)->set_Type_2(L_43);
		Il2CppObject* L_44 = ObjectMetadata_get_Properties_m1175853819((&V_0), /*hidden argument*/NULL);
		FieldInfo_t * L_45 = V_6;
		NullCheck(L_45);
		String_t* L_46 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_45);
		PropertyMetadata_t4066634616  L_47 = V_9;
		NullCheck(L_44);
		InterfaceActionInvoker2< String_t*, PropertyMetadata_t4066634616  >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.String,LitJson.PropertyMetadata>::Add(!0,!1) */, IDictionary_2_t169959035_il2cpp_TypeInfo_var, L_44, L_46, L_47);
		int32_t L_48 = V_8;
		V_8 = ((int32_t)((int32_t)L_48+(int32_t)1));
	}

IL_013c:
	{
		int32_t L_49 = V_8;
		FieldInfoU5BU5D_t2567562023* L_50 = V_7;
		NullCheck(L_50);
		if ((((int32_t)L_49) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_50)->max_length)))))))
		{
			goto IL_00f3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		Il2CppObject * L_51 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_object_metadata_lock_11();
		V_10 = L_51;
		Il2CppObject * L_52 = V_10;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
	}

IL_0155:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
			Il2CppObject* L_53 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_object_metadata_10();
			Type_t * L_54 = ___type0;
			ObjectMetadata_t2009294498  L_55 = V_0;
			NullCheck(L_53);
			InterfaceActionInvoker2< Type_t *, ObjectMetadata_t2009294498  >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,LitJson.ObjectMetadata>::Add(!0,!1) */, IDictionary_2_t1692590328_il2cpp_TypeInfo_var, L_53, L_54, L_55);
			goto IL_016c;
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t3991598821 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (ArgumentException_t928607144_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0166;
			throw e;
		}

CATCH_0166:
		{ // begin catch(System.ArgumentException)
			IL2CPP_LEAVE(0x179, FINALLY_0171);
		} // end catch (depth: 2)

IL_016c:
		{
			IL2CPP_LEAVE(0x179, FINALLY_0171);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0171;
	}

FINALLY_0171:
	{ // begin finally (depth: 1)
		Il2CppObject * L_56 = V_10;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_56, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(369)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(369)
	{
		IL2CPP_JUMP_TBL(0x179, IL_0179)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0179:
	{
		return;
	}
}
// System.Void LitJson.JsonMapper::AddTypeProperties(System.Type)
extern Il2CppClass* JsonMapper_t863513565_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t2149610353_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1139852872_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* PropertyMetadata_t4066634616_il2cpp_TypeInfo_var;
extern Il2CppClass* ICollection_1_t666257307_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3359518189_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2289459;
extern const uint32_t JsonMapper_AddTypeProperties_m1596814329_MetadataUsageId;
extern "C"  void JsonMapper_AddTypeProperties_m1596814329 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_AddTypeProperties_m1596814329_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	PropertyInfo_t * V_1 = NULL;
	PropertyInfoU5BU5D_t4286713048* V_2 = NULL;
	int32_t V_3 = 0;
	PropertyMetadata_t4066634616  V_4;
	memset(&V_4, 0, sizeof(V_4));
	FieldInfo_t * V_5 = NULL;
	FieldInfoU5BU5D_t2567562023* V_6 = NULL;
	int32_t V_7 = 0;
	PropertyMetadata_t4066634616  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Il2CppObject * V_9 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_type_properties_12();
		Type_t * L_1 = ___type0;
		NullCheck(L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, Type_t * >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IList`1<LitJson.PropertyMetadata>>::ContainsKey(!0) */, IDictionary_2_t2149610353_il2cpp_TypeInfo_var, L_0, L_1);
		if (!L_2)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		List_1_t1139852872 * L_3 = (List_1_t1139852872 *)il2cpp_codegen_object_new(List_1_t1139852872_il2cpp_TypeInfo_var);
		List_1__ctor_m3359518189(L_3, /*hidden argument*/List_1__ctor_m3359518189_MethodInfo_var);
		V_0 = L_3;
		Type_t * L_4 = ___type0;
		NullCheck(L_4);
		PropertyInfoU5BU5D_t4286713048* L_5 = Type_GetProperties_m3853308260(L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		V_3 = 0;
		goto IL_0067;
	}

IL_0025:
	{
		PropertyInfoU5BU5D_t4286713048* L_6 = V_2;
		int32_t L_7 = V_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		PropertyInfo_t * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = L_9;
		PropertyInfo_t * L_10 = V_1;
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_10);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_11, _stringLiteral2289459, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0043;
		}
	}
	{
		goto IL_0063;
	}

IL_0043:
	{
		Initobj (PropertyMetadata_t4066634616_il2cpp_TypeInfo_var, (&V_4));
		PropertyInfo_t * L_13 = V_1;
		(&V_4)->set_Info_0(L_13);
		(&V_4)->set_IsField_1((bool)0);
		Il2CppObject* L_14 = V_0;
		PropertyMetadata_t4066634616  L_15 = V_4;
		NullCheck(L_14);
		InterfaceActionInvoker1< PropertyMetadata_t4066634616  >::Invoke(2 /* System.Void System.Collections.Generic.ICollection`1<LitJson.PropertyMetadata>::Add(!0) */, ICollection_1_t666257307_il2cpp_TypeInfo_var, L_14, L_15);
	}

IL_0063:
	{
		int32_t L_16 = V_3;
		V_3 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0067:
	{
		int32_t L_17 = V_3;
		PropertyInfoU5BU5D_t4286713048* L_18 = V_2;
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_18)->max_length)))))))
		{
			goto IL_0025;
		}
	}
	{
		Type_t * L_19 = ___type0;
		NullCheck(L_19);
		FieldInfoU5BU5D_t2567562023* L_20 = Type_GetFields_m3137302773(L_19, /*hidden argument*/NULL);
		V_6 = L_20;
		V_7 = 0;
		goto IL_00ae;
	}

IL_0080:
	{
		FieldInfoU5BU5D_t2567562023* L_21 = V_6;
		int32_t L_22 = V_7;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		int32_t L_23 = L_22;
		FieldInfo_t * L_24 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		V_5 = L_24;
		Initobj (PropertyMetadata_t4066634616_il2cpp_TypeInfo_var, (&V_8));
		FieldInfo_t * L_25 = V_5;
		(&V_8)->set_Info_0(L_25);
		(&V_8)->set_IsField_1((bool)1);
		Il2CppObject* L_26 = V_0;
		PropertyMetadata_t4066634616  L_27 = V_8;
		NullCheck(L_26);
		InterfaceActionInvoker1< PropertyMetadata_t4066634616  >::Invoke(2 /* System.Void System.Collections.Generic.ICollection`1<LitJson.PropertyMetadata>::Add(!0) */, ICollection_1_t666257307_il2cpp_TypeInfo_var, L_26, L_27);
		int32_t L_28 = V_7;
		V_7 = ((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_00ae:
	{
		int32_t L_29 = V_7;
		FieldInfoU5BU5D_t2567562023* L_30 = V_6;
		NullCheck(L_30);
		if ((((int32_t)L_29) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_30)->max_length)))))))
		{
			goto IL_0080;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		Il2CppObject * L_31 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_type_properties_lock_13();
		V_9 = L_31;
		Il2CppObject * L_32 = V_9;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
	}

IL_00c7:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
			Il2CppObject* L_33 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_type_properties_12();
			Type_t * L_34 = ___type0;
			Il2CppObject* L_35 = V_0;
			NullCheck(L_33);
			InterfaceActionInvoker2< Type_t *, Il2CppObject* >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IList`1<LitJson.PropertyMetadata>>::Add(!0,!1) */, IDictionary_2_t2149610353_il2cpp_TypeInfo_var, L_33, L_34, L_35);
			goto IL_00de;
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t3991598821 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (ArgumentException_t928607144_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_00d8;
			throw e;
		}

CATCH_00d8:
		{ // begin catch(System.ArgumentException)
			IL2CPP_LEAVE(0xEB, FINALLY_00e3);
		} // end catch (depth: 2)

IL_00de:
		{
			IL2CPP_LEAVE(0xEB, FINALLY_00e3);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_00e3;
	}

FINALLY_00e3:
	{ // begin finally (depth: 1)
		Il2CppObject * L_36 = V_9;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(227)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(227)
	{
		IL2CPP_JUMP_TBL(0xEB, IL_00eb)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_00eb:
	{
		return;
	}
}
// System.Reflection.MethodInfo LitJson.JsonMapper::GetConvOp(System.Type,System.Type)
extern Il2CppClass* JsonMapper_t863513565_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t3980295021_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t424158550_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t2031895_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m3498791655_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1596924579;
extern const uint32_t JsonMapper_GetConvOp_m3287415259_MetadataUsageId;
extern "C"  MethodInfo_t * JsonMapper_GetConvOp_m3287415259 (Il2CppObject * __this /* static, unused */, Type_t * ___t10, Type_t * ___t21, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_GetConvOp_m3287415259_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	MethodInfo_t * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	MethodInfo_t * V_3 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_conv_ops_lock_9();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
			Il2CppObject* L_2 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_conv_ops_8();
			Type_t * L_3 = ___t10;
			NullCheck(L_2);
			bool L_4 = InterfaceFuncInvoker1< bool, Type_t * >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,System.Reflection.MethodInfo>>::ContainsKey(!0) */, IDictionary_2_t3980295021_il2cpp_TypeInfo_var, L_2, L_3);
			if (L_4)
			{
				goto IL_002c;
			}
		}

IL_001c:
		{
			IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
			Il2CppObject* L_5 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_conv_ops_8();
			Type_t * L_6 = ___t10;
			Dictionary_2_t424158550 * L_7 = (Dictionary_2_t424158550 *)il2cpp_codegen_object_new(Dictionary_2_t424158550_il2cpp_TypeInfo_var);
			Dictionary_2__ctor_m3498791655(L_7, /*hidden argument*/Dictionary_2__ctor_m3498791655_MethodInfo_var);
			NullCheck(L_5);
			InterfaceActionInvoker2< Type_t *, Il2CppObject* >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,System.Reflection.MethodInfo>>::Add(!0,!1) */, IDictionary_2_t3980295021_il2cpp_TypeInfo_var, L_5, L_6, L_7);
		}

IL_002c:
		{
			IL2CPP_LEAVE(0x38, FINALLY_0031);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0031;
	}

FINALLY_0031:
	{ // begin finally (depth: 1)
		Il2CppObject * L_8 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(49)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(49)
	{
		IL2CPP_JUMP_TBL(0x38, IL_0038)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0038:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		Il2CppObject* L_9 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_conv_ops_8();
		Type_t * L_10 = ___t10;
		NullCheck(L_9);
		Il2CppObject* L_11 = InterfaceFuncInvoker1< Il2CppObject*, Type_t * >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,System.Reflection.MethodInfo>>::get_Item(!0) */, IDictionary_2_t3980295021_il2cpp_TypeInfo_var, L_9, L_10);
		Type_t * L_12 = ___t21;
		NullCheck(L_11);
		bool L_13 = InterfaceFuncInvoker1< bool, Type_t * >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Type,System.Reflection.MethodInfo>::ContainsKey(!0) */, IDictionary_2_t2031895_il2cpp_TypeInfo_var, L_11, L_12);
		if (!L_13)
		{
			goto IL_0060;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		Il2CppObject* L_14 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_conv_ops_8();
		Type_t * L_15 = ___t10;
		NullCheck(L_14);
		Il2CppObject* L_16 = InterfaceFuncInvoker1< Il2CppObject*, Type_t * >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,System.Reflection.MethodInfo>>::get_Item(!0) */, IDictionary_2_t3980295021_il2cpp_TypeInfo_var, L_14, L_15);
		Type_t * L_17 = ___t21;
		NullCheck(L_16);
		MethodInfo_t * L_18 = InterfaceFuncInvoker1< MethodInfo_t *, Type_t * >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Type,System.Reflection.MethodInfo>::get_Item(!0) */, IDictionary_2_t2031895_il2cpp_TypeInfo_var, L_16, L_17);
		return L_18;
	}

IL_0060:
	{
		Type_t * L_19 = ___t10;
		TypeU5BU5D_t3339007067* L_20 = ((TypeU5BU5D_t3339007067*)SZArrayNew(TypeU5BU5D_t3339007067_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_21 = ___t21;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 0);
		ArrayElementTypeCheck (L_20, L_21);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_21);
		NullCheck(L_19);
		MethodInfo_t * L_22 = Type_GetMethod_m3977319851(L_19, _stringLiteral1596924579, L_20, /*hidden argument*/NULL);
		V_1 = L_22;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		Il2CppObject * L_23 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_conv_ops_lock_9();
		V_2 = L_23;
		Il2CppObject * L_24 = V_2;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
	}

IL_0082:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
			Il2CppObject* L_25 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_conv_ops_8();
			Type_t * L_26 = ___t10;
			NullCheck(L_25);
			Il2CppObject* L_27 = InterfaceFuncInvoker1< Il2CppObject*, Type_t * >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,System.Reflection.MethodInfo>>::get_Item(!0) */, IDictionary_2_t3980295021_il2cpp_TypeInfo_var, L_25, L_26);
			Type_t * L_28 = ___t21;
			MethodInfo_t * L_29 = V_1;
			NullCheck(L_27);
			InterfaceActionInvoker2< Type_t *, MethodInfo_t * >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,System.Reflection.MethodInfo>::Add(!0,!1) */, IDictionary_2_t2031895_il2cpp_TypeInfo_var, L_27, L_28, L_29);
			goto IL_00b1;
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t3991598821 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (ArgumentException_t928607144_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0099;
			throw e;
		}

CATCH_0099:
		{ // begin catch(System.ArgumentException)
			IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
			Il2CppObject* L_30 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_conv_ops_8();
			Type_t * L_31 = ___t10;
			NullCheck(L_30);
			Il2CppObject* L_32 = InterfaceFuncInvoker1< Il2CppObject*, Type_t * >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,System.Reflection.MethodInfo>>::get_Item(!0) */, IDictionary_2_t3980295021_il2cpp_TypeInfo_var, L_30, L_31);
			Type_t * L_33 = ___t21;
			NullCheck(L_32);
			MethodInfo_t * L_34 = InterfaceFuncInvoker1< MethodInfo_t *, Type_t * >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Type,System.Reflection.MethodInfo>::get_Item(!0) */, IDictionary_2_t2031895_il2cpp_TypeInfo_var, L_32, L_33);
			V_3 = L_34;
			IL2CPP_LEAVE(0xBF, FINALLY_00b6);
		} // end catch (depth: 2)

IL_00b1:
		{
			IL2CPP_LEAVE(0xBD, FINALLY_00b6);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_00b6;
	}

FINALLY_00b6:
	{ // begin finally (depth: 1)
		Il2CppObject * L_35 = V_2;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(182)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(182)
	{
		IL2CPP_JUMP_TBL(0xBF, IL_00bf)
		IL2CPP_JUMP_TBL(0xBD, IL_00bd)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_00bd:
	{
		MethodInfo_t * L_36 = V_1;
		return L_36;
	}

IL_00bf:
	{
		MethodInfo_t * L_37 = V_3;
		return L_37;
	}
}
// System.Object LitJson.JsonMapper::ReadValue(System.Type,LitJson.JsonReader)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonException_t3617621405_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonMapper_t863513565_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t1504911478_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t1821615648_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2862688501_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t3741638740_il2cpp_TypeInfo_var;
extern Il2CppClass* IList_t1751339649_il2cpp_TypeInfo_var;
extern Il2CppClass* ArrayList_t3948406897_il2cpp_TypeInfo_var;
extern Il2CppClass* ICollection_t2643922881_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppArray_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t1692590328_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t169959035_il2cpp_TypeInfo_var;
extern Il2CppClass* FieldInfo_t_il2cpp_TypeInfo_var;
extern Il2CppClass* PropertyInfo_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_t537317817_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2123704921;
extern Il2CppCodeGenString* _stringLiteral15474268;
extern Il2CppCodeGenString* _stringLiteral3886461381;
extern Il2CppCodeGenString* _stringLiteral3496246422;
extern const uint32_t JsonMapper_ReadValue_m1229353803_MetadataUsageId;
extern "C"  Il2CppObject * JsonMapper_ReadValue_m1229353803 (Il2CppObject * __this /* static, unused */, Type_t * ___inst_type0, JsonReader_t1009895007 * ___reader1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_ReadValue_m1229353803_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	Type_t * V_2 = NULL;
	ImporterFunc_t2138319818 * V_3 = NULL;
	ImporterFunc_t2138319818 * V_4 = NULL;
	MethodInfo_t * V_5 = NULL;
	Il2CppObject * V_6 = NULL;
	ArrayMetadata_t4058342910  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Il2CppObject * V_8 = NULL;
	Type_t * V_9 = NULL;
	Il2CppObject * V_10 = NULL;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	ObjectMetadata_t2009294498  V_13;
	memset(&V_13, 0, sizeof(V_13));
	String_t* V_14 = NULL;
	PropertyMetadata_t4066634616  V_15;
	memset(&V_15, 0, sizeof(V_15));
	PropertyInfo_t * V_16 = NULL;
	Type_t * G_B4_0 = NULL;
	Type_t * G_B3_0 = NULL;
	{
		JsonReader_t1009895007 * L_0 = ___reader1;
		NullCheck(L_0);
		JsonReader_Read_m3716278654(L_0, /*hidden argument*/NULL);
		JsonReader_t1009895007 * L_1 = ___reader1;
		NullCheck(L_1);
		int32_t L_2 = JsonReader_get_Token_m340664751(L_1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)5))))
		{
			goto IL_0015;
		}
	}
	{
		return NULL;
	}

IL_0015:
	{
		Type_t * L_3 = ___inst_type0;
		Type_t * L_4 = Nullable_GetUnderlyingType_m3559493248(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Type_t * L_5 = V_0;
		Type_t * L_6 = L_5;
		G_B3_0 = L_6;
		if (L_6)
		{
			G_B4_0 = L_6;
			goto IL_0025;
		}
	}
	{
		Type_t * L_7 = ___inst_type0;
		G_B4_0 = L_7;
	}

IL_0025:
	{
		V_1 = G_B4_0;
		JsonReader_t1009895007 * L_8 = ___reader1;
		NullCheck(L_8);
		int32_t L_9 = JsonReader_get_Token_m340664751(L_8, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_0057;
		}
	}
	{
		Type_t * L_10 = ___inst_type0;
		NullCheck(L_10);
		bool L_11 = Type_get_IsClass_m2426046944(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_0044;
		}
	}
	{
		Type_t * L_12 = V_0;
		if (!L_12)
		{
			goto IL_0046;
		}
	}

IL_0044:
	{
		return NULL;
	}

IL_0046:
	{
		Type_t * L_13 = ___inst_type0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral2123704921, L_13, /*hidden argument*/NULL);
		JsonException_t3617621405 * L_15 = (JsonException_t3617621405 *)il2cpp_codegen_object_new(JsonException_t3617621405_il2cpp_TypeInfo_var);
		JsonException__ctor_m2918697824(L_15, L_14, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_15);
	}

IL_0057:
	{
		JsonReader_t1009895007 * L_16 = ___reader1;
		NullCheck(L_16);
		int32_t L_17 = JsonReader_get_Token_m340664751(L_16, /*hidden argument*/NULL);
		if ((((int32_t)L_17) == ((int32_t)8)))
		{
			goto IL_0095;
		}
	}
	{
		JsonReader_t1009895007 * L_18 = ___reader1;
		NullCheck(L_18);
		int32_t L_19 = JsonReader_get_Token_m340664751(L_18, /*hidden argument*/NULL);
		if ((((int32_t)L_19) == ((int32_t)6)))
		{
			goto IL_0095;
		}
	}
	{
		JsonReader_t1009895007 * L_20 = ___reader1;
		NullCheck(L_20);
		int32_t L_21 = JsonReader_get_Token_m340664751(L_20, /*hidden argument*/NULL);
		if ((((int32_t)L_21) == ((int32_t)7)))
		{
			goto IL_0095;
		}
	}
	{
		JsonReader_t1009895007 * L_22 = ___reader1;
		NullCheck(L_22);
		int32_t L_23 = JsonReader_get_Token_m340664751(L_22, /*hidden argument*/NULL);
		if ((((int32_t)L_23) == ((int32_t)((int32_t)9))))
		{
			goto IL_0095;
		}
	}
	{
		JsonReader_t1009895007 * L_24 = ___reader1;
		NullCheck(L_24);
		int32_t L_25 = JsonReader_get_Token_m340664751(L_24, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_25) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0198;
		}
	}

IL_0095:
	{
		JsonReader_t1009895007 * L_26 = ___reader1;
		NullCheck(L_26);
		Il2CppObject * L_27 = JsonReader_get_Value_m950067043(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		Type_t * L_28 = Object_GetType_m2022236990(L_27, /*hidden argument*/NULL);
		V_2 = L_28;
		Type_t * L_29 = V_1;
		Type_t * L_30 = V_2;
		NullCheck(L_29);
		bool L_31 = VirtFuncInvoker1< bool, Type_t * >::Invoke(42 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_29, L_30);
		if (!L_31)
		{
			goto IL_00b4;
		}
	}
	{
		JsonReader_t1009895007 * L_32 = ___reader1;
		NullCheck(L_32);
		Il2CppObject * L_33 = JsonReader_get_Value_m950067043(L_32, /*hidden argument*/NULL);
		return L_33;
	}

IL_00b4:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		Il2CppObject* L_34 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_custom_importers_table_5();
		Type_t * L_35 = V_2;
		NullCheck(L_34);
		bool L_36 = InterfaceFuncInvoker1< bool, Type_t * >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>>::ContainsKey(!0) */, IDictionary_2_t1504911478_il2cpp_TypeInfo_var, L_34, L_35);
		if (!L_36)
		{
			goto IL_00f9;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		Il2CppObject* L_37 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_custom_importers_table_5();
		Type_t * L_38 = V_2;
		NullCheck(L_37);
		Il2CppObject* L_39 = InterfaceFuncInvoker1< Il2CppObject*, Type_t * >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>>::get_Item(!0) */, IDictionary_2_t1504911478_il2cpp_TypeInfo_var, L_37, L_38);
		Type_t * L_40 = V_1;
		NullCheck(L_39);
		bool L_41 = InterfaceFuncInvoker1< bool, Type_t * >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>::ContainsKey(!0) */, IDictionary_2_t1821615648_il2cpp_TypeInfo_var, L_39, L_40);
		if (!L_41)
		{
			goto IL_00f9;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		Il2CppObject* L_42 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_custom_importers_table_5();
		Type_t * L_43 = V_2;
		NullCheck(L_42);
		Il2CppObject* L_44 = InterfaceFuncInvoker1< Il2CppObject*, Type_t * >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>>::get_Item(!0) */, IDictionary_2_t1504911478_il2cpp_TypeInfo_var, L_42, L_43);
		Type_t * L_45 = V_1;
		NullCheck(L_44);
		ImporterFunc_t2138319818 * L_46 = InterfaceFuncInvoker1< ImporterFunc_t2138319818 *, Type_t * >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>::get_Item(!0) */, IDictionary_2_t1821615648_il2cpp_TypeInfo_var, L_44, L_45);
		V_3 = L_46;
		ImporterFunc_t2138319818 * L_47 = V_3;
		JsonReader_t1009895007 * L_48 = ___reader1;
		NullCheck(L_48);
		Il2CppObject * L_49 = JsonReader_get_Value_m950067043(L_48, /*hidden argument*/NULL);
		NullCheck(L_47);
		Il2CppObject * L_50 = ImporterFunc_Invoke_m2708021386(L_47, L_49, /*hidden argument*/NULL);
		return L_50;
	}

IL_00f9:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		Il2CppObject* L_51 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		Type_t * L_52 = V_2;
		NullCheck(L_51);
		bool L_53 = InterfaceFuncInvoker1< bool, Type_t * >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>>::ContainsKey(!0) */, IDictionary_2_t1504911478_il2cpp_TypeInfo_var, L_51, L_52);
		if (!L_53)
		{
			goto IL_0140;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		Il2CppObject* L_54 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		Type_t * L_55 = V_2;
		NullCheck(L_54);
		Il2CppObject* L_56 = InterfaceFuncInvoker1< Il2CppObject*, Type_t * >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>>::get_Item(!0) */, IDictionary_2_t1504911478_il2cpp_TypeInfo_var, L_54, L_55);
		Type_t * L_57 = V_1;
		NullCheck(L_56);
		bool L_58 = InterfaceFuncInvoker1< bool, Type_t * >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>::ContainsKey(!0) */, IDictionary_2_t1821615648_il2cpp_TypeInfo_var, L_56, L_57);
		if (!L_58)
		{
			goto IL_0140;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		Il2CppObject* L_59 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		Type_t * L_60 = V_2;
		NullCheck(L_59);
		Il2CppObject* L_61 = InterfaceFuncInvoker1< Il2CppObject*, Type_t * >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>>::get_Item(!0) */, IDictionary_2_t1504911478_il2cpp_TypeInfo_var, L_59, L_60);
		Type_t * L_62 = V_1;
		NullCheck(L_61);
		ImporterFunc_t2138319818 * L_63 = InterfaceFuncInvoker1< ImporterFunc_t2138319818 *, Type_t * >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>::get_Item(!0) */, IDictionary_2_t1821615648_il2cpp_TypeInfo_var, L_61, L_62);
		V_4 = L_63;
		ImporterFunc_t2138319818 * L_64 = V_4;
		JsonReader_t1009895007 * L_65 = ___reader1;
		NullCheck(L_65);
		Il2CppObject * L_66 = JsonReader_get_Value_m950067043(L_65, /*hidden argument*/NULL);
		NullCheck(L_64);
		Il2CppObject * L_67 = ImporterFunc_Invoke_m2708021386(L_64, L_66, /*hidden argument*/NULL);
		return L_67;
	}

IL_0140:
	{
		Type_t * L_68 = V_1;
		NullCheck(L_68);
		bool L_69 = Type_get_IsEnum_m3878730619(L_68, /*hidden argument*/NULL);
		if (!L_69)
		{
			goto IL_0158;
		}
	}
	{
		Type_t * L_70 = V_1;
		JsonReader_t1009895007 * L_71 = ___reader1;
		NullCheck(L_71);
		Il2CppObject * L_72 = JsonReader_get_Value_m950067043(L_71, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2862688501_il2cpp_TypeInfo_var);
		Il2CppObject * L_73 = Enum_ToObject_m1129836274(NULL /*static, unused*/, L_70, L_72, /*hidden argument*/NULL);
		return L_73;
	}

IL_0158:
	{
		Type_t * L_74 = V_1;
		Type_t * L_75 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		MethodInfo_t * L_76 = JsonMapper_GetConvOp_m3287415259(NULL /*static, unused*/, L_74, L_75, /*hidden argument*/NULL);
		V_5 = L_76;
		MethodInfo_t * L_77 = V_5;
		if (!L_77)
		{
			goto IL_0180;
		}
	}
	{
		MethodInfo_t * L_78 = V_5;
		ObjectU5BU5D_t1108656482* L_79 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)1));
		JsonReader_t1009895007 * L_80 = ___reader1;
		NullCheck(L_80);
		Il2CppObject * L_81 = JsonReader_get_Value_m950067043(L_80, /*hidden argument*/NULL);
		NullCheck(L_79);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_79, 0);
		ArrayElementTypeCheck (L_79, L_81);
		(L_79)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_81);
		NullCheck(L_78);
		Il2CppObject * L_82 = MethodBase_Invoke_m3435166155(L_78, NULL, L_79, /*hidden argument*/NULL);
		return L_82;
	}

IL_0180:
	{
		JsonReader_t1009895007 * L_83 = ___reader1;
		NullCheck(L_83);
		Il2CppObject * L_84 = JsonReader_get_Value_m950067043(L_83, /*hidden argument*/NULL);
		Type_t * L_85 = V_2;
		Type_t * L_86 = ___inst_type0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_87 = String_Format_m3928391288(NULL /*static, unused*/, _stringLiteral15474268, L_84, L_85, L_86, /*hidden argument*/NULL);
		JsonException_t3617621405 * L_88 = (JsonException_t3617621405 *)il2cpp_codegen_object_new(JsonException_t3617621405_il2cpp_TypeInfo_var);
		JsonException__ctor_m2918697824(L_88, L_87, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_88);
	}

IL_0198:
	{
		V_6 = NULL;
		JsonReader_t1009895007 * L_89 = ___reader1;
		NullCheck(L_89);
		int32_t L_90 = JsonReader_get_Token_m340664751(L_89, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_90) == ((uint32_t)4))))
		{
			goto IL_02a6;
		}
	}
	{
		Type_t * L_91 = ___inst_type0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		JsonMapper_AddArrayMetadata_m50814818(NULL /*static, unused*/, L_91, /*hidden argument*/NULL);
		Il2CppObject* L_92 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_array_metadata_6();
		Type_t * L_93 = ___inst_type0;
		NullCheck(L_92);
		ArrayMetadata_t4058342910  L_94 = InterfaceFuncInvoker1< ArrayMetadata_t4058342910 , Type_t * >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Type,LitJson.ArrayMetadata>::get_Item(!0) */, IDictionary_2_t3741638740_il2cpp_TypeInfo_var, L_92, L_93);
		V_7 = L_94;
		bool L_95 = ArrayMetadata_get_IsArray_m858588955((&V_7), /*hidden argument*/NULL);
		if (L_95)
		{
			goto IL_01e3;
		}
	}
	{
		bool L_96 = ArrayMetadata_get_IsList_m1858371582((&V_7), /*hidden argument*/NULL);
		if (L_96)
		{
			goto IL_01e3;
		}
	}
	{
		Type_t * L_97 = ___inst_type0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_98 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral3886461381, L_97, /*hidden argument*/NULL);
		JsonException_t3617621405 * L_99 = (JsonException_t3617621405 *)il2cpp_codegen_object_new(JsonException_t3617621405_il2cpp_TypeInfo_var);
		JsonException__ctor_m2918697824(L_99, L_98, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_99);
	}

IL_01e3:
	{
		bool L_100 = ArrayMetadata_get_IsArray_m858588955((&V_7), /*hidden argument*/NULL);
		if (L_100)
		{
			goto IL_020a;
		}
	}
	{
		Type_t * L_101 = ___inst_type0;
		Il2CppObject * L_102 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, L_101, /*hidden argument*/NULL);
		V_8 = ((Il2CppObject *)Castclass(L_102, IList_t1751339649_il2cpp_TypeInfo_var));
		Type_t * L_103 = ArrayMetadata_get_ElementType_m1471053318((&V_7), /*hidden argument*/NULL);
		V_9 = L_103;
		goto IL_0219;
	}

IL_020a:
	{
		ArrayList_t3948406897 * L_104 = (ArrayList_t3948406897 *)il2cpp_codegen_object_new(ArrayList_t3948406897_il2cpp_TypeInfo_var);
		ArrayList__ctor_m1878432947(L_104, /*hidden argument*/NULL);
		V_8 = L_104;
		Type_t * L_105 = ___inst_type0;
		NullCheck(L_105);
		Type_t * L_106 = VirtFuncInvoker0< Type_t * >::Invoke(44 /* System.Type System.Type::GetElementType() */, L_105);
		V_9 = L_106;
	}

IL_0219:
	{
		Type_t * L_107 = V_9;
		JsonReader_t1009895007 * L_108 = ___reader1;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		Il2CppObject * L_109 = JsonMapper_ReadValue_m1229353803(NULL /*static, unused*/, L_107, L_108, /*hidden argument*/NULL);
		V_10 = L_109;
		Il2CppObject * L_110 = V_10;
		if (L_110)
		{
			goto IL_023b;
		}
	}
	{
		JsonReader_t1009895007 * L_111 = ___reader1;
		NullCheck(L_111);
		int32_t L_112 = JsonReader_get_Token_m340664751(L_111, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_112) == ((uint32_t)5))))
		{
			goto IL_023b;
		}
	}
	{
		goto IL_024a;
	}

IL_023b:
	{
		Il2CppObject * L_113 = V_8;
		Il2CppObject * L_114 = V_10;
		NullCheck(L_113);
		InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(4 /* System.Int32 System.Collections.IList::Add(System.Object) */, IList_t1751339649_il2cpp_TypeInfo_var, L_113, L_114);
		goto IL_0219;
	}

IL_024a:
	{
		bool L_115 = ArrayMetadata_get_IsArray_m858588955((&V_7), /*hidden argument*/NULL);
		if (!L_115)
		{
			goto IL_029d;
		}
	}
	{
		Il2CppObject * L_116 = V_8;
		NullCheck(L_116);
		int32_t L_117 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.ICollection::get_Count() */, ICollection_t2643922881_il2cpp_TypeInfo_var, L_116);
		V_11 = L_117;
		Type_t * L_118 = V_9;
		int32_t L_119 = V_11;
		Il2CppArray * L_120 = Array_CreateInstance_m1364223436(NULL /*static, unused*/, L_118, L_119, /*hidden argument*/NULL);
		V_6 = L_120;
		V_12 = 0;
		goto IL_028f;
	}

IL_0272:
	{
		Il2CppObject * L_121 = V_6;
		Il2CppObject * L_122 = V_8;
		int32_t L_123 = V_12;
		NullCheck(L_122);
		Il2CppObject * L_124 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(2 /* System.Object System.Collections.IList::get_Item(System.Int32) */, IList_t1751339649_il2cpp_TypeInfo_var, L_122, L_123);
		int32_t L_125 = V_12;
		NullCheck(((Il2CppArray *)CastclassClass(L_121, Il2CppArray_il2cpp_TypeInfo_var)));
		Array_SetValue_m3564402974(((Il2CppArray *)CastclassClass(L_121, Il2CppArray_il2cpp_TypeInfo_var)), L_124, L_125, /*hidden argument*/NULL);
		int32_t L_126 = V_12;
		V_12 = ((int32_t)((int32_t)L_126+(int32_t)1));
	}

IL_028f:
	{
		int32_t L_127 = V_12;
		int32_t L_128 = V_11;
		if ((((int32_t)L_127) < ((int32_t)L_128)))
		{
			goto IL_0272;
		}
	}
	{
		goto IL_02a1;
	}

IL_029d:
	{
		Il2CppObject * L_129 = V_8;
		V_6 = L_129;
	}

IL_02a1:
	{
		goto IL_03e4;
	}

IL_02a6:
	{
		JsonReader_t1009895007 * L_130 = ___reader1;
		NullCheck(L_130);
		int32_t L_131 = JsonReader_get_Token_m340664751(L_130, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_131) == ((uint32_t)1))))
		{
			goto IL_03e4;
		}
	}
	{
		Type_t * L_132 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		JsonMapper_AddObjectMetadata_m902222360(NULL /*static, unused*/, L_132, /*hidden argument*/NULL);
		Il2CppObject* L_133 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_object_metadata_10();
		Type_t * L_134 = V_1;
		NullCheck(L_133);
		ObjectMetadata_t2009294498  L_135 = InterfaceFuncInvoker1< ObjectMetadata_t2009294498 , Type_t * >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Type,LitJson.ObjectMetadata>::get_Item(!0) */, IDictionary_2_t1692590328_il2cpp_TypeInfo_var, L_133, L_134);
		V_13 = L_135;
		Type_t * L_136 = V_1;
		Il2CppObject * L_137 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, L_136, /*hidden argument*/NULL);
		V_6 = L_137;
	}

IL_02cd:
	{
		JsonReader_t1009895007 * L_138 = ___reader1;
		NullCheck(L_138);
		JsonReader_Read_m3716278654(L_138, /*hidden argument*/NULL);
		JsonReader_t1009895007 * L_139 = ___reader1;
		NullCheck(L_139);
		int32_t L_140 = JsonReader_get_Token_m340664751(L_139, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_140) == ((uint32_t)3))))
		{
			goto IL_02e5;
		}
	}
	{
		goto IL_03e4;
	}

IL_02e5:
	{
		JsonReader_t1009895007 * L_141 = ___reader1;
		NullCheck(L_141);
		Il2CppObject * L_142 = JsonReader_get_Value_m950067043(L_141, /*hidden argument*/NULL);
		V_14 = ((String_t*)CastclassSealed(L_142, String_t_il2cpp_TypeInfo_var));
		Il2CppObject* L_143 = ObjectMetadata_get_Properties_m1175853819((&V_13), /*hidden argument*/NULL);
		String_t* L_144 = V_14;
		NullCheck(L_143);
		bool L_145 = InterfaceFuncInvoker1< bool, String_t* >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,LitJson.PropertyMetadata>::ContainsKey(!0) */, IDictionary_2_t169959035_il2cpp_TypeInfo_var, L_143, L_144);
		if (!L_145)
		{
			goto IL_038f;
		}
	}
	{
		Il2CppObject* L_146 = ObjectMetadata_get_Properties_m1175853819((&V_13), /*hidden argument*/NULL);
		String_t* L_147 = V_14;
		NullCheck(L_146);
		PropertyMetadata_t4066634616  L_148 = InterfaceFuncInvoker1< PropertyMetadata_t4066634616 , String_t* >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.String,LitJson.PropertyMetadata>::get_Item(!0) */, IDictionary_2_t169959035_il2cpp_TypeInfo_var, L_146, L_147);
		V_15 = L_148;
		bool L_149 = (&V_15)->get_IsField_1();
		if (!L_149)
		{
			goto IL_0346;
		}
	}
	{
		MemberInfo_t * L_150 = (&V_15)->get_Info_0();
		Il2CppObject * L_151 = V_6;
		Type_t * L_152 = (&V_15)->get_Type_2();
		JsonReader_t1009895007 * L_153 = ___reader1;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		Il2CppObject * L_154 = JsonMapper_ReadValue_m1229353803(NULL /*static, unused*/, L_152, L_153, /*hidden argument*/NULL);
		NullCheck(((FieldInfo_t *)CastclassClass(L_150, FieldInfo_t_il2cpp_TypeInfo_var)));
		FieldInfo_SetValue_m1669444927(((FieldInfo_t *)CastclassClass(L_150, FieldInfo_t_il2cpp_TypeInfo_var)), L_151, L_154, /*hidden argument*/NULL);
		goto IL_038a;
	}

IL_0346:
	{
		MemberInfo_t * L_155 = (&V_15)->get_Info_0();
		V_16 = ((PropertyInfo_t *)CastclassClass(L_155, PropertyInfo_t_il2cpp_TypeInfo_var));
		PropertyInfo_t * L_156 = V_16;
		NullCheck(L_156);
		bool L_157 = VirtFuncInvoker0< bool >::Invoke(16 /* System.Boolean System.Reflection.PropertyInfo::get_CanWrite() */, L_156);
		if (!L_157)
		{
			goto IL_037c;
		}
	}
	{
		PropertyInfo_t * L_158 = V_16;
		Il2CppObject * L_159 = V_6;
		Type_t * L_160 = (&V_15)->get_Type_2();
		JsonReader_t1009895007 * L_161 = ___reader1;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		Il2CppObject * L_162 = JsonMapper_ReadValue_m1229353803(NULL /*static, unused*/, L_160, L_161, /*hidden argument*/NULL);
		NullCheck(L_158);
		VirtActionInvoker3< Il2CppObject *, Il2CppObject *, ObjectU5BU5D_t1108656482* >::Invoke(24 /* System.Void System.Reflection.PropertyInfo::SetValue(System.Object,System.Object,System.Object[]) */, L_158, L_159, L_162, (ObjectU5BU5D_t1108656482*)(ObjectU5BU5D_t1108656482*)NULL);
		goto IL_038a;
	}

IL_037c:
	{
		Type_t * L_163 = (&V_15)->get_Type_2();
		JsonReader_t1009895007 * L_164 = ___reader1;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		JsonMapper_ReadValue_m1229353803(NULL /*static, unused*/, L_163, L_164, /*hidden argument*/NULL);
	}

IL_038a:
	{
		goto IL_03df;
	}

IL_038f:
	{
		bool L_165 = ObjectMetadata_get_IsDictionary_m1841257108((&V_13), /*hidden argument*/NULL);
		if (L_165)
		{
			goto IL_03c4;
		}
	}
	{
		JsonReader_t1009895007 * L_166 = ___reader1;
		NullCheck(L_166);
		bool L_167 = JsonReader_get_SkipNonMembers_m1256537244(L_166, /*hidden argument*/NULL);
		if (L_167)
		{
			goto IL_03b9;
		}
	}
	{
		Type_t * L_168 = ___inst_type0;
		String_t* L_169 = V_14;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_170 = String_Format_m2398979370(NULL /*static, unused*/, _stringLiteral3496246422, L_168, L_169, /*hidden argument*/NULL);
		JsonException_t3617621405 * L_171 = (JsonException_t3617621405 *)il2cpp_codegen_object_new(JsonException_t3617621405_il2cpp_TypeInfo_var);
		JsonException__ctor_m2918697824(L_171, L_170, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_171);
	}

IL_03b9:
	{
		JsonReader_t1009895007 * L_172 = ___reader1;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		JsonMapper_ReadSkip_m994794731(NULL /*static, unused*/, L_172, /*hidden argument*/NULL);
		goto IL_02cd;
	}

IL_03c4:
	{
		Il2CppObject * L_173 = V_6;
		String_t* L_174 = V_14;
		Type_t * L_175 = ObjectMetadata_get_ElementType_m3060033696((&V_13), /*hidden argument*/NULL);
		JsonReader_t1009895007 * L_176 = ___reader1;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		Il2CppObject * L_177 = JsonMapper_ReadValue_m1229353803(NULL /*static, unused*/, L_175, L_176, /*hidden argument*/NULL);
		NullCheck(((Il2CppObject *)Castclass(L_173, IDictionary_t537317817_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(2 /* System.Void System.Collections.IDictionary::Add(System.Object,System.Object) */, IDictionary_t537317817_il2cpp_TypeInfo_var, ((Il2CppObject *)Castclass(L_173, IDictionary_t537317817_il2cpp_TypeInfo_var)), L_174, L_177);
	}

IL_03df:
	{
		goto IL_02cd;
	}

IL_03e4:
	{
		Il2CppObject * L_178 = V_6;
		return L_178;
	}
}
// LitJson.IJsonWrapper LitJson.JsonMapper::ReadValue(LitJson.WrapperFactory,LitJson.JsonReader)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IJsonWrapper_t2026182966_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t1153838595_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonMapper_t863513565_il2cpp_TypeInfo_var;
extern Il2CppClass* IList_t1751339649_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_t537317817_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_ReadValue_m3662287667_MetadataUsageId;
extern "C"  Il2CppObject * JsonMapper_ReadValue_m3662287667 (Il2CppObject * __this /* static, unused */, WrapperFactory_t3264289803 * ___factory0, JsonReader_t1009895007 * ___reader1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_ReadValue_m3662287667_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	String_t* V_2 = NULL;
	{
		JsonReader_t1009895007 * L_0 = ___reader1;
		NullCheck(L_0);
		JsonReader_Read_m3716278654(L_0, /*hidden argument*/NULL);
		JsonReader_t1009895007 * L_1 = ___reader1;
		NullCheck(L_1);
		int32_t L_2 = JsonReader_get_Token_m340664751(L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) == ((int32_t)5)))
		{
			goto IL_0020;
		}
	}
	{
		JsonReader_t1009895007 * L_3 = ___reader1;
		NullCheck(L_3);
		int32_t L_4 = JsonReader_get_Token_m340664751(L_3, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_0022;
		}
	}

IL_0020:
	{
		return (Il2CppObject *)NULL;
	}

IL_0022:
	{
		WrapperFactory_t3264289803 * L_5 = ___factory0;
		NullCheck(L_5);
		Il2CppObject * L_6 = WrapperFactory_Invoke_m650811940(L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		JsonReader_t1009895007 * L_7 = ___reader1;
		NullCheck(L_7);
		int32_t L_8 = JsonReader_get_Token_m340664751(L_7, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)9)))))
		{
			goto IL_0049;
		}
	}
	{
		Il2CppObject * L_9 = V_0;
		JsonReader_t1009895007 * L_10 = ___reader1;
		NullCheck(L_10);
		Il2CppObject * L_11 = JsonReader_get_Value_m950067043(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		InterfaceActionInvoker1< String_t* >::Invoke(17 /* System.Void LitJson.IJsonWrapper::SetString(System.String) */, IJsonWrapper_t2026182966_il2cpp_TypeInfo_var, L_9, ((String_t*)CastclassSealed(L_11, String_t_il2cpp_TypeInfo_var)));
		Il2CppObject * L_12 = V_0;
		return L_12;
	}

IL_0049:
	{
		JsonReader_t1009895007 * L_13 = ___reader1;
		NullCheck(L_13);
		int32_t L_14 = JsonReader_get_Token_m340664751(L_13, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_14) == ((uint32_t)8))))
		{
			goto IL_0068;
		}
	}
	{
		Il2CppObject * L_15 = V_0;
		JsonReader_t1009895007 * L_16 = ___reader1;
		NullCheck(L_16);
		Il2CppObject * L_17 = JsonReader_get_Value_m950067043(L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		InterfaceActionInvoker1< double >::Invoke(13 /* System.Void LitJson.IJsonWrapper::SetDouble(System.Double) */, IJsonWrapper_t2026182966_il2cpp_TypeInfo_var, L_15, ((*(double*)((double*)UnBox (L_17, Double_t3868226565_il2cpp_TypeInfo_var)))));
		Il2CppObject * L_18 = V_0;
		return L_18;
	}

IL_0068:
	{
		JsonReader_t1009895007 * L_19 = ___reader1;
		NullCheck(L_19);
		int32_t L_20 = JsonReader_get_Token_m340664751(L_19, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_20) == ((uint32_t)6))))
		{
			goto IL_0087;
		}
	}
	{
		Il2CppObject * L_21 = V_0;
		JsonReader_t1009895007 * L_22 = ___reader1;
		NullCheck(L_22);
		Il2CppObject * L_23 = JsonReader_get_Value_m950067043(L_22, /*hidden argument*/NULL);
		NullCheck(L_21);
		InterfaceActionInvoker1< int32_t >::Invoke(14 /* System.Void LitJson.IJsonWrapper::SetInt(System.Int32) */, IJsonWrapper_t2026182966_il2cpp_TypeInfo_var, L_21, ((*(int32_t*)((int32_t*)UnBox (L_23, Int32_t1153838500_il2cpp_TypeInfo_var)))));
		Il2CppObject * L_24 = V_0;
		return L_24;
	}

IL_0087:
	{
		JsonReader_t1009895007 * L_25 = ___reader1;
		NullCheck(L_25);
		int32_t L_26 = JsonReader_get_Token_m340664751(L_25, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_26) == ((uint32_t)7))))
		{
			goto IL_00a6;
		}
	}
	{
		Il2CppObject * L_27 = V_0;
		JsonReader_t1009895007 * L_28 = ___reader1;
		NullCheck(L_28);
		Il2CppObject * L_29 = JsonReader_get_Value_m950067043(L_28, /*hidden argument*/NULL);
		NullCheck(L_27);
		InterfaceActionInvoker1< int64_t >::Invoke(16 /* System.Void LitJson.IJsonWrapper::SetLong(System.Int64) */, IJsonWrapper_t2026182966_il2cpp_TypeInfo_var, L_27, ((*(int64_t*)((int64_t*)UnBox (L_29, Int64_t1153838595_il2cpp_TypeInfo_var)))));
		Il2CppObject * L_30 = V_0;
		return L_30;
	}

IL_00a6:
	{
		JsonReader_t1009895007 * L_31 = ___reader1;
		NullCheck(L_31);
		int32_t L_32 = JsonReader_get_Token_m340664751(L_31, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_32) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_00c6;
		}
	}
	{
		Il2CppObject * L_33 = V_0;
		JsonReader_t1009895007 * L_34 = ___reader1;
		NullCheck(L_34);
		Il2CppObject * L_35 = JsonReader_get_Value_m950067043(L_34, /*hidden argument*/NULL);
		NullCheck(L_33);
		InterfaceActionInvoker1< bool >::Invoke(12 /* System.Void LitJson.IJsonWrapper::SetBoolean(System.Boolean) */, IJsonWrapper_t2026182966_il2cpp_TypeInfo_var, L_33, ((*(bool*)((bool*)UnBox (L_35, Boolean_t476798718_il2cpp_TypeInfo_var)))));
		Il2CppObject * L_36 = V_0;
		return L_36;
	}

IL_00c6:
	{
		JsonReader_t1009895007 * L_37 = ___reader1;
		NullCheck(L_37);
		int32_t L_38 = JsonReader_get_Token_m340664751(L_37, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_38) == ((uint32_t)4))))
		{
			goto IL_010a;
		}
	}
	{
		Il2CppObject * L_39 = V_0;
		NullCheck(L_39);
		InterfaceActionInvoker1< int32_t >::Invoke(15 /* System.Void LitJson.IJsonWrapper::SetJsonType(LitJson.JsonType) */, IJsonWrapper_t2026182966_il2cpp_TypeInfo_var, L_39, 2);
	}

IL_00d9:
	{
		WrapperFactory_t3264289803 * L_40 = ___factory0;
		JsonReader_t1009895007 * L_41 = ___reader1;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		Il2CppObject * L_42 = JsonMapper_ReadValue_m3662287667(NULL /*static, unused*/, L_40, L_41, /*hidden argument*/NULL);
		V_1 = L_42;
		Il2CppObject * L_43 = V_1;
		if (L_43)
		{
			goto IL_00f8;
		}
	}
	{
		JsonReader_t1009895007 * L_44 = ___reader1;
		NullCheck(L_44);
		int32_t L_45 = JsonReader_get_Token_m340664751(L_44, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_45) == ((uint32_t)5))))
		{
			goto IL_00f8;
		}
	}
	{
		goto IL_0105;
	}

IL_00f8:
	{
		Il2CppObject * L_46 = V_0;
		Il2CppObject * L_47 = V_1;
		NullCheck(L_46);
		InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(4 /* System.Int32 System.Collections.IList::Add(System.Object) */, IList_t1751339649_il2cpp_TypeInfo_var, L_46, L_47);
		goto IL_00d9;
	}

IL_0105:
	{
		goto IL_0154;
	}

IL_010a:
	{
		JsonReader_t1009895007 * L_48 = ___reader1;
		NullCheck(L_48);
		int32_t L_49 = JsonReader_get_Token_m340664751(L_48, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_49) == ((uint32_t)1))))
		{
			goto IL_0154;
		}
	}
	{
		Il2CppObject * L_50 = V_0;
		NullCheck(L_50);
		InterfaceActionInvoker1< int32_t >::Invoke(15 /* System.Void LitJson.IJsonWrapper::SetJsonType(LitJson.JsonType) */, IJsonWrapper_t2026182966_il2cpp_TypeInfo_var, L_50, 1);
	}

IL_011d:
	{
		JsonReader_t1009895007 * L_51 = ___reader1;
		NullCheck(L_51);
		JsonReader_Read_m3716278654(L_51, /*hidden argument*/NULL);
		JsonReader_t1009895007 * L_52 = ___reader1;
		NullCheck(L_52);
		int32_t L_53 = JsonReader_get_Token_m340664751(L_52, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_53) == ((uint32_t)3))))
		{
			goto IL_0135;
		}
	}
	{
		goto IL_0154;
	}

IL_0135:
	{
		JsonReader_t1009895007 * L_54 = ___reader1;
		NullCheck(L_54);
		Il2CppObject * L_55 = JsonReader_get_Value_m950067043(L_54, /*hidden argument*/NULL);
		V_2 = ((String_t*)CastclassSealed(L_55, String_t_il2cpp_TypeInfo_var));
		Il2CppObject * L_56 = V_0;
		String_t* L_57 = V_2;
		WrapperFactory_t3264289803 * L_58 = ___factory0;
		JsonReader_t1009895007 * L_59 = ___reader1;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		Il2CppObject * L_60 = JsonMapper_ReadValue_m3662287667(NULL /*static, unused*/, L_58, L_59, /*hidden argument*/NULL);
		NullCheck(L_56);
		InterfaceActionInvoker2< Il2CppObject *, Il2CppObject * >::Invoke(1 /* System.Void System.Collections.IDictionary::set_Item(System.Object,System.Object) */, IDictionary_t537317817_il2cpp_TypeInfo_var, L_56, L_57, L_60);
		goto IL_011d;
	}

IL_0154:
	{
		Il2CppObject * L_61 = V_0;
		return L_61;
	}
}
// System.Void LitJson.JsonMapper::ReadSkip(LitJson.JsonReader)
extern Il2CppClass* JsonMapper_t863513565_il2cpp_TypeInfo_var;
extern Il2CppClass* WrapperFactory_t3264289803_il2cpp_TypeInfo_var;
extern const MethodInfo* JsonMapper_U3CReadSkipU3Em__0_m2844748398_MethodInfo_var;
extern const uint32_t JsonMapper_ReadSkip_m994794731_MetadataUsageId;
extern "C"  void JsonMapper_ReadSkip_m994794731 (Il2CppObject * __this /* static, unused */, JsonReader_t1009895007 * ___reader0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_ReadSkip_m994794731_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		WrapperFactory_t3264289803 * L_0 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_16();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)JsonMapper_U3CReadSkipU3Em__0_m2844748398_MethodInfo_var);
		WrapperFactory_t3264289803 * L_2 = (WrapperFactory_t3264289803 *)il2cpp_codegen_object_new(WrapperFactory_t3264289803_il2cpp_TypeInfo_var);
		WrapperFactory__ctor_m1067507964(L_2, NULL, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_16(L_2);
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		WrapperFactory_t3264289803 * L_3 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_16();
		JsonReader_t1009895007 * L_4 = ___reader0;
		JsonMapper_ToWrapper_m3791715062(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonMapper::RegisterBaseExporters()
extern const Il2CppType* Byte_t2862609660_0_0_0_var;
extern const Il2CppType* Char_t2862622538_0_0_0_var;
extern const Il2CppType* DateTime_t4283661327_0_0_0_var;
extern const Il2CppType* Decimal_t1954350631_0_0_0_var;
extern const Il2CppType* SByte_t1161769777_0_0_0_var;
extern const Il2CppType* Int16_t1153838442_0_0_0_var;
extern const Il2CppType* UInt16_t24667923_0_0_0_var;
extern const Il2CppType* UInt32_t24667981_0_0_0_var;
extern const Il2CppType* UInt64_t24668076_0_0_0_var;
extern Il2CppClass* JsonMapper_t863513565_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ExporterFunc_t3330360473_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t3013656303_il2cpp_TypeInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseExportersU3Em__1_m1072502522_MethodInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseExportersU3Em__2_m2797983419_MethodInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseExportersU3Em__3_m228497020_MethodInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseExportersU3Em__4_m1953977917_MethodInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseExportersU3Em__5_m3679458814_MethodInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseExportersU3Em__6_m1109972415_MethodInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseExportersU3Em__7_m2835453312_MethodInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseExportersU3Em__8_m265966913_MethodInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseExportersU3Em__9_m1991447810_MethodInfo_var;
extern const uint32_t JsonMapper_RegisterBaseExporters_m1541476208_MetadataUsageId;
extern "C"  void JsonMapper_RegisterBaseExporters_m1541476208 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_RegisterBaseExporters_m1541476208_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * G_B2_0 = NULL;
	Il2CppObject* G_B2_1 = NULL;
	Type_t * G_B1_0 = NULL;
	Il2CppObject* G_B1_1 = NULL;
	Type_t * G_B4_0 = NULL;
	Il2CppObject* G_B4_1 = NULL;
	Type_t * G_B3_0 = NULL;
	Il2CppObject* G_B3_1 = NULL;
	Type_t * G_B6_0 = NULL;
	Il2CppObject* G_B6_1 = NULL;
	Type_t * G_B5_0 = NULL;
	Il2CppObject* G_B5_1 = NULL;
	Type_t * G_B8_0 = NULL;
	Il2CppObject* G_B8_1 = NULL;
	Type_t * G_B7_0 = NULL;
	Il2CppObject* G_B7_1 = NULL;
	Type_t * G_B10_0 = NULL;
	Il2CppObject* G_B10_1 = NULL;
	Type_t * G_B9_0 = NULL;
	Il2CppObject* G_B9_1 = NULL;
	Type_t * G_B12_0 = NULL;
	Il2CppObject* G_B12_1 = NULL;
	Type_t * G_B11_0 = NULL;
	Il2CppObject* G_B11_1 = NULL;
	Type_t * G_B14_0 = NULL;
	Il2CppObject* G_B14_1 = NULL;
	Type_t * G_B13_0 = NULL;
	Il2CppObject* G_B13_1 = NULL;
	Type_t * G_B16_0 = NULL;
	Il2CppObject* G_B16_1 = NULL;
	Type_t * G_B15_0 = NULL;
	Il2CppObject* G_B15_1 = NULL;
	Type_t * G_B18_0 = NULL;
	Il2CppObject* G_B18_1 = NULL;
	Type_t * G_B17_0 = NULL;
	Il2CppObject* G_B17_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_base_exporters_table_2();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Byte_t2862609660_0_0_0_var), /*hidden argument*/NULL);
		ExporterFunc_t3330360473 * L_2 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_17();
		G_B1_0 = L_1;
		G_B1_1 = L_0;
		if (L_2)
		{
			G_B2_0 = L_1;
			G_B2_1 = L_0;
			goto IL_0027;
		}
	}
	{
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseExportersU3Em__1_m1072502522_MethodInfo_var);
		ExporterFunc_t3330360473 * L_4 = (ExporterFunc_t3330360473 *)il2cpp_codegen_object_new(ExporterFunc_t3330360473_il2cpp_TypeInfo_var);
		ExporterFunc__ctor_m657802314(L_4, NULL, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache1_17(L_4);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		ExporterFunc_t3330360473 * L_5 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_17();
		NullCheck(G_B2_1);
		InterfaceActionInvoker2< Type_t *, ExporterFunc_t3330360473 * >::Invoke(3 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,LitJson.ExporterFunc>::set_Item(!0,!1) */, IDictionary_2_t3013656303_il2cpp_TypeInfo_var, G_B2_1, G_B2_0, L_5);
		Il2CppObject* L_6 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_base_exporters_table_2();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Char_t2862622538_0_0_0_var), /*hidden argument*/NULL);
		ExporterFunc_t3330360473 * L_8 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_18();
		G_B3_0 = L_7;
		G_B3_1 = L_6;
		if (L_8)
		{
			G_B4_0 = L_7;
			G_B4_1 = L_6;
			goto IL_0058;
		}
	}
	{
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseExportersU3Em__2_m2797983419_MethodInfo_var);
		ExporterFunc_t3330360473 * L_10 = (ExporterFunc_t3330360473 *)il2cpp_codegen_object_new(ExporterFunc_t3330360473_il2cpp_TypeInfo_var);
		ExporterFunc__ctor_m657802314(L_10, NULL, L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache2_18(L_10);
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
	}

IL_0058:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		ExporterFunc_t3330360473 * L_11 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_18();
		NullCheck(G_B4_1);
		InterfaceActionInvoker2< Type_t *, ExporterFunc_t3330360473 * >::Invoke(3 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,LitJson.ExporterFunc>::set_Item(!0,!1) */, IDictionary_2_t3013656303_il2cpp_TypeInfo_var, G_B4_1, G_B4_0, L_11);
		Il2CppObject* L_12 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_base_exporters_table_2();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_13 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(DateTime_t4283661327_0_0_0_var), /*hidden argument*/NULL);
		ExporterFunc_t3330360473 * L_14 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache3_19();
		G_B5_0 = L_13;
		G_B5_1 = L_12;
		if (L_14)
		{
			G_B6_0 = L_13;
			G_B6_1 = L_12;
			goto IL_0089;
		}
	}
	{
		IntPtr_t L_15;
		L_15.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseExportersU3Em__3_m228497020_MethodInfo_var);
		ExporterFunc_t3330360473 * L_16 = (ExporterFunc_t3330360473 *)il2cpp_codegen_object_new(ExporterFunc_t3330360473_il2cpp_TypeInfo_var);
		ExporterFunc__ctor_m657802314(L_16, NULL, L_15, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache3_19(L_16);
		G_B6_0 = G_B5_0;
		G_B6_1 = G_B5_1;
	}

IL_0089:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		ExporterFunc_t3330360473 * L_17 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache3_19();
		NullCheck(G_B6_1);
		InterfaceActionInvoker2< Type_t *, ExporterFunc_t3330360473 * >::Invoke(3 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,LitJson.ExporterFunc>::set_Item(!0,!1) */, IDictionary_2_t3013656303_il2cpp_TypeInfo_var, G_B6_1, G_B6_0, L_17);
		Il2CppObject* L_18 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_base_exporters_table_2();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_19 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Decimal_t1954350631_0_0_0_var), /*hidden argument*/NULL);
		ExporterFunc_t3330360473 * L_20 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache4_20();
		G_B7_0 = L_19;
		G_B7_1 = L_18;
		if (L_20)
		{
			G_B8_0 = L_19;
			G_B8_1 = L_18;
			goto IL_00ba;
		}
	}
	{
		IntPtr_t L_21;
		L_21.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseExportersU3Em__4_m1953977917_MethodInfo_var);
		ExporterFunc_t3330360473 * L_22 = (ExporterFunc_t3330360473 *)il2cpp_codegen_object_new(ExporterFunc_t3330360473_il2cpp_TypeInfo_var);
		ExporterFunc__ctor_m657802314(L_22, NULL, L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache4_20(L_22);
		G_B8_0 = G_B7_0;
		G_B8_1 = G_B7_1;
	}

IL_00ba:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		ExporterFunc_t3330360473 * L_23 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache4_20();
		NullCheck(G_B8_1);
		InterfaceActionInvoker2< Type_t *, ExporterFunc_t3330360473 * >::Invoke(3 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,LitJson.ExporterFunc>::set_Item(!0,!1) */, IDictionary_2_t3013656303_il2cpp_TypeInfo_var, G_B8_1, G_B8_0, L_23);
		Il2CppObject* L_24 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_base_exporters_table_2();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_25 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(SByte_t1161769777_0_0_0_var), /*hidden argument*/NULL);
		ExporterFunc_t3330360473 * L_26 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache5_21();
		G_B9_0 = L_25;
		G_B9_1 = L_24;
		if (L_26)
		{
			G_B10_0 = L_25;
			G_B10_1 = L_24;
			goto IL_00eb;
		}
	}
	{
		IntPtr_t L_27;
		L_27.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseExportersU3Em__5_m3679458814_MethodInfo_var);
		ExporterFunc_t3330360473 * L_28 = (ExporterFunc_t3330360473 *)il2cpp_codegen_object_new(ExporterFunc_t3330360473_il2cpp_TypeInfo_var);
		ExporterFunc__ctor_m657802314(L_28, NULL, L_27, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache5_21(L_28);
		G_B10_0 = G_B9_0;
		G_B10_1 = G_B9_1;
	}

IL_00eb:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		ExporterFunc_t3330360473 * L_29 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache5_21();
		NullCheck(G_B10_1);
		InterfaceActionInvoker2< Type_t *, ExporterFunc_t3330360473 * >::Invoke(3 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,LitJson.ExporterFunc>::set_Item(!0,!1) */, IDictionary_2_t3013656303_il2cpp_TypeInfo_var, G_B10_1, G_B10_0, L_29);
		Il2CppObject* L_30 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_base_exporters_table_2();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_31 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int16_t1153838442_0_0_0_var), /*hidden argument*/NULL);
		ExporterFunc_t3330360473 * L_32 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache6_22();
		G_B11_0 = L_31;
		G_B11_1 = L_30;
		if (L_32)
		{
			G_B12_0 = L_31;
			G_B12_1 = L_30;
			goto IL_011c;
		}
	}
	{
		IntPtr_t L_33;
		L_33.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseExportersU3Em__6_m1109972415_MethodInfo_var);
		ExporterFunc_t3330360473 * L_34 = (ExporterFunc_t3330360473 *)il2cpp_codegen_object_new(ExporterFunc_t3330360473_il2cpp_TypeInfo_var);
		ExporterFunc__ctor_m657802314(L_34, NULL, L_33, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache6_22(L_34);
		G_B12_0 = G_B11_0;
		G_B12_1 = G_B11_1;
	}

IL_011c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		ExporterFunc_t3330360473 * L_35 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache6_22();
		NullCheck(G_B12_1);
		InterfaceActionInvoker2< Type_t *, ExporterFunc_t3330360473 * >::Invoke(3 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,LitJson.ExporterFunc>::set_Item(!0,!1) */, IDictionary_2_t3013656303_il2cpp_TypeInfo_var, G_B12_1, G_B12_0, L_35);
		Il2CppObject* L_36 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_base_exporters_table_2();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_37 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(UInt16_t24667923_0_0_0_var), /*hidden argument*/NULL);
		ExporterFunc_t3330360473 * L_38 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache7_23();
		G_B13_0 = L_37;
		G_B13_1 = L_36;
		if (L_38)
		{
			G_B14_0 = L_37;
			G_B14_1 = L_36;
			goto IL_014d;
		}
	}
	{
		IntPtr_t L_39;
		L_39.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseExportersU3Em__7_m2835453312_MethodInfo_var);
		ExporterFunc_t3330360473 * L_40 = (ExporterFunc_t3330360473 *)il2cpp_codegen_object_new(ExporterFunc_t3330360473_il2cpp_TypeInfo_var);
		ExporterFunc__ctor_m657802314(L_40, NULL, L_39, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache7_23(L_40);
		G_B14_0 = G_B13_0;
		G_B14_1 = G_B13_1;
	}

IL_014d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		ExporterFunc_t3330360473 * L_41 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache7_23();
		NullCheck(G_B14_1);
		InterfaceActionInvoker2< Type_t *, ExporterFunc_t3330360473 * >::Invoke(3 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,LitJson.ExporterFunc>::set_Item(!0,!1) */, IDictionary_2_t3013656303_il2cpp_TypeInfo_var, G_B14_1, G_B14_0, L_41);
		Il2CppObject* L_42 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_base_exporters_table_2();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_43 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(UInt32_t24667981_0_0_0_var), /*hidden argument*/NULL);
		ExporterFunc_t3330360473 * L_44 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache8_24();
		G_B15_0 = L_43;
		G_B15_1 = L_42;
		if (L_44)
		{
			G_B16_0 = L_43;
			G_B16_1 = L_42;
			goto IL_017e;
		}
	}
	{
		IntPtr_t L_45;
		L_45.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseExportersU3Em__8_m265966913_MethodInfo_var);
		ExporterFunc_t3330360473 * L_46 = (ExporterFunc_t3330360473 *)il2cpp_codegen_object_new(ExporterFunc_t3330360473_il2cpp_TypeInfo_var);
		ExporterFunc__ctor_m657802314(L_46, NULL, L_45, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache8_24(L_46);
		G_B16_0 = G_B15_0;
		G_B16_1 = G_B15_1;
	}

IL_017e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		ExporterFunc_t3330360473 * L_47 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache8_24();
		NullCheck(G_B16_1);
		InterfaceActionInvoker2< Type_t *, ExporterFunc_t3330360473 * >::Invoke(3 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,LitJson.ExporterFunc>::set_Item(!0,!1) */, IDictionary_2_t3013656303_il2cpp_TypeInfo_var, G_B16_1, G_B16_0, L_47);
		Il2CppObject* L_48 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_base_exporters_table_2();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_49 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(UInt64_t24668076_0_0_0_var), /*hidden argument*/NULL);
		ExporterFunc_t3330360473 * L_50 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache9_25();
		G_B17_0 = L_49;
		G_B17_1 = L_48;
		if (L_50)
		{
			G_B18_0 = L_49;
			G_B18_1 = L_48;
			goto IL_01af;
		}
	}
	{
		IntPtr_t L_51;
		L_51.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseExportersU3Em__9_m1991447810_MethodInfo_var);
		ExporterFunc_t3330360473 * L_52 = (ExporterFunc_t3330360473 *)il2cpp_codegen_object_new(ExporterFunc_t3330360473_il2cpp_TypeInfo_var);
		ExporterFunc__ctor_m657802314(L_52, NULL, L_51, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache9_25(L_52);
		G_B18_0 = G_B17_0;
		G_B18_1 = G_B17_1;
	}

IL_01af:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		ExporterFunc_t3330360473 * L_53 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache9_25();
		NullCheck(G_B18_1);
		InterfaceActionInvoker2< Type_t *, ExporterFunc_t3330360473 * >::Invoke(3 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,LitJson.ExporterFunc>::set_Item(!0,!1) */, IDictionary_2_t3013656303_il2cpp_TypeInfo_var, G_B18_1, G_B18_0, L_53);
		return;
	}
}
// System.Void LitJson.JsonMapper::RegisterBaseImporters()
extern const Il2CppType* Int32_t1153838500_0_0_0_var;
extern const Il2CppType* Byte_t2862609660_0_0_0_var;
extern const Il2CppType* UInt64_t24668076_0_0_0_var;
extern const Il2CppType* SByte_t1161769777_0_0_0_var;
extern const Il2CppType* Int16_t1153838442_0_0_0_var;
extern const Il2CppType* UInt16_t24667923_0_0_0_var;
extern const Il2CppType* UInt32_t24667981_0_0_0_var;
extern const Il2CppType* Single_t4291918972_0_0_0_var;
extern const Il2CppType* Double_t3868226565_0_0_0_var;
extern const Il2CppType* Decimal_t1954350631_0_0_0_var;
extern const Il2CppType* Int64_t1153838595_0_0_0_var;
extern const Il2CppType* String_t_0_0_0_var;
extern const Il2CppType* Char_t2862622538_0_0_0_var;
extern const Il2CppType* DateTime_t4283661327_0_0_0_var;
extern Il2CppClass* JsonMapper_t863513565_il2cpp_TypeInfo_var;
extern Il2CppClass* ImporterFunc_t2138319818_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseImportersU3Em__A_m976906870_MethodInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseImportersU3Em__B_m466372693_MethodInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseImportersU3Em__C_m4250805812_MethodInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseImportersU3Em__D_m3740271635_MethodInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseImportersU3Em__E_m3229737458_MethodInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseImportersU3Em__F_m2719203281_MethodInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseImportersU3Em__10_m890446864_MethodInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseImportersU3Em__11_m379912687_MethodInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseImportersU3Em__12_m4164345806_MethodInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseImportersU3Em__13_m3653811629_MethodInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseImportersU3Em__14_m3143277452_MethodInfo_var;
extern const MethodInfo* JsonMapper_U3CRegisterBaseImportersU3Em__15_m2632743275_MethodInfo_var;
extern const uint32_t JsonMapper_RegisterBaseImporters_m810286623_MetadataUsageId;
extern "C"  void JsonMapper_RegisterBaseImporters_m810286623 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_RegisterBaseImporters_m810286623_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ImporterFunc_t2138319818 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		ImporterFunc_t2138319818 * L_0 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheA_26();
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseImportersU3Em__A_m976906870_MethodInfo_var);
		ImporterFunc_t2138319818 * L_2 = (ImporterFunc_t2138319818 *)il2cpp_codegen_object_new(ImporterFunc_t2138319818_il2cpp_TypeInfo_var);
		ImporterFunc__ctor_m35661563(L_2, NULL, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cacheA_26(L_2);
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		ImporterFunc_t2138319818 * L_3 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheA_26();
		V_0 = L_3;
		Il2CppObject* L_4 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int32_t1153838500_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_6 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Byte_t2862609660_0_0_0_var), /*hidden argument*/NULL);
		ImporterFunc_t2138319818 * L_7 = V_0;
		JsonMapper_RegisterImporter_m2108531963(NULL /*static, unused*/, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		ImporterFunc_t2138319818 * L_8 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheB_27();
		if (L_8)
		{
			goto IL_0055;
		}
	}
	{
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseImportersU3Em__B_m466372693_MethodInfo_var);
		ImporterFunc_t2138319818 * L_10 = (ImporterFunc_t2138319818 *)il2cpp_codegen_object_new(ImporterFunc_t2138319818_il2cpp_TypeInfo_var);
		ImporterFunc__ctor_m35661563(L_10, NULL, L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cacheB_27(L_10);
	}

IL_0055:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		ImporterFunc_t2138319818 * L_11 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheB_27();
		V_0 = L_11;
		Il2CppObject* L_12 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_13 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int32_t1153838500_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_14 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(UInt64_t24668076_0_0_0_var), /*hidden argument*/NULL);
		ImporterFunc_t2138319818 * L_15 = V_0;
		JsonMapper_RegisterImporter_m2108531963(NULL /*static, unused*/, L_12, L_13, L_14, L_15, /*hidden argument*/NULL);
		ImporterFunc_t2138319818 * L_16 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheC_28();
		if (L_16)
		{
			goto IL_0092;
		}
	}
	{
		IntPtr_t L_17;
		L_17.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseImportersU3Em__C_m4250805812_MethodInfo_var);
		ImporterFunc_t2138319818 * L_18 = (ImporterFunc_t2138319818 *)il2cpp_codegen_object_new(ImporterFunc_t2138319818_il2cpp_TypeInfo_var);
		ImporterFunc__ctor_m35661563(L_18, NULL, L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cacheC_28(L_18);
	}

IL_0092:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		ImporterFunc_t2138319818 * L_19 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheC_28();
		V_0 = L_19;
		Il2CppObject* L_20 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_21 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int32_t1153838500_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_22 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(SByte_t1161769777_0_0_0_var), /*hidden argument*/NULL);
		ImporterFunc_t2138319818 * L_23 = V_0;
		JsonMapper_RegisterImporter_m2108531963(NULL /*static, unused*/, L_20, L_21, L_22, L_23, /*hidden argument*/NULL);
		ImporterFunc_t2138319818 * L_24 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheD_29();
		if (L_24)
		{
			goto IL_00cf;
		}
	}
	{
		IntPtr_t L_25;
		L_25.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseImportersU3Em__D_m3740271635_MethodInfo_var);
		ImporterFunc_t2138319818 * L_26 = (ImporterFunc_t2138319818 *)il2cpp_codegen_object_new(ImporterFunc_t2138319818_il2cpp_TypeInfo_var);
		ImporterFunc__ctor_m35661563(L_26, NULL, L_25, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cacheD_29(L_26);
	}

IL_00cf:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		ImporterFunc_t2138319818 * L_27 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheD_29();
		V_0 = L_27;
		Il2CppObject* L_28 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_29 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int32_t1153838500_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_30 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int16_t1153838442_0_0_0_var), /*hidden argument*/NULL);
		ImporterFunc_t2138319818 * L_31 = V_0;
		JsonMapper_RegisterImporter_m2108531963(NULL /*static, unused*/, L_28, L_29, L_30, L_31, /*hidden argument*/NULL);
		ImporterFunc_t2138319818 * L_32 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheE_30();
		if (L_32)
		{
			goto IL_010c;
		}
	}
	{
		IntPtr_t L_33;
		L_33.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseImportersU3Em__E_m3229737458_MethodInfo_var);
		ImporterFunc_t2138319818 * L_34 = (ImporterFunc_t2138319818 *)il2cpp_codegen_object_new(ImporterFunc_t2138319818_il2cpp_TypeInfo_var);
		ImporterFunc__ctor_m35661563(L_34, NULL, L_33, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cacheE_30(L_34);
	}

IL_010c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		ImporterFunc_t2138319818 * L_35 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheE_30();
		V_0 = L_35;
		Il2CppObject* L_36 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_37 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int32_t1153838500_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_38 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(UInt16_t24667923_0_0_0_var), /*hidden argument*/NULL);
		ImporterFunc_t2138319818 * L_39 = V_0;
		JsonMapper_RegisterImporter_m2108531963(NULL /*static, unused*/, L_36, L_37, L_38, L_39, /*hidden argument*/NULL);
		ImporterFunc_t2138319818 * L_40 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheF_31();
		if (L_40)
		{
			goto IL_0149;
		}
	}
	{
		IntPtr_t L_41;
		L_41.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseImportersU3Em__F_m2719203281_MethodInfo_var);
		ImporterFunc_t2138319818 * L_42 = (ImporterFunc_t2138319818 *)il2cpp_codegen_object_new(ImporterFunc_t2138319818_il2cpp_TypeInfo_var);
		ImporterFunc__ctor_m35661563(L_42, NULL, L_41, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cacheF_31(L_42);
	}

IL_0149:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		ImporterFunc_t2138319818 * L_43 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cacheF_31();
		V_0 = L_43;
		Il2CppObject* L_44 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_45 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int32_t1153838500_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_46 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(UInt32_t24667981_0_0_0_var), /*hidden argument*/NULL);
		ImporterFunc_t2138319818 * L_47 = V_0;
		JsonMapper_RegisterImporter_m2108531963(NULL /*static, unused*/, L_44, L_45, L_46, L_47, /*hidden argument*/NULL);
		ImporterFunc_t2138319818 * L_48 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache10_32();
		if (L_48)
		{
			goto IL_0186;
		}
	}
	{
		IntPtr_t L_49;
		L_49.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseImportersU3Em__10_m890446864_MethodInfo_var);
		ImporterFunc_t2138319818 * L_50 = (ImporterFunc_t2138319818 *)il2cpp_codegen_object_new(ImporterFunc_t2138319818_il2cpp_TypeInfo_var);
		ImporterFunc__ctor_m35661563(L_50, NULL, L_49, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache10_32(L_50);
	}

IL_0186:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		ImporterFunc_t2138319818 * L_51 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache10_32();
		V_0 = L_51;
		Il2CppObject* L_52 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_53 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int32_t1153838500_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_54 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Single_t4291918972_0_0_0_var), /*hidden argument*/NULL);
		ImporterFunc_t2138319818 * L_55 = V_0;
		JsonMapper_RegisterImporter_m2108531963(NULL /*static, unused*/, L_52, L_53, L_54, L_55, /*hidden argument*/NULL);
		ImporterFunc_t2138319818 * L_56 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache11_33();
		if (L_56)
		{
			goto IL_01c3;
		}
	}
	{
		IntPtr_t L_57;
		L_57.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseImportersU3Em__11_m379912687_MethodInfo_var);
		ImporterFunc_t2138319818 * L_58 = (ImporterFunc_t2138319818 *)il2cpp_codegen_object_new(ImporterFunc_t2138319818_il2cpp_TypeInfo_var);
		ImporterFunc__ctor_m35661563(L_58, NULL, L_57, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache11_33(L_58);
	}

IL_01c3:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		ImporterFunc_t2138319818 * L_59 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache11_33();
		V_0 = L_59;
		Il2CppObject* L_60 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_61 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int32_t1153838500_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_62 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Double_t3868226565_0_0_0_var), /*hidden argument*/NULL);
		ImporterFunc_t2138319818 * L_63 = V_0;
		JsonMapper_RegisterImporter_m2108531963(NULL /*static, unused*/, L_60, L_61, L_62, L_63, /*hidden argument*/NULL);
		ImporterFunc_t2138319818 * L_64 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache12_34();
		if (L_64)
		{
			goto IL_0200;
		}
	}
	{
		IntPtr_t L_65;
		L_65.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseImportersU3Em__12_m4164345806_MethodInfo_var);
		ImporterFunc_t2138319818 * L_66 = (ImporterFunc_t2138319818 *)il2cpp_codegen_object_new(ImporterFunc_t2138319818_il2cpp_TypeInfo_var);
		ImporterFunc__ctor_m35661563(L_66, NULL, L_65, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache12_34(L_66);
	}

IL_0200:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		ImporterFunc_t2138319818 * L_67 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache12_34();
		V_0 = L_67;
		Il2CppObject* L_68 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_69 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Double_t3868226565_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_70 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Decimal_t1954350631_0_0_0_var), /*hidden argument*/NULL);
		ImporterFunc_t2138319818 * L_71 = V_0;
		JsonMapper_RegisterImporter_m2108531963(NULL /*static, unused*/, L_68, L_69, L_70, L_71, /*hidden argument*/NULL);
		ImporterFunc_t2138319818 * L_72 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache13_35();
		if (L_72)
		{
			goto IL_023d;
		}
	}
	{
		IntPtr_t L_73;
		L_73.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseImportersU3Em__13_m3653811629_MethodInfo_var);
		ImporterFunc_t2138319818 * L_74 = (ImporterFunc_t2138319818 *)il2cpp_codegen_object_new(ImporterFunc_t2138319818_il2cpp_TypeInfo_var);
		ImporterFunc__ctor_m35661563(L_74, NULL, L_73, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache13_35(L_74);
	}

IL_023d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		ImporterFunc_t2138319818 * L_75 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache13_35();
		V_0 = L_75;
		Il2CppObject* L_76 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_77 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int64_t1153838595_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_78 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(UInt32_t24667981_0_0_0_var), /*hidden argument*/NULL);
		ImporterFunc_t2138319818 * L_79 = V_0;
		JsonMapper_RegisterImporter_m2108531963(NULL /*static, unused*/, L_76, L_77, L_78, L_79, /*hidden argument*/NULL);
		ImporterFunc_t2138319818 * L_80 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache14_36();
		if (L_80)
		{
			goto IL_027a;
		}
	}
	{
		IntPtr_t L_81;
		L_81.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseImportersU3Em__14_m3143277452_MethodInfo_var);
		ImporterFunc_t2138319818 * L_82 = (ImporterFunc_t2138319818 *)il2cpp_codegen_object_new(ImporterFunc_t2138319818_il2cpp_TypeInfo_var);
		ImporterFunc__ctor_m35661563(L_82, NULL, L_81, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache14_36(L_82);
	}

IL_027a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		ImporterFunc_t2138319818 * L_83 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache14_36();
		V_0 = L_83;
		Il2CppObject* L_84 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_85 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_86 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Char_t2862622538_0_0_0_var), /*hidden argument*/NULL);
		ImporterFunc_t2138319818 * L_87 = V_0;
		JsonMapper_RegisterImporter_m2108531963(NULL /*static, unused*/, L_84, L_85, L_86, L_87, /*hidden argument*/NULL);
		ImporterFunc_t2138319818 * L_88 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache15_37();
		if (L_88)
		{
			goto IL_02b7;
		}
	}
	{
		IntPtr_t L_89;
		L_89.set_m_value_0((void*)(void*)JsonMapper_U3CRegisterBaseImportersU3Em__15_m2632743275_MethodInfo_var);
		ImporterFunc_t2138319818 * L_90 = (ImporterFunc_t2138319818 *)il2cpp_codegen_object_new(ImporterFunc_t2138319818_il2cpp_TypeInfo_var);
		ImporterFunc__ctor_m35661563(L_90, NULL, L_89, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache15_37(L_90);
	}

IL_02b7:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		ImporterFunc_t2138319818 * L_91 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache15_37();
		V_0 = L_91;
		Il2CppObject* L_92 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_base_importers_table_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_93 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_94 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(DateTime_t4283661327_0_0_0_var), /*hidden argument*/NULL);
		ImporterFunc_t2138319818 * L_95 = V_0;
		JsonMapper_RegisterImporter_m2108531963(NULL /*static, unused*/, L_92, L_93, L_94, L_95, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonMapper::RegisterImporter(System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>>,System.Type,System.Type,LitJson.ImporterFunc)
extern Il2CppClass* IDictionary_2_t1504911478_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t2243742303_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t1821615648_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2387401113_MethodInfo_var;
extern const uint32_t JsonMapper_RegisterImporter_m2108531963_MetadataUsageId;
extern "C"  void JsonMapper_RegisterImporter_m2108531963 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___table0, Type_t * ___json_type1, Type_t * ___value_type2, ImporterFunc_t2138319818 * ___importer3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_RegisterImporter_m2108531963_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___table0;
		Type_t * L_1 = ___json_type1;
		NullCheck(L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, Type_t * >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>>::ContainsKey(!0) */, IDictionary_2_t1504911478_il2cpp_TypeInfo_var, L_0, L_1);
		if (L_2)
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject* L_3 = ___table0;
		Type_t * L_4 = ___json_type1;
		Dictionary_2_t2243742303 * L_5 = (Dictionary_2_t2243742303 *)il2cpp_codegen_object_new(Dictionary_2_t2243742303_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2387401113(L_5, /*hidden argument*/Dictionary_2__ctor_m2387401113_MethodInfo_var);
		NullCheck(L_3);
		InterfaceActionInvoker2< Type_t *, Il2CppObject* >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>>::Add(!0,!1) */, IDictionary_2_t1504911478_il2cpp_TypeInfo_var, L_3, L_4, L_5);
	}

IL_0018:
	{
		Il2CppObject* L_6 = ___table0;
		Type_t * L_7 = ___json_type1;
		NullCheck(L_6);
		Il2CppObject* L_8 = InterfaceFuncInvoker1< Il2CppObject*, Type_t * >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>>::get_Item(!0) */, IDictionary_2_t1504911478_il2cpp_TypeInfo_var, L_6, L_7);
		Type_t * L_9 = ___value_type2;
		ImporterFunc_t2138319818 * L_10 = ___importer3;
		NullCheck(L_8);
		InterfaceActionInvoker2< Type_t *, ImporterFunc_t2138319818 * >::Invoke(3 /* System.Void System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>::set_Item(!0,!1) */, IDictionary_2_t1821615648_il2cpp_TypeInfo_var, L_8, L_9, L_10);
		return;
	}
}
// System.Void LitJson.JsonMapper::WriteValue(System.Object,LitJson.JsonWriter,System.Boolean,System.Int32)
extern const Il2CppType* Int64_t1153838595_0_0_0_var;
extern const Il2CppType* UInt32_t24667981_0_0_0_var;
extern const Il2CppType* UInt64_t24668076_0_0_0_var;
extern Il2CppClass* JsonMapper_t863513565_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonException_t3617621405_il2cpp_TypeInfo_var;
extern Il2CppClass* IJsonWrapper_t2026182966_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t1153838595_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppArray_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern Il2CppClass* IList_t1751339649_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_t3464557803_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_t537317817_il2cpp_TypeInfo_var;
extern Il2CppClass* DictionaryEntry_t1751606614_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t3013656303_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2862688501_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt64_t24668076_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t2149610353_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t3072580277_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t1683532369_il2cpp_TypeInfo_var;
extern Il2CppClass* FieldInfo_t_il2cpp_TypeInfo_var;
extern Il2CppClass* PropertyInfo_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral342420319;
extern const uint32_t JsonMapper_WriteValue_m1642136196_MetadataUsageId;
extern "C"  void JsonMapper_WriteValue_m1642136196 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, JsonWriter_t1165300239 * ___writer1, bool ___writer_is_private2, int32_t ___depth3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_WriteValue_m1642136196_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Il2CppObject * V_5 = NULL;
	DictionaryEntry_t1751606614  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Il2CppObject * V_7 = NULL;
	Il2CppObject * V_8 = NULL;
	Type_t * V_9 = NULL;
	ExporterFunc_t3330360473 * V_10 = NULL;
	ExporterFunc_t3330360473 * V_11 = NULL;
	Type_t * V_12 = NULL;
	Il2CppObject* V_13 = NULL;
	PropertyMetadata_t4066634616  V_14;
	memset(&V_14, 0, sizeof(V_14));
	Il2CppObject* V_15 = NULL;
	PropertyInfo_t * V_16 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___depth3;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		int32_t L_1 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_max_nesting_depth_0();
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			goto IL_0021;
		}
	}
	{
		Il2CppObject * L_2 = ___obj0;
		NullCheck(L_2);
		Type_t * L_3 = Object_GetType_m2022236990(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral342420319, L_3, /*hidden argument*/NULL);
		JsonException_t3617621405 * L_5 = (JsonException_t3617621405 *)il2cpp_codegen_object_new(JsonException_t3617621405_il2cpp_TypeInfo_var);
		JsonException__ctor_m2918697824(L_5, L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0021:
	{
		Il2CppObject * L_6 = ___obj0;
		if (L_6)
		{
			goto IL_002f;
		}
	}
	{
		JsonWriter_t1165300239 * L_7 = ___writer1;
		NullCheck(L_7);
		JsonWriter_Write_m1040069059(L_7, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}

IL_002f:
	{
		Il2CppObject * L_8 = ___obj0;
		if (!((Il2CppObject *)IsInst(L_8, IJsonWrapper_t2026182966_il2cpp_TypeInfo_var)))
		{
			goto IL_0068;
		}
	}
	{
		bool L_9 = ___writer_is_private2;
		if (!L_9)
		{
			goto IL_005b;
		}
	}
	{
		JsonWriter_t1165300239 * L_10 = ___writer1;
		NullCheck(L_10);
		TextWriter_t2304124208 * L_11 = JsonWriter_get_TextWriter_m496252161(L_10, /*hidden argument*/NULL);
		Il2CppObject * L_12 = ___obj0;
		NullCheck(((Il2CppObject *)Castclass(L_12, IJsonWrapper_t2026182966_il2cpp_TypeInfo_var)));
		String_t* L_13 = InterfaceFuncInvoker0< String_t* >::Invoke(18 /* System.String LitJson.IJsonWrapper::ToJson() */, IJsonWrapper_t2026182966_il2cpp_TypeInfo_var, ((Il2CppObject *)Castclass(L_12, IJsonWrapper_t2026182966_il2cpp_TypeInfo_var)));
		NullCheck(L_11);
		VirtActionInvoker1< String_t* >::Invoke(10 /* System.Void System.IO.TextWriter::Write(System.String) */, L_11, L_13);
		goto IL_0067;
	}

IL_005b:
	{
		Il2CppObject * L_14 = ___obj0;
		JsonWriter_t1165300239 * L_15 = ___writer1;
		NullCheck(((Il2CppObject *)Castclass(L_14, IJsonWrapper_t2026182966_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker1< JsonWriter_t1165300239 * >::Invoke(19 /* System.Void LitJson.IJsonWrapper::ToJson(LitJson.JsonWriter) */, IJsonWrapper_t2026182966_il2cpp_TypeInfo_var, ((Il2CppObject *)Castclass(L_14, IJsonWrapper_t2026182966_il2cpp_TypeInfo_var)), L_15);
	}

IL_0067:
	{
		return;
	}

IL_0068:
	{
		Il2CppObject * L_16 = ___obj0;
		if (!((String_t*)IsInstSealed(L_16, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_0080;
		}
	}
	{
		JsonWriter_t1165300239 * L_17 = ___writer1;
		Il2CppObject * L_18 = ___obj0;
		NullCheck(L_17);
		JsonWriter_Write_m1040069059(L_17, ((String_t*)CastclassSealed(L_18, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return;
	}

IL_0080:
	{
		Il2CppObject * L_19 = ___obj0;
		if (!((Il2CppObject *)IsInstSealed(L_19, Double_t3868226565_il2cpp_TypeInfo_var)))
		{
			goto IL_0098;
		}
	}
	{
		JsonWriter_t1165300239 * L_20 = ___writer1;
		Il2CppObject * L_21 = ___obj0;
		NullCheck(L_20);
		JsonWriter_Write_m471830019(L_20, ((*(double*)((double*)UnBox (L_21, Double_t3868226565_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return;
	}

IL_0098:
	{
		Il2CppObject * L_22 = ___obj0;
		if (!((Il2CppObject *)IsInstSealed(L_22, Int32_t1153838500_il2cpp_TypeInfo_var)))
		{
			goto IL_00b0;
		}
	}
	{
		JsonWriter_t1165300239 * L_23 = ___writer1;
		Il2CppObject * L_24 = ___obj0;
		NullCheck(L_23);
		JsonWriter_Write_m295913072(L_23, ((*(int32_t*)((int32_t*)UnBox (L_24, Int32_t1153838500_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return;
	}

IL_00b0:
	{
		Il2CppObject * L_25 = ___obj0;
		if (!((Il2CppObject *)IsInstSealed(L_25, Boolean_t476798718_il2cpp_TypeInfo_var)))
		{
			goto IL_00c8;
		}
	}
	{
		JsonWriter_t1165300239 * L_26 = ___writer1;
		Il2CppObject * L_27 = ___obj0;
		NullCheck(L_26);
		JsonWriter_Write_m2388425430(L_26, ((*(bool*)((bool*)UnBox (L_27, Boolean_t476798718_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return;
	}

IL_00c8:
	{
		Il2CppObject * L_28 = ___obj0;
		if (!((Il2CppObject *)IsInstSealed(L_28, Int64_t1153838595_il2cpp_TypeInfo_var)))
		{
			goto IL_00e0;
		}
	}
	{
		JsonWriter_t1165300239 * L_29 = ___writer1;
		Il2CppObject * L_30 = ___obj0;
		NullCheck(L_29);
		JsonWriter_Write_m295916017(L_29, ((*(int64_t*)((int64_t*)UnBox (L_30, Int64_t1153838595_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return;
	}

IL_00e0:
	{
		Il2CppObject * L_31 = ___obj0;
		if (!((Il2CppArray *)IsInstClass(L_31, Il2CppArray_il2cpp_TypeInfo_var)))
		{
			goto IL_013f;
		}
	}
	{
		JsonWriter_t1165300239 * L_32 = ___writer1;
		NullCheck(L_32);
		JsonWriter_WriteArrayStart_m721019272(L_32, /*hidden argument*/NULL);
		Il2CppObject * L_33 = ___obj0;
		NullCheck(((Il2CppArray *)CastclassClass(L_33, Il2CppArray_il2cpp_TypeInfo_var)));
		Il2CppObject * L_34 = Array_GetEnumerator_m2728734362(((Il2CppArray *)CastclassClass(L_33, Il2CppArray_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_1 = L_34;
	}

IL_00fd:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0114;
		}

IL_0102:
		{
			Il2CppObject * L_35 = V_1;
			NullCheck(L_35);
			Il2CppObject * L_36 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_35);
			V_0 = L_36;
			Il2CppObject * L_37 = V_0;
			JsonWriter_t1165300239 * L_38 = ___writer1;
			bool L_39 = ___writer_is_private2;
			int32_t L_40 = ___depth3;
			IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
			JsonMapper_WriteValue_m1642136196(NULL /*static, unused*/, L_37, L_38, L_39, ((int32_t)((int32_t)L_40+(int32_t)1)), /*hidden argument*/NULL);
		}

IL_0114:
		{
			Il2CppObject * L_41 = V_1;
			NullCheck(L_41);
			bool L_42 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_41);
			if (L_42)
			{
				goto IL_0102;
			}
		}

IL_011f:
		{
			IL2CPP_LEAVE(0x138, FINALLY_0124);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0124;
	}

FINALLY_0124:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_43 = V_1;
			Il2CppObject * L_44 = ((Il2CppObject *)IsInst(L_43, IDisposable_t1423340799_il2cpp_TypeInfo_var));
			V_2 = L_44;
			if (!L_44)
			{
				goto IL_0137;
			}
		}

IL_0131:
		{
			Il2CppObject * L_45 = V_2;
			NullCheck(L_45);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_45);
		}

IL_0137:
		{
			IL2CPP_END_FINALLY(292)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(292)
	{
		IL2CPP_JUMP_TBL(0x138, IL_0138)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0138:
	{
		JsonWriter_t1165300239 * L_46 = ___writer1;
		NullCheck(L_46);
		JsonWriter_WriteArrayEnd_m3594342657(L_46, /*hidden argument*/NULL);
		return;
	}

IL_013f:
	{
		Il2CppObject * L_47 = ___obj0;
		if (!((Il2CppObject *)IsInst(L_47, IList_t1751339649_il2cpp_TypeInfo_var)))
		{
			goto IL_01a4;
		}
	}
	{
		JsonWriter_t1165300239 * L_48 = ___writer1;
		NullCheck(L_48);
		JsonWriter_WriteArrayStart_m721019272(L_48, /*hidden argument*/NULL);
		Il2CppObject * L_49 = ___obj0;
		NullCheck(((Il2CppObject *)Castclass(L_49, IList_t1751339649_il2cpp_TypeInfo_var)));
		Il2CppObject * L_50 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t3464557803_il2cpp_TypeInfo_var, ((Il2CppObject *)Castclass(L_49, IList_t1751339649_il2cpp_TypeInfo_var)));
		V_4 = L_50;
	}

IL_015d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0175;
		}

IL_0162:
		{
			Il2CppObject * L_51 = V_4;
			NullCheck(L_51);
			Il2CppObject * L_52 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_51);
			V_3 = L_52;
			Il2CppObject * L_53 = V_3;
			JsonWriter_t1165300239 * L_54 = ___writer1;
			bool L_55 = ___writer_is_private2;
			int32_t L_56 = ___depth3;
			IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
			JsonMapper_WriteValue_m1642136196(NULL /*static, unused*/, L_53, L_54, L_55, ((int32_t)((int32_t)L_56+(int32_t)1)), /*hidden argument*/NULL);
		}

IL_0175:
		{
			Il2CppObject * L_57 = V_4;
			NullCheck(L_57);
			bool L_58 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_57);
			if (L_58)
			{
				goto IL_0162;
			}
		}

IL_0181:
		{
			IL2CPP_LEAVE(0x19D, FINALLY_0186);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0186;
	}

FINALLY_0186:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_59 = V_4;
			Il2CppObject * L_60 = ((Il2CppObject *)IsInst(L_59, IDisposable_t1423340799_il2cpp_TypeInfo_var));
			V_5 = L_60;
			if (!L_60)
			{
				goto IL_019c;
			}
		}

IL_0195:
		{
			Il2CppObject * L_61 = V_5;
			NullCheck(L_61);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_61);
		}

IL_019c:
		{
			IL2CPP_END_FINALLY(390)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(390)
	{
		IL2CPP_JUMP_TBL(0x19D, IL_019d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_019d:
	{
		JsonWriter_t1165300239 * L_62 = ___writer1;
		NullCheck(L_62);
		JsonWriter_WriteArrayEnd_m3594342657(L_62, /*hidden argument*/NULL);
		return;
	}

IL_01a4:
	{
		Il2CppObject * L_63 = ___obj0;
		if (!((Il2CppObject *)IsInst(L_63, IDictionary_t537317817_il2cpp_TypeInfo_var)))
		{
			goto IL_0227;
		}
	}
	{
		JsonWriter_t1165300239 * L_64 = ___writer1;
		NullCheck(L_64);
		JsonWriter_WriteObjectStart_m3796293414(L_64, /*hidden argument*/NULL);
		Il2CppObject * L_65 = ___obj0;
		NullCheck(((Il2CppObject *)Castclass(L_65, IDictionary_t537317817_il2cpp_TypeInfo_var)));
		Il2CppObject * L_66 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(3 /* System.Collections.IDictionaryEnumerator System.Collections.IDictionary::GetEnumerator() */, IDictionary_t537317817_il2cpp_TypeInfo_var, ((Il2CppObject *)Castclass(L_65, IDictionary_t537317817_il2cpp_TypeInfo_var)));
		V_7 = L_66;
	}

IL_01c2:
	try
	{ // begin try (depth: 1)
		{
			goto IL_01f8;
		}

IL_01c7:
		{
			Il2CppObject * L_67 = V_7;
			NullCheck(L_67);
			Il2CppObject * L_68 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_67);
			V_6 = ((*(DictionaryEntry_t1751606614 *)((DictionaryEntry_t1751606614 *)UnBox (L_68, DictionaryEntry_t1751606614_il2cpp_TypeInfo_var))));
			JsonWriter_t1165300239 * L_69 = ___writer1;
			Il2CppObject * L_70 = DictionaryEntry_get_Key_m3516209325((&V_6), /*hidden argument*/NULL);
			NullCheck(L_69);
			JsonWriter_WritePropertyName_m1552473251(L_69, ((String_t*)CastclassSealed(L_70, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
			Il2CppObject * L_71 = DictionaryEntry_get_Value_m4281303039((&V_6), /*hidden argument*/NULL);
			JsonWriter_t1165300239 * L_72 = ___writer1;
			bool L_73 = ___writer_is_private2;
			int32_t L_74 = ___depth3;
			IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
			JsonMapper_WriteValue_m1642136196(NULL /*static, unused*/, L_71, L_72, L_73, ((int32_t)((int32_t)L_74+(int32_t)1)), /*hidden argument*/NULL);
		}

IL_01f8:
		{
			Il2CppObject * L_75 = V_7;
			NullCheck(L_75);
			bool L_76 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_75);
			if (L_76)
			{
				goto IL_01c7;
			}
		}

IL_0204:
		{
			IL2CPP_LEAVE(0x220, FINALLY_0209);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0209;
	}

FINALLY_0209:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_77 = V_7;
			Il2CppObject * L_78 = ((Il2CppObject *)IsInst(L_77, IDisposable_t1423340799_il2cpp_TypeInfo_var));
			V_8 = L_78;
			if (!L_78)
			{
				goto IL_021f;
			}
		}

IL_0218:
		{
			Il2CppObject * L_79 = V_8;
			NullCheck(L_79);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_79);
		}

IL_021f:
		{
			IL2CPP_END_FINALLY(521)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(521)
	{
		IL2CPP_JUMP_TBL(0x220, IL_0220)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0220:
	{
		JsonWriter_t1165300239 * L_80 = ___writer1;
		NullCheck(L_80);
		JsonWriter_WriteObjectEnd_m2431063583(L_80, /*hidden argument*/NULL);
		return;
	}

IL_0227:
	{
		Il2CppObject * L_81 = ___obj0;
		NullCheck(L_81);
		Type_t * L_82 = Object_GetType_m2022236990(L_81, /*hidden argument*/NULL);
		V_9 = L_82;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		Il2CppObject* L_83 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_custom_exporters_table_3();
		Type_t * L_84 = V_9;
		NullCheck(L_83);
		bool L_85 = InterfaceFuncInvoker1< bool, Type_t * >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Type,LitJson.ExporterFunc>::ContainsKey(!0) */, IDictionary_2_t3013656303_il2cpp_TypeInfo_var, L_83, L_84);
		if (!L_85)
		{
			goto IL_0258;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		Il2CppObject* L_86 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_custom_exporters_table_3();
		Type_t * L_87 = V_9;
		NullCheck(L_86);
		ExporterFunc_t3330360473 * L_88 = InterfaceFuncInvoker1< ExporterFunc_t3330360473 *, Type_t * >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Type,LitJson.ExporterFunc>::get_Item(!0) */, IDictionary_2_t3013656303_il2cpp_TypeInfo_var, L_86, L_87);
		V_10 = L_88;
		ExporterFunc_t3330360473 * L_89 = V_10;
		Il2CppObject * L_90 = ___obj0;
		JsonWriter_t1165300239 * L_91 = ___writer1;
		NullCheck(L_89);
		ExporterFunc_Invoke_m4274113196(L_89, L_90, L_91, /*hidden argument*/NULL);
		return;
	}

IL_0258:
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		Il2CppObject* L_92 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_base_exporters_table_2();
		Type_t * L_93 = V_9;
		NullCheck(L_92);
		bool L_94 = InterfaceFuncInvoker1< bool, Type_t * >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.Type,LitJson.ExporterFunc>::ContainsKey(!0) */, IDictionary_2_t3013656303_il2cpp_TypeInfo_var, L_92, L_93);
		if (!L_94)
		{
			goto IL_0281;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		Il2CppObject* L_95 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_base_exporters_table_2();
		Type_t * L_96 = V_9;
		NullCheck(L_95);
		ExporterFunc_t3330360473 * L_97 = InterfaceFuncInvoker1< ExporterFunc_t3330360473 *, Type_t * >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Type,LitJson.ExporterFunc>::get_Item(!0) */, IDictionary_2_t3013656303_il2cpp_TypeInfo_var, L_95, L_96);
		V_11 = L_97;
		ExporterFunc_t3330360473 * L_98 = V_11;
		Il2CppObject * L_99 = ___obj0;
		JsonWriter_t1165300239 * L_100 = ___writer1;
		NullCheck(L_98);
		ExporterFunc_Invoke_m4274113196(L_98, L_99, L_100, /*hidden argument*/NULL);
		return;
	}

IL_0281:
	{
		Il2CppObject * L_101 = ___obj0;
		if (!((Enum_t2862688501 *)IsInstClass(L_101, Enum_t2862688501_il2cpp_TypeInfo_var)))
		{
			goto IL_02e6;
		}
	}
	{
		Type_t * L_102 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2862688501_il2cpp_TypeInfo_var);
		Type_t * L_103 = Enum_GetUnderlyingType_m2468052512(NULL /*static, unused*/, L_102, /*hidden argument*/NULL);
		V_12 = L_103;
		Type_t * L_104 = V_12;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_105 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Int64_t1153838595_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_104) == ((Il2CppObject*)(Type_t *)L_105)))
		{
			goto IL_02c8;
		}
	}
	{
		Type_t * L_106 = V_12;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_107 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(UInt32_t24667981_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_106) == ((Il2CppObject*)(Type_t *)L_107)))
		{
			goto IL_02c8;
		}
	}
	{
		Type_t * L_108 = V_12;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_109 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(UInt64_t24668076_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_108) == ((Il2CppObject*)(Type_t *)L_109))))
		{
			goto IL_02d9;
		}
	}

IL_02c8:
	{
		JsonWriter_t1165300239 * L_110 = ___writer1;
		Il2CppObject * L_111 = ___obj0;
		NullCheck(L_110);
		JsonWriter_Write_m1580601148(L_110, ((*(uint64_t*)((uint64_t*)UnBox (L_111, UInt64_t24668076_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		goto IL_02e5;
	}

IL_02d9:
	{
		JsonWriter_t1165300239 * L_112 = ___writer1;
		Il2CppObject * L_113 = ___obj0;
		NullCheck(L_112);
		JsonWriter_Write_m295913072(L_112, ((*(int32_t*)((int32_t*)UnBox (L_113, Int32_t1153838500_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
	}

IL_02e5:
	{
		return;
	}

IL_02e6:
	{
		Type_t * L_114 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		JsonMapper_AddTypeProperties_m1596814329(NULL /*static, unused*/, L_114, /*hidden argument*/NULL);
		Il2CppObject* L_115 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_type_properties_12();
		Type_t * L_116 = V_9;
		NullCheck(L_115);
		Il2CppObject* L_117 = InterfaceFuncInvoker1< Il2CppObject*, Type_t * >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IList`1<LitJson.PropertyMetadata>>::get_Item(!0) */, IDictionary_2_t2149610353_il2cpp_TypeInfo_var, L_115, L_116);
		V_13 = L_117;
		JsonWriter_t1165300239 * L_118 = ___writer1;
		NullCheck(L_118);
		JsonWriter_WriteObjectStart_m3796293414(L_118, /*hidden argument*/NULL);
		Il2CppObject* L_119 = V_13;
		NullCheck(L_119);
		Il2CppObject* L_120 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<LitJson.PropertyMetadata>::GetEnumerator() */, IEnumerable_1_t3072580277_il2cpp_TypeInfo_var, L_119);
		V_15 = L_120;
	}

IL_030a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0396;
		}

IL_030f:
		{
			Il2CppObject* L_121 = V_15;
			NullCheck(L_121);
			PropertyMetadata_t4066634616  L_122 = InterfaceFuncInvoker0< PropertyMetadata_t4066634616  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<LitJson.PropertyMetadata>::get_Current() */, IEnumerator_1_t1683532369_il2cpp_TypeInfo_var, L_121);
			V_14 = L_122;
			bool L_123 = (&V_14)->get_IsField_1();
			if (!L_123)
			{
				goto IL_0357;
			}
		}

IL_0324:
		{
			JsonWriter_t1165300239 * L_124 = ___writer1;
			MemberInfo_t * L_125 = (&V_14)->get_Info_0();
			NullCheck(L_125);
			String_t* L_126 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_125);
			NullCheck(L_124);
			JsonWriter_WritePropertyName_m1552473251(L_124, L_126, /*hidden argument*/NULL);
			MemberInfo_t * L_127 = (&V_14)->get_Info_0();
			Il2CppObject * L_128 = ___obj0;
			NullCheck(((FieldInfo_t *)CastclassClass(L_127, FieldInfo_t_il2cpp_TypeInfo_var)));
			Il2CppObject * L_129 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(17 /* System.Object System.Reflection.FieldInfo::GetValue(System.Object) */, ((FieldInfo_t *)CastclassClass(L_127, FieldInfo_t_il2cpp_TypeInfo_var)), L_128);
			JsonWriter_t1165300239 * L_130 = ___writer1;
			bool L_131 = ___writer_is_private2;
			int32_t L_132 = ___depth3;
			IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
			JsonMapper_WriteValue_m1642136196(NULL /*static, unused*/, L_129, L_130, L_131, ((int32_t)((int32_t)L_132+(int32_t)1)), /*hidden argument*/NULL);
			goto IL_0396;
		}

IL_0357:
		{
			MemberInfo_t * L_133 = (&V_14)->get_Info_0();
			V_16 = ((PropertyInfo_t *)CastclassClass(L_133, PropertyInfo_t_il2cpp_TypeInfo_var));
			PropertyInfo_t * L_134 = V_16;
			NullCheck(L_134);
			bool L_135 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Reflection.PropertyInfo::get_CanRead() */, L_134);
			if (!L_135)
			{
				goto IL_0396;
			}
		}

IL_0371:
		{
			JsonWriter_t1165300239 * L_136 = ___writer1;
			MemberInfo_t * L_137 = (&V_14)->get_Info_0();
			NullCheck(L_137);
			String_t* L_138 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_137);
			NullCheck(L_136);
			JsonWriter_WritePropertyName_m1552473251(L_136, L_138, /*hidden argument*/NULL);
			PropertyInfo_t * L_139 = V_16;
			Il2CppObject * L_140 = ___obj0;
			NullCheck(L_139);
			Il2CppObject * L_141 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, ObjectU5BU5D_t1108656482* >::Invoke(22 /* System.Object System.Reflection.PropertyInfo::GetValue(System.Object,System.Object[]) */, L_139, L_140, (ObjectU5BU5D_t1108656482*)(ObjectU5BU5D_t1108656482*)NULL);
			JsonWriter_t1165300239 * L_142 = ___writer1;
			bool L_143 = ___writer_is_private2;
			int32_t L_144 = ___depth3;
			IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
			JsonMapper_WriteValue_m1642136196(NULL /*static, unused*/, L_141, L_142, L_143, ((int32_t)((int32_t)L_144+(int32_t)1)), /*hidden argument*/NULL);
		}

IL_0396:
		{
			Il2CppObject* L_145 = V_15;
			NullCheck(L_145);
			bool L_146 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_145);
			if (L_146)
			{
				goto IL_030f;
			}
		}

IL_03a2:
		{
			IL2CPP_LEAVE(0x3B6, FINALLY_03a7);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_03a7;
	}

FINALLY_03a7:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_147 = V_15;
			if (!L_147)
			{
				goto IL_03b5;
			}
		}

IL_03ae:
		{
			Il2CppObject* L_148 = V_15;
			NullCheck(L_148);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_148);
		}

IL_03b5:
		{
			IL2CPP_END_FINALLY(935)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(935)
	{
		IL2CPP_JUMP_TBL(0x3B6, IL_03b6)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_03b6:
	{
		JsonWriter_t1165300239 * L_149 = ___writer1;
		NullCheck(L_149);
		JsonWriter_WriteObjectEnd_m2431063583(L_149, /*hidden argument*/NULL);
		return;
	}
}
// System.String LitJson.JsonMapper::ToJson(System.Object)
extern Il2CppClass* JsonMapper_t863513565_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_ToJson_m2576187774_MetadataUsageId;
extern "C"  String_t* JsonMapper_ToJson_m2576187774 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_ToJson_m2576187774_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	String_t* V_1 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_static_writer_lock_15();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		JsonWriter_t1165300239 * L_2 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_static_writer_14();
		NullCheck(L_2);
		JsonWriter_Reset_m1994338575(L_2, /*hidden argument*/NULL);
		Il2CppObject * L_3 = ___obj0;
		JsonWriter_t1165300239 * L_4 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_static_writer_14();
		JsonMapper_WriteValue_m1642136196(NULL /*static, unused*/, L_3, L_4, (bool)1, 0, /*hidden argument*/NULL);
		JsonWriter_t1165300239 * L_5 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_static_writer_14();
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_5);
		V_1 = L_6;
		IL2CPP_LEAVE(0x3A, FINALLY_0033);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0033;
	}

FINALLY_0033:
	{ // begin finally (depth: 1)
		Il2CppObject * L_7 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(51)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(51)
	{
		IL2CPP_JUMP_TBL(0x3A, IL_003a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_003a:
	{
		String_t* L_8 = V_1;
		return L_8;
	}
}
// LitJson.IJsonWrapper LitJson.JsonMapper::ToWrapper(LitJson.WrapperFactory,LitJson.JsonReader)
extern Il2CppClass* JsonMapper_t863513565_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_ToWrapper_m3791715062_MetadataUsageId;
extern "C"  Il2CppObject * JsonMapper_ToWrapper_m3791715062 (Il2CppObject * __this /* static, unused */, WrapperFactory_t3264289803 * ___factory0, JsonReader_t1009895007 * ___reader1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_ToWrapper_m3791715062_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		WrapperFactory_t3264289803 * L_0 = ___factory0;
		JsonReader_t1009895007 * L_1 = ___reader1;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = JsonMapper_ReadValue_m3662287667(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// LitJson.IJsonWrapper LitJson.JsonMapper::<ReadSkip>m__0()
extern Il2CppClass* JsonMockWrapper_t721699511_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_U3CReadSkipU3Em__0_m2844748398_MetadataUsageId;
extern "C"  Il2CppObject * JsonMapper_U3CReadSkipU3Em__0_m2844748398 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CReadSkipU3Em__0_m2844748398_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		JsonMockWrapper_t721699511 * L_0 = (JsonMockWrapper_t721699511 *)il2cpp_codegen_object_new(JsonMockWrapper_t721699511_il2cpp_TypeInfo_var);
		JsonMockWrapper__ctor_m1481103688(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void LitJson.JsonMapper::<RegisterBaseExporters>m__1(System.Object,LitJson.JsonWriter)
extern Il2CppClass* Byte_t2862609660_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_U3CRegisterBaseExportersU3Em__1_m1072502522_MetadataUsageId;
extern "C"  void JsonMapper_U3CRegisterBaseExportersU3Em__1_m1072502522 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, JsonWriter_t1165300239 * ___writer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseExportersU3Em__1_m1072502522_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		JsonWriter_t1165300239 * L_0 = ___writer1;
		Il2CppObject * L_1 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		int32_t L_2 = Convert_ToInt32_m100433720(NULL /*static, unused*/, ((*(uint8_t*)((uint8_t*)UnBox (L_1, Byte_t2862609660_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		NullCheck(L_0);
		JsonWriter_Write_m295913072(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonMapper::<RegisterBaseExporters>m__2(System.Object,LitJson.JsonWriter)
extern Il2CppClass* Char_t2862622538_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_U3CRegisterBaseExportersU3Em__2_m2797983419_MetadataUsageId;
extern "C"  void JsonMapper_U3CRegisterBaseExportersU3Em__2_m2797983419 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, JsonWriter_t1165300239 * ___writer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseExportersU3Em__2_m2797983419_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		JsonWriter_t1165300239 * L_0 = ___writer1;
		Il2CppObject * L_1 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		String_t* L_2 = Convert_ToString_m3462155176(NULL /*static, unused*/, ((*(Il2CppChar*)((Il2CppChar*)UnBox (L_1, Char_t2862622538_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		NullCheck(L_0);
		JsonWriter_Write_m1040069059(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonMapper::<RegisterBaseExporters>m__3(System.Object,LitJson.JsonWriter)
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonMapper_t863513565_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_U3CRegisterBaseExportersU3Em__3_m228497020_MetadataUsageId;
extern "C"  void JsonMapper_U3CRegisterBaseExportersU3Em__3_m228497020 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, JsonWriter_t1165300239 * ___writer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseExportersU3Em__3_m228497020_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		JsonWriter_t1165300239 * L_0 = ___writer1;
		Il2CppObject * L_1 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_datetime_format_1();
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		String_t* L_3 = Convert_ToString_m3623023633(NULL /*static, unused*/, ((*(DateTime_t4283661327 *)((DateTime_t4283661327 *)UnBox (L_1, DateTime_t4283661327_il2cpp_TypeInfo_var)))), L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		JsonWriter_Write_m1040069059(L_0, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonMapper::<RegisterBaseExporters>m__4(System.Object,LitJson.JsonWriter)
extern Il2CppClass* Decimal_t1954350631_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_U3CRegisterBaseExportersU3Em__4_m1953977917_MetadataUsageId;
extern "C"  void JsonMapper_U3CRegisterBaseExportersU3Em__4_m1953977917 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, JsonWriter_t1165300239 * ___writer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseExportersU3Em__4_m1953977917_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		JsonWriter_t1165300239 * L_0 = ___writer1;
		Il2CppObject * L_1 = ___obj0;
		NullCheck(L_0);
		JsonWriter_Write_m947894477(L_0, ((*(Decimal_t1954350631 *)((Decimal_t1954350631 *)UnBox (L_1, Decimal_t1954350631_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonMapper::<RegisterBaseExporters>m__5(System.Object,LitJson.JsonWriter)
extern Il2CppClass* SByte_t1161769777_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_U3CRegisterBaseExportersU3Em__5_m3679458814_MetadataUsageId;
extern "C"  void JsonMapper_U3CRegisterBaseExportersU3Em__5_m3679458814 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, JsonWriter_t1165300239 * ___writer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseExportersU3Em__5_m3679458814_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		JsonWriter_t1165300239 * L_0 = ___writer1;
		Il2CppObject * L_1 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		int32_t L_2 = Convert_ToInt32_m3549512503(NULL /*static, unused*/, ((*(int8_t*)((int8_t*)UnBox (L_1, SByte_t1161769777_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		NullCheck(L_0);
		JsonWriter_Write_m295913072(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonMapper::<RegisterBaseExporters>m__6(System.Object,LitJson.JsonWriter)
extern Il2CppClass* Int16_t1153838442_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_U3CRegisterBaseExportersU3Em__6_m1109972415_MetadataUsageId;
extern "C"  void JsonMapper_U3CRegisterBaseExportersU3Em__6_m1109972415 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, JsonWriter_t1165300239 * ___writer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseExportersU3Em__6_m1109972415_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		JsonWriter_t1165300239 * L_0 = ___writer1;
		Il2CppObject * L_1 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		int32_t L_2 = Convert_ToInt32_m3303641118(NULL /*static, unused*/, ((*(int16_t*)((int16_t*)UnBox (L_1, Int16_t1153838442_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		NullCheck(L_0);
		JsonWriter_Write_m295913072(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonMapper::<RegisterBaseExporters>m__7(System.Object,LitJson.JsonWriter)
extern Il2CppClass* UInt16_t24667923_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_U3CRegisterBaseExportersU3Em__7_m2835453312_MetadataUsageId;
extern "C"  void JsonMapper_U3CRegisterBaseExportersU3Em__7_m2835453312 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, JsonWriter_t1165300239 * ___writer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseExportersU3Em__7_m2835453312_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		JsonWriter_t1165300239 * L_0 = ___writer1;
		Il2CppObject * L_1 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		int32_t L_2 = Convert_ToInt32_m330941057(NULL /*static, unused*/, ((*(uint16_t*)((uint16_t*)UnBox (L_1, UInt16_t24667923_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		NullCheck(L_0);
		JsonWriter_Write_m295913072(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonMapper::<RegisterBaseExporters>m__8(System.Object,LitJson.JsonWriter)
extern Il2CppClass* UInt32_t24667981_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_U3CRegisterBaseExportersU3Em__8_m265966913_MetadataUsageId;
extern "C"  void JsonMapper_U3CRegisterBaseExportersU3Em__8_m265966913 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, JsonWriter_t1165300239 * ___writer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseExportersU3Em__8_m265966913_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		JsonWriter_t1165300239 * L_0 = ___writer1;
		Il2CppObject * L_1 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		uint64_t L_2 = Convert_ToUInt64_m2850089253(NULL /*static, unused*/, ((*(uint32_t*)((uint32_t*)UnBox (L_1, UInt32_t24667981_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		NullCheck(L_0);
		JsonWriter_Write_m1580601148(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonMapper::<RegisterBaseExporters>m__9(System.Object,LitJson.JsonWriter)
extern Il2CppClass* UInt64_t24668076_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_U3CRegisterBaseExportersU3Em__9_m1991447810_MetadataUsageId;
extern "C"  void JsonMapper_U3CRegisterBaseExportersU3Em__9_m1991447810 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, JsonWriter_t1165300239 * ___writer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseExportersU3Em__9_m1991447810_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		JsonWriter_t1165300239 * L_0 = ___writer1;
		Il2CppObject * L_1 = ___obj0;
		NullCheck(L_0);
		JsonWriter_Write_m1580601148(L_0, ((*(uint64_t*)((uint64_t*)UnBox (L_1, UInt64_t24668076_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return;
	}
}
// System.Object LitJson.JsonMapper::<RegisterBaseImporters>m__A(System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern Il2CppClass* Byte_t2862609660_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_U3CRegisterBaseImportersU3Em__A_m976906870_MetadataUsageId;
extern "C"  Il2CppObject * JsonMapper_U3CRegisterBaseImportersU3Em__A_m976906870 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseImportersU3Em__A_m976906870_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		uint8_t L_1 = Convert_ToByte_m263200262(NULL /*static, unused*/, ((*(int32_t*)((int32_t*)UnBox (L_0, Int32_t1153838500_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		uint8_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Byte_t2862609660_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.Object LitJson.JsonMapper::<RegisterBaseImporters>m__B(System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt64_t24668076_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_U3CRegisterBaseImportersU3Em__B_m466372693_MetadataUsageId;
extern "C"  Il2CppObject * JsonMapper_U3CRegisterBaseImportersU3Em__B_m466372693 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseImportersU3Em__B_m466372693_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		uint64_t L_1 = Convert_ToUInt64_m2276527046(NULL /*static, unused*/, ((*(int32_t*)((int32_t*)UnBox (L_0, Int32_t1153838500_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		uint64_t L_2 = L_1;
		Il2CppObject * L_3 = Box(UInt64_t24668076_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.Object LitJson.JsonMapper::<RegisterBaseImporters>m__C(System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern Il2CppClass* SByte_t1161769777_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_U3CRegisterBaseImportersU3Em__C_m4250805812_MetadataUsageId;
extern "C"  Il2CppObject * JsonMapper_U3CRegisterBaseImportersU3Em__C_m4250805812 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseImportersU3Em__C_m4250805812_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		int8_t L_1 = Convert_ToSByte_m1554306494(NULL /*static, unused*/, ((*(int32_t*)((int32_t*)UnBox (L_0, Int32_t1153838500_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		int8_t L_2 = L_1;
		Il2CppObject * L_3 = Box(SByte_t1161769777_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.Object LitJson.JsonMapper::<RegisterBaseImporters>m__D(System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern Il2CppClass* Int16_t1153838442_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_U3CRegisterBaseImportersU3Em__D_m3740271635_MetadataUsageId;
extern "C"  Il2CppObject * JsonMapper_U3CRegisterBaseImportersU3Em__D_m3740271635 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseImportersU3Em__D_m3740271635_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		int16_t L_1 = Convert_ToInt16_m3741024176(NULL /*static, unused*/, ((*(int32_t*)((int32_t*)UnBox (L_0, Int32_t1153838500_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		int16_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int16_t1153838442_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.Object LitJson.JsonMapper::<RegisterBaseImporters>m__E(System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt16_t24667923_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_U3CRegisterBaseImportersU3Em__E_m3229737458_MetadataUsageId;
extern "C"  Il2CppObject * JsonMapper_U3CRegisterBaseImportersU3Em__E_m3229737458 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseImportersU3Em__E_m3229737458_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		uint16_t L_1 = Convert_ToUInt16_m3265893798(NULL /*static, unused*/, ((*(int32_t*)((int32_t*)UnBox (L_0, Int32_t1153838500_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		uint16_t L_2 = L_1;
		Il2CppObject * L_3 = Box(UInt16_t24667923_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.Object LitJson.JsonMapper::<RegisterBaseImporters>m__F(System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t24667981_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_U3CRegisterBaseImportersU3Em__F_m2719203281_MetadataUsageId;
extern "C"  Il2CppObject * JsonMapper_U3CRegisterBaseImportersU3Em__F_m2719203281 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseImportersU3Em__F_m2719203281_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		uint32_t L_1 = Convert_ToUInt32_m3676846822(NULL /*static, unused*/, ((*(int32_t*)((int32_t*)UnBox (L_0, Int32_t1153838500_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		uint32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(UInt32_t24667981_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.Object LitJson.JsonMapper::<RegisterBaseImporters>m__10(System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_U3CRegisterBaseImportersU3Em__10_m890446864_MetadataUsageId;
extern "C"  Il2CppObject * JsonMapper_U3CRegisterBaseImportersU3Em__10_m890446864 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseImportersU3Em__10_m890446864_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		float L_1 = Convert_ToSingle_m998872518(NULL /*static, unused*/, ((*(int32_t*)((int32_t*)UnBox (L_0, Int32_t1153838500_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.Object LitJson.JsonMapper::<RegisterBaseImporters>m__11(System.Object)
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_U3CRegisterBaseImportersU3Em__11_m379912687_MetadataUsageId;
extern "C"  Il2CppObject * JsonMapper_U3CRegisterBaseImportersU3Em__11_m379912687 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseImportersU3Em__11_m379912687_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		double L_1 = Convert_ToDouble_m3920050662(NULL /*static, unused*/, ((*(int32_t*)((int32_t*)UnBox (L_0, Int32_t1153838500_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		double L_2 = L_1;
		Il2CppObject * L_3 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.Object LitJson.JsonMapper::<RegisterBaseImporters>m__12(System.Object)
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern Il2CppClass* Decimal_t1954350631_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_U3CRegisterBaseImportersU3Em__12_m4164345806_MetadataUsageId;
extern "C"  Il2CppObject * JsonMapper_U3CRegisterBaseImportersU3Em__12_m4164345806 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseImportersU3Em__12_m4164345806_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		Decimal_t1954350631  L_1 = Convert_ToDecimal_m6300041(NULL /*static, unused*/, ((*(double*)((double*)UnBox (L_0, Double_t3868226565_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		Decimal_t1954350631  L_2 = L_1;
		Il2CppObject * L_3 = Box(Decimal_t1954350631_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.Object LitJson.JsonMapper::<RegisterBaseImporters>m__13(System.Object)
extern Il2CppClass* Int64_t1153838595_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t24667981_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_U3CRegisterBaseImportersU3Em__13_m3653811629_MetadataUsageId;
extern "C"  Il2CppObject * JsonMapper_U3CRegisterBaseImportersU3Em__13_m3653811629 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseImportersU3Em__13_m3653811629_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		uint32_t L_1 = Convert_ToUInt32_m3676849767(NULL /*static, unused*/, ((*(int64_t*)((int64_t*)UnBox (L_0, Int64_t1153838595_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		uint32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(UInt32_t24667981_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.Object LitJson.JsonMapper::<RegisterBaseImporters>m__14(System.Object)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t2862622538_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_U3CRegisterBaseImportersU3Em__14_m3143277452_MetadataUsageId;
extern "C"  Il2CppObject * JsonMapper_U3CRegisterBaseImportersU3Em__14_m3143277452 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseImportersU3Em__14_m3143277452_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		Il2CppChar L_1 = Convert_ToChar_m2817096877(NULL /*static, unused*/, ((String_t*)CastclassSealed(L_0, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		Il2CppChar L_2 = L_1;
		Il2CppObject * L_3 = Box(Char_t2862622538_il2cpp_TypeInfo_var, &L_2);
		return L_3;
	}
}
// System.Object LitJson.JsonMapper::<RegisterBaseImporters>m__15(System.Object)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonMapper_t863513565_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern const uint32_t JsonMapper_U3CRegisterBaseImportersU3Em__15_m2632743275_MetadataUsageId;
extern "C"  Il2CppObject * JsonMapper_U3CRegisterBaseImportersU3Em__15_m2632743275 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonMapper_U3CRegisterBaseImportersU3Em__15_m2632743275_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonMapper_t863513565_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = ((JsonMapper_t863513565_StaticFields*)JsonMapper_t863513565_il2cpp_TypeInfo_var->static_fields)->get_datetime_format_1();
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		DateTime_t4283661327  L_2 = Convert_ToDateTime_m4273293575(NULL /*static, unused*/, ((String_t*)CastclassSealed(L_0, String_t_il2cpp_TypeInfo_var)), L_1, /*hidden argument*/NULL);
		DateTime_t4283661327  L_3 = L_2;
		Il2CppObject * L_4 = Box(DateTime_t4283661327_il2cpp_TypeInfo_var, &L_3);
		return L_4;
	}
}
// System.Boolean LitJson.JsonMockWrapper::System.Collections.IList.get_IsFixedSize()
extern "C"  bool JsonMockWrapper_System_Collections_IList_get_IsFixedSize_m833095657 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean LitJson.JsonMockWrapper::System.Collections.IList.get_IsReadOnly()
extern "C"  bool JsonMockWrapper_System_Collections_IList_get_IsReadOnly_m1866356208 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Object LitJson.JsonMockWrapper::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * JsonMockWrapper_System_Collections_IList_get_Item_m331917991 (JsonMockWrapper_t721699511 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		return NULL;
	}
}
// System.Void LitJson.JsonMockWrapper::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void JsonMockWrapper_System_Collections_IList_set_Item_m14162548 (JsonMockWrapper_t721699511 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Int32 LitJson.JsonMockWrapper::System.Collections.ICollection.get_Count()
extern "C"  int32_t JsonMockWrapper_System_Collections_ICollection_get_Count_m3269071315 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Boolean LitJson.JsonMockWrapper::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool JsonMockWrapper_System_Collections_ICollection_get_IsSynchronized_m167110690 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object LitJson.JsonMockWrapper::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * JsonMockWrapper_System_Collections_ICollection_get_SyncRoot_m3932193056 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method)
{
	{
		return NULL;
	}
}
// System.Object LitJson.JsonMockWrapper::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * JsonMockWrapper_System_Collections_IDictionary_get_Item_m3715834294 (JsonMockWrapper_t721699511 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	{
		return NULL;
	}
}
// System.Void LitJson.JsonMockWrapper::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void JsonMockWrapper_System_Collections_IDictionary_set_Item_m1813382373 (JsonMockWrapper_t721699511 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Boolean LitJson.JsonMockWrapper::get_IsArray()
extern "C"  bool JsonMockWrapper_get_IsArray_m4125479426 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.JsonMockWrapper::get_IsBoolean()
extern "C"  bool JsonMockWrapper_get_IsBoolean_m3815635473 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.JsonMockWrapper::get_IsDouble()
extern "C"  bool JsonMockWrapper_get_IsDouble_m1693245610 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.JsonMockWrapper::get_IsInt()
extern "C"  bool JsonMockWrapper_get_IsInt_m2880832536 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.JsonMockWrapper::get_IsLong()
extern "C"  bool JsonMockWrapper_get_IsLong_m3493155477 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.JsonMockWrapper::get_IsObject()
extern "C"  bool JsonMockWrapper_get_IsObject_m424273048 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.JsonMockWrapper::get_IsString()
extern "C"  bool JsonMockWrapper_get_IsString_m2128786666 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void LitJson.JsonMockWrapper::.ctor()
extern "C"  void JsonMockWrapper__ctor_m1481103688 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean LitJson.JsonMockWrapper::GetBoolean()
extern "C"  bool JsonMockWrapper_GetBoolean_m3692833272 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Double LitJson.JsonMockWrapper::GetDouble()
extern "C"  double JsonMockWrapper_GetDouble_m940097040 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method)
{
	{
		return (0.0);
	}
}
// System.Int32 LitJson.JsonMockWrapper::GetInt()
extern "C"  int32_t JsonMockWrapper_GetInt_m3085795109 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Int64 LitJson.JsonMockWrapper::GetLong()
extern "C"  int64_t JsonMockWrapper_GetLong_m1851991145 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method)
{
	{
		return (((int64_t)((int64_t)0)));
	}
}
// System.String LitJson.JsonMockWrapper::GetString()
extern Il2CppCodeGenString* _stringLiteral0;
extern const uint32_t JsonMockWrapper_GetString_m4133901840_MetadataUsageId;
extern "C"  String_t* JsonMockWrapper_GetString_m4133901840 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonMockWrapper_GetString_m4133901840_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral0;
	}
}
// System.Void LitJson.JsonMockWrapper::SetBoolean(System.Boolean)
extern "C"  void JsonMockWrapper_SetBoolean_m716015769 (JsonMockWrapper_t721699511 * __this, bool ___val0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void LitJson.JsonMockWrapper::SetDouble(System.Double)
extern "C"  void JsonMockWrapper_SetDouble_m244632041 (JsonMockWrapper_t721699511 * __this, double ___val0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void LitJson.JsonMockWrapper::SetInt(System.Int32)
extern "C"  void JsonMockWrapper_SetInt_m3295367418 (JsonMockWrapper_t721699511 * __this, int32_t ___val0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void LitJson.JsonMockWrapper::SetJsonType(LitJson.JsonType)
extern "C"  void JsonMockWrapper_SetJsonType_m320338713 (JsonMockWrapper_t721699511 * __this, int32_t ___type0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void LitJson.JsonMockWrapper::SetLong(System.Int64)
extern "C"  void JsonMockWrapper_SetLong_m3883225718 (JsonMockWrapper_t721699511 * __this, int64_t ___val0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void LitJson.JsonMockWrapper::SetString(System.String)
extern "C"  void JsonMockWrapper_SetString_m3473441129 (JsonMockWrapper_t721699511 * __this, String_t* ___val0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.String LitJson.JsonMockWrapper::ToJson()
extern Il2CppCodeGenString* _stringLiteral0;
extern const uint32_t JsonMockWrapper_ToJson_m2825825148_MetadataUsageId;
extern "C"  String_t* JsonMockWrapper_ToJson_m2825825148 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonMockWrapper_ToJson_m2825825148_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		return _stringLiteral0;
	}
}
// System.Void LitJson.JsonMockWrapper::ToJson(LitJson.JsonWriter)
extern "C"  void JsonMockWrapper_ToJson_m3238749269 (JsonMockWrapper_t721699511 * __this, JsonWriter_t1165300239 * ___writer0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Int32 LitJson.JsonMockWrapper::System.Collections.IList.Add(System.Object)
extern "C"  int32_t JsonMockWrapper_System_Collections_IList_Add_m2771630982 (JsonMockWrapper_t721699511 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Void LitJson.JsonMockWrapper::System.Collections.IList.Clear()
extern "C"  void JsonMockWrapper_System_Collections_IList_Clear_m2827871242 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Boolean LitJson.JsonMockWrapper::System.Collections.IList.Contains(System.Object)
extern "C"  bool JsonMockWrapper_System_Collections_IList_Contains_m3585512698 (JsonMockWrapper_t721699511 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Int32 LitJson.JsonMockWrapper::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t JsonMockWrapper_System_Collections_IList_IndexOf_m3581853406 (JsonMockWrapper_t721699511 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		return (-1);
	}
}
// System.Void LitJson.JsonMockWrapper::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void JsonMockWrapper_System_Collections_IList_Insert_m1974792669 (JsonMockWrapper_t721699511 * __this, int32_t ___i0, Il2CppObject * ___v1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void LitJson.JsonMockWrapper::System.Collections.IList.Remove(System.Object)
extern "C"  void JsonMockWrapper_System_Collections_IList_Remove_m1753871403 (JsonMockWrapper_t721699511 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void LitJson.JsonMockWrapper::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void JsonMockWrapper_System_Collections_IList_RemoveAt_m120794349 (JsonMockWrapper_t721699511 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void LitJson.JsonMockWrapper::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void JsonMockWrapper_System_Collections_ICollection_CopyTo_m2016273660 (JsonMockWrapper_t721699511 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Collections.IEnumerator LitJson.JsonMockWrapper::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * JsonMockWrapper_System_Collections_IEnumerable_GetEnumerator_m3438062999 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method)
{
	{
		return (Il2CppObject *)NULL;
	}
}
// System.Void LitJson.JsonMockWrapper::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void JsonMockWrapper_System_Collections_IDictionary_Add_m2671771212 (JsonMockWrapper_t721699511 * __this, Il2CppObject * ___k0, Il2CppObject * ___v1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void LitJson.JsonMockWrapper::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void JsonMockWrapper_System_Collections_IDictionary_Remove_m4285589795 (JsonMockWrapper_t721699511 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Collections.IDictionaryEnumerator LitJson.JsonMockWrapper::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * JsonMockWrapper_System_Collections_IDictionary_GetEnumerator_m2972871643 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method)
{
	{
		return (Il2CppObject *)NULL;
	}
}
// System.Collections.IDictionaryEnumerator LitJson.JsonMockWrapper::System.Collections.Specialized.IOrderedDictionary.GetEnumerator()
extern "C"  Il2CppObject * JsonMockWrapper_System_Collections_Specialized_IOrderedDictionary_GetEnumerator_m1245726173 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method)
{
	{
		return (Il2CppObject *)NULL;
	}
}
// System.Boolean LitJson.JsonReader::get_SkipNonMembers()
extern "C"  bool JsonReader_get_SkipNonMembers_m1256537244 (JsonReader_t1009895007 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_skip_non_members_12();
		return L_0;
	}
}
// LitJson.JsonToken LitJson.JsonReader::get_Token()
extern "C"  int32_t JsonReader_get_Token_m340664751 (JsonReader_t1009895007 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_token_14();
		return L_0;
	}
}
// System.Object LitJson.JsonReader::get_Value()
extern "C"  Il2CppObject * JsonReader_get_Value_m950067043 (JsonReader_t1009895007 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_token_value_13();
		return L_0;
	}
}
// System.Void LitJson.JsonReader::.cctor()
extern Il2CppClass* JsonReader_t1009895007_il2cpp_TypeInfo_var;
extern const uint32_t JsonReader__cctor_m1442341339_MetadataUsageId;
extern "C"  void JsonReader__cctor_m1442341339 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonReader__cctor_m1442341339_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		JsonReader_PopulateParseTable_m360057175(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonReader::.ctor(System.String)
extern Il2CppClass* StringReader_t4061477668_il2cpp_TypeInfo_var;
extern const uint32_t JsonReader__ctor_m2087739440_MetadataUsageId;
extern "C"  void JsonReader__ctor_m2087739440 (JsonReader_t1009895007 * __this, String_t* ___json_text0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonReader__ctor_m2087739440_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___json_text0;
		StringReader_t4061477668 * L_1 = (StringReader_t4061477668 *)il2cpp_codegen_object_new(StringReader_t4061477668_il2cpp_TypeInfo_var);
		StringReader__ctor_m1181104909(L_1, L_0, /*hidden argument*/NULL);
		JsonReader__ctor_m3780280964(__this, L_1, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonReader::.ctor(System.IO.TextReader,System.Boolean)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* Stack_1_t4252399424_il2cpp_TypeInfo_var;
extern Il2CppClass* Lexer_t3664372066_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1__ctor_m4178503087_MethodInfo_var;
extern const MethodInfo* Stack_1_Push_m2863445632_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3359987907;
extern const uint32_t JsonReader__ctor_m3780280964_MetadataUsageId;
extern "C"  void JsonReader__ctor_m3780280964 (JsonReader_t1009895007 * __this, TextReader_t2148718976 * ___reader0, bool ___owned1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonReader__ctor_m3780280964_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		TextReader_t2148718976 * L_0 = ___reader0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, _stringLiteral3359987907, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		__this->set_parser_in_string_7((bool)0);
		__this->set_parser_return_8((bool)0);
		__this->set_read_started_9((bool)0);
		Stack_1_t4252399424 * L_2 = (Stack_1_t4252399424 *)il2cpp_codegen_object_new(Stack_1_t4252399424_il2cpp_TypeInfo_var);
		Stack_1__ctor_m4178503087(L_2, /*hidden argument*/Stack_1__ctor_m4178503087_MethodInfo_var);
		__this->set_automaton_stack_1(L_2);
		Stack_1_t4252399424 * L_3 = __this->get_automaton_stack_1();
		NullCheck(L_3);
		Stack_1_Push_m2863445632(L_3, ((int32_t)65553), /*hidden argument*/Stack_1_Push_m2863445632_MethodInfo_var);
		Stack_1_t4252399424 * L_4 = __this->get_automaton_stack_1();
		NullCheck(L_4);
		Stack_1_Push_m2863445632(L_4, ((int32_t)65543), /*hidden argument*/Stack_1_Push_m2863445632_MethodInfo_var);
		TextReader_t2148718976 * L_5 = ___reader0;
		Lexer_t3664372066 * L_6 = (Lexer_t3664372066 *)il2cpp_codegen_object_new(Lexer_t3664372066_il2cpp_TypeInfo_var);
		Lexer__ctor_m1101987844(L_6, L_5, /*hidden argument*/NULL);
		__this->set_lexer_6(L_6);
		__this->set_end_of_input_5((bool)0);
		__this->set_end_of_json_4((bool)0);
		__this->set_skip_non_members_12((bool)1);
		TextReader_t2148718976 * L_7 = ___reader0;
		__this->set_reader_10(L_7);
		bool L_8 = ___owned1;
		__this->set_reader_is_owned_11(L_8);
		return;
	}
}
// System.Void LitJson.JsonReader::PopulateParseTable()
extern Il2CppClass* Dictionary_2_t2803247644_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonReader_t1009895007_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m453975534_MethodInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422____U24fieldU2D0_0_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422____U24fieldU2D1_1_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422____U24fieldU2D2_2_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422____U24fieldU2D3_3_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422____U24fieldU2D4_4_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422____U24fieldU2D5_5_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422____U24fieldU2D6_6_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422____U24fieldU2D7_7_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422____U24fieldU2D8_8_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422____U24fieldU2D9_9_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422____U24fieldU2DA_10_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422____U24fieldU2DB_11_FieldInfo_var;
extern const uint32_t JsonReader_PopulateParseTable_m360057175_MetadataUsageId;
extern "C"  void JsonReader_PopulateParseTable_m360057175 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonReader_PopulateParseTable_m360057175_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2803247644 * L_0 = (Dictionary_2_t2803247644 *)il2cpp_codegen_object_new(Dictionary_2_t2803247644_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m453975534(L_0, /*hidden argument*/Dictionary_2__ctor_m453975534_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(JsonReader_t1009895007_il2cpp_TypeInfo_var);
		((JsonReader_t1009895007_StaticFields*)JsonReader_t1009895007_il2cpp_TypeInfo_var->static_fields)->set_parse_table_0(L_0);
		JsonReader_TableAddRow_m123437648(NULL /*static, unused*/, ((int32_t)65548), /*hidden argument*/NULL);
		Int32U5BU5D_t3230847821* L_1 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)((int32_t)91));
		Int32U5BU5D_t3230847821* L_2 = L_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (int32_t)((int32_t)65549));
		JsonReader_TableAddCol_m3371401192(NULL /*static, unused*/, ((int32_t)65548), ((int32_t)91), L_2, /*hidden argument*/NULL);
		JsonReader_TableAddRow_m123437648(NULL /*static, unused*/, ((int32_t)65549), /*hidden argument*/NULL);
		Int32U5BU5D_t3230847821* L_3 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)3));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_3, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422____U24fieldU2D0_0_FieldInfo_var), /*hidden argument*/NULL);
		JsonReader_TableAddCol_m3371401192(NULL /*static, unused*/, ((int32_t)65549), ((int32_t)34), L_3, /*hidden argument*/NULL);
		Int32U5BU5D_t3230847821* L_4 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)3));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_4, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422____U24fieldU2D1_1_FieldInfo_var), /*hidden argument*/NULL);
		JsonReader_TableAddCol_m3371401192(NULL /*static, unused*/, ((int32_t)65549), ((int32_t)91), L_4, /*hidden argument*/NULL);
		Int32U5BU5D_t3230847821* L_5 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)((int32_t)93));
		JsonReader_TableAddCol_m3371401192(NULL /*static, unused*/, ((int32_t)65549), ((int32_t)93), L_5, /*hidden argument*/NULL);
		Int32U5BU5D_t3230847821* L_6 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)3));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_6, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422____U24fieldU2D2_2_FieldInfo_var), /*hidden argument*/NULL);
		JsonReader_TableAddCol_m3371401192(NULL /*static, unused*/, ((int32_t)65549), ((int32_t)123), L_6, /*hidden argument*/NULL);
		Int32U5BU5D_t3230847821* L_7 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)3));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_7, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422____U24fieldU2D3_3_FieldInfo_var), /*hidden argument*/NULL);
		JsonReader_TableAddCol_m3371401192(NULL /*static, unused*/, ((int32_t)65549), ((int32_t)65537), L_7, /*hidden argument*/NULL);
		Int32U5BU5D_t3230847821* L_8 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)3));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_8, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422____U24fieldU2D4_4_FieldInfo_var), /*hidden argument*/NULL);
		JsonReader_TableAddCol_m3371401192(NULL /*static, unused*/, ((int32_t)65549), ((int32_t)65538), L_8, /*hidden argument*/NULL);
		Int32U5BU5D_t3230847821* L_9 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)3));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_9, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422____U24fieldU2D5_5_FieldInfo_var), /*hidden argument*/NULL);
		JsonReader_TableAddCol_m3371401192(NULL /*static, unused*/, ((int32_t)65549), ((int32_t)65539), L_9, /*hidden argument*/NULL);
		Int32U5BU5D_t3230847821* L_10 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)3));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_10, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422____U24fieldU2D6_6_FieldInfo_var), /*hidden argument*/NULL);
		JsonReader_TableAddCol_m3371401192(NULL /*static, unused*/, ((int32_t)65549), ((int32_t)65540), L_10, /*hidden argument*/NULL);
		JsonReader_TableAddRow_m123437648(NULL /*static, unused*/, ((int32_t)65544), /*hidden argument*/NULL);
		Int32U5BU5D_t3230847821* L_11 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)((int32_t)123));
		Int32U5BU5D_t3230847821* L_12 = L_11;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 1);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(1), (int32_t)((int32_t)65545));
		JsonReader_TableAddCol_m3371401192(NULL /*static, unused*/, ((int32_t)65544), ((int32_t)123), L_12, /*hidden argument*/NULL);
		JsonReader_TableAddRow_m123437648(NULL /*static, unused*/, ((int32_t)65545), /*hidden argument*/NULL);
		Int32U5BU5D_t3230847821* L_13 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)3));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_13, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422____U24fieldU2D7_7_FieldInfo_var), /*hidden argument*/NULL);
		JsonReader_TableAddCol_m3371401192(NULL /*static, unused*/, ((int32_t)65545), ((int32_t)34), L_13, /*hidden argument*/NULL);
		Int32U5BU5D_t3230847821* L_14 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 0);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)((int32_t)125));
		JsonReader_TableAddCol_m3371401192(NULL /*static, unused*/, ((int32_t)65545), ((int32_t)125), L_14, /*hidden argument*/NULL);
		JsonReader_TableAddRow_m123437648(NULL /*static, unused*/, ((int32_t)65546), /*hidden argument*/NULL);
		Int32U5BU5D_t3230847821* L_15 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)3));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_15, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422____U24fieldU2D8_8_FieldInfo_var), /*hidden argument*/NULL);
		JsonReader_TableAddCol_m3371401192(NULL /*static, unused*/, ((int32_t)65546), ((int32_t)34), L_15, /*hidden argument*/NULL);
		JsonReader_TableAddRow_m123437648(NULL /*static, unused*/, ((int32_t)65547), /*hidden argument*/NULL);
		Int32U5BU5D_t3230847821* L_16 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)3));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_16, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422____U24fieldU2D9_9_FieldInfo_var), /*hidden argument*/NULL);
		JsonReader_TableAddCol_m3371401192(NULL /*static, unused*/, ((int32_t)65547), ((int32_t)44), L_16, /*hidden argument*/NULL);
		Int32U5BU5D_t3230847821* L_17 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 0);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)((int32_t)65554));
		JsonReader_TableAddCol_m3371401192(NULL /*static, unused*/, ((int32_t)65547), ((int32_t)125), L_17, /*hidden argument*/NULL);
		JsonReader_TableAddRow_m123437648(NULL /*static, unused*/, ((int32_t)65552), /*hidden argument*/NULL);
		Int32U5BU5D_t3230847821* L_18 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)3));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_18, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422____U24fieldU2DA_10_FieldInfo_var), /*hidden argument*/NULL);
		JsonReader_TableAddCol_m3371401192(NULL /*static, unused*/, ((int32_t)65552), ((int32_t)34), L_18, /*hidden argument*/NULL);
		JsonReader_TableAddRow_m123437648(NULL /*static, unused*/, ((int32_t)65543), /*hidden argument*/NULL);
		Int32U5BU5D_t3230847821* L_19 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 0);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)((int32_t)65548));
		JsonReader_TableAddCol_m3371401192(NULL /*static, unused*/, ((int32_t)65543), ((int32_t)91), L_19, /*hidden argument*/NULL);
		Int32U5BU5D_t3230847821* L_20 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 0);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)((int32_t)65544));
		JsonReader_TableAddCol_m3371401192(NULL /*static, unused*/, ((int32_t)65543), ((int32_t)123), L_20, /*hidden argument*/NULL);
		JsonReader_TableAddRow_m123437648(NULL /*static, unused*/, ((int32_t)65550), /*hidden argument*/NULL);
		Int32U5BU5D_t3230847821* L_21 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 0);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)((int32_t)65552));
		JsonReader_TableAddCol_m3371401192(NULL /*static, unused*/, ((int32_t)65550), ((int32_t)34), L_21, /*hidden argument*/NULL);
		Int32U5BU5D_t3230847821* L_22 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 0);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)((int32_t)65548));
		JsonReader_TableAddCol_m3371401192(NULL /*static, unused*/, ((int32_t)65550), ((int32_t)91), L_22, /*hidden argument*/NULL);
		Int32U5BU5D_t3230847821* L_23 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 0);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)((int32_t)65544));
		JsonReader_TableAddCol_m3371401192(NULL /*static, unused*/, ((int32_t)65550), ((int32_t)123), L_23, /*hidden argument*/NULL);
		Int32U5BU5D_t3230847821* L_24 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 0);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)((int32_t)65537));
		JsonReader_TableAddCol_m3371401192(NULL /*static, unused*/, ((int32_t)65550), ((int32_t)65537), L_24, /*hidden argument*/NULL);
		Int32U5BU5D_t3230847821* L_25 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 0);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)((int32_t)65538));
		JsonReader_TableAddCol_m3371401192(NULL /*static, unused*/, ((int32_t)65550), ((int32_t)65538), L_25, /*hidden argument*/NULL);
		Int32U5BU5D_t3230847821* L_26 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 0);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)((int32_t)65539));
		JsonReader_TableAddCol_m3371401192(NULL /*static, unused*/, ((int32_t)65550), ((int32_t)65539), L_26, /*hidden argument*/NULL);
		Int32U5BU5D_t3230847821* L_27 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, 0);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)((int32_t)65540));
		JsonReader_TableAddCol_m3371401192(NULL /*static, unused*/, ((int32_t)65550), ((int32_t)65540), L_27, /*hidden argument*/NULL);
		JsonReader_TableAddRow_m123437648(NULL /*static, unused*/, ((int32_t)65551), /*hidden argument*/NULL);
		Int32U5BU5D_t3230847821* L_28 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)3));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_28, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422____U24fieldU2DB_11_FieldInfo_var), /*hidden argument*/NULL);
		JsonReader_TableAddCol_m3371401192(NULL /*static, unused*/, ((int32_t)65551), ((int32_t)44), L_28, /*hidden argument*/NULL);
		Int32U5BU5D_t3230847821* L_29 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, 0);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)((int32_t)65554));
		JsonReader_TableAddCol_m3371401192(NULL /*static, unused*/, ((int32_t)65551), ((int32_t)93), L_29, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonReader::TableAddCol(LitJson.ParserToken,System.Int32,System.Int32[])
extern Il2CppClass* JsonReader_t1009895007_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t2381120989_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t2805984405_il2cpp_TypeInfo_var;
extern const uint32_t JsonReader_TableAddCol_m3371401192_MetadataUsageId;
extern "C"  void JsonReader_TableAddCol_m3371401192 (Il2CppObject * __this /* static, unused */, int32_t ___row0, int32_t ___col1, Int32U5BU5D_t3230847821* ___symbols2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonReader_TableAddCol_m3371401192_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonReader_t1009895007_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = ((JsonReader_t1009895007_StaticFields*)JsonReader_t1009895007_il2cpp_TypeInfo_var->static_fields)->get_parse_table_0();
		int32_t L_1 = ___row0;
		NullCheck(L_0);
		Il2CppObject* L_2 = InterfaceFuncInvoker1< Il2CppObject*, int32_t >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Int32,System.Collections.Generic.IDictionary`2<System.Int32,System.Int32[]>>::get_Item(!0) */, IDictionary_2_t2381120989_il2cpp_TypeInfo_var, L_0, L_1);
		int32_t L_3 = ___col1;
		Int32U5BU5D_t3230847821* L_4 = ___symbols2;
		NullCheck(L_2);
		InterfaceActionInvoker2< int32_t, Int32U5BU5D_t3230847821* >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.Int32,System.Int32[]>::Add(!0,!1) */, IDictionary_2_t2805984405_il2cpp_TypeInfo_var, L_2, L_3, L_4);
		return;
	}
}
// System.Void LitJson.JsonReader::TableAddRow(LitJson.ParserToken)
extern Il2CppClass* JsonReader_t1009895007_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t3228111060_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t2381120989_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m541413175_MethodInfo_var;
extern const uint32_t JsonReader_TableAddRow_m123437648_MetadataUsageId;
extern "C"  void JsonReader_TableAddRow_m123437648 (Il2CppObject * __this /* static, unused */, int32_t ___rule0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonReader_TableAddRow_m123437648_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(JsonReader_t1009895007_il2cpp_TypeInfo_var);
		Il2CppObject* L_0 = ((JsonReader_t1009895007_StaticFields*)JsonReader_t1009895007_il2cpp_TypeInfo_var->static_fields)->get_parse_table_0();
		int32_t L_1 = ___rule0;
		Dictionary_2_t3228111060 * L_2 = (Dictionary_2_t3228111060 *)il2cpp_codegen_object_new(Dictionary_2_t3228111060_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m541413175(L_2, /*hidden argument*/Dictionary_2__ctor_m541413175_MethodInfo_var);
		NullCheck(L_0);
		InterfaceActionInvoker2< int32_t, Il2CppObject* >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.Int32,System.Collections.Generic.IDictionary`2<System.Int32,System.Int32[]>>::Add(!0,!1) */, IDictionary_2_t2381120989_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return;
	}
}
// System.Void LitJson.JsonReader::ProcessNumber(System.String)
extern Il2CppClass* Double_t3868226565_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t1153838595_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt64_t24668076_il2cpp_TypeInfo_var;
extern const uint32_t JsonReader_ProcessNumber_m3141853498_MetadataUsageId;
extern "C"  void JsonReader_ProcessNumber_m3141853498 (JsonReader_t1009895007 * __this, String_t* ___number0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonReader_ProcessNumber_m3141853498_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	double V_0 = 0.0;
	int32_t V_1 = 0;
	int64_t V_2 = 0;
	uint64_t V_3 = 0;
	{
		String_t* L_0 = ___number0;
		NullCheck(L_0);
		int32_t L_1 = String_IndexOf_m2775210486(L_0, ((int32_t)46), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)(-1)))))
		{
			goto IL_002a;
		}
	}
	{
		String_t* L_2 = ___number0;
		NullCheck(L_2);
		int32_t L_3 = String_IndexOf_m2775210486(L_2, ((int32_t)101), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)(-1)))))
		{
			goto IL_002a;
		}
	}
	{
		String_t* L_4 = ___number0;
		NullCheck(L_4);
		int32_t L_5 = String_IndexOf_m2775210486(L_4, ((int32_t)69), /*hidden argument*/NULL);
		if ((((int32_t)L_5) == ((int32_t)(-1))))
		{
			goto IL_004b;
		}
	}

IL_002a:
	{
		String_t* L_6 = ___number0;
		bool L_7 = Double_TryParse_m2709942532(NULL /*static, unused*/, L_6, (&V_0), /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004b;
		}
	}
	{
		__this->set_token_14(8);
		double L_8 = V_0;
		double L_9 = L_8;
		Il2CppObject * L_10 = Box(Double_t3868226565_il2cpp_TypeInfo_var, &L_9);
		__this->set_token_value_13(L_10);
		return;
	}

IL_004b:
	{
		String_t* L_11 = ___number0;
		bool L_12 = Int32_TryParse_m695344220(NULL /*static, unused*/, L_11, (&V_1), /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_006c;
		}
	}
	{
		__this->set_token_14(6);
		int32_t L_13 = V_1;
		int32_t L_14 = L_13;
		Il2CppObject * L_15 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_14);
		__this->set_token_value_13(L_15);
		return;
	}

IL_006c:
	{
		String_t* L_16 = ___number0;
		bool L_17 = Int64_TryParse_m2106581948(NULL /*static, unused*/, L_16, (&V_2), /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_008d;
		}
	}
	{
		__this->set_token_14(7);
		int64_t L_18 = V_2;
		int64_t L_19 = L_18;
		Il2CppObject * L_20 = Box(Int64_t1153838595_il2cpp_TypeInfo_var, &L_19);
		__this->set_token_value_13(L_20);
		return;
	}

IL_008d:
	{
		String_t* L_21 = ___number0;
		bool L_22 = UInt64_TryParse_m1333073810(NULL /*static, unused*/, L_21, (&V_3), /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00ae;
		}
	}
	{
		__this->set_token_14(7);
		uint64_t L_23 = V_3;
		uint64_t L_24 = L_23;
		Il2CppObject * L_25 = Box(UInt64_t24668076_il2cpp_TypeInfo_var, &L_24);
		__this->set_token_value_13(L_25);
		return;
	}

IL_00ae:
	{
		__this->set_token_14(6);
		int32_t L_26 = 0;
		Il2CppObject * L_27 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_26);
		__this->set_token_value_13(L_27);
		return;
	}
}
// System.Void LitJson.JsonReader::ProcessSymbol()
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern const uint32_t JsonReader_ProcessSymbol_m3890791767_MetadataUsageId;
extern "C"  void JsonReader_ProcessSymbol_m3890791767 (JsonReader_t1009895007 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonReader_ProcessSymbol_m3890791767_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = __this->get_current_symbol_3();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)91)))))
		{
			goto IL_0020;
		}
	}
	{
		__this->set_token_14(4);
		__this->set_parser_return_8((bool)1);
		goto IL_01b8;
	}

IL_0020:
	{
		int32_t L_1 = __this->get_current_symbol_3();
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)93)))))
		{
			goto IL_0040;
		}
	}
	{
		__this->set_token_14(5);
		__this->set_parser_return_8((bool)1);
		goto IL_01b8;
	}

IL_0040:
	{
		int32_t L_2 = __this->get_current_symbol_3();
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)123)))))
		{
			goto IL_0060;
		}
	}
	{
		__this->set_token_14(1);
		__this->set_parser_return_8((bool)1);
		goto IL_01b8;
	}

IL_0060:
	{
		int32_t L_3 = __this->get_current_symbol_3();
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)125)))))
		{
			goto IL_0080;
		}
	}
	{
		__this->set_token_14(3);
		__this->set_parser_return_8((bool)1);
		goto IL_01b8;
	}

IL_0080:
	{
		int32_t L_4 = __this->get_current_symbol_3();
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_00ca;
		}
	}
	{
		bool L_5 = __this->get_parser_in_string_7();
		if (!L_5)
		{
			goto IL_00ab;
		}
	}
	{
		__this->set_parser_in_string_7((bool)0);
		__this->set_parser_return_8((bool)1);
		goto IL_00c5;
	}

IL_00ab:
	{
		int32_t L_6 = __this->get_token_14();
		if (L_6)
		{
			goto IL_00be;
		}
	}
	{
		__this->set_token_14(((int32_t)9));
	}

IL_00be:
	{
		__this->set_parser_in_string_7((bool)1);
	}

IL_00c5:
	{
		goto IL_01b8;
	}

IL_00ca:
	{
		int32_t L_7 = __this->get_current_symbol_3();
		if ((!(((uint32_t)L_7) == ((uint32_t)((int32_t)65541)))))
		{
			goto IL_00f0;
		}
	}
	{
		Lexer_t3664372066 * L_8 = __this->get_lexer_6();
		NullCheck(L_8);
		String_t* L_9 = Lexer_get_StringValue_m2250118517(L_8, /*hidden argument*/NULL);
		__this->set_token_value_13(L_9);
		goto IL_01b8;
	}

IL_00f0:
	{
		int32_t L_10 = __this->get_current_symbol_3();
		if ((!(((uint32_t)L_10) == ((uint32_t)((int32_t)65539)))))
		{
			goto IL_0120;
		}
	}
	{
		__this->set_token_14(((int32_t)10));
		bool L_11 = ((bool)0);
		Il2CppObject * L_12 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_11);
		__this->set_token_value_13(L_12);
		__this->set_parser_return_8((bool)1);
		goto IL_01b8;
	}

IL_0120:
	{
		int32_t L_13 = __this->get_current_symbol_3();
		if ((!(((uint32_t)L_13) == ((uint32_t)((int32_t)65540)))))
		{
			goto IL_0144;
		}
	}
	{
		__this->set_token_14(((int32_t)11));
		__this->set_parser_return_8((bool)1);
		goto IL_01b8;
	}

IL_0144:
	{
		int32_t L_14 = __this->get_current_symbol_3();
		if ((!(((uint32_t)L_14) == ((uint32_t)((int32_t)65537)))))
		{
			goto IL_0171;
		}
	}
	{
		Lexer_t3664372066 * L_15 = __this->get_lexer_6();
		NullCheck(L_15);
		String_t* L_16 = Lexer_get_StringValue_m2250118517(L_15, /*hidden argument*/NULL);
		JsonReader_ProcessNumber_m3141853498(__this, L_16, /*hidden argument*/NULL);
		__this->set_parser_return_8((bool)1);
		goto IL_01b8;
	}

IL_0171:
	{
		int32_t L_17 = __this->get_current_symbol_3();
		if ((!(((uint32_t)L_17) == ((uint32_t)((int32_t)65546)))))
		{
			goto IL_018d;
		}
	}
	{
		__this->set_token_14(2);
		goto IL_01b8;
	}

IL_018d:
	{
		int32_t L_18 = __this->get_current_symbol_3();
		if ((!(((uint32_t)L_18) == ((uint32_t)((int32_t)65538)))))
		{
			goto IL_01b8;
		}
	}
	{
		__this->set_token_14(((int32_t)10));
		bool L_19 = ((bool)1);
		Il2CppObject * L_20 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_19);
		__this->set_token_value_13(L_20);
		__this->set_parser_return_8((bool)1);
	}

IL_01b8:
	{
		return;
	}
}
// System.Boolean LitJson.JsonReader::ReadToken()
extern "C"  bool JsonReader_ReadToken_m1885543997 (JsonReader_t1009895007 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_end_of_input_5();
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Lexer_t3664372066 * L_1 = __this->get_lexer_6();
		NullCheck(L_1);
		Lexer_NextToken_m2325282711(L_1, /*hidden argument*/NULL);
		Lexer_t3664372066 * L_2 = __this->get_lexer_6();
		NullCheck(L_2);
		bool L_3 = Lexer_get_EndOfInput_m57994450(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0031;
		}
	}
	{
		JsonReader_Close_m1080201512(__this, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_0031:
	{
		Lexer_t3664372066 * L_4 = __this->get_lexer_6();
		NullCheck(L_4);
		int32_t L_5 = Lexer_get_Token_m3197663547(L_4, /*hidden argument*/NULL);
		__this->set_current_input_2(L_5);
		return (bool)1;
	}
}
// System.Void LitJson.JsonReader::Close()
extern "C"  void JsonReader_Close_m1080201512 (JsonReader_t1009895007 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_end_of_input_5();
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		__this->set_end_of_input_5((bool)1);
		__this->set_end_of_json_4((bool)1);
		bool L_1 = __this->get_reader_is_owned_11();
		if (!L_1)
		{
			goto IL_0030;
		}
	}
	{
		TextReader_t2148718976 * L_2 = __this->get_reader_10();
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(5 /* System.Void System.IO.TextReader::Close() */, L_2);
	}

IL_0030:
	{
		__this->set_reader_10((TextReader_t2148718976 *)NULL);
		return;
	}
}
// System.Boolean LitJson.JsonReader::Read()
extern Il2CppClass* JsonException_t3617621405_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonReader_t1009895007_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t2381120989_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t2805984405_il2cpp_TypeInfo_var;
extern Il2CppClass* KeyNotFoundException_t876339989_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1_Clear_m1584636378_MethodInfo_var;
extern const MethodInfo* Stack_1_Push_m2863445632_MethodInfo_var;
extern const MethodInfo* Stack_1_Peek_m639730496_MethodInfo_var;
extern const MethodInfo* Stack_1_Pop_m2930436846_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3055795879;
extern const uint32_t JsonReader_Read_m3716278654_MetadataUsageId;
extern "C"  bool JsonReader_Read_m3716278654 (JsonReader_t1009895007 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonReader_Read_m3716278654_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Int32U5BU5D_t3230847821* V_0 = NULL;
	KeyNotFoundException_t876339989 * V_1 = NULL;
	int32_t V_2 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = __this->get_end_of_input_5();
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		bool L_1 = __this->get_end_of_json_4();
		if (!L_1)
		{
			goto IL_004a;
		}
	}
	{
		__this->set_end_of_json_4((bool)0);
		Stack_1_t4252399424 * L_2 = __this->get_automaton_stack_1();
		NullCheck(L_2);
		Stack_1_Clear_m1584636378(L_2, /*hidden argument*/Stack_1_Clear_m1584636378_MethodInfo_var);
		Stack_1_t4252399424 * L_3 = __this->get_automaton_stack_1();
		NullCheck(L_3);
		Stack_1_Push_m2863445632(L_3, ((int32_t)65553), /*hidden argument*/Stack_1_Push_m2863445632_MethodInfo_var);
		Stack_1_t4252399424 * L_4 = __this->get_automaton_stack_1();
		NullCheck(L_4);
		Stack_1_Push_m2863445632(L_4, ((int32_t)65543), /*hidden argument*/Stack_1_Push_m2863445632_MethodInfo_var);
	}

IL_004a:
	{
		__this->set_parser_in_string_7((bool)0);
		__this->set_parser_return_8((bool)0);
		__this->set_token_14(0);
		__this->set_token_value_13(NULL);
		bool L_5 = __this->get_read_started_9();
		if (L_5)
		{
			goto IL_0085;
		}
	}
	{
		__this->set_read_started_9((bool)1);
		bool L_6 = JsonReader_ReadToken_m1885543997(__this, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0085;
		}
	}
	{
		return (bool)0;
	}

IL_0085:
	{
		bool L_7 = __this->get_parser_return_8();
		if (!L_7)
		{
			goto IL_00ae;
		}
	}
	{
		Stack_1_t4252399424 * L_8 = __this->get_automaton_stack_1();
		NullCheck(L_8);
		int32_t L_9 = Stack_1_Peek_m639730496(L_8, /*hidden argument*/Stack_1_Peek_m639730496_MethodInfo_var);
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)65553)))))
		{
			goto IL_00ac;
		}
	}
	{
		__this->set_end_of_json_4((bool)1);
	}

IL_00ac:
	{
		return (bool)1;
	}

IL_00ae:
	{
		Stack_1_t4252399424 * L_10 = __this->get_automaton_stack_1();
		NullCheck(L_10);
		int32_t L_11 = Stack_1_Pop_m2930436846(L_10, /*hidden argument*/Stack_1_Pop_m2930436846_MethodInfo_var);
		__this->set_current_symbol_3(L_11);
		JsonReader_ProcessSymbol_m3890791767(__this, /*hidden argument*/NULL);
		int32_t L_12 = __this->get_current_symbol_3();
		int32_t L_13 = __this->get_current_input_2();
		if ((!(((uint32_t)L_12) == ((uint32_t)L_13))))
		{
			goto IL_0115;
		}
	}
	{
		bool L_14 = JsonReader_ReadToken_m1885543997(__this, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_0110;
		}
	}
	{
		Stack_1_t4252399424 * L_15 = __this->get_automaton_stack_1();
		NullCheck(L_15);
		int32_t L_16 = Stack_1_Peek_m639730496(L_15, /*hidden argument*/Stack_1_Peek_m639730496_MethodInfo_var);
		if ((((int32_t)L_16) == ((int32_t)((int32_t)65553))))
		{
			goto IL_0101;
		}
	}
	{
		JsonException_t3617621405 * L_17 = (JsonException_t3617621405 *)il2cpp_codegen_object_new(JsonException_t3617621405_il2cpp_TypeInfo_var);
		JsonException__ctor_m2918697824(L_17, _stringLiteral3055795879, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_17);
	}

IL_0101:
	{
		bool L_18 = __this->get_parser_return_8();
		if (!L_18)
		{
			goto IL_010e;
		}
	}
	{
		return (bool)1;
	}

IL_010e:
	{
		return (bool)0;
	}

IL_0110:
	{
		goto IL_0085;
	}

IL_0115:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(JsonReader_t1009895007_il2cpp_TypeInfo_var);
		Il2CppObject* L_19 = ((JsonReader_t1009895007_StaticFields*)JsonReader_t1009895007_il2cpp_TypeInfo_var->static_fields)->get_parse_table_0();
		int32_t L_20 = __this->get_current_symbol_3();
		NullCheck(L_19);
		Il2CppObject* L_21 = InterfaceFuncInvoker1< Il2CppObject*, int32_t >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Int32,System.Collections.Generic.IDictionary`2<System.Int32,System.Int32[]>>::get_Item(!0) */, IDictionary_2_t2381120989_il2cpp_TypeInfo_var, L_19, L_20);
		int32_t L_22 = __this->get_current_input_2();
		NullCheck(L_21);
		Int32U5BU5D_t3230847821* L_23 = InterfaceFuncInvoker1< Int32U5BU5D_t3230847821*, int32_t >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Int32,System.Int32[]>::get_Item(!0) */, IDictionary_2_t2805984405_il2cpp_TypeInfo_var, L_21, L_22);
		V_0 = L_23;
		goto IL_0144;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (KeyNotFoundException_t876339989_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0136;
		throw e;
	}

CATCH_0136:
	{ // begin catch(System.Collections.Generic.KeyNotFoundException)
		V_1 = ((KeyNotFoundException_t876339989 *)__exception_local);
		int32_t L_24 = __this->get_current_input_2();
		KeyNotFoundException_t876339989 * L_25 = V_1;
		JsonException_t3617621405 * L_26 = (JsonException_t3617621405 *)il2cpp_codegen_object_new(JsonException_t3617621405_il2cpp_TypeInfo_var);
		JsonException__ctor_m1122554673(L_26, L_24, L_25, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_26);
	} // end catch (depth: 1)

IL_0144:
	{
		Int32U5BU5D_t3230847821* L_27 = V_0;
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, 0);
		int32_t L_28 = 0;
		int32_t L_29 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		if ((!(((uint32_t)L_29) == ((uint32_t)((int32_t)65554)))))
		{
			goto IL_0156;
		}
	}
	{
		goto IL_0085;
	}

IL_0156:
	{
		Int32U5BU5D_t3230847821* L_30 = V_0;
		NullCheck(L_30);
		V_2 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_30)->max_length))))-(int32_t)1));
		goto IL_0173;
	}

IL_0161:
	{
		Stack_1_t4252399424 * L_31 = __this->get_automaton_stack_1();
		Int32U5BU5D_t3230847821* L_32 = V_0;
		int32_t L_33 = V_2;
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, L_33);
		int32_t L_34 = L_33;
		int32_t L_35 = (L_32)->GetAt(static_cast<il2cpp_array_size_t>(L_34));
		NullCheck(L_31);
		Stack_1_Push_m2863445632(L_31, L_35, /*hidden argument*/Stack_1_Push_m2863445632_MethodInfo_var);
		int32_t L_36 = V_2;
		V_2 = ((int32_t)((int32_t)L_36-(int32_t)1));
	}

IL_0173:
	{
		int32_t L_37 = V_2;
		if ((((int32_t)L_37) >= ((int32_t)0)))
		{
			goto IL_0161;
		}
	}
	{
		goto IL_0085;
	}
}
// System.IO.TextWriter LitJson.JsonWriter::get_TextWriter()
extern "C"  TextWriter_t2304124208 * JsonWriter_get_TextWriter_m496252161 (JsonWriter_t1165300239 * __this, const MethodInfo* method)
{
	{
		TextWriter_t2304124208 * L_0 = __this->get_writer_10();
		return L_0;
	}
}
// System.Boolean LitJson.JsonWriter::get_Validate()
extern "C"  bool JsonWriter_get_Validate_m637559351 (JsonWriter_t1165300239 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_validate_9();
		return L_0;
	}
}
// System.Void LitJson.JsonWriter::set_Validate(System.Boolean)
extern "C"  void JsonWriter_set_Validate_m2559375852 (JsonWriter_t1165300239 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_validate_9(L_0);
		return;
	}
}
// System.Void LitJson.JsonWriter::.cctor()
extern Il2CppClass* NumberFormatInfo_t1637637232_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonWriter_t1165300239_il2cpp_TypeInfo_var;
extern const uint32_t JsonWriter__cctor_m1158992267_MetadataUsageId;
extern "C"  void JsonWriter__cctor_m1158992267 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter__cctor_m1158992267_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NumberFormatInfo_t1637637232_il2cpp_TypeInfo_var);
		NumberFormatInfo_t1637637232 * L_0 = NumberFormatInfo_get_InvariantInfo_m1900501910(NULL /*static, unused*/, /*hidden argument*/NULL);
		((JsonWriter_t1165300239_StaticFields*)JsonWriter_t1165300239_il2cpp_TypeInfo_var->static_fields)->set_number_format_0(L_0);
		return;
	}
}
// System.Void LitJson.JsonWriter::.ctor()
extern Il2CppClass* StringBuilder_t243639308_il2cpp_TypeInfo_var;
extern Il2CppClass* StringWriter_t4216882900_il2cpp_TypeInfo_var;
extern const uint32_t JsonWriter__ctor_m52938338_MetadataUsageId;
extern "C"  void JsonWriter__ctor_m52938338 (JsonWriter_t1165300239 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter__ctor_m52938338_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		StringBuilder_t243639308 * L_0 = (StringBuilder_t243639308 *)il2cpp_codegen_object_new(StringBuilder_t243639308_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_0, /*hidden argument*/NULL);
		__this->set_inst_string_builder_7(L_0);
		StringBuilder_t243639308 * L_1 = __this->get_inst_string_builder_7();
		StringWriter_t4216882900 * L_2 = (StringWriter_t4216882900 *)il2cpp_codegen_object_new(StringWriter_t4216882900_il2cpp_TypeInfo_var);
		StringWriter__ctor_m3379070757(L_2, L_1, /*hidden argument*/NULL);
		__this->set_writer_10(L_2);
		JsonWriter_Init_m784528306(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonWriter::.ctor(System.IO.TextWriter)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3515393139;
extern const uint32_t JsonWriter__ctor_m89997433_MetadataUsageId;
extern "C"  void JsonWriter__ctor_m89997433 (JsonWriter_t1165300239 * __this, TextWriter_t2304124208 * ___writer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter__ctor_m89997433_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		TextWriter_t2304124208 * L_0 = ___writer0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, _stringLiteral3515393139, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		TextWriter_t2304124208 * L_2 = ___writer0;
		__this->set_writer_10(L_2);
		JsonWriter_Init_m784528306(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonWriter::DoValidation(LitJson.Condition)
extern Il2CppClass* JsonException_t3617621405_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1235911913;
extern Il2CppCodeGenString* _stringLiteral2435487551;
extern Il2CppCodeGenString* _stringLiteral1896664905;
extern Il2CppCodeGenString* _stringLiteral2304872700;
extern Il2CppCodeGenString* _stringLiteral2436048026;
extern Il2CppCodeGenString* _stringLiteral2566629472;
extern const uint32_t JsonWriter_DoValidation_m1443221920_MetadataUsageId;
extern "C"  void JsonWriter_DoValidation_m1443221920 (JsonWriter_t1165300239 * __this, int32_t ___cond0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_DoValidation_m1443221920_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		WriterContext_t3060158226 * L_0 = __this->get_context_1();
		NullCheck(L_0);
		bool L_1 = L_0->get_ExpectingValue_3();
		if (L_1)
		{
			goto IL_0023;
		}
	}
	{
		WriterContext_t3060158226 * L_2 = __this->get_context_1();
		WriterContext_t3060158226 * L_3 = L_2;
		NullCheck(L_3);
		int32_t L_4 = L_3->get_Count_0();
		NullCheck(L_3);
		L_3->set_Count_0(((int32_t)((int32_t)L_4+(int32_t)1)));
	}

IL_0023:
	{
		bool L_5 = __this->get_validate_9();
		if (L_5)
		{
			goto IL_002f;
		}
	}
	{
		return;
	}

IL_002f:
	{
		bool L_6 = __this->get_has_reached_end_3();
		if (!L_6)
		{
			goto IL_0045;
		}
	}
	{
		JsonException_t3617621405 * L_7 = (JsonException_t3617621405 *)il2cpp_codegen_object_new(JsonException_t3617621405_il2cpp_TypeInfo_var);
		JsonException__ctor_m2918697824(L_7, _stringLiteral1235911913, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0045:
	{
		int32_t L_8 = ___cond0;
		if (L_8 == 0)
		{
			goto IL_0064;
		}
		if (L_8 == 1)
		{
			goto IL_0084;
		}
		if (L_8 == 2)
		{
			goto IL_00b4;
		}
		if (L_8 == 3)
		{
			goto IL_00e4;
		}
		if (L_8 == 4)
		{
			goto IL_0114;
		}
	}
	{
		goto IL_0154;
	}

IL_0064:
	{
		WriterContext_t3060158226 * L_9 = __this->get_context_1();
		NullCheck(L_9);
		bool L_10 = L_9->get_InArray_1();
		if (L_10)
		{
			goto IL_007f;
		}
	}
	{
		JsonException_t3617621405 * L_11 = (JsonException_t3617621405 *)il2cpp_codegen_object_new(JsonException_t3617621405_il2cpp_TypeInfo_var);
		JsonException__ctor_m2918697824(L_11, _stringLiteral2435487551, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}

IL_007f:
	{
		goto IL_0154;
	}

IL_0084:
	{
		WriterContext_t3060158226 * L_12 = __this->get_context_1();
		NullCheck(L_12);
		bool L_13 = L_12->get_InObject_2();
		if (!L_13)
		{
			goto IL_00a4;
		}
	}
	{
		WriterContext_t3060158226 * L_14 = __this->get_context_1();
		NullCheck(L_14);
		bool L_15 = L_14->get_ExpectingValue_3();
		if (!L_15)
		{
			goto IL_00af;
		}
	}

IL_00a4:
	{
		JsonException_t3617621405 * L_16 = (JsonException_t3617621405 *)il2cpp_codegen_object_new(JsonException_t3617621405_il2cpp_TypeInfo_var);
		JsonException__ctor_m2918697824(L_16, _stringLiteral1896664905, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
	}

IL_00af:
	{
		goto IL_0154;
	}

IL_00b4:
	{
		WriterContext_t3060158226 * L_17 = __this->get_context_1();
		NullCheck(L_17);
		bool L_18 = L_17->get_InObject_2();
		if (!L_18)
		{
			goto IL_00df;
		}
	}
	{
		WriterContext_t3060158226 * L_19 = __this->get_context_1();
		NullCheck(L_19);
		bool L_20 = L_19->get_ExpectingValue_3();
		if (L_20)
		{
			goto IL_00df;
		}
	}
	{
		JsonException_t3617621405 * L_21 = (JsonException_t3617621405 *)il2cpp_codegen_object_new(JsonException_t3617621405_il2cpp_TypeInfo_var);
		JsonException__ctor_m2918697824(L_21, _stringLiteral2304872700, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}

IL_00df:
	{
		goto IL_0154;
	}

IL_00e4:
	{
		WriterContext_t3060158226 * L_22 = __this->get_context_1();
		NullCheck(L_22);
		bool L_23 = L_22->get_InObject_2();
		if (!L_23)
		{
			goto IL_0104;
		}
	}
	{
		WriterContext_t3060158226 * L_24 = __this->get_context_1();
		NullCheck(L_24);
		bool L_25 = L_24->get_ExpectingValue_3();
		if (!L_25)
		{
			goto IL_010f;
		}
	}

IL_0104:
	{
		JsonException_t3617621405 * L_26 = (JsonException_t3617621405 *)il2cpp_codegen_object_new(JsonException_t3617621405_il2cpp_TypeInfo_var);
		JsonException__ctor_m2918697824(L_26, _stringLiteral2436048026, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_26);
	}

IL_010f:
	{
		goto IL_0154;
	}

IL_0114:
	{
		WriterContext_t3060158226 * L_27 = __this->get_context_1();
		NullCheck(L_27);
		bool L_28 = L_27->get_InArray_1();
		if (L_28)
		{
			goto IL_014f;
		}
	}
	{
		WriterContext_t3060158226 * L_29 = __this->get_context_1();
		NullCheck(L_29);
		bool L_30 = L_29->get_InObject_2();
		if (!L_30)
		{
			goto IL_0144;
		}
	}
	{
		WriterContext_t3060158226 * L_31 = __this->get_context_1();
		NullCheck(L_31);
		bool L_32 = L_31->get_ExpectingValue_3();
		if (L_32)
		{
			goto IL_014f;
		}
	}

IL_0144:
	{
		JsonException_t3617621405 * L_33 = (JsonException_t3617621405 *)il2cpp_codegen_object_new(JsonException_t3617621405_il2cpp_TypeInfo_var);
		JsonException__ctor_m2918697824(L_33, _stringLiteral2566629472, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_33);
	}

IL_014f:
	{
		goto IL_0154;
	}

IL_0154:
	{
		return;
	}
}
// System.Void LitJson.JsonWriter::Init()
extern Il2CppClass* CharU5BU5D_t3324145743_il2cpp_TypeInfo_var;
extern Il2CppClass* Stack_1_t1863751854_il2cpp_TypeInfo_var;
extern Il2CppClass* WriterContext_t3060158226_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1__ctor_m288009655_MethodInfo_var;
extern const MethodInfo* Stack_1_Push_m2517233528_MethodInfo_var;
extern const uint32_t JsonWriter_Init_m784528306_MetadataUsageId;
extern "C"  void JsonWriter_Init_m784528306 (JsonWriter_t1165300239 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Init_m784528306_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_has_reached_end_3((bool)0);
		__this->set_hex_seq_4(((CharU5BU5D_t3324145743*)SZArrayNew(CharU5BU5D_t3324145743_il2cpp_TypeInfo_var, (uint32_t)4)));
		__this->set_indentation_5(0);
		__this->set_indent_value_6(4);
		__this->set_pretty_print_8((bool)0);
		__this->set_validate_9((bool)1);
		Stack_1_t1863751854 * L_0 = (Stack_1_t1863751854 *)il2cpp_codegen_object_new(Stack_1_t1863751854_il2cpp_TypeInfo_var);
		Stack_1__ctor_m288009655(L_0, /*hidden argument*/Stack_1__ctor_m288009655_MethodInfo_var);
		__this->set_ctx_stack_2(L_0);
		WriterContext_t3060158226 * L_1 = (WriterContext_t3060158226 *)il2cpp_codegen_object_new(WriterContext_t3060158226_il2cpp_TypeInfo_var);
		WriterContext__ctor_m159390989(L_1, /*hidden argument*/NULL);
		__this->set_context_1(L_1);
		Stack_1_t1863751854 * L_2 = __this->get_ctx_stack_2();
		WriterContext_t3060158226 * L_3 = __this->get_context_1();
		NullCheck(L_2);
		Stack_1_Push_m2517233528(L_2, L_3, /*hidden argument*/Stack_1_Push_m2517233528_MethodInfo_var);
		return;
	}
}
// System.Void LitJson.JsonWriter::IntToHex(System.Int32,System.Char[])
extern "C"  void JsonWriter_IntToHex_m3949697945 (Il2CppObject * __this /* static, unused */, int32_t ___n0, CharU5BU5D_t3324145743* ___hex1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_1 = 0;
		goto IL_0039;
	}

IL_0007:
	{
		int32_t L_0 = ___n0;
		V_0 = ((int32_t)((int32_t)L_0%(int32_t)((int32_t)16)));
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) >= ((int32_t)((int32_t)10))))
		{
			goto IL_0023;
		}
	}
	{
		CharU5BU5D_t3324145743* L_2 = ___hex1;
		int32_t L_3 = V_1;
		int32_t L_4 = V_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, ((int32_t)((int32_t)3-(int32_t)L_3)));
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)3-(int32_t)L_3))), (Il2CppChar)(((int32_t)((uint16_t)((int32_t)((int32_t)((int32_t)48)+(int32_t)L_4))))));
		goto IL_0030;
	}

IL_0023:
	{
		CharU5BU5D_t3324145743* L_5 = ___hex1;
		int32_t L_6 = V_1;
		int32_t L_7 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, ((int32_t)((int32_t)3-(int32_t)L_6)));
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)3-(int32_t)L_6))), (Il2CppChar)(((int32_t)((uint16_t)((int32_t)((int32_t)((int32_t)65)+(int32_t)((int32_t)((int32_t)L_7-(int32_t)((int32_t)10)))))))));
	}

IL_0030:
	{
		int32_t L_8 = ___n0;
		___n0 = ((int32_t)((int32_t)L_8>>(int32_t)4));
		int32_t L_9 = V_1;
		V_1 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0039:
	{
		int32_t L_10 = V_1;
		if ((((int32_t)L_10) < ((int32_t)4)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void LitJson.JsonWriter::Indent()
extern "C"  void JsonWriter_Indent_m2157585422 (JsonWriter_t1165300239 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_pretty_print_8();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = __this->get_indentation_5();
		int32_t L_2 = __this->get_indent_value_6();
		__this->set_indentation_5(((int32_t)((int32_t)L_1+(int32_t)L_2)));
	}

IL_001e:
	{
		return;
	}
}
// System.Void LitJson.JsonWriter::Put(System.String)
extern "C"  void JsonWriter_Put_m1880069331 (JsonWriter_t1165300239 * __this, String_t* ___str0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get_pretty_print_8();
		if (!L_0)
		{
			goto IL_003f;
		}
	}
	{
		WriterContext_t3060158226 * L_1 = __this->get_context_1();
		NullCheck(L_1);
		bool L_2 = L_1->get_ExpectingValue_3();
		if (L_2)
		{
			goto IL_003f;
		}
	}
	{
		V_0 = 0;
		goto IL_0033;
	}

IL_0022:
	{
		TextWriter_t2304124208 * L_3 = __this->get_writer_10();
		NullCheck(L_3);
		VirtActionInvoker1< Il2CppChar >::Invoke(8 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_3, ((int32_t)32));
		int32_t L_4 = V_0;
		V_0 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_0033:
	{
		int32_t L_5 = V_0;
		int32_t L_6 = __this->get_indentation_5();
		if ((((int32_t)L_5) < ((int32_t)L_6)))
		{
			goto IL_0022;
		}
	}

IL_003f:
	{
		TextWriter_t2304124208 * L_7 = __this->get_writer_10();
		String_t* L_8 = ___str0;
		NullCheck(L_7);
		VirtActionInvoker1< String_t* >::Invoke(10 /* System.Void System.IO.TextWriter::Write(System.String) */, L_7, L_8);
		return;
	}
}
// System.Void LitJson.JsonWriter::PutNewline()
extern "C"  void JsonWriter_PutNewline_m1237710887 (JsonWriter_t1165300239 * __this, const MethodInfo* method)
{
	{
		JsonWriter_PutNewline_m2395463006(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonWriter::PutNewline(System.Boolean)
extern "C"  void JsonWriter_PutNewline_m2395463006 (JsonWriter_t1165300239 * __this, bool ___add_comma0, const MethodInfo* method)
{
	{
		bool L_0 = ___add_comma0;
		if (!L_0)
		{
			goto IL_0034;
		}
	}
	{
		WriterContext_t3060158226 * L_1 = __this->get_context_1();
		NullCheck(L_1);
		bool L_2 = L_1->get_ExpectingValue_3();
		if (L_2)
		{
			goto IL_0034;
		}
	}
	{
		WriterContext_t3060158226 * L_3 = __this->get_context_1();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_Count_0();
		if ((((int32_t)L_4) <= ((int32_t)1)))
		{
			goto IL_0034;
		}
	}
	{
		TextWriter_t2304124208 * L_5 = __this->get_writer_10();
		NullCheck(L_5);
		VirtActionInvoker1< Il2CppChar >::Invoke(8 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_5, ((int32_t)44));
	}

IL_0034:
	{
		bool L_6 = __this->get_pretty_print_8();
		if (!L_6)
		{
			goto IL_005c;
		}
	}
	{
		WriterContext_t3060158226 * L_7 = __this->get_context_1();
		NullCheck(L_7);
		bool L_8 = L_7->get_ExpectingValue_3();
		if (L_8)
		{
			goto IL_005c;
		}
	}
	{
		TextWriter_t2304124208 * L_9 = __this->get_writer_10();
		NullCheck(L_9);
		VirtActionInvoker1< Il2CppChar >::Invoke(8 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_9, ((int32_t)10));
	}

IL_005c:
	{
		return;
	}
}
// System.Void LitJson.JsonWriter::PutString(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonWriter_t1165300239_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2962;
extern Il2CppCodeGenString* _stringLiteral2966;
extern Il2CppCodeGenString* _stringLiteral2968;
extern Il2CppCodeGenString* _stringLiteral2954;
extern Il2CppCodeGenString* _stringLiteral2950;
extern Il2CppCodeGenString* _stringLiteral2969;
extern const uint32_t JsonWriter_PutString_m306582274_MetadataUsageId;
extern "C"  void JsonWriter_PutString_m306582274 (JsonWriter_t1165300239 * __this, String_t* ___str0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_PutString_m306582274_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Il2CppChar V_2 = 0x0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		JsonWriter_Put_m1880069331(__this, L_0, /*hidden argument*/NULL);
		TextWriter_t2304124208 * L_1 = __this->get_writer_10();
		NullCheck(L_1);
		VirtActionInvoker1< Il2CppChar >::Invoke(8 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_1, ((int32_t)34));
		String_t* L_2 = ___str0;
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m2979997331(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		V_1 = 0;
		goto IL_015a;
	}

IL_0026:
	{
		String_t* L_4 = ___str0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		Il2CppChar L_6 = String_get_Chars_m3015341861(L_4, L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		Il2CppChar L_7 = V_2;
		if (((int32_t)((int32_t)L_7-(int32_t)8)) == 0)
		{
			goto IL_00db;
		}
		if (((int32_t)((int32_t)L_7-(int32_t)8)) == 1)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_7-(int32_t)8)) == 2)
		{
			goto IL_0063;
		}
		if (((int32_t)((int32_t)L_7-(int32_t)8)) == 3)
		{
			goto IL_004e;
		}
		if (((int32_t)((int32_t)L_7-(int32_t)8)) == 4)
		{
			goto IL_00c6;
		}
		if (((int32_t)((int32_t)L_7-(int32_t)8)) == 5)
		{
			goto IL_0078;
		}
	}

IL_004e:
	{
		Il2CppChar L_8 = V_2;
		if ((((int32_t)L_8) == ((int32_t)((int32_t)34))))
		{
			goto IL_00a2;
		}
	}
	{
		Il2CppChar L_9 = V_2;
		if ((((int32_t)L_9) == ((int32_t)((int32_t)92))))
		{
			goto IL_00a2;
		}
	}
	{
		goto IL_00f0;
	}

IL_0063:
	{
		TextWriter_t2304124208 * L_10 = __this->get_writer_10();
		NullCheck(L_10);
		VirtActionInvoker1< String_t* >::Invoke(10 /* System.Void System.IO.TextWriter::Write(System.String) */, L_10, _stringLiteral2962);
		goto IL_0156;
	}

IL_0078:
	{
		TextWriter_t2304124208 * L_11 = __this->get_writer_10();
		NullCheck(L_11);
		VirtActionInvoker1< String_t* >::Invoke(10 /* System.Void System.IO.TextWriter::Write(System.String) */, L_11, _stringLiteral2966);
		goto IL_0156;
	}

IL_008d:
	{
		TextWriter_t2304124208 * L_12 = __this->get_writer_10();
		NullCheck(L_12);
		VirtActionInvoker1< String_t* >::Invoke(10 /* System.Void System.IO.TextWriter::Write(System.String) */, L_12, _stringLiteral2968);
		goto IL_0156;
	}

IL_00a2:
	{
		TextWriter_t2304124208 * L_13 = __this->get_writer_10();
		NullCheck(L_13);
		VirtActionInvoker1< Il2CppChar >::Invoke(8 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_13, ((int32_t)92));
		TextWriter_t2304124208 * L_14 = __this->get_writer_10();
		String_t* L_15 = ___str0;
		int32_t L_16 = V_1;
		NullCheck(L_15);
		Il2CppChar L_17 = String_get_Chars_m3015341861(L_15, L_16, /*hidden argument*/NULL);
		NullCheck(L_14);
		VirtActionInvoker1< Il2CppChar >::Invoke(8 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_14, L_17);
		goto IL_0156;
	}

IL_00c6:
	{
		TextWriter_t2304124208 * L_18 = __this->get_writer_10();
		NullCheck(L_18);
		VirtActionInvoker1< String_t* >::Invoke(10 /* System.Void System.IO.TextWriter::Write(System.String) */, L_18, _stringLiteral2954);
		goto IL_0156;
	}

IL_00db:
	{
		TextWriter_t2304124208 * L_19 = __this->get_writer_10();
		NullCheck(L_19);
		VirtActionInvoker1< String_t* >::Invoke(10 /* System.Void System.IO.TextWriter::Write(System.String) */, L_19, _stringLiteral2950);
		goto IL_0156;
	}

IL_00f0:
	{
		String_t* L_20 = ___str0;
		int32_t L_21 = V_1;
		NullCheck(L_20);
		Il2CppChar L_22 = String_get_Chars_m3015341861(L_20, L_21, /*hidden argument*/NULL);
		if ((((int32_t)L_22) < ((int32_t)((int32_t)32))))
		{
			goto IL_0123;
		}
	}
	{
		String_t* L_23 = ___str0;
		int32_t L_24 = V_1;
		NullCheck(L_23);
		Il2CppChar L_25 = String_get_Chars_m3015341861(L_23, L_24, /*hidden argument*/NULL);
		if ((((int32_t)L_25) > ((int32_t)((int32_t)126))))
		{
			goto IL_0123;
		}
	}
	{
		TextWriter_t2304124208 * L_26 = __this->get_writer_10();
		String_t* L_27 = ___str0;
		int32_t L_28 = V_1;
		NullCheck(L_27);
		Il2CppChar L_29 = String_get_Chars_m3015341861(L_27, L_28, /*hidden argument*/NULL);
		NullCheck(L_26);
		VirtActionInvoker1< Il2CppChar >::Invoke(8 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_26, L_29);
		goto IL_0156;
	}

IL_0123:
	{
		String_t* L_30 = ___str0;
		int32_t L_31 = V_1;
		NullCheck(L_30);
		Il2CppChar L_32 = String_get_Chars_m3015341861(L_30, L_31, /*hidden argument*/NULL);
		CharU5BU5D_t3324145743* L_33 = __this->get_hex_seq_4();
		IL2CPP_RUNTIME_CLASS_INIT(JsonWriter_t1165300239_il2cpp_TypeInfo_var);
		JsonWriter_IntToHex_m3949697945(NULL /*static, unused*/, L_32, L_33, /*hidden argument*/NULL);
		TextWriter_t2304124208 * L_34 = __this->get_writer_10();
		NullCheck(L_34);
		VirtActionInvoker1< String_t* >::Invoke(10 /* System.Void System.IO.TextWriter::Write(System.String) */, L_34, _stringLiteral2969);
		TextWriter_t2304124208 * L_35 = __this->get_writer_10();
		CharU5BU5D_t3324145743* L_36 = __this->get_hex_seq_4();
		NullCheck(L_35);
		VirtActionInvoker1< CharU5BU5D_t3324145743* >::Invoke(9 /* System.Void System.IO.TextWriter::Write(System.Char[]) */, L_35, L_36);
	}

IL_0156:
	{
		int32_t L_37 = V_1;
		V_1 = ((int32_t)((int32_t)L_37+(int32_t)1));
	}

IL_015a:
	{
		int32_t L_38 = V_1;
		int32_t L_39 = V_0;
		if ((((int32_t)L_38) < ((int32_t)L_39)))
		{
			goto IL_0026;
		}
	}
	{
		TextWriter_t2304124208 * L_40 = __this->get_writer_10();
		NullCheck(L_40);
		VirtActionInvoker1< Il2CppChar >::Invoke(8 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_40, ((int32_t)34));
		return;
	}
}
// System.Void LitJson.JsonWriter::Unindent()
extern "C"  void JsonWriter_Unindent_m130921831 (JsonWriter_t1165300239 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_pretty_print_8();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = __this->get_indentation_5();
		int32_t L_2 = __this->get_indent_value_6();
		__this->set_indentation_5(((int32_t)((int32_t)L_1-(int32_t)L_2)));
	}

IL_001e:
	{
		return;
	}
}
// System.String LitJson.JsonWriter::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t JsonWriter_ToString_m1656675505_MetadataUsageId;
extern "C"  String_t* JsonWriter_ToString_m1656675505 (JsonWriter_t1165300239 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_ToString_m1656675505_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringBuilder_t243639308 * L_0 = __this->get_inst_string_builder_7();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_1;
	}

IL_0011:
	{
		StringBuilder_t243639308 * L_2 = __this->get_inst_string_builder_7();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		return L_3;
	}
}
// System.Void LitJson.JsonWriter::Reset()
extern Il2CppClass* WriterContext_t3060158226_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1_Clear_m1989110242_MethodInfo_var;
extern const MethodInfo* Stack_1_Push_m2517233528_MethodInfo_var;
extern const uint32_t JsonWriter_Reset_m1994338575_MetadataUsageId;
extern "C"  void JsonWriter_Reset_m1994338575 (JsonWriter_t1165300239 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Reset_m1994338575_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_has_reached_end_3((bool)0);
		Stack_1_t1863751854 * L_0 = __this->get_ctx_stack_2();
		NullCheck(L_0);
		Stack_1_Clear_m1989110242(L_0, /*hidden argument*/Stack_1_Clear_m1989110242_MethodInfo_var);
		WriterContext_t3060158226 * L_1 = (WriterContext_t3060158226 *)il2cpp_codegen_object_new(WriterContext_t3060158226_il2cpp_TypeInfo_var);
		WriterContext__ctor_m159390989(L_1, /*hidden argument*/NULL);
		__this->set_context_1(L_1);
		Stack_1_t1863751854 * L_2 = __this->get_ctx_stack_2();
		WriterContext_t3060158226 * L_3 = __this->get_context_1();
		NullCheck(L_2);
		Stack_1_Push_m2517233528(L_2, L_3, /*hidden argument*/Stack_1_Push_m2517233528_MethodInfo_var);
		StringBuilder_t243639308 * L_4 = __this->get_inst_string_builder_7();
		if (!L_4)
		{
			goto IL_0051;
		}
	}
	{
		StringBuilder_t243639308 * L_5 = __this->get_inst_string_builder_7();
		StringBuilder_t243639308 * L_6 = __this->get_inst_string_builder_7();
		NullCheck(L_6);
		int32_t L_7 = StringBuilder_get_Length_m2443133099(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		StringBuilder_Remove_m970775893(L_5, 0, L_7, /*hidden argument*/NULL);
	}

IL_0051:
	{
		return;
	}
}
// System.Void LitJson.JsonWriter::Write(System.Boolean)
extern Il2CppCodeGenString* _stringLiteral3569038;
extern Il2CppCodeGenString* _stringLiteral97196323;
extern const uint32_t JsonWriter_Write_m2388425430_MetadataUsageId;
extern "C"  void JsonWriter_Write_m2388425430 (JsonWriter_t1165300239 * __this, bool ___boolean0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m2388425430_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JsonWriter_t1165300239 * G_B2_0 = NULL;
	JsonWriter_t1165300239 * G_B1_0 = NULL;
	String_t* G_B3_0 = NULL;
	JsonWriter_t1165300239 * G_B3_1 = NULL;
	{
		JsonWriter_DoValidation_m1443221920(__this, 4, /*hidden argument*/NULL);
		JsonWriter_PutNewline_m1237710887(__this, /*hidden argument*/NULL);
		bool L_0 = ___boolean0;
		G_B1_0 = __this;
		if (!L_0)
		{
			G_B2_0 = __this;
			goto IL_001e;
		}
	}
	{
		G_B3_0 = _stringLiteral3569038;
		G_B3_1 = G_B1_0;
		goto IL_0023;
	}

IL_001e:
	{
		G_B3_0 = _stringLiteral97196323;
		G_B3_1 = G_B2_0;
	}

IL_0023:
	{
		NullCheck(G_B3_1);
		JsonWriter_Put_m1880069331(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		WriterContext_t3060158226 * L_1 = __this->get_context_1();
		NullCheck(L_1);
		L_1->set_ExpectingValue_3((bool)0);
		return;
	}
}
// System.Void LitJson.JsonWriter::Write(System.Decimal)
extern Il2CppClass* JsonWriter_t1165300239_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern const uint32_t JsonWriter_Write_m947894477_MetadataUsageId;
extern "C"  void JsonWriter_Write_m947894477 (JsonWriter_t1165300239 * __this, Decimal_t1954350631  ___number0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m947894477_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		JsonWriter_DoValidation_m1443221920(__this, 4, /*hidden argument*/NULL);
		JsonWriter_PutNewline_m1237710887(__this, /*hidden argument*/NULL);
		Decimal_t1954350631  L_0 = ___number0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonWriter_t1165300239_il2cpp_TypeInfo_var);
		NumberFormatInfo_t1637637232 * L_1 = ((JsonWriter_t1165300239_StaticFields*)JsonWriter_t1165300239_il2cpp_TypeInfo_var->static_fields)->get_number_format_0();
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		String_t* L_2 = Convert_ToString_m4191404753(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		JsonWriter_Put_m1880069331(__this, L_2, /*hidden argument*/NULL);
		WriterContext_t3060158226 * L_3 = __this->get_context_1();
		NullCheck(L_3);
		L_3->set_ExpectingValue_3((bool)0);
		return;
	}
}
// System.Void LitJson.JsonWriter::Write(System.Double)
extern Il2CppClass* JsonWriter_t1165300239_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1474;
extern const uint32_t JsonWriter_Write_m471830019_MetadataUsageId;
extern "C"  void JsonWriter_Write_m471830019 (JsonWriter_t1165300239 * __this, double ___number0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m471830019_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		JsonWriter_DoValidation_m1443221920(__this, 4, /*hidden argument*/NULL);
		JsonWriter_PutNewline_m1237710887(__this, /*hidden argument*/NULL);
		double L_0 = ___number0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonWriter_t1165300239_il2cpp_TypeInfo_var);
		NumberFormatInfo_t1637637232 * L_1 = ((JsonWriter_t1165300239_StaticFields*)JsonWriter_t1165300239_il2cpp_TypeInfo_var->static_fields)->get_number_format_0();
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		String_t* L_2 = Convert_ToString_m1562162439(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = V_0;
		JsonWriter_Put_m1880069331(__this, L_3, /*hidden argument*/NULL);
		String_t* L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = String_IndexOf_m2775210486(L_4, ((int32_t)46), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_5) == ((uint32_t)(-1)))))
		{
			goto IL_004c;
		}
	}
	{
		String_t* L_6 = V_0;
		NullCheck(L_6);
		int32_t L_7 = String_IndexOf_m2775210486(L_6, ((int32_t)69), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)(-1)))))
		{
			goto IL_004c;
		}
	}
	{
		TextWriter_t2304124208 * L_8 = __this->get_writer_10();
		NullCheck(L_8);
		VirtActionInvoker1< String_t* >::Invoke(10 /* System.Void System.IO.TextWriter::Write(System.String) */, L_8, _stringLiteral1474);
	}

IL_004c:
	{
		WriterContext_t3060158226 * L_9 = __this->get_context_1();
		NullCheck(L_9);
		L_9->set_ExpectingValue_3((bool)0);
		return;
	}
}
// System.Void LitJson.JsonWriter::Write(System.Int32)
extern Il2CppClass* JsonWriter_t1165300239_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern const uint32_t JsonWriter_Write_m295913072_MetadataUsageId;
extern "C"  void JsonWriter_Write_m295913072 (JsonWriter_t1165300239 * __this, int32_t ___number0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m295913072_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		JsonWriter_DoValidation_m1443221920(__this, 4, /*hidden argument*/NULL);
		JsonWriter_PutNewline_m1237710887(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___number0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonWriter_t1165300239_il2cpp_TypeInfo_var);
		NumberFormatInfo_t1637637232 * L_1 = ((JsonWriter_t1165300239_StaticFields*)JsonWriter_t1165300239_il2cpp_TypeInfo_var->static_fields)->get_number_format_0();
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		String_t* L_2 = Convert_ToString_m428890190(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		JsonWriter_Put_m1880069331(__this, L_2, /*hidden argument*/NULL);
		WriterContext_t3060158226 * L_3 = __this->get_context_1();
		NullCheck(L_3);
		L_3->set_ExpectingValue_3((bool)0);
		return;
	}
}
// System.Void LitJson.JsonWriter::Write(System.Int64)
extern Il2CppClass* JsonWriter_t1165300239_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern const uint32_t JsonWriter_Write_m295916017_MetadataUsageId;
extern "C"  void JsonWriter_Write_m295916017 (JsonWriter_t1165300239 * __this, int64_t ___number0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m295916017_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		JsonWriter_DoValidation_m1443221920(__this, 4, /*hidden argument*/NULL);
		JsonWriter_PutNewline_m1237710887(__this, /*hidden argument*/NULL);
		int64_t L_0 = ___number0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonWriter_t1165300239_il2cpp_TypeInfo_var);
		NumberFormatInfo_t1637637232 * L_1 = ((JsonWriter_t1165300239_StaticFields*)JsonWriter_t1165300239_il2cpp_TypeInfo_var->static_fields)->get_number_format_0();
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		String_t* L_2 = Convert_ToString_m662498221(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		JsonWriter_Put_m1880069331(__this, L_2, /*hidden argument*/NULL);
		WriterContext_t3060158226 * L_3 = __this->get_context_1();
		NullCheck(L_3);
		L_3->set_ExpectingValue_3((bool)0);
		return;
	}
}
// System.Void LitJson.JsonWriter::Write(System.String)
extern Il2CppCodeGenString* _stringLiteral3392903;
extern const uint32_t JsonWriter_Write_m1040069059_MetadataUsageId;
extern "C"  void JsonWriter_Write_m1040069059 (JsonWriter_t1165300239 * __this, String_t* ___str0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m1040069059_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		JsonWriter_DoValidation_m1443221920(__this, 4, /*hidden argument*/NULL);
		JsonWriter_PutNewline_m1237710887(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___str0;
		if (L_0)
		{
			goto IL_0023;
		}
	}
	{
		JsonWriter_Put_m1880069331(__this, _stringLiteral3392903, /*hidden argument*/NULL);
		goto IL_002a;
	}

IL_0023:
	{
		String_t* L_1 = ___str0;
		JsonWriter_PutString_m306582274(__this, L_1, /*hidden argument*/NULL);
	}

IL_002a:
	{
		WriterContext_t3060158226 * L_2 = __this->get_context_1();
		NullCheck(L_2);
		L_2->set_ExpectingValue_3((bool)0);
		return;
	}
}
// System.Void LitJson.JsonWriter::Write(System.UInt64)
extern Il2CppClass* JsonWriter_t1165300239_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern const uint32_t JsonWriter_Write_m1580601148_MetadataUsageId;
extern "C"  void JsonWriter_Write_m1580601148 (JsonWriter_t1165300239 * __this, uint64_t ___number0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_Write_m1580601148_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		JsonWriter_DoValidation_m1443221920(__this, 4, /*hidden argument*/NULL);
		JsonWriter_PutNewline_m1237710887(__this, /*hidden argument*/NULL);
		uint64_t L_0 = ___number0;
		IL2CPP_RUNTIME_CLASS_INIT(JsonWriter_t1165300239_il2cpp_TypeInfo_var);
		NumberFormatInfo_t1637637232 * L_1 = ((JsonWriter_t1165300239_StaticFields*)JsonWriter_t1165300239_il2cpp_TypeInfo_var->static_fields)->get_number_format_0();
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		String_t* L_2 = Convert_ToString_m127787950(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		JsonWriter_Put_m1880069331(__this, L_2, /*hidden argument*/NULL);
		WriterContext_t3060158226 * L_3 = __this->get_context_1();
		NullCheck(L_3);
		L_3->set_ExpectingValue_3((bool)0);
		return;
	}
}
// System.Void LitJson.JsonWriter::WriteArrayEnd()
extern const MethodInfo* Stack_1_Pop_m3325825238_MethodInfo_var;
extern const MethodInfo* Stack_1_get_Count_m3332489163_MethodInfo_var;
extern const MethodInfo* Stack_1_Peek_m11868760_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t JsonWriter_WriteArrayEnd_m3594342657_MetadataUsageId;
extern "C"  void JsonWriter_WriteArrayEnd_m3594342657 (JsonWriter_t1165300239 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_WriteArrayEnd_m3594342657_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		JsonWriter_DoValidation_m1443221920(__this, 0, /*hidden argument*/NULL);
		JsonWriter_PutNewline_m2395463006(__this, (bool)0, /*hidden argument*/NULL);
		Stack_1_t1863751854 * L_0 = __this->get_ctx_stack_2();
		NullCheck(L_0);
		Stack_1_Pop_m3325825238(L_0, /*hidden argument*/Stack_1_Pop_m3325825238_MethodInfo_var);
		Stack_1_t1863751854 * L_1 = __this->get_ctx_stack_2();
		NullCheck(L_1);
		int32_t L_2 = Stack_1_get_Count_m3332489163(L_1, /*hidden argument*/Stack_1_get_Count_m3332489163_MethodInfo_var);
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_0037;
		}
	}
	{
		__this->set_has_reached_end_3((bool)1);
		goto IL_0054;
	}

IL_0037:
	{
		Stack_1_t1863751854 * L_3 = __this->get_ctx_stack_2();
		NullCheck(L_3);
		WriterContext_t3060158226 * L_4 = Stack_1_Peek_m11868760(L_3, /*hidden argument*/Stack_1_Peek_m11868760_MethodInfo_var);
		__this->set_context_1(L_4);
		WriterContext_t3060158226 * L_5 = __this->get_context_1();
		NullCheck(L_5);
		L_5->set_ExpectingValue_3((bool)0);
	}

IL_0054:
	{
		JsonWriter_Unindent_m130921831(__this, /*hidden argument*/NULL);
		JsonWriter_Put_m1880069331(__this, _stringLiteral93, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonWriter::WriteArrayStart()
extern Il2CppClass* WriterContext_t3060158226_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1_Push_m2517233528_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern const uint32_t JsonWriter_WriteArrayStart_m721019272_MetadataUsageId;
extern "C"  void JsonWriter_WriteArrayStart_m721019272 (JsonWriter_t1165300239 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_WriteArrayStart_m721019272_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		JsonWriter_DoValidation_m1443221920(__this, 2, /*hidden argument*/NULL);
		JsonWriter_PutNewline_m1237710887(__this, /*hidden argument*/NULL);
		JsonWriter_Put_m1880069331(__this, _stringLiteral91, /*hidden argument*/NULL);
		WriterContext_t3060158226 * L_0 = (WriterContext_t3060158226 *)il2cpp_codegen_object_new(WriterContext_t3060158226_il2cpp_TypeInfo_var);
		WriterContext__ctor_m159390989(L_0, /*hidden argument*/NULL);
		__this->set_context_1(L_0);
		WriterContext_t3060158226 * L_1 = __this->get_context_1();
		NullCheck(L_1);
		L_1->set_InArray_1((bool)1);
		Stack_1_t1863751854 * L_2 = __this->get_ctx_stack_2();
		WriterContext_t3060158226 * L_3 = __this->get_context_1();
		NullCheck(L_2);
		Stack_1_Push_m2517233528(L_2, L_3, /*hidden argument*/Stack_1_Push_m2517233528_MethodInfo_var);
		JsonWriter_Indent_m2157585422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonWriter::WriteObjectEnd()
extern const MethodInfo* Stack_1_Pop_m3325825238_MethodInfo_var;
extern const MethodInfo* Stack_1_get_Count_m3332489163_MethodInfo_var;
extern const MethodInfo* Stack_1_Peek_m11868760_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral125;
extern const uint32_t JsonWriter_WriteObjectEnd_m2431063583_MetadataUsageId;
extern "C"  void JsonWriter_WriteObjectEnd_m2431063583 (JsonWriter_t1165300239 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_WriteObjectEnd_m2431063583_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		JsonWriter_DoValidation_m1443221920(__this, 1, /*hidden argument*/NULL);
		JsonWriter_PutNewline_m2395463006(__this, (bool)0, /*hidden argument*/NULL);
		Stack_1_t1863751854 * L_0 = __this->get_ctx_stack_2();
		NullCheck(L_0);
		Stack_1_Pop_m3325825238(L_0, /*hidden argument*/Stack_1_Pop_m3325825238_MethodInfo_var);
		Stack_1_t1863751854 * L_1 = __this->get_ctx_stack_2();
		NullCheck(L_1);
		int32_t L_2 = Stack_1_get_Count_m3332489163(L_1, /*hidden argument*/Stack_1_get_Count_m3332489163_MethodInfo_var);
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_0037;
		}
	}
	{
		__this->set_has_reached_end_3((bool)1);
		goto IL_0054;
	}

IL_0037:
	{
		Stack_1_t1863751854 * L_3 = __this->get_ctx_stack_2();
		NullCheck(L_3);
		WriterContext_t3060158226 * L_4 = Stack_1_Peek_m11868760(L_3, /*hidden argument*/Stack_1_Peek_m11868760_MethodInfo_var);
		__this->set_context_1(L_4);
		WriterContext_t3060158226 * L_5 = __this->get_context_1();
		NullCheck(L_5);
		L_5->set_ExpectingValue_3((bool)0);
	}

IL_0054:
	{
		JsonWriter_Unindent_m130921831(__this, /*hidden argument*/NULL);
		JsonWriter_Put_m1880069331(__this, _stringLiteral125, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonWriter::WriteObjectStart()
extern Il2CppClass* WriterContext_t3060158226_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1_Push_m2517233528_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral123;
extern const uint32_t JsonWriter_WriteObjectStart_m3796293414_MetadataUsageId;
extern "C"  void JsonWriter_WriteObjectStart_m3796293414 (JsonWriter_t1165300239 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_WriteObjectStart_m3796293414_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		JsonWriter_DoValidation_m1443221920(__this, 2, /*hidden argument*/NULL);
		JsonWriter_PutNewline_m1237710887(__this, /*hidden argument*/NULL);
		JsonWriter_Put_m1880069331(__this, _stringLiteral123, /*hidden argument*/NULL);
		WriterContext_t3060158226 * L_0 = (WriterContext_t3060158226 *)il2cpp_codegen_object_new(WriterContext_t3060158226_il2cpp_TypeInfo_var);
		WriterContext__ctor_m159390989(L_0, /*hidden argument*/NULL);
		__this->set_context_1(L_0);
		WriterContext_t3060158226 * L_1 = __this->get_context_1();
		NullCheck(L_1);
		L_1->set_InObject_2((bool)1);
		Stack_1_t1863751854 * L_2 = __this->get_ctx_stack_2();
		WriterContext_t3060158226 * L_3 = __this->get_context_1();
		NullCheck(L_2);
		Stack_1_Push_m2517233528(L_2, L_3, /*hidden argument*/Stack_1_Push_m2517233528_MethodInfo_var);
		JsonWriter_Indent_m2157585422(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonWriter::WritePropertyName(System.String)
extern Il2CppCodeGenString* _stringLiteral1830;
extern const uint32_t JsonWriter_WritePropertyName_m1552473251_MetadataUsageId;
extern "C"  void JsonWriter_WritePropertyName_m1552473251 (JsonWriter_t1165300239 * __this, String_t* ___property_name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonWriter_WritePropertyName_m1552473251_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		JsonWriter_DoValidation_m1443221920(__this, 3, /*hidden argument*/NULL);
		JsonWriter_PutNewline_m1237710887(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___property_name0;
		JsonWriter_PutString_m306582274(__this, L_0, /*hidden argument*/NULL);
		bool L_1 = __this->get_pretty_print_8();
		if (!L_1)
		{
			goto IL_008b;
		}
	}
	{
		String_t* L_2 = ___property_name0;
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m2979997331(L_2, /*hidden argument*/NULL);
		WriterContext_t3060158226 * L_4 = __this->get_context_1();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_Padding_4();
		if ((((int32_t)L_3) <= ((int32_t)L_5)))
		{
			goto IL_0046;
		}
	}
	{
		WriterContext_t3060158226 * L_6 = __this->get_context_1();
		String_t* L_7 = ___property_name0;
		NullCheck(L_7);
		int32_t L_8 = String_get_Length_m2979997331(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		L_6->set_Padding_4(L_8);
	}

IL_0046:
	{
		WriterContext_t3060158226 * L_9 = __this->get_context_1();
		NullCheck(L_9);
		int32_t L_10 = L_9->get_Padding_4();
		String_t* L_11 = ___property_name0;
		NullCheck(L_11);
		int32_t L_12 = String_get_Length_m2979997331(L_11, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_10-(int32_t)L_12));
		goto IL_006f;
	}

IL_005e:
	{
		TextWriter_t2304124208 * L_13 = __this->get_writer_10();
		NullCheck(L_13);
		VirtActionInvoker1< Il2CppChar >::Invoke(8 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_13, ((int32_t)32));
		int32_t L_14 = V_0;
		V_0 = ((int32_t)((int32_t)L_14-(int32_t)1));
	}

IL_006f:
	{
		int32_t L_15 = V_0;
		if ((((int32_t)L_15) >= ((int32_t)0)))
		{
			goto IL_005e;
		}
	}
	{
		TextWriter_t2304124208 * L_16 = __this->get_writer_10();
		NullCheck(L_16);
		VirtActionInvoker1< String_t* >::Invoke(10 /* System.Void System.IO.TextWriter::Write(System.String) */, L_16, _stringLiteral1830);
		goto IL_0098;
	}

IL_008b:
	{
		TextWriter_t2304124208 * L_17 = __this->get_writer_10();
		NullCheck(L_17);
		VirtActionInvoker1< Il2CppChar >::Invoke(8 /* System.Void System.IO.TextWriter::Write(System.Char) */, L_17, ((int32_t)58));
	}

IL_0098:
	{
		WriterContext_t3060158226 * L_18 = __this->get_context_1();
		NullCheck(L_18);
		L_18->set_ExpectingValue_3((bool)1);
		return;
	}
}
// System.Boolean LitJson.Lexer::get_EndOfInput()
extern "C"  bool Lexer_get_EndOfInput_m57994450 (Lexer_t3664372066 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_end_of_input_4();
		return L_0;
	}
}
// System.Int32 LitJson.Lexer::get_Token()
extern "C"  int32_t Lexer_get_Token_m3197663547 (Lexer_t3664372066 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_token_12();
		return L_0;
	}
}
// System.String LitJson.Lexer::get_StringValue()
extern "C"  String_t* Lexer_get_StringValue_m2250118517 (Lexer_t3664372066 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_string_value_11();
		return L_0;
	}
}
// System.Void LitJson.Lexer::.cctor()
extern Il2CppClass* Lexer_t3664372066_il2cpp_TypeInfo_var;
extern const uint32_t Lexer__cctor_m312235920_MetadataUsageId;
extern "C"  void Lexer__cctor_m312235920 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Lexer__cctor_m312235920_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Lexer_PopulateFsmTables_m2601495830(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.Lexer::.ctor(System.IO.TextReader)
extern Il2CppClass* StringBuilder_t243639308_il2cpp_TypeInfo_var;
extern Il2CppClass* FsmContext_t3936467683_il2cpp_TypeInfo_var;
extern const uint32_t Lexer__ctor_m1101987844_MetadataUsageId;
extern "C"  void Lexer__ctor_m1101987844 (Lexer_t3664372066 * __this, TextReader_t2148718976 * ___reader0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Lexer__ctor_m1101987844_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		__this->set_allow_comments_2((bool)1);
		__this->set_allow_single_quoted_strings_3((bool)1);
		__this->set_input_buffer_6(0);
		StringBuilder_t243639308 * L_0 = (StringBuilder_t243639308 *)il2cpp_codegen_object_new(StringBuilder_t243639308_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3624398269(L_0, ((int32_t)128), /*hidden argument*/NULL);
		__this->set_string_buffer_10(L_0);
		__this->set_state_9(1);
		__this->set_end_of_input_4((bool)0);
		TextReader_t2148718976 * L_1 = ___reader0;
		__this->set_reader_8(L_1);
		FsmContext_t3936467683 * L_2 = (FsmContext_t3936467683 *)il2cpp_codegen_object_new(FsmContext_t3936467683_il2cpp_TypeInfo_var);
		FsmContext__ctor_m4002008078(L_2, /*hidden argument*/NULL);
		__this->set_fsm_context_5(L_2);
		FsmContext_t3936467683 * L_3 = __this->get_fsm_context_5();
		NullCheck(L_3);
		L_3->set_L_2(__this);
		return;
	}
}
// System.Int32 LitJson.Lexer::HexValue(System.Int32)
extern "C"  int32_t Lexer_HexValue_m1760624318 (Il2CppObject * __this /* static, unused */, int32_t ___digit0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___digit0;
		if (((int32_t)((int32_t)L_0-(int32_t)((int32_t)65))) == 0)
		{
			goto IL_0047;
		}
		if (((int32_t)((int32_t)L_0-(int32_t)((int32_t)65))) == 1)
		{
			goto IL_004a;
		}
		if (((int32_t)((int32_t)L_0-(int32_t)((int32_t)65))) == 2)
		{
			goto IL_004d;
		}
		if (((int32_t)((int32_t)L_0-(int32_t)((int32_t)65))) == 3)
		{
			goto IL_0050;
		}
		if (((int32_t)((int32_t)L_0-(int32_t)((int32_t)65))) == 4)
		{
			goto IL_0053;
		}
		if (((int32_t)((int32_t)L_0-(int32_t)((int32_t)65))) == 5)
		{
			goto IL_0056;
		}
	}
	{
		int32_t L_1 = ___digit0;
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)97))) == 0)
		{
			goto IL_0047;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)97))) == 1)
		{
			goto IL_004a;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)97))) == 2)
		{
			goto IL_004d;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)97))) == 3)
		{
			goto IL_0050;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)97))) == 4)
		{
			goto IL_0053;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)((int32_t)97))) == 5)
		{
			goto IL_0056;
		}
	}
	{
		goto IL_0059;
	}

IL_0047:
	{
		return ((int32_t)10);
	}

IL_004a:
	{
		return ((int32_t)11);
	}

IL_004d:
	{
		return ((int32_t)12);
	}

IL_0050:
	{
		return ((int32_t)13);
	}

IL_0053:
	{
		return ((int32_t)14);
	}

IL_0056:
	{
		return ((int32_t)15);
	}

IL_0059:
	{
		int32_t L_2 = ___digit0;
		return ((int32_t)((int32_t)L_2-(int32_t)((int32_t)48)));
	}
}
// System.Void LitJson.Lexer::PopulateFsmTables()
extern Il2CppClass* StateHandlerU5BU5D_t2657855690_il2cpp_TypeInfo_var;
extern Il2CppClass* Lexer_t3664372066_il2cpp_TypeInfo_var;
extern Il2CppClass* StateHandler_t3261942315_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var;
extern const MethodInfo* Lexer_State1_m1833119603_MethodInfo_var;
extern const MethodInfo* Lexer_State2_m2044470516_MethodInfo_var;
extern const MethodInfo* Lexer_State3_m2255821429_MethodInfo_var;
extern const MethodInfo* Lexer_State4_m2467172342_MethodInfo_var;
extern const MethodInfo* Lexer_State5_m2678523255_MethodInfo_var;
extern const MethodInfo* Lexer_State6_m2889874168_MethodInfo_var;
extern const MethodInfo* Lexer_State7_m3101225081_MethodInfo_var;
extern const MethodInfo* Lexer_State8_m3312575994_MethodInfo_var;
extern const MethodInfo* Lexer_State9_m3523926907_MethodInfo_var;
extern const MethodInfo* Lexer_State10_m3439175875_MethodInfo_var;
extern const MethodInfo* Lexer_State11_m3650526788_MethodInfo_var;
extern const MethodInfo* Lexer_State12_m3861877701_MethodInfo_var;
extern const MethodInfo* Lexer_State13_m4073228614_MethodInfo_var;
extern const MethodInfo* Lexer_State14_m4284579527_MethodInfo_var;
extern const MethodInfo* Lexer_State15_m200963144_MethodInfo_var;
extern const MethodInfo* Lexer_State16_m412314057_MethodInfo_var;
extern const MethodInfo* Lexer_State17_m623664970_MethodInfo_var;
extern const MethodInfo* Lexer_State18_m835015883_MethodInfo_var;
extern const MethodInfo* Lexer_State19_m1046366796_MethodInfo_var;
extern const MethodInfo* Lexer_State20_m1401119586_MethodInfo_var;
extern const MethodInfo* Lexer_State21_m1612470499_MethodInfo_var;
extern const MethodInfo* Lexer_State22_m1823821412_MethodInfo_var;
extern const MethodInfo* Lexer_State23_m2035172325_MethodInfo_var;
extern const MethodInfo* Lexer_State24_m2246523238_MethodInfo_var;
extern const MethodInfo* Lexer_State25_m2457874151_MethodInfo_var;
extern const MethodInfo* Lexer_State26_m2669225064_MethodInfo_var;
extern const MethodInfo* Lexer_State27_m2880575977_MethodInfo_var;
extern const MethodInfo* Lexer_State28_m3091926890_MethodInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422____U24fieldU2DC_12_FieldInfo_var;
extern const uint32_t Lexer_PopulateFsmTables_m2601495830_MetadataUsageId;
extern "C"  void Lexer_PopulateFsmTables_m2601495830 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Lexer_PopulateFsmTables_m2601495830_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B2_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B2_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B1_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B1_2 = NULL;
	int32_t G_B4_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B4_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B4_2 = NULL;
	int32_t G_B3_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B3_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B3_2 = NULL;
	int32_t G_B6_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B6_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B6_2 = NULL;
	int32_t G_B5_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B5_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B5_2 = NULL;
	int32_t G_B8_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B8_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B8_2 = NULL;
	int32_t G_B7_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B7_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B7_2 = NULL;
	int32_t G_B10_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B10_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B10_2 = NULL;
	int32_t G_B9_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B9_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B9_2 = NULL;
	int32_t G_B12_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B12_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B12_2 = NULL;
	int32_t G_B11_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B11_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B11_2 = NULL;
	int32_t G_B14_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B14_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B14_2 = NULL;
	int32_t G_B13_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B13_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B13_2 = NULL;
	int32_t G_B16_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B16_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B16_2 = NULL;
	int32_t G_B15_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B15_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B15_2 = NULL;
	int32_t G_B18_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B18_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B18_2 = NULL;
	int32_t G_B17_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B17_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B17_2 = NULL;
	int32_t G_B20_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B20_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B20_2 = NULL;
	int32_t G_B19_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B19_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B19_2 = NULL;
	int32_t G_B22_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B22_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B22_2 = NULL;
	int32_t G_B21_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B21_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B21_2 = NULL;
	int32_t G_B24_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B24_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B24_2 = NULL;
	int32_t G_B23_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B23_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B23_2 = NULL;
	int32_t G_B26_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B26_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B26_2 = NULL;
	int32_t G_B25_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B25_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B25_2 = NULL;
	int32_t G_B28_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B28_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B28_2 = NULL;
	int32_t G_B27_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B27_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B27_2 = NULL;
	int32_t G_B30_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B30_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B30_2 = NULL;
	int32_t G_B29_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B29_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B29_2 = NULL;
	int32_t G_B32_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B32_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B32_2 = NULL;
	int32_t G_B31_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B31_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B31_2 = NULL;
	int32_t G_B34_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B34_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B34_2 = NULL;
	int32_t G_B33_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B33_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B33_2 = NULL;
	int32_t G_B36_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B36_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B36_2 = NULL;
	int32_t G_B35_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B35_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B35_2 = NULL;
	int32_t G_B38_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B38_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B38_2 = NULL;
	int32_t G_B37_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B37_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B37_2 = NULL;
	int32_t G_B40_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B40_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B40_2 = NULL;
	int32_t G_B39_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B39_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B39_2 = NULL;
	int32_t G_B42_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B42_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B42_2 = NULL;
	int32_t G_B41_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B41_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B41_2 = NULL;
	int32_t G_B44_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B44_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B44_2 = NULL;
	int32_t G_B43_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B43_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B43_2 = NULL;
	int32_t G_B46_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B46_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B46_2 = NULL;
	int32_t G_B45_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B45_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B45_2 = NULL;
	int32_t G_B48_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B48_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B48_2 = NULL;
	int32_t G_B47_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B47_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B47_2 = NULL;
	int32_t G_B50_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B50_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B50_2 = NULL;
	int32_t G_B49_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B49_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B49_2 = NULL;
	int32_t G_B52_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B52_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B52_2 = NULL;
	int32_t G_B51_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B51_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B51_2 = NULL;
	int32_t G_B54_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B54_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B54_2 = NULL;
	int32_t G_B53_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B53_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B53_2 = NULL;
	int32_t G_B56_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B56_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B56_2 = NULL;
	int32_t G_B55_0 = 0;
	StateHandlerU5BU5D_t2657855690* G_B55_1 = NULL;
	StateHandlerU5BU5D_t2657855690* G_B55_2 = NULL;
	{
		StateHandlerU5BU5D_t2657855690* L_0 = ((StateHandlerU5BU5D_t2657855690*)SZArrayNew(StateHandlerU5BU5D_t2657855690_il2cpp_TypeInfo_var, (uint32_t)((int32_t)28)));
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		StateHandler_t3261942315 * L_1 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache0_14();
		G_B1_0 = 0;
		G_B1_1 = L_0;
		G_B1_2 = L_0;
		if (L_1)
		{
			G_B2_0 = 0;
			G_B2_1 = L_0;
			G_B2_2 = L_0;
			goto IL_0021;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)Lexer_State1_m1833119603_MethodInfo_var);
		StateHandler_t3261942315 * L_3 = (StateHandler_t3261942315 *)il2cpp_codegen_object_new(StateHandler_t3261942315_il2cpp_TypeInfo_var);
		StateHandler__ctor_m3491238465(L_3, NULL, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache0_14(L_3);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		StateHandler_t3261942315 * L_4 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache0_14();
		NullCheck(G_B2_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B2_1, G_B2_0);
		ArrayElementTypeCheck (G_B2_1, L_4);
		(G_B2_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B2_0), (StateHandler_t3261942315 *)L_4);
		StateHandlerU5BU5D_t2657855690* L_5 = G_B2_2;
		StateHandler_t3261942315 * L_6 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache1_15();
		G_B3_0 = 1;
		G_B3_1 = L_5;
		G_B3_2 = L_5;
		if (L_6)
		{
			G_B4_0 = 1;
			G_B4_1 = L_5;
			G_B4_2 = L_5;
			goto IL_0041;
		}
	}
	{
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)Lexer_State2_m2044470516_MethodInfo_var);
		StateHandler_t3261942315 * L_8 = (StateHandler_t3261942315 *)il2cpp_codegen_object_new(StateHandler_t3261942315_il2cpp_TypeInfo_var);
		StateHandler__ctor_m3491238465(L_8, NULL, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache1_15(L_8);
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
		G_B4_2 = G_B3_2;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		StateHandler_t3261942315 * L_9 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache1_15();
		NullCheck(G_B4_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B4_1, G_B4_0);
		ArrayElementTypeCheck (G_B4_1, L_9);
		(G_B4_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B4_0), (StateHandler_t3261942315 *)L_9);
		StateHandlerU5BU5D_t2657855690* L_10 = G_B4_2;
		StateHandler_t3261942315 * L_11 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache2_16();
		G_B5_0 = 2;
		G_B5_1 = L_10;
		G_B5_2 = L_10;
		if (L_11)
		{
			G_B6_0 = 2;
			G_B6_1 = L_10;
			G_B6_2 = L_10;
			goto IL_0061;
		}
	}
	{
		IntPtr_t L_12;
		L_12.set_m_value_0((void*)(void*)Lexer_State3_m2255821429_MethodInfo_var);
		StateHandler_t3261942315 * L_13 = (StateHandler_t3261942315 *)il2cpp_codegen_object_new(StateHandler_t3261942315_il2cpp_TypeInfo_var);
		StateHandler__ctor_m3491238465(L_13, NULL, L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache2_16(L_13);
		G_B6_0 = G_B5_0;
		G_B6_1 = G_B5_1;
		G_B6_2 = G_B5_2;
	}

IL_0061:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		StateHandler_t3261942315 * L_14 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache2_16();
		NullCheck(G_B6_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_1, G_B6_0);
		ArrayElementTypeCheck (G_B6_1, L_14);
		(G_B6_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_0), (StateHandler_t3261942315 *)L_14);
		StateHandlerU5BU5D_t2657855690* L_15 = G_B6_2;
		StateHandler_t3261942315 * L_16 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache3_17();
		G_B7_0 = 3;
		G_B7_1 = L_15;
		G_B7_2 = L_15;
		if (L_16)
		{
			G_B8_0 = 3;
			G_B8_1 = L_15;
			G_B8_2 = L_15;
			goto IL_0081;
		}
	}
	{
		IntPtr_t L_17;
		L_17.set_m_value_0((void*)(void*)Lexer_State4_m2467172342_MethodInfo_var);
		StateHandler_t3261942315 * L_18 = (StateHandler_t3261942315 *)il2cpp_codegen_object_new(StateHandler_t3261942315_il2cpp_TypeInfo_var);
		StateHandler__ctor_m3491238465(L_18, NULL, L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache3_17(L_18);
		G_B8_0 = G_B7_0;
		G_B8_1 = G_B7_1;
		G_B8_2 = G_B7_2;
	}

IL_0081:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		StateHandler_t3261942315 * L_19 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache3_17();
		NullCheck(G_B8_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B8_1, G_B8_0);
		ArrayElementTypeCheck (G_B8_1, L_19);
		(G_B8_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B8_0), (StateHandler_t3261942315 *)L_19);
		StateHandlerU5BU5D_t2657855690* L_20 = G_B8_2;
		StateHandler_t3261942315 * L_21 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache4_18();
		G_B9_0 = 4;
		G_B9_1 = L_20;
		G_B9_2 = L_20;
		if (L_21)
		{
			G_B10_0 = 4;
			G_B10_1 = L_20;
			G_B10_2 = L_20;
			goto IL_00a1;
		}
	}
	{
		IntPtr_t L_22;
		L_22.set_m_value_0((void*)(void*)Lexer_State5_m2678523255_MethodInfo_var);
		StateHandler_t3261942315 * L_23 = (StateHandler_t3261942315 *)il2cpp_codegen_object_new(StateHandler_t3261942315_il2cpp_TypeInfo_var);
		StateHandler__ctor_m3491238465(L_23, NULL, L_22, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache4_18(L_23);
		G_B10_0 = G_B9_0;
		G_B10_1 = G_B9_1;
		G_B10_2 = G_B9_2;
	}

IL_00a1:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		StateHandler_t3261942315 * L_24 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache4_18();
		NullCheck(G_B10_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B10_1, G_B10_0);
		ArrayElementTypeCheck (G_B10_1, L_24);
		(G_B10_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B10_0), (StateHandler_t3261942315 *)L_24);
		StateHandlerU5BU5D_t2657855690* L_25 = G_B10_2;
		StateHandler_t3261942315 * L_26 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache5_19();
		G_B11_0 = 5;
		G_B11_1 = L_25;
		G_B11_2 = L_25;
		if (L_26)
		{
			G_B12_0 = 5;
			G_B12_1 = L_25;
			G_B12_2 = L_25;
			goto IL_00c1;
		}
	}
	{
		IntPtr_t L_27;
		L_27.set_m_value_0((void*)(void*)Lexer_State6_m2889874168_MethodInfo_var);
		StateHandler_t3261942315 * L_28 = (StateHandler_t3261942315 *)il2cpp_codegen_object_new(StateHandler_t3261942315_il2cpp_TypeInfo_var);
		StateHandler__ctor_m3491238465(L_28, NULL, L_27, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache5_19(L_28);
		G_B12_0 = G_B11_0;
		G_B12_1 = G_B11_1;
		G_B12_2 = G_B11_2;
	}

IL_00c1:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		StateHandler_t3261942315 * L_29 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache5_19();
		NullCheck(G_B12_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B12_1, G_B12_0);
		ArrayElementTypeCheck (G_B12_1, L_29);
		(G_B12_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B12_0), (StateHandler_t3261942315 *)L_29);
		StateHandlerU5BU5D_t2657855690* L_30 = G_B12_2;
		StateHandler_t3261942315 * L_31 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache6_20();
		G_B13_0 = 6;
		G_B13_1 = L_30;
		G_B13_2 = L_30;
		if (L_31)
		{
			G_B14_0 = 6;
			G_B14_1 = L_30;
			G_B14_2 = L_30;
			goto IL_00e1;
		}
	}
	{
		IntPtr_t L_32;
		L_32.set_m_value_0((void*)(void*)Lexer_State7_m3101225081_MethodInfo_var);
		StateHandler_t3261942315 * L_33 = (StateHandler_t3261942315 *)il2cpp_codegen_object_new(StateHandler_t3261942315_il2cpp_TypeInfo_var);
		StateHandler__ctor_m3491238465(L_33, NULL, L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache6_20(L_33);
		G_B14_0 = G_B13_0;
		G_B14_1 = G_B13_1;
		G_B14_2 = G_B13_2;
	}

IL_00e1:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		StateHandler_t3261942315 * L_34 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache6_20();
		NullCheck(G_B14_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B14_1, G_B14_0);
		ArrayElementTypeCheck (G_B14_1, L_34);
		(G_B14_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B14_0), (StateHandler_t3261942315 *)L_34);
		StateHandlerU5BU5D_t2657855690* L_35 = G_B14_2;
		StateHandler_t3261942315 * L_36 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache7_21();
		G_B15_0 = 7;
		G_B15_1 = L_35;
		G_B15_2 = L_35;
		if (L_36)
		{
			G_B16_0 = 7;
			G_B16_1 = L_35;
			G_B16_2 = L_35;
			goto IL_0101;
		}
	}
	{
		IntPtr_t L_37;
		L_37.set_m_value_0((void*)(void*)Lexer_State8_m3312575994_MethodInfo_var);
		StateHandler_t3261942315 * L_38 = (StateHandler_t3261942315 *)il2cpp_codegen_object_new(StateHandler_t3261942315_il2cpp_TypeInfo_var);
		StateHandler__ctor_m3491238465(L_38, NULL, L_37, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache7_21(L_38);
		G_B16_0 = G_B15_0;
		G_B16_1 = G_B15_1;
		G_B16_2 = G_B15_2;
	}

IL_0101:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		StateHandler_t3261942315 * L_39 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache7_21();
		NullCheck(G_B16_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B16_1, G_B16_0);
		ArrayElementTypeCheck (G_B16_1, L_39);
		(G_B16_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B16_0), (StateHandler_t3261942315 *)L_39);
		StateHandlerU5BU5D_t2657855690* L_40 = G_B16_2;
		StateHandler_t3261942315 * L_41 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache8_22();
		G_B17_0 = 8;
		G_B17_1 = L_40;
		G_B17_2 = L_40;
		if (L_41)
		{
			G_B18_0 = 8;
			G_B18_1 = L_40;
			G_B18_2 = L_40;
			goto IL_0121;
		}
	}
	{
		IntPtr_t L_42;
		L_42.set_m_value_0((void*)(void*)Lexer_State9_m3523926907_MethodInfo_var);
		StateHandler_t3261942315 * L_43 = (StateHandler_t3261942315 *)il2cpp_codegen_object_new(StateHandler_t3261942315_il2cpp_TypeInfo_var);
		StateHandler__ctor_m3491238465(L_43, NULL, L_42, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache8_22(L_43);
		G_B18_0 = G_B17_0;
		G_B18_1 = G_B17_1;
		G_B18_2 = G_B17_2;
	}

IL_0121:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		StateHandler_t3261942315 * L_44 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache8_22();
		NullCheck(G_B18_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B18_1, G_B18_0);
		ArrayElementTypeCheck (G_B18_1, L_44);
		(G_B18_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B18_0), (StateHandler_t3261942315 *)L_44);
		StateHandlerU5BU5D_t2657855690* L_45 = G_B18_2;
		StateHandler_t3261942315 * L_46 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache9_23();
		G_B19_0 = ((int32_t)9);
		G_B19_1 = L_45;
		G_B19_2 = L_45;
		if (L_46)
		{
			G_B20_0 = ((int32_t)9);
			G_B20_1 = L_45;
			G_B20_2 = L_45;
			goto IL_0142;
		}
	}
	{
		IntPtr_t L_47;
		L_47.set_m_value_0((void*)(void*)Lexer_State10_m3439175875_MethodInfo_var);
		StateHandler_t3261942315 * L_48 = (StateHandler_t3261942315 *)il2cpp_codegen_object_new(StateHandler_t3261942315_il2cpp_TypeInfo_var);
		StateHandler__ctor_m3491238465(L_48, NULL, L_47, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache9_23(L_48);
		G_B20_0 = G_B19_0;
		G_B20_1 = G_B19_1;
		G_B20_2 = G_B19_2;
	}

IL_0142:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		StateHandler_t3261942315 * L_49 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache9_23();
		NullCheck(G_B20_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B20_1, G_B20_0);
		ArrayElementTypeCheck (G_B20_1, L_49);
		(G_B20_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B20_0), (StateHandler_t3261942315 *)L_49);
		StateHandlerU5BU5D_t2657855690* L_50 = G_B20_2;
		StateHandler_t3261942315 * L_51 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cacheA_24();
		G_B21_0 = ((int32_t)10);
		G_B21_1 = L_50;
		G_B21_2 = L_50;
		if (L_51)
		{
			G_B22_0 = ((int32_t)10);
			G_B22_1 = L_50;
			G_B22_2 = L_50;
			goto IL_0163;
		}
	}
	{
		IntPtr_t L_52;
		L_52.set_m_value_0((void*)(void*)Lexer_State11_m3650526788_MethodInfo_var);
		StateHandler_t3261942315 * L_53 = (StateHandler_t3261942315 *)il2cpp_codegen_object_new(StateHandler_t3261942315_il2cpp_TypeInfo_var);
		StateHandler__ctor_m3491238465(L_53, NULL, L_52, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cacheA_24(L_53);
		G_B22_0 = G_B21_0;
		G_B22_1 = G_B21_1;
		G_B22_2 = G_B21_2;
	}

IL_0163:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		StateHandler_t3261942315 * L_54 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cacheA_24();
		NullCheck(G_B22_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B22_1, G_B22_0);
		ArrayElementTypeCheck (G_B22_1, L_54);
		(G_B22_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B22_0), (StateHandler_t3261942315 *)L_54);
		StateHandlerU5BU5D_t2657855690* L_55 = G_B22_2;
		StateHandler_t3261942315 * L_56 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cacheB_25();
		G_B23_0 = ((int32_t)11);
		G_B23_1 = L_55;
		G_B23_2 = L_55;
		if (L_56)
		{
			G_B24_0 = ((int32_t)11);
			G_B24_1 = L_55;
			G_B24_2 = L_55;
			goto IL_0184;
		}
	}
	{
		IntPtr_t L_57;
		L_57.set_m_value_0((void*)(void*)Lexer_State12_m3861877701_MethodInfo_var);
		StateHandler_t3261942315 * L_58 = (StateHandler_t3261942315 *)il2cpp_codegen_object_new(StateHandler_t3261942315_il2cpp_TypeInfo_var);
		StateHandler__ctor_m3491238465(L_58, NULL, L_57, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cacheB_25(L_58);
		G_B24_0 = G_B23_0;
		G_B24_1 = G_B23_1;
		G_B24_2 = G_B23_2;
	}

IL_0184:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		StateHandler_t3261942315 * L_59 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cacheB_25();
		NullCheck(G_B24_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B24_1, G_B24_0);
		ArrayElementTypeCheck (G_B24_1, L_59);
		(G_B24_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B24_0), (StateHandler_t3261942315 *)L_59);
		StateHandlerU5BU5D_t2657855690* L_60 = G_B24_2;
		StateHandler_t3261942315 * L_61 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cacheC_26();
		G_B25_0 = ((int32_t)12);
		G_B25_1 = L_60;
		G_B25_2 = L_60;
		if (L_61)
		{
			G_B26_0 = ((int32_t)12);
			G_B26_1 = L_60;
			G_B26_2 = L_60;
			goto IL_01a5;
		}
	}
	{
		IntPtr_t L_62;
		L_62.set_m_value_0((void*)(void*)Lexer_State13_m4073228614_MethodInfo_var);
		StateHandler_t3261942315 * L_63 = (StateHandler_t3261942315 *)il2cpp_codegen_object_new(StateHandler_t3261942315_il2cpp_TypeInfo_var);
		StateHandler__ctor_m3491238465(L_63, NULL, L_62, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cacheC_26(L_63);
		G_B26_0 = G_B25_0;
		G_B26_1 = G_B25_1;
		G_B26_2 = G_B25_2;
	}

IL_01a5:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		StateHandler_t3261942315 * L_64 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cacheC_26();
		NullCheck(G_B26_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B26_1, G_B26_0);
		ArrayElementTypeCheck (G_B26_1, L_64);
		(G_B26_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B26_0), (StateHandler_t3261942315 *)L_64);
		StateHandlerU5BU5D_t2657855690* L_65 = G_B26_2;
		StateHandler_t3261942315 * L_66 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cacheD_27();
		G_B27_0 = ((int32_t)13);
		G_B27_1 = L_65;
		G_B27_2 = L_65;
		if (L_66)
		{
			G_B28_0 = ((int32_t)13);
			G_B28_1 = L_65;
			G_B28_2 = L_65;
			goto IL_01c6;
		}
	}
	{
		IntPtr_t L_67;
		L_67.set_m_value_0((void*)(void*)Lexer_State14_m4284579527_MethodInfo_var);
		StateHandler_t3261942315 * L_68 = (StateHandler_t3261942315 *)il2cpp_codegen_object_new(StateHandler_t3261942315_il2cpp_TypeInfo_var);
		StateHandler__ctor_m3491238465(L_68, NULL, L_67, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cacheD_27(L_68);
		G_B28_0 = G_B27_0;
		G_B28_1 = G_B27_1;
		G_B28_2 = G_B27_2;
	}

IL_01c6:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		StateHandler_t3261942315 * L_69 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cacheD_27();
		NullCheck(G_B28_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B28_1, G_B28_0);
		ArrayElementTypeCheck (G_B28_1, L_69);
		(G_B28_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B28_0), (StateHandler_t3261942315 *)L_69);
		StateHandlerU5BU5D_t2657855690* L_70 = G_B28_2;
		StateHandler_t3261942315 * L_71 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cacheE_28();
		G_B29_0 = ((int32_t)14);
		G_B29_1 = L_70;
		G_B29_2 = L_70;
		if (L_71)
		{
			G_B30_0 = ((int32_t)14);
			G_B30_1 = L_70;
			G_B30_2 = L_70;
			goto IL_01e7;
		}
	}
	{
		IntPtr_t L_72;
		L_72.set_m_value_0((void*)(void*)Lexer_State15_m200963144_MethodInfo_var);
		StateHandler_t3261942315 * L_73 = (StateHandler_t3261942315 *)il2cpp_codegen_object_new(StateHandler_t3261942315_il2cpp_TypeInfo_var);
		StateHandler__ctor_m3491238465(L_73, NULL, L_72, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cacheE_28(L_73);
		G_B30_0 = G_B29_0;
		G_B30_1 = G_B29_1;
		G_B30_2 = G_B29_2;
	}

IL_01e7:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		StateHandler_t3261942315 * L_74 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cacheE_28();
		NullCheck(G_B30_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B30_1, G_B30_0);
		ArrayElementTypeCheck (G_B30_1, L_74);
		(G_B30_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B30_0), (StateHandler_t3261942315 *)L_74);
		StateHandlerU5BU5D_t2657855690* L_75 = G_B30_2;
		StateHandler_t3261942315 * L_76 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cacheF_29();
		G_B31_0 = ((int32_t)15);
		G_B31_1 = L_75;
		G_B31_2 = L_75;
		if (L_76)
		{
			G_B32_0 = ((int32_t)15);
			G_B32_1 = L_75;
			G_B32_2 = L_75;
			goto IL_0208;
		}
	}
	{
		IntPtr_t L_77;
		L_77.set_m_value_0((void*)(void*)Lexer_State16_m412314057_MethodInfo_var);
		StateHandler_t3261942315 * L_78 = (StateHandler_t3261942315 *)il2cpp_codegen_object_new(StateHandler_t3261942315_il2cpp_TypeInfo_var);
		StateHandler__ctor_m3491238465(L_78, NULL, L_77, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cacheF_29(L_78);
		G_B32_0 = G_B31_0;
		G_B32_1 = G_B31_1;
		G_B32_2 = G_B31_2;
	}

IL_0208:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		StateHandler_t3261942315 * L_79 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cacheF_29();
		NullCheck(G_B32_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B32_1, G_B32_0);
		ArrayElementTypeCheck (G_B32_1, L_79);
		(G_B32_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B32_0), (StateHandler_t3261942315 *)L_79);
		StateHandlerU5BU5D_t2657855690* L_80 = G_B32_2;
		StateHandler_t3261942315 * L_81 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache10_30();
		G_B33_0 = ((int32_t)16);
		G_B33_1 = L_80;
		G_B33_2 = L_80;
		if (L_81)
		{
			G_B34_0 = ((int32_t)16);
			G_B34_1 = L_80;
			G_B34_2 = L_80;
			goto IL_0229;
		}
	}
	{
		IntPtr_t L_82;
		L_82.set_m_value_0((void*)(void*)Lexer_State17_m623664970_MethodInfo_var);
		StateHandler_t3261942315 * L_83 = (StateHandler_t3261942315 *)il2cpp_codegen_object_new(StateHandler_t3261942315_il2cpp_TypeInfo_var);
		StateHandler__ctor_m3491238465(L_83, NULL, L_82, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache10_30(L_83);
		G_B34_0 = G_B33_0;
		G_B34_1 = G_B33_1;
		G_B34_2 = G_B33_2;
	}

IL_0229:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		StateHandler_t3261942315 * L_84 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache10_30();
		NullCheck(G_B34_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B34_1, G_B34_0);
		ArrayElementTypeCheck (G_B34_1, L_84);
		(G_B34_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B34_0), (StateHandler_t3261942315 *)L_84);
		StateHandlerU5BU5D_t2657855690* L_85 = G_B34_2;
		StateHandler_t3261942315 * L_86 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache11_31();
		G_B35_0 = ((int32_t)17);
		G_B35_1 = L_85;
		G_B35_2 = L_85;
		if (L_86)
		{
			G_B36_0 = ((int32_t)17);
			G_B36_1 = L_85;
			G_B36_2 = L_85;
			goto IL_024a;
		}
	}
	{
		IntPtr_t L_87;
		L_87.set_m_value_0((void*)(void*)Lexer_State18_m835015883_MethodInfo_var);
		StateHandler_t3261942315 * L_88 = (StateHandler_t3261942315 *)il2cpp_codegen_object_new(StateHandler_t3261942315_il2cpp_TypeInfo_var);
		StateHandler__ctor_m3491238465(L_88, NULL, L_87, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache11_31(L_88);
		G_B36_0 = G_B35_0;
		G_B36_1 = G_B35_1;
		G_B36_2 = G_B35_2;
	}

IL_024a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		StateHandler_t3261942315 * L_89 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache11_31();
		NullCheck(G_B36_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B36_1, G_B36_0);
		ArrayElementTypeCheck (G_B36_1, L_89);
		(G_B36_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B36_0), (StateHandler_t3261942315 *)L_89);
		StateHandlerU5BU5D_t2657855690* L_90 = G_B36_2;
		StateHandler_t3261942315 * L_91 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache12_32();
		G_B37_0 = ((int32_t)18);
		G_B37_1 = L_90;
		G_B37_2 = L_90;
		if (L_91)
		{
			G_B38_0 = ((int32_t)18);
			G_B38_1 = L_90;
			G_B38_2 = L_90;
			goto IL_026b;
		}
	}
	{
		IntPtr_t L_92;
		L_92.set_m_value_0((void*)(void*)Lexer_State19_m1046366796_MethodInfo_var);
		StateHandler_t3261942315 * L_93 = (StateHandler_t3261942315 *)il2cpp_codegen_object_new(StateHandler_t3261942315_il2cpp_TypeInfo_var);
		StateHandler__ctor_m3491238465(L_93, NULL, L_92, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache12_32(L_93);
		G_B38_0 = G_B37_0;
		G_B38_1 = G_B37_1;
		G_B38_2 = G_B37_2;
	}

IL_026b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		StateHandler_t3261942315 * L_94 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache12_32();
		NullCheck(G_B38_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B38_1, G_B38_0);
		ArrayElementTypeCheck (G_B38_1, L_94);
		(G_B38_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B38_0), (StateHandler_t3261942315 *)L_94);
		StateHandlerU5BU5D_t2657855690* L_95 = G_B38_2;
		StateHandler_t3261942315 * L_96 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache13_33();
		G_B39_0 = ((int32_t)19);
		G_B39_1 = L_95;
		G_B39_2 = L_95;
		if (L_96)
		{
			G_B40_0 = ((int32_t)19);
			G_B40_1 = L_95;
			G_B40_2 = L_95;
			goto IL_028c;
		}
	}
	{
		IntPtr_t L_97;
		L_97.set_m_value_0((void*)(void*)Lexer_State20_m1401119586_MethodInfo_var);
		StateHandler_t3261942315 * L_98 = (StateHandler_t3261942315 *)il2cpp_codegen_object_new(StateHandler_t3261942315_il2cpp_TypeInfo_var);
		StateHandler__ctor_m3491238465(L_98, NULL, L_97, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache13_33(L_98);
		G_B40_0 = G_B39_0;
		G_B40_1 = G_B39_1;
		G_B40_2 = G_B39_2;
	}

IL_028c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		StateHandler_t3261942315 * L_99 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache13_33();
		NullCheck(G_B40_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B40_1, G_B40_0);
		ArrayElementTypeCheck (G_B40_1, L_99);
		(G_B40_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B40_0), (StateHandler_t3261942315 *)L_99);
		StateHandlerU5BU5D_t2657855690* L_100 = G_B40_2;
		StateHandler_t3261942315 * L_101 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache14_34();
		G_B41_0 = ((int32_t)20);
		G_B41_1 = L_100;
		G_B41_2 = L_100;
		if (L_101)
		{
			G_B42_0 = ((int32_t)20);
			G_B42_1 = L_100;
			G_B42_2 = L_100;
			goto IL_02ad;
		}
	}
	{
		IntPtr_t L_102;
		L_102.set_m_value_0((void*)(void*)Lexer_State21_m1612470499_MethodInfo_var);
		StateHandler_t3261942315 * L_103 = (StateHandler_t3261942315 *)il2cpp_codegen_object_new(StateHandler_t3261942315_il2cpp_TypeInfo_var);
		StateHandler__ctor_m3491238465(L_103, NULL, L_102, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache14_34(L_103);
		G_B42_0 = G_B41_0;
		G_B42_1 = G_B41_1;
		G_B42_2 = G_B41_2;
	}

IL_02ad:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		StateHandler_t3261942315 * L_104 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache14_34();
		NullCheck(G_B42_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B42_1, G_B42_0);
		ArrayElementTypeCheck (G_B42_1, L_104);
		(G_B42_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B42_0), (StateHandler_t3261942315 *)L_104);
		StateHandlerU5BU5D_t2657855690* L_105 = G_B42_2;
		StateHandler_t3261942315 * L_106 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache15_35();
		G_B43_0 = ((int32_t)21);
		G_B43_1 = L_105;
		G_B43_2 = L_105;
		if (L_106)
		{
			G_B44_0 = ((int32_t)21);
			G_B44_1 = L_105;
			G_B44_2 = L_105;
			goto IL_02ce;
		}
	}
	{
		IntPtr_t L_107;
		L_107.set_m_value_0((void*)(void*)Lexer_State22_m1823821412_MethodInfo_var);
		StateHandler_t3261942315 * L_108 = (StateHandler_t3261942315 *)il2cpp_codegen_object_new(StateHandler_t3261942315_il2cpp_TypeInfo_var);
		StateHandler__ctor_m3491238465(L_108, NULL, L_107, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache15_35(L_108);
		G_B44_0 = G_B43_0;
		G_B44_1 = G_B43_1;
		G_B44_2 = G_B43_2;
	}

IL_02ce:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		StateHandler_t3261942315 * L_109 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache15_35();
		NullCheck(G_B44_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B44_1, G_B44_0);
		ArrayElementTypeCheck (G_B44_1, L_109);
		(G_B44_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B44_0), (StateHandler_t3261942315 *)L_109);
		StateHandlerU5BU5D_t2657855690* L_110 = G_B44_2;
		StateHandler_t3261942315 * L_111 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache16_36();
		G_B45_0 = ((int32_t)22);
		G_B45_1 = L_110;
		G_B45_2 = L_110;
		if (L_111)
		{
			G_B46_0 = ((int32_t)22);
			G_B46_1 = L_110;
			G_B46_2 = L_110;
			goto IL_02ef;
		}
	}
	{
		IntPtr_t L_112;
		L_112.set_m_value_0((void*)(void*)Lexer_State23_m2035172325_MethodInfo_var);
		StateHandler_t3261942315 * L_113 = (StateHandler_t3261942315 *)il2cpp_codegen_object_new(StateHandler_t3261942315_il2cpp_TypeInfo_var);
		StateHandler__ctor_m3491238465(L_113, NULL, L_112, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache16_36(L_113);
		G_B46_0 = G_B45_0;
		G_B46_1 = G_B45_1;
		G_B46_2 = G_B45_2;
	}

IL_02ef:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		StateHandler_t3261942315 * L_114 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache16_36();
		NullCheck(G_B46_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B46_1, G_B46_0);
		ArrayElementTypeCheck (G_B46_1, L_114);
		(G_B46_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B46_0), (StateHandler_t3261942315 *)L_114);
		StateHandlerU5BU5D_t2657855690* L_115 = G_B46_2;
		StateHandler_t3261942315 * L_116 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache17_37();
		G_B47_0 = ((int32_t)23);
		G_B47_1 = L_115;
		G_B47_2 = L_115;
		if (L_116)
		{
			G_B48_0 = ((int32_t)23);
			G_B48_1 = L_115;
			G_B48_2 = L_115;
			goto IL_0310;
		}
	}
	{
		IntPtr_t L_117;
		L_117.set_m_value_0((void*)(void*)Lexer_State24_m2246523238_MethodInfo_var);
		StateHandler_t3261942315 * L_118 = (StateHandler_t3261942315 *)il2cpp_codegen_object_new(StateHandler_t3261942315_il2cpp_TypeInfo_var);
		StateHandler__ctor_m3491238465(L_118, NULL, L_117, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache17_37(L_118);
		G_B48_0 = G_B47_0;
		G_B48_1 = G_B47_1;
		G_B48_2 = G_B47_2;
	}

IL_0310:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		StateHandler_t3261942315 * L_119 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache17_37();
		NullCheck(G_B48_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B48_1, G_B48_0);
		ArrayElementTypeCheck (G_B48_1, L_119);
		(G_B48_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B48_0), (StateHandler_t3261942315 *)L_119);
		StateHandlerU5BU5D_t2657855690* L_120 = G_B48_2;
		StateHandler_t3261942315 * L_121 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache18_38();
		G_B49_0 = ((int32_t)24);
		G_B49_1 = L_120;
		G_B49_2 = L_120;
		if (L_121)
		{
			G_B50_0 = ((int32_t)24);
			G_B50_1 = L_120;
			G_B50_2 = L_120;
			goto IL_0331;
		}
	}
	{
		IntPtr_t L_122;
		L_122.set_m_value_0((void*)(void*)Lexer_State25_m2457874151_MethodInfo_var);
		StateHandler_t3261942315 * L_123 = (StateHandler_t3261942315 *)il2cpp_codegen_object_new(StateHandler_t3261942315_il2cpp_TypeInfo_var);
		StateHandler__ctor_m3491238465(L_123, NULL, L_122, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache18_38(L_123);
		G_B50_0 = G_B49_0;
		G_B50_1 = G_B49_1;
		G_B50_2 = G_B49_2;
	}

IL_0331:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		StateHandler_t3261942315 * L_124 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache18_38();
		NullCheck(G_B50_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B50_1, G_B50_0);
		ArrayElementTypeCheck (G_B50_1, L_124);
		(G_B50_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B50_0), (StateHandler_t3261942315 *)L_124);
		StateHandlerU5BU5D_t2657855690* L_125 = G_B50_2;
		StateHandler_t3261942315 * L_126 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache19_39();
		G_B51_0 = ((int32_t)25);
		G_B51_1 = L_125;
		G_B51_2 = L_125;
		if (L_126)
		{
			G_B52_0 = ((int32_t)25);
			G_B52_1 = L_125;
			G_B52_2 = L_125;
			goto IL_0352;
		}
	}
	{
		IntPtr_t L_127;
		L_127.set_m_value_0((void*)(void*)Lexer_State26_m2669225064_MethodInfo_var);
		StateHandler_t3261942315 * L_128 = (StateHandler_t3261942315 *)il2cpp_codegen_object_new(StateHandler_t3261942315_il2cpp_TypeInfo_var);
		StateHandler__ctor_m3491238465(L_128, NULL, L_127, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache19_39(L_128);
		G_B52_0 = G_B51_0;
		G_B52_1 = G_B51_1;
		G_B52_2 = G_B51_2;
	}

IL_0352:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		StateHandler_t3261942315 * L_129 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache19_39();
		NullCheck(G_B52_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B52_1, G_B52_0);
		ArrayElementTypeCheck (G_B52_1, L_129);
		(G_B52_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B52_0), (StateHandler_t3261942315 *)L_129);
		StateHandlerU5BU5D_t2657855690* L_130 = G_B52_2;
		StateHandler_t3261942315 * L_131 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache1A_40();
		G_B53_0 = ((int32_t)26);
		G_B53_1 = L_130;
		G_B53_2 = L_130;
		if (L_131)
		{
			G_B54_0 = ((int32_t)26);
			G_B54_1 = L_130;
			G_B54_2 = L_130;
			goto IL_0373;
		}
	}
	{
		IntPtr_t L_132;
		L_132.set_m_value_0((void*)(void*)Lexer_State27_m2880575977_MethodInfo_var);
		StateHandler_t3261942315 * L_133 = (StateHandler_t3261942315 *)il2cpp_codegen_object_new(StateHandler_t3261942315_il2cpp_TypeInfo_var);
		StateHandler__ctor_m3491238465(L_133, NULL, L_132, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache1A_40(L_133);
		G_B54_0 = G_B53_0;
		G_B54_1 = G_B53_1;
		G_B54_2 = G_B53_2;
	}

IL_0373:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		StateHandler_t3261942315 * L_134 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache1A_40();
		NullCheck(G_B54_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B54_1, G_B54_0);
		ArrayElementTypeCheck (G_B54_1, L_134);
		(G_B54_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B54_0), (StateHandler_t3261942315 *)L_134);
		StateHandlerU5BU5D_t2657855690* L_135 = G_B54_2;
		StateHandler_t3261942315 * L_136 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache1B_41();
		G_B55_0 = ((int32_t)27);
		G_B55_1 = L_135;
		G_B55_2 = L_135;
		if (L_136)
		{
			G_B56_0 = ((int32_t)27);
			G_B56_1 = L_135;
			G_B56_2 = L_135;
			goto IL_0394;
		}
	}
	{
		IntPtr_t L_137;
		L_137.set_m_value_0((void*)(void*)Lexer_State28_m3091926890_MethodInfo_var);
		StateHandler_t3261942315 * L_138 = (StateHandler_t3261942315 *)il2cpp_codegen_object_new(StateHandler_t3261942315_il2cpp_TypeInfo_var);
		StateHandler__ctor_m3491238465(L_138, NULL, L_137, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache1B_41(L_138);
		G_B56_0 = G_B55_0;
		G_B56_1 = G_B55_1;
		G_B56_2 = G_B55_2;
	}

IL_0394:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		StateHandler_t3261942315 * L_139 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache1B_41();
		NullCheck(G_B56_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B56_1, G_B56_0);
		ArrayElementTypeCheck (G_B56_1, L_139);
		(G_B56_1)->SetAt(static_cast<il2cpp_array_size_t>(G_B56_0), (StateHandler_t3261942315 *)L_139);
		((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->set_fsm_handler_table_1(G_B56_2);
		Int32U5BU5D_t3230847821* L_140 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)((int32_t)28)));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_140, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422____U24fieldU2DC_12_FieldInfo_var), /*hidden argument*/NULL);
		((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->set_fsm_return_table_0(L_140);
		return;
	}
}
// System.Char LitJson.Lexer::ProcessEscChar(System.Int32)
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern const uint32_t Lexer_ProcessEscChar_m2805824502_MetadataUsageId;
extern "C"  Il2CppChar Lexer_ProcessEscChar_m2805824502 (Il2CppObject * __this /* static, unused */, int32_t ___esc_char0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Lexer_ProcessEscChar_m2805824502_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___esc_char0;
		if (((int32_t)((int32_t)L_0-(int32_t)((int32_t)114))) == 0)
		{
			goto IL_005f;
		}
		if (((int32_t)((int32_t)L_0-(int32_t)((int32_t)114))) == 1)
		{
			goto IL_0015;
		}
		if (((int32_t)((int32_t)L_0-(int32_t)((int32_t)114))) == 2)
		{
			goto IL_005c;
		}
	}

IL_0015:
	{
		int32_t L_1 = ___esc_char0;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)34))))
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_2 = ___esc_char0;
		if ((((int32_t)L_2) == ((int32_t)((int32_t)39))))
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_3 = ___esc_char0;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)47))))
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_4 = ___esc_char0;
		if ((((int32_t)L_4) == ((int32_t)((int32_t)92))))
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_5 = ___esc_char0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)98))))
		{
			goto IL_0062;
		}
	}
	{
		int32_t L_6 = ___esc_char0;
		if ((((int32_t)L_6) == ((int32_t)((int32_t)102))))
		{
			goto IL_0064;
		}
	}
	{
		int32_t L_7 = ___esc_char0;
		if ((((int32_t)L_7) == ((int32_t)((int32_t)110))))
		{
			goto IL_0059;
		}
	}
	{
		goto IL_0067;
	}

IL_0052:
	{
		int32_t L_8 = ___esc_char0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		Il2CppChar L_9 = Convert_ToChar_m353236550(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return L_9;
	}

IL_0059:
	{
		return ((int32_t)10);
	}

IL_005c:
	{
		return ((int32_t)9);
	}

IL_005f:
	{
		return ((int32_t)13);
	}

IL_0062:
	{
		return 8;
	}

IL_0064:
	{
		return ((int32_t)12);
	}

IL_0067:
	{
		return ((int32_t)63);
	}
}
// System.Boolean LitJson.Lexer::State1(LitJson.FsmContext)
extern "C"  bool Lexer_State1_m1833119603 (Il2CppObject * __this /* static, unused */, FsmContext_t3936467683 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		goto IL_01eb;
	}

IL_0005:
	{
		FsmContext_t3936467683 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t3664372066 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_input_char_7();
		if ((((int32_t)L_2) == ((int32_t)((int32_t)32))))
		{
			goto IL_003b;
		}
	}
	{
		FsmContext_t3936467683 * L_3 = ___ctx0;
		NullCheck(L_3);
		Lexer_t3664372066 * L_4 = L_3->get_L_2();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_input_char_7();
		if ((((int32_t)L_5) < ((int32_t)((int32_t)9))))
		{
			goto IL_0040;
		}
	}
	{
		FsmContext_t3936467683 * L_6 = ___ctx0;
		NullCheck(L_6);
		Lexer_t3664372066 * L_7 = L_6->get_L_2();
		NullCheck(L_7);
		int32_t L_8 = L_7->get_input_char_7();
		if ((((int32_t)L_8) > ((int32_t)((int32_t)13))))
		{
			goto IL_0040;
		}
	}

IL_003b:
	{
		goto IL_01eb;
	}

IL_0040:
	{
		FsmContext_t3936467683 * L_9 = ___ctx0;
		NullCheck(L_9);
		Lexer_t3664372066 * L_10 = L_9->get_L_2();
		NullCheck(L_10);
		int32_t L_11 = L_10->get_input_char_7();
		if ((((int32_t)L_11) < ((int32_t)((int32_t)49))))
		{
			goto IL_008a;
		}
	}
	{
		FsmContext_t3936467683 * L_12 = ___ctx0;
		NullCheck(L_12);
		Lexer_t3664372066 * L_13 = L_12->get_L_2();
		NullCheck(L_13);
		int32_t L_14 = L_13->get_input_char_7();
		if ((((int32_t)L_14) > ((int32_t)((int32_t)57))))
		{
			goto IL_008a;
		}
	}
	{
		FsmContext_t3936467683 * L_15 = ___ctx0;
		NullCheck(L_15);
		Lexer_t3664372066 * L_16 = L_15->get_L_2();
		NullCheck(L_16);
		StringBuilder_t243639308 * L_17 = L_16->get_string_buffer_10();
		FsmContext_t3936467683 * L_18 = ___ctx0;
		NullCheck(L_18);
		Lexer_t3664372066 * L_19 = L_18->get_L_2();
		NullCheck(L_19);
		int32_t L_20 = L_19->get_input_char_7();
		NullCheck(L_17);
		StringBuilder_Append_m2143093878(L_17, (((int32_t)((uint16_t)L_20))), /*hidden argument*/NULL);
		FsmContext_t3936467683 * L_21 = ___ctx0;
		NullCheck(L_21);
		L_21->set_NextState_1(3);
		return (bool)1;
	}

IL_008a:
	{
		FsmContext_t3936467683 * L_22 = ___ctx0;
		NullCheck(L_22);
		Lexer_t3664372066 * L_23 = L_22->get_L_2();
		NullCheck(L_23);
		int32_t L_24 = L_23->get_input_char_7();
		V_0 = L_24;
		int32_t L_25 = V_0;
		if (((int32_t)((int32_t)L_25-(int32_t)((int32_t)44))) == 0)
		{
			goto IL_0123;
		}
		if (((int32_t)((int32_t)L_25-(int32_t)((int32_t)44))) == 1)
		{
			goto IL_0133;
		}
		if (((int32_t)((int32_t)L_25-(int32_t)((int32_t)44))) == 2)
		{
			goto IL_00b3;
		}
		if (((int32_t)((int32_t)L_25-(int32_t)((int32_t)44))) == 3)
		{
			goto IL_01cd;
		}
		if (((int32_t)((int32_t)L_25-(int32_t)((int32_t)44))) == 4)
		{
			goto IL_0159;
		}
	}

IL_00b3:
	{
		int32_t L_26 = V_0;
		if (((int32_t)((int32_t)L_26-(int32_t)((int32_t)91))) == 0)
		{
			goto IL_0123;
		}
		if (((int32_t)((int32_t)L_26-(int32_t)((int32_t)91))) == 1)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_26-(int32_t)((int32_t)91))) == 2)
		{
			goto IL_0123;
		}
	}

IL_00c8:
	{
		int32_t L_27 = V_0;
		if (((int32_t)((int32_t)L_27-(int32_t)((int32_t)123))) == 0)
		{
			goto IL_0123;
		}
		if (((int32_t)((int32_t)L_27-(int32_t)((int32_t)123))) == 1)
		{
			goto IL_00dd;
		}
		if (((int32_t)((int32_t)L_27-(int32_t)((int32_t)123))) == 2)
		{
			goto IL_0123;
		}
	}

IL_00dd:
	{
		int32_t L_28 = V_0;
		if ((((int32_t)L_28) == ((int32_t)((int32_t)34))))
		{
			goto IL_0112;
		}
	}
	{
		int32_t L_29 = V_0;
		if ((((int32_t)L_29) == ((int32_t)((int32_t)39))))
		{
			goto IL_019d;
		}
	}
	{
		int32_t L_30 = V_0;
		if ((((int32_t)L_30) == ((int32_t)((int32_t)58))))
		{
			goto IL_0123;
		}
	}
	{
		int32_t L_31 = V_0;
		if ((((int32_t)L_31) == ((int32_t)((int32_t)102))))
		{
			goto IL_017f;
		}
	}
	{
		int32_t L_32 = V_0;
		if ((((int32_t)L_32) == ((int32_t)((int32_t)110))))
		{
			goto IL_0189;
		}
	}
	{
		int32_t L_33 = V_0;
		if ((((int32_t)L_33) == ((int32_t)((int32_t)116))))
		{
			goto IL_0193;
		}
	}
	{
		goto IL_01e9;
	}

IL_0112:
	{
		FsmContext_t3936467683 * L_34 = ___ctx0;
		NullCheck(L_34);
		L_34->set_NextState_1(((int32_t)19));
		FsmContext_t3936467683 * L_35 = ___ctx0;
		NullCheck(L_35);
		L_35->set_Return_0((bool)1);
		return (bool)1;
	}

IL_0123:
	{
		FsmContext_t3936467683 * L_36 = ___ctx0;
		NullCheck(L_36);
		L_36->set_NextState_1(1);
		FsmContext_t3936467683 * L_37 = ___ctx0;
		NullCheck(L_37);
		L_37->set_Return_0((bool)1);
		return (bool)1;
	}

IL_0133:
	{
		FsmContext_t3936467683 * L_38 = ___ctx0;
		NullCheck(L_38);
		Lexer_t3664372066 * L_39 = L_38->get_L_2();
		NullCheck(L_39);
		StringBuilder_t243639308 * L_40 = L_39->get_string_buffer_10();
		FsmContext_t3936467683 * L_41 = ___ctx0;
		NullCheck(L_41);
		Lexer_t3664372066 * L_42 = L_41->get_L_2();
		NullCheck(L_42);
		int32_t L_43 = L_42->get_input_char_7();
		NullCheck(L_40);
		StringBuilder_Append_m2143093878(L_40, (((int32_t)((uint16_t)L_43))), /*hidden argument*/NULL);
		FsmContext_t3936467683 * L_44 = ___ctx0;
		NullCheck(L_44);
		L_44->set_NextState_1(2);
		return (bool)1;
	}

IL_0159:
	{
		FsmContext_t3936467683 * L_45 = ___ctx0;
		NullCheck(L_45);
		Lexer_t3664372066 * L_46 = L_45->get_L_2();
		NullCheck(L_46);
		StringBuilder_t243639308 * L_47 = L_46->get_string_buffer_10();
		FsmContext_t3936467683 * L_48 = ___ctx0;
		NullCheck(L_48);
		Lexer_t3664372066 * L_49 = L_48->get_L_2();
		NullCheck(L_49);
		int32_t L_50 = L_49->get_input_char_7();
		NullCheck(L_47);
		StringBuilder_Append_m2143093878(L_47, (((int32_t)((uint16_t)L_50))), /*hidden argument*/NULL);
		FsmContext_t3936467683 * L_51 = ___ctx0;
		NullCheck(L_51);
		L_51->set_NextState_1(4);
		return (bool)1;
	}

IL_017f:
	{
		FsmContext_t3936467683 * L_52 = ___ctx0;
		NullCheck(L_52);
		L_52->set_NextState_1(((int32_t)12));
		return (bool)1;
	}

IL_0189:
	{
		FsmContext_t3936467683 * L_53 = ___ctx0;
		NullCheck(L_53);
		L_53->set_NextState_1(((int32_t)16));
		return (bool)1;
	}

IL_0193:
	{
		FsmContext_t3936467683 * L_54 = ___ctx0;
		NullCheck(L_54);
		L_54->set_NextState_1(((int32_t)9));
		return (bool)1;
	}

IL_019d:
	{
		FsmContext_t3936467683 * L_55 = ___ctx0;
		NullCheck(L_55);
		Lexer_t3664372066 * L_56 = L_55->get_L_2();
		NullCheck(L_56);
		bool L_57 = L_56->get_allow_single_quoted_strings_3();
		if (L_57)
		{
			goto IL_01af;
		}
	}
	{
		return (bool)0;
	}

IL_01af:
	{
		FsmContext_t3936467683 * L_58 = ___ctx0;
		NullCheck(L_58);
		Lexer_t3664372066 * L_59 = L_58->get_L_2();
		NullCheck(L_59);
		L_59->set_input_char_7(((int32_t)34));
		FsmContext_t3936467683 * L_60 = ___ctx0;
		NullCheck(L_60);
		L_60->set_NextState_1(((int32_t)23));
		FsmContext_t3936467683 * L_61 = ___ctx0;
		NullCheck(L_61);
		L_61->set_Return_0((bool)1);
		return (bool)1;
	}

IL_01cd:
	{
		FsmContext_t3936467683 * L_62 = ___ctx0;
		NullCheck(L_62);
		Lexer_t3664372066 * L_63 = L_62->get_L_2();
		NullCheck(L_63);
		bool L_64 = L_63->get_allow_comments_2();
		if (L_64)
		{
			goto IL_01df;
		}
	}
	{
		return (bool)0;
	}

IL_01df:
	{
		FsmContext_t3936467683 * L_65 = ___ctx0;
		NullCheck(L_65);
		L_65->set_NextState_1(((int32_t)25));
		return (bool)1;
	}

IL_01e9:
	{
		return (bool)0;
	}

IL_01eb:
	{
		FsmContext_t3936467683 * L_66 = ___ctx0;
		NullCheck(L_66);
		Lexer_t3664372066 * L_67 = L_66->get_L_2();
		NullCheck(L_67);
		bool L_68 = Lexer_GetChar_m4104177437(L_67, /*hidden argument*/NULL);
		if (L_68)
		{
			goto IL_0005;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean LitJson.Lexer::State2(LitJson.FsmContext)
extern "C"  bool Lexer_State2_m2044470516 (Il2CppObject * __this /* static, unused */, FsmContext_t3936467683 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t3936467683 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t3664372066 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m4104177437(L_1, /*hidden argument*/NULL);
		FsmContext_t3936467683 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t3664372066 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		if ((((int32_t)L_4) < ((int32_t)((int32_t)49))))
		{
			goto IL_0056;
		}
	}
	{
		FsmContext_t3936467683 * L_5 = ___ctx0;
		NullCheck(L_5);
		Lexer_t3664372066 * L_6 = L_5->get_L_2();
		NullCheck(L_6);
		int32_t L_7 = L_6->get_input_char_7();
		if ((((int32_t)L_7) > ((int32_t)((int32_t)57))))
		{
			goto IL_0056;
		}
	}
	{
		FsmContext_t3936467683 * L_8 = ___ctx0;
		NullCheck(L_8);
		Lexer_t3664372066 * L_9 = L_8->get_L_2();
		NullCheck(L_9);
		StringBuilder_t243639308 * L_10 = L_9->get_string_buffer_10();
		FsmContext_t3936467683 * L_11 = ___ctx0;
		NullCheck(L_11);
		Lexer_t3664372066 * L_12 = L_11->get_L_2();
		NullCheck(L_12);
		int32_t L_13 = L_12->get_input_char_7();
		NullCheck(L_10);
		StringBuilder_Append_m2143093878(L_10, (((int32_t)((uint16_t)L_13))), /*hidden argument*/NULL);
		FsmContext_t3936467683 * L_14 = ___ctx0;
		NullCheck(L_14);
		L_14->set_NextState_1(3);
		return (bool)1;
	}

IL_0056:
	{
		FsmContext_t3936467683 * L_15 = ___ctx0;
		NullCheck(L_15);
		Lexer_t3664372066 * L_16 = L_15->get_L_2();
		NullCheck(L_16);
		int32_t L_17 = L_16->get_input_char_7();
		V_0 = L_17;
		int32_t L_18 = V_0;
		if ((((int32_t)L_18) == ((int32_t)((int32_t)48))))
		{
			goto IL_006f;
		}
	}
	{
		goto IL_0095;
	}

IL_006f:
	{
		FsmContext_t3936467683 * L_19 = ___ctx0;
		NullCheck(L_19);
		Lexer_t3664372066 * L_20 = L_19->get_L_2();
		NullCheck(L_20);
		StringBuilder_t243639308 * L_21 = L_20->get_string_buffer_10();
		FsmContext_t3936467683 * L_22 = ___ctx0;
		NullCheck(L_22);
		Lexer_t3664372066 * L_23 = L_22->get_L_2();
		NullCheck(L_23);
		int32_t L_24 = L_23->get_input_char_7();
		NullCheck(L_21);
		StringBuilder_Append_m2143093878(L_21, (((int32_t)((uint16_t)L_24))), /*hidden argument*/NULL);
		FsmContext_t3936467683 * L_25 = ___ctx0;
		NullCheck(L_25);
		L_25->set_NextState_1(4);
		return (bool)1;
	}

IL_0095:
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.Lexer::State3(LitJson.FsmContext)
extern "C"  bool Lexer_State3_m2255821429 (Il2CppObject * __this /* static, unused */, FsmContext_t3936467683 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		goto IL_0140;
	}

IL_0005:
	{
		FsmContext_t3936467683 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t3664372066 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_input_char_7();
		if ((((int32_t)L_2) < ((int32_t)((int32_t)48))))
		{
			goto IL_004b;
		}
	}
	{
		FsmContext_t3936467683 * L_3 = ___ctx0;
		NullCheck(L_3);
		Lexer_t3664372066 * L_4 = L_3->get_L_2();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_input_char_7();
		if ((((int32_t)L_5) > ((int32_t)((int32_t)57))))
		{
			goto IL_004b;
		}
	}
	{
		FsmContext_t3936467683 * L_6 = ___ctx0;
		NullCheck(L_6);
		Lexer_t3664372066 * L_7 = L_6->get_L_2();
		NullCheck(L_7);
		StringBuilder_t243639308 * L_8 = L_7->get_string_buffer_10();
		FsmContext_t3936467683 * L_9 = ___ctx0;
		NullCheck(L_9);
		Lexer_t3664372066 * L_10 = L_9->get_L_2();
		NullCheck(L_10);
		int32_t L_11 = L_10->get_input_char_7();
		NullCheck(L_8);
		StringBuilder_Append_m2143093878(L_8, (((int32_t)((uint16_t)L_11))), /*hidden argument*/NULL);
		goto IL_0140;
	}

IL_004b:
	{
		FsmContext_t3936467683 * L_12 = ___ctx0;
		NullCheck(L_12);
		Lexer_t3664372066 * L_13 = L_12->get_L_2();
		NullCheck(L_13);
		int32_t L_14 = L_13->get_input_char_7();
		if ((((int32_t)L_14) == ((int32_t)((int32_t)32))))
		{
			goto IL_0081;
		}
	}
	{
		FsmContext_t3936467683 * L_15 = ___ctx0;
		NullCheck(L_15);
		Lexer_t3664372066 * L_16 = L_15->get_L_2();
		NullCheck(L_16);
		int32_t L_17 = L_16->get_input_char_7();
		if ((((int32_t)L_17) < ((int32_t)((int32_t)9))))
		{
			goto IL_0091;
		}
	}
	{
		FsmContext_t3936467683 * L_18 = ___ctx0;
		NullCheck(L_18);
		Lexer_t3664372066 * L_19 = L_18->get_L_2();
		NullCheck(L_19);
		int32_t L_20 = L_19->get_input_char_7();
		if ((((int32_t)L_20) > ((int32_t)((int32_t)13))))
		{
			goto IL_0091;
		}
	}

IL_0081:
	{
		FsmContext_t3936467683 * L_21 = ___ctx0;
		NullCheck(L_21);
		L_21->set_Return_0((bool)1);
		FsmContext_t3936467683 * L_22 = ___ctx0;
		NullCheck(L_22);
		L_22->set_NextState_1(1);
		return (bool)1;
	}

IL_0091:
	{
		FsmContext_t3936467683 * L_23 = ___ctx0;
		NullCheck(L_23);
		Lexer_t3664372066 * L_24 = L_23->get_L_2();
		NullCheck(L_24);
		int32_t L_25 = L_24->get_input_char_7();
		V_0 = L_25;
		int32_t L_26 = V_0;
		if (((int32_t)((int32_t)L_26-(int32_t)((int32_t)44))) == 0)
		{
			goto IL_00d7;
		}
		if (((int32_t)((int32_t)L_26-(int32_t)((int32_t)44))) == 1)
		{
			goto IL_00b2;
		}
		if (((int32_t)((int32_t)L_26-(int32_t)((int32_t)44))) == 2)
		{
			goto IL_00f2;
		}
	}

IL_00b2:
	{
		int32_t L_27 = V_0;
		if ((((int32_t)L_27) == ((int32_t)((int32_t)69))))
		{
			goto IL_0118;
		}
	}
	{
		int32_t L_28 = V_0;
		if ((((int32_t)L_28) == ((int32_t)((int32_t)93))))
		{
			goto IL_00d7;
		}
	}
	{
		int32_t L_29 = V_0;
		if ((((int32_t)L_29) == ((int32_t)((int32_t)101))))
		{
			goto IL_0118;
		}
	}
	{
		int32_t L_30 = V_0;
		if ((((int32_t)L_30) == ((int32_t)((int32_t)125))))
		{
			goto IL_00d7;
		}
	}
	{
		goto IL_013e;
	}

IL_00d7:
	{
		FsmContext_t3936467683 * L_31 = ___ctx0;
		NullCheck(L_31);
		Lexer_t3664372066 * L_32 = L_31->get_L_2();
		NullCheck(L_32);
		Lexer_UngetChar_m3833420334(L_32, /*hidden argument*/NULL);
		FsmContext_t3936467683 * L_33 = ___ctx0;
		NullCheck(L_33);
		L_33->set_Return_0((bool)1);
		FsmContext_t3936467683 * L_34 = ___ctx0;
		NullCheck(L_34);
		L_34->set_NextState_1(1);
		return (bool)1;
	}

IL_00f2:
	{
		FsmContext_t3936467683 * L_35 = ___ctx0;
		NullCheck(L_35);
		Lexer_t3664372066 * L_36 = L_35->get_L_2();
		NullCheck(L_36);
		StringBuilder_t243639308 * L_37 = L_36->get_string_buffer_10();
		FsmContext_t3936467683 * L_38 = ___ctx0;
		NullCheck(L_38);
		Lexer_t3664372066 * L_39 = L_38->get_L_2();
		NullCheck(L_39);
		int32_t L_40 = L_39->get_input_char_7();
		NullCheck(L_37);
		StringBuilder_Append_m2143093878(L_37, (((int32_t)((uint16_t)L_40))), /*hidden argument*/NULL);
		FsmContext_t3936467683 * L_41 = ___ctx0;
		NullCheck(L_41);
		L_41->set_NextState_1(5);
		return (bool)1;
	}

IL_0118:
	{
		FsmContext_t3936467683 * L_42 = ___ctx0;
		NullCheck(L_42);
		Lexer_t3664372066 * L_43 = L_42->get_L_2();
		NullCheck(L_43);
		StringBuilder_t243639308 * L_44 = L_43->get_string_buffer_10();
		FsmContext_t3936467683 * L_45 = ___ctx0;
		NullCheck(L_45);
		Lexer_t3664372066 * L_46 = L_45->get_L_2();
		NullCheck(L_46);
		int32_t L_47 = L_46->get_input_char_7();
		NullCheck(L_44);
		StringBuilder_Append_m2143093878(L_44, (((int32_t)((uint16_t)L_47))), /*hidden argument*/NULL);
		FsmContext_t3936467683 * L_48 = ___ctx0;
		NullCheck(L_48);
		L_48->set_NextState_1(7);
		return (bool)1;
	}

IL_013e:
	{
		return (bool)0;
	}

IL_0140:
	{
		FsmContext_t3936467683 * L_49 = ___ctx0;
		NullCheck(L_49);
		Lexer_t3664372066 * L_50 = L_49->get_L_2();
		NullCheck(L_50);
		bool L_51 = Lexer_GetChar_m4104177437(L_50, /*hidden argument*/NULL);
		if (L_51)
		{
			goto IL_0005;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean LitJson.Lexer::State4(LitJson.FsmContext)
extern "C"  bool Lexer_State4_m2467172342 (Il2CppObject * __this /* static, unused */, FsmContext_t3936467683 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t3936467683 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t3664372066 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m4104177437(L_1, /*hidden argument*/NULL);
		FsmContext_t3936467683 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t3664372066 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		if ((((int32_t)L_4) == ((int32_t)((int32_t)32))))
		{
			goto IL_0042;
		}
	}
	{
		FsmContext_t3936467683 * L_5 = ___ctx0;
		NullCheck(L_5);
		Lexer_t3664372066 * L_6 = L_5->get_L_2();
		NullCheck(L_6);
		int32_t L_7 = L_6->get_input_char_7();
		if ((((int32_t)L_7) < ((int32_t)((int32_t)9))))
		{
			goto IL_0052;
		}
	}
	{
		FsmContext_t3936467683 * L_8 = ___ctx0;
		NullCheck(L_8);
		Lexer_t3664372066 * L_9 = L_8->get_L_2();
		NullCheck(L_9);
		int32_t L_10 = L_9->get_input_char_7();
		if ((((int32_t)L_10) > ((int32_t)((int32_t)13))))
		{
			goto IL_0052;
		}
	}

IL_0042:
	{
		FsmContext_t3936467683 * L_11 = ___ctx0;
		NullCheck(L_11);
		L_11->set_Return_0((bool)1);
		FsmContext_t3936467683 * L_12 = ___ctx0;
		NullCheck(L_12);
		L_12->set_NextState_1(1);
		return (bool)1;
	}

IL_0052:
	{
		FsmContext_t3936467683 * L_13 = ___ctx0;
		NullCheck(L_13);
		Lexer_t3664372066 * L_14 = L_13->get_L_2();
		NullCheck(L_14);
		int32_t L_15 = L_14->get_input_char_7();
		V_0 = L_15;
		int32_t L_16 = V_0;
		if (((int32_t)((int32_t)L_16-(int32_t)((int32_t)44))) == 0)
		{
			goto IL_0098;
		}
		if (((int32_t)((int32_t)L_16-(int32_t)((int32_t)44))) == 1)
		{
			goto IL_0073;
		}
		if (((int32_t)((int32_t)L_16-(int32_t)((int32_t)44))) == 2)
		{
			goto IL_00b3;
		}
	}

IL_0073:
	{
		int32_t L_17 = V_0;
		if ((((int32_t)L_17) == ((int32_t)((int32_t)69))))
		{
			goto IL_00d9;
		}
	}
	{
		int32_t L_18 = V_0;
		if ((((int32_t)L_18) == ((int32_t)((int32_t)93))))
		{
			goto IL_0098;
		}
	}
	{
		int32_t L_19 = V_0;
		if ((((int32_t)L_19) == ((int32_t)((int32_t)101))))
		{
			goto IL_00d9;
		}
	}
	{
		int32_t L_20 = V_0;
		if ((((int32_t)L_20) == ((int32_t)((int32_t)125))))
		{
			goto IL_0098;
		}
	}
	{
		goto IL_00ff;
	}

IL_0098:
	{
		FsmContext_t3936467683 * L_21 = ___ctx0;
		NullCheck(L_21);
		Lexer_t3664372066 * L_22 = L_21->get_L_2();
		NullCheck(L_22);
		Lexer_UngetChar_m3833420334(L_22, /*hidden argument*/NULL);
		FsmContext_t3936467683 * L_23 = ___ctx0;
		NullCheck(L_23);
		L_23->set_Return_0((bool)1);
		FsmContext_t3936467683 * L_24 = ___ctx0;
		NullCheck(L_24);
		L_24->set_NextState_1(1);
		return (bool)1;
	}

IL_00b3:
	{
		FsmContext_t3936467683 * L_25 = ___ctx0;
		NullCheck(L_25);
		Lexer_t3664372066 * L_26 = L_25->get_L_2();
		NullCheck(L_26);
		StringBuilder_t243639308 * L_27 = L_26->get_string_buffer_10();
		FsmContext_t3936467683 * L_28 = ___ctx0;
		NullCheck(L_28);
		Lexer_t3664372066 * L_29 = L_28->get_L_2();
		NullCheck(L_29);
		int32_t L_30 = L_29->get_input_char_7();
		NullCheck(L_27);
		StringBuilder_Append_m2143093878(L_27, (((int32_t)((uint16_t)L_30))), /*hidden argument*/NULL);
		FsmContext_t3936467683 * L_31 = ___ctx0;
		NullCheck(L_31);
		L_31->set_NextState_1(5);
		return (bool)1;
	}

IL_00d9:
	{
		FsmContext_t3936467683 * L_32 = ___ctx0;
		NullCheck(L_32);
		Lexer_t3664372066 * L_33 = L_32->get_L_2();
		NullCheck(L_33);
		StringBuilder_t243639308 * L_34 = L_33->get_string_buffer_10();
		FsmContext_t3936467683 * L_35 = ___ctx0;
		NullCheck(L_35);
		Lexer_t3664372066 * L_36 = L_35->get_L_2();
		NullCheck(L_36);
		int32_t L_37 = L_36->get_input_char_7();
		NullCheck(L_34);
		StringBuilder_Append_m2143093878(L_34, (((int32_t)((uint16_t)L_37))), /*hidden argument*/NULL);
		FsmContext_t3936467683 * L_38 = ___ctx0;
		NullCheck(L_38);
		L_38->set_NextState_1(7);
		return (bool)1;
	}

IL_00ff:
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.Lexer::State5(LitJson.FsmContext)
extern "C"  bool Lexer_State5_m2678523255 (Il2CppObject * __this /* static, unused */, FsmContext_t3936467683 * ___ctx0, const MethodInfo* method)
{
	{
		FsmContext_t3936467683 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t3664372066 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m4104177437(L_1, /*hidden argument*/NULL);
		FsmContext_t3936467683 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t3664372066 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		if ((((int32_t)L_4) < ((int32_t)((int32_t)48))))
		{
			goto IL_0056;
		}
	}
	{
		FsmContext_t3936467683 * L_5 = ___ctx0;
		NullCheck(L_5);
		Lexer_t3664372066 * L_6 = L_5->get_L_2();
		NullCheck(L_6);
		int32_t L_7 = L_6->get_input_char_7();
		if ((((int32_t)L_7) > ((int32_t)((int32_t)57))))
		{
			goto IL_0056;
		}
	}
	{
		FsmContext_t3936467683 * L_8 = ___ctx0;
		NullCheck(L_8);
		Lexer_t3664372066 * L_9 = L_8->get_L_2();
		NullCheck(L_9);
		StringBuilder_t243639308 * L_10 = L_9->get_string_buffer_10();
		FsmContext_t3936467683 * L_11 = ___ctx0;
		NullCheck(L_11);
		Lexer_t3664372066 * L_12 = L_11->get_L_2();
		NullCheck(L_12);
		int32_t L_13 = L_12->get_input_char_7();
		NullCheck(L_10);
		StringBuilder_Append_m2143093878(L_10, (((int32_t)((uint16_t)L_13))), /*hidden argument*/NULL);
		FsmContext_t3936467683 * L_14 = ___ctx0;
		NullCheck(L_14);
		L_14->set_NextState_1(6);
		return (bool)1;
	}

IL_0056:
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.Lexer::State6(LitJson.FsmContext)
extern "C"  bool Lexer_State6_m2889874168 (Il2CppObject * __this /* static, unused */, FsmContext_t3936467683 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		goto IL_010d;
	}

IL_0005:
	{
		FsmContext_t3936467683 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t3664372066 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_input_char_7();
		if ((((int32_t)L_2) < ((int32_t)((int32_t)48))))
		{
			goto IL_004b;
		}
	}
	{
		FsmContext_t3936467683 * L_3 = ___ctx0;
		NullCheck(L_3);
		Lexer_t3664372066 * L_4 = L_3->get_L_2();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_input_char_7();
		if ((((int32_t)L_5) > ((int32_t)((int32_t)57))))
		{
			goto IL_004b;
		}
	}
	{
		FsmContext_t3936467683 * L_6 = ___ctx0;
		NullCheck(L_6);
		Lexer_t3664372066 * L_7 = L_6->get_L_2();
		NullCheck(L_7);
		StringBuilder_t243639308 * L_8 = L_7->get_string_buffer_10();
		FsmContext_t3936467683 * L_9 = ___ctx0;
		NullCheck(L_9);
		Lexer_t3664372066 * L_10 = L_9->get_L_2();
		NullCheck(L_10);
		int32_t L_11 = L_10->get_input_char_7();
		NullCheck(L_8);
		StringBuilder_Append_m2143093878(L_8, (((int32_t)((uint16_t)L_11))), /*hidden argument*/NULL);
		goto IL_010d;
	}

IL_004b:
	{
		FsmContext_t3936467683 * L_12 = ___ctx0;
		NullCheck(L_12);
		Lexer_t3664372066 * L_13 = L_12->get_L_2();
		NullCheck(L_13);
		int32_t L_14 = L_13->get_input_char_7();
		if ((((int32_t)L_14) == ((int32_t)((int32_t)32))))
		{
			goto IL_0081;
		}
	}
	{
		FsmContext_t3936467683 * L_15 = ___ctx0;
		NullCheck(L_15);
		Lexer_t3664372066 * L_16 = L_15->get_L_2();
		NullCheck(L_16);
		int32_t L_17 = L_16->get_input_char_7();
		if ((((int32_t)L_17) < ((int32_t)((int32_t)9))))
		{
			goto IL_0091;
		}
	}
	{
		FsmContext_t3936467683 * L_18 = ___ctx0;
		NullCheck(L_18);
		Lexer_t3664372066 * L_19 = L_18->get_L_2();
		NullCheck(L_19);
		int32_t L_20 = L_19->get_input_char_7();
		if ((((int32_t)L_20) > ((int32_t)((int32_t)13))))
		{
			goto IL_0091;
		}
	}

IL_0081:
	{
		FsmContext_t3936467683 * L_21 = ___ctx0;
		NullCheck(L_21);
		L_21->set_Return_0((bool)1);
		FsmContext_t3936467683 * L_22 = ___ctx0;
		NullCheck(L_22);
		L_22->set_NextState_1(1);
		return (bool)1;
	}

IL_0091:
	{
		FsmContext_t3936467683 * L_23 = ___ctx0;
		NullCheck(L_23);
		Lexer_t3664372066 * L_24 = L_23->get_L_2();
		NullCheck(L_24);
		int32_t L_25 = L_24->get_input_char_7();
		V_0 = L_25;
		int32_t L_26 = V_0;
		if ((((int32_t)L_26) == ((int32_t)((int32_t)44))))
		{
			goto IL_00ca;
		}
	}
	{
		int32_t L_27 = V_0;
		if ((((int32_t)L_27) == ((int32_t)((int32_t)69))))
		{
			goto IL_00e5;
		}
	}
	{
		int32_t L_28 = V_0;
		if ((((int32_t)L_28) == ((int32_t)((int32_t)93))))
		{
			goto IL_00ca;
		}
	}
	{
		int32_t L_29 = V_0;
		if ((((int32_t)L_29) == ((int32_t)((int32_t)101))))
		{
			goto IL_00e5;
		}
	}
	{
		int32_t L_30 = V_0;
		if ((((int32_t)L_30) == ((int32_t)((int32_t)125))))
		{
			goto IL_00ca;
		}
	}
	{
		goto IL_010b;
	}

IL_00ca:
	{
		FsmContext_t3936467683 * L_31 = ___ctx0;
		NullCheck(L_31);
		Lexer_t3664372066 * L_32 = L_31->get_L_2();
		NullCheck(L_32);
		Lexer_UngetChar_m3833420334(L_32, /*hidden argument*/NULL);
		FsmContext_t3936467683 * L_33 = ___ctx0;
		NullCheck(L_33);
		L_33->set_Return_0((bool)1);
		FsmContext_t3936467683 * L_34 = ___ctx0;
		NullCheck(L_34);
		L_34->set_NextState_1(1);
		return (bool)1;
	}

IL_00e5:
	{
		FsmContext_t3936467683 * L_35 = ___ctx0;
		NullCheck(L_35);
		Lexer_t3664372066 * L_36 = L_35->get_L_2();
		NullCheck(L_36);
		StringBuilder_t243639308 * L_37 = L_36->get_string_buffer_10();
		FsmContext_t3936467683 * L_38 = ___ctx0;
		NullCheck(L_38);
		Lexer_t3664372066 * L_39 = L_38->get_L_2();
		NullCheck(L_39);
		int32_t L_40 = L_39->get_input_char_7();
		NullCheck(L_37);
		StringBuilder_Append_m2143093878(L_37, (((int32_t)((uint16_t)L_40))), /*hidden argument*/NULL);
		FsmContext_t3936467683 * L_41 = ___ctx0;
		NullCheck(L_41);
		L_41->set_NextState_1(7);
		return (bool)1;
	}

IL_010b:
	{
		return (bool)0;
	}

IL_010d:
	{
		FsmContext_t3936467683 * L_42 = ___ctx0;
		NullCheck(L_42);
		Lexer_t3664372066 * L_43 = L_42->get_L_2();
		NullCheck(L_43);
		bool L_44 = Lexer_GetChar_m4104177437(L_43, /*hidden argument*/NULL);
		if (L_44)
		{
			goto IL_0005;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean LitJson.Lexer::State7(LitJson.FsmContext)
extern "C"  bool Lexer_State7_m3101225081 (Il2CppObject * __this /* static, unused */, FsmContext_t3936467683 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t3936467683 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t3664372066 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m4104177437(L_1, /*hidden argument*/NULL);
		FsmContext_t3936467683 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t3664372066 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		if ((((int32_t)L_4) < ((int32_t)((int32_t)48))))
		{
			goto IL_0056;
		}
	}
	{
		FsmContext_t3936467683 * L_5 = ___ctx0;
		NullCheck(L_5);
		Lexer_t3664372066 * L_6 = L_5->get_L_2();
		NullCheck(L_6);
		int32_t L_7 = L_6->get_input_char_7();
		if ((((int32_t)L_7) > ((int32_t)((int32_t)57))))
		{
			goto IL_0056;
		}
	}
	{
		FsmContext_t3936467683 * L_8 = ___ctx0;
		NullCheck(L_8);
		Lexer_t3664372066 * L_9 = L_8->get_L_2();
		NullCheck(L_9);
		StringBuilder_t243639308 * L_10 = L_9->get_string_buffer_10();
		FsmContext_t3936467683 * L_11 = ___ctx0;
		NullCheck(L_11);
		Lexer_t3664372066 * L_12 = L_11->get_L_2();
		NullCheck(L_12);
		int32_t L_13 = L_12->get_input_char_7();
		NullCheck(L_10);
		StringBuilder_Append_m2143093878(L_10, (((int32_t)((uint16_t)L_13))), /*hidden argument*/NULL);
		FsmContext_t3936467683 * L_14 = ___ctx0;
		NullCheck(L_14);
		L_14->set_NextState_1(8);
		return (bool)1;
	}

IL_0056:
	{
		FsmContext_t3936467683 * L_15 = ___ctx0;
		NullCheck(L_15);
		Lexer_t3664372066 * L_16 = L_15->get_L_2();
		NullCheck(L_16);
		int32_t L_17 = L_16->get_input_char_7();
		V_0 = L_17;
		int32_t L_18 = V_0;
		if ((((int32_t)L_18) == ((int32_t)((int32_t)43))))
		{
			goto IL_0077;
		}
	}
	{
		int32_t L_19 = V_0;
		if ((((int32_t)L_19) == ((int32_t)((int32_t)45))))
		{
			goto IL_0077;
		}
	}
	{
		goto IL_009d;
	}

IL_0077:
	{
		FsmContext_t3936467683 * L_20 = ___ctx0;
		NullCheck(L_20);
		Lexer_t3664372066 * L_21 = L_20->get_L_2();
		NullCheck(L_21);
		StringBuilder_t243639308 * L_22 = L_21->get_string_buffer_10();
		FsmContext_t3936467683 * L_23 = ___ctx0;
		NullCheck(L_23);
		Lexer_t3664372066 * L_24 = L_23->get_L_2();
		NullCheck(L_24);
		int32_t L_25 = L_24->get_input_char_7();
		NullCheck(L_22);
		StringBuilder_Append_m2143093878(L_22, (((int32_t)((uint16_t)L_25))), /*hidden argument*/NULL);
		FsmContext_t3936467683 * L_26 = ___ctx0;
		NullCheck(L_26);
		L_26->set_NextState_1(8);
		return (bool)1;
	}

IL_009d:
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.Lexer::State8(LitJson.FsmContext)
extern "C"  bool Lexer_State8_m3312575994 (Il2CppObject * __this /* static, unused */, FsmContext_t3936467683 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		goto IL_00d7;
	}

IL_0005:
	{
		FsmContext_t3936467683 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t3664372066 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_input_char_7();
		if ((((int32_t)L_2) < ((int32_t)((int32_t)48))))
		{
			goto IL_004b;
		}
	}
	{
		FsmContext_t3936467683 * L_3 = ___ctx0;
		NullCheck(L_3);
		Lexer_t3664372066 * L_4 = L_3->get_L_2();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_input_char_7();
		if ((((int32_t)L_5) > ((int32_t)((int32_t)57))))
		{
			goto IL_004b;
		}
	}
	{
		FsmContext_t3936467683 * L_6 = ___ctx0;
		NullCheck(L_6);
		Lexer_t3664372066 * L_7 = L_6->get_L_2();
		NullCheck(L_7);
		StringBuilder_t243639308 * L_8 = L_7->get_string_buffer_10();
		FsmContext_t3936467683 * L_9 = ___ctx0;
		NullCheck(L_9);
		Lexer_t3664372066 * L_10 = L_9->get_L_2();
		NullCheck(L_10);
		int32_t L_11 = L_10->get_input_char_7();
		NullCheck(L_8);
		StringBuilder_Append_m2143093878(L_8, (((int32_t)((uint16_t)L_11))), /*hidden argument*/NULL);
		goto IL_00d7;
	}

IL_004b:
	{
		FsmContext_t3936467683 * L_12 = ___ctx0;
		NullCheck(L_12);
		Lexer_t3664372066 * L_13 = L_12->get_L_2();
		NullCheck(L_13);
		int32_t L_14 = L_13->get_input_char_7();
		if ((((int32_t)L_14) == ((int32_t)((int32_t)32))))
		{
			goto IL_0081;
		}
	}
	{
		FsmContext_t3936467683 * L_15 = ___ctx0;
		NullCheck(L_15);
		Lexer_t3664372066 * L_16 = L_15->get_L_2();
		NullCheck(L_16);
		int32_t L_17 = L_16->get_input_char_7();
		if ((((int32_t)L_17) < ((int32_t)((int32_t)9))))
		{
			goto IL_0091;
		}
	}
	{
		FsmContext_t3936467683 * L_18 = ___ctx0;
		NullCheck(L_18);
		Lexer_t3664372066 * L_19 = L_18->get_L_2();
		NullCheck(L_19);
		int32_t L_20 = L_19->get_input_char_7();
		if ((((int32_t)L_20) > ((int32_t)((int32_t)13))))
		{
			goto IL_0091;
		}
	}

IL_0081:
	{
		FsmContext_t3936467683 * L_21 = ___ctx0;
		NullCheck(L_21);
		L_21->set_Return_0((bool)1);
		FsmContext_t3936467683 * L_22 = ___ctx0;
		NullCheck(L_22);
		L_22->set_NextState_1(1);
		return (bool)1;
	}

IL_0091:
	{
		FsmContext_t3936467683 * L_23 = ___ctx0;
		NullCheck(L_23);
		Lexer_t3664372066 * L_24 = L_23->get_L_2();
		NullCheck(L_24);
		int32_t L_25 = L_24->get_input_char_7();
		V_0 = L_25;
		int32_t L_26 = V_0;
		if ((((int32_t)L_26) == ((int32_t)((int32_t)44))))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_27 = V_0;
		if ((((int32_t)L_27) == ((int32_t)((int32_t)93))))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_28 = V_0;
		if ((((int32_t)L_28) == ((int32_t)((int32_t)125))))
		{
			goto IL_00ba;
		}
	}
	{
		goto IL_00d5;
	}

IL_00ba:
	{
		FsmContext_t3936467683 * L_29 = ___ctx0;
		NullCheck(L_29);
		Lexer_t3664372066 * L_30 = L_29->get_L_2();
		NullCheck(L_30);
		Lexer_UngetChar_m3833420334(L_30, /*hidden argument*/NULL);
		FsmContext_t3936467683 * L_31 = ___ctx0;
		NullCheck(L_31);
		L_31->set_Return_0((bool)1);
		FsmContext_t3936467683 * L_32 = ___ctx0;
		NullCheck(L_32);
		L_32->set_NextState_1(1);
		return (bool)1;
	}

IL_00d5:
	{
		return (bool)0;
	}

IL_00d7:
	{
		FsmContext_t3936467683 * L_33 = ___ctx0;
		NullCheck(L_33);
		Lexer_t3664372066 * L_34 = L_33->get_L_2();
		NullCheck(L_34);
		bool L_35 = Lexer_GetChar_m4104177437(L_34, /*hidden argument*/NULL);
		if (L_35)
		{
			goto IL_0005;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean LitJson.Lexer::State9(LitJson.FsmContext)
extern "C"  bool Lexer_State9_m3523926907 (Il2CppObject * __this /* static, unused */, FsmContext_t3936467683 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t3936467683 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t3664372066 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m4104177437(L_1, /*hidden argument*/NULL);
		FsmContext_t3936467683 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t3664372066 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)114))))
		{
			goto IL_0025;
		}
	}
	{
		goto IL_002f;
	}

IL_0025:
	{
		FsmContext_t3936467683 * L_6 = ___ctx0;
		NullCheck(L_6);
		L_6->set_NextState_1(((int32_t)10));
		return (bool)1;
	}

IL_002f:
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.Lexer::State10(LitJson.FsmContext)
extern "C"  bool Lexer_State10_m3439175875 (Il2CppObject * __this /* static, unused */, FsmContext_t3936467683 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t3936467683 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t3664372066 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m4104177437(L_1, /*hidden argument*/NULL);
		FsmContext_t3936467683 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t3664372066 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)117))))
		{
			goto IL_0025;
		}
	}
	{
		goto IL_002f;
	}

IL_0025:
	{
		FsmContext_t3936467683 * L_6 = ___ctx0;
		NullCheck(L_6);
		L_6->set_NextState_1(((int32_t)11));
		return (bool)1;
	}

IL_002f:
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.Lexer::State11(LitJson.FsmContext)
extern "C"  bool Lexer_State11_m3650526788 (Il2CppObject * __this /* static, unused */, FsmContext_t3936467683 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t3936467683 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t3664372066 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m4104177437(L_1, /*hidden argument*/NULL);
		FsmContext_t3936467683 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t3664372066 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)101))))
		{
			goto IL_0025;
		}
	}
	{
		goto IL_0035;
	}

IL_0025:
	{
		FsmContext_t3936467683 * L_6 = ___ctx0;
		NullCheck(L_6);
		L_6->set_Return_0((bool)1);
		FsmContext_t3936467683 * L_7 = ___ctx0;
		NullCheck(L_7);
		L_7->set_NextState_1(1);
		return (bool)1;
	}

IL_0035:
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.Lexer::State12(LitJson.FsmContext)
extern "C"  bool Lexer_State12_m3861877701 (Il2CppObject * __this /* static, unused */, FsmContext_t3936467683 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t3936467683 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t3664372066 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m4104177437(L_1, /*hidden argument*/NULL);
		FsmContext_t3936467683 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t3664372066 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)97))))
		{
			goto IL_0025;
		}
	}
	{
		goto IL_002f;
	}

IL_0025:
	{
		FsmContext_t3936467683 * L_6 = ___ctx0;
		NullCheck(L_6);
		L_6->set_NextState_1(((int32_t)13));
		return (bool)1;
	}

IL_002f:
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.Lexer::State13(LitJson.FsmContext)
extern "C"  bool Lexer_State13_m4073228614 (Il2CppObject * __this /* static, unused */, FsmContext_t3936467683 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t3936467683 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t3664372066 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m4104177437(L_1, /*hidden argument*/NULL);
		FsmContext_t3936467683 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t3664372066 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)108))))
		{
			goto IL_0025;
		}
	}
	{
		goto IL_002f;
	}

IL_0025:
	{
		FsmContext_t3936467683 * L_6 = ___ctx0;
		NullCheck(L_6);
		L_6->set_NextState_1(((int32_t)14));
		return (bool)1;
	}

IL_002f:
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.Lexer::State14(LitJson.FsmContext)
extern "C"  bool Lexer_State14_m4284579527 (Il2CppObject * __this /* static, unused */, FsmContext_t3936467683 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t3936467683 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t3664372066 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m4104177437(L_1, /*hidden argument*/NULL);
		FsmContext_t3936467683 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t3664372066 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)115))))
		{
			goto IL_0025;
		}
	}
	{
		goto IL_002f;
	}

IL_0025:
	{
		FsmContext_t3936467683 * L_6 = ___ctx0;
		NullCheck(L_6);
		L_6->set_NextState_1(((int32_t)15));
		return (bool)1;
	}

IL_002f:
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.Lexer::State15(LitJson.FsmContext)
extern "C"  bool Lexer_State15_m200963144 (Il2CppObject * __this /* static, unused */, FsmContext_t3936467683 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t3936467683 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t3664372066 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m4104177437(L_1, /*hidden argument*/NULL);
		FsmContext_t3936467683 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t3664372066 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)101))))
		{
			goto IL_0025;
		}
	}
	{
		goto IL_0035;
	}

IL_0025:
	{
		FsmContext_t3936467683 * L_6 = ___ctx0;
		NullCheck(L_6);
		L_6->set_Return_0((bool)1);
		FsmContext_t3936467683 * L_7 = ___ctx0;
		NullCheck(L_7);
		L_7->set_NextState_1(1);
		return (bool)1;
	}

IL_0035:
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.Lexer::State16(LitJson.FsmContext)
extern "C"  bool Lexer_State16_m412314057 (Il2CppObject * __this /* static, unused */, FsmContext_t3936467683 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t3936467683 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t3664372066 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m4104177437(L_1, /*hidden argument*/NULL);
		FsmContext_t3936467683 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t3664372066 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)117))))
		{
			goto IL_0025;
		}
	}
	{
		goto IL_002f;
	}

IL_0025:
	{
		FsmContext_t3936467683 * L_6 = ___ctx0;
		NullCheck(L_6);
		L_6->set_NextState_1(((int32_t)17));
		return (bool)1;
	}

IL_002f:
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.Lexer::State17(LitJson.FsmContext)
extern "C"  bool Lexer_State17_m623664970 (Il2CppObject * __this /* static, unused */, FsmContext_t3936467683 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t3936467683 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t3664372066 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m4104177437(L_1, /*hidden argument*/NULL);
		FsmContext_t3936467683 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t3664372066 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)108))))
		{
			goto IL_0025;
		}
	}
	{
		goto IL_002f;
	}

IL_0025:
	{
		FsmContext_t3936467683 * L_6 = ___ctx0;
		NullCheck(L_6);
		L_6->set_NextState_1(((int32_t)18));
		return (bool)1;
	}

IL_002f:
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.Lexer::State18(LitJson.FsmContext)
extern "C"  bool Lexer_State18_m835015883 (Il2CppObject * __this /* static, unused */, FsmContext_t3936467683 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t3936467683 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t3664372066 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m4104177437(L_1, /*hidden argument*/NULL);
		FsmContext_t3936467683 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t3664372066 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)108))))
		{
			goto IL_0025;
		}
	}
	{
		goto IL_0035;
	}

IL_0025:
	{
		FsmContext_t3936467683 * L_6 = ___ctx0;
		NullCheck(L_6);
		L_6->set_Return_0((bool)1);
		FsmContext_t3936467683 * L_7 = ___ctx0;
		NullCheck(L_7);
		L_7->set_NextState_1(1);
		return (bool)1;
	}

IL_0035:
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.Lexer::State19(LitJson.FsmContext)
extern "C"  bool Lexer_State19_m1046366796 (Il2CppObject * __this /* static, unused */, FsmContext_t3936467683 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		goto IL_0076;
	}

IL_0005:
	{
		FsmContext_t3936467683 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t3664372066 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_input_char_7();
		V_0 = L_2;
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)34))))
		{
			goto IL_0026;
		}
	}
	{
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)((int32_t)92))))
		{
			goto IL_0042;
		}
	}
	{
		goto IL_0054;
	}

IL_0026:
	{
		FsmContext_t3936467683 * L_5 = ___ctx0;
		NullCheck(L_5);
		Lexer_t3664372066 * L_6 = L_5->get_L_2();
		NullCheck(L_6);
		Lexer_UngetChar_m3833420334(L_6, /*hidden argument*/NULL);
		FsmContext_t3936467683 * L_7 = ___ctx0;
		NullCheck(L_7);
		L_7->set_Return_0((bool)1);
		FsmContext_t3936467683 * L_8 = ___ctx0;
		NullCheck(L_8);
		L_8->set_NextState_1(((int32_t)20));
		return (bool)1;
	}

IL_0042:
	{
		FsmContext_t3936467683 * L_9 = ___ctx0;
		NullCheck(L_9);
		L_9->set_StateStack_3(((int32_t)19));
		FsmContext_t3936467683 * L_10 = ___ctx0;
		NullCheck(L_10);
		L_10->set_NextState_1(((int32_t)21));
		return (bool)1;
	}

IL_0054:
	{
		FsmContext_t3936467683 * L_11 = ___ctx0;
		NullCheck(L_11);
		Lexer_t3664372066 * L_12 = L_11->get_L_2();
		NullCheck(L_12);
		StringBuilder_t243639308 * L_13 = L_12->get_string_buffer_10();
		FsmContext_t3936467683 * L_14 = ___ctx0;
		NullCheck(L_14);
		Lexer_t3664372066 * L_15 = L_14->get_L_2();
		NullCheck(L_15);
		int32_t L_16 = L_15->get_input_char_7();
		NullCheck(L_13);
		StringBuilder_Append_m2143093878(L_13, (((int32_t)((uint16_t)L_16))), /*hidden argument*/NULL);
		goto IL_0076;
	}

IL_0076:
	{
		FsmContext_t3936467683 * L_17 = ___ctx0;
		NullCheck(L_17);
		Lexer_t3664372066 * L_18 = L_17->get_L_2();
		NullCheck(L_18);
		bool L_19 = Lexer_GetChar_m4104177437(L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_0005;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean LitJson.Lexer::State20(LitJson.FsmContext)
extern "C"  bool Lexer_State20_m1401119586 (Il2CppObject * __this /* static, unused */, FsmContext_t3936467683 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t3936467683 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t3664372066 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m4104177437(L_1, /*hidden argument*/NULL);
		FsmContext_t3936467683 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t3664372066 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)34))))
		{
			goto IL_0025;
		}
	}
	{
		goto IL_0035;
	}

IL_0025:
	{
		FsmContext_t3936467683 * L_6 = ___ctx0;
		NullCheck(L_6);
		L_6->set_Return_0((bool)1);
		FsmContext_t3936467683 * L_7 = ___ctx0;
		NullCheck(L_7);
		L_7->set_NextState_1(1);
		return (bool)1;
	}

IL_0035:
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.Lexer::State21(LitJson.FsmContext)
extern Il2CppClass* Lexer_t3664372066_il2cpp_TypeInfo_var;
extern const uint32_t Lexer_State21_m1612470499_MetadataUsageId;
extern "C"  bool Lexer_State21_m1612470499 (Il2CppObject * __this /* static, unused */, FsmContext_t3936467683 * ___ctx0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Lexer_State21_m1612470499_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		FsmContext_t3936467683 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t3664372066 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m4104177437(L_1, /*hidden argument*/NULL);
		FsmContext_t3936467683 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t3664372066 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)114))) == 0)
		{
			goto IL_0078;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)114))) == 1)
		{
			goto IL_0031;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)114))) == 2)
		{
			goto IL_0078;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)114))) == 3)
		{
			goto IL_006e;
		}
	}

IL_0031:
	{
		int32_t L_6 = V_0;
		if ((((int32_t)L_6) == ((int32_t)((int32_t)34))))
		{
			goto IL_0078;
		}
	}
	{
		int32_t L_7 = V_0;
		if ((((int32_t)L_7) == ((int32_t)((int32_t)39))))
		{
			goto IL_0078;
		}
	}
	{
		int32_t L_8 = V_0;
		if ((((int32_t)L_8) == ((int32_t)((int32_t)47))))
		{
			goto IL_0078;
		}
	}
	{
		int32_t L_9 = V_0;
		if ((((int32_t)L_9) == ((int32_t)((int32_t)92))))
		{
			goto IL_0078;
		}
	}
	{
		int32_t L_10 = V_0;
		if ((((int32_t)L_10) == ((int32_t)((int32_t)98))))
		{
			goto IL_0078;
		}
	}
	{
		int32_t L_11 = V_0;
		if ((((int32_t)L_11) == ((int32_t)((int32_t)102))))
		{
			goto IL_0078;
		}
	}
	{
		int32_t L_12 = V_0;
		if ((((int32_t)L_12) == ((int32_t)((int32_t)110))))
		{
			goto IL_0078;
		}
	}
	{
		goto IL_00a7;
	}

IL_006e:
	{
		FsmContext_t3936467683 * L_13 = ___ctx0;
		NullCheck(L_13);
		L_13->set_NextState_1(((int32_t)22));
		return (bool)1;
	}

IL_0078:
	{
		FsmContext_t3936467683 * L_14 = ___ctx0;
		NullCheck(L_14);
		Lexer_t3664372066 * L_15 = L_14->get_L_2();
		NullCheck(L_15);
		StringBuilder_t243639308 * L_16 = L_15->get_string_buffer_10();
		FsmContext_t3936467683 * L_17 = ___ctx0;
		NullCheck(L_17);
		Lexer_t3664372066 * L_18 = L_17->get_L_2();
		NullCheck(L_18);
		int32_t L_19 = L_18->get_input_char_7();
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		Il2CppChar L_20 = Lexer_ProcessEscChar_m2805824502(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		NullCheck(L_16);
		StringBuilder_Append_m2143093878(L_16, L_20, /*hidden argument*/NULL);
		FsmContext_t3936467683 * L_21 = ___ctx0;
		FsmContext_t3936467683 * L_22 = ___ctx0;
		NullCheck(L_22);
		int32_t L_23 = L_22->get_StateStack_3();
		NullCheck(L_21);
		L_21->set_NextState_1(L_23);
		return (bool)1;
	}

IL_00a7:
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.Lexer::State22(LitJson.FsmContext)
extern Il2CppClass* Lexer_t3664372066_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern const uint32_t Lexer_State22_m1823821412_MetadataUsageId;
extern "C"  bool Lexer_State22_m1823821412 (Il2CppObject * __this /* static, unused */, FsmContext_t3936467683 * ___ctx0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Lexer_State22_m1823821412_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		V_1 = ((int32_t)4096);
		FsmContext_t3936467683 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t3664372066 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		L_1->set_unichar_13(0);
		goto IL_00ef;
	}

IL_0019:
	{
		FsmContext_t3936467683 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t3664372066 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		if ((((int32_t)L_4) < ((int32_t)((int32_t)48))))
		{
			goto IL_003d;
		}
	}
	{
		FsmContext_t3936467683 * L_5 = ___ctx0;
		NullCheck(L_5);
		Lexer_t3664372066 * L_6 = L_5->get_L_2();
		NullCheck(L_6);
		int32_t L_7 = L_6->get_input_char_7();
		if ((((int32_t)L_7) <= ((int32_t)((int32_t)57))))
		{
			goto IL_0085;
		}
	}

IL_003d:
	{
		FsmContext_t3936467683 * L_8 = ___ctx0;
		NullCheck(L_8);
		Lexer_t3664372066 * L_9 = L_8->get_L_2();
		NullCheck(L_9);
		int32_t L_10 = L_9->get_input_char_7();
		if ((((int32_t)L_10) < ((int32_t)((int32_t)65))))
		{
			goto IL_0061;
		}
	}
	{
		FsmContext_t3936467683 * L_11 = ___ctx0;
		NullCheck(L_11);
		Lexer_t3664372066 * L_12 = L_11->get_L_2();
		NullCheck(L_12);
		int32_t L_13 = L_12->get_input_char_7();
		if ((((int32_t)L_13) <= ((int32_t)((int32_t)70))))
		{
			goto IL_0085;
		}
	}

IL_0061:
	{
		FsmContext_t3936467683 * L_14 = ___ctx0;
		NullCheck(L_14);
		Lexer_t3664372066 * L_15 = L_14->get_L_2();
		NullCheck(L_15);
		int32_t L_16 = L_15->get_input_char_7();
		if ((((int32_t)L_16) < ((int32_t)((int32_t)97))))
		{
			goto IL_00ed;
		}
	}
	{
		FsmContext_t3936467683 * L_17 = ___ctx0;
		NullCheck(L_17);
		Lexer_t3664372066 * L_18 = L_17->get_L_2();
		NullCheck(L_18);
		int32_t L_19 = L_18->get_input_char_7();
		if ((((int32_t)L_19) > ((int32_t)((int32_t)102))))
		{
			goto IL_00ed;
		}
	}

IL_0085:
	{
		FsmContext_t3936467683 * L_20 = ___ctx0;
		NullCheck(L_20);
		Lexer_t3664372066 * L_21 = L_20->get_L_2();
		Lexer_t3664372066 * L_22 = L_21;
		NullCheck(L_22);
		int32_t L_23 = L_22->get_unichar_13();
		FsmContext_t3936467683 * L_24 = ___ctx0;
		NullCheck(L_24);
		Lexer_t3664372066 * L_25 = L_24->get_L_2();
		NullCheck(L_25);
		int32_t L_26 = L_25->get_input_char_7();
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		int32_t L_27 = Lexer_HexValue_m1760624318(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		int32_t L_28 = V_1;
		NullCheck(L_22);
		L_22->set_unichar_13(((int32_t)((int32_t)L_23+(int32_t)((int32_t)((int32_t)L_27*(int32_t)L_28)))));
		int32_t L_29 = V_0;
		V_0 = ((int32_t)((int32_t)L_29+(int32_t)1));
		int32_t L_30 = V_1;
		V_1 = ((int32_t)((int32_t)L_30/(int32_t)((int32_t)16)));
		int32_t L_31 = V_0;
		if ((!(((uint32_t)L_31) == ((uint32_t)4))))
		{
			goto IL_00e8;
		}
	}
	{
		FsmContext_t3936467683 * L_32 = ___ctx0;
		NullCheck(L_32);
		Lexer_t3664372066 * L_33 = L_32->get_L_2();
		NullCheck(L_33);
		StringBuilder_t243639308 * L_34 = L_33->get_string_buffer_10();
		FsmContext_t3936467683 * L_35 = ___ctx0;
		NullCheck(L_35);
		Lexer_t3664372066 * L_36 = L_35->get_L_2();
		NullCheck(L_36);
		int32_t L_37 = L_36->get_unichar_13();
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		Il2CppChar L_38 = Convert_ToChar_m353236550(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		NullCheck(L_34);
		StringBuilder_Append_m2143093878(L_34, L_38, /*hidden argument*/NULL);
		FsmContext_t3936467683 * L_39 = ___ctx0;
		FsmContext_t3936467683 * L_40 = ___ctx0;
		NullCheck(L_40);
		int32_t L_41 = L_40->get_StateStack_3();
		NullCheck(L_39);
		L_39->set_NextState_1(L_41);
		return (bool)1;
	}

IL_00e8:
	{
		goto IL_00ef;
	}

IL_00ed:
	{
		return (bool)0;
	}

IL_00ef:
	{
		FsmContext_t3936467683 * L_42 = ___ctx0;
		NullCheck(L_42);
		Lexer_t3664372066 * L_43 = L_42->get_L_2();
		NullCheck(L_43);
		bool L_44 = Lexer_GetChar_m4104177437(L_43, /*hidden argument*/NULL);
		if (L_44)
		{
			goto IL_0019;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean LitJson.Lexer::State23(LitJson.FsmContext)
extern "C"  bool Lexer_State23_m2035172325 (Il2CppObject * __this /* static, unused */, FsmContext_t3936467683 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		goto IL_0076;
	}

IL_0005:
	{
		FsmContext_t3936467683 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t3664372066 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_input_char_7();
		V_0 = L_2;
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)39))))
		{
			goto IL_0026;
		}
	}
	{
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)((int32_t)92))))
		{
			goto IL_0042;
		}
	}
	{
		goto IL_0054;
	}

IL_0026:
	{
		FsmContext_t3936467683 * L_5 = ___ctx0;
		NullCheck(L_5);
		Lexer_t3664372066 * L_6 = L_5->get_L_2();
		NullCheck(L_6);
		Lexer_UngetChar_m3833420334(L_6, /*hidden argument*/NULL);
		FsmContext_t3936467683 * L_7 = ___ctx0;
		NullCheck(L_7);
		L_7->set_Return_0((bool)1);
		FsmContext_t3936467683 * L_8 = ___ctx0;
		NullCheck(L_8);
		L_8->set_NextState_1(((int32_t)24));
		return (bool)1;
	}

IL_0042:
	{
		FsmContext_t3936467683 * L_9 = ___ctx0;
		NullCheck(L_9);
		L_9->set_StateStack_3(((int32_t)23));
		FsmContext_t3936467683 * L_10 = ___ctx0;
		NullCheck(L_10);
		L_10->set_NextState_1(((int32_t)21));
		return (bool)1;
	}

IL_0054:
	{
		FsmContext_t3936467683 * L_11 = ___ctx0;
		NullCheck(L_11);
		Lexer_t3664372066 * L_12 = L_11->get_L_2();
		NullCheck(L_12);
		StringBuilder_t243639308 * L_13 = L_12->get_string_buffer_10();
		FsmContext_t3936467683 * L_14 = ___ctx0;
		NullCheck(L_14);
		Lexer_t3664372066 * L_15 = L_14->get_L_2();
		NullCheck(L_15);
		int32_t L_16 = L_15->get_input_char_7();
		NullCheck(L_13);
		StringBuilder_Append_m2143093878(L_13, (((int32_t)((uint16_t)L_16))), /*hidden argument*/NULL);
		goto IL_0076;
	}

IL_0076:
	{
		FsmContext_t3936467683 * L_17 = ___ctx0;
		NullCheck(L_17);
		Lexer_t3664372066 * L_18 = L_17->get_L_2();
		NullCheck(L_18);
		bool L_19 = Lexer_GetChar_m4104177437(L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_0005;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean LitJson.Lexer::State24(LitJson.FsmContext)
extern "C"  bool Lexer_State24_m2246523238 (Il2CppObject * __this /* static, unused */, FsmContext_t3936467683 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t3936467683 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t3664372066 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m4104177437(L_1, /*hidden argument*/NULL);
		FsmContext_t3936467683 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t3664372066 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)39))))
		{
			goto IL_0025;
		}
	}
	{
		goto IL_0042;
	}

IL_0025:
	{
		FsmContext_t3936467683 * L_6 = ___ctx0;
		NullCheck(L_6);
		Lexer_t3664372066 * L_7 = L_6->get_L_2();
		NullCheck(L_7);
		L_7->set_input_char_7(((int32_t)34));
		FsmContext_t3936467683 * L_8 = ___ctx0;
		NullCheck(L_8);
		L_8->set_Return_0((bool)1);
		FsmContext_t3936467683 * L_9 = ___ctx0;
		NullCheck(L_9);
		L_9->set_NextState_1(1);
		return (bool)1;
	}

IL_0042:
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.Lexer::State25(LitJson.FsmContext)
extern "C"  bool Lexer_State25_m2457874151 (Il2CppObject * __this /* static, unused */, FsmContext_t3936467683 * ___ctx0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FsmContext_t3936467683 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t3664372066 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		Lexer_GetChar_m4104177437(L_1, /*hidden argument*/NULL);
		FsmContext_t3936467683 * L_2 = ___ctx0;
		NullCheck(L_2);
		Lexer_t3664372066 * L_3 = L_2->get_L_2();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_input_char_7();
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)42))))
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_6 = V_0;
		if ((((int32_t)L_6) == ((int32_t)((int32_t)47))))
		{
			goto IL_0037;
		}
	}
	{
		goto IL_0041;
	}

IL_002d:
	{
		FsmContext_t3936467683 * L_7 = ___ctx0;
		NullCheck(L_7);
		L_7->set_NextState_1(((int32_t)27));
		return (bool)1;
	}

IL_0037:
	{
		FsmContext_t3936467683 * L_8 = ___ctx0;
		NullCheck(L_8);
		L_8->set_NextState_1(((int32_t)26));
		return (bool)1;
	}

IL_0041:
	{
		return (bool)0;
	}
}
// System.Boolean LitJson.Lexer::State26(LitJson.FsmContext)
extern "C"  bool Lexer_State26_m2669225064 (Il2CppObject * __this /* static, unused */, FsmContext_t3936467683 * ___ctx0, const MethodInfo* method)
{
	{
		goto IL_0020;
	}

IL_0005:
	{
		FsmContext_t3936467683 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t3664372066 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_input_char_7();
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0020;
		}
	}
	{
		FsmContext_t3936467683 * L_3 = ___ctx0;
		NullCheck(L_3);
		L_3->set_NextState_1(1);
		return (bool)1;
	}

IL_0020:
	{
		FsmContext_t3936467683 * L_4 = ___ctx0;
		NullCheck(L_4);
		Lexer_t3664372066 * L_5 = L_4->get_L_2();
		NullCheck(L_5);
		bool L_6 = Lexer_GetChar_m4104177437(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0005;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean LitJson.Lexer::State27(LitJson.FsmContext)
extern "C"  bool Lexer_State27_m2880575977 (Il2CppObject * __this /* static, unused */, FsmContext_t3936467683 * ___ctx0, const MethodInfo* method)
{
	{
		goto IL_0021;
	}

IL_0005:
	{
		FsmContext_t3936467683 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t3664372066 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_input_char_7();
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)42)))))
		{
			goto IL_0021;
		}
	}
	{
		FsmContext_t3936467683 * L_3 = ___ctx0;
		NullCheck(L_3);
		L_3->set_NextState_1(((int32_t)28));
		return (bool)1;
	}

IL_0021:
	{
		FsmContext_t3936467683 * L_4 = ___ctx0;
		NullCheck(L_4);
		Lexer_t3664372066 * L_5 = L_4->get_L_2();
		NullCheck(L_5);
		bool L_6 = Lexer_GetChar_m4104177437(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0005;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean LitJson.Lexer::State28(LitJson.FsmContext)
extern "C"  bool Lexer_State28_m3091926890 (Il2CppObject * __this /* static, unused */, FsmContext_t3936467683 * ___ctx0, const MethodInfo* method)
{
	{
		goto IL_0041;
	}

IL_0005:
	{
		FsmContext_t3936467683 * L_0 = ___ctx0;
		NullCheck(L_0);
		Lexer_t3664372066 * L_1 = L_0->get_L_2();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_input_char_7();
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)42)))))
		{
			goto IL_001c;
		}
	}
	{
		goto IL_0041;
	}

IL_001c:
	{
		FsmContext_t3936467683 * L_3 = ___ctx0;
		NullCheck(L_3);
		Lexer_t3664372066 * L_4 = L_3->get_L_2();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_input_char_7();
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_0037;
		}
	}
	{
		FsmContext_t3936467683 * L_6 = ___ctx0;
		NullCheck(L_6);
		L_6->set_NextState_1(1);
		return (bool)1;
	}

IL_0037:
	{
		FsmContext_t3936467683 * L_7 = ___ctx0;
		NullCheck(L_7);
		L_7->set_NextState_1(((int32_t)27));
		return (bool)1;
	}

IL_0041:
	{
		FsmContext_t3936467683 * L_8 = ___ctx0;
		NullCheck(L_8);
		Lexer_t3664372066 * L_9 = L_8->get_L_2();
		NullCheck(L_9);
		bool L_10 = Lexer_GetChar_m4104177437(L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0005;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean LitJson.Lexer::GetChar()
extern "C"  bool Lexer_GetChar_m4104177437 (Lexer_t3664372066 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Lexer_NextChar_m2560439808(__this, /*hidden argument*/NULL);
		int32_t L_1 = L_0;
		V_0 = L_1;
		__this->set_input_char_7(L_1);
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0017;
		}
	}
	{
		return (bool)1;
	}

IL_0017:
	{
		__this->set_end_of_input_4((bool)1);
		return (bool)0;
	}
}
// System.Int32 LitJson.Lexer::NextChar()
extern "C"  int32_t Lexer_NextChar_m2560439808 (Lexer_t3664372066 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_input_buffer_6();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		int32_t L_1 = __this->get_input_buffer_6();
		V_0 = L_1;
		__this->set_input_buffer_6(0);
		int32_t L_2 = V_0;
		return L_2;
	}

IL_001b:
	{
		TextReader_t2148718976 * L_3 = __this->get_reader_8();
		NullCheck(L_3);
		int32_t L_4 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 System.IO.TextReader::Read() */, L_3);
		return L_4;
	}
}
// System.Boolean LitJson.Lexer::NextToken()
extern Il2CppClass* Lexer_t3664372066_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonException_t3617621405_il2cpp_TypeInfo_var;
extern const uint32_t Lexer_NextToken_m2325282711_MetadataUsageId;
extern "C"  bool Lexer_NextToken_m2325282711 (Lexer_t3664372066 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Lexer_NextToken_m2325282711_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StateHandler_t3261942315 * V_0 = NULL;
	{
		FsmContext_t3936467683 * L_0 = __this->get_fsm_context_5();
		NullCheck(L_0);
		L_0->set_Return_0((bool)0);
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		StateHandlerU5BU5D_t2657855690* L_1 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_fsm_handler_table_1();
		int32_t L_2 = __this->get_state_9();
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, ((int32_t)((int32_t)L_2-(int32_t)1)));
		int32_t L_3 = ((int32_t)((int32_t)L_2-(int32_t)1));
		StateHandler_t3261942315 * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_0 = L_4;
		StateHandler_t3261942315 * L_5 = V_0;
		FsmContext_t3936467683 * L_6 = __this->get_fsm_context_5();
		NullCheck(L_5);
		bool L_7 = StateHandler_Invoke_m165363923(L_5, L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_8 = __this->get_input_char_7();
		JsonException_t3617621405 * L_9 = (JsonException_t3617621405 *)il2cpp_codegen_object_new(JsonException_t3617621405_il2cpp_TypeInfo_var);
		JsonException__ctor_m3681649971(L_9, L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0038:
	{
		bool L_10 = __this->get_end_of_input_4();
		if (!L_10)
		{
			goto IL_0045;
		}
	}
	{
		return (bool)0;
	}

IL_0045:
	{
		FsmContext_t3936467683 * L_11 = __this->get_fsm_context_5();
		NullCheck(L_11);
		bool L_12 = L_11->get_Return_0();
		if (!L_12)
		{
			goto IL_00c1;
		}
	}
	{
		StringBuilder_t243639308 * L_13 = __this->get_string_buffer_10();
		NullCheck(L_13);
		String_t* L_14 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_13);
		__this->set_string_value_11(L_14);
		StringBuilder_t243639308 * L_15 = __this->get_string_buffer_10();
		StringBuilder_t243639308 * L_16 = __this->get_string_buffer_10();
		NullCheck(L_16);
		int32_t L_17 = StringBuilder_get_Length_m2443133099(L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		StringBuilder_Remove_m970775893(L_15, 0, L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Lexer_t3664372066_il2cpp_TypeInfo_var);
		Int32U5BU5D_t3230847821* L_18 = ((Lexer_t3664372066_StaticFields*)Lexer_t3664372066_il2cpp_TypeInfo_var->static_fields)->get_fsm_return_table_0();
		int32_t L_19 = __this->get_state_9();
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)((int32_t)L_19-(int32_t)1)));
		int32_t L_20 = ((int32_t)((int32_t)L_19-(int32_t)1));
		int32_t L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		__this->set_token_12(L_21);
		int32_t L_22 = __this->get_token_12();
		if ((!(((uint32_t)L_22) == ((uint32_t)((int32_t)65542)))))
		{
			goto IL_00ae;
		}
	}
	{
		int32_t L_23 = __this->get_input_char_7();
		__this->set_token_12(L_23);
	}

IL_00ae:
	{
		FsmContext_t3936467683 * L_24 = __this->get_fsm_context_5();
		NullCheck(L_24);
		int32_t L_25 = L_24->get_NextState_1();
		__this->set_state_9(L_25);
		return (bool)1;
	}

IL_00c1:
	{
		FsmContext_t3936467683 * L_26 = __this->get_fsm_context_5();
		NullCheck(L_26);
		int32_t L_27 = L_26->get_NextState_1();
		__this->set_state_9(L_27);
		goto IL_000c;
	}
}
// System.Void LitJson.Lexer::UngetChar()
extern "C"  void Lexer_UngetChar_m3833420334 (Lexer_t3664372066 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_input_char_7();
		__this->set_input_buffer_6(L_0);
		return;
	}
}
// System.Void LitJson.Lexer/StateHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void StateHandler__ctor_m3491238465 (StateHandler_t3261942315 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean LitJson.Lexer/StateHandler::Invoke(LitJson.FsmContext)
extern "C"  bool StateHandler_Invoke_m165363923 (StateHandler_t3261942315 * __this, FsmContext_t3936467683 * ___ctx0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		StateHandler_Invoke_m165363923((StateHandler_t3261942315 *)__this->get_prev_9(),___ctx0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, FsmContext_t3936467683 * ___ctx0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___ctx0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (void* __this, FsmContext_t3936467683 * ___ctx0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___ctx0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___ctx0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult LitJson.Lexer/StateHandler::BeginInvoke(LitJson.FsmContext,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * StateHandler_BeginInvoke_m1450599572 (StateHandler_t3261942315 * __this, FsmContext_t3936467683 * ___ctx0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___ctx0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean LitJson.Lexer/StateHandler::EndInvoke(System.IAsyncResult)
extern "C"  bool StateHandler_EndInvoke_m3407616839 (StateHandler_t3261942315 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Type LitJson.ObjectMetadata::get_ElementType()
extern const Il2CppType* JsonData_t1715015430_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t ObjectMetadata_get_ElementType_m3060033696_MetadataUsageId;
extern "C"  Type_t * ObjectMetadata_get_ElementType_m3060033696 (ObjectMetadata_t2009294498 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ObjectMetadata_get_ElementType_m3060033696_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = __this->get_element_type_0();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(JsonData_t1715015430_0_0_0_var), /*hidden argument*/NULL);
		return L_1;
	}

IL_0016:
	{
		Type_t * L_2 = __this->get_element_type_0();
		return L_2;
	}
}
extern "C"  Type_t * ObjectMetadata_get_ElementType_m3060033696_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ObjectMetadata_t2009294498 * _thisAdjusted = reinterpret_cast<ObjectMetadata_t2009294498 *>(__this + 1);
	return ObjectMetadata_get_ElementType_m3060033696(_thisAdjusted, method);
}
// System.Void LitJson.ObjectMetadata::set_ElementType(System.Type)
extern "C"  void ObjectMetadata_set_ElementType_m3476049203 (ObjectMetadata_t2009294498 * __this, Type_t * ___value0, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___value0;
		__this->set_element_type_0(L_0);
		return;
	}
}
extern "C"  void ObjectMetadata_set_ElementType_m3476049203_AdjustorThunk (Il2CppObject * __this, Type_t * ___value0, const MethodInfo* method)
{
	ObjectMetadata_t2009294498 * _thisAdjusted = reinterpret_cast<ObjectMetadata_t2009294498 *>(__this + 1);
	ObjectMetadata_set_ElementType_m3476049203(_thisAdjusted, ___value0, method);
}
// System.Boolean LitJson.ObjectMetadata::get_IsDictionary()
extern "C"  bool ObjectMetadata_get_IsDictionary_m1841257108 (ObjectMetadata_t2009294498 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_is_dictionary_1();
		return L_0;
	}
}
extern "C"  bool ObjectMetadata_get_IsDictionary_m1841257108_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ObjectMetadata_t2009294498 * _thisAdjusted = reinterpret_cast<ObjectMetadata_t2009294498 *>(__this + 1);
	return ObjectMetadata_get_IsDictionary_m1841257108(_thisAdjusted, method);
}
// System.Void LitJson.ObjectMetadata::set_IsDictionary(System.Boolean)
extern "C"  void ObjectMetadata_set_IsDictionary_m1761380233 (ObjectMetadata_t2009294498 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_is_dictionary_1(L_0);
		return;
	}
}
extern "C"  void ObjectMetadata_set_IsDictionary_m1761380233_AdjustorThunk (Il2CppObject * __this, bool ___value0, const MethodInfo* method)
{
	ObjectMetadata_t2009294498 * _thisAdjusted = reinterpret_cast<ObjectMetadata_t2009294498 *>(__this + 1);
	ObjectMetadata_set_IsDictionary_m1761380233(_thisAdjusted, ___value0, method);
}
// System.Collections.Generic.IDictionary`2<System.String,LitJson.PropertyMetadata> LitJson.ObjectMetadata::get_Properties()
extern "C"  Il2CppObject* ObjectMetadata_get_Properties_m1175853819 (ObjectMetadata_t2009294498 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = __this->get_properties_2();
		return L_0;
	}
}
extern "C"  Il2CppObject* ObjectMetadata_get_Properties_m1175853819_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ObjectMetadata_t2009294498 * _thisAdjusted = reinterpret_cast<ObjectMetadata_t2009294498 *>(__this + 1);
	return ObjectMetadata_get_Properties_m1175853819(_thisAdjusted, method);
}
// System.Void LitJson.ObjectMetadata::set_Properties(System.Collections.Generic.IDictionary`2<System.String,LitJson.PropertyMetadata>)
extern "C"  void ObjectMetadata_set_Properties_m2278608048 (ObjectMetadata_t2009294498 * __this, Il2CppObject* ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ___value0;
		__this->set_properties_2(L_0);
		return;
	}
}
extern "C"  void ObjectMetadata_set_Properties_m2278608048_AdjustorThunk (Il2CppObject * __this, Il2CppObject* ___value0, const MethodInfo* method)
{
	ObjectMetadata_t2009294498 * _thisAdjusted = reinterpret_cast<ObjectMetadata_t2009294498 *>(__this + 1);
	ObjectMetadata_set_Properties_m2278608048(_thisAdjusted, ___value0, method);
}
// Conversion methods for marshalling of: LitJson.ObjectMetadata
extern "C" void ObjectMetadata_t2009294498_marshal_pinvoke(const ObjectMetadata_t2009294498& unmarshaled, ObjectMetadata_t2009294498_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___element_type_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'element_type' of type 'ObjectMetadata': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___element_type_0Exception);
}
extern "C" void ObjectMetadata_t2009294498_marshal_pinvoke_back(const ObjectMetadata_t2009294498_marshaled_pinvoke& marshaled, ObjectMetadata_t2009294498& unmarshaled)
{
	Il2CppCodeGenException* ___element_type_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'element_type' of type 'ObjectMetadata': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___element_type_0Exception);
}
// Conversion method for clean up from marshalling of: LitJson.ObjectMetadata
extern "C" void ObjectMetadata_t2009294498_marshal_pinvoke_cleanup(ObjectMetadata_t2009294498_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: LitJson.ObjectMetadata
extern "C" void ObjectMetadata_t2009294498_marshal_com(const ObjectMetadata_t2009294498& unmarshaled, ObjectMetadata_t2009294498_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___element_type_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'element_type' of type 'ObjectMetadata': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___element_type_0Exception);
}
extern "C" void ObjectMetadata_t2009294498_marshal_com_back(const ObjectMetadata_t2009294498_marshaled_com& marshaled, ObjectMetadata_t2009294498& unmarshaled)
{
	Il2CppCodeGenException* ___element_type_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'element_type' of type 'ObjectMetadata': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___element_type_0Exception);
}
// Conversion method for clean up from marshalling of: LitJson.ObjectMetadata
extern "C" void ObjectMetadata_t2009294498_marshal_com_cleanup(ObjectMetadata_t2009294498_marshaled_com& marshaled)
{
}
// System.Object LitJson.OrderedDictionaryEnumerator::get_Current()
extern Il2CppClass* DictionaryEntry_t1751606614_il2cpp_TypeInfo_var;
extern const uint32_t OrderedDictionaryEnumerator_get_Current_m2645963973_MetadataUsageId;
extern "C"  Il2CppObject * OrderedDictionaryEnumerator_get_Current_m2645963973 (OrderedDictionaryEnumerator_t3526912157 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OrderedDictionaryEnumerator_get_Current_m2645963973_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DictionaryEntry_t1751606614  L_0 = OrderedDictionaryEnumerator_get_Entry_m3888368964(__this, /*hidden argument*/NULL);
		DictionaryEntry_t1751606614  L_1 = L_0;
		Il2CppObject * L_2 = Box(DictionaryEntry_t1751606614_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Collections.DictionaryEntry LitJson.OrderedDictionaryEnumerator::get_Entry()
extern Il2CppClass* IEnumerator_1_t51112259_il2cpp_TypeInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m3350570668_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m1371513117_MethodInfo_var;
extern const uint32_t OrderedDictionaryEnumerator_get_Entry_m3888368964_MetadataUsageId;
extern "C"  DictionaryEntry_t1751606614  OrderedDictionaryEnumerator_get_Entry_m3888368964 (OrderedDictionaryEnumerator_t3526912157 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OrderedDictionaryEnumerator_get_Entry_m3888368964_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2_t2434214506  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Il2CppObject* L_0 = __this->get_list_enumerator_0();
		NullCheck(L_0);
		KeyValuePair_2_t2434214506  L_1 = InterfaceFuncInvoker0< KeyValuePair_2_t2434214506  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>::get_Current() */, IEnumerator_1_t51112259_il2cpp_TypeInfo_var, L_0);
		V_0 = L_1;
		String_t* L_2 = KeyValuePair_2_get_Key_m3350570668((&V_0), /*hidden argument*/KeyValuePair_2_get_Key_m3350570668_MethodInfo_var);
		JsonData_t1715015430 * L_3 = KeyValuePair_2_get_Value_m1371513117((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m1371513117_MethodInfo_var);
		DictionaryEntry_t1751606614  L_4;
		memset(&L_4, 0, sizeof(L_4));
		DictionaryEntry__ctor_m2600671860(&L_4, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Object LitJson.OrderedDictionaryEnumerator::get_Key()
extern Il2CppClass* IEnumerator_1_t51112259_il2cpp_TypeInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m3350570668_MethodInfo_var;
extern const uint32_t OrderedDictionaryEnumerator_get_Key_m3640750507_MetadataUsageId;
extern "C"  Il2CppObject * OrderedDictionaryEnumerator_get_Key_m3640750507 (OrderedDictionaryEnumerator_t3526912157 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OrderedDictionaryEnumerator_get_Key_m3640750507_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2_t2434214506  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Il2CppObject* L_0 = __this->get_list_enumerator_0();
		NullCheck(L_0);
		KeyValuePair_2_t2434214506  L_1 = InterfaceFuncInvoker0< KeyValuePair_2_t2434214506  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>::get_Current() */, IEnumerator_1_t51112259_il2cpp_TypeInfo_var, L_0);
		V_0 = L_1;
		String_t* L_2 = KeyValuePair_2_get_Key_m3350570668((&V_0), /*hidden argument*/KeyValuePair_2_get_Key_m3350570668_MethodInfo_var);
		return L_2;
	}
}
// System.Object LitJson.OrderedDictionaryEnumerator::get_Value()
extern Il2CppClass* IEnumerator_1_t51112259_il2cpp_TypeInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m1371513117_MethodInfo_var;
extern const uint32_t OrderedDictionaryEnumerator_get_Value_m3706294653_MetadataUsageId;
extern "C"  Il2CppObject * OrderedDictionaryEnumerator_get_Value_m3706294653 (OrderedDictionaryEnumerator_t3526912157 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OrderedDictionaryEnumerator_get_Value_m3706294653_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2_t2434214506  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Il2CppObject* L_0 = __this->get_list_enumerator_0();
		NullCheck(L_0);
		KeyValuePair_2_t2434214506  L_1 = InterfaceFuncInvoker0< KeyValuePair_2_t2434214506  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>::get_Current() */, IEnumerator_1_t51112259_il2cpp_TypeInfo_var, L_0);
		V_0 = L_1;
		JsonData_t1715015430 * L_2 = KeyValuePair_2_get_Value_m1371513117((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m1371513117_MethodInfo_var);
		return L_2;
	}
}
// System.Void LitJson.OrderedDictionaryEnumerator::.ctor(System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,LitJson.JsonData>>)
extern "C"  void OrderedDictionaryEnumerator__ctor_m1595318391 (OrderedDictionaryEnumerator_t3526912157 * __this, Il2CppObject* ___enumerator0, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___enumerator0;
		__this->set_list_enumerator_0(L_0);
		return;
	}
}
// System.Boolean LitJson.OrderedDictionaryEnumerator::MoveNext()
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern const uint32_t OrderedDictionaryEnumerator_MoveNext_m3101166992_MetadataUsageId;
extern "C"  bool OrderedDictionaryEnumerator_MoveNext_m3101166992 (OrderedDictionaryEnumerator_t3526912157 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OrderedDictionaryEnumerator_MoveNext_m3101166992_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = __this->get_list_enumerator_0();
		NullCheck(L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Void LitJson.OrderedDictionaryEnumerator::Reset()
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern const uint32_t OrderedDictionaryEnumerator_Reset_m907474511_MetadataUsageId;
extern "C"  void OrderedDictionaryEnumerator_Reset_m907474511 (OrderedDictionaryEnumerator_t3526912157 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OrderedDictionaryEnumerator_Reset_m907474511_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = __this->get_list_enumerator_0();
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.Collections.IEnumerator::Reset() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_0);
		return;
	}
}
// Conversion methods for marshalling of: LitJson.PropertyMetadata
extern "C" void PropertyMetadata_t4066634616_marshal_pinvoke(const PropertyMetadata_t4066634616& unmarshaled, PropertyMetadata_t4066634616_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___Info_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'Info' of type 'PropertyMetadata': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___Info_0Exception);
}
extern "C" void PropertyMetadata_t4066634616_marshal_pinvoke_back(const PropertyMetadata_t4066634616_marshaled_pinvoke& marshaled, PropertyMetadata_t4066634616& unmarshaled)
{
	Il2CppCodeGenException* ___Info_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'Info' of type 'PropertyMetadata': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___Info_0Exception);
}
// Conversion method for clean up from marshalling of: LitJson.PropertyMetadata
extern "C" void PropertyMetadata_t4066634616_marshal_pinvoke_cleanup(PropertyMetadata_t4066634616_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: LitJson.PropertyMetadata
extern "C" void PropertyMetadata_t4066634616_marshal_com(const PropertyMetadata_t4066634616& unmarshaled, PropertyMetadata_t4066634616_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___Info_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'Info' of type 'PropertyMetadata': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___Info_0Exception);
}
extern "C" void PropertyMetadata_t4066634616_marshal_com_back(const PropertyMetadata_t4066634616_marshaled_com& marshaled, PropertyMetadata_t4066634616& unmarshaled)
{
	Il2CppCodeGenException* ___Info_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'Info' of type 'PropertyMetadata': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___Info_0Exception);
}
// Conversion method for clean up from marshalling of: LitJson.PropertyMetadata
extern "C" void PropertyMetadata_t4066634616_marshal_com_cleanup(PropertyMetadata_t4066634616_marshaled_com& marshaled)
{
}
// System.Void LitJson.WrapperFactory::.ctor(System.Object,System.IntPtr)
extern "C"  void WrapperFactory__ctor_m1067507964 (WrapperFactory_t3264289803 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// LitJson.IJsonWrapper LitJson.WrapperFactory::Invoke()
extern "C"  Il2CppObject * WrapperFactory_Invoke_m650811940 (WrapperFactory_t3264289803 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		WrapperFactory_Invoke_m650811940((WrapperFactory_t3264289803 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult LitJson.WrapperFactory::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * WrapperFactory_BeginInvoke_m1958095341 (WrapperFactory_t3264289803 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// LitJson.IJsonWrapper LitJson.WrapperFactory::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * WrapperFactory_EndInvoke_m419990618 (WrapperFactory_t3264289803 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void LitJson.WriterContext::.ctor()
extern "C"  void WriterContext__ctor_m159390989 (WriterContext_t3060158226 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
