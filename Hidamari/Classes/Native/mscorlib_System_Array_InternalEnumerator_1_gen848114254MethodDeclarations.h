﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen848114254.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22065771578.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3050775493_gshared (InternalEnumerator_1_t848114254 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3050775493(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t848114254 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3050775493_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3236964795_gshared (InternalEnumerator_1_t848114254 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3236964795(__this, method) ((  void (*) (InternalEnumerator_1_t848114254 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3236964795_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1870831463_gshared (InternalEnumerator_1_t848114254 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1870831463(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t848114254 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1870831463_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2373234652_gshared (InternalEnumerator_1_t848114254 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2373234652(__this, method) ((  void (*) (InternalEnumerator_1_t848114254 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2373234652_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3154342823_gshared (InternalEnumerator_1_t848114254 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3154342823(__this, method) ((  bool (*) (InternalEnumerator_1_t848114254 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3154342823_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>>::get_Current()
extern "C"  KeyValuePair_2_t2065771578  InternalEnumerator_1_get_Current_m2312868556_gshared (InternalEnumerator_1_t848114254 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2312868556(__this, method) ((  KeyValuePair_2_t2065771578  (*) (InternalEnumerator_1_t848114254 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2312868556_gshared)(__this, method)
