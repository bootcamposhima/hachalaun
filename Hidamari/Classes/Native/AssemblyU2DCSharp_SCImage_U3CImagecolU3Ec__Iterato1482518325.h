﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t538875265;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2761310900;
// System.Object
struct Il2CppObject;
// SCImage
struct SCImage_t2637275371;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SCImage/<Imagecol>c__Iterator16
struct  U3CImagecolU3Ec__Iterator16_t1482518325  : public Il2CppObject
{
public:
	// System.Single SCImage/<Imagecol>c__Iterator16::<time>__0
	float ___U3CtimeU3E__0_0;
	// System.Single SCImage/<Imagecol>c__Iterator16::<a>__1
	float ___U3CaU3E__1_1;
	// UnityEngine.UI.Image SCImage/<Imagecol>c__Iterator16::<im>__2
	Image_t538875265 * ___U3CimU3E__2_2;
	// System.Int32 SCImage/<Imagecol>c__Iterator16::<i>__3
	int32_t ___U3CiU3E__3_3;
	// UnityEngine.Sprite[] SCImage/<Imagecol>c__Iterator16::image
	SpriteU5BU5D_t2761310900* ___image_4;
	// System.Int32 SCImage/<Imagecol>c__Iterator16::$PC
	int32_t ___U24PC_5;
	// System.Object SCImage/<Imagecol>c__Iterator16::$current
	Il2CppObject * ___U24current_6;
	// UnityEngine.Sprite[] SCImage/<Imagecol>c__Iterator16::<$>image
	SpriteU5BU5D_t2761310900* ___U3CU24U3Eimage_7;
	// SCImage SCImage/<Imagecol>c__Iterator16::<>f__this
	SCImage_t2637275371 * ___U3CU3Ef__this_8;

public:
	inline static int32_t get_offset_of_U3CtimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CImagecolU3Ec__Iterator16_t1482518325, ___U3CtimeU3E__0_0)); }
	inline float get_U3CtimeU3E__0_0() const { return ___U3CtimeU3E__0_0; }
	inline float* get_address_of_U3CtimeU3E__0_0() { return &___U3CtimeU3E__0_0; }
	inline void set_U3CtimeU3E__0_0(float value)
	{
		___U3CtimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CaU3E__1_1() { return static_cast<int32_t>(offsetof(U3CImagecolU3Ec__Iterator16_t1482518325, ___U3CaU3E__1_1)); }
	inline float get_U3CaU3E__1_1() const { return ___U3CaU3E__1_1; }
	inline float* get_address_of_U3CaU3E__1_1() { return &___U3CaU3E__1_1; }
	inline void set_U3CaU3E__1_1(float value)
	{
		___U3CaU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CimU3E__2_2() { return static_cast<int32_t>(offsetof(U3CImagecolU3Ec__Iterator16_t1482518325, ___U3CimU3E__2_2)); }
	inline Image_t538875265 * get_U3CimU3E__2_2() const { return ___U3CimU3E__2_2; }
	inline Image_t538875265 ** get_address_of_U3CimU3E__2_2() { return &___U3CimU3E__2_2; }
	inline void set_U3CimU3E__2_2(Image_t538875265 * value)
	{
		___U3CimU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CimU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CiU3E__3_3() { return static_cast<int32_t>(offsetof(U3CImagecolU3Ec__Iterator16_t1482518325, ___U3CiU3E__3_3)); }
	inline int32_t get_U3CiU3E__3_3() const { return ___U3CiU3E__3_3; }
	inline int32_t* get_address_of_U3CiU3E__3_3() { return &___U3CiU3E__3_3; }
	inline void set_U3CiU3E__3_3(int32_t value)
	{
		___U3CiU3E__3_3 = value;
	}

	inline static int32_t get_offset_of_image_4() { return static_cast<int32_t>(offsetof(U3CImagecolU3Ec__Iterator16_t1482518325, ___image_4)); }
	inline SpriteU5BU5D_t2761310900* get_image_4() const { return ___image_4; }
	inline SpriteU5BU5D_t2761310900** get_address_of_image_4() { return &___image_4; }
	inline void set_image_4(SpriteU5BU5D_t2761310900* value)
	{
		___image_4 = value;
		Il2CppCodeGenWriteBarrier(&___image_4, value);
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CImagecolU3Ec__Iterator16_t1482518325, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CImagecolU3Ec__Iterator16_t1482518325, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eimage_7() { return static_cast<int32_t>(offsetof(U3CImagecolU3Ec__Iterator16_t1482518325, ___U3CU24U3Eimage_7)); }
	inline SpriteU5BU5D_t2761310900* get_U3CU24U3Eimage_7() const { return ___U3CU24U3Eimage_7; }
	inline SpriteU5BU5D_t2761310900** get_address_of_U3CU24U3Eimage_7() { return &___U3CU24U3Eimage_7; }
	inline void set_U3CU24U3Eimage_7(SpriteU5BU5D_t2761310900* value)
	{
		___U3CU24U3Eimage_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eimage_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_8() { return static_cast<int32_t>(offsetof(U3CImagecolU3Ec__Iterator16_t1482518325, ___U3CU3Ef__this_8)); }
	inline SCImage_t2637275371 * get_U3CU3Ef__this_8() const { return ___U3CU3Ef__this_8; }
	inline SCImage_t2637275371 ** get_address_of_U3CU3Ef__this_8() { return &___U3CU3Ef__this_8; }
	inline void set_U3CU3Ef__this_8(SCImage_t2637275371 * value)
	{
		___U3CU3Ef__this_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
