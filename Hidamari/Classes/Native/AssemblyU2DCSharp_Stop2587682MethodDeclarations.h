﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Stop
struct Stop_t2587682;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void Stop::.ctor()
extern "C"  void Stop__ctor_m1036293305 (Stop_t2587682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Stop::Start()
extern "C"  void Stop_Start_m4278398393 (Stop_t2587682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Stop::Update()
extern "C"  void Stop_Update_m3787183476 (Stop_t2587682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Stop::SetisPlay(System.Boolean)
extern "C"  void Stop_SetisPlay_m866394734 (Stop_t2587682 * __this, bool ___play0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Stop::SelectStop()
extern "C"  void Stop_SelectStop_m1267804553 (Stop_t2587682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Stop::NoRetire()
extern "C"  void Stop_NoRetire_m2784259751 (Stop_t2587682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Stop::Retire()
extern "C"  void Stop_Retire_m2146508870 (Stop_t2587682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Stop::ShowRetire()
extern "C"  Il2CppObject * Stop_ShowRetire_m2739177899 (Stop_t2587682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
