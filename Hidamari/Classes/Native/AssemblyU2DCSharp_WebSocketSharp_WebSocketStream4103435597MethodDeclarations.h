﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.WebSocketStream
struct WebSocketStream_t4103435597;
// System.IO.Stream
struct Stream_t1561764144;
// System.Net.Sockets.NetworkStream
struct NetworkStream_t3953762560;
// WebSocketSharp.Net.Security.SslStream
struct SslStream_t3623155758;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.Net.Sockets.TcpClient
struct TcpClient_t838416830;
// System.Net.Security.RemoteCertificateValidationCallback
struct RemoteCertificateValidationCallback_t1894914657;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t3076817455;
// WebSocketSharp.WebSocketFrame
struct WebSocketFrame_t778194306;
// System.Action`1<WebSocketSharp.WebSocketFrame>
struct Action_1_t1174010442;
// System.Action`1<System.Exception>
struct Action_1_t92447661;
// WebSocketSharp.HandshakeRequest
struct HandshakeRequest_t1037477780;
// WebSocketSharp.HandshakeResponse
struct HandshakeResponse_t3229697822;
// WebSocketSharp.HandshakeBase
struct HandshakeBase_t1248407470;
// System.Object
struct Il2CppObject;
// System.Security.Cryptography.X509Certificates.X509Chain
struct X509Chain_t1111884825;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "System_System_Net_Sockets_NetworkStream3953762560.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_Security_SslS3623155758.h"
#include "mscorlib_System_String7231557.h"
#include "System_System_Net_Sockets_TcpClient838416830.h"
#include "System_System_Net_Security_RemoteCertificateValida1894914657.h"
#include "mscorlib_System_Security_Cryptography_X509Certific3076817455.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocketFrame778194306.h"
#include "AssemblyU2DCSharp_WebSocketSharp_HandshakeBase1248407470.h"
#include "mscorlib_System_Object4170816371.h"
#include "System_System_Security_Cryptography_X509Certificat1111884825.h"
#include "System_System_Net_Security_SslPolicyErrors3099591579.h"

// System.Void WebSocketSharp.WebSocketStream::.ctor(System.IO.Stream,System.Boolean)
extern "C"  void WebSocketStream__ctor_m20502183 (WebSocketStream_t4103435597 * __this, Stream_t1561764144 * ___innerStream0, bool ___secure1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocketStream::.ctor(System.Net.Sockets.NetworkStream)
extern "C"  void WebSocketStream__ctor_m4292048655 (WebSocketStream_t4103435597 * __this, NetworkStream_t3953762560 * ___innerStream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocketStream::.ctor(WebSocketSharp.Net.Security.SslStream)
extern "C"  void WebSocketStream__ctor_m1977349721 (WebSocketStream_t4103435597 * __this, SslStream_t3623155758 * ___innerStream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocketStream::get_DataAvailable()
extern "C"  bool WebSocketStream_get_DataAvailable_m2361437439 (WebSocketStream_t4103435597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocketStream::get_IsSecure()
extern "C"  bool WebSocketStream_get_IsSecure_m3197194531 (WebSocketStream_t4103435597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] WebSocketSharp.WebSocketStream::readHandshakeEntityBody(System.IO.Stream,System.String)
extern "C"  ByteU5BU5D_t4260760469* WebSocketStream_readHandshakeEntityBody_m1812677996 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___stream0, String_t* ___length1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] WebSocketSharp.WebSocketStream::readHandshakeHeaders(System.IO.Stream)
extern "C"  StringU5BU5D_t4054002952* WebSocketStream_readHandshakeHeaders_m3212613090 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___stream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocketStream::Write(System.Byte[])
extern "C"  bool WebSocketStream_Write_m2408900417 (WebSocketStream_t4103435597 * __this, ByteU5BU5D_t4260760469* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocketStream::Close()
extern "C"  void WebSocketStream_Close_m3759737493 (WebSocketStream_t4103435597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.WebSocketStream WebSocketSharp.WebSocketStream::CreateClientStream(System.Net.Sockets.TcpClient,System.Boolean,System.String,System.Net.Security.RemoteCertificateValidationCallback)
extern "C"  WebSocketStream_t4103435597 * WebSocketStream_CreateClientStream_m3072197941 (Il2CppObject * __this /* static, unused */, TcpClient_t838416830 * ___client0, bool ___secure1, String_t* ___host2, RemoteCertificateValidationCallback_t1894914657 * ___validationCallback3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.WebSocketStream WebSocketSharp.WebSocketStream::CreateServerStream(System.Net.Sockets.TcpClient,System.Boolean,System.Security.Cryptography.X509Certificates.X509Certificate)
extern "C"  WebSocketStream_t4103435597 * WebSocketStream_CreateServerStream_m1643920762 (Il2CppObject * __this /* static, unused */, TcpClient_t838416830 * ___client0, bool ___secure1, X509Certificate_t3076817455 * ___cert2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocketStream::Dispose()
extern "C"  void WebSocketStream_Dispose_m1774972412 (WebSocketStream_t4103435597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketStream::ReadFrame()
extern "C"  WebSocketFrame_t778194306 * WebSocketStream_ReadFrame_m2749362418 (WebSocketStream_t4103435597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocketStream::ReadFrameAsync(System.Action`1<WebSocketSharp.WebSocketFrame>,System.Action`1<System.Exception>)
extern "C"  void WebSocketStream_ReadFrameAsync_m3409431931 (WebSocketStream_t4103435597 * __this, Action_1_t1174010442 * ___completed0, Action_1_t92447661 * ___error1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.HandshakeRequest WebSocketSharp.WebSocketStream::ReadHandshakeRequest()
extern "C"  HandshakeRequest_t1037477780 * WebSocketStream_ReadHandshakeRequest_m4135427139 (WebSocketStream_t4103435597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.HandshakeResponse WebSocketSharp.WebSocketStream::ReadHandshakeResponse()
extern "C"  HandshakeResponse_t3229697822 * WebSocketStream_ReadHandshakeResponse_m569574287 (WebSocketStream_t4103435597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocketStream::WriteFrame(WebSocketSharp.WebSocketFrame)
extern "C"  bool WebSocketStream_WriteFrame_m665530746 (WebSocketStream_t4103435597 * __this, WebSocketFrame_t778194306 * ___frame0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocketStream::WriteHandshake(WebSocketSharp.HandshakeBase)
extern "C"  bool WebSocketStream_WriteHandshake_m511451412 (WebSocketStream_t4103435597 * __this, HandshakeBase_t1248407470 * ___handshake0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocketStream::<CreateClientStream>m__1E(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern "C"  bool WebSocketStream_U3CCreateClientStreamU3Em__1E_m1962976644 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___sender0, X509Certificate_t3076817455 * ___certificate1, X509Chain_t1111884825 * ___chain2, int32_t ___sslPolicyErrors3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
