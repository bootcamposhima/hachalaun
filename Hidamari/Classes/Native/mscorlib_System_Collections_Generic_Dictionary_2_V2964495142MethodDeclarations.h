﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Char>
struct Dictionary_2_t737694438;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2964495142.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Char>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m4183865912_gshared (Enumerator_t2964495142 * __this, Dictionary_2_t737694438 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m4183865912(__this, ___host0, method) ((  void (*) (Enumerator_t2964495142 *, Dictionary_2_t737694438 *, const MethodInfo*))Enumerator__ctor_m4183865912_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Char>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m627296297_gshared (Enumerator_t2964495142 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m627296297(__this, method) ((  Il2CppObject * (*) (Enumerator_t2964495142 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m627296297_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Char>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m707912573_gshared (Enumerator_t2964495142 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m707912573(__this, method) ((  void (*) (Enumerator_t2964495142 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m707912573_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Char>::Dispose()
extern "C"  void Enumerator_Dispose_m2041577178_gshared (Enumerator_t2964495142 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2041577178(__this, method) ((  void (*) (Enumerator_t2964495142 *, const MethodInfo*))Enumerator_Dispose_m2041577178_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Char>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m412545385_gshared (Enumerator_t2964495142 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m412545385(__this, method) ((  bool (*) (Enumerator_t2964495142 *, const MethodInfo*))Enumerator_MoveNext_m412545385_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Char>::get_Current()
extern "C"  Il2CppChar Enumerator_get_Current_m1889288057_gshared (Enumerator_t2964495142 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1889288057(__this, method) ((  Il2CppChar (*) (Enumerator_t2964495142 *, const MethodInfo*))Enumerator_get_Current_m1889288057_gshared)(__this, method)
