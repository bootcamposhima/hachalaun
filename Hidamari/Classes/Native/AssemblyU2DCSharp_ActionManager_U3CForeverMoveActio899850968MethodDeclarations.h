﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ActionManager/<ForeverMoveAction>c__Iterator5
struct U3CForeverMoveActionU3Ec__Iterator5_t899850968;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ActionManager/<ForeverMoveAction>c__Iterator5::.ctor()
extern "C"  void U3CForeverMoveActionU3Ec__Iterator5__ctor_m2500317587 (U3CForeverMoveActionU3Ec__Iterator5_t899850968 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ActionManager/<ForeverMoveAction>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CForeverMoveActionU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m827498591 (U3CForeverMoveActionU3Ec__Iterator5_t899850968 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ActionManager/<ForeverMoveAction>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CForeverMoveActionU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m632766963 (U3CForeverMoveActionU3Ec__Iterator5_t899850968 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ActionManager/<ForeverMoveAction>c__Iterator5::MoveNext()
extern "C"  bool U3CForeverMoveActionU3Ec__Iterator5_MoveNext_m963652225 (U3CForeverMoveActionU3Ec__Iterator5_t899850968 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActionManager/<ForeverMoveAction>c__Iterator5::Dispose()
extern "C"  void U3CForeverMoveActionU3Ec__Iterator5_Dispose_m1816765712 (U3CForeverMoveActionU3Ec__Iterator5_t899850968 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActionManager/<ForeverMoveAction>c__Iterator5::Reset()
extern "C"  void U3CForeverMoveActionU3Ec__Iterator5_Reset_m146750528 (U3CForeverMoveActionU3Ec__Iterator5_t899850968 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
