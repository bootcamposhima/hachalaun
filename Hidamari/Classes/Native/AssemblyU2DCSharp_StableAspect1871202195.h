﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Camera
struct Camera_t2727095145;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StableAspect
struct  StableAspect_t1871202195  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Camera StableAspect::cam
	Camera_t2727095145 * ___cam_2;
	// System.Single StableAspect::width
	float ___width_3;
	// System.Single StableAspect::height
	float ___height_4;
	// System.Single StableAspect::pixelPerUnit
	float ___pixelPerUnit_5;

public:
	inline static int32_t get_offset_of_cam_2() { return static_cast<int32_t>(offsetof(StableAspect_t1871202195, ___cam_2)); }
	inline Camera_t2727095145 * get_cam_2() const { return ___cam_2; }
	inline Camera_t2727095145 ** get_address_of_cam_2() { return &___cam_2; }
	inline void set_cam_2(Camera_t2727095145 * value)
	{
		___cam_2 = value;
		Il2CppCodeGenWriteBarrier(&___cam_2, value);
	}

	inline static int32_t get_offset_of_width_3() { return static_cast<int32_t>(offsetof(StableAspect_t1871202195, ___width_3)); }
	inline float get_width_3() const { return ___width_3; }
	inline float* get_address_of_width_3() { return &___width_3; }
	inline void set_width_3(float value)
	{
		___width_3 = value;
	}

	inline static int32_t get_offset_of_height_4() { return static_cast<int32_t>(offsetof(StableAspect_t1871202195, ___height_4)); }
	inline float get_height_4() const { return ___height_4; }
	inline float* get_address_of_height_4() { return &___height_4; }
	inline void set_height_4(float value)
	{
		___height_4 = value;
	}

	inline static int32_t get_offset_of_pixelPerUnit_5() { return static_cast<int32_t>(offsetof(StableAspect_t1871202195, ___pixelPerUnit_5)); }
	inline float get_pixelPerUnit_5() const { return ___pixelPerUnit_5; }
	inline float* get_address_of_pixelPerUnit_5() { return &___pixelPerUnit_5; }
	inline void set_pixelPerUnit_5(float value)
	{
		___pixelPerUnit_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
