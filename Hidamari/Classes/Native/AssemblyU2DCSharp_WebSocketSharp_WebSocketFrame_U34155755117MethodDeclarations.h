﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator20
struct U3CGetEnumeratorU3Ec__Iterator20_t4155755117;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator20::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator20__ctor_m2089199822 (U3CGetEnumeratorU3Ec__Iterator20_t4155755117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator20::System.Collections.Generic.IEnumerator<byte>.get_Current()
extern "C"  uint8_t U3CGetEnumeratorU3Ec__Iterator20_System_Collections_Generic_IEnumeratorU3CbyteU3E_get_Current_m674939580 (U3CGetEnumeratorU3Ec__Iterator20_t4155755117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator20::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator20_System_Collections_IEnumerator_get_Current_m1232272674 (U3CGetEnumeratorU3Ec__Iterator20_t4155755117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator20::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator20_MoveNext_m3719833934 (U3CGetEnumeratorU3Ec__Iterator20_t4155755117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator20::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator20_Dispose_m1869584779 (U3CGetEnumeratorU3Ec__Iterator20_t4155755117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator20::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator20_Reset_m4030600059 (U3CGetEnumeratorU3Ec__Iterator20_t4155755117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
