﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`2<WebSocketSharp.Net.HttpListenerRequest,WebSocketSharp.Net.AuthenticationSchemes>
struct Func_2_t310774036;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>
struct Dictionary_2_t3390149787;
// System.Collections.Generic.List`1<WebSocketSharp.Net.HttpListenerContext>
struct List_1_t817877357;
// System.Func`2<System.Security.Principal.IIdentity,WebSocketSharp.Net.NetworkCredential>
struct Func_2_t4138449219;
// System.Security.Cryptography.X509Certificates.X509Certificate2
struct X509Certificate2_t160474609;
// WebSocketSharp.Net.HttpListenerPrefixCollection
struct HttpListenerPrefixCollection_t1379363822;
// System.Collections.Generic.Dictionary`2<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>
struct Dictionary_2_t1349563175;
// System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerAsyncResult>
struct List_1_t1977401631;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_Authenticatio3190130368.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpListener
struct  HttpListener_t398944510  : public Il2CppObject
{
public:
	// WebSocketSharp.Net.AuthenticationSchemes WebSocketSharp.Net.HttpListener::_authSchemes
	int32_t ____authSchemes_0;
	// System.Func`2<WebSocketSharp.Net.HttpListenerRequest,WebSocketSharp.Net.AuthenticationSchemes> WebSocketSharp.Net.HttpListener::_authSchemeSelector
	Func_2_t310774036 * ____authSchemeSelector_1;
	// System.String WebSocketSharp.Net.HttpListener::_certFolderPath
	String_t* ____certFolderPath_2;
	// System.Collections.Generic.Dictionary`2<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection> WebSocketSharp.Net.HttpListener::_connections
	Dictionary_2_t3390149787 * ____connections_3;
	// System.Collections.Generic.List`1<WebSocketSharp.Net.HttpListenerContext> WebSocketSharp.Net.HttpListener::_contextQueue
	List_1_t817877357 * ____contextQueue_4;
	// System.Func`2<System.Security.Principal.IIdentity,WebSocketSharp.Net.NetworkCredential> WebSocketSharp.Net.HttpListener::_credentialsFinder
	Func_2_t4138449219 * ____credentialsFinder_5;
	// System.Security.Cryptography.X509Certificates.X509Certificate2 WebSocketSharp.Net.HttpListener::_defaultCert
	X509Certificate2_t160474609 * ____defaultCert_6;
	// System.Boolean WebSocketSharp.Net.HttpListener::_disposed
	bool ____disposed_7;
	// System.Boolean WebSocketSharp.Net.HttpListener::_ignoreWriteExceptions
	bool ____ignoreWriteExceptions_8;
	// System.Boolean WebSocketSharp.Net.HttpListener::_listening
	bool ____listening_9;
	// WebSocketSharp.Net.HttpListenerPrefixCollection WebSocketSharp.Net.HttpListener::_prefixes
	HttpListenerPrefixCollection_t1379363822 * ____prefixes_10;
	// System.String WebSocketSharp.Net.HttpListener::_realm
	String_t* ____realm_11;
	// System.Collections.Generic.Dictionary`2<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext> WebSocketSharp.Net.HttpListener::_registry
	Dictionary_2_t1349563175 * ____registry_12;
	// System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerAsyncResult> WebSocketSharp.Net.HttpListener::_waitQueue
	List_1_t1977401631 * ____waitQueue_13;

public:
	inline static int32_t get_offset_of__authSchemes_0() { return static_cast<int32_t>(offsetof(HttpListener_t398944510, ____authSchemes_0)); }
	inline int32_t get__authSchemes_0() const { return ____authSchemes_0; }
	inline int32_t* get_address_of__authSchemes_0() { return &____authSchemes_0; }
	inline void set__authSchemes_0(int32_t value)
	{
		____authSchemes_0 = value;
	}

	inline static int32_t get_offset_of__authSchemeSelector_1() { return static_cast<int32_t>(offsetof(HttpListener_t398944510, ____authSchemeSelector_1)); }
	inline Func_2_t310774036 * get__authSchemeSelector_1() const { return ____authSchemeSelector_1; }
	inline Func_2_t310774036 ** get_address_of__authSchemeSelector_1() { return &____authSchemeSelector_1; }
	inline void set__authSchemeSelector_1(Func_2_t310774036 * value)
	{
		____authSchemeSelector_1 = value;
		Il2CppCodeGenWriteBarrier(&____authSchemeSelector_1, value);
	}

	inline static int32_t get_offset_of__certFolderPath_2() { return static_cast<int32_t>(offsetof(HttpListener_t398944510, ____certFolderPath_2)); }
	inline String_t* get__certFolderPath_2() const { return ____certFolderPath_2; }
	inline String_t** get_address_of__certFolderPath_2() { return &____certFolderPath_2; }
	inline void set__certFolderPath_2(String_t* value)
	{
		____certFolderPath_2 = value;
		Il2CppCodeGenWriteBarrier(&____certFolderPath_2, value);
	}

	inline static int32_t get_offset_of__connections_3() { return static_cast<int32_t>(offsetof(HttpListener_t398944510, ____connections_3)); }
	inline Dictionary_2_t3390149787 * get__connections_3() const { return ____connections_3; }
	inline Dictionary_2_t3390149787 ** get_address_of__connections_3() { return &____connections_3; }
	inline void set__connections_3(Dictionary_2_t3390149787 * value)
	{
		____connections_3 = value;
		Il2CppCodeGenWriteBarrier(&____connections_3, value);
	}

	inline static int32_t get_offset_of__contextQueue_4() { return static_cast<int32_t>(offsetof(HttpListener_t398944510, ____contextQueue_4)); }
	inline List_1_t817877357 * get__contextQueue_4() const { return ____contextQueue_4; }
	inline List_1_t817877357 ** get_address_of__contextQueue_4() { return &____contextQueue_4; }
	inline void set__contextQueue_4(List_1_t817877357 * value)
	{
		____contextQueue_4 = value;
		Il2CppCodeGenWriteBarrier(&____contextQueue_4, value);
	}

	inline static int32_t get_offset_of__credentialsFinder_5() { return static_cast<int32_t>(offsetof(HttpListener_t398944510, ____credentialsFinder_5)); }
	inline Func_2_t4138449219 * get__credentialsFinder_5() const { return ____credentialsFinder_5; }
	inline Func_2_t4138449219 ** get_address_of__credentialsFinder_5() { return &____credentialsFinder_5; }
	inline void set__credentialsFinder_5(Func_2_t4138449219 * value)
	{
		____credentialsFinder_5 = value;
		Il2CppCodeGenWriteBarrier(&____credentialsFinder_5, value);
	}

	inline static int32_t get_offset_of__defaultCert_6() { return static_cast<int32_t>(offsetof(HttpListener_t398944510, ____defaultCert_6)); }
	inline X509Certificate2_t160474609 * get__defaultCert_6() const { return ____defaultCert_6; }
	inline X509Certificate2_t160474609 ** get_address_of__defaultCert_6() { return &____defaultCert_6; }
	inline void set__defaultCert_6(X509Certificate2_t160474609 * value)
	{
		____defaultCert_6 = value;
		Il2CppCodeGenWriteBarrier(&____defaultCert_6, value);
	}

	inline static int32_t get_offset_of__disposed_7() { return static_cast<int32_t>(offsetof(HttpListener_t398944510, ____disposed_7)); }
	inline bool get__disposed_7() const { return ____disposed_7; }
	inline bool* get_address_of__disposed_7() { return &____disposed_7; }
	inline void set__disposed_7(bool value)
	{
		____disposed_7 = value;
	}

	inline static int32_t get_offset_of__ignoreWriteExceptions_8() { return static_cast<int32_t>(offsetof(HttpListener_t398944510, ____ignoreWriteExceptions_8)); }
	inline bool get__ignoreWriteExceptions_8() const { return ____ignoreWriteExceptions_8; }
	inline bool* get_address_of__ignoreWriteExceptions_8() { return &____ignoreWriteExceptions_8; }
	inline void set__ignoreWriteExceptions_8(bool value)
	{
		____ignoreWriteExceptions_8 = value;
	}

	inline static int32_t get_offset_of__listening_9() { return static_cast<int32_t>(offsetof(HttpListener_t398944510, ____listening_9)); }
	inline bool get__listening_9() const { return ____listening_9; }
	inline bool* get_address_of__listening_9() { return &____listening_9; }
	inline void set__listening_9(bool value)
	{
		____listening_9 = value;
	}

	inline static int32_t get_offset_of__prefixes_10() { return static_cast<int32_t>(offsetof(HttpListener_t398944510, ____prefixes_10)); }
	inline HttpListenerPrefixCollection_t1379363822 * get__prefixes_10() const { return ____prefixes_10; }
	inline HttpListenerPrefixCollection_t1379363822 ** get_address_of__prefixes_10() { return &____prefixes_10; }
	inline void set__prefixes_10(HttpListenerPrefixCollection_t1379363822 * value)
	{
		____prefixes_10 = value;
		Il2CppCodeGenWriteBarrier(&____prefixes_10, value);
	}

	inline static int32_t get_offset_of__realm_11() { return static_cast<int32_t>(offsetof(HttpListener_t398944510, ____realm_11)); }
	inline String_t* get__realm_11() const { return ____realm_11; }
	inline String_t** get_address_of__realm_11() { return &____realm_11; }
	inline void set__realm_11(String_t* value)
	{
		____realm_11 = value;
		Il2CppCodeGenWriteBarrier(&____realm_11, value);
	}

	inline static int32_t get_offset_of__registry_12() { return static_cast<int32_t>(offsetof(HttpListener_t398944510, ____registry_12)); }
	inline Dictionary_2_t1349563175 * get__registry_12() const { return ____registry_12; }
	inline Dictionary_2_t1349563175 ** get_address_of__registry_12() { return &____registry_12; }
	inline void set__registry_12(Dictionary_2_t1349563175 * value)
	{
		____registry_12 = value;
		Il2CppCodeGenWriteBarrier(&____registry_12, value);
	}

	inline static int32_t get_offset_of__waitQueue_13() { return static_cast<int32_t>(offsetof(HttpListener_t398944510, ____waitQueue_13)); }
	inline List_1_t1977401631 * get__waitQueue_13() const { return ____waitQueue_13; }
	inline List_1_t1977401631 ** get_address_of__waitQueue_13() { return &____waitQueue_13; }
	inline void set__waitQueue_13(List_1_t1977401631 * value)
	{
		____waitQueue_13 = value;
		Il2CppCodeGenWriteBarrier(&____waitQueue_13, value);
	}
};

struct HttpListener_t398944510_StaticFields
{
public:
	// System.Func`2<System.Security.Principal.IIdentity,WebSocketSharp.Net.NetworkCredential> WebSocketSharp.Net.HttpListener::<>f__am$cacheE
	Func_2_t4138449219 * ___U3CU3Ef__amU24cacheE_14;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheE_14() { return static_cast<int32_t>(offsetof(HttpListener_t398944510_StaticFields, ___U3CU3Ef__amU24cacheE_14)); }
	inline Func_2_t4138449219 * get_U3CU3Ef__amU24cacheE_14() const { return ___U3CU3Ef__amU24cacheE_14; }
	inline Func_2_t4138449219 ** get_address_of_U3CU3Ef__amU24cacheE_14() { return &___U3CU3Ef__amU24cacheE_14; }
	inline void set_U3CU3Ef__amU24cacheE_14(Func_2_t4138449219 * value)
	{
		___U3CU3Ef__amU24cacheE_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheE_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
