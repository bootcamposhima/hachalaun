﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_gen2112091504MethodDeclarations.h"

// System.Void System.Collections.Generic.Queue`1<WebSocketSharp.MessageEventArgs>::.ctor()
#define Queue_1__ctor_m885798143(__this, method) ((  void (*) (Queue_1_t2273188169 *, const MethodInfo*))Queue_1__ctor_m3042804833_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<WebSocketSharp.MessageEventArgs>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Queue_1_System_Collections_ICollection_CopyTo_m1834402214(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t2273188169 *, Il2CppArray *, int32_t, const MethodInfo*))Queue_1_System_Collections_ICollection_CopyTo_m3260144643_gshared)(__this, ___array0, ___idx1, method)
// System.Boolean System.Collections.Generic.Queue`1<WebSocketSharp.MessageEventArgs>::System.Collections.ICollection.get_IsSynchronized()
#define Queue_1_System_Collections_ICollection_get_IsSynchronized_m1510301912(__this, method) ((  bool (*) (Queue_1_t2273188169 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_IsSynchronized_m63917275_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1<WebSocketSharp.MessageEventArgs>::System.Collections.ICollection.get_SyncRoot()
#define Queue_1_System_Collections_ICollection_get_SyncRoot_m4080040246(__this, method) ((  Il2CppObject * (*) (Queue_1_t2273188169 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_SyncRoot_m2093948217_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<WebSocketSharp.MessageEventArgs>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2745030120(__this, method) ((  Il2CppObject* (*) (Queue_1_t2273188169 *, const MethodInfo*))Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m472615211_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<WebSocketSharp.MessageEventArgs>::System.Collections.IEnumerable.GetEnumerator()
#define Queue_1_System_Collections_IEnumerable_GetEnumerator_m3972761889(__this, method) ((  Il2CppObject * (*) (Queue_1_t2273188169 *, const MethodInfo*))Queue_1_System_Collections_IEnumerable_GetEnumerator_m3688614462_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<WebSocketSharp.MessageEventArgs>::Clear()
#define Queue_1_Clear_m2586898730(__this, method) ((  void (*) (Queue_1_t2273188169 *, const MethodInfo*))Queue_1_Clear_m448938124_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<WebSocketSharp.MessageEventArgs>::CopyTo(T[],System.Int32)
#define Queue_1_CopyTo_m1107465489(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t2273188169 *, MessageEventArgsU5BU5D_t3247095301*, int32_t, const MethodInfo*))Queue_1_CopyTo_m3592753262_gshared)(__this, ___array0, ___idx1, method)
// T System.Collections.Generic.Queue`1<WebSocketSharp.MessageEventArgs>::Dequeue()
#define Queue_1_Dequeue_m4004481531(__this, method) ((  MessageEventArgs_t36945740 * (*) (Queue_1_t2273188169 *, const MethodInfo*))Queue_1_Dequeue_m102813934_gshared)(__this, method)
// T System.Collections.Generic.Queue`1<WebSocketSharp.MessageEventArgs>::Peek()
#define Queue_1_Peek_m773757730(__this, method) ((  MessageEventArgs_t36945740 * (*) (Queue_1_t2273188169 *, const MethodInfo*))Queue_1_Peek_m3013356031_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<WebSocketSharp.MessageEventArgs>::Enqueue(T)
#define Queue_1_Enqueue_m1121821142(__this, ___item0, method) ((  void (*) (Queue_1_t2273188169 *, MessageEventArgs_t36945740 *, const MethodInfo*))Queue_1_Enqueue_m4079343671_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Queue`1<WebSocketSharp.MessageEventArgs>::SetCapacity(System.Int32)
#define Queue_1_SetCapacity_m1839180745(__this, ___new_size0, method) ((  void (*) (Queue_1_t2273188169 *, int32_t, const MethodInfo*))Queue_1_SetCapacity_m1573690380_gshared)(__this, ___new_size0, method)
// System.Int32 System.Collections.Generic.Queue`1<WebSocketSharp.MessageEventArgs>::get_Count()
#define Queue_1_get_Count_m4166990737(__this, method) ((  int32_t (*) (Queue_1_t2273188169 *, const MethodInfo*))Queue_1_get_Count_m1429559317_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<WebSocketSharp.MessageEventArgs>::GetEnumerator()
#define Queue_1_GetEnumerator_m2381364847(__this, method) ((  Enumerator_t3562273681  (*) (Queue_1_t2273188169 *, const MethodInfo*))Queue_1_GetEnumerator_m3965043378_gshared)(__this, method)
