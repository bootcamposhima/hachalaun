﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_ValueType1744280289.h"
#include "AssemblyU2DCSharp_JsonVector3719439144.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoveJson
struct  MoveJson_t4254904441 
{
public:
	// JsonVector3 MoveJson::position
	JsonVector3_t719439144  ___position_0;
	// JsonVector3 MoveJson::movepos
	JsonVector3_t719439144  ___movepos_1;
	// System.Int32 MoveJson::movestate
	int32_t ___movestate_2;
	// System.String MoveJson::charspd
	String_t* ___charspd_3;
	// System.String MoveJson::time
	String_t* ___time_4;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(MoveJson_t4254904441, ___position_0)); }
	inline JsonVector3_t719439144  get_position_0() const { return ___position_0; }
	inline JsonVector3_t719439144 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(JsonVector3_t719439144  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_movepos_1() { return static_cast<int32_t>(offsetof(MoveJson_t4254904441, ___movepos_1)); }
	inline JsonVector3_t719439144  get_movepos_1() const { return ___movepos_1; }
	inline JsonVector3_t719439144 * get_address_of_movepos_1() { return &___movepos_1; }
	inline void set_movepos_1(JsonVector3_t719439144  value)
	{
		___movepos_1 = value;
	}

	inline static int32_t get_offset_of_movestate_2() { return static_cast<int32_t>(offsetof(MoveJson_t4254904441, ___movestate_2)); }
	inline int32_t get_movestate_2() const { return ___movestate_2; }
	inline int32_t* get_address_of_movestate_2() { return &___movestate_2; }
	inline void set_movestate_2(int32_t value)
	{
		___movestate_2 = value;
	}

	inline static int32_t get_offset_of_charspd_3() { return static_cast<int32_t>(offsetof(MoveJson_t4254904441, ___charspd_3)); }
	inline String_t* get_charspd_3() const { return ___charspd_3; }
	inline String_t** get_address_of_charspd_3() { return &___charspd_3; }
	inline void set_charspd_3(String_t* value)
	{
		___charspd_3 = value;
		Il2CppCodeGenWriteBarrier(&___charspd_3, value);
	}

	inline static int32_t get_offset_of_time_4() { return static_cast<int32_t>(offsetof(MoveJson_t4254904441, ___time_4)); }
	inline String_t* get_time_4() const { return ___time_4; }
	inline String_t** get_address_of_time_4() { return &___time_4; }
	inline void set_time_4(String_t* value)
	{
		___time_4 = value;
		Il2CppCodeGenWriteBarrier(&___time_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: MoveJson
struct MoveJson_t4254904441_marshaled_pinvoke
{
	JsonVector3_t719439144_marshaled_pinvoke ___position_0;
	JsonVector3_t719439144_marshaled_pinvoke ___movepos_1;
	int32_t ___movestate_2;
	char* ___charspd_3;
	char* ___time_4;
};
// Native definition for marshalling of: MoveJson
struct MoveJson_t4254904441_marshaled_com
{
	JsonVector3_t719439144_marshaled_com ___position_0;
	JsonVector3_t719439144_marshaled_com ___movepos_1;
	int32_t ___movestate_2;
	Il2CppChar* ___charspd_3;
	Il2CppChar* ___time_4;
};
