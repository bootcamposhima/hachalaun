﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// WebSocketSharp.Net.HttpConnection
struct HttpConnection_t602292776;
// System.String
struct String_t;
// WebSocketSharp.Net.HttpListenerRequest
struct HttpListenerRequest_t3888821117;
// WebSocketSharp.Net.HttpListenerResponse
struct HttpListenerResponse_t1992878431;
// System.Security.Principal.IPrincipal
struct IPrincipal_t1899242073;
// WebSocketSharp.Net.HttpListener
struct HttpListener_t398944510;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpListenerContext
struct  HttpListenerContext_t3744659101  : public Il2CppObject
{
public:
	// WebSocketSharp.Net.HttpConnection WebSocketSharp.Net.HttpListenerContext::_connection
	HttpConnection_t602292776 * ____connection_0;
	// System.String WebSocketSharp.Net.HttpListenerContext::_error
	String_t* ____error_1;
	// System.Int32 WebSocketSharp.Net.HttpListenerContext::_errorStatus
	int32_t ____errorStatus_2;
	// WebSocketSharp.Net.HttpListenerRequest WebSocketSharp.Net.HttpListenerContext::_request
	HttpListenerRequest_t3888821117 * ____request_3;
	// WebSocketSharp.Net.HttpListenerResponse WebSocketSharp.Net.HttpListenerContext::_response
	HttpListenerResponse_t1992878431 * ____response_4;
	// System.Security.Principal.IPrincipal WebSocketSharp.Net.HttpListenerContext::_user
	Il2CppObject * ____user_5;
	// WebSocketSharp.Net.HttpListener WebSocketSharp.Net.HttpListenerContext::Listener
	HttpListener_t398944510 * ___Listener_6;

public:
	inline static int32_t get_offset_of__connection_0() { return static_cast<int32_t>(offsetof(HttpListenerContext_t3744659101, ____connection_0)); }
	inline HttpConnection_t602292776 * get__connection_0() const { return ____connection_0; }
	inline HttpConnection_t602292776 ** get_address_of__connection_0() { return &____connection_0; }
	inline void set__connection_0(HttpConnection_t602292776 * value)
	{
		____connection_0 = value;
		Il2CppCodeGenWriteBarrier(&____connection_0, value);
	}

	inline static int32_t get_offset_of__error_1() { return static_cast<int32_t>(offsetof(HttpListenerContext_t3744659101, ____error_1)); }
	inline String_t* get__error_1() const { return ____error_1; }
	inline String_t** get_address_of__error_1() { return &____error_1; }
	inline void set__error_1(String_t* value)
	{
		____error_1 = value;
		Il2CppCodeGenWriteBarrier(&____error_1, value);
	}

	inline static int32_t get_offset_of__errorStatus_2() { return static_cast<int32_t>(offsetof(HttpListenerContext_t3744659101, ____errorStatus_2)); }
	inline int32_t get__errorStatus_2() const { return ____errorStatus_2; }
	inline int32_t* get_address_of__errorStatus_2() { return &____errorStatus_2; }
	inline void set__errorStatus_2(int32_t value)
	{
		____errorStatus_2 = value;
	}

	inline static int32_t get_offset_of__request_3() { return static_cast<int32_t>(offsetof(HttpListenerContext_t3744659101, ____request_3)); }
	inline HttpListenerRequest_t3888821117 * get__request_3() const { return ____request_3; }
	inline HttpListenerRequest_t3888821117 ** get_address_of__request_3() { return &____request_3; }
	inline void set__request_3(HttpListenerRequest_t3888821117 * value)
	{
		____request_3 = value;
		Il2CppCodeGenWriteBarrier(&____request_3, value);
	}

	inline static int32_t get_offset_of__response_4() { return static_cast<int32_t>(offsetof(HttpListenerContext_t3744659101, ____response_4)); }
	inline HttpListenerResponse_t1992878431 * get__response_4() const { return ____response_4; }
	inline HttpListenerResponse_t1992878431 ** get_address_of__response_4() { return &____response_4; }
	inline void set__response_4(HttpListenerResponse_t1992878431 * value)
	{
		____response_4 = value;
		Il2CppCodeGenWriteBarrier(&____response_4, value);
	}

	inline static int32_t get_offset_of__user_5() { return static_cast<int32_t>(offsetof(HttpListenerContext_t3744659101, ____user_5)); }
	inline Il2CppObject * get__user_5() const { return ____user_5; }
	inline Il2CppObject ** get_address_of__user_5() { return &____user_5; }
	inline void set__user_5(Il2CppObject * value)
	{
		____user_5 = value;
		Il2CppCodeGenWriteBarrier(&____user_5, value);
	}

	inline static int32_t get_offset_of_Listener_6() { return static_cast<int32_t>(offsetof(HttpListenerContext_t3744659101, ___Listener_6)); }
	inline HttpListener_t398944510 * get_Listener_6() const { return ___Listener_6; }
	inline HttpListener_t398944510 ** get_address_of_Listener_6() { return &___Listener_6; }
	inline void set_Listener_6(HttpListener_t398944510 * value)
	{
		___Listener_6 = value;
		Il2CppCodeGenWriteBarrier(&___Listener_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
