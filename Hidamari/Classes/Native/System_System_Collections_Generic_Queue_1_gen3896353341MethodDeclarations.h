﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_gen2112091504MethodDeclarations.h"

// System.Void System.Collections.Generic.Queue`1<SocketIO.Packet>::.ctor()
#define Queue_1__ctor_m927279137(__this, method) ((  void (*) (Queue_1_t3896353341 *, const MethodInfo*))Queue_1__ctor_m3042804833_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<SocketIO.Packet>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Queue_1_System_Collections_ICollection_CopyTo_m2212061892(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t3896353341 *, Il2CppArray *, int32_t, const MethodInfo*))Queue_1_System_Collections_ICollection_CopyTo_m3260144643_gshared)(__this, ___array0, ___idx1, method)
// System.Boolean System.Collections.Generic.Queue`1<SocketIO.Packet>::System.Collections.ICollection.get_IsSynchronized()
#define Queue_1_System_Collections_ICollection_get_IsSynchronized_m2382967546(__this, method) ((  bool (*) (Queue_1_t3896353341 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_IsSynchronized_m63917275_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1<SocketIO.Packet>::System.Collections.ICollection.get_SyncRoot()
#define Queue_1_System_Collections_ICollection_get_SyncRoot_m3435932376(__this, method) ((  Il2CppObject * (*) (Queue_1_t3896353341 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_SyncRoot_m2093948217_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<SocketIO.Packet>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1935463050(__this, method) ((  Il2CppObject* (*) (Queue_1_t3896353341 *, const MethodInfo*))Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m472615211_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<SocketIO.Packet>::System.Collections.IEnumerable.GetEnumerator()
#define Queue_1_System_Collections_IEnumerable_GetEnumerator_m2146091455(__this, method) ((  Il2CppObject * (*) (Queue_1_t3896353341 *, const MethodInfo*))Queue_1_System_Collections_IEnumerable_GetEnumerator_m3688614462_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<SocketIO.Packet>::Clear()
#define Queue_1_Clear_m1922778795(__this, method) ((  void (*) (Queue_1_t3896353341 *, const MethodInfo*))Queue_1_Clear_m448938124_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<SocketIO.Packet>::CopyTo(T[],System.Int32)
#define Queue_1_CopyTo_m1147350063(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t3896353341 *, PacketU5BU5D_t676810433*, int32_t, const MethodInfo*))Queue_1_CopyTo_m3592753262_gshared)(__this, ___array0, ___idx1, method)
// T System.Collections.Generic.Queue`1<SocketIO.Packet>::Dequeue()
#define Queue_1_Dequeue_m2648004765(__this, method) ((  Packet_t1660110912 * (*) (Queue_1_t3896353341 *, const MethodInfo*))Queue_1_Dequeue_m102813934_gshared)(__this, method)
// T System.Collections.Generic.Queue`1<SocketIO.Packet>::Peek()
#define Queue_1_Peek_m2121416640(__this, method) ((  Packet_t1660110912 * (*) (Queue_1_t3896353341 *, const MethodInfo*))Queue_1_Peek_m3013356031_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<SocketIO.Packet>::Enqueue(T)
#define Queue_1_Enqueue_m2877567992(__this, ___item0, method) ((  void (*) (Queue_1_t3896353341 *, Packet_t1660110912 *, const MethodInfo*))Queue_1_Enqueue_m4079343671_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Queue`1<SocketIO.Packet>::SetCapacity(System.Int32)
#define Queue_1_SetCapacity_m3075602539(__this, ___new_size0, method) ((  void (*) (Queue_1_t3896353341 *, int32_t, const MethodInfo*))Queue_1_SetCapacity_m1573690380_gshared)(__this, ___new_size0, method)
// System.Int32 System.Collections.Generic.Queue`1<SocketIO.Packet>::get_Count()
#define Queue_1_get_Count_m291482035(__this, method) ((  int32_t (*) (Queue_1_t3896353341 *, const MethodInfo*))Queue_1_get_Count_m1429559317_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<SocketIO.Packet>::GetEnumerator()
#define Queue_1_GetEnumerator_m3172900753(__this, method) ((  Enumerator_t890471557  (*) (Queue_1_t3896353341 *, const MethodInfo*))Queue_1_GetEnumerator_m3965043378_gshared)(__this, method)
