﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_VertexHelper3377436606.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect3555037586.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffect2306480155.h"
#include "UnityEngine_UI_UnityEngine_UI_Outline3745177896.h"
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV14062429115.h"
#include "UnityEngine_UI_UnityEngine_UI_Shadow75537580.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E3053238933.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E_3379220348.h"
#include "UnityEngine_Analytics_U3CModuleU3E86524790.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790.h"
#include "AssemblyU2DCSharp_AIData1930434546.h"
#include "AssemblyU2DCSharp_AIDatas4008896193.h"
#include "AssemblyU2DCSharp_AI2088.h"
#include "AssemblyU2DCSharp_BehaviorTree2111599920.h"
#include "AssemblyU2DCSharp_BehaviorTree_U3CAIPlayU3Ec__Itera226282564.h"
#include "AssemblyU2DCSharp_BlackBoard328561223.h"
#include "AssemblyU2DCSharp_BulletSarch300647239.h"
#include "AssemblyU2DCSharp_Condition1142656251.h"
#include "AssemblyU2DCSharp_IsFlag2198325206.h"
#include "AssemblyU2DCSharp_TaskOrder2170039465.h"
#include "AssemblyU2DCSharp_TaskOrder_U3CTaskStartU3Ec__Iter3806606775.h"
#include "AssemblyU2DCSharp_CenterMove3155240422.h"
#include "AssemblyU2DCSharp_Move2404337.h"
#include "AssemblyU2DCSharp_SearchMoveTask1050018526.h"
#include "AssemblyU2DCSharp_Task2599333.h"
#include "AssemblyU2DCSharp_BigballBullet3675719873.h"
#include "AssemblyU2DCSharp_Bullet2000900386.h"
#include "AssemblyU2DCSharp_BulletNumber605304587.h"
#include "AssemblyU2DCSharp_CannonBullet1475316863.h"
#include "AssemblyU2DCSharp_RiceBullet2445594459.h"
#include "AssemblyU2DCSharp_Rocket2453156596.h"
#include "AssemblyU2DCSharp_TestBullet3235265620.h"
#include "AssemblyU2DCSharp_DefenceManager3329977693.h"
#include "AssemblyU2DCSharp_DemoPlay921155543.h"
#include "AssemblyU2DCSharp_Player2393081601.h"
#include "AssemblyU2DCSharp_PlayerJson4089020297.h"
#include "AssemblyU2DCSharp_MoveJson4254904441.h"
#include "AssemblyU2DCSharp_HitJson2591263483.h"
#include "AssemblyU2DCSharp_JsonVector3719439144.h"
#include "AssemblyU2DCSharp_ShirkAI3726263989.h"
#include "AssemblyU2DCSharp_Charctor1500651146.h"
#include "AssemblyU2DCSharp_Charctor_U3CEndActionColU3Ec__It2737953241.h"
#include "AssemblyU2DCSharp_CharData1499709504.h"
#include "AssemblyU2DCSharp_East2152477.h"
#include "AssemblyU2DCSharp_Enemy67100520.h"
#include "AssemblyU2DCSharp_Enemy_U3CEndActionColU3Ec__Itera1666219580.h"
#include "AssemblyU2DCSharp_Direction1041377119.h"
#include "AssemblyU2DCSharp_EnemyManager1035150117.h"
#include "AssemblyU2DCSharp_North75454693.h"
#include "AssemblyU2DCSharp_South80075181.h"
#include "AssemblyU2DCSharp_TitleEnemy195907632.h"
#include "AssemblyU2DCSharp_West2692559.h"
#include "AssemblyU2DCSharp_SkipSpeed4087407080.h"
#include "AssemblyU2DCSharp_Stop2587682.h"
#include "AssemblyU2DCSharp_Stop_U3CShowRetireU3Ec__Iterator2670935930.h"
#include "AssemblyU2DCSharp_ActionManager3022458999.h"
#include "AssemblyU2DCSharp_ActionManager_U3CForeverMoveActio899850968.h"
#include "AssemblyU2DCSharp_ActionManager_U3CMoveActionU3Ec__173834552.h"
#include "AssemblyU2DCSharp_ActionManager_U3CRotateActionU3Ec625807471.h"
#include "AssemblyU2DCSharp_ButtonEvent4179473000.h"
#include "AssemblyU2DCSharp_Factory572770538.h"
#include "AssemblyU2DCSharp_IDManager1951742482.h"
#include "AssemblyU2DCSharp_FixedID820738927.h"
#include "AssemblyU2DCSharp_ParticleManager73248295.h"
#include "AssemblyU2DCSharp_SocketManager2142175386.h"
#include "AssemblyU2DCSharp_SoundManager2444342206.h"
#include "AssemblyU2DCSharp_StableAspect1871202195.h"
#include "AssemblyU2DCSharp_StageManager3194912495.h"
#include "AssemblyU2DCSharp_StartScaleAnimation3364620860.h"
#include "AssemblyU2DCSharp_TextLodaer3639884186.h"
#include "AssemblyU2DCSharp_TouchInput375919595.h"
#include "AssemblyU2DCSharp_TransitionAnimation4286760335.h"
#include "AssemblyU2DCSharp_TransitionAnimation_U3CSceneAnima732764625.h"
#include "AssemblyU2DCSharp_TransitionAnimation_U3CFadeinU3E2485504041.h"
#include "AssemblyU2DCSharp_TransitionAnimation_U3CFadeoutU32464685144.h"
#include "AssemblyU2DCSharp_TransitionAnimation_U3CFadeAnima1701036547.h"
#include "AssemblyU2DCSharp_TransitionAnimation_U3CScaleActi2199007308.h"
#include "AssemblyU2DCSharp_TransitionAnimation_U3CAllScaleA4063999446.h"
#include "AssemblyU2DCSharp_GodTouches_ClickLongPressDrag90319163.h"
#include "AssemblyU2DCSharp_GodTouches_ClickLongPressDrag_Ev1380888277.h"
#include "AssemblyU2DCSharp_GodTouches_Demo2005335435.h"
#include "AssemblyU2DCSharp_GodTouches_GodTouch1844913483.h"
#include "AssemblyU2DCSharp_GodTouches_GodPhase1840992135.h"
#include "AssemblyU2DCSharp_Fight67876848.h"
#include "AssemblyU2DCSharp_FightManager1489248669.h"
#include "AssemblyU2DCSharp_MessageJson794471791.h"
#include "AssemblyU2DCSharp_SummarizeJson970273193.h"
#include "AssemblyU2DCSharp_FightRound2951839614.h"
#include "AssemblyU2DCSharp_FightRound_U3CRoundTimeActionU3E3629899821.h"
#include "AssemblyU2DCSharp_FightRound_U3CShowRetireU3Ec__It4208130037.h"
#include "AssemblyU2DCSharp_FightJson649175288.h"
#include "AssemblyU2DCSharp_FightSocket1340804995.h"
#include "AssemblyU2DCSharp_MatchingManager1847897296.h"
#include "AssemblyU2DCSharp_MatchingManager_U3CMatingTimeAct3526127701.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1800[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1801[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1802[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (VertexHelper_t3377436606), -1, sizeof(VertexHelper_t3377436606_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1803[9] = 
{
	VertexHelper_t3377436606::get_offset_of_m_Positions_0(),
	VertexHelper_t3377436606::get_offset_of_m_Colors_1(),
	VertexHelper_t3377436606::get_offset_of_m_Uv0S_2(),
	VertexHelper_t3377436606::get_offset_of_m_Uv1S_3(),
	VertexHelper_t3377436606::get_offset_of_m_Normals_4(),
	VertexHelper_t3377436606::get_offset_of_m_Tangents_5(),
	VertexHelper_t3377436606::get_offset_of_m_Indices_6(),
	VertexHelper_t3377436606_StaticFields::get_offset_of_s_DefaultTangent_7(),
	VertexHelper_t3377436606_StaticFields::get_offset_of_s_DefaultNormal_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (BaseVertexEffect_t3555037586), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (BaseMeshEffect_t2306480155), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1805[1] = 
{
	BaseMeshEffect_t2306480155::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (Outline_t3745177896), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (PositionAsUV1_t4062429115), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (Shadow_t75537580), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1810[4] = 
{
	0,
	Shadow_t75537580::get_offset_of_m_EffectColor_4(),
	Shadow_t75537580::get_offset_of_m_EffectDistance_5(),
	Shadow_t75537580::get_offset_of_m_UseGraphicAlpha_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (U3CPrivateImplementationDetailsU3E_t3053238937), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1811[1] = 
{
	U3CPrivateImplementationDetailsU3E_t3053238937_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { sizeof (U24ArrayTypeU2412_t3379220351)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t3379220351_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (U3CModuleU3E_t86524797), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (U3CModuleU3E_t86524798), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (AIData_t1930434546)+ sizeof (Il2CppObject), sizeof(AIData_t1930434546_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1815[4] = 
{
	AIData_t1930434546::get_offset_of__charactornumber_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AIData_t1930434546::get_offset_of__direction_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AIData_t1930434546::get_offset_of__bullet_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AIData_t1930434546::get_offset_of__position_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { sizeof (AIDatas_t4008896193)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1816[1] = 
{
	AIDatas_t4008896193::get_offset_of__dataList_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { sizeof (AI_t2088), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { sizeof (BehaviorTree_t2111599920), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1818[3] = 
{
	BehaviorTree_t2111599920::get_offset_of__taskorder_2(),
	BehaviorTree_t2111599920::get_offset_of__cleantaskorder_3(),
	BehaviorTree_t2111599920::get_offset_of__count_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { sizeof (U3CAIPlayU3Ec__Iterator0_t226282564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1819[5] = 
{
	U3CAIPlayU3Ec__Iterator0_t226282564::get_offset_of_U3CiU3E__0_0(),
	U3CAIPlayU3Ec__Iterator0_t226282564::get_offset_of_U3CjU3E__1_1(),
	U3CAIPlayU3Ec__Iterator0_t226282564::get_offset_of_U24PC_2(),
	U3CAIPlayU3Ec__Iterator0_t226282564::get_offset_of_U24current_3(),
	U3CAIPlayU3Ec__Iterator0_t226282564::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { sizeof (BlackBoard_t328561223), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1820[6] = 
{
	BlackBoard_t328561223::get_offset_of__parent_2(),
	BlackBoard_t328561223::get_offset_of__gameobject_3(),
	BlackBoard_t328561223::get_offset_of__vector3_4(),
	BlackBoard_t328561223::get_offset_of__integer_5(),
	BlackBoard_t328561223::get_offset_of__fraction_6(),
	BlackBoard_t328561223::get_offset_of__jude_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { sizeof (BulletSarch_t300647239), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { sizeof (Condition_t1142656251), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1823 = { sizeof (IsFlag_t2198325206), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1823[2] = 
{
	IsFlag_t2198325206::get_offset_of_str_2(),
	IsFlag_t2198325206::get_offset_of_flag_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1824 = { sizeof (TaskOrder_t2170039465), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1824[6] = 
{
	TaskOrder_t2170039465::get_offset_of__taskorder_2(),
	TaskOrder_t2170039465::get_offset_of__condition_3(),
	TaskOrder_t2170039465::get_offset_of__tasks_4(),
	TaskOrder_t2170039465::get_offset_of_issequence_5(),
	TaskOrder_t2170039465::get_offset_of_iscoroutines_6(),
	TaskOrder_t2170039465::get_offset_of_bb_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1825 = { sizeof (U3CTaskStartU3Ec__Iterator1_t3806606775), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1825[6] = 
{
	U3CTaskStartU3Ec__Iterator1_t3806606775::get_offset_of_U3CcountU3E__0_0(),
	U3CTaskStartU3Ec__Iterator1_t3806606775::get_offset_of_U3CjU3E__1_1(),
	U3CTaskStartU3Ec__Iterator1_t3806606775::get_offset_of_U3CjU3E__2_2(),
	U3CTaskStartU3Ec__Iterator1_t3806606775::get_offset_of_U24PC_3(),
	U3CTaskStartU3Ec__Iterator1_t3806606775::get_offset_of_U24current_4(),
	U3CTaskStartU3Ec__Iterator1_t3806606775::get_offset_of_U3CU3Ef__this_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1826 = { sizeof (CenterMove_t3155240422), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1827 = { sizeof (Move_t2404337), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1827[2] = 
{
	Move_t2404337::get_offset_of_istart_2(),
	Move_t2404337::get_offset_of_movePar_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1828 = { sizeof (SearchMoveTask_t1050018526), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1828[5] = 
{
	0,
	0,
	0,
	0,
	SearchMoveTask_t1050018526::get_offset_of_test_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1829 = { sizeof (Task_t2599333), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1830 = { sizeof (BigballBullet_t3675719873), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1831 = { sizeof (Bullet_t2000900386), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1831[7] = 
{
	Bullet_t2000900386::get_offset_of__spd_2(),
	Bullet_t2000900386::get_offset_of__power_3(),
	Bullet_t2000900386::get_offset_of_isflyweight_4(),
	Bullet_t2000900386::get_offset_of__direction_5(),
	Bullet_t2000900386::get_offset_of_gameSpeed_6(),
	Bullet_t2000900386::get_offset_of__technique_7(),
	Bullet_t2000900386::get_offset_of_isStop_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1832 = { sizeof (BulletNumber_t605304587)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1832[6] = 
{
	BulletNumber_t605304587::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1833 = { sizeof (CannonBullet_t1475316863), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1834 = { sizeof (RiceBullet_t2445594459), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1835 = { sizeof (Rocket_t2453156596), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1836 = { sizeof (TestBullet_t3235265620), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1837 = { sizeof (DefenceManager_t3329977693), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1837[6] = 
{
	DefenceManager_t3329977693::get_offset_of__hpgauge_2(),
	DefenceManager_t3329977693::get_offset_of__hptext_3(),
	DefenceManager_t3329977693::get_offset_of__AI_4(),
	DefenceManager_t3329977693::get_offset_of_round_5(),
	DefenceManager_t3329977693::get_offset_of_ob_6(),
	DefenceManager_t3329977693::get_offset_of__isAI_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1838 = { sizeof (DemoPlay_t921155543), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1838[1] = 
{
	DemoPlay_t921155543::get_offset_of_isdemoplay_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1839 = { sizeof (Player_t2393081601), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1839[13] = 
{
	Player_t2393081601::get_offset_of_m_y_10(),
	Player_t2393081601::get_offset_of__movestate_11(),
	Player_t2393081601::get_offset_of__anim_12(),
	Player_t2393081601::get_offset_of_isanim_13(),
	Player_t2393081601::get_offset_of_isfight_14(),
	Player_t2393081601::get_offset_of_isPlayer_15(),
	Player_t2393081601::get_offset_of__time_16(),
	Player_t2393081601::get_offset_of__initcolor_17(),
	Player_t2393081601::get_offset_of__hp_18(),
	Player_t2393081601::get_offset_of__recoverylife_19(),
	Player_t2393081601::get_offset_of__hpgauge_20(),
	Player_t2393081601::get_offset_of__hptext_21(),
	Player_t2393081601::get_offset_of_calldelaymove_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1840 = { sizeof (PlayerJson_t4089020297)+ sizeof (Il2CppObject), sizeof(PlayerJson_t4089020297_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1840[3] = 
{
	PlayerJson_t4089020297::get_offset_of_num_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PlayerJson_t4089020297::get_offset_of_mj_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PlayerJson_t4089020297::get_offset_of_hj_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1841 = { sizeof (MoveJson_t4254904441)+ sizeof (Il2CppObject), sizeof(MoveJson_t4254904441_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1841[5] = 
{
	MoveJson_t4254904441::get_offset_of_position_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MoveJson_t4254904441::get_offset_of_movepos_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MoveJson_t4254904441::get_offset_of_movestate_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MoveJson_t4254904441::get_offset_of_charspd_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MoveJson_t4254904441::get_offset_of_time_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1842 = { sizeof (HitJson_t2591263483)+ sizeof (Il2CppObject), sizeof(HitJson_t2591263483_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1842[2] = 
{
	HitJson_t2591263483::get_offset_of_damage_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HitJson_t2591263483::get_offset_of_direction_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1843 = { sizeof (JsonVector3_t719439144)+ sizeof (Il2CppObject), sizeof(JsonVector3_t719439144_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1843[3] = 
{
	JsonVector3_t719439144::get_offset_of_x_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JsonVector3_t719439144::get_offset_of_y_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JsonVector3_t719439144::get_offset_of_z_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1844 = { sizeof (ShirkAI_t3726263989), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1844[11] = 
{
	ShirkAI_t3726263989::get_offset_of_m_y_10(),
	ShirkAI_t3726263989::get_offset_of__movestate_11(),
	ShirkAI_t3726263989::get_offset_of__anim_12(),
	ShirkAI_t3726263989::get_offset_of_isanim_13(),
	ShirkAI_t3726263989::get_offset_of__initcolor_14(),
	ShirkAI_t3726263989::get_offset_of__hp_15(),
	ShirkAI_t3726263989::get_offset_of__angle_16(),
	ShirkAI_t3726263989::get_offset_of__hpgauge_17(),
	ShirkAI_t3726263989::get_offset_of__hptext_18(),
	ShirkAI_t3726263989::get_offset_of_bb_19(),
	ShirkAI_t3726263989::get_offset_of_istitle_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1845 = { sizeof (Charctor_t1500651146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1845[8] = 
{
	Charctor_t1500651146::get_offset_of_m_pos_2(),
	Charctor_t1500651146::get_offset_of_ismove_3(),
	Charctor_t1500651146::get_offset_of__chardata_4(),
	Charctor_t1500651146::get_offset_of_gameSpeed_5(),
	Charctor_t1500651146::get_offset_of_isplay_6(),
	Charctor_t1500651146::get_offset_of_par_7(),
	Charctor_t1500651146::get_offset_of_endPar_8(),
	Charctor_t1500651146::get_offset_of_movePar_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1846 = { sizeof (U3CEndActionColU3Ec__Iterator2_t2737953241), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1846[3] = 
{
	U3CEndActionColU3Ec__Iterator2_t2737953241::get_offset_of_U24PC_0(),
	U3CEndActionColU3Ec__Iterator2_t2737953241::get_offset_of_U24current_1(),
	U3CEndActionColU3Ec__Iterator2_t2737953241::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1847 = { sizeof (CharData_t1499709504)+ sizeof (Il2CppObject), sizeof(CharData_t1499709504_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1847[3] = 
{
	CharData_t1499709504::get_offset_of__hp_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CharData_t1499709504::get_offset_of__spd_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CharData_t1499709504::get_offset_of__technique_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1848 = { sizeof (East_t2152477), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1849 = { sizeof (Enemy_t67100520), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1849[8] = 
{
	Enemy_t67100520::get_offset_of__aidata_10(),
	Enemy_t67100520::get_offset_of__ainum_11(),
	Enemy_t67100520::get_offset_of_f_12(),
	Enemy_t67100520::get_offset_of_isStart_13(),
	Enemy_t67100520::get_offset_of_hayasa_14(),
	Enemy_t67100520::get_offset_of__bullettechnich_15(),
	Enemy_t67100520::get_offset_of_shotPar_16(),
	Enemy_t67100520::get_offset_of_isEnd_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1850 = { sizeof (U3CEndActionColU3Ec__Iterator3_t1666219580), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1850[4] = 
{
	U3CEndActionColU3Ec__Iterator3_t1666219580::get_offset_of_U3CpU3E__0_0(),
	U3CEndActionColU3Ec__Iterator3_t1666219580::get_offset_of_U24PC_1(),
	U3CEndActionColU3Ec__Iterator3_t1666219580::get_offset_of_U24current_2(),
	U3CEndActionColU3Ec__Iterator3_t1666219580::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1851 = { sizeof (Direction_t1041377119)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1851[6] = 
{
	Direction_t1041377119::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1852 = { sizeof (EnemyManager_t1035150117), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1852[6] = 
{
	EnemyManager_t1035150117::get_offset_of_Round_2(),
	EnemyManager_t1035150117::get_offset_of_AIEndNum_3(),
	EnemyManager_t1035150117::get_offset_of__ai_4(),
	EnemyManager_t1035150117::get_offset_of_time_5(),
	EnemyManager_t1035150117::get_offset_of_isDemo_6(),
	EnemyManager_t1035150117::get_offset_of_isPlay_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1853 = { sizeof (North_t75454693), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1854 = { sizeof (South_t80075181), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1855 = { sizeof (TitleEnemy_t195907632), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1856 = { sizeof (West_t2692559), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1857 = { sizeof (SkipSpeed_t4087407080), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1857[5] = 
{
	SkipSpeed_t4087407080::get_offset_of_playerEnemy_2(),
	SkipSpeed_t4087407080::get_offset_of_defenceManager_3(),
	SkipSpeed_t4087407080::get_offset_of_sp1_4(),
	SkipSpeed_t4087407080::get_offset_of_sp2_5(),
	SkipSpeed_t4087407080::get_offset_of_speed_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1858 = { sizeof (Stop_t2587682), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1858[6] = 
{
	Stop_t2587682::get_offset_of__ReCanvas_2(),
	Stop_t2587682::get_offset_of__Retire_3(),
	Stop_t2587682::get_offset_of__playselect_4(),
	Stop_t2587682::get_offset_of_isFight_5(),
	Stop_t2587682::get_offset_of_isPlay_6(),
	Stop_t2587682::get_offset_of_isSelect_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1859 = { sizeof (U3CShowRetireU3Ec__Iterator4_t2670935930), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1859[5] = 
{
	U3CShowRetireU3Ec__Iterator4_t2670935930::get_offset_of_U3CtimeU3E__0_0(),
	U3CShowRetireU3Ec__Iterator4_t2670935930::get_offset_of_U3CpU3E__1_1(),
	U3CShowRetireU3Ec__Iterator4_t2670935930::get_offset_of_U24PC_2(),
	U3CShowRetireU3Ec__Iterator4_t2670935930::get_offset_of_U24current_3(),
	U3CShowRetireU3Ec__Iterator4_t2670935930::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1860 = { sizeof (ActionManager_t3022458999), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1860[1] = 
{
	ActionManager_t3022458999::get_offset_of__moveforever_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1861 = { sizeof (U3CForeverMoveActionU3Ec__Iterator5_t899850968), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1861[12] = 
{
	U3CForeverMoveActionU3Ec__Iterator5_t899850968::get_offset_of__ob_0(),
	U3CForeverMoveActionU3Ec__Iterator5_t899850968::get_offset_of_U3CdefaultposU3E__0_1(),
	U3CForeverMoveActionU3Ec__Iterator5_t899850968::get_offset_of_destination_2(),
	U3CForeverMoveActionU3Ec__Iterator5_t899850968::get_offset_of_deltatime_3(),
	U3CForeverMoveActionU3Ec__Iterator5_t899850968::get_offset_of_waittime_4(),
	U3CForeverMoveActionU3Ec__Iterator5_t899850968::get_offset_of_U24PC_5(),
	U3CForeverMoveActionU3Ec__Iterator5_t899850968::get_offset_of_U24current_6(),
	U3CForeverMoveActionU3Ec__Iterator5_t899850968::get_offset_of_U3CU24U3E_ob_7(),
	U3CForeverMoveActionU3Ec__Iterator5_t899850968::get_offset_of_U3CU24U3Edestination_8(),
	U3CForeverMoveActionU3Ec__Iterator5_t899850968::get_offset_of_U3CU24U3Edeltatime_9(),
	U3CForeverMoveActionU3Ec__Iterator5_t899850968::get_offset_of_U3CU24U3Ewaittime_10(),
	U3CForeverMoveActionU3Ec__Iterator5_t899850968::get_offset_of_U3CU3Ef__this_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1862 = { sizeof (U3CMoveActionU3Ec__Iterator6_t173834552), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1862[13] = 
{
	U3CMoveActionU3Ec__Iterator6_t173834552::get_offset_of_destination_0(),
	U3CMoveActionU3Ec__Iterator6_t173834552::get_offset_of__ob_1(),
	U3CMoveActionU3Ec__Iterator6_t173834552::get_offset_of_U3CpU3E__0_2(),
	U3CMoveActionU3Ec__Iterator6_t173834552::get_offset_of_deltatime_3(),
	U3CMoveActionU3Ec__Iterator6_t173834552::get_offset_of_U3CspdxU3E__1_4(),
	U3CMoveActionU3Ec__Iterator6_t173834552::get_offset_of_U3CspdyU3E__2_5(),
	U3CMoveActionU3Ec__Iterator6_t173834552::get_offset_of_U3CspdzU3E__3_6(),
	U3CMoveActionU3Ec__Iterator6_t173834552::get_offset_of_U3CmpU3E__4_7(),
	U3CMoveActionU3Ec__Iterator6_t173834552::get_offset_of_U24PC_8(),
	U3CMoveActionU3Ec__Iterator6_t173834552::get_offset_of_U24current_9(),
	U3CMoveActionU3Ec__Iterator6_t173834552::get_offset_of_U3CU24U3Edestination_10(),
	U3CMoveActionU3Ec__Iterator6_t173834552::get_offset_of_U3CU24U3E_ob_11(),
	U3CMoveActionU3Ec__Iterator6_t173834552::get_offset_of_U3CU24U3Edeltatime_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1863 = { sizeof (U3CRotateActionU3Ec__Iterator7_t625807471), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1863[13] = 
{
	U3CRotateActionU3Ec__Iterator7_t625807471::get_offset_of_rotate_0(),
	U3CRotateActionU3Ec__Iterator7_t625807471::get_offset_of__ob_1(),
	U3CRotateActionU3Ec__Iterator7_t625807471::get_offset_of_U3CpU3E__0_2(),
	U3CRotateActionU3Ec__Iterator7_t625807471::get_offset_of_deltatime_3(),
	U3CRotateActionU3Ec__Iterator7_t625807471::get_offset_of_U3CspdxU3E__1_4(),
	U3CRotateActionU3Ec__Iterator7_t625807471::get_offset_of_U3CspdyU3E__2_5(),
	U3CRotateActionU3Ec__Iterator7_t625807471::get_offset_of_U3CspdzU3E__3_6(),
	U3CRotateActionU3Ec__Iterator7_t625807471::get_offset_of_U3CmpU3E__4_7(),
	U3CRotateActionU3Ec__Iterator7_t625807471::get_offset_of_U24PC_8(),
	U3CRotateActionU3Ec__Iterator7_t625807471::get_offset_of_U24current_9(),
	U3CRotateActionU3Ec__Iterator7_t625807471::get_offset_of_U3CU24U3Erotate_10(),
	U3CRotateActionU3Ec__Iterator7_t625807471::get_offset_of_U3CU24U3E_ob_11(),
	U3CRotateActionU3Ec__Iterator7_t625807471::get_offset_of_U3CU24U3Edeltatime_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1864 = { sizeof (ButtonEvent_t4179473000), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1864[2] = 
{
	ButtonEvent_t4179473000::get_offset_of__object_2(),
	ButtonEvent_t4179473000::get_offset_of_num_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1865 = { sizeof (Factory_t572770538), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1865[6] = 
{
	Factory_t572770538::get_offset_of__bullets_3(),
	Factory_t572770538::get_offset_of__charactors_4(),
	Factory_t572770538::get_offset_of__charactorspritedatas_5(),
	Factory_t572770538::get_offset_of__bulletspritedatas_6(),
	Factory_t572770538::get_offset_of__bullet_7(),
	Factory_t572770538::get_offset_of_isbulletstop_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1866 = { sizeof (IDManager_t1951742482), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1866[1] = 
{
	IDManager_t1951742482::get_offset_of_fid_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1867 = { sizeof (FixedID_t820738927)+ sizeof (Il2CppObject), sizeof(FixedID_t820738927_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1867[2] = 
{
	FixedID_t820738927::get_offset_of_id_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FixedID_t820738927::get_offset_of_name_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1868 = { sizeof (ParticleManager_t73248295), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1868[9] = 
{
	ParticleManager_t73248295::get_offset_of_p_sortiePar_3(),
	ParticleManager_t73248295::get_offset_of_p_admissionPar_4(),
	ParticleManager_t73248295::get_offset_of_p_lossPar_5(),
	ParticleManager_t73248295::get_offset_of_p_winPar_6(),
	ParticleManager_t73248295::get_offset_of_p_changePar_7(),
	ParticleManager_t73248295::get_offset_of_p_damegePar_8(),
	ParticleManager_t73248295::get_offset_of_p_bloodPar_9(),
	ParticleManager_t73248295::get_offset_of_p_shotPar_10(),
	ParticleManager_t73248295::get_offset_of_p_explosionsPar_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1869 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1869[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1870 = { sizeof (SocketManager_t2142175386), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1870[1] = 
{
	SocketManager_t2142175386::get_offset_of_socket_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1871 = { sizeof (SoundManager_t2444342206), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1871[11] = 
{
	SoundManager_t2444342206::get_offset_of_PlaySound_3(),
	SoundManager_t2444342206::get_offset_of_GameSound_4(),
	SoundManager_t2444342206::get_offset_of_WinSound_5(),
	SoundManager_t2444342206::get_offset_of_LossSound_6(),
	SoundManager_t2444342206::get_offset_of_SE1_7(),
	SoundManager_t2444342206::get_offset_of_SE2_8(),
	SoundManager_t2444342206::get_offset_of_LossSE_9(),
	SoundManager_t2444342206::get_offset_of_WinSE_10(),
	SoundManager_t2444342206::get_offset_of_BomSE_11(),
	SoundManager_t2444342206::get_offset_of_FlySE_12(),
	SoundManager_t2444342206::get_offset_of__audios_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1872 = { sizeof (StableAspect_t1871202195), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1872[4] = 
{
	StableAspect_t1871202195::get_offset_of_cam_2(),
	StableAspect_t1871202195::get_offset_of_width_3(),
	StableAspect_t1871202195::get_offset_of_height_4(),
	StableAspect_t1871202195::get_offset_of_pixelPerUnit_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1873 = { sizeof (StageManager_t3194912495), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1873[1] = 
{
	StageManager_t3194912495::get_offset_of_is_play_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1874 = { sizeof (StartScaleAnimation_t3364620860), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1875 = { sizeof (TextLodaer_t3639884186), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1875[1] = 
{
	TextLodaer_t3639884186::get_offset_of_m_filepas_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1876 = { sizeof (TouchInput_t375919595), -1, sizeof(TouchInput_t375919595_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1876[2] = 
{
	TouchInput_t375919595_StaticFields::get_offset_of__instance_2(),
	TouchInput_t375919595::get_offset_of_active_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1877 = { sizeof (TransitionAnimation_t4286760335), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1877[9] = 
{
	TransitionAnimation_t4286760335::get_offset_of_T_tex2D_3(),
	TransitionAnimation_t4286760335::get_offset_of_m_Alpha_4(),
	TransitionAnimation_t4286760335::get_offset_of_m_IsAnimation_5(),
	TransitionAnimation_t4286760335::get_offset_of_color_6(),
	TransitionAnimation_t4286760335::get_offset_of__list_7(),
	TransitionAnimation_t4286760335::get_offset_of__ilist_8(),
	TransitionAnimation_t4286760335::get_offset_of__allscaleanimarion_9(),
	TransitionAnimation_t4286760335::get_offset_of__scale_10(),
	TransitionAnimation_t4286760335::get_offset_of__moveaction_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1878 = { sizeof (U3CSceneAnimationU3Ec__Iterator8_t732764625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1878[8] = 
{
	U3CSceneAnimationU3Ec__Iterator8_t732764625::get_offset_of_U3CtimeU3E__0_0(),
	U3CSceneAnimationU3Ec__Iterator8_t732764625::get_offset_of_interval_1(),
	U3CSceneAnimationU3Ec__Iterator8_t732764625::get_offset_of_scene_2(),
	U3CSceneAnimationU3Ec__Iterator8_t732764625::get_offset_of_U24PC_3(),
	U3CSceneAnimationU3Ec__Iterator8_t732764625::get_offset_of_U24current_4(),
	U3CSceneAnimationU3Ec__Iterator8_t732764625::get_offset_of_U3CU24U3Einterval_5(),
	U3CSceneAnimationU3Ec__Iterator8_t732764625::get_offset_of_U3CU24U3Escene_6(),
	U3CSceneAnimationU3Ec__Iterator8_t732764625::get_offset_of_U3CU3Ef__this_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1879 = { sizeof (U3CFadeinU3Ec__Iterator9_t2485504041), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1879[6] = 
{
	U3CFadeinU3Ec__Iterator9_t2485504041::get_offset_of_U3CtimeU3E__0_0(),
	U3CFadeinU3Ec__Iterator9_t2485504041::get_offset_of_interval_1(),
	U3CFadeinU3Ec__Iterator9_t2485504041::get_offset_of_U24PC_2(),
	U3CFadeinU3Ec__Iterator9_t2485504041::get_offset_of_U24current_3(),
	U3CFadeinU3Ec__Iterator9_t2485504041::get_offset_of_U3CU24U3Einterval_4(),
	U3CFadeinU3Ec__Iterator9_t2485504041::get_offset_of_U3CU3Ef__this_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1880 = { sizeof (U3CFadeoutU3Ec__IteratorA_t2464685144), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1880[6] = 
{
	U3CFadeoutU3Ec__IteratorA_t2464685144::get_offset_of_U3CtimeU3E__0_0(),
	U3CFadeoutU3Ec__IteratorA_t2464685144::get_offset_of_interval_1(),
	U3CFadeoutU3Ec__IteratorA_t2464685144::get_offset_of_U24PC_2(),
	U3CFadeoutU3Ec__IteratorA_t2464685144::get_offset_of_U24current_3(),
	U3CFadeoutU3Ec__IteratorA_t2464685144::get_offset_of_U3CU24U3Einterval_4(),
	U3CFadeoutU3Ec__IteratorA_t2464685144::get_offset_of_U3CU3Ef__this_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1881 = { sizeof (U3CFadeAnimationU3Ec__IteratorB_t1701036547), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1881[8] = 
{
	U3CFadeAnimationU3Ec__IteratorB_t1701036547::get_offset_of_U3CtimeU3E__0_0(),
	U3CFadeAnimationU3Ec__IteratorB_t1701036547::get_offset_of_interval_1(),
	U3CFadeAnimationU3Ec__IteratorB_t1701036547::get_offset_of_gb_2(),
	U3CFadeAnimationU3Ec__IteratorB_t1701036547::get_offset_of_U24PC_3(),
	U3CFadeAnimationU3Ec__IteratorB_t1701036547::get_offset_of_U24current_4(),
	U3CFadeAnimationU3Ec__IteratorB_t1701036547::get_offset_of_U3CU24U3Einterval_5(),
	U3CFadeAnimationU3Ec__IteratorB_t1701036547::get_offset_of_U3CU24U3Egb_6(),
	U3CFadeAnimationU3Ec__IteratorB_t1701036547::get_offset_of_U3CU3Ef__this_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1882 = { sizeof (U3CScaleActionU3Ec__IteratorC_t2199007308), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1882[15] = 
{
	U3CScaleActionU3Ec__IteratorC_t2199007308::get_offset_of_defoult_0(),
	U3CScaleActionU3Ec__IteratorC_t2199007308::get_offset_of_U3CdefoultscaleU3E__0_1(),
	U3CScaleActionU3Ec__IteratorC_t2199007308::get_offset_of_nextscale_2(),
	U3CScaleActionU3Ec__IteratorC_t2199007308::get_offset_of_U3CsU3E__1_3(),
	U3CScaleActionU3Ec__IteratorC_t2199007308::get_offset_of_speed_4(),
	U3CScaleActionU3Ec__IteratorC_t2199007308::get_offset_of_U3CnumU3E__2_5(),
	U3CScaleActionU3Ec__IteratorC_t2199007308::get_offset_of_U3CssU3E__3_6(),
	U3CScaleActionU3Ec__IteratorC_t2199007308::get_offset_of_U3CisupU3E__4_7(),
	U3CScaleActionU3Ec__IteratorC_t2199007308::get_offset_of_g_8(),
	U3CScaleActionU3Ec__IteratorC_t2199007308::get_offset_of_U24PC_9(),
	U3CScaleActionU3Ec__IteratorC_t2199007308::get_offset_of_U24current_10(),
	U3CScaleActionU3Ec__IteratorC_t2199007308::get_offset_of_U3CU24U3Edefoult_11(),
	U3CScaleActionU3Ec__IteratorC_t2199007308::get_offset_of_U3CU24U3Enextscale_12(),
	U3CScaleActionU3Ec__IteratorC_t2199007308::get_offset_of_U3CU24U3Espeed_13(),
	U3CScaleActionU3Ec__IteratorC_t2199007308::get_offset_of_U3CU24U3Eg_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1883 = { sizeof (U3CAllScaleActionU3Ec__IteratorD_t4063999446), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1883[15] = 
{
	U3CAllScaleActionU3Ec__IteratorD_t4063999446::get_offset_of_defoult_0(),
	U3CAllScaleActionU3Ec__IteratorD_t4063999446::get_offset_of_U3CdefoultscaleU3E__0_1(),
	U3CAllScaleActionU3Ec__IteratorD_t4063999446::get_offset_of_nextscale_2(),
	U3CAllScaleActionU3Ec__IteratorD_t4063999446::get_offset_of_U3CsU3E__1_3(),
	U3CAllScaleActionU3Ec__IteratorD_t4063999446::get_offset_of_speed_4(),
	U3CAllScaleActionU3Ec__IteratorD_t4063999446::get_offset_of_U3CnumU3E__2_5(),
	U3CAllScaleActionU3Ec__IteratorD_t4063999446::get_offset_of_U3CisupU3E__3_6(),
	U3CAllScaleActionU3Ec__IteratorD_t4063999446::get_offset_of_U3CcountU3E__4_7(),
	U3CAllScaleActionU3Ec__IteratorD_t4063999446::get_offset_of_U3CiU3E__5_8(),
	U3CAllScaleActionU3Ec__IteratorD_t4063999446::get_offset_of_U24PC_9(),
	U3CAllScaleActionU3Ec__IteratorD_t4063999446::get_offset_of_U24current_10(),
	U3CAllScaleActionU3Ec__IteratorD_t4063999446::get_offset_of_U3CU24U3Edefoult_11(),
	U3CAllScaleActionU3Ec__IteratorD_t4063999446::get_offset_of_U3CU24U3Enextscale_12(),
	U3CAllScaleActionU3Ec__IteratorD_t4063999446::get_offset_of_U3CU24U3Espeed_13(),
	U3CAllScaleActionU3Ec__IteratorD_t4063999446::get_offset_of_U3CU3Ef__this_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1884 = { sizeof (ClickLongPressDrag_t90319163), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1884[7] = 
{
	ClickLongPressDrag_t90319163::get_offset_of_CheckDistance_2(),
	ClickLongPressDrag_t90319163::get_offset_of_CheckTime_3(),
	ClickLongPressDrag_t90319163::get_offset_of_Text_4(),
	ClickLongPressDrag_t90319163::get_offset_of_type_5(),
	ClickLongPressDrag_t90319163::get_offset_of_isRunning_6(),
	ClickLongPressDrag_t90319163::get_offset_of_startPos_7(),
	ClickLongPressDrag_t90319163::get_offset_of_startTime_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1885 = { sizeof (EventType_t1380888277)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1885[6] = 
{
	EventType_t1380888277::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1886 = { sizeof (Demo_t2005335435), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1886[3] = 
{
	Demo_t2005335435::get_offset_of_Move_2(),
	Demo_t2005335435::get_offset_of_ClickLongPressDrag_3(),
	Demo_t2005335435::get_offset_of_startPos_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1887 = { sizeof (GodTouch_t1844913483), -1, sizeof(GodTouch_t1844913483_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1887[4] = 
{
	GodTouch_t1844913483_StaticFields::get_offset_of_IsAndroid_0(),
	GodTouch_t1844913483_StaticFields::get_offset_of_IsIOS_1(),
	GodTouch_t1844913483_StaticFields::get_offset_of_IsEditor_2(),
	GodTouch_t1844913483_StaticFields::get_offset_of_prebPosition_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1888 = { sizeof (GodPhase_t1840992135)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1888[7] = 
{
	GodPhase_t1840992135::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1889 = { sizeof (Fight_t67876848), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1889[3] = 
{
	Fight_t67876848::get_offset_of__roomid_2(),
	Fight_t67876848::get_offset_of__enemyfid_3(),
	Fight_t67876848::get_offset_of__attack_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1890 = { sizeof (FightManager_t1489248669), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1890[1] = 
{
	FightManager_t1489248669::get_offset_of__fm_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1891 = { sizeof (MessageJson_t794471791)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1891[2] = 
{
	MessageJson_t794471791::get_offset_of_id_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MessageJson_t794471791::get_offset_of_value_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1892 = { sizeof (SummarizeJson_t970273193)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1892[3] = 
{
	SummarizeJson_t970273193::get_offset_of_num_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SummarizeJson_t970273193::get_offset_of_fjson_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SummarizeJson_t970273193::get_offset_of_pjson_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1893 = { sizeof (FightRound_t2951839614), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1893[16] = 
{
	FightRound_t2951839614::get_offset_of__stopbutton_11(),
	FightRound_t2951839614::get_offset_of__retiretext_12(),
	FightRound_t2951839614::get_offset_of__text_13(),
	FightRound_t2951839614::get_offset_of_decision_14(),
	FightRound_t2951839614::get_offset_of_GameCount_15(),
	FightRound_t2951839614::get_offset_of_isdisconnection_16(),
	FightRound_t2951839614::get_offset_of_isretire_17(),
	FightRound_t2951839614::get_offset_of_isNextGame_18(),
	FightRound_t2951839614::get_offset_of__receptionAI_19(),
	FightRound_t2951839614::get_offset_of__receptionscore_20(),
	FightRound_t2951839614::get_offset_of__sendAI_21(),
	FightRound_t2951839614::get_offset_of__sendscore_22(),
	FightRound_t2951839614::get_offset_of_isstart_23(),
	FightRound_t2951839614::get_offset_of__enemyAi_24(),
	FightRound_t2951839614::get_offset_of__charnum_25(),
	FightRound_t2951839614::get_offset_of__fm_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1894 = { sizeof (U3CRoundTimeActionU3Ec__IteratorF_t3629899821), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1894[5] = 
{
	U3CRoundTimeActionU3Ec__IteratorF_t3629899821::get_offset_of_U3CtimeU3E__0_0(),
	U3CRoundTimeActionU3Ec__IteratorF_t3629899821::get_offset_of_U3CtU3E__1_1(),
	U3CRoundTimeActionU3Ec__IteratorF_t3629899821::get_offset_of_U24PC_2(),
	U3CRoundTimeActionU3Ec__IteratorF_t3629899821::get_offset_of_U24current_3(),
	U3CRoundTimeActionU3Ec__IteratorF_t3629899821::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1895 = { sizeof (U3CShowRetireU3Ec__Iterator10_t4208130037), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1895[6] = 
{
	U3CShowRetireU3Ec__Iterator10_t4208130037::get_offset_of__go_0(),
	U3CShowRetireU3Ec__Iterator10_t4208130037::get_offset_of_U3CtimeU3E__0_1(),
	U3CShowRetireU3Ec__Iterator10_t4208130037::get_offset_of_U3CpU3E__1_2(),
	U3CShowRetireU3Ec__Iterator10_t4208130037::get_offset_of_U24PC_3(),
	U3CShowRetireU3Ec__Iterator10_t4208130037::get_offset_of_U24current_4(),
	U3CShowRetireU3Ec__Iterator10_t4208130037::get_offset_of_U3CU24U3E_go_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1896 = { sizeof (FightJson_t649175288)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1896[3] = 
{
	FightJson_t649175288::get_offset_of_num_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FightJson_t649175288::get_offset_of_hp_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FightJson_t649175288::get_offset_of_aidatas_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1897 = { sizeof (FightSocket_t1340804995), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1897[2] = 
{
	FightSocket_t1340804995::get_offset_of_socket_3(),
	FightSocket_t1340804995::get_offset_of_fid_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1898 = { sizeof (MatchingManager_t1847897296), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1898[6] = 
{
	MatchingManager_t1847897296::get_offset_of_createcount_3(),
	MatchingManager_t1847897296::get_offset_of_deletecount_4(),
	MatchingManager_t1847897296::get_offset_of_time_5(),
	MatchingManager_t1847897296::get_offset_of__button_6(),
	MatchingManager_t1847897296::get_offset_of_par_7(),
	MatchingManager_t1847897296::get_offset_of__text_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1899 = { sizeof (U3CMatingTimeActionU3Ec__Iterator11_t3526127701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1899[5] = 
{
	U3CMatingTimeActionU3Ec__Iterator11_t3526127701::get_offset_of_U3CtimeU3E__0_0(),
	U3CMatingTimeActionU3Ec__Iterator11_t3526127701::get_offset_of_U3CtU3E__1_1(),
	U3CMatingTimeActionU3Ec__Iterator11_t3526127701::get_offset_of_U24PC_2(),
	U3CMatingTimeActionU3Ec__Iterator11_t3526127701::get_offset_of_U24current_3(),
	U3CMatingTimeActionU3Ec__Iterator11_t3526127701::get_offset_of_U3CU3Ef__this_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
