﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<AIData>
struct DefaultComparer_t378211213;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AIData1930434546.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<AIData>::.ctor()
extern "C"  void DefaultComparer__ctor_m289529374_gshared (DefaultComparer_t378211213 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m289529374(__this, method) ((  void (*) (DefaultComparer_t378211213 *, const MethodInfo*))DefaultComparer__ctor_m289529374_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<AIData>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m347375637_gshared (DefaultComparer_t378211213 * __this, AIData_t1930434546  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m347375637(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t378211213 *, AIData_t1930434546 , const MethodInfo*))DefaultComparer_GetHashCode_m347375637_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<AIData>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2246510003_gshared (DefaultComparer_t378211213 * __this, AIData_t1930434546  ___x0, AIData_t1930434546  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m2246510003(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t378211213 *, AIData_t1930434546 , AIData_t1930434546 , const MethodInfo*))DefaultComparer_Equals_m2246510003_gshared)(__this, ___x0, ___y1, method)
