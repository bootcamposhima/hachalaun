﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va746493984MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m2748649878(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t50168888 *, Dictionary_2_t1349563175 *, const MethodInfo*))ValueCollection__ctor_m4177258586_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1343594460(__this, ___item0, method) ((  void (*) (ValueCollection_t50168888 *, HttpListenerContext_t3744659101 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1528225944_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2284706469(__this, method) ((  void (*) (ValueCollection_t50168888 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2827278689_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3308202570(__this, ___item0, method) ((  bool (*) (ValueCollection_t50168888 *, HttpListenerContext_t3744659101 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2015709838_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1434602863(__this, ___item0, method) ((  bool (*) (ValueCollection_t50168888 *, HttpListenerContext_t3744659101 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3578506931_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3962996467(__this, method) ((  Il2CppObject* (*) (ValueCollection_t50168888 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4041606511_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m4156764521(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t50168888 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1709700389_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2108906020(__this, method) ((  Il2CppObject * (*) (ValueCollection_t50168888 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2843387488_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1583378429(__this, method) ((  bool (*) (ValueCollection_t50168888 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m290885697_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1039731037(__this, method) ((  bool (*) (ValueCollection_t50168888 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1930063777_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2696384841(__this, method) ((  Il2CppObject * (*) (ValueCollection_t50168888 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1874108365_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m1870372957(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t50168888 *, HttpListenerContextU5BU5D_t838488656*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1735386657_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::GetEnumerator()
#define ValueCollection_GetEnumerator_m2956811968(__this, method) ((  Enumerator_t3576363879  (*) (ValueCollection_t50168888 *, const MethodInfo*))ValueCollection_GetEnumerator_m1204216004_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::get_Count()
#define ValueCollection_get_Count_m1238796579(__this, method) ((  int32_t (*) (ValueCollection_t50168888 *, const MethodInfo*))ValueCollection_get_Count_m2709231847_gshared)(__this, method)
