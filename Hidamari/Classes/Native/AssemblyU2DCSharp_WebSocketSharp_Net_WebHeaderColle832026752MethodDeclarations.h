﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.Net.WebHeaderCollection/<ToStringMultiValue>c__AnonStorey25
struct U3CToStringMultiValueU3Ec__AnonStorey25_t832026752;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocketSharp.Net.WebHeaderCollection/<ToStringMultiValue>c__AnonStorey25::.ctor()
extern "C"  void U3CToStringMultiValueU3Ec__AnonStorey25__ctor_m3951996955 (U3CToStringMultiValueU3Ec__AnonStorey25_t832026752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.WebHeaderCollection/<ToStringMultiValue>c__AnonStorey25::<>m__9(System.Int32)
extern "C"  void U3CToStringMultiValueU3Ec__AnonStorey25_U3CU3Em__9_m1077717576 (U3CToStringMultiValueU3Ec__AnonStorey25_t832026752 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
