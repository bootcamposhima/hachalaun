﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AIChoiceAction
struct AIChoiceAction_t410758015;

#include "codegen/il2cpp-codegen.h"

// System.Void AIChoiceAction::.ctor()
extern "C"  void AIChoiceAction__ctor_m3875119100 (AIChoiceAction_t410758015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIChoiceAction::Awake()
extern "C"  void AIChoiceAction_Awake_m4112724319 (AIChoiceAction_t410758015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIChoiceAction::Start()
extern "C"  void AIChoiceAction_Start_m2822256892 (AIChoiceAction_t410758015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIChoiceAction::Update()
extern "C"  void AIChoiceAction_Update_m1596469905 (AIChoiceAction_t410758015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIChoiceAction::ActionChoice(System.Int32)
extern "C"  void AIChoiceAction_ActionChoice_m1339300688 (AIChoiceAction_t410758015 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIChoiceAction::ActionStart()
extern "C"  void AIChoiceAction_ActionStart_m934466342 (AIChoiceAction_t410758015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIChoiceAction::ActionStop()
extern "C"  void AIChoiceAction_ActionStop_m861840864 (AIChoiceAction_t410758015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIChoiceAction::Defoult(System.Int32)
extern "C"  void AIChoiceAction_Defoult_m3816902494 (AIChoiceAction_t410758015 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIChoiceAction::Defoultpos()
extern "C"  void AIChoiceAction_Defoultpos_m3479541321 (AIChoiceAction_t410758015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
