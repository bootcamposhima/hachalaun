﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2343149357MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.Byte[]>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m4062039464(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2433093455 *, Dictionary_2_t1115770063 *, const MethodInfo*))Enumerator__ctor_m3563270077_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.Byte[]>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3093201091(__this, method) ((  Il2CppObject * (*) (Enumerator_t2433093455 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1341511502_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.Byte[]>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1695648269(__this, method) ((  void (*) (Enumerator_t2433093455 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2086888664_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.Byte[]>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m9788548(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t2433093455 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2409560079_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.Byte[]>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2959925087(__this, method) ((  Il2CppObject * (*) (Enumerator_t2433093455 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m195944874_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.Byte[]>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2268095025(__this, method) ((  Il2CppObject * (*) (Enumerator_t2433093455 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m372899260_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.Byte[]>::MoveNext()
#define Enumerator_MoveNext_m3205001277(__this, method) ((  bool (*) (Enumerator_t2433093455 *, const MethodInfo*))Enumerator_MoveNext_m2059000200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.Byte[]>::get_Current()
#define Enumerator_get_Current_m2739778655(__this, method) ((  KeyValuePair_2_t1014550769  (*) (Enumerator_t2433093455 *, const MethodInfo*))Enumerator_get_Current_m2916729652_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.Byte[]>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2619927046(__this, method) ((  uint8_t (*) (Enumerator_t2433093455 *, const MethodInfo*))Enumerator_get_CurrentKey_m4242206481_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.Byte[]>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2381540294(__this, method) ((  ByteU5BU5D_t4260760469* (*) (Enumerator_t2433093455 *, const MethodInfo*))Enumerator_get_CurrentValue_m2318948881_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.Byte[]>::Reset()
#define Enumerator_Reset_m4156845050(__this, method) ((  void (*) (Enumerator_t2433093455 *, const MethodInfo*))Enumerator_Reset_m619019919_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.Byte[]>::VerifyState()
#define Enumerator_VerifyState_m712071299(__this, method) ((  void (*) (Enumerator_t2433093455 *, const MethodInfo*))Enumerator_VerifyState_m889022296_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.Byte[]>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m4145590059(__this, method) ((  void (*) (Enumerator_t2433093455 *, const MethodInfo*))Enumerator_VerifyCurrent_m2396806336_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.Byte[]>::Dispose()
#define Enumerator_Dispose_m2931936842(__this, method) ((  void (*) (Enumerator_t2433093455 *, const MethodInfo*))Enumerator_Dispose_m401117087_gshared)(__this, method)
