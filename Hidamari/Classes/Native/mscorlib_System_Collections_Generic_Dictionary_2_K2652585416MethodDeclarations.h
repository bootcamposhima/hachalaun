﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.Object>
struct KeyCollection_t2652585416;
// System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>
struct Dictionary_2_t1025825965;
// System.Collections.Generic.IEnumerator`1<WebSocketSharp.CompressionMethod>
struct IEnumerator_1_t4138461830;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// WebSocketSharp.CompressionMethod[]
struct CompressionMethodU5BU5D_t88594176;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_WebSocketSharp_CompressionMethod2226596781.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1640762019.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m293092086_gshared (KeyCollection_t2652585416 * __this, Dictionary_2_t1025825965 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m293092086(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t2652585416 *, Dictionary_2_t1025825965 *, const MethodInfo*))KeyCollection__ctor_m293092086_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1919261856_gshared (KeyCollection_t2652585416 * __this, uint8_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1919261856(__this, ___item0, method) ((  void (*) (KeyCollection_t2652585416 *, uint8_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1919261856_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1923770903_gshared (KeyCollection_t2652585416 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1923770903(__this, method) ((  void (*) (KeyCollection_t2652585416 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1923770903_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2539460654_gshared (KeyCollection_t2652585416 * __this, uint8_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2539460654(__this, ___item0, method) ((  bool (*) (KeyCollection_t2652585416 *, uint8_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2539460654_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3033382163_gshared (KeyCollection_t2652585416 * __this, uint8_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3033382163(__this, ___item0, method) ((  bool (*) (KeyCollection_t2652585416 *, uint8_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3033382163_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1243973865_gshared (KeyCollection_t2652585416 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1243973865(__this, method) ((  Il2CppObject* (*) (KeyCollection_t2652585416 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1243973865_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m847438857_gshared (KeyCollection_t2652585416 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m847438857(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2652585416 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m847438857_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1790171608_gshared (KeyCollection_t2652585416 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1790171608(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2652585416 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1790171608_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3761323023_gshared (KeyCollection_t2652585416 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3761323023(__this, method) ((  bool (*) (KeyCollection_t2652585416 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3761323023_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m89996161_gshared (KeyCollection_t2652585416 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m89996161(__this, method) ((  bool (*) (KeyCollection_t2652585416 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m89996161_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m1701827571_gshared (KeyCollection_t2652585416 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m1701827571(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2652585416 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1701827571_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m2229205419_gshared (KeyCollection_t2652585416 * __this, CompressionMethodU5BU5D_t88594176* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m2229205419(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2652585416 *, CompressionMethodU5BU5D_t88594176*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2229205419_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.Object>::GetEnumerator()
extern "C"  Enumerator_t1640762019  KeyCollection_GetEnumerator_m1444326392_gshared (KeyCollection_t2652585416 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m1444326392(__this, method) ((  Enumerator_t1640762019  (*) (KeyCollection_t2652585416 *, const MethodInfo*))KeyCollection_GetEnumerator_m1444326392_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m3976517947_gshared (KeyCollection_t2652585416 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m3976517947(__this, method) ((  int32_t (*) (KeyCollection_t2652585416 *, const MethodInfo*))KeyCollection_get_Count_m3976517947_gshared)(__this, method)
