﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Func`3<WebSocketSharp.Opcode,System.Object,System.Boolean>
struct Func_3_t1235414321;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Opcode3782140426.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void System.Func`3<WebSocketSharp.Opcode,System.Object,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m23952469_gshared (Func_3_t1235414321 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Func_3__ctor_m23952469(__this, ___object0, ___method1, method) ((  void (*) (Func_3_t1235414321 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_3__ctor_m23952469_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`3<WebSocketSharp.Opcode,System.Object,System.Boolean>::Invoke(T1,T2)
extern "C"  bool Func_3_Invoke_m2075810392_gshared (Func_3_t1235414321 * __this, uint8_t ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
#define Func_3_Invoke_m2075810392(__this, ___arg10, ___arg21, method) ((  bool (*) (Func_3_t1235414321 *, uint8_t, Il2CppObject *, const MethodInfo*))Func_3_Invoke_m2075810392_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Func`3<WebSocketSharp.Opcode,System.Object,System.Boolean>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_3_BeginInvoke_m3367373661_gshared (Func_3_t1235414321 * __this, uint8_t ___arg10, Il2CppObject * ___arg21, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Func_3_BeginInvoke_m3367373661(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Func_3_t1235414321 *, uint8_t, Il2CppObject *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Func_3_BeginInvoke_m3367373661_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// TResult System.Func`3<WebSocketSharp.Opcode,System.Object,System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  bool Func_3_EndInvoke_m1137194883_gshared (Func_3_t1235414321 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Func_3_EndInvoke_m1137194883(__this, ___result0, method) ((  bool (*) (Func_3_t1235414321 *, Il2CppObject *, const MethodInfo*))Func_3_EndInvoke_m1137194883_gshared)(__this, ___result0, method)
