﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FightRound
struct FightRound_t2951839614;
// SocketIO.SocketIOEvent
struct SocketIOEvent_t4011854063;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SocketIO_SocketIOEvent4011854063.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void FightRound::.ctor()
extern "C"  void FightRound__ctor_m4131301085 (FightRound_t2951839614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightRound::Start()
extern "C"  void FightRound_Start_m3078438877 (FightRound_t2951839614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightRound::Update()
extern "C"  void FightRound_Update_m948176848 (FightRound_t2951839614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightRound::Awake()
extern "C"  void FightRound_Awake_m73939008 (FightRound_t2951839614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightRound::init()
extern "C"  void FightRound_init_m2247863223 (FightRound_t2951839614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightRound::Fightinit()
extern "C"  void FightRound_Fightinit_m65190939 (FightRound_t2951839614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightRound::ChoiceDone()
extern "C"  void FightRound_ChoiceDone_m36004426 (FightRound_t2951839614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightRound::ChoiceStart()
extern "C"  void FightRound_ChoiceStart_m1675390844 (FightRound_t2951839614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightRound::DissconnectMessage(SocketIO.SocketIOEvent)
extern "C"  void FightRound_DissconnectMessage_m4115657981 (FightRound_t2951839614 * __this, SocketIOEvent_t4011854063 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightRound::GameStart()
extern "C"  void FightRound_GameStart_m2027391627 (FightRound_t2951839614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightRound::RetireMassage(SocketIO.SocketIOEvent)
extern "C"  void FightRound_RetireMassage_m1971990015 (FightRound_t2951839614 * __this, SocketIOEvent_t4011854063 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightRound::Message(SocketIO.SocketIOEvent)
extern "C"  void FightRound_Message_m348347286 (FightRound_t2951839614 * __this, SocketIOEvent_t4011854063 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightRound::NextGame()
extern "C"  void FightRound_NextGame_m3631886348 (FightRound_t2951839614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightRound::NextGameStart()
extern "C"  void FightRound_NextGameStart_m3373388952 (FightRound_t2951839614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightRound::RoundStart()
extern "C"  void FightRound_RoundStart_m666118331 (FightRound_t2951839614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightRound::ReceptionAI(SocketIO.SocketIOEvent)
extern "C"  void FightRound_ReceptionAI_m3753340358 (FightRound_t2951839614 * __this, SocketIOEvent_t4011854063 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightRound::ReceptionChar(SocketIO.SocketIOEvent)
extern "C"  void FightRound_ReceptionChar_m1166312660 (FightRound_t2951839614 * __this, SocketIOEvent_t4011854063 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightRound::ReceptionScore(SocketIO.SocketIOEvent)
extern "C"  void FightRound_ReceptionScore_m2651300190 (FightRound_t2951839614 * __this, SocketIOEvent_t4011854063 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightRound::ReceptoionStart()
extern "C"  void FightRound_ReceptoionStart_m433129387 (FightRound_t2951839614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator FightRound::RoundTimeAction()
extern "C"  Il2CppObject * FightRound_RoundTimeAction_m1195124100 (FightRound_t2951839614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator FightRound::ShowRetire(UnityEngine.GameObject)
extern "C"  Il2CppObject * FightRound_ShowRetire_m1100445759 (FightRound_t2951839614 * __this, GameObject_t3674682005 * ____go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
