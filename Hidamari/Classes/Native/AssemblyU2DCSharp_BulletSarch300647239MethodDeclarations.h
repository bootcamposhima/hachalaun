﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BulletSarch
struct BulletSarch_t300647239;
// BlackBoard
struct BlackBoard_t328561223;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BlackBoard328561223.h"

// System.Void BulletSarch::.ctor()
extern "C"  void BulletSarch__ctor_m2397157380 (BulletSarch_t300647239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BulletSarch::Start()
extern "C"  void BulletSarch_Start_m1344295172 (BulletSarch_t300647239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BulletSarch::Update()
extern "C"  void BulletSarch_Update_m3024296841 (BulletSarch_t300647239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BulletSarch::PlayCondition(BlackBoard)
extern "C"  bool BulletSarch_PlayCondition_m2480460022 (BulletSarch_t300647239 * __this, BlackBoard_t328561223 * ___blackboard0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
