﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FightSocket
struct FightSocket_t1340804995;
// JSONObject
struct JSONObject_t1752376903;
// System.String
struct String_t;
// SocketIO.SocketIOEvent
struct SocketIOEvent_t4011854063;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSONObject1752376903.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_SocketIO_SocketIOEvent4011854063.h"

// System.Void FightSocket::.ctor()
extern "C"  void FightSocket__ctor_m181808968 (FightSocket_t1340804995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightSocket::Start()
extern "C"  void FightSocket_Start_m3423914056 (FightSocket_t1340804995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightSocket::Awake()
extern "C"  void FightSocket_Awake_m419414187 (FightSocket_t1340804995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightSocket::Update()
extern "C"  void FightSocket_Update_m3067972805 (FightSocket_t1340804995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightSocket::get_isConnect()
extern "C"  bool FightSocket_get_isConnect_m2838496561 (FightSocket_t1340804995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightSocket::EmitConnection()
extern "C"  bool FightSocket_EmitConnection_m1539404761 (FightSocket_t1340804995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightSocket::EmitReConnection()
extern "C"  bool FightSocket_EmitReConnection_m1349790508 (FightSocket_t1340804995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightSocket::EmitJoin()
extern "C"  void FightSocket_EmitJoin_m566578137 (FightSocket_t1340804995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightSocket::EmitMassage(JSONObject)
extern "C"  void FightSocket_EmitMassage_m4125347127 (FightSocket_t1340804995 * __this, JSONObject_t1752376903 * ___jsonObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightSocket::EmitMassage(System.String)
extern "C"  void FightSocket_EmitMassage_m1761717988 (FightSocket_t1340804995 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightSocket::EmitOldmessagerequest(JSONObject)
extern "C"  void FightSocket_EmitOldmessagerequest_m1696318907 (FightSocket_t1340804995 * __this, JSONObject_t1752376903 * ___jsonObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightSocket::EmitDisconnect()
extern "C"  void FightSocket_EmitDisconnect_m2548389963 (FightSocket_t1340804995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightSocket::EmitOnlinenotification()
extern "C"  void FightSocket_EmitOnlinenotification_m4149332973 (FightSocket_t1340804995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightSocket::EmitEndnotification()
extern "C"  void FightSocket_EmitEndnotification_m2109315033 (FightSocket_t1340804995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightSocket::Online(SocketIO.SocketIOEvent)
extern "C"  void FightSocket_Online_m2888803299 (FightSocket_t1340804995 * __this, SocketIOEvent_t4011854063 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightSocket::Join(SocketIO.SocketIOEvent)
extern "C"  void FightSocket_Join_m4027250810 (FightSocket_t1340804995 * __this, SocketIOEvent_t4011854063 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightSocket::Offline(SocketIO.SocketIOEvent)
extern "C"  void FightSocket_Offline_m3500494045 (FightSocket_t1340804995 * __this, SocketIOEvent_t4011854063 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightSocket::Message(SocketIO.SocketIOEvent)
extern "C"  void FightSocket_Message_m2104693505 (FightSocket_t1340804995 * __this, SocketIOEvent_t4011854063 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightSocket::Onlinenotification(SocketIO.SocketIOEvent)
extern "C"  void FightSocket_Onlinenotification_m1489688270 (FightSocket_t1340804995 * __this, SocketIOEvent_t4011854063 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightSocket::Endnotification(SocketIO.SocketIOEvent)
extern "C"  void FightSocket_Endnotification_m1752068672 (FightSocket_t1340804995 * __this, SocketIOEvent_t4011854063 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightSocket::TestOpen(SocketIO.SocketIOEvent)
extern "C"  void FightSocket_TestOpen_m4262344140 (FightSocket_t1340804995 * __this, SocketIOEvent_t4011854063 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightSocket::TestError(SocketIO.SocketIOEvent)
extern "C"  void FightSocket_TestError_m3645520208 (FightSocket_t1340804995 * __this, SocketIOEvent_t4011854063 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightSocket::TestClose(SocketIO.SocketIOEvent)
extern "C"  void FightSocket_TestClose_m2260890944 (FightSocket_t1340804995 * __this, SocketIOEvent_t4011854063 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
