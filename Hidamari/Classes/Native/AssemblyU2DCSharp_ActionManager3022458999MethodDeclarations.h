﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ActionManager
struct ActionManager_t3022458999;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void ActionManager::.ctor()
extern "C"  void ActionManager__ctor_m329836244 (ActionManager_t3022458999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActionManager::Awake()
extern "C"  void ActionManager_Awake_m567441463 (ActionManager_t3022458999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActionManager::Start()
extern "C"  void ActionManager_Start_m3571941332 (ActionManager_t3022458999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActionManager::Update()
extern "C"  void ActionManager_Update_m3361851065 (ActionManager_t3022458999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActionManager::StartMoveAction(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  void ActionManager_StartMoveAction_m4230752139 (ActionManager_t3022458999 * __this, GameObject_t3674682005 * ____ob0, Vector3_t4282066566  ___destination1, float ___deltatime2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActionManager::StartMoveForeverAction(UnityEngine.GameObject,UnityEngine.Vector3,System.Single,System.Single)
extern "C"  void ActionManager_StartMoveForeverAction_m1516317103 (ActionManager_t3022458999 * __this, GameObject_t3674682005 * ____ob0, Vector3_t4282066566  ___destination1, float ___deltatime2, float ___waittime3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActionManager::StopMoveForeverAction()
extern "C"  void ActionManager_StopMoveForeverAction_m4119690684 (ActionManager_t3022458999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ActionManager::ForeverMoveAction(UnityEngine.GameObject,UnityEngine.Vector3,System.Single,System.Single)
extern "C"  Il2CppObject * ActionManager_ForeverMoveAction_m3151818493 (ActionManager_t3022458999 * __this, GameObject_t3674682005 * ____ob0, Vector3_t4282066566  ___destination1, float ___deltatime2, float ___waittime3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ActionManager::MoveAction(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  Il2CppObject * ActionManager_MoveAction_m1625003199 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____ob0, Vector3_t4282066566  ___destination1, float ___deltatime2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActionManager::StartRotateAction(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  void ActionManager_StartRotateAction_m329234517 (ActionManager_t3022458999 * __this, GameObject_t3674682005 * ____ob0, Vector3_t4282066566  ___rotate1, float ___deltatime2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ActionManager::RotateAction(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  Il2CppObject * ActionManager_RotateAction_m170436745 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____ob0, Vector3_t4282066566  ___rotate1, float ___deltatime2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ActionManager::UIMove(UnityEngine.GameObject,UnityEngine.Vector3,System.Single&)
extern "C"  bool ActionManager_UIMove_m291031579 (ActionManager_t3022458999 * __this, GameObject_t3674682005 * ____ob0, Vector3_t4282066566  ___destination1, float* ___spd2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
