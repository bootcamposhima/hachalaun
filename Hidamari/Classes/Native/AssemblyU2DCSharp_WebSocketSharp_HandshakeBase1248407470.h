﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t2791941106;
// System.Version
struct Version_t763695022;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.HandshakeBase
struct  HandshakeBase_t1248407470  : public Il2CppObject
{
public:
	// System.Collections.Specialized.NameValueCollection WebSocketSharp.HandshakeBase::_headers
	NameValueCollection_t2791941106 * ____headers_1;
	// System.Version WebSocketSharp.HandshakeBase::_version
	Version_t763695022 * ____version_2;
	// System.Byte[] WebSocketSharp.HandshakeBase::EntityBodyData
	ByteU5BU5D_t4260760469* ___EntityBodyData_3;

public:
	inline static int32_t get_offset_of__headers_1() { return static_cast<int32_t>(offsetof(HandshakeBase_t1248407470, ____headers_1)); }
	inline NameValueCollection_t2791941106 * get__headers_1() const { return ____headers_1; }
	inline NameValueCollection_t2791941106 ** get_address_of__headers_1() { return &____headers_1; }
	inline void set__headers_1(NameValueCollection_t2791941106 * value)
	{
		____headers_1 = value;
		Il2CppCodeGenWriteBarrier(&____headers_1, value);
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(HandshakeBase_t1248407470, ____version_2)); }
	inline Version_t763695022 * get__version_2() const { return ____version_2; }
	inline Version_t763695022 ** get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(Version_t763695022 * value)
	{
		____version_2 = value;
		Il2CppCodeGenWriteBarrier(&____version_2, value);
	}

	inline static int32_t get_offset_of_EntityBodyData_3() { return static_cast<int32_t>(offsetof(HandshakeBase_t1248407470, ___EntityBodyData_3)); }
	inline ByteU5BU5D_t4260760469* get_EntityBodyData_3() const { return ___EntityBodyData_3; }
	inline ByteU5BU5D_t4260760469** get_address_of_EntityBodyData_3() { return &___EntityBodyData_3; }
	inline void set_EntityBodyData_3(ByteU5BU5D_t4260760469* value)
	{
		___EntityBodyData_3 = value;
		Il2CppCodeGenWriteBarrier(&___EntityBodyData_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
