﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IsFlag
struct IsFlag_t2198325206;
// BlackBoard
struct BlackBoard_t328561223;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BlackBoard328561223.h"

// System.Void IsFlag::.ctor()
extern "C"  void IsFlag__ctor_m2373193093 (IsFlag_t2198325206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IsFlag::Start()
extern "C"  void IsFlag_Start_m1320330885 (IsFlag_t2198325206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IsFlag::Update()
extern "C"  void IsFlag_Update_m2281403944 (IsFlag_t2198325206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean IsFlag::PlayCondition(BlackBoard)
extern "C"  bool IsFlag_PlayCondition_m2128058447 (IsFlag_t2198325206 * __this, BlackBoard_t328561223 * ___blackboard0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
