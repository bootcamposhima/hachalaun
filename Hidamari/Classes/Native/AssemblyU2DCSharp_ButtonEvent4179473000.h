﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonEvent
struct  ButtonEvent_t4179473000  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject ButtonEvent::_object
	GameObject_t3674682005 * ____object_2;
	// System.Int32 ButtonEvent::num
	int32_t ___num_3;

public:
	inline static int32_t get_offset_of__object_2() { return static_cast<int32_t>(offsetof(ButtonEvent_t4179473000, ____object_2)); }
	inline GameObject_t3674682005 * get__object_2() const { return ____object_2; }
	inline GameObject_t3674682005 ** get_address_of__object_2() { return &____object_2; }
	inline void set__object_2(GameObject_t3674682005 * value)
	{
		____object_2 = value;
		Il2CppCodeGenWriteBarrier(&____object_2, value);
	}

	inline static int32_t get_offset_of_num_3() { return static_cast<int32_t>(offsetof(ButtonEvent_t4179473000, ___num_3)); }
	inline int32_t get_num_3() const { return ___num_3; }
	inline int32_t* get_address_of_num_3() { return &___num_3; }
	inline void set_num_3(int32_t value)
	{
		___num_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
