﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LitJson.JsonMockWrapper
struct JsonMockWrapper_t721699511;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// LitJson.JsonWriter
struct JsonWriter_t1165300239;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "LitJson_LitJson_JsonType1715515030.h"
#include "mscorlib_System_String7231557.h"
#include "LitJson_LitJson_JsonWriter1165300239.h"
#include "mscorlib_System_Array1146569071.h"

// System.Boolean LitJson.JsonMockWrapper::System.Collections.IList.get_IsFixedSize()
extern "C"  bool JsonMockWrapper_System_Collections_IList_get_IsFixedSize_m833095657 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonMockWrapper::System.Collections.IList.get_IsReadOnly()
extern "C"  bool JsonMockWrapper_System_Collections_IList_get_IsReadOnly_m1866356208 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LitJson.JsonMockWrapper::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * JsonMockWrapper_System_Collections_IList_get_Item_m331917991 (JsonMockWrapper_t721699511 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMockWrapper::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void JsonMockWrapper_System_Collections_IList_set_Item_m14162548 (JsonMockWrapper_t721699511 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LitJson.JsonMockWrapper::System.Collections.ICollection.get_Count()
extern "C"  int32_t JsonMockWrapper_System_Collections_ICollection_get_Count_m3269071315 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonMockWrapper::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool JsonMockWrapper_System_Collections_ICollection_get_IsSynchronized_m167110690 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LitJson.JsonMockWrapper::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * JsonMockWrapper_System_Collections_ICollection_get_SyncRoot_m3932193056 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LitJson.JsonMockWrapper::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * JsonMockWrapper_System_Collections_IDictionary_get_Item_m3715834294 (JsonMockWrapper_t721699511 * __this, Il2CppObject * ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMockWrapper::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void JsonMockWrapper_System_Collections_IDictionary_set_Item_m1813382373 (JsonMockWrapper_t721699511 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonMockWrapper::get_IsArray()
extern "C"  bool JsonMockWrapper_get_IsArray_m4125479426 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonMockWrapper::get_IsBoolean()
extern "C"  bool JsonMockWrapper_get_IsBoolean_m3815635473 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonMockWrapper::get_IsDouble()
extern "C"  bool JsonMockWrapper_get_IsDouble_m1693245610 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonMockWrapper::get_IsInt()
extern "C"  bool JsonMockWrapper_get_IsInt_m2880832536 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonMockWrapper::get_IsLong()
extern "C"  bool JsonMockWrapper_get_IsLong_m3493155477 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonMockWrapper::get_IsObject()
extern "C"  bool JsonMockWrapper_get_IsObject_m424273048 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonMockWrapper::get_IsString()
extern "C"  bool JsonMockWrapper_get_IsString_m2128786666 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMockWrapper::.ctor()
extern "C"  void JsonMockWrapper__ctor_m1481103688 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonMockWrapper::GetBoolean()
extern "C"  bool JsonMockWrapper_GetBoolean_m3692833272 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double LitJson.JsonMockWrapper::GetDouble()
extern "C"  double JsonMockWrapper_GetDouble_m940097040 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LitJson.JsonMockWrapper::GetInt()
extern "C"  int32_t JsonMockWrapper_GetInt_m3085795109 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 LitJson.JsonMockWrapper::GetLong()
extern "C"  int64_t JsonMockWrapper_GetLong_m1851991145 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LitJson.JsonMockWrapper::GetString()
extern "C"  String_t* JsonMockWrapper_GetString_m4133901840 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMockWrapper::SetBoolean(System.Boolean)
extern "C"  void JsonMockWrapper_SetBoolean_m716015769 (JsonMockWrapper_t721699511 * __this, bool ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMockWrapper::SetDouble(System.Double)
extern "C"  void JsonMockWrapper_SetDouble_m244632041 (JsonMockWrapper_t721699511 * __this, double ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMockWrapper::SetInt(System.Int32)
extern "C"  void JsonMockWrapper_SetInt_m3295367418 (JsonMockWrapper_t721699511 * __this, int32_t ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMockWrapper::SetJsonType(LitJson.JsonType)
extern "C"  void JsonMockWrapper_SetJsonType_m320338713 (JsonMockWrapper_t721699511 * __this, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMockWrapper::SetLong(System.Int64)
extern "C"  void JsonMockWrapper_SetLong_m3883225718 (JsonMockWrapper_t721699511 * __this, int64_t ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMockWrapper::SetString(System.String)
extern "C"  void JsonMockWrapper_SetString_m3473441129 (JsonMockWrapper_t721699511 * __this, String_t* ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LitJson.JsonMockWrapper::ToJson()
extern "C"  String_t* JsonMockWrapper_ToJson_m2825825148 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMockWrapper::ToJson(LitJson.JsonWriter)
extern "C"  void JsonMockWrapper_ToJson_m3238749269 (JsonMockWrapper_t721699511 * __this, JsonWriter_t1165300239 * ___writer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LitJson.JsonMockWrapper::System.Collections.IList.Add(System.Object)
extern "C"  int32_t JsonMockWrapper_System_Collections_IList_Add_m2771630982 (JsonMockWrapper_t721699511 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMockWrapper::System.Collections.IList.Clear()
extern "C"  void JsonMockWrapper_System_Collections_IList_Clear_m2827871242 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonMockWrapper::System.Collections.IList.Contains(System.Object)
extern "C"  bool JsonMockWrapper_System_Collections_IList_Contains_m3585512698 (JsonMockWrapper_t721699511 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LitJson.JsonMockWrapper::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t JsonMockWrapper_System_Collections_IList_IndexOf_m3581853406 (JsonMockWrapper_t721699511 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMockWrapper::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void JsonMockWrapper_System_Collections_IList_Insert_m1974792669 (JsonMockWrapper_t721699511 * __this, int32_t ___i0, Il2CppObject * ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMockWrapper::System.Collections.IList.Remove(System.Object)
extern "C"  void JsonMockWrapper_System_Collections_IList_Remove_m1753871403 (JsonMockWrapper_t721699511 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMockWrapper::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void JsonMockWrapper_System_Collections_IList_RemoveAt_m120794349 (JsonMockWrapper_t721699511 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMockWrapper::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void JsonMockWrapper_System_Collections_ICollection_CopyTo_m2016273660 (JsonMockWrapper_t721699511 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LitJson.JsonMockWrapper::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * JsonMockWrapper_System_Collections_IEnumerable_GetEnumerator_m3438062999 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMockWrapper::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void JsonMockWrapper_System_Collections_IDictionary_Add_m2671771212 (JsonMockWrapper_t721699511 * __this, Il2CppObject * ___k0, Il2CppObject * ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonMockWrapper::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void JsonMockWrapper_System_Collections_IDictionary_Remove_m4285589795 (JsonMockWrapper_t721699511 * __this, Il2CppObject * ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionaryEnumerator LitJson.JsonMockWrapper::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * JsonMockWrapper_System_Collections_IDictionary_GetEnumerator_m2972871643 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionaryEnumerator LitJson.JsonMockWrapper::System.Collections.Specialized.IOrderedDictionary.GetEnumerator()
extern "C"  Il2CppObject * JsonMockWrapper_System_Collections_Specialized_IOrderedDictionary_GetEnumerator_m1245726173 (JsonMockWrapper_t721699511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
