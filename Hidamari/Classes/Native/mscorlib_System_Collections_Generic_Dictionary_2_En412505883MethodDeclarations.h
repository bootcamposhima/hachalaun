﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3363211663MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m4039982377(__this, ___dictionary0, method) ((  void (*) (Enumerator_t412505883 *, Dictionary_2_t3390149787 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3681575320(__this, method) ((  Il2CppObject * (*) (Enumerator_t412505883 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1452428268(__this, method) ((  void (*) (Enumerator_t412505883 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4291356789(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t412505883 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4196797556(__this, method) ((  Il2CppObject * (*) (Enumerator_t412505883 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1196596742(__this, method) ((  Il2CppObject * (*) (Enumerator_t412505883 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>::MoveNext()
#define Enumerator_MoveNext_m1423267032(__this, method) ((  bool (*) (Enumerator_t412505883 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>::get_Current()
#define Enumerator_get_Current_m17386584(__this, method) ((  KeyValuePair_2_t3288930493  (*) (Enumerator_t412505883 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3210159269(__this, method) ((  HttpConnection_t602292776 * (*) (Enumerator_t412505883 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1338227209(__this, method) ((  HttpConnection_t602292776 * (*) (Enumerator_t412505883 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>::Reset()
#define Enumerator_Reset_m922495483(__this, method) ((  void (*) (Enumerator_t412505883 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>::VerifyState()
#define Enumerator_VerifyState_m4182299076(__this, method) ((  void (*) (Enumerator_t412505883 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1844894764(__this, method) ((  void (*) (Enumerator_t412505883 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>::Dispose()
#define Enumerator_Dispose_m4278325259(__this, method) ((  void (*) (Enumerator_t412505883 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
