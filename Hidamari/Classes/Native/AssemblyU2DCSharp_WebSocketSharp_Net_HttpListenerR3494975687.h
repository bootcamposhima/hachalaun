﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// WebSocketSharp.Net.Cookie
struct Cookie_t2077085446;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// WebSocketSharp.Net.HttpListenerResponse
struct HttpListenerResponse_t1992878431;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator1B
struct  U3CfindCookieU3Ec__Iterator1B_t3494975687  : public Il2CppObject
{
public:
	// WebSocketSharp.Net.Cookie WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator1B::cookie
	Cookie_t2077085446 * ___cookie_0;
	// System.String WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator1B::<name>__0
	String_t* ___U3CnameU3E__0_1;
	// System.String WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator1B::<domain>__1
	String_t* ___U3CdomainU3E__1_2;
	// System.String WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator1B::<path>__2
	String_t* ___U3CpathU3E__2_3;
	// System.Collections.IEnumerator WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator1B::<$s_96>__3
	Il2CppObject * ___U3CU24s_96U3E__3_4;
	// WebSocketSharp.Net.Cookie WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator1B::<c>__4
	Cookie_t2077085446 * ___U3CcU3E__4_5;
	// System.Int32 WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator1B::$PC
	int32_t ___U24PC_6;
	// WebSocketSharp.Net.Cookie WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator1B::$current
	Cookie_t2077085446 * ___U24current_7;
	// WebSocketSharp.Net.Cookie WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator1B::<$>cookie
	Cookie_t2077085446 * ___U3CU24U3Ecookie_8;
	// WebSocketSharp.Net.HttpListenerResponse WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator1B::<>f__this
	HttpListenerResponse_t1992878431 * ___U3CU3Ef__this_9;

public:
	inline static int32_t get_offset_of_cookie_0() { return static_cast<int32_t>(offsetof(U3CfindCookieU3Ec__Iterator1B_t3494975687, ___cookie_0)); }
	inline Cookie_t2077085446 * get_cookie_0() const { return ___cookie_0; }
	inline Cookie_t2077085446 ** get_address_of_cookie_0() { return &___cookie_0; }
	inline void set_cookie_0(Cookie_t2077085446 * value)
	{
		___cookie_0 = value;
		Il2CppCodeGenWriteBarrier(&___cookie_0, value);
	}

	inline static int32_t get_offset_of_U3CnameU3E__0_1() { return static_cast<int32_t>(offsetof(U3CfindCookieU3Ec__Iterator1B_t3494975687, ___U3CnameU3E__0_1)); }
	inline String_t* get_U3CnameU3E__0_1() const { return ___U3CnameU3E__0_1; }
	inline String_t** get_address_of_U3CnameU3E__0_1() { return &___U3CnameU3E__0_1; }
	inline void set_U3CnameU3E__0_1(String_t* value)
	{
		___U3CnameU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CnameU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3CdomainU3E__1_2() { return static_cast<int32_t>(offsetof(U3CfindCookieU3Ec__Iterator1B_t3494975687, ___U3CdomainU3E__1_2)); }
	inline String_t* get_U3CdomainU3E__1_2() const { return ___U3CdomainU3E__1_2; }
	inline String_t** get_address_of_U3CdomainU3E__1_2() { return &___U3CdomainU3E__1_2; }
	inline void set_U3CdomainU3E__1_2(String_t* value)
	{
		___U3CdomainU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdomainU3E__1_2, value);
	}

	inline static int32_t get_offset_of_U3CpathU3E__2_3() { return static_cast<int32_t>(offsetof(U3CfindCookieU3Ec__Iterator1B_t3494975687, ___U3CpathU3E__2_3)); }
	inline String_t* get_U3CpathU3E__2_3() const { return ___U3CpathU3E__2_3; }
	inline String_t** get_address_of_U3CpathU3E__2_3() { return &___U3CpathU3E__2_3; }
	inline void set_U3CpathU3E__2_3(String_t* value)
	{
		___U3CpathU3E__2_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpathU3E__2_3, value);
	}

	inline static int32_t get_offset_of_U3CU24s_96U3E__3_4() { return static_cast<int32_t>(offsetof(U3CfindCookieU3Ec__Iterator1B_t3494975687, ___U3CU24s_96U3E__3_4)); }
	inline Il2CppObject * get_U3CU24s_96U3E__3_4() const { return ___U3CU24s_96U3E__3_4; }
	inline Il2CppObject ** get_address_of_U3CU24s_96U3E__3_4() { return &___U3CU24s_96U3E__3_4; }
	inline void set_U3CU24s_96U3E__3_4(Il2CppObject * value)
	{
		___U3CU24s_96U3E__3_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_96U3E__3_4, value);
	}

	inline static int32_t get_offset_of_U3CcU3E__4_5() { return static_cast<int32_t>(offsetof(U3CfindCookieU3Ec__Iterator1B_t3494975687, ___U3CcU3E__4_5)); }
	inline Cookie_t2077085446 * get_U3CcU3E__4_5() const { return ___U3CcU3E__4_5; }
	inline Cookie_t2077085446 ** get_address_of_U3CcU3E__4_5() { return &___U3CcU3E__4_5; }
	inline void set_U3CcU3E__4_5(Cookie_t2077085446 * value)
	{
		___U3CcU3E__4_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcU3E__4_5, value);
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CfindCookieU3Ec__Iterator1B_t3494975687, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CfindCookieU3Ec__Iterator1B_t3494975687, ___U24current_7)); }
	inline Cookie_t2077085446 * get_U24current_7() const { return ___U24current_7; }
	inline Cookie_t2077085446 ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Cookie_t2077085446 * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ecookie_8() { return static_cast<int32_t>(offsetof(U3CfindCookieU3Ec__Iterator1B_t3494975687, ___U3CU24U3Ecookie_8)); }
	inline Cookie_t2077085446 * get_U3CU24U3Ecookie_8() const { return ___U3CU24U3Ecookie_8; }
	inline Cookie_t2077085446 ** get_address_of_U3CU24U3Ecookie_8() { return &___U3CU24U3Ecookie_8; }
	inline void set_U3CU24U3Ecookie_8(Cookie_t2077085446 * value)
	{
		___U3CU24U3Ecookie_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Ecookie_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_9() { return static_cast<int32_t>(offsetof(U3CfindCookieU3Ec__Iterator1B_t3494975687, ___U3CU3Ef__this_9)); }
	inline HttpListenerResponse_t1992878431 * get_U3CU3Ef__this_9() const { return ___U3CU3Ef__this_9; }
	inline HttpListenerResponse_t1992878431 ** get_address_of_U3CU3Ef__this_9() { return &___U3CU3Ef__this_9; }
	inline void set_U3CU3Ef__this_9(HttpListenerResponse_t1992878431 * value)
	{
		___U3CU3Ef__this_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
