﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AIManager
struct AIManager_t3650593381;

#include "codegen/il2cpp-codegen.h"

// System.Void AIManager::.ctor()
extern "C"  void AIManager__ctor_m1475564710 (AIManager_t3650593381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIManager::Start()
extern "C"  void AIManager_Start_m422702502 (AIManager_t3650593381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIManager::Awake()
extern "C"  void AIManager_Awake_m1713169929 (AIManager_t3650593381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIManager::Update()
extern "C"  void AIManager_Update_m224727847 (AIManager_t3650593381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIManager::setScrollMove()
extern "C"  void AIManager_setScrollMove_m2392026404 (AIManager_t3650593381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIManager::seTop()
extern "C"  void AIManager_seTop_m2611480263 (AIManager_t3650593381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIManager::EntranceAction()
extern "C"  void AIManager_EntranceAction_m46112042 (AIManager_t3650593381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIManager::BulletEntranceAction()
extern "C"  void AIManager_BulletEntranceAction_m2406340748 (AIManager_t3650593381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIManager::LeavingAction()
extern "C"  void AIManager_LeavingAction_m890509646 (AIManager_t3650593381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIManager::BulletLeavingAction()
extern "C"  void AIManager_BulletLeavingAction_m1105193388 (AIManager_t3650593381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIManager::ChangeBulletScroll(System.Int32)
extern "C"  void AIManager_ChangeBulletScroll_m1754157422 (AIManager_t3650593381 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIManager::ReGoBack()
extern "C"  void AIManager_ReGoBack_m3611694816 (AIManager_t3650593381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIManager::ChangeBulletScroll()
extern "C"  void AIManager_ChangeBulletScroll_m400301981 (AIManager_t3650593381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIManager::UIStart()
extern "C"  void AIManager_UIStart_m1745676242 (AIManager_t3650593381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIManager::UIEnd()
extern "C"  void AIManager_UIEnd_m940663243 (AIManager_t3650593381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
