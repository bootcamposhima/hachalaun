﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GodTouches.Demo
struct Demo_t2005335435;

#include "codegen/il2cpp-codegen.h"

// System.Void GodTouches.Demo::.ctor()
extern "C"  void Demo__ctor_m1863332299 (Demo_t2005335435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GodTouches.Demo::Start()
extern "C"  void Demo_Start_m810470091 (Demo_t2005335435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GodTouches.Demo::Update()
extern "C"  void Demo_Update_m3655588514 (Demo_t2005335435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
