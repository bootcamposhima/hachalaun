﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_924606671.h"
#include "AssemblyU2DCSharp_WebSocketSharp_CompressionMethod2226596781.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.KeyValuePair`2<WebSocketSharp.CompressionMethod,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m200737057_gshared (KeyValuePair_2_t924606671 * __this, uint8_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m200737057(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t924606671 *, uint8_t, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m200737057_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<WebSocketSharp.CompressionMethod,System.Object>::get_Key()
extern "C"  uint8_t KeyValuePair_2_get_Key_m2143879591_gshared (KeyValuePair_2_t924606671 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m2143879591(__this, method) ((  uint8_t (*) (KeyValuePair_2_t924606671 *, const MethodInfo*))KeyValuePair_2_get_Key_m2143879591_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<WebSocketSharp.CompressionMethod,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3037882472_gshared (KeyValuePair_2_t924606671 * __this, uint8_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m3037882472(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t924606671 *, uint8_t, const MethodInfo*))KeyValuePair_2_set_Key_m3037882472_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<WebSocketSharp.CompressionMethod,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1606892327_gshared (KeyValuePair_2_t924606671 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m1606892327(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t924606671 *, const MethodInfo*))KeyValuePair_2_get_Value_m1606892327_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<WebSocketSharp.CompressionMethod,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2410891368_gshared (KeyValuePair_2_t924606671 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m2410891368(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t924606671 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m2410891368_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<WebSocketSharp.CompressionMethod,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2079672442_gshared (KeyValuePair_2_t924606671 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m2079672442(__this, method) ((  String_t* (*) (KeyValuePair_2_t924606671 *, const MethodInfo*))KeyValuePair_2_ToString_m2079672442_gshared)(__this, method)
