﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MatchingManager
struct MatchingManager_t1847897296;
// SocketIO.SocketIOEvent
struct SocketIOEvent_t4011854063;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SocketIO_SocketIOEvent4011854063.h"

// System.Void MatchingManager::.ctor()
extern "C"  void MatchingManager__ctor_m3761276059 (MatchingManager_t1847897296 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MatchingManager::Start()
extern "C"  void MatchingManager_Start_m2708413851 (MatchingManager_t1847897296 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MatchingManager::Update()
extern "C"  void MatchingManager_Update_m2362302930 (MatchingManager_t1847897296 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MatchingManager::Online(SocketIO.SocketIOEvent)
extern "C"  void MatchingManager_Online_m2860974384 (MatchingManager_t1847897296 * __this, SocketIOEvent_t4011854063 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MatchingManager::Join(SocketIO.SocketIOEvent)
extern "C"  void MatchingManager_Join_m3678618887 (MatchingManager_t1847897296 * __this, SocketIOEvent_t4011854063 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MatchingManager::Offline(SocketIO.SocketIOEvent)
extern "C"  void MatchingManager_Offline_m2637797680 (MatchingManager_t1847897296 * __this, SocketIOEvent_t4011854063 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MatchingManager::Message(SocketIO.SocketIOEvent)
extern "C"  void MatchingManager_Message_m1241997140 (MatchingManager_t1847897296 * __this, SocketIOEvent_t4011854063 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MatchingManager::Onlinenotification(SocketIO.SocketIOEvent)
extern "C"  void MatchingManager_Onlinenotification_m3815221403 (MatchingManager_t1847897296 * __this, SocketIOEvent_t4011854063 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MatchingManager::Endnotification(SocketIO.SocketIOEvent)
extern "C"  void MatchingManager_Endnotification_m3290872723 (MatchingManager_t1847897296 * __this, SocketIOEvent_t4011854063 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MatchingManager::Error(SocketIO.SocketIOEvent)
extern "C"  void MatchingManager_Error_m2094222421 (MatchingManager_t1847897296 * __this, SocketIOEvent_t4011854063 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MatchingManager::MatingTimeAction()
extern "C"  Il2CppObject * MatchingManager_MatingTimeAction_m2242155590 (MatchingManager_t1847897296 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MatchingManager::MoveStag()
extern "C"  void MatchingManager_MoveStag_m1146690817 (MatchingManager_t1847897296 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MatchingManager::StartParticle()
extern "C"  void MatchingManager_StartParticle_m3595478209 (MatchingManager_t1847897296 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MatchingManager::StopPar()
extern "C"  void MatchingManager_StopPar_m3781910840 (MatchingManager_t1847897296 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MatchingManager::TopReturnButton()
extern "C"  void MatchingManager_TopReturnButton_m2772044912 (MatchingManager_t1847897296 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
