﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResultData
struct  ResultData_t1421128071  : public MonoBehaviour_t667441552
{
public:
	// System.Int32[] ResultData::Player1HP
	Int32U5BU5D_t3230847821* ___Player1HP_2;
	// System.Int32[] ResultData::Player2HP
	Int32U5BU5D_t3230847821* ___Player2HP_3;
	// System.Int32[] ResultData::roundResult
	Int32U5BU5D_t3230847821* ___roundResult_4;
	// System.Boolean ResultData::isretire
	bool ___isretire_5;
	// System.Boolean ResultData::isdisconnection
	bool ___isdisconnection_6;

public:
	inline static int32_t get_offset_of_Player1HP_2() { return static_cast<int32_t>(offsetof(ResultData_t1421128071, ___Player1HP_2)); }
	inline Int32U5BU5D_t3230847821* get_Player1HP_2() const { return ___Player1HP_2; }
	inline Int32U5BU5D_t3230847821** get_address_of_Player1HP_2() { return &___Player1HP_2; }
	inline void set_Player1HP_2(Int32U5BU5D_t3230847821* value)
	{
		___Player1HP_2 = value;
		Il2CppCodeGenWriteBarrier(&___Player1HP_2, value);
	}

	inline static int32_t get_offset_of_Player2HP_3() { return static_cast<int32_t>(offsetof(ResultData_t1421128071, ___Player2HP_3)); }
	inline Int32U5BU5D_t3230847821* get_Player2HP_3() const { return ___Player2HP_3; }
	inline Int32U5BU5D_t3230847821** get_address_of_Player2HP_3() { return &___Player2HP_3; }
	inline void set_Player2HP_3(Int32U5BU5D_t3230847821* value)
	{
		___Player2HP_3 = value;
		Il2CppCodeGenWriteBarrier(&___Player2HP_3, value);
	}

	inline static int32_t get_offset_of_roundResult_4() { return static_cast<int32_t>(offsetof(ResultData_t1421128071, ___roundResult_4)); }
	inline Int32U5BU5D_t3230847821* get_roundResult_4() const { return ___roundResult_4; }
	inline Int32U5BU5D_t3230847821** get_address_of_roundResult_4() { return &___roundResult_4; }
	inline void set_roundResult_4(Int32U5BU5D_t3230847821* value)
	{
		___roundResult_4 = value;
		Il2CppCodeGenWriteBarrier(&___roundResult_4, value);
	}

	inline static int32_t get_offset_of_isretire_5() { return static_cast<int32_t>(offsetof(ResultData_t1421128071, ___isretire_5)); }
	inline bool get_isretire_5() const { return ___isretire_5; }
	inline bool* get_address_of_isretire_5() { return &___isretire_5; }
	inline void set_isretire_5(bool value)
	{
		___isretire_5 = value;
	}

	inline static int32_t get_offset_of_isdisconnection_6() { return static_cast<int32_t>(offsetof(ResultData_t1421128071, ___isdisconnection_6)); }
	inline bool get_isdisconnection_6() const { return ___isdisconnection_6; }
	inline bool* get_address_of_isdisconnection_6() { return &___isdisconnection_6; }
	inline void set_isdisconnection_6(bool value)
	{
		___isdisconnection_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
