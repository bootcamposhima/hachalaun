﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_U3CPrivateImplementationDetailsU3E3053238933.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24Array1676615740.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24Array3379220348.h"
#include "Mono_Security_U3CModuleU3E86524790.h"
#include "Mono_Security_Locale2281372282.h"
#include "Mono_Security_Mono_Math_BigInteger3334373498.h"
#include "Mono_Security_Mono_Math_BigInteger_Sign162809939.h"
#include "Mono_Security_Mono_Math_BigInteger_ModulusRing3697743089.h"
#include "Mono_Security_Mono_Math_BigInteger_Kernel1609132371.h"
#include "Mono_Security_Mono_Math_Prime_ConfidenceFactor3802548591.h"
#include "Mono_Security_Mono_Math_Prime_PrimalityTests194676186.h"
#include "Mono_Security_Mono_Math_Prime_Generator_PrimeGener1565729053.h"
#include "Mono_Security_Mono_Math_Prime_Generator_Sequential3892164356.h"
#include "Mono_Security_Mono_Security_ASN13752917377.h"
#include "Mono_Security_Mono_Security_ASN1Convert1533901092.h"
#include "Mono_Security_Mono_Security_BitConverterLE159646808.h"
#include "Mono_Security_Mono_Security_PKCS71481122354.h"
#include "Mono_Security_Mono_Security_PKCS7_ContentInfo3081670593.h"
#include "Mono_Security_Mono_Security_PKCS7_EncryptedData2709367816.h"
#include "Mono_Security_Mono_Security_Cryptography_ARC4Manag1354014707.h"
#include "Mono_Security_Mono_Security_Cryptography_CryptoCon3338259528.h"
#include "Mono_Security_Mono_Security_Cryptography_KeyBuilder373726640.h"
#include "Mono_Security_Mono_Security_Cryptography_MD2227518833.h"
#include "Mono_Security_Mono_Security_Cryptography_MD2Managed915988216.h"
#include "Mono_Security_Mono_Security_Cryptography_MD4227518835.h"
#include "Mono_Security_Mono_Security_Cryptography_MD4Managed106641590.h"
#include "Mono_Security_Mono_Security_Cryptography_PKCS13260516764.h"
#include "Mono_Security_Mono_Security_Cryptography_PKCS83260516771.h"
#include "Mono_Security_Mono_Security_Cryptography_PKCS8_Pri3021279627.h"
#include "Mono_Security_Mono_Security_Cryptography_PKCS8_Encr300608461.h"
#include "Mono_Security_Mono_Security_Cryptography_RC4227523609.h"
#include "Mono_Security_Mono_Security_Cryptography_RSAManage3258726067.h"
#include "Mono_Security_Mono_Security_Cryptography_RSAManage2889700849.h"
#include "Mono_Security_Mono_Security_X509_SafeBag95817449.h"
#include "Mono_Security_Mono_Security_X509_PKCS121158032324.h"
#include "Mono_Security_Mono_Security_X509_PKCS12_DeriveByte3666234662.h"
#include "Mono_Security_Mono_Security_X509_X5011661886358.h"
#include "Mono_Security_Mono_Security_X509_X509Certificate1623369439.h"
#include "Mono_Security_Mono_Security_X509_X509CertificateCo4135795357.h"
#include "Mono_Security_Mono_Security_X509_X509CertificateCo4235433104.h"
#include "Mono_Security_Mono_Security_X509_X509Chain2146754505.h"
#include "Mono_Security_Mono_Security_X509_X509ChainStatusFl3401646578.h"
#include "Mono_Security_Mono_Security_X509_X509Crl3222473765.h"
#include "Mono_Security_Mono_Security_X509_X509Crl_X509CrlEnt503881240.h"
#include "Mono_Security_Mono_Security_X509_X509Extension4211806919.h"
#include "Mono_Security_Mono_Security_X509_X509ExtensionColl3703825541.h"
#include "Mono_Security_Mono_Security_X509_X509Store2161902057.h"
#include "Mono_Security_Mono_Security_X509_X509StoreManager4083493578.h"
#include "Mono_Security_Mono_Security_X509_X509Stores2120745776.h"
#include "Mono_Security_Mono_Security_X509_X5201661886419.h"
#include "Mono_Security_Mono_Security_X509_X520_AttributeTyp2125183969.h"
#include "Mono_Security_Mono_Security_X509_X520_CommonName286506949.h"
#include "Mono_Security_Mono_Security_X509_X520_LocalityName3829901181.h"
#include "Mono_Security_Mono_Security_X509_X520_StateOrProvi1270785664.h"
#include "Mono_Security_Mono_Security_X509_X520_Organization3558481869.h"
#include "Mono_Security_Mono_Security_X509_X520_Organization1865992988.h"
#include "Mono_Security_Mono_Security_X509_X520_EmailAddress3721725351.h"
#include "Mono_Security_Mono_Security_X509_X520_DomainCompon3375793482.h"
#include "Mono_Security_Mono_Security_X509_X520_UserId3956503157.h"
#include "Mono_Security_Mono_Security_X509_X520_Oid98594811.h"
#include "Mono_Security_Mono_Security_X509_X520_Title264969289.h"
#include "Mono_Security_Mono_Security_X509_X520_CountryName756059250.h"
#include "Mono_Security_Mono_Security_X509_X520_DnQualifier3540680817.h"
#include "Mono_Security_Mono_Security_X509_X520_Surname686674572.h"
#include "Mono_Security_Mono_Security_X509_X520_GivenName2548463897.h"
#include "Mono_Security_Mono_Security_X509_X520_Initial193042677.h"
#include "Mono_Security_Mono_Security_X509_Extensions_Authori253620168.h"
#include "Mono_Security_Mono_Security_X509_Extensions_BasicC2578001957.h"
#include "Mono_Security_Mono_Security_X509_Extensions_Extend3095953362.h"
#include "Mono_Security_Mono_Security_X509_Extensions_Genera1128159800.h"
#include "Mono_Security_Mono_Security_X509_Extensions_KeyUsa2509425759.h"
#include "Mono_Security_Mono_Security_X509_Extensions_KeyUsag723947275.h"
#include "Mono_Security_Mono_Security_X509_Extensions_Netsca3510265670.h"
#include "Mono_Security_Mono_Security_X509_Extensions_Netsca1343248969.h"
#include "Mono_Security_Mono_Security_X509_Extensions_Subjec3859801861.h"
#include "Mono_Security_Mono_Security_Cryptography_HMAC2066897019.h"
#include "Mono_Security_Mono_Security_Cryptography_MD5SHA13104883481.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertLeve3033872806.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertDesc4238799518.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Alert2013630382.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherAlg1887515856.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherSuit580420223.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherSui2487974013.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherSuit311944379.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ClientCon3745725366.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ClientRec3260910930.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ClientSes1380030187.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ClientSes1861128725.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ContentTy2370127397.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Context658806145.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ExchangeA3271961912.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake1470195192.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_HashAlgor4118445357.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_HttpsClient25214728.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_RecordPro1331996967.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_RecordPro4254633253.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_RecordPro3836770528.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_RSASslSign654537083.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_RSASslSig1731244892.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityCo131120690.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1100 = { sizeof (U3CPrivateImplementationDetailsU3E_t3053238934), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1100[3] = 
{
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D2_0(),
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D3_1(),
	U3CPrivateImplementationDetailsU3E_t3053238934_StaticFields::get_offset_of_U24U24fieldU2D4_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1101 = { sizeof (U24ArrayTypeU24128_t1676615741)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24128_t1676615741_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1102 = { sizeof (U24ArrayTypeU2412_t3379220349)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t3379220349_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1103 = { sizeof (U3CModuleU3E_t86524792), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1104 = { sizeof (Locale_t2281372284), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1105 = { sizeof (BigInteger_t3334373499), -1, sizeof(BigInteger_t3334373499_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1105[4] = 
{
	BigInteger_t3334373499::get_offset_of_length_0(),
	BigInteger_t3334373499::get_offset_of_data_1(),
	BigInteger_t3334373499_StaticFields::get_offset_of_smallPrimes_2(),
	BigInteger_t3334373499_StaticFields::get_offset_of_rng_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1106 = { sizeof (Sign_t162809940)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1106[4] = 
{
	Sign_t162809940::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1107 = { sizeof (ModulusRing_t3697743090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1107[2] = 
{
	ModulusRing_t3697743090::get_offset_of_mod_0(),
	ModulusRing_t3697743090::get_offset_of_constant_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1108 = { sizeof (Kernel_t1609132372), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1109 = { sizeof (ConfidenceFactor_t3802548592)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1109[7] = 
{
	ConfidenceFactor_t3802548592::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1110 = { sizeof (PrimalityTests_t194676187), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1111 = { sizeof (PrimeGeneratorBase_t1565729054), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1112 = { sizeof (SequentialSearchPrimeGeneratorBase_t3892164357), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1113 = { sizeof (ASN1_t3752917378), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1113[3] = 
{
	ASN1_t3752917378::get_offset_of_m_nTag_0(),
	ASN1_t3752917378::get_offset_of_m_aValue_1(),
	ASN1_t3752917378::get_offset_of_elist_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1114 = { sizeof (ASN1Convert_t1533901093), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1115 = { sizeof (BitConverterLE_t159646809), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1116 = { sizeof (PKCS7_t1481122355), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1117 = { sizeof (ContentInfo_t3081670594), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1117[2] = 
{
	ContentInfo_t3081670594::get_offset_of_contentType_0(),
	ContentInfo_t3081670594::get_offset_of_content_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1118 = { sizeof (EncryptedData_t2709367817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1118[4] = 
{
	EncryptedData_t2709367817::get_offset_of__version_0(),
	EncryptedData_t2709367817::get_offset_of__content_1(),
	EncryptedData_t2709367817::get_offset_of__encryptionAlgorithm_2(),
	EncryptedData_t2709367817::get_offset_of__encrypted_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1119 = { sizeof (ARC4Managed_t1354014707), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1119[5] = 
{
	ARC4Managed_t1354014707::get_offset_of_key_12(),
	ARC4Managed_t1354014707::get_offset_of_state_13(),
	ARC4Managed_t1354014707::get_offset_of_x_14(),
	ARC4Managed_t1354014707::get_offset_of_y_15(),
	ARC4Managed_t1354014707::get_offset_of_m_disposed_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1120 = { sizeof (CryptoConvert_t3338259529), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1121 = { sizeof (KeyBuilder_t373726641), -1, sizeof(KeyBuilder_t373726641_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1121[1] = 
{
	KeyBuilder_t373726641_StaticFields::get_offset_of_rng_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1122 = { sizeof (MD2_t227518833), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1123 = { sizeof (MD2Managed_t915988216), -1, sizeof(MD2Managed_t915988216_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1123[6] = 
{
	MD2Managed_t915988216::get_offset_of_state_4(),
	MD2Managed_t915988216::get_offset_of_checksum_5(),
	MD2Managed_t915988216::get_offset_of_buffer_6(),
	MD2Managed_t915988216::get_offset_of_count_7(),
	MD2Managed_t915988216::get_offset_of_x_8(),
	MD2Managed_t915988216_StaticFields::get_offset_of_PI_SUBST_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1124 = { sizeof (MD4_t227518835), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1125 = { sizeof (MD4Managed_t106641590), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1125[5] = 
{
	MD4Managed_t106641590::get_offset_of_state_4(),
	MD4Managed_t106641590::get_offset_of_buffer_5(),
	MD4Managed_t106641590::get_offset_of_count_6(),
	MD4Managed_t106641590::get_offset_of_x_7(),
	MD4Managed_t106641590::get_offset_of_digest_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1126 = { sizeof (PKCS1_t3260516765), -1, sizeof(PKCS1_t3260516765_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1126[4] = 
{
	PKCS1_t3260516765_StaticFields::get_offset_of_emptySHA1_0(),
	PKCS1_t3260516765_StaticFields::get_offset_of_emptySHA256_1(),
	PKCS1_t3260516765_StaticFields::get_offset_of_emptySHA384_2(),
	PKCS1_t3260516765_StaticFields::get_offset_of_emptySHA512_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1127 = { sizeof (PKCS8_t3260516772), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1128 = { sizeof (PrivateKeyInfo_t3021279628), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1128[4] = 
{
	PrivateKeyInfo_t3021279628::get_offset_of__version_0(),
	PrivateKeyInfo_t3021279628::get_offset_of__algorithm_1(),
	PrivateKeyInfo_t3021279628::get_offset_of__key_2(),
	PrivateKeyInfo_t3021279628::get_offset_of__list_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1129 = { sizeof (EncryptedPrivateKeyInfo_t300608462), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1129[4] = 
{
	EncryptedPrivateKeyInfo_t300608462::get_offset_of__algorithm_0(),
	EncryptedPrivateKeyInfo_t300608462::get_offset_of__salt_1(),
	EncryptedPrivateKeyInfo_t300608462::get_offset_of__iterations_2(),
	EncryptedPrivateKeyInfo_t300608462::get_offset_of__data_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1130 = { sizeof (RC4_t227523609), -1, sizeof(RC4_t227523609_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1130[2] = 
{
	RC4_t227523609_StaticFields::get_offset_of_s_legalBlockSizes_10(),
	RC4_t227523609_StaticFields::get_offset_of_s_legalKeySizes_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1131 = { sizeof (RSAManaged_t3258726068), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1131[13] = 
{
	RSAManaged_t3258726068::get_offset_of_isCRTpossible_2(),
	RSAManaged_t3258726068::get_offset_of_keyBlinding_3(),
	RSAManaged_t3258726068::get_offset_of_keypairGenerated_4(),
	RSAManaged_t3258726068::get_offset_of_m_disposed_5(),
	RSAManaged_t3258726068::get_offset_of_d_6(),
	RSAManaged_t3258726068::get_offset_of_p_7(),
	RSAManaged_t3258726068::get_offset_of_q_8(),
	RSAManaged_t3258726068::get_offset_of_dp_9(),
	RSAManaged_t3258726068::get_offset_of_dq_10(),
	RSAManaged_t3258726068::get_offset_of_qInv_11(),
	RSAManaged_t3258726068::get_offset_of_n_12(),
	RSAManaged_t3258726068::get_offset_of_e_13(),
	RSAManaged_t3258726068::get_offset_of_KeyGenerated_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1132 = { sizeof (KeyGeneratedEventHandler_t2889700850), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1133 = { sizeof (SafeBag_t95817450), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1133[2] = 
{
	SafeBag_t95817450::get_offset_of__bagOID_0(),
	SafeBag_t95817450::get_offset_of__asn1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1134 = { sizeof (PKCS12_t1158032325), -1, sizeof(PKCS12_t1158032325_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1134[17] = 
{
	PKCS12_t1158032325_StaticFields::get_offset_of_recommendedIterationCount_0(),
	PKCS12_t1158032325::get_offset_of__password_1(),
	PKCS12_t1158032325::get_offset_of__keyBags_2(),
	PKCS12_t1158032325::get_offset_of__secretBags_3(),
	PKCS12_t1158032325::get_offset_of__certs_4(),
	PKCS12_t1158032325::get_offset_of__keyBagsChanged_5(),
	PKCS12_t1158032325::get_offset_of__secretBagsChanged_6(),
	PKCS12_t1158032325::get_offset_of__certsChanged_7(),
	PKCS12_t1158032325::get_offset_of__iterations_8(),
	PKCS12_t1158032325::get_offset_of__safeBags_9(),
	PKCS12_t1158032325::get_offset_of__rng_10(),
	PKCS12_t1158032325_StaticFields::get_offset_of_password_max_length_11(),
	PKCS12_t1158032325_StaticFields::get_offset_of_U3CU3Ef__switchU24map5_12(),
	PKCS12_t1158032325_StaticFields::get_offset_of_U3CU3Ef__switchU24map6_13(),
	PKCS12_t1158032325_StaticFields::get_offset_of_U3CU3Ef__switchU24map7_14(),
	PKCS12_t1158032325_StaticFields::get_offset_of_U3CU3Ef__switchU24map8_15(),
	PKCS12_t1158032325_StaticFields::get_offset_of_U3CU3Ef__switchU24mapC_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1135 = { sizeof (DeriveBytes_t3666234663), -1, sizeof(DeriveBytes_t3666234663_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1135[7] = 
{
	DeriveBytes_t3666234663_StaticFields::get_offset_of_keyDiversifier_0(),
	DeriveBytes_t3666234663_StaticFields::get_offset_of_ivDiversifier_1(),
	DeriveBytes_t3666234663_StaticFields::get_offset_of_macDiversifier_2(),
	DeriveBytes_t3666234663::get_offset_of__hashName_3(),
	DeriveBytes_t3666234663::get_offset_of__iterations_4(),
	DeriveBytes_t3666234663::get_offset_of__password_5(),
	DeriveBytes_t3666234663::get_offset_of__salt_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1136 = { sizeof (X501_t1661886359), -1, sizeof(X501_t1661886359_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1136[16] = 
{
	X501_t1661886359_StaticFields::get_offset_of_countryName_0(),
	X501_t1661886359_StaticFields::get_offset_of_organizationName_1(),
	X501_t1661886359_StaticFields::get_offset_of_organizationalUnitName_2(),
	X501_t1661886359_StaticFields::get_offset_of_commonName_3(),
	X501_t1661886359_StaticFields::get_offset_of_localityName_4(),
	X501_t1661886359_StaticFields::get_offset_of_stateOrProvinceName_5(),
	X501_t1661886359_StaticFields::get_offset_of_streetAddress_6(),
	X501_t1661886359_StaticFields::get_offset_of_domainComponent_7(),
	X501_t1661886359_StaticFields::get_offset_of_userid_8(),
	X501_t1661886359_StaticFields::get_offset_of_email_9(),
	X501_t1661886359_StaticFields::get_offset_of_dnQualifier_10(),
	X501_t1661886359_StaticFields::get_offset_of_title_11(),
	X501_t1661886359_StaticFields::get_offset_of_surname_12(),
	X501_t1661886359_StaticFields::get_offset_of_givenName_13(),
	X501_t1661886359_StaticFields::get_offset_of_initial_14(),
	X501_t1661886359_StaticFields::get_offset_of_U3CU3Ef__switchU24mapD_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1137 = { sizeof (X509Certificate_t1623369440), -1, sizeof(X509Certificate_t1623369440_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1137[26] = 
{
	X509Certificate_t1623369440::get_offset_of_decoder_0(),
	X509Certificate_t1623369440::get_offset_of_m_encodedcert_1(),
	X509Certificate_t1623369440::get_offset_of_m_from_2(),
	X509Certificate_t1623369440::get_offset_of_m_until_3(),
	X509Certificate_t1623369440::get_offset_of_issuer_4(),
	X509Certificate_t1623369440::get_offset_of_m_issuername_5(),
	X509Certificate_t1623369440::get_offset_of_m_keyalgo_6(),
	X509Certificate_t1623369440::get_offset_of_m_keyalgoparams_7(),
	X509Certificate_t1623369440::get_offset_of_subject_8(),
	X509Certificate_t1623369440::get_offset_of_m_subject_9(),
	X509Certificate_t1623369440::get_offset_of_m_publickey_10(),
	X509Certificate_t1623369440::get_offset_of_signature_11(),
	X509Certificate_t1623369440::get_offset_of_m_signaturealgo_12(),
	X509Certificate_t1623369440::get_offset_of_m_signaturealgoparams_13(),
	X509Certificate_t1623369440::get_offset_of_certhash_14(),
	X509Certificate_t1623369440::get_offset_of__rsa_15(),
	X509Certificate_t1623369440::get_offset_of__dsa_16(),
	X509Certificate_t1623369440::get_offset_of_version_17(),
	X509Certificate_t1623369440::get_offset_of_serialnumber_18(),
	X509Certificate_t1623369440::get_offset_of_issuerUniqueID_19(),
	X509Certificate_t1623369440::get_offset_of_subjectUniqueID_20(),
	X509Certificate_t1623369440::get_offset_of_extensions_21(),
	X509Certificate_t1623369440_StaticFields::get_offset_of_encoding_error_22(),
	X509Certificate_t1623369440_StaticFields::get_offset_of_U3CU3Ef__switchU24mapF_23(),
	X509Certificate_t1623369440_StaticFields::get_offset_of_U3CU3Ef__switchU24map10_24(),
	X509Certificate_t1623369440_StaticFields::get_offset_of_U3CU3Ef__switchU24map11_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1138 = { sizeof (X509CertificateCollection_t4135795358), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1139 = { sizeof (X509CertificateEnumerator_t4235433105), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1139[1] = 
{
	X509CertificateEnumerator_t4235433105::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1140 = { sizeof (X509Chain_t2146754505), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1140[5] = 
{
	X509Chain_t2146754505::get_offset_of_roots_0(),
	X509Chain_t2146754505::get_offset_of_certs_1(),
	X509Chain_t2146754505::get_offset_of__root_2(),
	X509Chain_t2146754505::get_offset_of__chain_3(),
	X509Chain_t2146754505::get_offset_of__status_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1141 = { sizeof (X509ChainStatusFlags_t3401646578)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1141[8] = 
{
	X509ChainStatusFlags_t3401646578::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1142 = { sizeof (X509Crl_t3222473765), -1, sizeof(X509Crl_t3222473765_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1142[11] = 
{
	X509Crl_t3222473765::get_offset_of_issuer_0(),
	X509Crl_t3222473765::get_offset_of_version_1(),
	X509Crl_t3222473765::get_offset_of_thisUpdate_2(),
	X509Crl_t3222473765::get_offset_of_nextUpdate_3(),
	X509Crl_t3222473765::get_offset_of_entries_4(),
	X509Crl_t3222473765::get_offset_of_signatureOID_5(),
	X509Crl_t3222473765::get_offset_of_signature_6(),
	X509Crl_t3222473765::get_offset_of_extensions_7(),
	X509Crl_t3222473765::get_offset_of_encoded_8(),
	X509Crl_t3222473765::get_offset_of_hash_value_9(),
	X509Crl_t3222473765_StaticFields::get_offset_of_U3CU3Ef__switchU24map13_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1143 = { sizeof (X509CrlEntry_t503881240), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1143[3] = 
{
	X509CrlEntry_t503881240::get_offset_of_sn_0(),
	X509CrlEntry_t503881240::get_offset_of_revocationDate_1(),
	X509CrlEntry_t503881240::get_offset_of_extensions_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1144 = { sizeof (X509Extension_t4211806920), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1144[3] = 
{
	X509Extension_t4211806920::get_offset_of_extnOid_0(),
	X509Extension_t4211806920::get_offset_of_extnCritical_1(),
	X509Extension_t4211806920::get_offset_of_extnValue_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1145 = { sizeof (X509ExtensionCollection_t3703825542), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1145[1] = 
{
	X509ExtensionCollection_t3703825542::get_offset_of_readOnly_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1146 = { sizeof (X509Store_t2161902057), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1146[4] = 
{
	X509Store_t2161902057::get_offset_of__storePath_0(),
	X509Store_t2161902057::get_offset_of__certificates_1(),
	X509Store_t2161902057::get_offset_of__crls_2(),
	X509Store_t2161902057::get_offset_of__crl_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1147 = { sizeof (X509StoreManager_t4083493578), -1, sizeof(X509StoreManager_t4083493578_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1147[2] = 
{
	X509StoreManager_t4083493578_StaticFields::get_offset_of__userStore_0(),
	X509StoreManager_t4083493578_StaticFields::get_offset_of__machineStore_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1148 = { sizeof (X509Stores_t2120745776), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1148[2] = 
{
	X509Stores_t2120745776::get_offset_of__storePath_0(),
	X509Stores_t2120745776::get_offset_of__trusted_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1149 = { sizeof (X520_t1661886419), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1150 = { sizeof (AttributeTypeAndValue_t2125183969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1150[4] = 
{
	AttributeTypeAndValue_t2125183969::get_offset_of_oid_0(),
	AttributeTypeAndValue_t2125183969::get_offset_of_attrValue_1(),
	AttributeTypeAndValue_t2125183969::get_offset_of_upperBound_2(),
	AttributeTypeAndValue_t2125183969::get_offset_of_encoding_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1151 = { sizeof (CommonName_t286506949), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1152 = { sizeof (LocalityName_t3829901181), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1153 = { sizeof (StateOrProvinceName_t1270785664), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1154 = { sizeof (OrganizationName_t3558481869), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1155 = { sizeof (OrganizationalUnitName_t1865992988), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1156 = { sizeof (EmailAddress_t3721725351), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1157 = { sizeof (DomainComponent_t3375793482), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1158 = { sizeof (UserId_t3956503157), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1159 = { sizeof (Oid_t98594811), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1160 = { sizeof (Title_t264969289), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1161 = { sizeof (CountryName_t756059250), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1162 = { sizeof (DnQualifier_t3540680817), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1163 = { sizeof (Surname_t686674572), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1164 = { sizeof (GivenName_t2548463897), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1165 = { sizeof (Initial_t193042677), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1166 = { sizeof (AuthorityKeyIdentifierExtension_t253620168), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1166[1] = 
{
	AuthorityKeyIdentifierExtension_t253620168::get_offset_of_aki_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1167 = { sizeof (BasicConstraintsExtension_t2578001957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1167[2] = 
{
	BasicConstraintsExtension_t2578001957::get_offset_of_cA_3(),
	BasicConstraintsExtension_t2578001957::get_offset_of_pathLenConstraint_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1168 = { sizeof (ExtendedKeyUsageExtension_t3095953362), -1, sizeof(ExtendedKeyUsageExtension_t3095953362_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1168[2] = 
{
	ExtendedKeyUsageExtension_t3095953362::get_offset_of_keyPurpose_3(),
	ExtendedKeyUsageExtension_t3095953362_StaticFields::get_offset_of_U3CU3Ef__switchU24map14_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1169 = { sizeof (GeneralNames_t1128159800), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1169[5] = 
{
	GeneralNames_t1128159800::get_offset_of_rfc822Name_0(),
	GeneralNames_t1128159800::get_offset_of_dnsName_1(),
	GeneralNames_t1128159800::get_offset_of_directoryNames_2(),
	GeneralNames_t1128159800::get_offset_of_uris_3(),
	GeneralNames_t1128159800::get_offset_of_ipAddr_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1170 = { sizeof (KeyUsages_t2509425759)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1170[11] = 
{
	KeyUsages_t2509425759::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1171 = { sizeof (KeyUsageExtension_t723947275), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1171[1] = 
{
	KeyUsageExtension_t723947275::get_offset_of_kubits_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1172 = { sizeof (NetscapeCertTypeExtension_t3510265670), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1172[1] = 
{
	NetscapeCertTypeExtension_t3510265670::get_offset_of_ctbits_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1173 = { sizeof (CertTypes_t1343248969)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1173[8] = 
{
	CertTypes_t1343248969::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1174 = { sizeof (SubjectAltNameExtension_t3859801861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1174[1] = 
{
	SubjectAltNameExtension_t3859801861::get_offset_of__names_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1175 = { sizeof (HMAC_t2066897019), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1175[4] = 
{
	HMAC_t2066897019::get_offset_of_hash_5(),
	HMAC_t2066897019::get_offset_of_hashing_6(),
	HMAC_t2066897019::get_offset_of_innerPad_7(),
	HMAC_t2066897019::get_offset_of_outerPad_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1176 = { sizeof (MD5SHA1_t3104883481), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1176[3] = 
{
	MD5SHA1_t3104883481::get_offset_of_md5_4(),
	MD5SHA1_t3104883481::get_offset_of_sha_5(),
	MD5SHA1_t3104883481::get_offset_of_hashing_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1177 = { sizeof (AlertLevel_t3033872806)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1177[3] = 
{
	AlertLevel_t3033872806::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1178 = { sizeof (AlertDescription_t4238799518)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1178[25] = 
{
	AlertDescription_t4238799518::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1179 = { sizeof (Alert_t2013630382), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1179[2] = 
{
	Alert_t2013630382::get_offset_of_level_0(),
	Alert_t2013630382::get_offset_of_description_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1180 = { sizeof (CipherAlgorithmType_t1887515856)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1180[8] = 
{
	CipherAlgorithmType_t1887515856::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1181 = { sizeof (CipherSuite_t580420223), -1, sizeof(CipherSuite_t580420223_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1181[21] = 
{
	CipherSuite_t580420223_StaticFields::get_offset_of_EmptyArray_0(),
	CipherSuite_t580420223::get_offset_of_code_1(),
	CipherSuite_t580420223::get_offset_of_name_2(),
	CipherSuite_t580420223::get_offset_of_cipherAlgorithmType_3(),
	CipherSuite_t580420223::get_offset_of_hashAlgorithmType_4(),
	CipherSuite_t580420223::get_offset_of_exchangeAlgorithmType_5(),
	CipherSuite_t580420223::get_offset_of_isExportable_6(),
	CipherSuite_t580420223::get_offset_of_cipherMode_7(),
	CipherSuite_t580420223::get_offset_of_keyMaterialSize_8(),
	CipherSuite_t580420223::get_offset_of_keyBlockSize_9(),
	CipherSuite_t580420223::get_offset_of_expandedKeyMaterialSize_10(),
	CipherSuite_t580420223::get_offset_of_effectiveKeyBits_11(),
	CipherSuite_t580420223::get_offset_of_ivSize_12(),
	CipherSuite_t580420223::get_offset_of_blockSize_13(),
	CipherSuite_t580420223::get_offset_of_context_14(),
	CipherSuite_t580420223::get_offset_of_encryptionAlgorithm_15(),
	CipherSuite_t580420223::get_offset_of_encryptionCipher_16(),
	CipherSuite_t580420223::get_offset_of_decryptionAlgorithm_17(),
	CipherSuite_t580420223::get_offset_of_decryptionCipher_18(),
	CipherSuite_t580420223::get_offset_of_clientHMAC_19(),
	CipherSuite_t580420223::get_offset_of_serverHMAC_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1182 = { sizeof (CipherSuiteCollection_t2487974013), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1182[2] = 
{
	CipherSuiteCollection_t2487974013::get_offset_of_cipherSuites_0(),
	CipherSuiteCollection_t2487974013::get_offset_of_protocol_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1183 = { sizeof (CipherSuiteFactory_t311944379), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1184 = { sizeof (ClientContext_t3745725366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1184[2] = 
{
	ClientContext_t3745725366::get_offset_of_sslStream_30(),
	ClientContext_t3745725366::get_offset_of_clientHelloProtocol_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1185 = { sizeof (ClientRecordProtocol_t3260910930), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1186 = { sizeof (ClientSessionInfo_t1380030187), -1, sizeof(ClientSessionInfo_t1380030187_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1186[6] = 
{
	ClientSessionInfo_t1380030187_StaticFields::get_offset_of_ValidityInterval_0(),
	ClientSessionInfo_t1380030187::get_offset_of_disposed_1(),
	ClientSessionInfo_t1380030187::get_offset_of_validuntil_2(),
	ClientSessionInfo_t1380030187::get_offset_of_host_3(),
	ClientSessionInfo_t1380030187::get_offset_of_sid_4(),
	ClientSessionInfo_t1380030187::get_offset_of_masterSecret_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1187 = { sizeof (ClientSessionCache_t1861128725), -1, sizeof(ClientSessionCache_t1861128725_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1187[2] = 
{
	ClientSessionCache_t1861128725_StaticFields::get_offset_of_cache_0(),
	ClientSessionCache_t1861128725_StaticFields::get_offset_of_locker_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1188 = { sizeof (ContentType_t2370127397)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1188[5] = 
{
	ContentType_t2370127397::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1189 = { sizeof (Context_t658806145), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1189[30] = 
{
	Context_t658806145::get_offset_of_securityProtocol_0(),
	Context_t658806145::get_offset_of_sessionId_1(),
	Context_t658806145::get_offset_of_compressionMethod_2(),
	Context_t658806145::get_offset_of_serverSettings_3(),
	Context_t658806145::get_offset_of_clientSettings_4(),
	Context_t658806145::get_offset_of_current_5(),
	Context_t658806145::get_offset_of_negotiating_6(),
	Context_t658806145::get_offset_of_read_7(),
	Context_t658806145::get_offset_of_write_8(),
	Context_t658806145::get_offset_of_supportedCiphers_9(),
	Context_t658806145::get_offset_of_lastHandshakeMsg_10(),
	Context_t658806145::get_offset_of_handshakeState_11(),
	Context_t658806145::get_offset_of_abbreviatedHandshake_12(),
	Context_t658806145::get_offset_of_receivedConnectionEnd_13(),
	Context_t658806145::get_offset_of_sentConnectionEnd_14(),
	Context_t658806145::get_offset_of_protocolNegotiated_15(),
	Context_t658806145::get_offset_of_writeSequenceNumber_16(),
	Context_t658806145::get_offset_of_readSequenceNumber_17(),
	Context_t658806145::get_offset_of_clientRandom_18(),
	Context_t658806145::get_offset_of_serverRandom_19(),
	Context_t658806145::get_offset_of_randomCS_20(),
	Context_t658806145::get_offset_of_randomSC_21(),
	Context_t658806145::get_offset_of_masterSecret_22(),
	Context_t658806145::get_offset_of_clientWriteKey_23(),
	Context_t658806145::get_offset_of_serverWriteKey_24(),
	Context_t658806145::get_offset_of_clientWriteIV_25(),
	Context_t658806145::get_offset_of_serverWriteIV_26(),
	Context_t658806145::get_offset_of_handshakeMessages_27(),
	Context_t658806145::get_offset_of_random_28(),
	Context_t658806145::get_offset_of_recordProtocol_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1190 = { sizeof (ExchangeAlgorithmType_t3271961912)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1190[6] = 
{
	ExchangeAlgorithmType_t3271961912::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1191 = { sizeof (HandshakeState_t1470195192)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1191[4] = 
{
	HandshakeState_t1470195192::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1192 = { sizeof (HashAlgorithmType_t4118445357)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1192[4] = 
{
	HashAlgorithmType_t4118445357::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1193 = { sizeof (HttpsClientStream_t25214728), -1, sizeof(HttpsClientStream_t25214728_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1193[4] = 
{
	HttpsClientStream_t25214728::get_offset_of__request_20(),
	HttpsClientStream_t25214728::get_offset_of__status_21(),
	HttpsClientStream_t25214728_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_22(),
	HttpsClientStream_t25214728_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1194 = { sizeof (RecordProtocol_t1331996967), -1, sizeof(RecordProtocol_t1331996967_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1194[3] = 
{
	RecordProtocol_t1331996967_StaticFields::get_offset_of_record_processing_0(),
	RecordProtocol_t1331996967::get_offset_of_innerStream_1(),
	RecordProtocol_t1331996967::get_offset_of_context_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1195 = { sizeof (ReceiveRecordAsyncResult_t4254633253), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1195[9] = 
{
	ReceiveRecordAsyncResult_t4254633253::get_offset_of_locker_0(),
	ReceiveRecordAsyncResult_t4254633253::get_offset_of__userCallback_1(),
	ReceiveRecordAsyncResult_t4254633253::get_offset_of__userState_2(),
	ReceiveRecordAsyncResult_t4254633253::get_offset_of__asyncException_3(),
	ReceiveRecordAsyncResult_t4254633253::get_offset_of_handle_4(),
	ReceiveRecordAsyncResult_t4254633253::get_offset_of__resultingBuffer_5(),
	ReceiveRecordAsyncResult_t4254633253::get_offset_of__record_6(),
	ReceiveRecordAsyncResult_t4254633253::get_offset_of_completed_7(),
	ReceiveRecordAsyncResult_t4254633253::get_offset_of__initialBuffer_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1196 = { sizeof (SendRecordAsyncResult_t3836770528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1196[7] = 
{
	SendRecordAsyncResult_t3836770528::get_offset_of_locker_0(),
	SendRecordAsyncResult_t3836770528::get_offset_of__userCallback_1(),
	SendRecordAsyncResult_t3836770528::get_offset_of__userState_2(),
	SendRecordAsyncResult_t3836770528::get_offset_of__asyncException_3(),
	SendRecordAsyncResult_t3836770528::get_offset_of_handle_4(),
	SendRecordAsyncResult_t3836770528::get_offset_of__message_5(),
	SendRecordAsyncResult_t3836770528::get_offset_of_completed_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1197 = { sizeof (RSASslSignatureDeformatter_t654537083), -1, sizeof(RSASslSignatureDeformatter_t654537083_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1197[3] = 
{
	RSASslSignatureDeformatter_t654537083::get_offset_of_key_0(),
	RSASslSignatureDeformatter_t654537083::get_offset_of_hash_1(),
	RSASslSignatureDeformatter_t654537083_StaticFields::get_offset_of_U3CU3Ef__switchU24map15_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1198 = { sizeof (RSASslSignatureFormatter_t1731244892), -1, sizeof(RSASslSignatureFormatter_t1731244892_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1198[3] = 
{
	RSASslSignatureFormatter_t1731244892::get_offset_of_key_0(),
	RSASslSignatureFormatter_t1731244892::get_offset_of_hash_1(),
	RSASslSignatureFormatter_t1731244892_StaticFields::get_offset_of_U3CU3Ef__switchU24map16_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1199 = { sizeof (SecurityCompressionType_t131120690)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1199[3] = 
{
	SecurityCompressionType_t131120690::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
