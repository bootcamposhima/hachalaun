﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// East
struct East_t2152477;

#include "codegen/il2cpp-codegen.h"

// System.Void East::.ctor()
extern "C"  void East__ctor_m3307438878 (East_t2152477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void East::Start()
extern "C"  void East_Start_m2254576670 (East_t2152477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void East::Awake()
extern "C"  void East_Awake_m3545044097 (East_t2152477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void East::Update()
extern "C"  void East_Update_m1178252207 (East_t2152477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void East::FixedUpdate()
extern "C"  void East_FixedUpdate_m4164555289 (East_t2152477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void East::MoveSatrtPosition()
extern "C"  void East_MoveSatrtPosition_m259894620 (East_t2152477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void East::MoveStart()
extern "C"  void East_MoveStart_m1699390861 (East_t2152477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void East::ChangeMove()
extern "C"  void East_ChangeMove_m4077581831 (East_t2152477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void East::setCoordinate()
extern "C"  void East_setCoordinate_m1361463894 (East_t2152477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void East::Shot()
extern "C"  void East_Shot_m754799328 (East_t2152477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void East::Move()
extern "C"  void East_Move_m589683191 (East_t2152477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void East::ShotParticle()
extern "C"  void East_ShotParticle_m357156102 (East_t2152477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
