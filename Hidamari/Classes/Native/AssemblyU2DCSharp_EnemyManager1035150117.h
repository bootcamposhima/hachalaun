﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_AIDatas4008896193.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyManager
struct  EnemyManager_t1035150117  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject EnemyManager::Round
	GameObject_t3674682005 * ___Round_2;
	// System.Int32 EnemyManager::AIEndNum
	int32_t ___AIEndNum_3;
	// AIDatas EnemyManager::_ai
	AIDatas_t4008896193  ____ai_4;
	// System.Single EnemyManager::time
	float ___time_5;
	// System.Boolean EnemyManager::isDemo
	bool ___isDemo_6;
	// System.Boolean EnemyManager::isPlay
	bool ___isPlay_7;

public:
	inline static int32_t get_offset_of_Round_2() { return static_cast<int32_t>(offsetof(EnemyManager_t1035150117, ___Round_2)); }
	inline GameObject_t3674682005 * get_Round_2() const { return ___Round_2; }
	inline GameObject_t3674682005 ** get_address_of_Round_2() { return &___Round_2; }
	inline void set_Round_2(GameObject_t3674682005 * value)
	{
		___Round_2 = value;
		Il2CppCodeGenWriteBarrier(&___Round_2, value);
	}

	inline static int32_t get_offset_of_AIEndNum_3() { return static_cast<int32_t>(offsetof(EnemyManager_t1035150117, ___AIEndNum_3)); }
	inline int32_t get_AIEndNum_3() const { return ___AIEndNum_3; }
	inline int32_t* get_address_of_AIEndNum_3() { return &___AIEndNum_3; }
	inline void set_AIEndNum_3(int32_t value)
	{
		___AIEndNum_3 = value;
	}

	inline static int32_t get_offset_of__ai_4() { return static_cast<int32_t>(offsetof(EnemyManager_t1035150117, ____ai_4)); }
	inline AIDatas_t4008896193  get__ai_4() const { return ____ai_4; }
	inline AIDatas_t4008896193 * get_address_of__ai_4() { return &____ai_4; }
	inline void set__ai_4(AIDatas_t4008896193  value)
	{
		____ai_4 = value;
	}

	inline static int32_t get_offset_of_time_5() { return static_cast<int32_t>(offsetof(EnemyManager_t1035150117, ___time_5)); }
	inline float get_time_5() const { return ___time_5; }
	inline float* get_address_of_time_5() { return &___time_5; }
	inline void set_time_5(float value)
	{
		___time_5 = value;
	}

	inline static int32_t get_offset_of_isDemo_6() { return static_cast<int32_t>(offsetof(EnemyManager_t1035150117, ___isDemo_6)); }
	inline bool get_isDemo_6() const { return ___isDemo_6; }
	inline bool* get_address_of_isDemo_6() { return &___isDemo_6; }
	inline void set_isDemo_6(bool value)
	{
		___isDemo_6 = value;
	}

	inline static int32_t get_offset_of_isPlay_7() { return static_cast<int32_t>(offsetof(EnemyManager_t1035150117, ___isPlay_7)); }
	inline bool get_isPlay_7() const { return ___isPlay_7; }
	inline bool* get_address_of_isPlay_7() { return &___isPlay_7; }
	inline void set_isPlay_7(bool value)
	{
		___isPlay_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
