﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TestJson
struct TestJson_t3212538042;
struct TestJson_t3212538042_marshaled_pinvoke;
struct TestJson_t3212538042_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TestJson3212538042.h"

// System.Void TestJson::.ctor(System.Single)
extern "C"  void TestJson__ctor_m2753617962 (TestJson_t3212538042 * __this, float ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct TestJson_t3212538042;
struct TestJson_t3212538042_marshaled_pinvoke;

extern "C" void TestJson_t3212538042_marshal_pinvoke(const TestJson_t3212538042& unmarshaled, TestJson_t3212538042_marshaled_pinvoke& marshaled);
extern "C" void TestJson_t3212538042_marshal_pinvoke_back(const TestJson_t3212538042_marshaled_pinvoke& marshaled, TestJson_t3212538042& unmarshaled);
extern "C" void TestJson_t3212538042_marshal_pinvoke_cleanup(TestJson_t3212538042_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct TestJson_t3212538042;
struct TestJson_t3212538042_marshaled_com;

extern "C" void TestJson_t3212538042_marshal_com(const TestJson_t3212538042& unmarshaled, TestJson_t3212538042_marshaled_com& marshaled);
extern "C" void TestJson_t3212538042_marshal_com_back(const TestJson_t3212538042_marshaled_com& marshaled, TestJson_t3212538042& unmarshaled);
extern "C" void TestJson_t3212538042_marshal_com_cleanup(TestJson_t3212538042_marshaled_com& marshaled);
