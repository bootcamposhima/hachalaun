﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Round
struct Round_t79151470;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void Round::.ctor()
extern "C"  void Round__ctor_m3606430653 (Round_t79151470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Round::Start()
extern "C"  void Round_Start_m2553568445 (Round_t79151470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Round::Update()
extern "C"  void Round_Update_m1857062640 (Round_t79151470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Round::ShowGameResult()
extern "C"  void Round_ShowGameResult_m211617011 (Round_t79151470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Round::ChangeScene()
extern "C"  void Round_ChangeScene_m3967123735 (Round_t79151470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Round::ShowRoundText()
extern "C"  void Round_ShowRoundText_m2918847673 (Round_t79151470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Round::RoundTextAnimation()
extern "C"  Il2CppObject * Round_RoundTextAnimation_m526376904 (Round_t79151470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
