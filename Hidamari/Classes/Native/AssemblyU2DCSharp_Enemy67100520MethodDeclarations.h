﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Enemy
struct Enemy_t67100520;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AIData1930434546.h"

// System.Void Enemy::.ctor()
extern "C"  void Enemy__ctor_m1781972739 (Enemy_t67100520 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Enemy::set_Aidata(AIData)
extern "C"  void Enemy_set_Aidata_m1714486654 (Enemy_t67100520 * __this, AIData_t1930434546  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Enemy::StartAction()
extern "C"  void Enemy_StartAction_m2950347609 (Enemy_t67100520 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Enemy::StartParticle()
extern "C"  void Enemy_StartParticle_m3074946857 (Enemy_t67100520 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Enemy::EndActionCol()
extern "C"  Il2CppObject * Enemy_EndActionCol_m2284461480 (Enemy_t67100520 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Enemy::EndParticle(System.Int32)
extern "C"  void Enemy_EndParticle_m975024819 (Enemy_t67100520 * __this, int32_t ___nun0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Enemy::StopShotPar()
extern "C"  void Enemy_StopShotPar_m464081862 (Enemy_t67100520 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Enemy::MoveStop()
extern "C"  void Enemy_MoveStop_m1327917204 (Enemy_t67100520 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Enemy::MoveReStart()
extern "C"  void Enemy_MoveReStart_m1561909631 (Enemy_t67100520 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Enemy::EndAction()
extern "C"  void Enemy_EndAction_m1070415570 (Enemy_t67100520 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
