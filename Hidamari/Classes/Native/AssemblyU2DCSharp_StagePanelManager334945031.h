﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t747900261;
// System.Collections.Generic.List`1<System.Collections.IEnumerator>
struct List_1_t537793463;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StagePanelManager
struct  StagePanelManager_t334945031  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject StagePanelManager::_stagepanel
	GameObject_t3674682005 * ____stagepanel_2;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> StagePanelManager::_listobjects
	List_1_t747900261 * ____listobjects_3;
	// System.Collections.Generic.List`1<System.Collections.IEnumerator> StagePanelManager::_listienumerator
	List_1_t537793463 * ____listienumerator_4;
	// System.Boolean StagePanelManager::isstop
	bool ___isstop_5;

public:
	inline static int32_t get_offset_of__stagepanel_2() { return static_cast<int32_t>(offsetof(StagePanelManager_t334945031, ____stagepanel_2)); }
	inline GameObject_t3674682005 * get__stagepanel_2() const { return ____stagepanel_2; }
	inline GameObject_t3674682005 ** get_address_of__stagepanel_2() { return &____stagepanel_2; }
	inline void set__stagepanel_2(GameObject_t3674682005 * value)
	{
		____stagepanel_2 = value;
		Il2CppCodeGenWriteBarrier(&____stagepanel_2, value);
	}

	inline static int32_t get_offset_of__listobjects_3() { return static_cast<int32_t>(offsetof(StagePanelManager_t334945031, ____listobjects_3)); }
	inline List_1_t747900261 * get__listobjects_3() const { return ____listobjects_3; }
	inline List_1_t747900261 ** get_address_of__listobjects_3() { return &____listobjects_3; }
	inline void set__listobjects_3(List_1_t747900261 * value)
	{
		____listobjects_3 = value;
		Il2CppCodeGenWriteBarrier(&____listobjects_3, value);
	}

	inline static int32_t get_offset_of__listienumerator_4() { return static_cast<int32_t>(offsetof(StagePanelManager_t334945031, ____listienumerator_4)); }
	inline List_1_t537793463 * get__listienumerator_4() const { return ____listienumerator_4; }
	inline List_1_t537793463 ** get_address_of__listienumerator_4() { return &____listienumerator_4; }
	inline void set__listienumerator_4(List_1_t537793463 * value)
	{
		____listienumerator_4 = value;
		Il2CppCodeGenWriteBarrier(&____listienumerator_4, value);
	}

	inline static int32_t get_offset_of_isstop_5() { return static_cast<int32_t>(offsetof(StagePanelManager_t334945031, ___isstop_5)); }
	inline bool get_isstop_5() const { return ___isstop_5; }
	inline bool* get_address_of_isstop_5() { return &___isstop_5; }
	inline void set_isstop_5(bool value)
	{
		___isstop_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
