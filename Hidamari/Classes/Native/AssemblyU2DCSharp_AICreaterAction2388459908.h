﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// TransitionAnimation
struct TransitionAnimation_t4286760335;
// ActionManager
struct ActionManager_t3022458999;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AICreaterAction
struct  AICreaterAction_t2388459908  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject AICreaterAction::_formationButton
	GameObject_t3674682005 * ____formationButton_2;
	// UnityEngine.GameObject AICreaterAction::_arrows
	GameObject_t3674682005 * ____arrows_3;
	// TransitionAnimation AICreaterAction::_ta
	TransitionAnimation_t4286760335 * ____ta_4;
	// ActionManager AICreaterAction::_am
	ActionManager_t3022458999 * ____am_5;

public:
	inline static int32_t get_offset_of__formationButton_2() { return static_cast<int32_t>(offsetof(AICreaterAction_t2388459908, ____formationButton_2)); }
	inline GameObject_t3674682005 * get__formationButton_2() const { return ____formationButton_2; }
	inline GameObject_t3674682005 ** get_address_of__formationButton_2() { return &____formationButton_2; }
	inline void set__formationButton_2(GameObject_t3674682005 * value)
	{
		____formationButton_2 = value;
		Il2CppCodeGenWriteBarrier(&____formationButton_2, value);
	}

	inline static int32_t get_offset_of__arrows_3() { return static_cast<int32_t>(offsetof(AICreaterAction_t2388459908, ____arrows_3)); }
	inline GameObject_t3674682005 * get__arrows_3() const { return ____arrows_3; }
	inline GameObject_t3674682005 ** get_address_of__arrows_3() { return &____arrows_3; }
	inline void set__arrows_3(GameObject_t3674682005 * value)
	{
		____arrows_3 = value;
		Il2CppCodeGenWriteBarrier(&____arrows_3, value);
	}

	inline static int32_t get_offset_of__ta_4() { return static_cast<int32_t>(offsetof(AICreaterAction_t2388459908, ____ta_4)); }
	inline TransitionAnimation_t4286760335 * get__ta_4() const { return ____ta_4; }
	inline TransitionAnimation_t4286760335 ** get_address_of__ta_4() { return &____ta_4; }
	inline void set__ta_4(TransitionAnimation_t4286760335 * value)
	{
		____ta_4 = value;
		Il2CppCodeGenWriteBarrier(&____ta_4, value);
	}

	inline static int32_t get_offset_of__am_5() { return static_cast<int32_t>(offsetof(AICreaterAction_t2388459908, ____am_5)); }
	inline ActionManager_t3022458999 * get__am_5() const { return ____am_5; }
	inline ActionManager_t3022458999 ** get_address_of__am_5() { return &____am_5; }
	inline void set__am_5(ActionManager_t3022458999 * value)
	{
		____am_5 = value;
		Il2CppCodeGenWriteBarrier(&____am_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
