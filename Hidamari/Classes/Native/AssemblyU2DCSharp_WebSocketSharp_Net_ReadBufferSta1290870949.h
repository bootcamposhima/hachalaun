﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// WebSocketSharp.Net.HttpStreamAsyncResult
struct HttpStreamAsyncResult_t303264603;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.ReadBufferState
struct  ReadBufferState_t1290870949  : public Il2CppObject
{
public:
	// WebSocketSharp.Net.HttpStreamAsyncResult WebSocketSharp.Net.ReadBufferState::<AsyncResult>k__BackingField
	HttpStreamAsyncResult_t303264603 * ___U3CAsyncResultU3Ek__BackingField_0;
	// System.Byte[] WebSocketSharp.Net.ReadBufferState::<Buffer>k__BackingField
	ByteU5BU5D_t4260760469* ___U3CBufferU3Ek__BackingField_1;
	// System.Int32 WebSocketSharp.Net.ReadBufferState::<Count>k__BackingField
	int32_t ___U3CCountU3Ek__BackingField_2;
	// System.Int32 WebSocketSharp.Net.ReadBufferState::<InitialCount>k__BackingField
	int32_t ___U3CInitialCountU3Ek__BackingField_3;
	// System.Int32 WebSocketSharp.Net.ReadBufferState::<Offset>k__BackingField
	int32_t ___U3COffsetU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CAsyncResultU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ReadBufferState_t1290870949, ___U3CAsyncResultU3Ek__BackingField_0)); }
	inline HttpStreamAsyncResult_t303264603 * get_U3CAsyncResultU3Ek__BackingField_0() const { return ___U3CAsyncResultU3Ek__BackingField_0; }
	inline HttpStreamAsyncResult_t303264603 ** get_address_of_U3CAsyncResultU3Ek__BackingField_0() { return &___U3CAsyncResultU3Ek__BackingField_0; }
	inline void set_U3CAsyncResultU3Ek__BackingField_0(HttpStreamAsyncResult_t303264603 * value)
	{
		___U3CAsyncResultU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAsyncResultU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CBufferU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ReadBufferState_t1290870949, ___U3CBufferU3Ek__BackingField_1)); }
	inline ByteU5BU5D_t4260760469* get_U3CBufferU3Ek__BackingField_1() const { return ___U3CBufferU3Ek__BackingField_1; }
	inline ByteU5BU5D_t4260760469** get_address_of_U3CBufferU3Ek__BackingField_1() { return &___U3CBufferU3Ek__BackingField_1; }
	inline void set_U3CBufferU3Ek__BackingField_1(ByteU5BU5D_t4260760469* value)
	{
		___U3CBufferU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CBufferU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CCountU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ReadBufferState_t1290870949, ___U3CCountU3Ek__BackingField_2)); }
	inline int32_t get_U3CCountU3Ek__BackingField_2() const { return ___U3CCountU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CCountU3Ek__BackingField_2() { return &___U3CCountU3Ek__BackingField_2; }
	inline void set_U3CCountU3Ek__BackingField_2(int32_t value)
	{
		___U3CCountU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CInitialCountU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ReadBufferState_t1290870949, ___U3CInitialCountU3Ek__BackingField_3)); }
	inline int32_t get_U3CInitialCountU3Ek__BackingField_3() const { return ___U3CInitialCountU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CInitialCountU3Ek__BackingField_3() { return &___U3CInitialCountU3Ek__BackingField_3; }
	inline void set_U3CInitialCountU3Ek__BackingField_3(int32_t value)
	{
		___U3CInitialCountU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3COffsetU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ReadBufferState_t1290870949, ___U3COffsetU3Ek__BackingField_4)); }
	inline int32_t get_U3COffsetU3Ek__BackingField_4() const { return ___U3COffsetU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3COffsetU3Ek__BackingField_4() { return &___U3COffsetU3Ek__BackingField_4; }
	inline void set_U3COffsetU3Ek__BackingField_4(int32_t value)
	{
		___U3COffsetU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
