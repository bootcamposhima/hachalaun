﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MessageJson
struct MessageJson_t794471791;
struct MessageJson_t794471791_marshaled_pinvoke;
struct MessageJson_t794471791_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MessageJson794471791.h"
#include "AssemblyU2DCSharp_SummarizeJson970273193.h"

// System.Void MessageJson::.ctor(SummarizeJson)
extern "C"  void MessageJson__ctor_m3061826509 (MessageJson_t794471791 * __this, SummarizeJson_t970273193  ___j0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct MessageJson_t794471791;
struct MessageJson_t794471791_marshaled_pinvoke;

extern "C" void MessageJson_t794471791_marshal_pinvoke(const MessageJson_t794471791& unmarshaled, MessageJson_t794471791_marshaled_pinvoke& marshaled);
extern "C" void MessageJson_t794471791_marshal_pinvoke_back(const MessageJson_t794471791_marshaled_pinvoke& marshaled, MessageJson_t794471791& unmarshaled);
extern "C" void MessageJson_t794471791_marshal_pinvoke_cleanup(MessageJson_t794471791_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct MessageJson_t794471791;
struct MessageJson_t794471791_marshaled_com;

extern "C" void MessageJson_t794471791_marshal_com(const MessageJson_t794471791& unmarshaled, MessageJson_t794471791_marshaled_com& marshaled);
extern "C" void MessageJson_t794471791_marshal_com_back(const MessageJson_t794471791_marshaled_com& marshaled, MessageJson_t794471791& unmarshaled);
extern "C" void MessageJson_t794471791_marshal_com_cleanup(MessageJson_t794471791_marshaled_com& marshaled);
