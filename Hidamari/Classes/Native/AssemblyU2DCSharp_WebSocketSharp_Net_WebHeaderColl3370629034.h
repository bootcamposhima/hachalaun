﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Text.StringBuilder
struct StringBuilder_t243639308;
// WebSocketSharp.Net.WebHeaderCollection
struct WebHeaderCollection_t288332393;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.WebHeaderCollection/<ToString>c__AnonStorey27
struct  U3CToStringU3Ec__AnonStorey27_t3370629034  : public Il2CppObject
{
public:
	// System.Text.StringBuilder WebSocketSharp.Net.WebHeaderCollection/<ToString>c__AnonStorey27::buff
	StringBuilder_t243639308 * ___buff_0;
	// WebSocketSharp.Net.WebHeaderCollection WebSocketSharp.Net.WebHeaderCollection/<ToString>c__AnonStorey27::<>f__this
	WebHeaderCollection_t288332393 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_buff_0() { return static_cast<int32_t>(offsetof(U3CToStringU3Ec__AnonStorey27_t3370629034, ___buff_0)); }
	inline StringBuilder_t243639308 * get_buff_0() const { return ___buff_0; }
	inline StringBuilder_t243639308 ** get_address_of_buff_0() { return &___buff_0; }
	inline void set_buff_0(StringBuilder_t243639308 * value)
	{
		___buff_0 = value;
		Il2CppCodeGenWriteBarrier(&___buff_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CToStringU3Ec__AnonStorey27_t3370629034, ___U3CU3Ef__this_1)); }
	inline WebHeaderCollection_t288332393 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline WebHeaderCollection_t288332393 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(WebHeaderCollection_t288332393 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
