﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3363211663MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m421465117(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2666886567 *, Dictionary_2_t1349563175 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m368373604(__this, method) ((  Il2CppObject * (*) (Enumerator_t2666886567 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3909782648(__this, method) ((  void (*) (Enumerator_t2666886567 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m201566401(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t2666886567 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m935596352(__this, method) ((  Il2CppObject * (*) (Enumerator_t2666886567 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2508365778(__this, method) ((  Il2CppObject * (*) (Enumerator_t2666886567 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::MoveNext()
#define Enumerator_MoveNext_m885328356(__this, method) ((  bool (*) (Enumerator_t2666886567 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::get_Current()
#define Enumerator_get_Current_m2832651084(__this, method) ((  KeyValuePair_2_t1248343881  (*) (Enumerator_t2666886567 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2913445425(__this, method) ((  HttpListenerContext_t3744659101 * (*) (Enumerator_t2666886567 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1591712277(__this, method) ((  HttpListenerContext_t3744659101 * (*) (Enumerator_t2666886567 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::Reset()
#define Enumerator_Reset_m3966552815(__this, method) ((  void (*) (Enumerator_t2666886567 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::VerifyState()
#define Enumerator_VerifyState_m724691896(__this, method) ((  void (*) (Enumerator_t2666886567 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3389081888(__this, method) ((  void (*) (Enumerator_t2666886567 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::Dispose()
#define Enumerator_Dispose_m449725439(__this, method) ((  void (*) (Enumerator_t2666886567 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
