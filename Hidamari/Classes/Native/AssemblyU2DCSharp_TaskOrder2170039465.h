﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Condition
struct Condition_t1142656251;
// System.Collections.Generic.List`1<Task>
struct List_1_t1370784885;
// BlackBoard
struct BlackBoard_t328561223;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TaskOrder
struct  TaskOrder_t2170039465  : public MonoBehaviour_t667441552
{
public:
	// System.Int32 TaskOrder::_taskorder
	int32_t ____taskorder_2;
	// Condition TaskOrder::_condition
	Condition_t1142656251 * ____condition_3;
	// System.Collections.Generic.List`1<Task> TaskOrder::_tasks
	List_1_t1370784885 * ____tasks_4;
	// System.Boolean TaskOrder::issequence
	bool ___issequence_5;
	// System.Boolean TaskOrder::iscoroutines
	bool ___iscoroutines_6;
	// BlackBoard TaskOrder::bb
	BlackBoard_t328561223 * ___bb_7;

public:
	inline static int32_t get_offset_of__taskorder_2() { return static_cast<int32_t>(offsetof(TaskOrder_t2170039465, ____taskorder_2)); }
	inline int32_t get__taskorder_2() const { return ____taskorder_2; }
	inline int32_t* get_address_of__taskorder_2() { return &____taskorder_2; }
	inline void set__taskorder_2(int32_t value)
	{
		____taskorder_2 = value;
	}

	inline static int32_t get_offset_of__condition_3() { return static_cast<int32_t>(offsetof(TaskOrder_t2170039465, ____condition_3)); }
	inline Condition_t1142656251 * get__condition_3() const { return ____condition_3; }
	inline Condition_t1142656251 ** get_address_of__condition_3() { return &____condition_3; }
	inline void set__condition_3(Condition_t1142656251 * value)
	{
		____condition_3 = value;
		Il2CppCodeGenWriteBarrier(&____condition_3, value);
	}

	inline static int32_t get_offset_of__tasks_4() { return static_cast<int32_t>(offsetof(TaskOrder_t2170039465, ____tasks_4)); }
	inline List_1_t1370784885 * get__tasks_4() const { return ____tasks_4; }
	inline List_1_t1370784885 ** get_address_of__tasks_4() { return &____tasks_4; }
	inline void set__tasks_4(List_1_t1370784885 * value)
	{
		____tasks_4 = value;
		Il2CppCodeGenWriteBarrier(&____tasks_4, value);
	}

	inline static int32_t get_offset_of_issequence_5() { return static_cast<int32_t>(offsetof(TaskOrder_t2170039465, ___issequence_5)); }
	inline bool get_issequence_5() const { return ___issequence_5; }
	inline bool* get_address_of_issequence_5() { return &___issequence_5; }
	inline void set_issequence_5(bool value)
	{
		___issequence_5 = value;
	}

	inline static int32_t get_offset_of_iscoroutines_6() { return static_cast<int32_t>(offsetof(TaskOrder_t2170039465, ___iscoroutines_6)); }
	inline bool get_iscoroutines_6() const { return ___iscoroutines_6; }
	inline bool* get_address_of_iscoroutines_6() { return &___iscoroutines_6; }
	inline void set_iscoroutines_6(bool value)
	{
		___iscoroutines_6 = value;
	}

	inline static int32_t get_offset_of_bb_7() { return static_cast<int32_t>(offsetof(TaskOrder_t2170039465, ___bb_7)); }
	inline BlackBoard_t328561223 * get_bb_7() const { return ___bb_7; }
	inline BlackBoard_t328561223 ** get_address_of_bb_7() { return &___bb_7; }
	inline void set_bb_7(BlackBoard_t328561223 * value)
	{
		___bb_7 = value;
		Il2CppCodeGenWriteBarrier(&___bb_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
