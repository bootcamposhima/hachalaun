﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2660824325MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3132504175(__this, ___host0, method) ((  void (*) (Enumerator_t1964499229 *, Dictionary_2_t1349563175 *, const MethodInfo*))Enumerator__ctor_m2661607283_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3651564050(__this, method) ((  Il2CppObject * (*) (Enumerator_t1964499229 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2640325710_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m113512678(__this, method) ((  void (*) (Enumerator_t1964499229 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1606518626_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::Dispose()
#define Enumerator_Dispose_m3485468881(__this, method) ((  void (*) (Enumerator_t1964499229 *, const MethodInfo*))Enumerator_Dispose_m2264940757_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::MoveNext()
#define Enumerator_MoveNext_m128228050(__this, method) ((  bool (*) (Enumerator_t1964499229 *, const MethodInfo*))Enumerator_MoveNext_m3041849038_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>::get_Current()
#define Enumerator_get_Current_m1388017602(__this, method) ((  HttpListenerContext_t3744659101 * (*) (Enumerator_t1964499229 *, const MethodInfo*))Enumerator_get_Current_m3451690438_gshared)(__this, method)
