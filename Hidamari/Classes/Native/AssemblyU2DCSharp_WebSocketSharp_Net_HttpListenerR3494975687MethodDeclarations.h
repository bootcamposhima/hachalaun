﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator1B
struct U3CfindCookieU3Ec__Iterator1B_t3494975687;
// WebSocketSharp.Net.Cookie
struct Cookie_t2077085446;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<WebSocketSharp.Net.Cookie>
struct IEnumerator_1_t3988950495;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator1B::.ctor()
extern "C"  void U3CfindCookieU3Ec__Iterator1B__ctor_m1924142212 (U3CfindCookieU3Ec__Iterator1B_t3494975687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.Net.Cookie WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator1B::System.Collections.Generic.IEnumerator<WebSocketSharp.Net.Cookie>.get_Current()
extern "C"  Cookie_t2077085446 * U3CfindCookieU3Ec__Iterator1B_System_Collections_Generic_IEnumeratorU3CWebSocketSharp_Net_CookieU3E_get_Current_m418084211 (U3CfindCookieU3Ec__Iterator1B_t3494975687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator1B::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CfindCookieU3Ec__Iterator1B_System_Collections_IEnumerator_get_Current_m1381101090 (U3CfindCookieU3Ec__Iterator1B_t3494975687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator1B::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CfindCookieU3Ec__Iterator1B_System_Collections_IEnumerable_GetEnumerator_m1281096797 (U3CfindCookieU3Ec__Iterator1B_t3494975687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<WebSocketSharp.Net.Cookie> WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator1B::System.Collections.Generic.IEnumerable<WebSocketSharp.Net.Cookie>.GetEnumerator()
extern "C"  Il2CppObject* U3CfindCookieU3Ec__Iterator1B_System_Collections_Generic_IEnumerableU3CWebSocketSharp_Net_CookieU3E_GetEnumerator_m153117710 (U3CfindCookieU3Ec__Iterator1B_t3494975687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator1B::MoveNext()
extern "C"  bool U3CfindCookieU3Ec__Iterator1B_MoveNext_m4166146032 (U3CfindCookieU3Ec__Iterator1B_t3494975687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator1B::Dispose()
extern "C"  void U3CfindCookieU3Ec__Iterator1B_Dispose_m2163011521 (U3CfindCookieU3Ec__Iterator1B_t3494975687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator1B::Reset()
extern "C"  void U3CfindCookieU3Ec__Iterator1B_Reset_m3865542449 (U3CfindCookieU3Ec__Iterator1B_t3494975687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
