﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MoveJson
struct MoveJson_t4254904441;
struct MoveJson_t4254904441_marshaled_pinvoke;
struct MoveJson_t4254904441_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MoveJson4254904441.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void MoveJson::.ctor(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,System.Single)
extern "C"  void MoveJson__ctor_m130281037 (MoveJson_t4254904441 * __this, Vector3_t4282066566  ___pos0, Vector3_t4282066566  ___mp1, float ___cspd2, int32_t ___ms3, float ___t4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveJson::.ctor(System.Int32)
extern "C"  void MoveJson__ctor_m1755113619 (MoveJson_t4254904441 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct MoveJson_t4254904441;
struct MoveJson_t4254904441_marshaled_pinvoke;

extern "C" void MoveJson_t4254904441_marshal_pinvoke(const MoveJson_t4254904441& unmarshaled, MoveJson_t4254904441_marshaled_pinvoke& marshaled);
extern "C" void MoveJson_t4254904441_marshal_pinvoke_back(const MoveJson_t4254904441_marshaled_pinvoke& marshaled, MoveJson_t4254904441& unmarshaled);
extern "C" void MoveJson_t4254904441_marshal_pinvoke_cleanup(MoveJson_t4254904441_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct MoveJson_t4254904441;
struct MoveJson_t4254904441_marshaled_com;

extern "C" void MoveJson_t4254904441_marshal_com(const MoveJson_t4254904441& unmarshaled, MoveJson_t4254904441_marshaled_com& marshaled);
extern "C" void MoveJson_t4254904441_marshal_com_back(const MoveJson_t4254904441_marshaled_com& marshaled, MoveJson_t4254904441& unmarshaled);
extern "C" void MoveJson_t4254904441_marshal_com_cleanup(MoveJson_t4254904441_marshaled_com& marshaled);
