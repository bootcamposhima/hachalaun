﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_Condition1142656251.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IsFlag
struct  IsFlag_t2198325206  : public Condition_t1142656251
{
public:
	// System.String IsFlag::str
	String_t* ___str_2;
	// System.Boolean IsFlag::flag
	bool ___flag_3;

public:
	inline static int32_t get_offset_of_str_2() { return static_cast<int32_t>(offsetof(IsFlag_t2198325206, ___str_2)); }
	inline String_t* get_str_2() const { return ___str_2; }
	inline String_t** get_address_of_str_2() { return &___str_2; }
	inline void set_str_2(String_t* value)
	{
		___str_2 = value;
		Il2CppCodeGenWriteBarrier(&___str_2, value);
	}

	inline static int32_t get_offset_of_flag_3() { return static_cast<int32_t>(offsetof(IsFlag_t2198325206, ___flag_3)); }
	inline bool get_flag_3() const { return ___flag_3; }
	inline bool* get_address_of_flag_3() { return &___flag_3; }
	inline void set_flag_3(bool value)
	{
		___flag_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
