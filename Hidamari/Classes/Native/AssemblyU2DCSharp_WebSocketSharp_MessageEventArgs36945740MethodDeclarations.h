﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.MessageEventArgs
struct MessageEventArgs_t36945740;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// WebSocketSharp.PayloadData
struct PayloadData_t39926750;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Opcode3782140426.h"
#include "AssemblyU2DCSharp_WebSocketSharp_PayloadData39926750.h"

// System.Void WebSocketSharp.MessageEventArgs::.ctor(WebSocketSharp.Opcode,System.Byte[])
extern "C"  void MessageEventArgs__ctor_m2265064076 (MessageEventArgs_t36945740 * __this, uint8_t ___opcode0, ByteU5BU5D_t4260760469* ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.MessageEventArgs::.ctor(WebSocketSharp.Opcode,WebSocketSharp.PayloadData)
extern "C"  void MessageEventArgs__ctor_m2365520848 (MessageEventArgs_t36945740 * __this, uint8_t ___opcode0, PayloadData_t39926750 * ___payload1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.MessageEventArgs::get_Data()
extern "C"  String_t* MessageEventArgs_get_Data_m3589934276 (MessageEventArgs_t36945740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] WebSocketSharp.MessageEventArgs::get_RawData()
extern "C"  ByteU5BU5D_t4260760469* MessageEventArgs_get_RawData_m1299359681 (MessageEventArgs_t36945740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.Opcode WebSocketSharp.MessageEventArgs::get_Type()
extern "C"  uint8_t MessageEventArgs_get_Type_m1054457981 (MessageEventArgs_t36945740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.MessageEventArgs::convertToString(WebSocketSharp.Opcode,System.Byte[])
extern "C"  String_t* MessageEventArgs_convertToString_m3383542956 (Il2CppObject * __this /* static, unused */, uint8_t ___opcode0, ByteU5BU5D_t4260760469* ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
