﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Rocket
struct Rocket_t2453156596;

#include "codegen/il2cpp-codegen.h"

// System.Void Rocket::.ctor()
extern "C"  void Rocket__ctor_m3878110247 (Rocket_t2453156596 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Rocket::Start()
extern "C"  void Rocket_Start_m2825248039 (Rocket_t2453156596 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Rocket::Update()
extern "C"  void Rocket_Update_m1689195462 (Rocket_t2453156596 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
