﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Charctor
struct Charctor_t1500651146;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CharData1499709504.h"
#include "AssemblyU2DCSharp_Direction1041377119.h"

// System.Void Charctor::.ctor()
extern "C"  void Charctor__ctor_m2049399633 (Charctor_t1500651146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Charctor::Start()
extern "C"  void Charctor_Start_m996537425 (Charctor_t1500651146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Charctor::Update()
extern "C"  void Charctor_Update_m833741276 (Charctor_t1500651146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Charctor::setCharData(System.Int32)
extern "C"  void Charctor_setCharData_m1414173698 (Charctor_t1500651146 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Charctor::get_IsMove()
extern "C"  bool Charctor_get_IsMove_m3723656747 (Charctor_t1500651146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Charctor::set_IsMove(System.Boolean)
extern "C"  void Charctor_set_IsMove_m2640039202 (Charctor_t1500651146 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Charctor::get_IsPlay()
extern "C"  bool Charctor_get_IsPlay_m3806167246 (Charctor_t1500651146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Charctor::set_IsPlay(System.Boolean)
extern "C"  void Charctor_set_IsPlay_m412087941 (Charctor_t1500651146 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Charctor::setMove()
extern "C"  void Charctor_setMove_m434593698 (Charctor_t1500651146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CharData Charctor::GetCharData(System.Int32)
extern "C"  CharData_t1499709504  Charctor_GetCharData_m2282908217 (Charctor_t1500651146 * __this, int32_t ___datanum0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Charctor::HitBullet(System.Int32,Direction)
extern "C"  void Charctor_HitBullet_m3357329570 (Charctor_t1500651146 * __this, int32_t ___damage0, int32_t ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Charctor::MoveStop()
extern "C"  void Charctor_MoveStop_m1078182278 (Charctor_t1500651146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Charctor::MoveReStart()
extern "C"  void Charctor_MoveReStart_m592085837 (Charctor_t1500651146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Charctor::SetGameSpeed(System.Single)
extern "C"  void Charctor_SetGameSpeed_m4242052389 (Charctor_t1500651146 * __this, float ___speed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Charctor::DestroyMe()
extern "C"  void Charctor_DestroyMe_m3129403425 (Charctor_t1500651146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Charctor::StopPar()
extern "C"  void Charctor_StopPar_m3641139822 (Charctor_t1500651146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Charctor::StopEndPar(System.Int32)
extern "C"  void Charctor_StopEndPar_m600301964 (Charctor_t1500651146 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Charctor::StopMovePar()
extern "C"  void Charctor_StopMovePar_m2551941917 (Charctor_t1500651146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Charctor::EndAction()
extern "C"  void Charctor_EndAction_m1918567456 (Charctor_t1500651146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Charctor::EndActionCol()
extern "C"  Il2CppObject * Charctor_EndActionCol_m2362119466 (Charctor_t1500651146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Charctor::EndParticle(System.Int32)
extern "C"  void Charctor_EndParticle_m2877888385 (Charctor_t1500651146 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Charctor::EndParticle2(System.Int32)
extern "C"  void Charctor_EndParticle2_m4105109685 (Charctor_t1500651146 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
