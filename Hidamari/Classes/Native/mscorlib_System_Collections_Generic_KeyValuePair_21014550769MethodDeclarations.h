﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_924606671MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<WebSocketSharp.CompressionMethod,System.Byte[]>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m263328470(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1014550769 *, uint8_t, ByteU5BU5D_t4260760469*, const MethodInfo*))KeyValuePair_2__ctor_m200737057_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<WebSocketSharp.CompressionMethod,System.Byte[]>::get_Key()
#define KeyValuePair_2_get_Key_m379732050(__this, method) ((  uint8_t (*) (KeyValuePair_2_t1014550769 *, const MethodInfo*))KeyValuePair_2_get_Key_m2143879591_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<WebSocketSharp.CompressionMethod,System.Byte[]>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2860931475(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1014550769 *, uint8_t, const MethodInfo*))KeyValuePair_2_set_Key_m3037882472_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<WebSocketSharp.CompressionMethod,System.Byte[]>::get_Value()
#define KeyValuePair_2_get_Value_m2773187346(__this, method) ((  ByteU5BU5D_t4260760469* (*) (KeyValuePair_2_t1014550769 *, const MethodInfo*))KeyValuePair_2_get_Value_m1606892327_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<WebSocketSharp.CompressionMethod,System.Byte[]>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3659836435(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1014550769 *, ByteU5BU5D_t4260760469*, const MethodInfo*))KeyValuePair_2_set_Value_m2410891368_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<WebSocketSharp.CompressionMethod,System.Byte[]>::ToString()
#define KeyValuePair_2_ToString_m3225673519(__this, method) ((  String_t* (*) (KeyValuePair_2_t1014550769 *, const MethodInfo*))KeyValuePair_2_ToString_m2079672442_gshared)(__this, method)
