﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.HandshakeResponse
struct HandshakeResponse_t3229697822;
// System.Version
struct Version_t763695022;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t2791941106;
// WebSocketSharp.Net.AuthenticationChallenge
struct AuthenticationChallenge_t1782907061;
// WebSocketSharp.Net.CookieCollection
struct CookieCollection_t1136277956;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Version763695022.h"
#include "System_System_Collections_Specialized_NameValueCol2791941106.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_HttpStatusCod1625451593.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_CookieCollect1136277956.h"

// System.Void WebSocketSharp.HandshakeResponse::.ctor(System.Version,System.Collections.Specialized.NameValueCollection)
extern "C"  void HandshakeResponse__ctor_m1677193098 (HandshakeResponse_t3229697822 * __this, Version_t763695022 * ___version0, NameValueCollection_t2791941106 * ___headers1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.HandshakeResponse::.ctor(WebSocketSharp.Net.HttpStatusCode)
extern "C"  void HandshakeResponse__ctor_m3348926657 (HandshakeResponse_t3229697822 * __this, int32_t ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.Net.AuthenticationChallenge WebSocketSharp.HandshakeResponse::get_AuthChallenge()
extern "C"  AuthenticationChallenge_t1782907061 * HandshakeResponse_get_AuthChallenge_m1867389680 (HandshakeResponse_t3229697822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.Net.CookieCollection WebSocketSharp.HandshakeResponse::get_Cookies()
extern "C"  CookieCollection_t1136277956 * HandshakeResponse_get_Cookies_m1824763165 (HandshakeResponse_t3229697822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.HandshakeResponse::get_IsUnauthorized()
extern "C"  bool HandshakeResponse_get_IsUnauthorized_m1179508529 (HandshakeResponse_t3229697822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.HandshakeResponse::get_IsWebSocketResponse()
extern "C"  bool HandshakeResponse_get_IsWebSocketResponse_m4059370381 (HandshakeResponse_t3229697822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.HandshakeResponse::get_Reason()
extern "C"  String_t* HandshakeResponse_get_Reason_m707407846 (HandshakeResponse_t3229697822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.HandshakeResponse::get_StatusCode()
extern "C"  String_t* HandshakeResponse_get_StatusCode_m2808269665 (HandshakeResponse_t3229697822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.HandshakeResponse WebSocketSharp.HandshakeResponse::CreateCloseResponse(WebSocketSharp.Net.HttpStatusCode)
extern "C"  HandshakeResponse_t3229697822 * HandshakeResponse_CreateCloseResponse_m3277193654 (Il2CppObject * __this /* static, unused */, int32_t ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.HandshakeResponse WebSocketSharp.HandshakeResponse::Parse(System.String[])
extern "C"  HandshakeResponse_t3229697822 * HandshakeResponse_Parse_m1951366865 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t4054002952* ___headerParts0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.HandshakeResponse::SetCookies(WebSocketSharp.Net.CookieCollection)
extern "C"  void HandshakeResponse_SetCookies_m3204268305 (HandshakeResponse_t3229697822 * __this, CookieCollection_t1136277956 * ___cookies0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.HandshakeResponse::ToString()
extern "C"  String_t* HandshakeResponse_ToString_m3137325381 (HandshakeResponse_t3229697822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
