﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.Net.WebHeaderCollection/<GetObjectData>c__AnonStorey26
struct U3CGetObjectDataU3Ec__AnonStorey26_t1884298844;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocketSharp.Net.WebHeaderCollection/<GetObjectData>c__AnonStorey26::.ctor()
extern "C"  void U3CGetObjectDataU3Ec__AnonStorey26__ctor_m4073771663 (U3CGetObjectDataU3Ec__AnonStorey26_t1884298844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.WebHeaderCollection/<GetObjectData>c__AnonStorey26::<>m__A(System.Int32)
extern "C"  void U3CGetObjectDataU3Ec__AnonStorey26_U3CU3Em__A_m811365980 (U3CGetObjectDataU3Ec__AnonStorey26_t1884298844 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
