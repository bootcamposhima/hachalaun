﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SkipSpeed
struct SkipSpeed_t4087407080;

#include "codegen/il2cpp-codegen.h"

// System.Void SkipSpeed::.ctor()
extern "C"  void SkipSpeed__ctor_m2356533379 (SkipSpeed_t4087407080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkipSpeed::Start()
extern "C"  void SkipSpeed_Start_m1303671171 (SkipSpeed_t4087407080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkipSpeed::Update()
extern "C"  void SkipSpeed_Update_m1764952810 (SkipSpeed_t4087407080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkipSpeed::changeSpeed()
extern "C"  void SkipSpeed_changeSpeed_m1328815736 (SkipSpeed_t4087407080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkipSpeed::NotActive()
extern "C"  void SkipSpeed_NotActive_m109137690 (SkipSpeed_t4087407080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkipSpeed::Active()
extern "C"  void SkipSpeed_Active_m3081352679 (SkipSpeed_t4087407080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
