﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Task2599333.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SearchMoveTask
struct  SearchMoveTask_t1050018526  : public Task_t2599333
{
public:
	// System.Boolean SearchMoveTask::test
	bool ___test_6;

public:
	inline static int32_t get_offset_of_test_6() { return static_cast<int32_t>(offsetof(SearchMoveTask_t1050018526, ___test_6)); }
	inline bool get_test_6() const { return ___test_6; }
	inline bool* get_address_of_test_6() { return &___test_6; }
	inline void set_test_6(bool value)
	{
		___test_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
