﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StagePanelManager
struct StagePanelManager_t334945031;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void StagePanelManager::.ctor()
extern "C"  void StagePanelManager__ctor_m442845764 (StagePanelManager_t334945031 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StagePanelManager::Start()
extern "C"  void StagePanelManager_Start_m3684950852 (StagePanelManager_t334945031 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StagePanelManager::Update()
extern "C"  void StagePanelManager_Update_m2570178889 (StagePanelManager_t334945031 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject StagePanelManager::CreateObject()
extern "C"  GameObject_t3674682005 * StagePanelManager_CreateObject_m4260070406 (StagePanelManager_t334945031 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject StagePanelManager::GetObject()
extern "C"  GameObject_t3674682005 * StagePanelManager_GetObject_m3810743244 (StagePanelManager_t334945031 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StagePanelManager::LightAction(System.Int32,System.Single,System.Single)
extern "C"  void StagePanelManager_LightAction_m1023904617 (StagePanelManager_t334945031 * __this, int32_t ___direction0, float ___position1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator StagePanelManager::StagePanelLightAction(UnityEngine.GameObject,System.Int32,System.Single,System.Single)
extern "C"  Il2CppObject * StagePanelManager_StagePanelLightAction_m702555481 (StagePanelManager_t334945031 * __this, GameObject_t3674682005 * ___ob0, int32_t ___direction1, float ___position2, float ___time3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
