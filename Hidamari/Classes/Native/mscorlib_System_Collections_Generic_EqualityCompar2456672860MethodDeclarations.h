﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<AIDatas>
struct DefaultComparer_t2456672860;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AIDatas4008896193.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<AIDatas>::.ctor()
extern "C"  void DefaultComparer__ctor_m2308984255_gshared (DefaultComparer_t2456672860 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2308984255(__this, method) ((  void (*) (DefaultComparer_t2456672860 *, const MethodInfo*))DefaultComparer__ctor_m2308984255_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<AIDatas>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1840104204_gshared (DefaultComparer_t2456672860 * __this, AIDatas_t4008896193  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1840104204(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t2456672860 *, AIDatas_t4008896193 , const MethodInfo*))DefaultComparer_GetHashCode_m1840104204_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<AIDatas>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3318245392_gshared (DefaultComparer_t2456672860 * __this, AIDatas_t4008896193  ___x0, AIDatas_t4008896193  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m3318245392(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t2456672860 *, AIDatas_t4008896193 , AIDatas_t4008896193 , const MethodInfo*))DefaultComparer_Equals_m3318245392_gshared)(__this, ___x0, ___y1, method)
