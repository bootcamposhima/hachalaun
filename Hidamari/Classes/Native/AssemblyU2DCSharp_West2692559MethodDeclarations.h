﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// West
struct West_t2692559;

#include "codegen/il2cpp-codegen.h"

// System.Void West::.ctor()
extern "C"  void West__ctor_m2837482924 (West_t2692559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void West::Start()
extern "C"  void West_Start_m1784620716 (West_t2692559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void West::Awake()
extern "C"  void West_Awake_m3075088143 (West_t2692559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void West::Update()
extern "C"  void West_Update_m3789486817 (West_t2692559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void West::FixedUpdate()
extern "C"  void West_FixedUpdate_m2155641383 (West_t2692559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void West::MoveSatrtPosition()
extern "C"  void West_MoveSatrtPosition_m1256805098 (West_t2692559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void West::MoveStart()
extern "C"  void West_MoveStart_m2541992219 (West_t2692559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void West::ChangeMove()
extern "C"  void West_ChangeMove_m133452857 (West_t2692559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void West::setCoordinate()
extern "C"  void West_setCoordinate_m3530483428 (West_t2692559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void West::Shot()
extern "C"  void West_Shot_m185450130 (West_t2692559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void West::Move()
extern "C"  void West_Move_m20333993 (West_t2692559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void West::ShotParticle()
extern "C"  void West_ShotParticle_m2505334456 (West_t2692559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
