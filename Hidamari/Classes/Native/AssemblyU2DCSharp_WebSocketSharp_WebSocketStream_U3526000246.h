﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.Byte>
struct List_1_t4230795212;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocketStream/<readHandshakeHeaders>c__AnonStorey32
struct  U3CreadHandshakeHeadersU3Ec__AnonStorey32_t3526000246  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<System.Byte> WebSocketSharp.WebSocketStream/<readHandshakeHeaders>c__AnonStorey32::buff
	List_1_t4230795212 * ___buff_0;
	// System.Int32 WebSocketSharp.WebSocketStream/<readHandshakeHeaders>c__AnonStorey32::count
	int32_t ___count_1;

public:
	inline static int32_t get_offset_of_buff_0() { return static_cast<int32_t>(offsetof(U3CreadHandshakeHeadersU3Ec__AnonStorey32_t3526000246, ___buff_0)); }
	inline List_1_t4230795212 * get_buff_0() const { return ___buff_0; }
	inline List_1_t4230795212 ** get_address_of_buff_0() { return &___buff_0; }
	inline void set_buff_0(List_1_t4230795212 * value)
	{
		___buff_0 = value;
		Il2CppCodeGenWriteBarrier(&___buff_0, value);
	}

	inline static int32_t get_offset_of_count_1() { return static_cast<int32_t>(offsetof(U3CreadHandshakeHeadersU3Ec__AnonStorey32_t3526000246, ___count_1)); }
	inline int32_t get_count_1() const { return ___count_1; }
	inline int32_t* get_address_of_count_1() { return &___count_1; }
	inline void set_count_1(int32_t value)
	{
		___count_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
