﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CenterMove
struct CenterMove_t3155240422;
// BlackBoard
struct BlackBoard_t328561223;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BlackBoard328561223.h"

// System.Void CenterMove::.ctor()
extern "C"  void CenterMove__ctor_m4197430645 (CenterMove_t3155240422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CenterMove::Start()
extern "C"  void CenterMove_Start_m3144568437 (CenterMove_t3155240422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CenterMove::Update()
extern "C"  void CenterMove_Update_m2998193208 (CenterMove_t3155240422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CenterMove::PlayTask(BlackBoard)
extern "C"  bool CenterMove_PlayTask_m2534265173 (CenterMove_t3155240422 * __this, BlackBoard_t328561223 * ___blackboard0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CenterMove::CenterMovePlace(BlackBoard)
extern "C"  bool CenterMove_CenterMovePlace_m1368553049 (CenterMove_t3155240422 * __this, BlackBoard_t328561223 * ___blackboard0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
