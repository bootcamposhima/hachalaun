﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.Net.WebSockets.WebSocketContext
struct WebSocketContext_t763725542;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocketSharp.Net.WebSockets.WebSocketContext::.ctor()
extern "C"  void WebSocketContext__ctor_m547194573 (WebSocketContext_t763725542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
