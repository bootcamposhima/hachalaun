﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TouchInput
struct TouchInput_t375919595;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchInput
struct  TouchInput_t375919595  : public MonoBehaviour_t667441552
{
public:
	// System.Boolean TouchInput::active
	bool ___active_3;

public:
	inline static int32_t get_offset_of_active_3() { return static_cast<int32_t>(offsetof(TouchInput_t375919595, ___active_3)); }
	inline bool get_active_3() const { return ___active_3; }
	inline bool* get_address_of_active_3() { return &___active_3; }
	inline void set_active_3(bool value)
	{
		___active_3 = value;
	}
};

struct TouchInput_t375919595_StaticFields
{
public:
	// TouchInput TouchInput::_instance
	TouchInput_t375919595 * ____instance_2;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(TouchInput_t375919595_StaticFields, ____instance_2)); }
	inline TouchInput_t375919595 * get__instance_2() const { return ____instance_2; }
	inline TouchInput_t375919595 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(TouchInput_t375919595 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier(&____instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
