﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Net.IPAddress[]
struct IPAddressU5BU5D_t1215594974;
// System.String
struct String_t;
// WebSocketSharp.Net.HttpListener
struct HttpListener_t398944510;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.ListenerPrefix
struct  ListenerPrefix_t2663314696  : public Il2CppObject
{
public:
	// System.Net.IPAddress[] WebSocketSharp.Net.ListenerPrefix::_addresses
	IPAddressU5BU5D_t1215594974* ____addresses_0;
	// System.String WebSocketSharp.Net.ListenerPrefix::_host
	String_t* ____host_1;
	// System.String WebSocketSharp.Net.ListenerPrefix::_original
	String_t* ____original_2;
	// System.String WebSocketSharp.Net.ListenerPrefix::_path
	String_t* ____path_3;
	// System.UInt16 WebSocketSharp.Net.ListenerPrefix::_port
	uint16_t ____port_4;
	// System.Boolean WebSocketSharp.Net.ListenerPrefix::_secure
	bool ____secure_5;
	// WebSocketSharp.Net.HttpListener WebSocketSharp.Net.ListenerPrefix::Listener
	HttpListener_t398944510 * ___Listener_6;

public:
	inline static int32_t get_offset_of__addresses_0() { return static_cast<int32_t>(offsetof(ListenerPrefix_t2663314696, ____addresses_0)); }
	inline IPAddressU5BU5D_t1215594974* get__addresses_0() const { return ____addresses_0; }
	inline IPAddressU5BU5D_t1215594974** get_address_of__addresses_0() { return &____addresses_0; }
	inline void set__addresses_0(IPAddressU5BU5D_t1215594974* value)
	{
		____addresses_0 = value;
		Il2CppCodeGenWriteBarrier(&____addresses_0, value);
	}

	inline static int32_t get_offset_of__host_1() { return static_cast<int32_t>(offsetof(ListenerPrefix_t2663314696, ____host_1)); }
	inline String_t* get__host_1() const { return ____host_1; }
	inline String_t** get_address_of__host_1() { return &____host_1; }
	inline void set__host_1(String_t* value)
	{
		____host_1 = value;
		Il2CppCodeGenWriteBarrier(&____host_1, value);
	}

	inline static int32_t get_offset_of__original_2() { return static_cast<int32_t>(offsetof(ListenerPrefix_t2663314696, ____original_2)); }
	inline String_t* get__original_2() const { return ____original_2; }
	inline String_t** get_address_of__original_2() { return &____original_2; }
	inline void set__original_2(String_t* value)
	{
		____original_2 = value;
		Il2CppCodeGenWriteBarrier(&____original_2, value);
	}

	inline static int32_t get_offset_of__path_3() { return static_cast<int32_t>(offsetof(ListenerPrefix_t2663314696, ____path_3)); }
	inline String_t* get__path_3() const { return ____path_3; }
	inline String_t** get_address_of__path_3() { return &____path_3; }
	inline void set__path_3(String_t* value)
	{
		____path_3 = value;
		Il2CppCodeGenWriteBarrier(&____path_3, value);
	}

	inline static int32_t get_offset_of__port_4() { return static_cast<int32_t>(offsetof(ListenerPrefix_t2663314696, ____port_4)); }
	inline uint16_t get__port_4() const { return ____port_4; }
	inline uint16_t* get_address_of__port_4() { return &____port_4; }
	inline void set__port_4(uint16_t value)
	{
		____port_4 = value;
	}

	inline static int32_t get_offset_of__secure_5() { return static_cast<int32_t>(offsetof(ListenerPrefix_t2663314696, ____secure_5)); }
	inline bool get__secure_5() const { return ____secure_5; }
	inline bool* get_address_of__secure_5() { return &____secure_5; }
	inline void set__secure_5(bool value)
	{
		____secure_5 = value;
	}

	inline static int32_t get_offset_of_Listener_6() { return static_cast<int32_t>(offsetof(ListenerPrefix_t2663314696, ___Listener_6)); }
	inline HttpListener_t398944510 * get_Listener_6() const { return ___Listener_6; }
	inline HttpListener_t398944510 ** get_address_of_Listener_6() { return &___Listener_6; }
	inline void set_Listener_6(HttpListener_t398944510 * value)
	{
		___Listener_6 = value;
		Il2CppCodeGenWriteBarrier(&___Listener_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
