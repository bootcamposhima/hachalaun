﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// PlaySelect
struct PlaySelect_t3562688880;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Stop
struct  Stop_t2587682  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject Stop::_ReCanvas
	GameObject_t3674682005 * ____ReCanvas_2;
	// UnityEngine.GameObject Stop::_Retire
	GameObject_t3674682005 * ____Retire_3;
	// PlaySelect Stop::_playselect
	PlaySelect_t3562688880 * ____playselect_4;
	// System.Boolean Stop::isFight
	bool ___isFight_5;
	// System.Boolean Stop::isPlay
	bool ___isPlay_6;
	// System.Boolean Stop::isSelect
	bool ___isSelect_7;

public:
	inline static int32_t get_offset_of__ReCanvas_2() { return static_cast<int32_t>(offsetof(Stop_t2587682, ____ReCanvas_2)); }
	inline GameObject_t3674682005 * get__ReCanvas_2() const { return ____ReCanvas_2; }
	inline GameObject_t3674682005 ** get_address_of__ReCanvas_2() { return &____ReCanvas_2; }
	inline void set__ReCanvas_2(GameObject_t3674682005 * value)
	{
		____ReCanvas_2 = value;
		Il2CppCodeGenWriteBarrier(&____ReCanvas_2, value);
	}

	inline static int32_t get_offset_of__Retire_3() { return static_cast<int32_t>(offsetof(Stop_t2587682, ____Retire_3)); }
	inline GameObject_t3674682005 * get__Retire_3() const { return ____Retire_3; }
	inline GameObject_t3674682005 ** get_address_of__Retire_3() { return &____Retire_3; }
	inline void set__Retire_3(GameObject_t3674682005 * value)
	{
		____Retire_3 = value;
		Il2CppCodeGenWriteBarrier(&____Retire_3, value);
	}

	inline static int32_t get_offset_of__playselect_4() { return static_cast<int32_t>(offsetof(Stop_t2587682, ____playselect_4)); }
	inline PlaySelect_t3562688880 * get__playselect_4() const { return ____playselect_4; }
	inline PlaySelect_t3562688880 ** get_address_of__playselect_4() { return &____playselect_4; }
	inline void set__playselect_4(PlaySelect_t3562688880 * value)
	{
		____playselect_4 = value;
		Il2CppCodeGenWriteBarrier(&____playselect_4, value);
	}

	inline static int32_t get_offset_of_isFight_5() { return static_cast<int32_t>(offsetof(Stop_t2587682, ___isFight_5)); }
	inline bool get_isFight_5() const { return ___isFight_5; }
	inline bool* get_address_of_isFight_5() { return &___isFight_5; }
	inline void set_isFight_5(bool value)
	{
		___isFight_5 = value;
	}

	inline static int32_t get_offset_of_isPlay_6() { return static_cast<int32_t>(offsetof(Stop_t2587682, ___isPlay_6)); }
	inline bool get_isPlay_6() const { return ___isPlay_6; }
	inline bool* get_address_of_isPlay_6() { return &___isPlay_6; }
	inline void set_isPlay_6(bool value)
	{
		___isPlay_6 = value;
	}

	inline static int32_t get_offset_of_isSelect_7() { return static_cast<int32_t>(offsetof(Stop_t2587682, ___isSelect_7)); }
	inline bool get_isSelect_7() const { return ___isSelect_7; }
	inline bool* get_address_of_isSelect_7() { return &___isSelect_7; }
	inline void set_isSelect_7(bool value)
	{
		___isSelect_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
