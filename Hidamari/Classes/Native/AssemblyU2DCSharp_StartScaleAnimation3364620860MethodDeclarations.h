﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StartScaleAnimation
struct StartScaleAnimation_t3364620860;

#include "codegen/il2cpp-codegen.h"

// System.Void StartScaleAnimation::.ctor()
extern "C"  void StartScaleAnimation__ctor_m3602107567 (StartScaleAnimation_t3364620860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StartScaleAnimation::Start()
extern "C"  void StartScaleAnimation_Start_m2549245359 (StartScaleAnimation_t3364620860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StartScaleAnimation::Update()
extern "C"  void StartScaleAnimation_Update_m1723046974 (StartScaleAnimation_t3364620860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
