﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>
struct Dictionary_2_t1025825965;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3252626669.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<WebSocketSharp.CompressionMethod,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m769366653_gshared (Enumerator_t3252626669 * __this, Dictionary_2_t1025825965 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m769366653(__this, ___host0, method) ((  void (*) (Enumerator_t3252626669 *, Dictionary_2_t1025825965 *, const MethodInfo*))Enumerator__ctor_m769366653_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1512553102_gshared (Enumerator_t3252626669 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1512553102(__this, method) ((  Il2CppObject * (*) (Enumerator_t3252626669 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1512553102_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m241549848_gshared (Enumerator_t3252626669 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m241549848(__this, method) ((  void (*) (Enumerator_t3252626669 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m241549848_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<WebSocketSharp.CompressionMethod,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m539306591_gshared (Enumerator_t3252626669 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m539306591(__this, method) ((  void (*) (Enumerator_t3252626669 *, const MethodInfo*))Enumerator_Dispose_m539306591_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<WebSocketSharp.CompressionMethod,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3627897544_gshared (Enumerator_t3252626669 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3627897544(__this, method) ((  bool (*) (Enumerator_t3252626669 *, const MethodInfo*))Enumerator_MoveNext_m3627897544_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<WebSocketSharp.CompressionMethod,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m979620962_gshared (Enumerator_t3252626669 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m979620962(__this, method) ((  Il2CppObject * (*) (Enumerator_t3252626669 *, const MethodInfo*))Enumerator_get_Current_m979620962_gshared)(__this, method)
