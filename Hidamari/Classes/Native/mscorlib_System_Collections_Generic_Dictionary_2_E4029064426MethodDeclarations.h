﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2343149357MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.IO.Stream>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1259838154(__this, ___dictionary0, method) ((  void (*) (Enumerator_t4029064426 *, Dictionary_2_t2711741034 *, const MethodInfo*))Enumerator__ctor_m3563270077_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.IO.Stream>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1753381591(__this, method) ((  Il2CppObject * (*) (Enumerator_t4029064426 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1341511502_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.IO.Stream>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1385925547(__this, method) ((  void (*) (Enumerator_t4029064426 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2086888664_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.IO.Stream>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m507347508(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t4029064426 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2409560079_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.IO.Stream>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1915353715(__this, method) ((  Il2CppObject * (*) (Enumerator_t4029064426 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m195944874_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.IO.Stream>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3457353797(__this, method) ((  Il2CppObject * (*) (Enumerator_t4029064426 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m372899260_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.IO.Stream>::MoveNext()
#define Enumerator_MoveNext_m3736923287(__this, method) ((  bool (*) (Enumerator_t4029064426 *, const MethodInfo*))Enumerator_MoveNext_m2059000200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.IO.Stream>::get_Current()
#define Enumerator_get_Current_m1643518649(__this, method) ((  KeyValuePair_2_t2610521740  (*) (Enumerator_t4029064426 *, const MethodInfo*))Enumerator_get_Current_m2916729652_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.IO.Stream>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m982330788(__this, method) ((  uint8_t (*) (Enumerator_t4029064426 *, const MethodInfo*))Enumerator_get_CurrentKey_m4242206481_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.IO.Stream>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3053109192(__this, method) ((  Stream_t1561764144 * (*) (Enumerator_t4029064426 *, const MethodInfo*))Enumerator_get_CurrentValue_m2318948881_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.IO.Stream>::Reset()
#define Enumerator_Reset_m3225134620(__this, method) ((  void (*) (Enumerator_t4029064426 *, const MethodInfo*))Enumerator_Reset_m619019919_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.IO.Stream>::VerifyState()
#define Enumerator_VerifyState_m2913530149(__this, method) ((  void (*) (Enumerator_t4029064426 *, const MethodInfo*))Enumerator_VerifyState_m889022296_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.IO.Stream>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m2328667981(__this, method) ((  void (*) (Enumerator_t4029064426 *, const MethodInfo*))Enumerator_VerifyCurrent_m2396806336_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.IO.Stream>::Dispose()
#define Enumerator_Dispose_m911411180(__this, method) ((  void (*) (Enumerator_t4029064426 *, const MethodInfo*))Enumerator_Dispose_m401117087_gshared)(__this, method)
