﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType1744280289.h"
#include "AssemblyU2DCSharp_Direction1041377119.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HitJson
struct  HitJson_t2591263483 
{
public:
	// System.Int32 HitJson::damage
	int32_t ___damage_0;
	// Direction HitJson::direction
	int32_t ___direction_1;

public:
	inline static int32_t get_offset_of_damage_0() { return static_cast<int32_t>(offsetof(HitJson_t2591263483, ___damage_0)); }
	inline int32_t get_damage_0() const { return ___damage_0; }
	inline int32_t* get_address_of_damage_0() { return &___damage_0; }
	inline void set_damage_0(int32_t value)
	{
		___damage_0 = value;
	}

	inline static int32_t get_offset_of_direction_1() { return static_cast<int32_t>(offsetof(HitJson_t2591263483, ___direction_1)); }
	inline int32_t get_direction_1() const { return ___direction_1; }
	inline int32_t* get_address_of_direction_1() { return &___direction_1; }
	inline void set_direction_1(int32_t value)
	{
		___direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: HitJson
struct HitJson_t2591263483_marshaled_pinvoke
{
	int32_t ___damage_0;
	int32_t ___direction_1;
};
// Native definition for marshalling of: HitJson
struct HitJson_t2591263483_marshaled_com
{
	int32_t ___damage_0;
	int32_t ___direction_1;
};
