﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SocketIO.Decoder
struct Decoder_t2133550898;
// SocketIO.Packet
struct Packet_t1660110912;
// WebSocketSharp.MessageEventArgs
struct MessageEventArgs_t36945740;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_WebSocketSharp_MessageEventArgs36945740.h"

// System.Void SocketIO.Decoder::.ctor()
extern "C"  void Decoder__ctor_m1489306700 (Decoder_t2133550898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SocketIO.Packet SocketIO.Decoder::Decode(WebSocketSharp.MessageEventArgs)
extern "C"  Packet_t1660110912 * Decoder_Decode_m1276394139 (Decoder_t2133550898 * __this, MessageEventArgs_t36945740 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
