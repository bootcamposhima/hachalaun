﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InterimResult
struct InterimResult_t3822800669;

#include "codegen/il2cpp-codegen.h"

// System.Void InterimResult::.ctor()
extern "C"  void InterimResult__ctor_m2138686190 (InterimResult_t3822800669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InterimResult::Start()
extern "C"  void InterimResult_Start_m1085823982 (InterimResult_t3822800669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InterimResult::Update()
extern "C"  void InterimResult_Update_m3601624543 (InterimResult_t3822800669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InterimResult::ShowSetInterimResult(System.Int32,System.Int32,System.Int32)
extern "C"  void InterimResult_ShowSetInterimResult_m3412971679 (InterimResult_t3822800669 * __this, int32_t ___round0, int32_t ___player1HP1, int32_t ___player2HP2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InterimResult::ShowInterimResult()
extern "C"  void InterimResult_ShowInterimResult_m4250474188 (InterimResult_t3822800669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InterimResult::NotShowInterimResult()
extern "C"  void InterimResult_NotShowInterimResult_m1854259203 (InterimResult_t3822800669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
