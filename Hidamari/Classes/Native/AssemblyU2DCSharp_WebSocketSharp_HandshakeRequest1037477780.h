﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_WebSocketSharp_HandshakeBase1248407470.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.HandshakeRequest
struct  HandshakeRequest_t1037477780  : public HandshakeBase_t1248407470
{
public:
	// System.String WebSocketSharp.HandshakeRequest::_method
	String_t* ____method_4;
	// System.String WebSocketSharp.HandshakeRequest::_uri
	String_t* ____uri_5;
	// System.Boolean WebSocketSharp.HandshakeRequest::_websocketRequest
	bool ____websocketRequest_6;
	// System.Boolean WebSocketSharp.HandshakeRequest::_websocketRequestWasSet
	bool ____websocketRequestWasSet_7;

public:
	inline static int32_t get_offset_of__method_4() { return static_cast<int32_t>(offsetof(HandshakeRequest_t1037477780, ____method_4)); }
	inline String_t* get__method_4() const { return ____method_4; }
	inline String_t** get_address_of__method_4() { return &____method_4; }
	inline void set__method_4(String_t* value)
	{
		____method_4 = value;
		Il2CppCodeGenWriteBarrier(&____method_4, value);
	}

	inline static int32_t get_offset_of__uri_5() { return static_cast<int32_t>(offsetof(HandshakeRequest_t1037477780, ____uri_5)); }
	inline String_t* get__uri_5() const { return ____uri_5; }
	inline String_t** get_address_of__uri_5() { return &____uri_5; }
	inline void set__uri_5(String_t* value)
	{
		____uri_5 = value;
		Il2CppCodeGenWriteBarrier(&____uri_5, value);
	}

	inline static int32_t get_offset_of__websocketRequest_6() { return static_cast<int32_t>(offsetof(HandshakeRequest_t1037477780, ____websocketRequest_6)); }
	inline bool get__websocketRequest_6() const { return ____websocketRequest_6; }
	inline bool* get_address_of__websocketRequest_6() { return &____websocketRequest_6; }
	inline void set__websocketRequest_6(bool value)
	{
		____websocketRequest_6 = value;
	}

	inline static int32_t get_offset_of__websocketRequestWasSet_7() { return static_cast<int32_t>(offsetof(HandshakeRequest_t1037477780, ____websocketRequestWasSet_7)); }
	inline bool get__websocketRequestWasSet_7() const { return ____websocketRequestWasSet_7; }
	inline bool* get_address_of__websocketRequestWasSet_7() { return &____websocketRequestWasSet_7; }
	inline void set__websocketRequestWasSet_7(bool value)
	{
		____websocketRequestWasSet_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
