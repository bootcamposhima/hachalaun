﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// FightSocket
struct FightSocket_t1340804995;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SingletonMonoBehaviour`1<FightSocket>
struct  SingletonMonoBehaviour_1_t670979848  : public MonoBehaviour_t667441552
{
public:

public:
};

struct SingletonMonoBehaviour_1_t670979848_StaticFields
{
public:
	// T SingletonMonoBehaviour`1::instance
	FightSocket_t1340804995 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(SingletonMonoBehaviour_1_t670979848_StaticFields, ___instance_2)); }
	inline FightSocket_t1340804995 * get_instance_2() const { return ___instance_2; }
	inline FightSocket_t1340804995 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(FightSocket_t1340804995 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
