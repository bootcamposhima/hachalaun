﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t4260760469;
// WebSocketSharp.WebSocketFrame
struct WebSocketFrame_t778194306;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator20
struct  U3CGetEnumeratorU3Ec__Iterator20_t4155755117  : public Il2CppObject
{
public:
	// System.Byte[] WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator20::<$s_168>__0
	ByteU5BU5D_t4260760469* ___U3CU24s_168U3E__0_0;
	// System.Int32 WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator20::<$s_169>__1
	int32_t ___U3CU24s_169U3E__1_1;
	// System.Byte WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator20::<b>__2
	uint8_t ___U3CbU3E__2_2;
	// System.Int32 WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator20::$PC
	int32_t ___U24PC_3;
	// System.Byte WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator20::$current
	uint8_t ___U24current_4;
	// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator20::<>f__this
	WebSocketFrame_t778194306 * ___U3CU3Ef__this_5;

public:
	inline static int32_t get_offset_of_U3CU24s_168U3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator20_t4155755117, ___U3CU24s_168U3E__0_0)); }
	inline ByteU5BU5D_t4260760469* get_U3CU24s_168U3E__0_0() const { return ___U3CU24s_168U3E__0_0; }
	inline ByteU5BU5D_t4260760469** get_address_of_U3CU24s_168U3E__0_0() { return &___U3CU24s_168U3E__0_0; }
	inline void set_U3CU24s_168U3E__0_0(ByteU5BU5D_t4260760469* value)
	{
		___U3CU24s_168U3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_168U3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CU24s_169U3E__1_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator20_t4155755117, ___U3CU24s_169U3E__1_1)); }
	inline int32_t get_U3CU24s_169U3E__1_1() const { return ___U3CU24s_169U3E__1_1; }
	inline int32_t* get_address_of_U3CU24s_169U3E__1_1() { return &___U3CU24s_169U3E__1_1; }
	inline void set_U3CU24s_169U3E__1_1(int32_t value)
	{
		___U3CU24s_169U3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CbU3E__2_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator20_t4155755117, ___U3CbU3E__2_2)); }
	inline uint8_t get_U3CbU3E__2_2() const { return ___U3CbU3E__2_2; }
	inline uint8_t* get_address_of_U3CbU3E__2_2() { return &___U3CbU3E__2_2; }
	inline void set_U3CbU3E__2_2(uint8_t value)
	{
		___U3CbU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator20_t4155755117, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator20_t4155755117, ___U24current_4)); }
	inline uint8_t get_U24current_4() const { return ___U24current_4; }
	inline uint8_t* get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(uint8_t value)
	{
		___U24current_4 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_5() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator20_t4155755117, ___U3CU3Ef__this_5)); }
	inline WebSocketFrame_t778194306 * get_U3CU3Ef__this_5() const { return ___U3CU3Ef__this_5; }
	inline WebSocketFrame_t778194306 ** get_address_of_U3CU3Ef__this_5() { return &___U3CU3Ef__this_5; }
	inline void set_U3CU3Ef__this_5(WebSocketFrame_t778194306 * value)
	{
		___U3CU3Ef__this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
