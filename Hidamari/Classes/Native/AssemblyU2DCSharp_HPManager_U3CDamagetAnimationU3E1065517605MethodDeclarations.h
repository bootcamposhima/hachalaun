﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HPManager/<DamagetAnimation>c__Iterator12
struct U3CDamagetAnimationU3Ec__Iterator12_t1065517605;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void HPManager/<DamagetAnimation>c__Iterator12::.ctor()
extern "C"  void U3CDamagetAnimationU3Ec__Iterator12__ctor_m1882885350 (U3CDamagetAnimationU3Ec__Iterator12_t1065517605 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HPManager/<DamagetAnimation>c__Iterator12::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDamagetAnimationU3Ec__Iterator12_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1595121772 (U3CDamagetAnimationU3Ec__Iterator12_t1065517605 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HPManager/<DamagetAnimation>c__Iterator12::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDamagetAnimationU3Ec__Iterator12_System_Collections_IEnumerator_get_Current_m4153146368 (U3CDamagetAnimationU3Ec__Iterator12_t1065517605 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HPManager/<DamagetAnimation>c__Iterator12::MoveNext()
extern "C"  bool U3CDamagetAnimationU3Ec__Iterator12_MoveNext_m108990542 (U3CDamagetAnimationU3Ec__Iterator12_t1065517605 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HPManager/<DamagetAnimation>c__Iterator12::Dispose()
extern "C"  void U3CDamagetAnimationU3Ec__Iterator12_Dispose_m1169872803 (U3CDamagetAnimationU3Ec__Iterator12_t1065517605 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HPManager/<DamagetAnimation>c__Iterator12::Reset()
extern "C"  void U3CDamagetAnimationU3Ec__Iterator12_Reset_m3824285587 (U3CDamagetAnimationU3Ec__Iterator12_t1065517605 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
