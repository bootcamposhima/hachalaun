﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FightRound/<ShowRetire>c__Iterator10
struct  U3CShowRetireU3Ec__Iterator10_t4208130037  : public Il2CppObject
{
public:
	// UnityEngine.GameObject FightRound/<ShowRetire>c__Iterator10::_go
	GameObject_t3674682005 * ____go_0;
	// System.Single FightRound/<ShowRetire>c__Iterator10::<time>__0
	float ___U3CtimeU3E__0_1;
	// UnityEngine.Vector3 FightRound/<ShowRetire>c__Iterator10::<p>__1
	Vector3_t4282066566  ___U3CpU3E__1_2;
	// System.Int32 FightRound/<ShowRetire>c__Iterator10::$PC
	int32_t ___U24PC_3;
	// System.Object FightRound/<ShowRetire>c__Iterator10::$current
	Il2CppObject * ___U24current_4;
	// UnityEngine.GameObject FightRound/<ShowRetire>c__Iterator10::<$>_go
	GameObject_t3674682005 * ___U3CU24U3E_go_5;

public:
	inline static int32_t get_offset_of__go_0() { return static_cast<int32_t>(offsetof(U3CShowRetireU3Ec__Iterator10_t4208130037, ____go_0)); }
	inline GameObject_t3674682005 * get__go_0() const { return ____go_0; }
	inline GameObject_t3674682005 ** get_address_of__go_0() { return &____go_0; }
	inline void set__go_0(GameObject_t3674682005 * value)
	{
		____go_0 = value;
		Il2CppCodeGenWriteBarrier(&____go_0, value);
	}

	inline static int32_t get_offset_of_U3CtimeU3E__0_1() { return static_cast<int32_t>(offsetof(U3CShowRetireU3Ec__Iterator10_t4208130037, ___U3CtimeU3E__0_1)); }
	inline float get_U3CtimeU3E__0_1() const { return ___U3CtimeU3E__0_1; }
	inline float* get_address_of_U3CtimeU3E__0_1() { return &___U3CtimeU3E__0_1; }
	inline void set_U3CtimeU3E__0_1(float value)
	{
		___U3CtimeU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CpU3E__1_2() { return static_cast<int32_t>(offsetof(U3CShowRetireU3Ec__Iterator10_t4208130037, ___U3CpU3E__1_2)); }
	inline Vector3_t4282066566  get_U3CpU3E__1_2() const { return ___U3CpU3E__1_2; }
	inline Vector3_t4282066566 * get_address_of_U3CpU3E__1_2() { return &___U3CpU3E__1_2; }
	inline void set_U3CpU3E__1_2(Vector3_t4282066566  value)
	{
		___U3CpU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CShowRetireU3Ec__Iterator10_t4208130037, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CShowRetireU3Ec__Iterator10_t4208130037, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U3CU24U3E_go_5() { return static_cast<int32_t>(offsetof(U3CShowRetireU3Ec__Iterator10_t4208130037, ___U3CU24U3E_go_5)); }
	inline GameObject_t3674682005 * get_U3CU24U3E_go_5() const { return ___U3CU24U3E_go_5; }
	inline GameObject_t3674682005 ** get_address_of_U3CU24U3E_go_5() { return &___U3CU24U3E_go_5; }
	inline void set_U3CU24U3E_go_5(GameObject_t3674682005 * value)
	{
		___U3CU24U3E_go_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3E_go_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
