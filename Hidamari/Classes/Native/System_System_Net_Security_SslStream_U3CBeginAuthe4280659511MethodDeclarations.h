﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.Security.SslStream/<BeginAuthenticateAsServer>c__AnonStorey8
struct U3CBeginAuthenticateAsServerU3Ec__AnonStorey8_t4280659511;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t1241690687;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t3076817455;
// System.String
struct String_t;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Security_Cryptography_X509Certific3076817455.h"
#include "mscorlib_System_String7231557.h"

// System.Void System.Net.Security.SslStream/<BeginAuthenticateAsServer>c__AnonStorey8::.ctor()
extern "C"  void U3CBeginAuthenticateAsServerU3Ec__AnonStorey8__ctor_m2933050355 (U3CBeginAuthenticateAsServerU3Ec__AnonStorey8_t4280659511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsymmetricAlgorithm System.Net.Security.SslStream/<BeginAuthenticateAsServer>c__AnonStorey8::<>m__9(System.Security.Cryptography.X509Certificates.X509Certificate,System.String)
extern "C"  AsymmetricAlgorithm_t1241690687 * U3CBeginAuthenticateAsServerU3Ec__AnonStorey8_U3CU3Em__9_m2411652432 (U3CBeginAuthenticateAsServerU3Ec__AnonStorey8_t4280659511 * __this, X509Certificate_t3076817455 * ___cert0, String_t* ___targetHost1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.Security.SslStream/<BeginAuthenticateAsServer>c__AnonStorey8::<>m__A(System.Security.Cryptography.X509Certificates.X509Certificate,System.Int32[])
extern "C"  bool U3CBeginAuthenticateAsServerU3Ec__AnonStorey8_U3CU3Em__A_m956293964 (U3CBeginAuthenticateAsServerU3Ec__AnonStorey8_t4280659511 * __this, X509Certificate_t3076817455 * ___cert0, Int32U5BU5D_t3230847821* ___certErrors1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
