﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ButtonEvent
struct ButtonEvent_t4179473000;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"

// System.Void ButtonEvent::.ctor()
extern "C"  void ButtonEvent__ctor_m2569797123 (ButtonEvent_t4179473000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonEvent::Start()
extern "C"  void ButtonEvent_Start_m1516934915 (ButtonEvent_t4179473000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonEvent::Update()
extern "C"  void ButtonEvent_Update_m4081161578 (ButtonEvent_t4179473000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject ButtonEvent::get_EventObject()
extern "C"  GameObject_t3674682005 * ButtonEvent_get_EventObject_m4081719846 (ButtonEvent_t4179473000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ButtonEvent::get_EventNum()
extern "C"  int32_t ButtonEvent_get_EventNum_m173644840 (ButtonEvent_t4179473000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
