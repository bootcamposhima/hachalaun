﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.EqualityComparer`1<AIDatas>
struct EqualityComparer_1_t3116733074;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<AIDatas>
struct  EqualityComparer_1_t3116733074  : public Il2CppObject
{
public:

public:
};

struct EqualityComparer_1_t3116733074_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t3116733074 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t3116733074_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t3116733074 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t3116733074 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t3116733074 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier(&____default_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
