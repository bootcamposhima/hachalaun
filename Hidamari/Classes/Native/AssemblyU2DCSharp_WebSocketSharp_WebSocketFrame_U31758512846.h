﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IO.Stream
struct Stream_t1561764144;
// System.Action`1<WebSocketSharp.WebSocketFrame>
struct Action_1_t1174010442;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocketFrame/<ParseAsync>c__AnonStorey31
struct  U3CParseAsyncU3Ec__AnonStorey31_t1758512846  : public Il2CppObject
{
public:
	// System.IO.Stream WebSocketSharp.WebSocketFrame/<ParseAsync>c__AnonStorey31::stream
	Stream_t1561764144 * ___stream_0;
	// System.Boolean WebSocketSharp.WebSocketFrame/<ParseAsync>c__AnonStorey31::unmask
	bool ___unmask_1;
	// System.Action`1<WebSocketSharp.WebSocketFrame> WebSocketSharp.WebSocketFrame/<ParseAsync>c__AnonStorey31::completed
	Action_1_t1174010442 * ___completed_2;

public:
	inline static int32_t get_offset_of_stream_0() { return static_cast<int32_t>(offsetof(U3CParseAsyncU3Ec__AnonStorey31_t1758512846, ___stream_0)); }
	inline Stream_t1561764144 * get_stream_0() const { return ___stream_0; }
	inline Stream_t1561764144 ** get_address_of_stream_0() { return &___stream_0; }
	inline void set_stream_0(Stream_t1561764144 * value)
	{
		___stream_0 = value;
		Il2CppCodeGenWriteBarrier(&___stream_0, value);
	}

	inline static int32_t get_offset_of_unmask_1() { return static_cast<int32_t>(offsetof(U3CParseAsyncU3Ec__AnonStorey31_t1758512846, ___unmask_1)); }
	inline bool get_unmask_1() const { return ___unmask_1; }
	inline bool* get_address_of_unmask_1() { return &___unmask_1; }
	inline void set_unmask_1(bool value)
	{
		___unmask_1 = value;
	}

	inline static int32_t get_offset_of_completed_2() { return static_cast<int32_t>(offsetof(U3CParseAsyncU3Ec__AnonStorey31_t1758512846, ___completed_2)); }
	inline Action_1_t1174010442 * get_completed_2() const { return ___completed_2; }
	inline Action_1_t1174010442 ** get_address_of_completed_2() { return &___completed_2; }
	inline void set_completed_2(Action_1_t1174010442 * value)
	{
		___completed_2 = value;
		Il2CppCodeGenWriteBarrier(&___completed_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
