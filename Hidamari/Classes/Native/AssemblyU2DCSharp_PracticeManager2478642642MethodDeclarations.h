﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PracticeManager
struct PracticeManager_t2478642642;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AIDatas4008896193.h"

// System.Void PracticeManager::.ctor()
extern "C"  void PracticeManager__ctor_m2375097817 (PracticeManager_t2478642642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PracticeManager::Start()
extern "C"  void PracticeManager_Start_m1322235609 (PracticeManager_t2478642642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PracticeManager::Awake()
extern "C"  void PracticeManager_Awake_m2612703036 (PracticeManager_t2478642642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PracticeManager::Update()
extern "C"  void PracticeManager_Update_m2340450388 (PracticeManager_t2478642642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PracticeManager::TopAIChoiceButton(System.Int32)
extern "C"  void PracticeManager_TopAIChoiceButton_m2036153816 (PracticeManager_t2478642642 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PracticeManager::FinishButton()
extern "C"  void PracticeManager_FinishButton_m163501744 (PracticeManager_t2478642642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PracticeManager::AIChoiceActive()
extern "C"  void PracticeManager_AIChoiceActive_m2515037242 (PracticeManager_t2478642642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PracticeManager::ChangeSave(AIDatas)
extern "C"  void PracticeManager_ChangeSave_m3057499129 (PracticeManager_t2478642642 * __this, AIDatas_t4008896193  ___aidatas0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
