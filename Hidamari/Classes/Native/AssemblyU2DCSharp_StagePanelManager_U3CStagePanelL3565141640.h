﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StagePanelManager/<StagePanelLightAction>c__Iterator14
struct  U3CStagePanelLightActionU3Ec__Iterator14_t3565141640  : public Il2CppObject
{
public:
	// UnityEngine.GameObject StagePanelManager/<StagePanelLightAction>c__Iterator14::ob
	GameObject_t3674682005 * ___ob_0;
	// System.Int32 StagePanelManager/<StagePanelLightAction>c__Iterator14::direction
	int32_t ___direction_1;
	// System.Single StagePanelManager/<StagePanelLightAction>c__Iterator14::position
	float ___position_2;
	// System.Single StagePanelManager/<StagePanelLightAction>c__Iterator14::time
	float ___time_3;
	// System.Int32 StagePanelManager/<StagePanelLightAction>c__Iterator14::$PC
	int32_t ___U24PC_4;
	// System.Object StagePanelManager/<StagePanelLightAction>c__Iterator14::$current
	Il2CppObject * ___U24current_5;
	// UnityEngine.GameObject StagePanelManager/<StagePanelLightAction>c__Iterator14::<$>ob
	GameObject_t3674682005 * ___U3CU24U3Eob_6;
	// System.Int32 StagePanelManager/<StagePanelLightAction>c__Iterator14::<$>direction
	int32_t ___U3CU24U3Edirection_7;
	// System.Single StagePanelManager/<StagePanelLightAction>c__Iterator14::<$>position
	float ___U3CU24U3Eposition_8;
	// System.Single StagePanelManager/<StagePanelLightAction>c__Iterator14::<$>time
	float ___U3CU24U3Etime_9;

public:
	inline static int32_t get_offset_of_ob_0() { return static_cast<int32_t>(offsetof(U3CStagePanelLightActionU3Ec__Iterator14_t3565141640, ___ob_0)); }
	inline GameObject_t3674682005 * get_ob_0() const { return ___ob_0; }
	inline GameObject_t3674682005 ** get_address_of_ob_0() { return &___ob_0; }
	inline void set_ob_0(GameObject_t3674682005 * value)
	{
		___ob_0 = value;
		Il2CppCodeGenWriteBarrier(&___ob_0, value);
	}

	inline static int32_t get_offset_of_direction_1() { return static_cast<int32_t>(offsetof(U3CStagePanelLightActionU3Ec__Iterator14_t3565141640, ___direction_1)); }
	inline int32_t get_direction_1() const { return ___direction_1; }
	inline int32_t* get_address_of_direction_1() { return &___direction_1; }
	inline void set_direction_1(int32_t value)
	{
		___direction_1 = value;
	}

	inline static int32_t get_offset_of_position_2() { return static_cast<int32_t>(offsetof(U3CStagePanelLightActionU3Ec__Iterator14_t3565141640, ___position_2)); }
	inline float get_position_2() const { return ___position_2; }
	inline float* get_address_of_position_2() { return &___position_2; }
	inline void set_position_2(float value)
	{
		___position_2 = value;
	}

	inline static int32_t get_offset_of_time_3() { return static_cast<int32_t>(offsetof(U3CStagePanelLightActionU3Ec__Iterator14_t3565141640, ___time_3)); }
	inline float get_time_3() const { return ___time_3; }
	inline float* get_address_of_time_3() { return &___time_3; }
	inline void set_time_3(float value)
	{
		___time_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CStagePanelLightActionU3Ec__Iterator14_t3565141640, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CStagePanelLightActionU3Ec__Iterator14_t3565141640, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eob_6() { return static_cast<int32_t>(offsetof(U3CStagePanelLightActionU3Ec__Iterator14_t3565141640, ___U3CU24U3Eob_6)); }
	inline GameObject_t3674682005 * get_U3CU24U3Eob_6() const { return ___U3CU24U3Eob_6; }
	inline GameObject_t3674682005 ** get_address_of_U3CU24U3Eob_6() { return &___U3CU24U3Eob_6; }
	inline void set_U3CU24U3Eob_6(GameObject_t3674682005 * value)
	{
		___U3CU24U3Eob_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eob_6, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Edirection_7() { return static_cast<int32_t>(offsetof(U3CStagePanelLightActionU3Ec__Iterator14_t3565141640, ___U3CU24U3Edirection_7)); }
	inline int32_t get_U3CU24U3Edirection_7() const { return ___U3CU24U3Edirection_7; }
	inline int32_t* get_address_of_U3CU24U3Edirection_7() { return &___U3CU24U3Edirection_7; }
	inline void set_U3CU24U3Edirection_7(int32_t value)
	{
		___U3CU24U3Edirection_7 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Eposition_8() { return static_cast<int32_t>(offsetof(U3CStagePanelLightActionU3Ec__Iterator14_t3565141640, ___U3CU24U3Eposition_8)); }
	inline float get_U3CU24U3Eposition_8() const { return ___U3CU24U3Eposition_8; }
	inline float* get_address_of_U3CU24U3Eposition_8() { return &___U3CU24U3Eposition_8; }
	inline void set_U3CU24U3Eposition_8(float value)
	{
		___U3CU24U3Eposition_8 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Etime_9() { return static_cast<int32_t>(offsetof(U3CStagePanelLightActionU3Ec__Iterator14_t3565141640, ___U3CU24U3Etime_9)); }
	inline float get_U3CU24U3Etime_9() const { return ___U3CU24U3Etime_9; }
	inline float* get_address_of_U3CU24U3Etime_9() { return &___U3CU24U3Etime_9; }
	inline void set_U3CU24U3Etime_9(float value)
	{
		___U3CU24U3Etime_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
