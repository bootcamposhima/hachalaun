﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "AssemblyU2DCSharp_Charctor1500651146.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Player
struct  Player_t2393081601  : public Charctor_t1500651146
{
public:
	// System.Single Player::m_y
	float ___m_y_10;
	// System.Int32 Player::_movestate
	int32_t ____movestate_11;
	// System.Single Player::_anim
	float ____anim_12;
	// System.Boolean Player::isanim
	bool ___isanim_13;
	// System.Boolean Player::isfight
	bool ___isfight_14;
	// System.Boolean Player::isPlayer
	bool ___isPlayer_15;
	// System.Single Player::_time
	float ____time_16;
	// UnityEngine.Color Player::_initcolor
	Color_t4194546905  ____initcolor_17;
	// System.Int32 Player::_hp
	int32_t ____hp_18;
	// System.Int32 Player::_recoverylife
	int32_t ____recoverylife_19;
	// UnityEngine.GameObject Player::_hpgauge
	GameObject_t3674682005 * ____hpgauge_20;
	// UnityEngine.GameObject Player::_hptext
	GameObject_t3674682005 * ____hptext_21;
	// System.Int32 Player::calldelaymove
	int32_t ___calldelaymove_22;

public:
	inline static int32_t get_offset_of_m_y_10() { return static_cast<int32_t>(offsetof(Player_t2393081601, ___m_y_10)); }
	inline float get_m_y_10() const { return ___m_y_10; }
	inline float* get_address_of_m_y_10() { return &___m_y_10; }
	inline void set_m_y_10(float value)
	{
		___m_y_10 = value;
	}

	inline static int32_t get_offset_of__movestate_11() { return static_cast<int32_t>(offsetof(Player_t2393081601, ____movestate_11)); }
	inline int32_t get__movestate_11() const { return ____movestate_11; }
	inline int32_t* get_address_of__movestate_11() { return &____movestate_11; }
	inline void set__movestate_11(int32_t value)
	{
		____movestate_11 = value;
	}

	inline static int32_t get_offset_of__anim_12() { return static_cast<int32_t>(offsetof(Player_t2393081601, ____anim_12)); }
	inline float get__anim_12() const { return ____anim_12; }
	inline float* get_address_of__anim_12() { return &____anim_12; }
	inline void set__anim_12(float value)
	{
		____anim_12 = value;
	}

	inline static int32_t get_offset_of_isanim_13() { return static_cast<int32_t>(offsetof(Player_t2393081601, ___isanim_13)); }
	inline bool get_isanim_13() const { return ___isanim_13; }
	inline bool* get_address_of_isanim_13() { return &___isanim_13; }
	inline void set_isanim_13(bool value)
	{
		___isanim_13 = value;
	}

	inline static int32_t get_offset_of_isfight_14() { return static_cast<int32_t>(offsetof(Player_t2393081601, ___isfight_14)); }
	inline bool get_isfight_14() const { return ___isfight_14; }
	inline bool* get_address_of_isfight_14() { return &___isfight_14; }
	inline void set_isfight_14(bool value)
	{
		___isfight_14 = value;
	}

	inline static int32_t get_offset_of_isPlayer_15() { return static_cast<int32_t>(offsetof(Player_t2393081601, ___isPlayer_15)); }
	inline bool get_isPlayer_15() const { return ___isPlayer_15; }
	inline bool* get_address_of_isPlayer_15() { return &___isPlayer_15; }
	inline void set_isPlayer_15(bool value)
	{
		___isPlayer_15 = value;
	}

	inline static int32_t get_offset_of__time_16() { return static_cast<int32_t>(offsetof(Player_t2393081601, ____time_16)); }
	inline float get__time_16() const { return ____time_16; }
	inline float* get_address_of__time_16() { return &____time_16; }
	inline void set__time_16(float value)
	{
		____time_16 = value;
	}

	inline static int32_t get_offset_of__initcolor_17() { return static_cast<int32_t>(offsetof(Player_t2393081601, ____initcolor_17)); }
	inline Color_t4194546905  get__initcolor_17() const { return ____initcolor_17; }
	inline Color_t4194546905 * get_address_of__initcolor_17() { return &____initcolor_17; }
	inline void set__initcolor_17(Color_t4194546905  value)
	{
		____initcolor_17 = value;
	}

	inline static int32_t get_offset_of__hp_18() { return static_cast<int32_t>(offsetof(Player_t2393081601, ____hp_18)); }
	inline int32_t get__hp_18() const { return ____hp_18; }
	inline int32_t* get_address_of__hp_18() { return &____hp_18; }
	inline void set__hp_18(int32_t value)
	{
		____hp_18 = value;
	}

	inline static int32_t get_offset_of__recoverylife_19() { return static_cast<int32_t>(offsetof(Player_t2393081601, ____recoverylife_19)); }
	inline int32_t get__recoverylife_19() const { return ____recoverylife_19; }
	inline int32_t* get_address_of__recoverylife_19() { return &____recoverylife_19; }
	inline void set__recoverylife_19(int32_t value)
	{
		____recoverylife_19 = value;
	}

	inline static int32_t get_offset_of__hpgauge_20() { return static_cast<int32_t>(offsetof(Player_t2393081601, ____hpgauge_20)); }
	inline GameObject_t3674682005 * get__hpgauge_20() const { return ____hpgauge_20; }
	inline GameObject_t3674682005 ** get_address_of__hpgauge_20() { return &____hpgauge_20; }
	inline void set__hpgauge_20(GameObject_t3674682005 * value)
	{
		____hpgauge_20 = value;
		Il2CppCodeGenWriteBarrier(&____hpgauge_20, value);
	}

	inline static int32_t get_offset_of__hptext_21() { return static_cast<int32_t>(offsetof(Player_t2393081601, ____hptext_21)); }
	inline GameObject_t3674682005 * get__hptext_21() const { return ____hptext_21; }
	inline GameObject_t3674682005 ** get_address_of__hptext_21() { return &____hptext_21; }
	inline void set__hptext_21(GameObject_t3674682005 * value)
	{
		____hptext_21 = value;
		Il2CppCodeGenWriteBarrier(&____hptext_21, value);
	}

	inline static int32_t get_offset_of_calldelaymove_22() { return static_cast<int32_t>(offsetof(Player_t2393081601, ___calldelaymove_22)); }
	inline int32_t get_calldelaymove_22() const { return ___calldelaymove_22; }
	inline int32_t* get_address_of_calldelaymove_22() { return &___calldelaymove_22; }
	inline void set_calldelaymove_22(int32_t value)
	{
		___calldelaymove_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
