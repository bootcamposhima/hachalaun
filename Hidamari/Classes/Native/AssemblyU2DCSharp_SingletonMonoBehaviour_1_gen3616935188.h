﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TransitionAnimation
struct TransitionAnimation_t4286760335;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SingletonMonoBehaviour`1<TransitionAnimation>
struct  SingletonMonoBehaviour_1_t3616935188  : public MonoBehaviour_t667441552
{
public:

public:
};

struct SingletonMonoBehaviour_1_t3616935188_StaticFields
{
public:
	// T SingletonMonoBehaviour`1::instance
	TransitionAnimation_t4286760335 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(SingletonMonoBehaviour_1_t3616935188_StaticFields, ___instance_2)); }
	inline TransitionAnimation_t4286760335 * get_instance_2() const { return ___instance_2; }
	inline TransitionAnimation_t4286760335 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(TransitionAnimation_t4286760335 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
