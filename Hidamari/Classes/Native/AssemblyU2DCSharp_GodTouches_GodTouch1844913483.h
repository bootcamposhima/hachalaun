﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GodTouches.GodTouch
struct  GodTouch_t1844913483  : public Il2CppObject
{
public:

public:
};

struct GodTouch_t1844913483_StaticFields
{
public:
	// System.Boolean GodTouches.GodTouch::IsAndroid
	bool ___IsAndroid_0;
	// System.Boolean GodTouches.GodTouch::IsIOS
	bool ___IsIOS_1;
	// System.Boolean GodTouches.GodTouch::IsEditor
	bool ___IsEditor_2;
	// UnityEngine.Vector3 GodTouches.GodTouch::prebPosition
	Vector3_t4282066566  ___prebPosition_3;

public:
	inline static int32_t get_offset_of_IsAndroid_0() { return static_cast<int32_t>(offsetof(GodTouch_t1844913483_StaticFields, ___IsAndroid_0)); }
	inline bool get_IsAndroid_0() const { return ___IsAndroid_0; }
	inline bool* get_address_of_IsAndroid_0() { return &___IsAndroid_0; }
	inline void set_IsAndroid_0(bool value)
	{
		___IsAndroid_0 = value;
	}

	inline static int32_t get_offset_of_IsIOS_1() { return static_cast<int32_t>(offsetof(GodTouch_t1844913483_StaticFields, ___IsIOS_1)); }
	inline bool get_IsIOS_1() const { return ___IsIOS_1; }
	inline bool* get_address_of_IsIOS_1() { return &___IsIOS_1; }
	inline void set_IsIOS_1(bool value)
	{
		___IsIOS_1 = value;
	}

	inline static int32_t get_offset_of_IsEditor_2() { return static_cast<int32_t>(offsetof(GodTouch_t1844913483_StaticFields, ___IsEditor_2)); }
	inline bool get_IsEditor_2() const { return ___IsEditor_2; }
	inline bool* get_address_of_IsEditor_2() { return &___IsEditor_2; }
	inline void set_IsEditor_2(bool value)
	{
		___IsEditor_2 = value;
	}

	inline static int32_t get_offset_of_prebPosition_3() { return static_cast<int32_t>(offsetof(GodTouch_t1844913483_StaticFields, ___prebPosition_3)); }
	inline Vector3_t4282066566  get_prebPosition_3() const { return ___prebPosition_3; }
	inline Vector3_t4282066566 * get_address_of_prebPosition_3() { return &___prebPosition_3; }
	inline void set_prebPosition_3(Vector3_t4282066566  value)
	{
		___prebPosition_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
