﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<WebSocketSharp.CompressionMethod>
struct DefaultComparer_t674373448;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_WebSocketSharp_CompressionMethod2226596781.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<WebSocketSharp.CompressionMethod>::.ctor()
extern "C"  void DefaultComparer__ctor_m2165804008_gshared (DefaultComparer_t674373448 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2165804008(__this, method) ((  void (*) (DefaultComparer_t674373448 *, const MethodInfo*))DefaultComparer__ctor_m2165804008_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<WebSocketSharp.CompressionMethod>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3789286923_gshared (DefaultComparer_t674373448 * __this, uint8_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m3789286923(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t674373448 *, uint8_t, const MethodInfo*))DefaultComparer_GetHashCode_m3789286923_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<WebSocketSharp.CompressionMethod>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3057958909_gshared (DefaultComparer_t674373448 * __this, uint8_t ___x0, uint8_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m3057958909(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t674373448 *, uint8_t, uint8_t, const MethodInfo*))DefaultComparer_Equals_m3057958909_gshared)(__this, ___x0, ___y1, method)
