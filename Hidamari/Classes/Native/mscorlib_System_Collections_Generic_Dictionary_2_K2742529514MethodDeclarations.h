﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2652585416MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.Byte[]>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m791861473(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t2742529514 *, Dictionary_2_t1115770063 *, const MethodInfo*))KeyCollection__ctor_m293092086_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.Byte[]>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2136258517(__this, ___item0, method) ((  void (*) (KeyCollection_t2742529514 *, uint8_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1919261856_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.Byte[]>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3818966668(__this, method) ((  void (*) (KeyCollection_t2742529514 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1923770903_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.Byte[]>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3038230041(__this, ___item0, method) ((  bool (*) (KeyCollection_t2742529514 *, uint8_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2539460654_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.Byte[]>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3655129534(__this, ___item0, method) ((  bool (*) (KeyCollection_t2742529514 *, uint8_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3033382163_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.Byte[]>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3820922974(__this, method) ((  Il2CppObject* (*) (KeyCollection_t2742529514 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1243973865_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.Byte[]>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m317014270(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2742529514 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m847438857_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.Byte[]>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1536686605(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2742529514 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1790171608_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.Byte[]>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2042366778(__this, method) ((  bool (*) (KeyCollection_t2742529514 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3761323023_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.Byte[]>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4169004140(__this, method) ((  bool (*) (KeyCollection_t2742529514 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m89996161_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.Byte[]>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m169629982(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2742529514 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1701827571_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.Byte[]>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m468976534(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2742529514 *, CompressionMethodU5BU5D_t88594176*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2229205419_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.Byte[]>::GetEnumerator()
#define KeyCollection_GetEnumerator_m3193110115(__this, method) ((  Enumerator_t1730706117  (*) (KeyCollection_t2742529514 *, const MethodInfo*))KeyCollection_GetEnumerator_m1444326392_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.Byte[]>::get_Count()
#define KeyCollection_get_Count_m847845670(__this, method) ((  int32_t (*) (KeyCollection_t2742529514 *, const MethodInfo*))KeyCollection_get_Count_m3976517947_gshared)(__this, method)
