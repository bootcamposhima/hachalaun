﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestJson
struct  TestJson_t3212538042 
{
public:
	// System.Single TestJson::num
	float ___num_0;

public:
	inline static int32_t get_offset_of_num_0() { return static_cast<int32_t>(offsetof(TestJson_t3212538042, ___num_0)); }
	inline float get_num_0() const { return ___num_0; }
	inline float* get_address_of_num_0() { return &___num_0; }
	inline void set_num_0(float value)
	{
		___num_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: TestJson
struct TestJson_t3212538042_marshaled_pinvoke
{
	float ___num_0;
};
// Native definition for marshalling of: TestJson
struct TestJson_t3212538042_marshaled_com
{
	float ___num_0;
};
