﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_ValueType1744280289.h"
#include "AssemblyU2DCSharp_SummarizeJson970273193.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MessageJson
struct  MessageJson_t794471791 
{
public:
	// System.String MessageJson::id
	String_t* ___id_0;
	// SummarizeJson MessageJson::value
	SummarizeJson_t970273193  ___value_1;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(MessageJson_t794471791, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier(&___id_0, value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(MessageJson_t794471791, ___value_1)); }
	inline SummarizeJson_t970273193  get_value_1() const { return ___value_1; }
	inline SummarizeJson_t970273193 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(SummarizeJson_t970273193  value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: MessageJson
struct MessageJson_t794471791_marshaled_pinvoke
{
	char* ___id_0;
	SummarizeJson_t970273193_marshaled_pinvoke ___value_1;
};
// Native definition for marshalling of: MessageJson
struct MessageJson_t794471791_marshaled_com
{
	Il2CppChar* ___id_0;
	SummarizeJson_t970273193_marshaled_com ___value_1;
};
