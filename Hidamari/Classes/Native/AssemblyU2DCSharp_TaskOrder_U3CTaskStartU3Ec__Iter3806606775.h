﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// TaskOrder
struct TaskOrder_t2170039465;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TaskOrder/<TaskStart>c__Iterator1
struct  U3CTaskStartU3Ec__Iterator1_t3806606775  : public Il2CppObject
{
public:
	// System.Int32 TaskOrder/<TaskStart>c__Iterator1::<count>__0
	int32_t ___U3CcountU3E__0_0;
	// System.Int32 TaskOrder/<TaskStart>c__Iterator1::<j>__1
	int32_t ___U3CjU3E__1_1;
	// System.Int32 TaskOrder/<TaskStart>c__Iterator1::<j>__2
	int32_t ___U3CjU3E__2_2;
	// System.Int32 TaskOrder/<TaskStart>c__Iterator1::$PC
	int32_t ___U24PC_3;
	// System.Object TaskOrder/<TaskStart>c__Iterator1::$current
	Il2CppObject * ___U24current_4;
	// TaskOrder TaskOrder/<TaskStart>c__Iterator1::<>f__this
	TaskOrder_t2170039465 * ___U3CU3Ef__this_5;

public:
	inline static int32_t get_offset_of_U3CcountU3E__0_0() { return static_cast<int32_t>(offsetof(U3CTaskStartU3Ec__Iterator1_t3806606775, ___U3CcountU3E__0_0)); }
	inline int32_t get_U3CcountU3E__0_0() const { return ___U3CcountU3E__0_0; }
	inline int32_t* get_address_of_U3CcountU3E__0_0() { return &___U3CcountU3E__0_0; }
	inline void set_U3CcountU3E__0_0(int32_t value)
	{
		___U3CcountU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CjU3E__1_1() { return static_cast<int32_t>(offsetof(U3CTaskStartU3Ec__Iterator1_t3806606775, ___U3CjU3E__1_1)); }
	inline int32_t get_U3CjU3E__1_1() const { return ___U3CjU3E__1_1; }
	inline int32_t* get_address_of_U3CjU3E__1_1() { return &___U3CjU3E__1_1; }
	inline void set_U3CjU3E__1_1(int32_t value)
	{
		___U3CjU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CjU3E__2_2() { return static_cast<int32_t>(offsetof(U3CTaskStartU3Ec__Iterator1_t3806606775, ___U3CjU3E__2_2)); }
	inline int32_t get_U3CjU3E__2_2() const { return ___U3CjU3E__2_2; }
	inline int32_t* get_address_of_U3CjU3E__2_2() { return &___U3CjU3E__2_2; }
	inline void set_U3CjU3E__2_2(int32_t value)
	{
		___U3CjU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CTaskStartU3Ec__Iterator1_t3806606775, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CTaskStartU3Ec__Iterator1_t3806606775, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_5() { return static_cast<int32_t>(offsetof(U3CTaskStartU3Ec__Iterator1_t3806606775, ___U3CU3Ef__this_5)); }
	inline TaskOrder_t2170039465 * get_U3CU3Ef__this_5() const { return ___U3CU3Ef__this_5; }
	inline TaskOrder_t2170039465 ** get_address_of_U3CU3Ef__this_5() { return &___U3CU3Ef__this_5; }
	inline void set_U3CU3Ef__this_5(TaskOrder_t2170039465 * value)
	{
		___U3CU3Ef__this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
