﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InterimResult
struct  InterimResult_t3822800669  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject InterimResult::RoundText
	GameObject_t3674682005 * ___RoundText_2;
	// UnityEngine.GameObject InterimResult::player1
	GameObject_t3674682005 * ___player1_3;
	// UnityEngine.GameObject InterimResult::player2
	GameObject_t3674682005 * ___player2_4;
	// UnityEngine.GameObject InterimResult::UIcamera
	GameObject_t3674682005 * ___UIcamera_5;
	// UnityEngine.GameObject InterimResult::Resultcamera
	GameObject_t3674682005 * ___Resultcamera_6;
	// System.Boolean InterimResult::isWin
	bool ___isWin_7;
	// System.Int32 InterimResult::nowRound
	int32_t ___nowRound_8;

public:
	inline static int32_t get_offset_of_RoundText_2() { return static_cast<int32_t>(offsetof(InterimResult_t3822800669, ___RoundText_2)); }
	inline GameObject_t3674682005 * get_RoundText_2() const { return ___RoundText_2; }
	inline GameObject_t3674682005 ** get_address_of_RoundText_2() { return &___RoundText_2; }
	inline void set_RoundText_2(GameObject_t3674682005 * value)
	{
		___RoundText_2 = value;
		Il2CppCodeGenWriteBarrier(&___RoundText_2, value);
	}

	inline static int32_t get_offset_of_player1_3() { return static_cast<int32_t>(offsetof(InterimResult_t3822800669, ___player1_3)); }
	inline GameObject_t3674682005 * get_player1_3() const { return ___player1_3; }
	inline GameObject_t3674682005 ** get_address_of_player1_3() { return &___player1_3; }
	inline void set_player1_3(GameObject_t3674682005 * value)
	{
		___player1_3 = value;
		Il2CppCodeGenWriteBarrier(&___player1_3, value);
	}

	inline static int32_t get_offset_of_player2_4() { return static_cast<int32_t>(offsetof(InterimResult_t3822800669, ___player2_4)); }
	inline GameObject_t3674682005 * get_player2_4() const { return ___player2_4; }
	inline GameObject_t3674682005 ** get_address_of_player2_4() { return &___player2_4; }
	inline void set_player2_4(GameObject_t3674682005 * value)
	{
		___player2_4 = value;
		Il2CppCodeGenWriteBarrier(&___player2_4, value);
	}

	inline static int32_t get_offset_of_UIcamera_5() { return static_cast<int32_t>(offsetof(InterimResult_t3822800669, ___UIcamera_5)); }
	inline GameObject_t3674682005 * get_UIcamera_5() const { return ___UIcamera_5; }
	inline GameObject_t3674682005 ** get_address_of_UIcamera_5() { return &___UIcamera_5; }
	inline void set_UIcamera_5(GameObject_t3674682005 * value)
	{
		___UIcamera_5 = value;
		Il2CppCodeGenWriteBarrier(&___UIcamera_5, value);
	}

	inline static int32_t get_offset_of_Resultcamera_6() { return static_cast<int32_t>(offsetof(InterimResult_t3822800669, ___Resultcamera_6)); }
	inline GameObject_t3674682005 * get_Resultcamera_6() const { return ___Resultcamera_6; }
	inline GameObject_t3674682005 ** get_address_of_Resultcamera_6() { return &___Resultcamera_6; }
	inline void set_Resultcamera_6(GameObject_t3674682005 * value)
	{
		___Resultcamera_6 = value;
		Il2CppCodeGenWriteBarrier(&___Resultcamera_6, value);
	}

	inline static int32_t get_offset_of_isWin_7() { return static_cast<int32_t>(offsetof(InterimResult_t3822800669, ___isWin_7)); }
	inline bool get_isWin_7() const { return ___isWin_7; }
	inline bool* get_address_of_isWin_7() { return &___isWin_7; }
	inline void set_isWin_7(bool value)
	{
		___isWin_7 = value;
	}

	inline static int32_t get_offset_of_nowRound_8() { return static_cast<int32_t>(offsetof(InterimResult_t3822800669, ___nowRound_8)); }
	inline int32_t get_nowRound_8() const { return ___nowRound_8; }
	inline int32_t* get_address_of_nowRound_8() { return &___nowRound_8; }
	inline void set_nowRound_8(int32_t value)
	{
		___nowRound_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
