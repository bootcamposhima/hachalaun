﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.UI.Text
struct Text_t9039225;
// FightSocket
struct FightSocket_t1340804995;

#include "AssemblyU2DCSharp_Round79151470.h"
#include "AssemblyU2DCSharp_AIDatas4008896193.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FightRound
struct  FightRound_t2951839614  : public Round_t79151470
{
public:
	// UnityEngine.GameObject FightRound::_stopbutton
	GameObject_t3674682005 * ____stopbutton_11;
	// UnityEngine.GameObject FightRound::_retiretext
	GameObject_t3674682005 * ____retiretext_12;
	// UnityEngine.UI.Text FightRound::_text
	Text_t9039225 * ____text_13;
	// System.Int32 FightRound::decision
	int32_t ___decision_14;
	// System.Int32 FightRound::GameCount
	int32_t ___GameCount_15;
	// System.Boolean FightRound::isdisconnection
	bool ___isdisconnection_16;
	// System.Boolean FightRound::isretire
	bool ___isretire_17;
	// System.Boolean FightRound::isNextGame
	bool ___isNextGame_18;
	// System.Boolean FightRound::_receptionAI
	bool ____receptionAI_19;
	// System.Boolean FightRound::_receptionscore
	bool ____receptionscore_20;
	// System.Boolean FightRound::_sendAI
	bool ____sendAI_21;
	// System.Boolean FightRound::_sendscore
	bool ____sendscore_22;
	// System.Boolean FightRound::isstart
	bool ___isstart_23;
	// AIDatas FightRound::_enemyAi
	AIDatas_t4008896193  ____enemyAi_24;
	// System.Int32 FightRound::_charnum
	int32_t ____charnum_25;
	// FightSocket FightRound::_fm
	FightSocket_t1340804995 * ____fm_26;

public:
	inline static int32_t get_offset_of__stopbutton_11() { return static_cast<int32_t>(offsetof(FightRound_t2951839614, ____stopbutton_11)); }
	inline GameObject_t3674682005 * get__stopbutton_11() const { return ____stopbutton_11; }
	inline GameObject_t3674682005 ** get_address_of__stopbutton_11() { return &____stopbutton_11; }
	inline void set__stopbutton_11(GameObject_t3674682005 * value)
	{
		____stopbutton_11 = value;
		Il2CppCodeGenWriteBarrier(&____stopbutton_11, value);
	}

	inline static int32_t get_offset_of__retiretext_12() { return static_cast<int32_t>(offsetof(FightRound_t2951839614, ____retiretext_12)); }
	inline GameObject_t3674682005 * get__retiretext_12() const { return ____retiretext_12; }
	inline GameObject_t3674682005 ** get_address_of__retiretext_12() { return &____retiretext_12; }
	inline void set__retiretext_12(GameObject_t3674682005 * value)
	{
		____retiretext_12 = value;
		Il2CppCodeGenWriteBarrier(&____retiretext_12, value);
	}

	inline static int32_t get_offset_of__text_13() { return static_cast<int32_t>(offsetof(FightRound_t2951839614, ____text_13)); }
	inline Text_t9039225 * get__text_13() const { return ____text_13; }
	inline Text_t9039225 ** get_address_of__text_13() { return &____text_13; }
	inline void set__text_13(Text_t9039225 * value)
	{
		____text_13 = value;
		Il2CppCodeGenWriteBarrier(&____text_13, value);
	}

	inline static int32_t get_offset_of_decision_14() { return static_cast<int32_t>(offsetof(FightRound_t2951839614, ___decision_14)); }
	inline int32_t get_decision_14() const { return ___decision_14; }
	inline int32_t* get_address_of_decision_14() { return &___decision_14; }
	inline void set_decision_14(int32_t value)
	{
		___decision_14 = value;
	}

	inline static int32_t get_offset_of_GameCount_15() { return static_cast<int32_t>(offsetof(FightRound_t2951839614, ___GameCount_15)); }
	inline int32_t get_GameCount_15() const { return ___GameCount_15; }
	inline int32_t* get_address_of_GameCount_15() { return &___GameCount_15; }
	inline void set_GameCount_15(int32_t value)
	{
		___GameCount_15 = value;
	}

	inline static int32_t get_offset_of_isdisconnection_16() { return static_cast<int32_t>(offsetof(FightRound_t2951839614, ___isdisconnection_16)); }
	inline bool get_isdisconnection_16() const { return ___isdisconnection_16; }
	inline bool* get_address_of_isdisconnection_16() { return &___isdisconnection_16; }
	inline void set_isdisconnection_16(bool value)
	{
		___isdisconnection_16 = value;
	}

	inline static int32_t get_offset_of_isretire_17() { return static_cast<int32_t>(offsetof(FightRound_t2951839614, ___isretire_17)); }
	inline bool get_isretire_17() const { return ___isretire_17; }
	inline bool* get_address_of_isretire_17() { return &___isretire_17; }
	inline void set_isretire_17(bool value)
	{
		___isretire_17 = value;
	}

	inline static int32_t get_offset_of_isNextGame_18() { return static_cast<int32_t>(offsetof(FightRound_t2951839614, ___isNextGame_18)); }
	inline bool get_isNextGame_18() const { return ___isNextGame_18; }
	inline bool* get_address_of_isNextGame_18() { return &___isNextGame_18; }
	inline void set_isNextGame_18(bool value)
	{
		___isNextGame_18 = value;
	}

	inline static int32_t get_offset_of__receptionAI_19() { return static_cast<int32_t>(offsetof(FightRound_t2951839614, ____receptionAI_19)); }
	inline bool get__receptionAI_19() const { return ____receptionAI_19; }
	inline bool* get_address_of__receptionAI_19() { return &____receptionAI_19; }
	inline void set__receptionAI_19(bool value)
	{
		____receptionAI_19 = value;
	}

	inline static int32_t get_offset_of__receptionscore_20() { return static_cast<int32_t>(offsetof(FightRound_t2951839614, ____receptionscore_20)); }
	inline bool get__receptionscore_20() const { return ____receptionscore_20; }
	inline bool* get_address_of__receptionscore_20() { return &____receptionscore_20; }
	inline void set__receptionscore_20(bool value)
	{
		____receptionscore_20 = value;
	}

	inline static int32_t get_offset_of__sendAI_21() { return static_cast<int32_t>(offsetof(FightRound_t2951839614, ____sendAI_21)); }
	inline bool get__sendAI_21() const { return ____sendAI_21; }
	inline bool* get_address_of__sendAI_21() { return &____sendAI_21; }
	inline void set__sendAI_21(bool value)
	{
		____sendAI_21 = value;
	}

	inline static int32_t get_offset_of__sendscore_22() { return static_cast<int32_t>(offsetof(FightRound_t2951839614, ____sendscore_22)); }
	inline bool get__sendscore_22() const { return ____sendscore_22; }
	inline bool* get_address_of__sendscore_22() { return &____sendscore_22; }
	inline void set__sendscore_22(bool value)
	{
		____sendscore_22 = value;
	}

	inline static int32_t get_offset_of_isstart_23() { return static_cast<int32_t>(offsetof(FightRound_t2951839614, ___isstart_23)); }
	inline bool get_isstart_23() const { return ___isstart_23; }
	inline bool* get_address_of_isstart_23() { return &___isstart_23; }
	inline void set_isstart_23(bool value)
	{
		___isstart_23 = value;
	}

	inline static int32_t get_offset_of__enemyAi_24() { return static_cast<int32_t>(offsetof(FightRound_t2951839614, ____enemyAi_24)); }
	inline AIDatas_t4008896193  get__enemyAi_24() const { return ____enemyAi_24; }
	inline AIDatas_t4008896193 * get_address_of__enemyAi_24() { return &____enemyAi_24; }
	inline void set__enemyAi_24(AIDatas_t4008896193  value)
	{
		____enemyAi_24 = value;
	}

	inline static int32_t get_offset_of__charnum_25() { return static_cast<int32_t>(offsetof(FightRound_t2951839614, ____charnum_25)); }
	inline int32_t get__charnum_25() const { return ____charnum_25; }
	inline int32_t* get_address_of__charnum_25() { return &____charnum_25; }
	inline void set__charnum_25(int32_t value)
	{
		____charnum_25 = value;
	}

	inline static int32_t get_offset_of__fm_26() { return static_cast<int32_t>(offsetof(FightRound_t2951839614, ____fm_26)); }
	inline FightSocket_t1340804995 * get__fm_26() const { return ____fm_26; }
	inline FightSocket_t1340804995 ** get_address_of__fm_26() { return &____fm_26; }
	inline void set__fm_26(FightSocket_t1340804995 * value)
	{
		____fm_26 = value;
		Il2CppCodeGenWriteBarrier(&____fm_26, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
