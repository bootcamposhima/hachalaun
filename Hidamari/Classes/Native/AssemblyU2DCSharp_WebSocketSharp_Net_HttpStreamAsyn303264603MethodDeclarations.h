﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.Net.HttpStreamAsyncResult
struct HttpStreamAsyncResult_t303264603;
// System.AsyncCallback
struct AsyncCallback_t1369114871;
// System.Object
struct Il2CppObject;
// System.Threading.WaitHandle
struct WaitHandle_t1661568373;
// System.Exception
struct Exception_t3991598821;
// System.IAsyncResult
struct IAsyncResult_t2754620036;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_AsyncCallback1369114871.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Exception3991598821.h"

// System.Void WebSocketSharp.Net.HttpStreamAsyncResult::.ctor(System.AsyncCallback,System.Object)
extern "C"  void HttpStreamAsyncResult__ctor_m608371090 (HttpStreamAsyncResult_t303264603 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object WebSocketSharp.Net.HttpStreamAsyncResult::get_AsyncState()
extern "C"  Il2CppObject * HttpStreamAsyncResult_get_AsyncState_m4204869073 (HttpStreamAsyncResult_t303264603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.WaitHandle WebSocketSharp.Net.HttpStreamAsyncResult::get_AsyncWaitHandle()
extern "C"  WaitHandle_t1661568373 * HttpStreamAsyncResult_get_AsyncWaitHandle_m2649483411 (HttpStreamAsyncResult_t303264603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.Net.HttpStreamAsyncResult::get_CompletedSynchronously()
extern "C"  bool HttpStreamAsyncResult_get_CompletedSynchronously_m1919835430 (HttpStreamAsyncResult_t303264603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.Net.HttpStreamAsyncResult::get_IsCompleted()
extern "C"  bool HttpStreamAsyncResult_get_IsCompleted_m180628584 (HttpStreamAsyncResult_t303264603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.HttpStreamAsyncResult::Complete()
extern "C"  void HttpStreamAsyncResult_Complete_m3737307543 (HttpStreamAsyncResult_t303264603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.HttpStreamAsyncResult::Complete(System.Exception)
extern "C"  void HttpStreamAsyncResult_Complete_m3893120071 (HttpStreamAsyncResult_t303264603 * __this, Exception_t3991598821 * ___exception0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.HttpStreamAsyncResult::<Complete>m__7(System.IAsyncResult)
extern "C"  void HttpStreamAsyncResult_U3CCompleteU3Em__7_m2387366066 (HttpStreamAsyncResult_t303264603 * __this, Il2CppObject * ___ar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
