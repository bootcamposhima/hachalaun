﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.Net.HttpListenerResponse/<Close>c__AnonStorey24
struct U3CCloseU3Ec__AnonStorey24_t788655901;
// System.IAsyncResult
struct IAsyncResult_t2754620036;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocketSharp.Net.HttpListenerResponse/<Close>c__AnonStorey24::.ctor()
extern "C"  void U3CCloseU3Ec__AnonStorey24__ctor_m619773470 (U3CCloseU3Ec__AnonStorey24_t788655901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.HttpListenerResponse/<Close>c__AnonStorey24::<>m__6(System.IAsyncResult)
extern "C"  void U3CCloseU3Ec__AnonStorey24_U3CU3Em__6_m2943737778 (U3CCloseU3Ec__AnonStorey24_t788655901 * __this, Il2CppObject * ___ar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
