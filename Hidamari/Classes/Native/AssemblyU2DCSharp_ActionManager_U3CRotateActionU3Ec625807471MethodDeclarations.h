﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ActionManager/<RotateAction>c__Iterator7
struct U3CRotateActionU3Ec__Iterator7_t625807471;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ActionManager/<RotateAction>c__Iterator7::.ctor()
extern "C"  void U3CRotateActionU3Ec__Iterator7__ctor_m2191481612 (U3CRotateActionU3Ec__Iterator7_t625807471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ActionManager/<RotateAction>c__Iterator7::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CRotateActionU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4044147024 (U3CRotateActionU3Ec__Iterator7_t625807471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ActionManager/<RotateAction>c__Iterator7::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CRotateActionU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m1595632868 (U3CRotateActionU3Ec__Iterator7_t625807471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ActionManager/<RotateAction>c__Iterator7::MoveNext()
extern "C"  bool U3CRotateActionU3Ec__Iterator7_MoveNext_m1590733648 (U3CRotateActionU3Ec__Iterator7_t625807471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActionManager/<RotateAction>c__Iterator7::Dispose()
extern "C"  void U3CRotateActionU3Ec__Iterator7_Dispose_m1378137161 (U3CRotateActionU3Ec__Iterator7_t625807471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActionManager/<RotateAction>c__Iterator7::Reset()
extern "C"  void U3CRotateActionU3Ec__Iterator7_Reset_m4132881849 (U3CRotateActionU3Ec__Iterator7_t625807471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
