﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BigballBullet
struct BigballBullet_t3675719873;

#include "codegen/il2cpp-codegen.h"

// System.Void BigballBullet::.ctor()
extern "C"  void BigballBullet__ctor_m208314826 (BigballBullet_t3675719873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BigballBullet::Start()
extern "C"  void BigballBullet_Start_m3450419914 (BigballBullet_t3675719873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BigballBullet::Update()
extern "C"  void BigballBullet_Update_m3889654403 (BigballBullet_t3675719873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
