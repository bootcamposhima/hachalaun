﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// FightRound
struct FightRound_t2951839614;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FightRound/<RoundTimeAction>c__IteratorF
struct  U3CRoundTimeActionU3Ec__IteratorF_t3629899821  : public Il2CppObject
{
public:
	// System.Single FightRound/<RoundTimeAction>c__IteratorF::<time>__0
	float ___U3CtimeU3E__0_0;
	// System.Int32 FightRound/<RoundTimeAction>c__IteratorF::<t>__1
	int32_t ___U3CtU3E__1_1;
	// System.Int32 FightRound/<RoundTimeAction>c__IteratorF::$PC
	int32_t ___U24PC_2;
	// System.Object FightRound/<RoundTimeAction>c__IteratorF::$current
	Il2CppObject * ___U24current_3;
	// FightRound FightRound/<RoundTimeAction>c__IteratorF::<>f__this
	FightRound_t2951839614 * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_U3CtimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CRoundTimeActionU3Ec__IteratorF_t3629899821, ___U3CtimeU3E__0_0)); }
	inline float get_U3CtimeU3E__0_0() const { return ___U3CtimeU3E__0_0; }
	inline float* get_address_of_U3CtimeU3E__0_0() { return &___U3CtimeU3E__0_0; }
	inline void set_U3CtimeU3E__0_0(float value)
	{
		___U3CtimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E__1_1() { return static_cast<int32_t>(offsetof(U3CRoundTimeActionU3Ec__IteratorF_t3629899821, ___U3CtU3E__1_1)); }
	inline int32_t get_U3CtU3E__1_1() const { return ___U3CtU3E__1_1; }
	inline int32_t* get_address_of_U3CtU3E__1_1() { return &___U3CtU3E__1_1; }
	inline void set_U3CtU3E__1_1(int32_t value)
	{
		___U3CtU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CRoundTimeActionU3Ec__IteratorF_t3629899821, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CRoundTimeActionU3Ec__IteratorF_t3629899821, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3CRoundTimeActionU3Ec__IteratorF_t3629899821, ___U3CU3Ef__this_4)); }
	inline FightRound_t2951839614 * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline FightRound_t2951839614 ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(FightRound_t2951839614 * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
