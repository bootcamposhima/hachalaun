﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey2F/<dump>c__AnonStorey30
struct U3CdumpU3Ec__AnonStorey30_t1917037014;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey2F/<dump>c__AnonStorey30::.ctor()
extern "C"  void U3CdumpU3Ec__AnonStorey30__ctor_m1114561621 (U3CdumpU3Ec__AnonStorey30_t1917037014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey2F/<dump>c__AnonStorey30::<>m__1B(System.String,System.String,System.String,System.String)
extern "C"  void U3CdumpU3Ec__AnonStorey30_U3CU3Em__1B_m412092807 (U3CdumpU3Ec__AnonStorey30_t1917037014 * __this, String_t* ___arg10, String_t* ___arg21, String_t* ___arg32, String_t* ___arg43, const MethodInfo* method) IL2CPP_METHOD_ATTR;
