﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m393180855(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3288930493 *, HttpConnection_t602292776 *, HttpConnection_t602292776 *, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>::get_Key()
#define KeyValuePair_2_get_Key_m4167930577(__this, method) ((  HttpConnection_t602292776 * (*) (KeyValuePair_2_t3288930493 *, const MethodInfo*))KeyValuePair_2_get_Key_m3256475977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m549473554(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3288930493 *, HttpConnection_t602292776 *, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>::get_Value()
#define KeyValuePair_2_get_Value_m2925900085(__this, method) ((  HttpConnection_t602292776 * (*) (KeyValuePair_2_t3288930493 *, const MethodInfo*))KeyValuePair_2_get_Value_m3899079597_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m4079667218(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3288930493 *, HttpConnection_t602292776 *, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>::ToString()
#define KeyValuePair_2_ToString_m1055426614(__this, method) ((  String_t* (*) (KeyValuePair_2_t3288930493 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
