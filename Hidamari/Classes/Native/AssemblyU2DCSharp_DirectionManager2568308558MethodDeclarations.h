﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DirectionManager
struct DirectionManager_t2568308558;

#include "codegen/il2cpp-codegen.h"

// System.Void DirectionManager::.ctor()
extern "C"  void DirectionManager__ctor_m2208315149 (DirectionManager_t2568308558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DirectionManager::Start()
extern "C"  void DirectionManager_Start_m1155452941 (DirectionManager_t2568308558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DirectionManager::Awake()
extern "C"  void DirectionManager_Awake_m2445920368 (DirectionManager_t2568308558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DirectionManager::Update()
extern "C"  void DirectionManager_Update_m1465154976 (DirectionManager_t2568308558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DirectionManager::set_DirectionButtonActive(System.Boolean)
extern "C"  void DirectionManager_set_DirectionButtonActive_m3883552572 (DirectionManager_t2568308558 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DirectionManager::UIChange()
extern "C"  void DirectionManager_UIChange_m741592027 (DirectionManager_t2568308558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DirectionManager::EntranceAction()
extern "C"  void DirectionManager_EntranceAction_m1476230819 (DirectionManager_t2568308558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DirectionManager::LeavingAction()
extern "C"  void DirectionManager_LeavingAction_m105358517 (DirectionManager_t2568308558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DirectionManager::UIStart()
extern "C"  void DirectionManager_UIStart_m1544211577 (DirectionManager_t2568308558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DirectionManager::UIEnd()
extern "C"  void DirectionManager_UIEnd_m1673413682 (DirectionManager_t2568308558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DirectionManager::ChangeButtonColor(System.Int32)
extern "C"  void DirectionManager_ChangeButtonColor_m2427023805 (DirectionManager_t2568308558 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DirectionManager::init()
extern "C"  void DirectionManager_init_m2601473415 (DirectionManager_t2568308558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
