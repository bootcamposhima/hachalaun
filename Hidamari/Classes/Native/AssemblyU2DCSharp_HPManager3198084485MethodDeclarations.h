﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HPManager
struct HPManager_t3198084485;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void HPManager::.ctor()
extern "C"  void HPManager__ctor_m3969226118 (HPManager_t3198084485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HPManager::Start()
extern "C"  void HPManager_Start_m2916363910 (HPManager_t3198084485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HPManager::Update()
extern "C"  void HPManager_Update_m218820167 (HPManager_t3198084485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HPManager::init(System.Int32)
extern "C"  void HPManager_init_m2499347519 (HPManager_t3198084485 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HPManager::Damage(System.Int32)
extern "C"  void HPManager_Damage_m3774359870 (HPManager_t3198084485 * __this, int32_t ___damage0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HPManager::SetHPActive(System.Boolean)
extern "C"  void HPManager_SetHPActive_m3727086443 (HPManager_t3198084485 * __this, bool ___flag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HPManager::ChangeObjects(System.Int32)
extern "C"  void HPManager_ChangeObjects_m3630285625 (HPManager_t3198084485 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator HPManager::DamagetAnimation()
extern "C"  Il2CppObject * HPManager_DamagetAnimation_m582320277 (HPManager_t3198084485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator HPManager::DamagetAnimation2()
extern "C"  Il2CppObject * HPManager_DamagetAnimation2_m872069023 (HPManager_t3198084485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HPManager::bomParticle(System.Int32,UnityEngine.Vector3)
extern "C"  void HPManager_bomParticle_m265155534 (HPManager_t3198084485 * __this, int32_t ___num0, Vector3_t4282066566  ___parpos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HPManager::StopBomPar0()
extern "C"  void HPManager_StopBomPar0_m3208133713 (HPManager_t3198084485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HPManager::StopBomPar1()
extern "C"  void HPManager_StopBomPar1_m3208134674 (HPManager_t3198084485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
