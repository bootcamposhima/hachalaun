﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.CompressionMethod,System.Object>
struct ValueCollection_t4021398974;
// System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>
struct Dictionary_2_t1025825965;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3252626669.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.CompressionMethod,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m2651207012_gshared (ValueCollection_t4021398974 * __this, Dictionary_2_t1025825965 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m2651207012(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t4021398974 *, Dictionary_2_t1025825965 *, const MethodInfo*))ValueCollection__ctor_m2651207012_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1617545806_gshared (ValueCollection_t4021398974 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1617545806(__this, ___item0, method) ((  void (*) (ValueCollection_t4021398974 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1617545806_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1259146775_gshared (ValueCollection_t4021398974 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1259146775(__this, method) ((  void (*) (ValueCollection_t4021398974 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1259146775_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2949579100_gshared (ValueCollection_t4021398974 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2949579100(__this, ___item0, method) ((  bool (*) (ValueCollection_t4021398974 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2949579100_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2716909825_gshared (ValueCollection_t4021398974 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2716909825(__this, ___item0, method) ((  bool (*) (ValueCollection_t4021398974 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2716909825_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1427265367_gshared (ValueCollection_t4021398974 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1427265367(__this, method) ((  Il2CppObject* (*) (ValueCollection_t4021398974 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1427265367_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m3444981339_gshared (ValueCollection_t4021398974 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m3444981339(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t4021398974 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3444981339_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m560456234_gshared (ValueCollection_t4021398974 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m560456234(__this, method) ((  Il2CppObject * (*) (ValueCollection_t4021398974 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m560456234_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1224754959_gshared (ValueCollection_t4021398974 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1224754959(__this, method) ((  bool (*) (ValueCollection_t4021398974 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1224754959_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1021668207_gshared (ValueCollection_t4021398974 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1021668207(__this, method) ((  bool (*) (ValueCollection_t4021398974 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1021668207_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m3513245089_gshared (ValueCollection_t4021398974 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m3513245089(__this, method) ((  Il2CppObject * (*) (ValueCollection_t4021398974 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m3513245089_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.CompressionMethod,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m1119338667_gshared (ValueCollection_t4021398974 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m1119338667(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t4021398974 *, ObjectU5BU5D_t1108656482*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1119338667_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.CompressionMethod,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3252626669  ValueCollection_GetEnumerator_m345604308_gshared (ValueCollection_t4021398974 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m345604308(__this, method) ((  Enumerator_t3252626669  (*) (ValueCollection_t4021398974 *, const MethodInfo*))ValueCollection_GetEnumerator_m345604308_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.CompressionMethod,System.Object>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m1761333417_gshared (ValueCollection_t4021398974 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m1761333417(__this, method) ((  int32_t (*) (ValueCollection_t4021398974 *, const MethodInfo*))ValueCollection_get_Count_m1761333417_gshared)(__this, method)
