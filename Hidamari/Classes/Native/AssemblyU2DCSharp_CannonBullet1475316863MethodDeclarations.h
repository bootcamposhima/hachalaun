﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CannonBullet
struct CannonBullet_t1475316863;

#include "codegen/il2cpp-codegen.h"

// System.Void CannonBullet::.ctor()
extern "C"  void CannonBullet__ctor_m117769468 (CannonBullet_t1475316863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CannonBullet::Start()
extern "C"  void CannonBullet_Start_m3359874556 (CannonBullet_t1475316863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CannonBullet::Update()
extern "C"  void CannonBullet_Update_m1082748305 (CannonBullet_t1475316863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
