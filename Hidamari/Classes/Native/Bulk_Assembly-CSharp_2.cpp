﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<>c__Iterator1D
struct U3CU3Ec__Iterator1D_t3823346890;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.String>
struct IEnumerator_1_t1919096606;
// WebSocketSharp.Net.WebSockets.WebSocketContext
struct WebSocketContext_t763725542;
// WebSocketSharp.PayloadData
struct PayloadData_t39926750;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Collections.Generic.IEnumerator`1<System.Byte>
struct IEnumerator_1_t479507413;
// WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator1E
struct U3CGetEnumeratorU3Ec__Iterator1E_t303122823;
// WebSocketSharp.WebSocket
struct WebSocket_t1342580397;
// WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext
struct HttpListenerWebSocketContext_t1074545506;
// WebSocketSharp.Logger
struct Logger_t3695440972;
// WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext
struct TcpListenerWebSocketContext_t3782393199;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.EventHandler`1<WebSocketSharp.CloseEventArgs>
struct EventHandler_1_t2615270477;
// System.EventHandler`1<WebSocketSharp.ErrorEventArgs>
struct EventHandler_1_t569145917;
// System.EventHandler`1<WebSocketSharp.MessageEventArgs>
struct EventHandler_1_t181896286;
// System.EventHandler
struct EventHandler_t2463957060;
// WebSocketSharp.Net.CookieCollection
struct CookieCollection_t1136277956;
// System.Func`2<WebSocketSharp.Net.WebSockets.WebSocketContext,System.String>
struct Func_2_t636757868;
// System.Collections.Generic.IEnumerable`1<WebSocketSharp.Net.Cookie>
struct IEnumerable_1_t1083031107;
// WebSocketSharp.Net.NetworkCredential
struct NetworkCredential_t1204099087;
// System.Net.Security.RemoteCertificateValidationCallback
struct RemoteCertificateValidationCallback_t1894914657;
// System.Uri
struct Uri_t1116831938;
// WebSocketSharp.WebSocketFrame
struct WebSocketFrame_t778194306;
// System.Exception
struct Exception_t3991598821;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t3308144514;
// System.Func`2<System.String,System.Boolean>
struct Func_2_t3730860138;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3176762032;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t785513668;
// WebSocketSharp.HandshakeResponse
struct HandshakeResponse_t3229697822;
// WebSocketSharp.CloseEventArgs
struct CloseEventArgs_t2470319931;
// System.EventHandler`1<System.Object>
struct EventHandler_1_t20799621;
// System.Action
struct Action_t3771233898;
// System.IO.Stream
struct Stream_t1561764144;
// WebSocketSharp.HandshakeRequest
struct HandshakeRequest_t1037477780;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// WebSocketSharp.MessageEventArgs
struct MessageEventArgs_t36945740;
// WebSocketSharp.ErrorEventArgs
struct ErrorEventArgs_t424195371;
// System.Action`1<System.Boolean>
struct Action_1_t872614854;
// System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Byte[]>
struct Dictionary_2_t1115770063;
// System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.IO.Stream>
struct Dictionary_2_t2711741034;
// System.IO.FileInfo
struct FileInfo_t3233670074;
// WebSocketSharp.Net.Cookie
struct Cookie_t2077085446;
// WebSocketSharp.WebSocket/<>c__Iterator1F
struct U3CU3Ec__Iterator1F_t90658481;
// System.Collections.Generic.IEnumerator`1<WebSocketSharp.Net.Cookie>
struct IEnumerator_1_t3988950495;
// WebSocketSharp.WebSocket/<closeAsync>c__AnonStorey28
struct U3CcloseAsyncU3Ec__AnonStorey28_t239095580;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// WebSocketSharp.WebSocket/<ConnectAsync>c__AnonStorey2D
struct U3CConnectAsyncU3Ec__AnonStorey2D_t4104301910;
// WebSocketSharp.WebSocket/<sendAsync>c__AnonStorey29
struct U3CsendAsyncU3Ec__AnonStorey29_t861700891;
// WebSocketSharp.WebSocket/<sendAsync>c__AnonStorey2A
struct U3CsendAsyncU3Ec__AnonStorey2A_t861700899;
// WebSocketSharp.WebSocket/<SendAsync>c__AnonStorey2E
struct U3CSendAsyncU3Ec__AnonStorey2E_t2907890439;
// WebSocketSharp.WebSocket/<startReceiving>c__AnonStorey2B
struct U3CstartReceivingU3Ec__AnonStorey2B_t802946016;
// WebSocketSharp.WebSocket/<validateSecWebSocketProtocolHeader>c__AnonStorey2C
struct U3CvalidateSecWebSocketProtocolHeaderU3Ec__AnonStorey2C_t467005428;
// WebSocketSharp.WebSocketException
struct WebSocketException_t2311987812;
// System.Action`1<WebSocketSharp.WebSocketFrame>
struct Action_1_t1174010442;
// System.Action`1<System.Exception>
struct Action_1_t92447661;
// WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey2F
struct U3CdumpU3Ec__AnonStorey2F_t681710991;
// System.Action`4<System.String,System.String,System.String,System.String>
struct Action_4_t828544145;
// WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey2F/<dump>c__AnonStorey30
struct U3CdumpU3Ec__AnonStorey30_t1917037014;
// WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator20
struct U3CGetEnumeratorU3Ec__Iterator20_t4155755117;
// WebSocketSharp.WebSocketFrame/<ParseAsync>c__AnonStorey31
struct U3CParseAsyncU3Ec__AnonStorey31_t1758512846;
// WebSocketSharp.WebSocketStream
struct WebSocketStream_t4103435597;
// System.Net.Sockets.NetworkStream
struct NetworkStream_t3953762560;
// WebSocketSharp.Net.Security.SslStream
struct SslStream_t3623155758;
// System.Net.Sockets.TcpClient
struct TcpClient_t838416830;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t3076817455;
// System.Func`2<System.String[],WebSocketSharp.HandshakeRequest>
struct Func_2_t3775863249;
// System.Func`2<System.String[],System.Object>
struct Func_2_t2614234544;
// System.Func`2<System.String[],WebSocketSharp.HandshakeResponse>
struct Func_2_t1673115995;
// WebSocketSharp.HandshakeBase
struct HandshakeBase_t1248407470;
// System.Security.Cryptography.X509Certificates.X509Chain
struct X509Chain_t1111884825;
// WebSocketSharp.WebSocketStream/<readHandshakeHeaders>c__AnonStorey32
struct U3CreadHandshakeHeadersU3Ec__AnonStorey32_t3526000246;
// West
struct West_t2692559;
// EnemyManager
struct EnemyManager_t1035150117;
// Bullet
struct Bullet_t2000900386;
// StagePanelManager
struct StagePanelManager_t334945031;
// ParticleManager
struct ParticleManager_t73248295;
// UnityEngine.ParticleSystem
struct ParticleSystem_t381473177;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_WebSockets_Tc3823346890.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_WebSockets_Tc3823346890MethodDeclarations.h"
#include "mscorlib_System_Void2863195528.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Threading_Interlocked373807572MethodDeclarations.h"
#include "mscorlib_System_Int321153838500.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_WebSockets_Tc3782393199.h"
#include "mscorlib_System_Boolean476798718.h"
#include "AssemblyU2DCSharp_WebSocketSharp_HandshakeBase1248407470MethodDeclarations.h"
#include "System_System_Collections_Specialized_NameValueCol2791941106MethodDeclarations.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "mscorlib_System_UInt3224667981.h"
#include "AssemblyU2DCSharp_WebSocketSharp_HandshakeRequest1037477780.h"
#include "System_System_Collections_Specialized_NameValueCol2791941106.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Char2862622538.h"
#include "mscorlib_System_NotSupportedException1732551818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1732551818.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_WebSockets_Web763725542.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_WebSockets_Web763725542MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Opcode3782140426.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Opcode3782140426MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketSharp_PayloadData39926750.h"
#include "AssemblyU2DCSharp_WebSocketSharp_PayloadData39926750MethodDeclarations.h"
#include "mscorlib_System_Byte2862609660.h"
#include "mscorlib_System_Text_Encoding2012439129MethodDeclarations.h"
#include "mscorlib_System_Text_Encoding2012439129.h"
#include "mscorlib_System_Array1146569071MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException3816648464MethodDeclarations.h"
#include "mscorlib_System_Int641153838595.h"
#include "mscorlib_System_ArgumentOutOfRangeException3816648464.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Ext3262160039MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Ext3262160039.h"
#include "mscorlib_System_UInt1624667923.h"
#include "AssemblyU2DCSharp_WebSocketSharp_ByteOrder422296044.h"
#include "mscorlib_System_UInt6424668076.h"
#include "AssemblyU2DCSharp_WebSocketSharp_PayloadData_U3CGet303122823MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketSharp_PayloadData_U3CGet303122823.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4230795212MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4230795212.h"
#include "mscorlib_System_BitConverter1260277767MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Rsv3262172379.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Rsv3262172379MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocket1342580397.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocket1342580397MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_WebSockets_Ht1074545506.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Logger3695440972.h"
#include "System_Core_System_Action3771233898MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_WebSockets_Ht1074545506MethodDeclarations.h"
#include "System_Core_System_Action3771233898.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocketStream4103435597.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_WebSockets_Tc3782393199MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException3573189601MethodDeclarations.h"
#include "mscorlib_System_ArgumentException928607144MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Logger3695440972MethodDeclarations.h"
#include "System_System_Uri1116831938MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException3573189601.h"
#include "System_System_Uri1116831938.h"
#include "mscorlib_System_ArgumentException928607144.h"
#include "mscorlib_System_EventHandler_1_gen2615270477.h"
#include "mscorlib_System_Delegate3310234105MethodDeclarations.h"
#include "mscorlib_System_Delegate3310234105.h"
#include "mscorlib_System_EventHandler_1_gen569145917.h"
#include "mscorlib_System_EventHandler_1_gen181896286.h"
#include "mscorlib_System_EventHandler2463957060.h"
#include "AssemblyU2DCSharp_WebSocketSharp_CloseStatusCode3936110621.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_CookieCollect1136277956.h"
#include "System_Core_System_Func_2_gen636757868.h"
#include "System_Core_System_Func_2_gen636757868MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocketState790259878.h"
#include "AssemblyU2DCSharp_WebSocketSharp_CompressionMethod2226596781.h"
#include "mscorlib_System_Threading_Monitor2734674376MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocket_U3CU3Ec__90658481MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocket_U3CU3Ec__90658481.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_NetworkCreden1204099087.h"
#include "System_System_UriKind238866934.h"
#include "System_System_Net_Security_RemoteCertificateValida1894914657.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocketFrame778194306.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocketFrame778194306MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketSharp_MessageEventArgs36945740MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketSharp_MessageEventArgs36945740.h"
#include "mscorlib_System_Exception3991598821.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocketExceptio2311987812MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocketExceptio2311987812.h"
#include "mscorlib_System_Exception3991598821MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_HttpStatusCod1625451593.h"
#include "mscorlib_System_IO_MemoryStream418716369MethodDeclarations.h"
#include "mscorlib_System_IO_MemoryStream418716369.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "mscorlib_System_IO_Stream1561764144MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3730860138MethodDeclarations.h"
#include "System_System_Net_IPEndPoint2123960758.h"
#include "System_Core_System_Func_2_gen3730860138.h"
#include "AssemblyU2DCSharp_WebSocketSharp_HandshakeResponse3229697822.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Mask3422653544.h"
#include "mscorlib_System_Threading_EventWaitHandle77918117MethodDeclarations.h"
#include "mscorlib_System_Threading_AutoResetEvent874642578.h"
#include "mscorlib_System_Text_StringBuilder243639308MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"
#include "AssemblyU2DCSharp_WebSocketSharp_HandshakeResponse3229697822MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_Authenticatio1650711563MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_Authenticatio1782907061.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_Authenticatio3190130368.h"
#include "AssemblyU2DCSharp_WebSocketSharp_CloseEventArgs2470319931MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketSharp_CloseEventArgs2470319931.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocket_U3Cclose239095580MethodDeclarations.h"
#include "System_Core_System_Action_3_gen2371519456MethodDeclarations.h"
#include "mscorlib_System_AsyncCallback1369114871MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocket_U3Cclose239095580.h"
#include "System_Core_System_Action_3_gen2371519456.h"
#include "mscorlib_System_AsyncCallback1369114871.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocketStream4103435597MethodDeclarations.h"
#include "System_System_Net_Sockets_TcpClient838416830MethodDeclarations.h"
#include "System_System_Net_Sockets_TcpClient838416830.h"
#include "mscorlib_System_Threading_WaitHandle1661568373MethodDeclarations.h"
#include "mscorlib_System_Threading_WaitHandle1661568373.h"
#include "AssemblyU2DCSharp_WebSocketSharp_HandshakeRequest1037477780MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_Authenticatio2112712571MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_CookieCollect1136277956MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_Authenticatio2112712571.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_Authenticatio1650711563.h"
#include "System_System_Collections_Generic_Queue_1_gen2273188169MethodDeclarations.h"
#include "System_System_Collections_Generic_Queue_1_gen2273188169.h"
#include "AssemblyU2DCSharp_WebSocketSharp_ErrorEventArgs424195371MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketSharp_ErrorEventArgs424195371.h"
#include "mscorlib_System_EventArgs2540831021.h"
#include "mscorlib_System_EventArgs2540831021MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketSharp_HandshakeBase1248407470.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Fin3262160529.h"
#include "mscorlib_System_Action_1_gen872614854.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocket_U3CsendA861700891MethodDeclarations.h"
#include "System_Core_System_Func_3_gen2548063815MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocket_U3CsendA861700891.h"
#include "System_Core_System_Func_3_gen2548063815.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocket_U3CsendA861700899MethodDeclarations.h"
#include "System_Core_System_Func_3_gen2764730336MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocket_U3CsendA861700899.h"
#include "System_Core_System_Func_3_gen2764730336.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocket_U3Cstart802946016MethodDeclarations.h"
#include "mscorlib_System_Threading_AutoResetEvent874642578MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocket_U3Cstart802946016.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocket_U3Cvalid467005428MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocket_U3Cvalid467005428.h"
#include "mscorlib_System_Random4255898871MethodDeclarations.h"
#include "mscorlib_System_Convert1363677321MethodDeclarations.h"
#include "mscorlib_System_Random4255898871.h"
#include "mscorlib_System_Security_Cryptography_SHA1CryptoSe3725620262MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_HashAlgorithm532578791MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_SHA13976944113.h"
#include "mscorlib_System_Security_Cryptography_SHA1CryptoSe3725620262.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1115770063.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1115770063MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2711741034.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2711741034MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocket_U3CConn4104301910MethodDeclarations.h"
#include "System_Core_System_Func_1_gen1601960292MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocket_U3CConn4104301910.h"
#include "System_Core_System_Func_1_gen1601960292.h"
#include "mscorlib_System_IO_FileInfo3233670074.h"
#include "mscorlib_System_IO_FileInfo3233670074MethodDeclarations.h"
#include "mscorlib_System_IO_FileStream2141505868.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocket_U3CSend2907890439MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen361609309MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen92447661MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocket_U3CSend2907890439.h"
#include "mscorlib_System_Action_1_gen361609309.h"
#include "mscorlib_System_Action_1_gen92447661.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_Cookie2077085446.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_NetworkCreden1204099087MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen872614854MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1174010442MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1174010442.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocketFrame_U3C681710991MethodDeclarations.h"
#include "System_Core_System_Func_1_gen1953705719MethodDeclarations.h"
#include "System_Core_System_Action_4_gen828544145MethodDeclarations.h"
#include "System_Core_System_Func_1_gen1953705719.h"
#include "System_Core_System_Action_4_gen828544145.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocketFrame_U3C681710991.h"
#include "mscorlib_System_Enum2862688501MethodDeclarations.h"
#include "mscorlib_System_Enum2862688501.h"
#include "mscorlib_System_UInt1624667923MethodDeclarations.h"
#include "mscorlib_System_UInt6424668076MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocketFrame_U34155755117MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocketFrame_U34155755117.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocketFrame_U31758512846MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocketFrame_U31758512846.h"
#include "mscorlib_System_Console1363597357MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocketFrame_U31917037014MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocketFrame_U31917037014.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocketState790259878MethodDeclarations.h"
#include "System_System_Net_Sockets_NetworkStream3953762560.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_Security_SslS3623155758.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_Security_SslS3623155758MethodDeclarations.h"
#include "System_System_Net_Sockets_NetworkStream3953762560MethodDeclarations.h"
#include "mscorlib_System_Int641153838595MethodDeclarations.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocketStream_U3526000246MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1549654636MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1549654636.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocketStream_U3526000246.h"
#include "mscorlib_System_StringSplitOptions3126613641.h"
#include "System_System_Net_Security_RemoteCertificateValida1894914657MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_X509Certific3076817455.h"
#include "System_System_Security_Cryptography_X509Certificat1111884825.h"
#include "System_System_Net_Security_SslPolicyErrors3099591579.h"
#include "System_System_Net_Security_SslStream490807902MethodDeclarations.h"
#include "System_System_Net_Security_SslStream490807902.h"
#include "System_Core_System_Func_2_gen3775863249MethodDeclarations.h"
#include "System_Core_System_Func_2_gen3775863249.h"
#include "System_Core_System_Func_2_gen1673115995MethodDeclarations.h"
#include "System_Core_System_Func_2_gen1673115995.h"
#include "AssemblyU2DCSharp_West2692559.h"
#include "AssemblyU2DCSharp_West2692559MethodDeclarations.h"
#include "AssemblyU2DCSharp_Enemy67100520MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3501516275MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject3674682005MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform1659122786MethodDeclarations.h"
#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen4197912687MethodDeclarations.h"
#include "mscorlib_System_Single4291918972.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Vector34282066566MethodDeclarations.h"
#include "AssemblyU2DCSharp_Charctor1500651146.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "AssemblyU2DCSharp_Enemy67100520.h"
#include "AssemblyU2DCSharp_Factory572770538.h"
#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen2352633852MethodDeclarations.h"
#include "AssemblyU2DCSharp_ActionManager3022458999MethodDeclarations.h"
#include "AssemblyU2DCSharp_ActionManager3022458999.h"
#include "AssemblyU2DCSharp_Factory572770538MethodDeclarations.h"
#include "AssemblyU2DCSharp_AIData1930434546.h"
#include "AssemblyU2DCSharp_CharData1499709504.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552MethodDeclarations.h"
#include "AssemblyU2DCSharp_EnemyManager1035150117.h"
#include "UnityEngine_UnityEngine_Component3501516275.h"
#include "AssemblyU2DCSharp_EnemyManager1035150117MethodDeclarations.h"
#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen1774517059MethodDeclarations.h"
#include "AssemblyU2DCSharp_SoundManager2444342206MethodDeclarations.h"
#include "AssemblyU2DCSharp_Bullet2000900386MethodDeclarations.h"
#include "AssemblyU2DCSharp_SoundManager2444342206.h"
#include "AssemblyU2DCSharp_Bullet2000900386.h"
#include "AssemblyU2DCSharp_Direction1041377119.h"
#include "UnityEngine_UnityEngine_Object3071478659MethodDeclarations.h"
#include "AssemblyU2DCSharp_StagePanelManager334945031MethodDeclarations.h"
#include "AssemblyU2DCSharp_Charctor1500651146MethodDeclarations.h"
#include "AssemblyU2DCSharp_StagePanelManager334945031.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "AssemblyU2DCSharp_ParticleManager73248295MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ParticleSystem381473177MethodDeclarations.h"
#include "AssemblyU2DCSharp_ParticleManager73248295.h"
#include "UnityEngine_UnityEngine_ParticleSystem381473177.h"

// !!0[] WebSocketSharp.Ext::SubArray<System.Byte>(!!0[],System.Int32,System.Int32)
extern "C"  ByteU5BU5D_t4260760469* Ext_SubArray_TisByte_t2862609660_m1882478596_gshared (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* p0, int32_t p1, int32_t p2, const MethodInfo* method);
#define Ext_SubArray_TisByte_t2862609660_m1882478596(__this /* static, unused */, p0, p1, p2, method) ((  ByteU5BU5D_t4260760469* (*) (Il2CppObject * /* static, unused */, ByteU5BU5D_t4260760469*, int32_t, int32_t, const MethodInfo*))Ext_SubArray_TisByte_t2862609660_m1882478596_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Boolean WebSocketSharp.Ext::Contains<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C"  bool Ext_Contains_TisIl2CppObject_m1031814249_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t785513668 * p1, const MethodInfo* method);
#define Ext_Contains_TisIl2CppObject_m1031814249(__this /* static, unused */, p0, p1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t785513668 *, const MethodInfo*))Ext_Contains_TisIl2CppObject_m1031814249_gshared)(__this /* static, unused */, p0, p1, method)
// System.Boolean WebSocketSharp.Ext::Contains<System.String>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Ext_Contains_TisString_t_m802592955(__this /* static, unused */, p0, p1, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t3730860138 *, const MethodInfo*))Ext_Contains_TisIl2CppObject_m1031814249_gshared)(__this /* static, unused */, p0, p1, method)
// System.Void WebSocketSharp.Ext::Emit<System.Object>(System.EventHandler`1<!!0>,System.Object,!!0)
extern "C"  void Ext_Emit_TisIl2CppObject_m3095910828_gshared (Il2CppObject * __this /* static, unused */, EventHandler_1_t20799621 * p0, Il2CppObject * p1, Il2CppObject * p2, const MethodInfo* method);
#define Ext_Emit_TisIl2CppObject_m3095910828(__this /* static, unused */, p0, p1, p2, method) ((  void (*) (Il2CppObject * /* static, unused */, EventHandler_1_t20799621 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Ext_Emit_TisIl2CppObject_m3095910828_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void WebSocketSharp.Ext::Emit<WebSocketSharp.CloseEventArgs>(System.EventHandler`1<!!0>,System.Object,!!0)
#define Ext_Emit_TisCloseEventArgs_t2470319931_m1774194048(__this /* static, unused */, p0, p1, p2, method) ((  void (*) (Il2CppObject * /* static, unused */, EventHandler_1_t2615270477 *, Il2CppObject *, CloseEventArgs_t2470319931 *, const MethodInfo*))Ext_Emit_TisIl2CppObject_m3095910828_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.String WebSocketSharp.Ext::ToString<System.Object>(!!0[],System.String)
extern "C"  String_t* Ext_ToString_TisIl2CppObject_m1031380364_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* p0, String_t* p1, const MethodInfo* method);
#define Ext_ToString_TisIl2CppObject_m1031380364(__this /* static, unused */, p0, p1, method) ((  String_t* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t1108656482*, String_t*, const MethodInfo*))Ext_ToString_TisIl2CppObject_m1031380364_gshared)(__this /* static, unused */, p0, p1, method)
// System.String WebSocketSharp.Ext::ToString<System.String>(!!0[],System.String)
#define Ext_ToString_TisString_t_m931729630(__this /* static, unused */, p0, p1, method) ((  String_t* (*) (Il2CppObject * /* static, unused */, StringU5BU5D_t4054002952*, String_t*, const MethodInfo*))Ext_ToString_TisIl2CppObject_m1031380364_gshared)(__this /* static, unused */, p0, p1, method)
// System.Void WebSocketSharp.Ext::Emit<WebSocketSharp.ErrorEventArgs>(System.EventHandler`1<!!0>,System.Object,!!0)
#define Ext_Emit_TisErrorEventArgs_t424195371_m3920859536(__this /* static, unused */, p0, p1, p2, method) ((  void (*) (Il2CppObject * /* static, unused */, EventHandler_1_t569145917 *, Il2CppObject *, ErrorEventArgs_t424195371 *, const MethodInfo*))Ext_Emit_TisIl2CppObject_m3095910828_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0[] WebSocketSharp.Ext::Copy<System.Byte>(!!0[],System.Int64)
extern "C"  ByteU5BU5D_t4260760469* Ext_Copy_TisByte_t2862609660_m4188581816_gshared (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* p0, int64_t p1, const MethodInfo* method);
#define Ext_Copy_TisByte_t2862609660_m4188581816(__this /* static, unused */, p0, p1, method) ((  ByteU5BU5D_t4260760469* (*) (Il2CppObject * /* static, unused */, ByteU5BU5D_t4260760469*, int64_t, const MethodInfo*))Ext_Copy_TisByte_t2862609660_m4188581816_gshared)(__this /* static, unused */, p0, p1, method)
// System.Void WebSocketSharp.Ext::Emit<WebSocketSharp.MessageEventArgs>(System.EventHandler`1<!!0>,System.Object,!!0)
#define Ext_Emit_TisMessageEventArgs_t36945740_m2913779343(__this /* static, unused */, p0, p1, p2, method) ((  void (*) (Il2CppObject * /* static, unused */, EventHandler_1_t181896286 *, Il2CppObject *, MessageEventArgs_t36945740 *, const MethodInfo*))Ext_Emit_TisIl2CppObject_m3095910828_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 WebSocketSharp.WebSocketStream::ReadHandshake<System.Object>(System.Func`2<System.String[],!!0>,System.Int32)
extern "C"  Il2CppObject * WebSocketStream_ReadHandshake_TisIl2CppObject_m2979771395_gshared (WebSocketStream_t4103435597 * __this, Func_2_t2614234544 * p0, int32_t p1, const MethodInfo* method);
#define WebSocketStream_ReadHandshake_TisIl2CppObject_m2979771395(__this, p0, p1, method) ((  Il2CppObject * (*) (WebSocketStream_t4103435597 *, Func_2_t2614234544 *, int32_t, const MethodInfo*))WebSocketStream_ReadHandshake_TisIl2CppObject_m2979771395_gshared)(__this, p0, p1, method)
// !!0 WebSocketSharp.WebSocketStream::ReadHandshake<WebSocketSharp.HandshakeRequest>(System.Func`2<System.String[],!!0>,System.Int32)
#define WebSocketStream_ReadHandshake_TisHandshakeRequest_t1037477780_m1150541384(__this, p0, p1, method) ((  HandshakeRequest_t1037477780 * (*) (WebSocketStream_t4103435597 *, Func_2_t3775863249 *, int32_t, const MethodInfo*))WebSocketStream_ReadHandshake_TisIl2CppObject_m2979771395_gshared)(__this, p0, p1, method)
// !!0 WebSocketSharp.WebSocketStream::ReadHandshake<WebSocketSharp.HandshakeResponse>(System.Func`2<System.String[],!!0>,System.Int32)
#define WebSocketStream_ReadHandshake_TisHandshakeResponse_t3229697822_m385670550(__this, p0, p1, method) ((  HandshakeResponse_t3229697822 * (*) (WebSocketStream_t4103435597 *, Func_2_t1673115995 *, int32_t, const MethodInfo*))WebSocketStream_ReadHandshake_TisIl2CppObject_m2979771395_gshared)(__this, p0, p1, method)
// !!0 UnityEngine.Component::GetComponentInParent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInParent_TisIl2CppObject_m1297875695_gshared (Component_t3501516275 * __this, const MethodInfo* method);
#define Component_GetComponentInParent_TisIl2CppObject_m1297875695(__this, method) ((  Il2CppObject * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponentInParent_TisIl2CppObject_m1297875695_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInParent<EnemyManager>()
#define Component_GetComponentInParent_TisEnemyManager_t1035150117_m1180037263(__this, method) ((  EnemyManager_t1035150117 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponentInParent_TisIl2CppObject_m1297875695_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared (GameObject_t3674682005 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m3652735468(__this, method) ((  Il2CppObject * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<Bullet>()
#define GameObject_GetComponent_TisBullet_t2000900386_m4055895035(__this, method) ((  Bullet_t2000900386 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
// !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
extern "C"  Il2CppObject * Object_FindObjectOfType_TisIl2CppObject_m2892359027_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Object_FindObjectOfType_TisIl2CppObject_m2892359027(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisIl2CppObject_m2892359027_gshared)(__this /* static, unused */, method)
// !!0 UnityEngine.Object::FindObjectOfType<StagePanelManager>()
#define Object_FindObjectOfType_TisStagePanelManager_t334945031_m3286582442(__this /* static, unused */, method) ((  StagePanelManager_t334945031 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisIl2CppObject_m2892359027_gshared)(__this /* static, unused */, method)
// !!0 UnityEngine.Object::FindObjectOfType<ParticleManager>()
#define Object_FindObjectOfType_TisParticleManager_t73248295_m857524746(__this /* static, unused */, method) ((  ParticleManager_t73248295 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisIl2CppObject_m2892359027_gshared)(__this /* static, unused */, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.ParticleSystem>()
#define GameObject_GetComponent_TisParticleSystem_t381473177_m2450916872(__this, method) ((  ParticleSystem_t381473177 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m3652735468_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<>c__Iterator1D::.ctor()
extern "C"  void U3CU3Ec__Iterator1D__ctor_m3257398241 (U3CU3Ec__Iterator1D_t3823346890 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<>c__Iterator1D::System.Collections.Generic.IEnumerator<string>.get_Current()
extern "C"  String_t* U3CU3Ec__Iterator1D_System_Collections_Generic_IEnumeratorU3CstringU3E_get_Current_m3706070033 (U3CU3Ec__Iterator1D_t3823346890 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Object WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<>c__Iterator1D::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CU3Ec__Iterator1D_System_Collections_IEnumerator_get_Current_m742685157 (U3CU3Ec__Iterator1D_t3823346890 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Collections.IEnumerator WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<>c__Iterator1D::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CU3Ec__Iterator1D_System_Collections_IEnumerable_GetEnumerator_m1712162400 (U3CU3Ec__Iterator1D_t3823346890 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = U3CU3Ec__Iterator1D_System_Collections_Generic_IEnumerableU3CstringU3E_GetEnumerator_m3264289290(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<System.String> WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<>c__Iterator1D::System.Collections.Generic.IEnumerable<string>.GetEnumerator()
extern Il2CppClass* U3CU3Ec__Iterator1D_t3823346890_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec__Iterator1D_System_Collections_Generic_IEnumerableU3CstringU3E_GetEnumerator_m3264289290_MetadataUsageId;
extern "C"  Il2CppObject* U3CU3Ec__Iterator1D_System_Collections_Generic_IEnumerableU3CstringU3E_GetEnumerator_m3264289290 (U3CU3Ec__Iterator1D_t3823346890 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__Iterator1D_System_Collections_Generic_IEnumerableU3CstringU3E_GetEnumerator_m3264289290_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__Iterator1D_t3823346890 * V_0 = NULL;
	{
		int32_t* L_0 = __this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, L_0, 0, ((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CU3Ec__Iterator1D_t3823346890 * L_2 = (U3CU3Ec__Iterator1D_t3823346890 *)il2cpp_codegen_object_new(U3CU3Ec__Iterator1D_t3823346890_il2cpp_TypeInfo_var);
		U3CU3Ec__Iterator1D__ctor_m3257398241(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		U3CU3Ec__Iterator1D_t3823346890 * L_3 = V_0;
		TcpListenerWebSocketContext_t3782393199 * L_4 = __this->get_U3CU3Ef__this_6();
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_6(L_4);
		U3CU3Ec__Iterator1D_t3823346890 * L_5 = V_0;
		return L_5;
	}
}
// System.Boolean WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<>c__Iterator1D::MoveNext()
extern Il2CppClass* CharU5BU5D_t3324145743_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral408195354;
extern const uint32_t U3CU3Ec__Iterator1D_MoveNext_m2055721843_MetadataUsageId;
extern "C"  bool U3CU3Ec__Iterator1D_MoveNext_m2055721843 (U3CU3Ec__Iterator1D_t3823346890 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__Iterator1D_MoveNext_m2055721843_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00a4;
		}
	}
	{
		goto IL_00cc;
	}

IL_0021:
	{
		TcpListenerWebSocketContext_t3782393199 * L_2 = __this->get_U3CU3Ef__this_6();
		NullCheck(L_2);
		HandshakeRequest_t1037477780 * L_3 = L_2->get__request_3();
		NullCheck(L_3);
		NameValueCollection_t2791941106 * L_4 = HandshakeBase_get_Headers_m3632488899(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		String_t* L_5 = NameValueCollection_get_Item_m3428939380(L_4, _stringLiteral408195354, /*hidden argument*/NULL);
		__this->set_U3CprotocolsU3E__0_0(L_5);
		String_t* L_6 = __this->get_U3CprotocolsU3E__0_0();
		if (!L_6)
		{
			goto IL_00c5;
		}
	}
	{
		String_t* L_7 = __this->get_U3CprotocolsU3E__0_0();
		CharU5BU5D_t3324145743* L_8 = ((CharU5BU5D_t3324145743*)SZArrayNew(CharU5BU5D_t3324145743_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)44));
		NullCheck(L_7);
		StringU5BU5D_t4054002952* L_9 = String_Split_m290179486(L_7, L_8, /*hidden argument*/NULL);
		__this->set_U3CU24s_138U3E__1_1(L_9);
		__this->set_U3CU24s_139U3E__2_2(0);
		goto IL_00b2;
	}

IL_0074:
	{
		StringU5BU5D_t4054002952* L_10 = __this->get_U3CU24s_138U3E__1_1();
		int32_t L_11 = __this->get_U3CU24s_139U3E__2_2();
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		String_t* L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		__this->set_U3CprotocolU3E__3_3(L_13);
		String_t* L_14 = __this->get_U3CprotocolU3E__3_3();
		NullCheck(L_14);
		String_t* L_15 = String_Trim_m1030489823(L_14, /*hidden argument*/NULL);
		__this->set_U24current_5(L_15);
		__this->set_U24PC_4(1);
		goto IL_00ce;
	}

IL_00a4:
	{
		int32_t L_16 = __this->get_U3CU24s_139U3E__2_2();
		__this->set_U3CU24s_139U3E__2_2(((int32_t)((int32_t)L_16+(int32_t)1)));
	}

IL_00b2:
	{
		int32_t L_17 = __this->get_U3CU24s_139U3E__2_2();
		StringU5BU5D_t4054002952* L_18 = __this->get_U3CU24s_138U3E__1_1();
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_18)->max_length)))))))
		{
			goto IL_0074;
		}
	}

IL_00c5:
	{
		__this->set_U24PC_4((-1));
	}

IL_00cc:
	{
		return (bool)0;
	}

IL_00ce:
	{
		return (bool)1;
	}
	// Dead block : IL_00d0: ldloc.1
}
// System.Void WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<>c__Iterator1D::Dispose()
extern "C"  void U3CU3Ec__Iterator1D_Dispose_m3521801182 (U3CU3Ec__Iterator1D_t3823346890 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<>c__Iterator1D::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec__Iterator1D_Reset_m903831182_MetadataUsageId;
extern "C"  void U3CU3Ec__Iterator1D_Reset_m903831182 (U3CU3Ec__Iterator1D_t3823346890 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__Iterator1D_Reset_m903831182_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void WebSocketSharp.Net.WebSockets.WebSocketContext::.ctor()
extern "C"  void WebSocketContext__ctor_m547194573 (WebSocketContext_t763725542 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.PayloadData::.ctor()
extern Il2CppClass* ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var;
extern const uint32_t PayloadData__ctor_m1953054222_MetadataUsageId;
extern "C"  void PayloadData__ctor_m1953054222 (PayloadData_t39926750 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PayloadData__ctor_m1953054222_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PayloadData__ctor_m46018943(__this, ((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)0)), ((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)0)), (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.PayloadData::.ctor(System.Byte[])
extern Il2CppClass* ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var;
extern const uint32_t PayloadData__ctor_m719751291_MetadataUsageId;
extern "C"  void PayloadData__ctor_m719751291 (PayloadData_t39926750 * __this, ByteU5BU5D_t4260760469* ___applicationData0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PayloadData__ctor_m719751291_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ByteU5BU5D_t4260760469* L_0 = ___applicationData0;
		PayloadData__ctor_m46018943(__this, ((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)0)), L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.PayloadData::.ctor(System.String)
extern Il2CppClass* ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t2012439129_il2cpp_TypeInfo_var;
extern const uint32_t PayloadData__ctor_m2777556916_MetadataUsageId;
extern "C"  void PayloadData__ctor_m2777556916 (PayloadData_t39926750 * __this, String_t* ___applicationData0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PayloadData__ctor_m2777556916_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t2012439129_il2cpp_TypeInfo_var);
		Encoding_t2012439129 * L_0 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = ___applicationData0;
		NullCheck(L_0);
		ByteU5BU5D_t4260760469* L_2 = VirtFuncInvoker1< ByteU5BU5D_t4260760469*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_0, L_1);
		PayloadData__ctor_m46018943(__this, ((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)0)), L_2, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.PayloadData::.ctor(System.Byte[],System.Boolean)
extern Il2CppClass* ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var;
extern const uint32_t PayloadData__ctor_m3599656354_MetadataUsageId;
extern "C"  void PayloadData__ctor_m3599656354 (PayloadData_t39926750 * __this, ByteU5BU5D_t4260760469* ___applicationData0, bool ___masked1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PayloadData__ctor_m3599656354_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ByteU5BU5D_t4260760469* L_0 = ___applicationData0;
		bool L_1 = ___masked1;
		PayloadData__ctor_m46018943(__this, ((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)0)), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.PayloadData::.ctor(System.Byte[],System.Byte[],System.Boolean)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral684456896;
extern const uint32_t PayloadData__ctor_m46018943_MetadataUsageId;
extern "C"  void PayloadData__ctor_m46018943 (PayloadData_t39926750 * __this, ByteU5BU5D_t4260760469* ___extensionData0, ByteU5BU5D_t4260760469* ___applicationData1, bool ___masked2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PayloadData__ctor_m46018943_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t4260760469* L_0 = ___extensionData0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		int64_t L_1 = Array_get_LongLength_m1209728916((Il2CppArray *)(Il2CppArray *)L_0, /*hidden argument*/NULL);
		ByteU5BU5D_t4260760469* L_2 = ___applicationData1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_2);
		int64_t L_3 = Array_get_LongLength_m1209728916((Il2CppArray *)(Il2CppArray *)L_2, /*hidden argument*/NULL);
		if ((!(((uint64_t)((int64_t)((int64_t)L_1+(int64_t)L_3))) > ((uint64_t)((int64_t)std::numeric_limits<int64_t>::max())))))
		{
			goto IL_002c;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_4 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_4, _stringLiteral684456896, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_002c:
	{
		ByteU5BU5D_t4260760469* L_5 = ___extensionData0;
		__this->set__extensionData_2(L_5);
		ByteU5BU5D_t4260760469* L_6 = ___applicationData1;
		__this->set__applicationData_1(L_6);
		bool L_7 = ___masked2;
		__this->set__masked_3(L_7);
		return;
	}
}
// System.Collections.IEnumerator WebSocketSharp.PayloadData::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * PayloadData_System_Collections_IEnumerable_GetEnumerator_m1737577763 (PayloadData_t39926750 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = PayloadData_GetEnumerator_m2795194391(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean WebSocketSharp.PayloadData::get_ContainsReservedCloseStatusCode()
extern const MethodInfo* Ext_SubArray_TisByte_t2862609660_m1882478596_MethodInfo_var;
extern const uint32_t PayloadData_get_ContainsReservedCloseStatusCode_m2380973759_MetadataUsageId;
extern "C"  bool PayloadData_get_ContainsReservedCloseStatusCode_m2380973759 (PayloadData_t39926750 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PayloadData_get_ContainsReservedCloseStatusCode_m2380973759_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		ByteU5BU5D_t4260760469* L_0 = __this->get__applicationData_1();
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) <= ((int32_t)1)))
		{
			goto IL_0028;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_1 = __this->get__applicationData_1();
		ByteU5BU5D_t4260760469* L_2 = Ext_SubArray_TisByte_t2862609660_m1882478596(NULL /*static, unused*/, L_1, 0, 2, /*hidden argument*/Ext_SubArray_TisByte_t2862609660_m1882478596_MethodInfo_var);
		uint16_t L_3 = Ext_ToUInt16_m2711859920(NULL /*static, unused*/, L_2, 1, /*hidden argument*/NULL);
		bool L_4 = Ext_IsReserved_m627022511(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_4));
		goto IL_0029;
	}

IL_0028:
	{
		G_B3_0 = 0;
	}

IL_0029:
	{
		return (bool)G_B3_0;
	}
}
// System.Byte[] WebSocketSharp.PayloadData::get_ApplicationData()
extern "C"  ByteU5BU5D_t4260760469* PayloadData_get_ApplicationData_m3147947059 (PayloadData_t39926750 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t4260760469* L_0 = __this->get__applicationData_1();
		return L_0;
	}
}
// System.Byte[] WebSocketSharp.PayloadData::get_ExtensionData()
extern "C"  ByteU5BU5D_t4260760469* PayloadData_get_ExtensionData_m2164787682 (PayloadData_t39926750 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t4260760469* L_0 = __this->get__extensionData_2();
		return L_0;
	}
}
// System.Boolean WebSocketSharp.PayloadData::get_IsMasked()
extern "C"  bool PayloadData_get_IsMasked_m2863479752 (PayloadData_t39926750 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get__masked_3();
		return L_0;
	}
}
// System.UInt64 WebSocketSharp.PayloadData::get_Length()
extern "C"  uint64_t PayloadData_get_Length_m3166044545 (PayloadData_t39926750 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t4260760469* L_0 = __this->get__extensionData_2();
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		int64_t L_1 = Array_get_LongLength_m1209728916((Il2CppArray *)(Il2CppArray *)L_0, /*hidden argument*/NULL);
		ByteU5BU5D_t4260760469* L_2 = __this->get__applicationData_1();
		NullCheck((Il2CppArray *)(Il2CppArray *)L_2);
		int64_t L_3 = Array_get_LongLength_m1209728916((Il2CppArray *)(Il2CppArray *)L_2, /*hidden argument*/NULL);
		return ((int64_t)((int64_t)L_1+(int64_t)L_3));
	}
}
// System.Void WebSocketSharp.PayloadData::mask(System.Byte[],System.Byte[])
extern "C"  void PayloadData_mask_m1332147850 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___src0, ByteU5BU5D_t4260760469* ___key1, const MethodInfo* method)
{
	int64_t V_0 = 0;
	{
		V_0 = (((int64_t)((int64_t)0)));
		goto IL_001e;
	}

IL_0008:
	{
		ByteU5BU5D_t4260760469* L_0 = ___src0;
		int64_t L_1 = V_0;
		if ((int64_t)(L_1) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		ByteU5BU5D_t4260760469* L_2 = ___src0;
		int64_t L_3 = V_0;
		if ((int64_t)(L_3) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, (il2cpp_array_size_t)(uintptr_t)(((intptr_t)L_3)));
		intptr_t L_4 = (((intptr_t)L_3));
		uint8_t L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		ByteU5BU5D_t4260760469* L_6 = ___key1;
		int64_t L_7 = V_0;
		if ((int64_t)(((int64_t)((int64_t)L_7%(int64_t)(((int64_t)((int64_t)4)))))) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, (il2cpp_array_size_t)(uintptr_t)(((intptr_t)((int64_t)((int64_t)L_7%(int64_t)(((int64_t)((int64_t)4))))))));
		intptr_t L_8 = (((intptr_t)((int64_t)((int64_t)L_7%(int64_t)(((int64_t)((int64_t)4)))))));
		uint8_t L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, (il2cpp_array_size_t)(uintptr_t)(((intptr_t)L_1)));
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>((((intptr_t)L_1))), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_5^(int32_t)L_9))))));
		int64_t L_10 = V_0;
		V_0 = ((int64_t)((int64_t)L_10+(int64_t)(((int64_t)((int64_t)1)))));
	}

IL_001e:
	{
		int64_t L_11 = V_0;
		ByteU5BU5D_t4260760469* L_12 = ___src0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_12);
		int64_t L_13 = Array_get_LongLength_m1209728916((Il2CppArray *)(Il2CppArray *)L_12, /*hidden argument*/NULL);
		if ((((int64_t)L_11) < ((int64_t)L_13)))
		{
			goto IL_0008;
		}
	}
	{
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Byte> WebSocketSharp.PayloadData::GetEnumerator()
extern Il2CppClass* U3CGetEnumeratorU3Ec__Iterator1E_t303122823_il2cpp_TypeInfo_var;
extern const uint32_t PayloadData_GetEnumerator_m2795194391_MetadataUsageId;
extern "C"  Il2CppObject* PayloadData_GetEnumerator_m2795194391 (PayloadData_t39926750 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PayloadData_GetEnumerator_m2795194391_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetEnumeratorU3Ec__Iterator1E_t303122823 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator1E_t303122823 * L_0 = (U3CGetEnumeratorU3Ec__Iterator1E_t303122823 *)il2cpp_codegen_object_new(U3CGetEnumeratorU3Ec__Iterator1E_t303122823_il2cpp_TypeInfo_var);
		U3CGetEnumeratorU3Ec__Iterator1E__ctor_m1103379908(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetEnumeratorU3Ec__Iterator1E_t303122823 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_8(__this);
		U3CGetEnumeratorU3Ec__Iterator1E_t303122823 * L_2 = V_0;
		return L_2;
	}
}
// System.Void WebSocketSharp.PayloadData::Mask(System.Byte[])
extern "C"  void PayloadData_Mask_m2120375719 (PayloadData_t39926750 * __this, ByteU5BU5D_t4260760469* ___maskingKey0, const MethodInfo* method)
{
	{
		ByteU5BU5D_t4260760469* L_0 = __this->get__extensionData_2();
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		int64_t L_1 = Array_get_LongLength_m1209728916((Il2CppArray *)(Il2CppArray *)L_0, /*hidden argument*/NULL);
		if ((((int64_t)L_1) <= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_001e;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_2 = __this->get__extensionData_2();
		ByteU5BU5D_t4260760469* L_3 = ___maskingKey0;
		PayloadData_mask_m1332147850(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
	}

IL_001e:
	{
		ByteU5BU5D_t4260760469* L_4 = __this->get__applicationData_1();
		NullCheck((Il2CppArray *)(Il2CppArray *)L_4);
		int64_t L_5 = Array_get_LongLength_m1209728916((Il2CppArray *)(Il2CppArray *)L_4, /*hidden argument*/NULL);
		if ((((int64_t)L_5) <= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_003c;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_6 = __this->get__applicationData_1();
		ByteU5BU5D_t4260760469* L_7 = ___maskingKey0;
		PayloadData_mask_m1332147850(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
	}

IL_003c:
	{
		bool L_8 = __this->get__masked_3();
		__this->set__masked_3((bool)((((int32_t)L_8) == ((int32_t)0))? 1 : 0));
		return;
	}
}
// System.Byte[] WebSocketSharp.PayloadData::ToByteArray()
extern Il2CppClass* List_1_t4230795212_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3277037792_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m3980858669_MethodInfo_var;
extern const uint32_t PayloadData_ToByteArray_m147904440_MetadataUsageId;
extern "C"  ByteU5BU5D_t4260760469* PayloadData_ToByteArray_m147904440 (PayloadData_t39926750 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PayloadData_ToByteArray_m147904440_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t4260760469* G_B3_0 = NULL;
	{
		ByteU5BU5D_t4260760469* L_0 = __this->get__extensionData_2();
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		int64_t L_1 = Array_get_LongLength_m1209728916((Il2CppArray *)(Il2CppArray *)L_0, /*hidden argument*/NULL);
		if ((((int64_t)L_1) <= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_0022;
		}
	}
	{
		List_1_t4230795212 * L_2 = (List_1_t4230795212 *)il2cpp_codegen_object_new(List_1_t4230795212_il2cpp_TypeInfo_var);
		List_1__ctor_m3277037792(L_2, __this, /*hidden argument*/List_1__ctor_m3277037792_MethodInfo_var);
		NullCheck(L_2);
		ByteU5BU5D_t4260760469* L_3 = List_1_ToArray_m3980858669(L_2, /*hidden argument*/List_1_ToArray_m3980858669_MethodInfo_var);
		G_B3_0 = L_3;
		goto IL_0028;
	}

IL_0022:
	{
		ByteU5BU5D_t4260760469* L_4 = __this->get__applicationData_1();
		G_B3_0 = L_4;
	}

IL_0028:
	{
		return G_B3_0;
	}
}
// System.String WebSocketSharp.PayloadData::ToString()
extern Il2CppClass* BitConverter_t1260277767_il2cpp_TypeInfo_var;
extern const uint32_t PayloadData_ToString_m441557573_MetadataUsageId;
extern "C"  String_t* PayloadData_ToString_m441557573 (PayloadData_t39926750 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PayloadData_ToString_m441557573_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ByteU5BU5D_t4260760469* L_0 = PayloadData_ToByteArray_m147904440(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t1260277767_il2cpp_TypeInfo_var);
		String_t* L_1 = BitConverter_ToString_m1865594174(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator1E::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator1E__ctor_m1103379908 (U3CGetEnumeratorU3Ec__Iterator1E_t303122823 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Byte WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator1E::System.Collections.Generic.IEnumerator<byte>.get_Current()
extern "C"  uint8_t U3CGetEnumeratorU3Ec__Iterator1E_System_Collections_Generic_IEnumeratorU3CbyteU3E_get_Current_m3060147566 (U3CGetEnumeratorU3Ec__Iterator1E_t303122823 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = __this->get_U24current_7();
		return L_0;
	}
}
// System.Object WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator1E::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* Byte_t2862609660_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator1E_System_Collections_IEnumerator_get_Current_m3228423842_MetadataUsageId;
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator1E_System_Collections_IEnumerator_get_Current_m3228423842 (U3CGetEnumeratorU3Ec__Iterator1E_t303122823 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator1E_System_Collections_IEnumerator_get_Current_m3228423842_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		uint8_t L_0 = __this->get_U24current_7();
		uint8_t L_1 = L_0;
		Il2CppObject * L_2 = Box(Byte_t2862609660_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Boolean WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator1E::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator1E_MoveNext_m2976200112 (U3CGetEnumeratorU3Ec__Iterator1E_t303122823 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_6();
		V_0 = L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_006d;
		}
		if (L_1 == 2)
		{
			goto IL_00d6;
		}
	}
	{
		goto IL_00fe;
	}

IL_0025:
	{
		PayloadData_t39926750 * L_2 = __this->get_U3CU3Ef__this_8();
		NullCheck(L_2);
		ByteU5BU5D_t4260760469* L_3 = L_2->get__extensionData_2();
		__this->set_U3CU24s_140U3E__0_0(L_3);
		__this->set_U3CU24s_141U3E__1_1(0);
		goto IL_007b;
	}

IL_0042:
	{
		ByteU5BU5D_t4260760469* L_4 = __this->get_U3CU24s_140U3E__0_0();
		int32_t L_5 = __this->get_U3CU24s_141U3E__1_1();
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		uint8_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		__this->set_U3CbU3E__2_2(L_7);
		uint8_t L_8 = __this->get_U3CbU3E__2_2();
		__this->set_U24current_7(L_8);
		__this->set_U24PC_6(1);
		goto IL_0100;
	}

IL_006d:
	{
		int32_t L_9 = __this->get_U3CU24s_141U3E__1_1();
		__this->set_U3CU24s_141U3E__1_1(((int32_t)((int32_t)L_9+(int32_t)1)));
	}

IL_007b:
	{
		int32_t L_10 = __this->get_U3CU24s_141U3E__1_1();
		ByteU5BU5D_t4260760469* L_11 = __this->get_U3CU24s_140U3E__0_0();
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0042;
		}
	}
	{
		PayloadData_t39926750 * L_12 = __this->get_U3CU3Ef__this_8();
		NullCheck(L_12);
		ByteU5BU5D_t4260760469* L_13 = L_12->get__applicationData_1();
		__this->set_U3CU24s_142U3E__3_3(L_13);
		__this->set_U3CU24s_143U3E__4_4(0);
		goto IL_00e4;
	}

IL_00ab:
	{
		ByteU5BU5D_t4260760469* L_14 = __this->get_U3CU24s_142U3E__3_3();
		int32_t L_15 = __this->get_U3CU24s_143U3E__4_4();
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		uint8_t L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		__this->set_U3CbU3E__5_5(L_17);
		uint8_t L_18 = __this->get_U3CbU3E__5_5();
		__this->set_U24current_7(L_18);
		__this->set_U24PC_6(2);
		goto IL_0100;
	}

IL_00d6:
	{
		int32_t L_19 = __this->get_U3CU24s_143U3E__4_4();
		__this->set_U3CU24s_143U3E__4_4(((int32_t)((int32_t)L_19+(int32_t)1)));
	}

IL_00e4:
	{
		int32_t L_20 = __this->get_U3CU24s_143U3E__4_4();
		ByteU5BU5D_t4260760469* L_21 = __this->get_U3CU24s_142U3E__3_3();
		NullCheck(L_21);
		if ((((int32_t)L_20) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_21)->max_length)))))))
		{
			goto IL_00ab;
		}
	}
	{
		__this->set_U24PC_6((-1));
	}

IL_00fe:
	{
		return (bool)0;
	}

IL_0100:
	{
		return (bool)1;
	}
	// Dead block : IL_0102: ldloc.1
}
// System.Void WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator1E::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator1E_Dispose_m3684419841 (U3CGetEnumeratorU3Ec__Iterator1E_t303122823 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_6((-1));
		return;
	}
}
// System.Void WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator1E::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator1E_Reset_m3044780145_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator1E_Reset_m3044780145 (U3CGetEnumeratorU3Ec__Iterator1E_t303122823 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator1E_Reset_m3044780145_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void WebSocketSharp.WebSocket::.ctor(WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext,System.String,WebSocketSharp.Logger)
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern const MethodInfo* HttpListenerWebSocketContext_Close_m2643023591_MethodInfo_var;
extern const uint32_t WebSocket__ctor_m3396704322_MetadataUsageId;
extern "C"  void WebSocket__ctor_m3396704322 (WebSocket_t1342580397 * __this, HttpListenerWebSocketContext_t1074545506 * ___context0, String_t* ___protocol1, Logger_t3695440972 * ___logger2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket__ctor_m3396704322_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		HttpListenerWebSocketContext_t1074545506 * L_0 = ___context0;
		__this->set__context_9(L_0);
		String_t* L_1 = ___protocol1;
		__this->set__protocol_25(L_1);
		Logger_t3695440972 * L_2 = ___logger2;
		il2cpp_codegen_memory_barrier();
		__this->set__logger_19(L_2);
		HttpListenerWebSocketContext_t1074545506 * L_3 = ___context0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)HttpListenerWebSocketContext_Close_m2643023591_MethodInfo_var);
		Action_t3771233898 * L_5 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_5, L_3, L_4, /*hidden argument*/NULL);
		__this->set__closeContext_7(L_5);
		HttpListenerWebSocketContext_t1074545506 * L_6 = ___context0;
		NullCheck(L_6);
		bool L_7 = VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext::get_IsSecureConnection() */, L_6);
		__this->set__secure_29(L_7);
		HttpListenerWebSocketContext_t1074545506 * L_8 = ___context0;
		NullCheck(L_8);
		WebSocketStream_t4103435597 * L_9 = HttpListenerWebSocketContext_get_Stream_m1108211965(L_8, /*hidden argument*/NULL);
		__this->set__stream_30(L_9);
		WebSocket_init_m2877534261(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::.ctor(WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext,System.String,WebSocketSharp.Logger)
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern const MethodInfo* TcpListenerWebSocketContext_Close_m1144748106_MethodInfo_var;
extern const uint32_t WebSocket__ctor_m496774313_MetadataUsageId;
extern "C"  void WebSocket__ctor_m496774313 (WebSocket_t1342580397 * __this, TcpListenerWebSocketContext_t3782393199 * ___context0, String_t* ___protocol1, Logger_t3695440972 * ___logger2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket__ctor_m496774313_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		TcpListenerWebSocketContext_t3782393199 * L_0 = ___context0;
		__this->set__context_9(L_0);
		String_t* L_1 = ___protocol1;
		__this->set__protocol_25(L_1);
		Logger_t3695440972 * L_2 = ___logger2;
		il2cpp_codegen_memory_barrier();
		__this->set__logger_19(L_2);
		TcpListenerWebSocketContext_t3782393199 * L_3 = ___context0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)TcpListenerWebSocketContext_Close_m1144748106_MethodInfo_var);
		Action_t3771233898 * L_5 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_5, L_3, L_4, /*hidden argument*/NULL);
		__this->set__closeContext_7(L_5);
		TcpListenerWebSocketContext_t3782393199 * L_6 = ___context0;
		NullCheck(L_6);
		bool L_7 = VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext::get_IsSecureConnection() */, L_6);
		__this->set__secure_29(L_7);
		TcpListenerWebSocketContext_t3782393199 * L_8 = ___context0;
		NullCheck(L_8);
		WebSocketStream_t4103435597 * L_9 = TcpListenerWebSocketContext_get_Stream_m1233149848(L_8, /*hidden argument*/NULL);
		__this->set__stream_30(L_9);
		WebSocket_init_m2877534261(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::.ctor(System.String,System.String[])
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppClass* Logger_t3695440972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral116079;
extern Il2CppCodeGenString* _stringLiteral3695658203;
extern Il2CppCodeGenString* _stringLiteral118039;
extern const uint32_t WebSocket__ctor_m2306802077_MetadataUsageId;
extern "C"  void WebSocket__ctor_m2306802077 (WebSocket_t1342580397 * __this, String_t* ___url0, StringU5BU5D_t4054002952* ___protocols1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket__ctor_m2306802077_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___url0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, _stringLiteral116079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		String_t* L_2 = ___url0;
		Uri_t1116831938 ** L_3 = __this->get_address_of__uri_32();
		bool L_4 = Ext_TryCreateWebSocketUri_m304807878(NULL /*static, unused*/, L_2, L_3, (&V_0), /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0036;
		}
	}
	{
		String_t* L_5 = V_0;
		ArgumentException_t928607144 * L_6 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m732321503(L_6, L_5, _stringLiteral116079, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_0036:
	{
		StringU5BU5D_t4054002952* L_7 = ___protocols1;
		if (!L_7)
		{
			goto IL_0065;
		}
	}
	{
		StringU5BU5D_t4054002952* L_8 = ___protocols1;
		NullCheck(L_8);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_0065;
		}
	}
	{
		StringU5BU5D_t4054002952* L_9 = ___protocols1;
		String_t* L_10 = Ext_CheckIfValidProtocols_m3548151740(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		V_0 = L_10;
		String_t* L_11 = V_0;
		if (!L_11)
		{
			goto IL_005e;
		}
	}
	{
		String_t* L_12 = V_0;
		ArgumentException_t928607144 * L_13 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m732321503(L_13, L_12, _stringLiteral3695658203, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_13);
	}

IL_005e:
	{
		StringU5BU5D_t4054002952* L_14 = ___protocols1;
		__this->set__protocols_26(L_14);
	}

IL_0065:
	{
		String_t* L_15 = WebSocket_CreateBase64Key_m565389870(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__base64Key_4(L_15);
		__this->set__client_6((bool)1);
		Logger_t3695440972 * L_16 = (Logger_t3695440972 *)il2cpp_codegen_object_new(Logger_t3695440972_il2cpp_TypeInfo_var);
		Logger__ctor_m3044192816(L_16, /*hidden argument*/NULL);
		il2cpp_codegen_memory_barrier();
		__this->set__logger_19(L_16);
		Uri_t1116831938 * L_17 = __this->get__uri_32();
		NullCheck(L_17);
		String_t* L_18 = Uri_get_Scheme_m2606456870(L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_19 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_18, _stringLiteral118039, /*hidden argument*/NULL);
		__this->set__secure_29(L_19);
		WebSocket_init_m2877534261(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::add_OnClose(System.EventHandler`1<WebSocketSharp.CloseEventArgs>)
extern Il2CppClass* EventHandler_1_t2615270477_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_add_OnClose_m3939987016_MetadataUsageId;
extern "C"  void WebSocket_add_OnClose_m3939987016 (WebSocket_t1342580397 * __this, EventHandler_1_t2615270477 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_add_OnClose_m3939987016_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventHandler_1_t2615270477 * L_0 = __this->get_OnClose_33();
		EventHandler_1_t2615270477 * L_1 = ___value0;
		Delegate_t3310234105 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_OnClose_33(((EventHandler_1_t2615270477 *)CastclassSealed(L_2, EventHandler_1_t2615270477_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::remove_OnClose(System.EventHandler`1<WebSocketSharp.CloseEventArgs>)
extern Il2CppClass* EventHandler_1_t2615270477_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_remove_OnClose_m2678343347_MetadataUsageId;
extern "C"  void WebSocket_remove_OnClose_m2678343347 (WebSocket_t1342580397 * __this, EventHandler_1_t2615270477 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_remove_OnClose_m2678343347_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventHandler_1_t2615270477 * L_0 = __this->get_OnClose_33();
		EventHandler_1_t2615270477 * L_1 = ___value0;
		Delegate_t3310234105 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_OnClose_33(((EventHandler_1_t2615270477 *)CastclassSealed(L_2, EventHandler_1_t2615270477_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::add_OnError(System.EventHandler`1<WebSocketSharp.ErrorEventArgs>)
extern Il2CppClass* EventHandler_1_t569145917_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_add_OnError_m215455816_MetadataUsageId;
extern "C"  void WebSocket_add_OnError_m215455816 (WebSocket_t1342580397 * __this, EventHandler_1_t569145917 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_add_OnError_m215455816_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventHandler_1_t569145917 * L_0 = __this->get_OnError_34();
		EventHandler_1_t569145917 * L_1 = ___value0;
		Delegate_t3310234105 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_OnError_34(((EventHandler_1_t569145917 *)CastclassSealed(L_2, EventHandler_1_t569145917_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::remove_OnError(System.EventHandler`1<WebSocketSharp.ErrorEventArgs>)
extern Il2CppClass* EventHandler_1_t569145917_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_remove_OnError_m3248779443_MetadataUsageId;
extern "C"  void WebSocket_remove_OnError_m3248779443 (WebSocket_t1342580397 * __this, EventHandler_1_t569145917 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_remove_OnError_m3248779443_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventHandler_1_t569145917 * L_0 = __this->get_OnError_34();
		EventHandler_1_t569145917 * L_1 = ___value0;
		Delegate_t3310234105 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_OnError_34(((EventHandler_1_t569145917 *)CastclassSealed(L_2, EventHandler_1_t569145917_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::add_OnMessage(System.EventHandler`1<WebSocketSharp.MessageEventArgs>)
extern Il2CppClass* EventHandler_1_t181896286_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_add_OnMessage_m1138713256_MetadataUsageId;
extern "C"  void WebSocket_add_OnMessage_m1138713256 (WebSocket_t1342580397 * __this, EventHandler_1_t181896286 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_add_OnMessage_m1138713256_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventHandler_1_t181896286 * L_0 = __this->get_OnMessage_35();
		EventHandler_1_t181896286 * L_1 = ___value0;
		Delegate_t3310234105 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_OnMessage_35(((EventHandler_1_t181896286 *)CastclassSealed(L_2, EventHandler_1_t181896286_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::remove_OnMessage(System.EventHandler`1<WebSocketSharp.MessageEventArgs>)
extern Il2CppClass* EventHandler_1_t181896286_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_remove_OnMessage_m2623802771_MetadataUsageId;
extern "C"  void WebSocket_remove_OnMessage_m2623802771 (WebSocket_t1342580397 * __this, EventHandler_1_t181896286 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_remove_OnMessage_m2623802771_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventHandler_1_t181896286 * L_0 = __this->get_OnMessage_35();
		EventHandler_1_t181896286 * L_1 = ___value0;
		Delegate_t3310234105 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_OnMessage_35(((EventHandler_1_t181896286 *)CastclassSealed(L_2, EventHandler_1_t181896286_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::add_OnOpen(System.EventHandler)
extern Il2CppClass* EventHandler_t2463957060_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_add_OnOpen_m3674536951_MetadataUsageId;
extern "C"  void WebSocket_add_OnOpen_m3674536951 (WebSocket_t1342580397 * __this, EventHandler_t2463957060 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_add_OnOpen_m3674536951_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventHandler_t2463957060 * L_0 = __this->get_OnOpen_36();
		EventHandler_t2463957060 * L_1 = ___value0;
		Delegate_t3310234105 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_OnOpen_36(((EventHandler_t2463957060 *)CastclassSealed(L_2, EventHandler_t2463957060_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::remove_OnOpen(System.EventHandler)
extern Il2CppClass* EventHandler_t2463957060_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_remove_OnOpen_m2525126434_MetadataUsageId;
extern "C"  void WebSocket_remove_OnOpen_m2525126434 (WebSocket_t1342580397 * __this, EventHandler_t2463957060 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_remove_OnOpen_m2525126434_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventHandler_t2463957060 * L_0 = __this->get_OnOpen_36();
		EventHandler_t2463957060 * L_1 = ___value0;
		Delegate_t3310234105 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_OnOpen_36(((EventHandler_t2463957060 *)CastclassSealed(L_2, EventHandler_t2463957060_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::System.IDisposable.Dispose()
extern "C"  void WebSocket_System_IDisposable_Dispose_m2593617632 (WebSocket_t1342580397 * __this, const MethodInfo* method)
{
	{
		WebSocket_Close_m1852531397(__this, ((int32_t)1001), (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// WebSocketSharp.Net.CookieCollection WebSocketSharp.WebSocket::get_CookieCollection()
extern "C"  CookieCollection_t1136277956 * WebSocket_get_CookieCollection_m1369077125 (WebSocket_t1342580397 * __this, const MethodInfo* method)
{
	{
		CookieCollection_t1136277956 * L_0 = __this->get__cookies_10();
		return L_0;
	}
}
// System.Func`2<WebSocketSharp.Net.WebSockets.WebSocketContext,System.String> WebSocketSharp.WebSocket::get_CustomHandshakeRequestChecker()
extern Il2CppClass* WebSocket_t1342580397_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t636757868_il2cpp_TypeInfo_var;
extern const MethodInfo* WebSocket_U3Cget_CustomHandshakeRequestCheckerU3Em__C_m6268665_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m147252604_MethodInfo_var;
extern const uint32_t WebSocket_get_CustomHandshakeRequestChecker_m1227021650_MetadataUsageId;
extern "C"  Func_2_t636757868 * WebSocket_get_CustomHandshakeRequestChecker_m1227021650 (WebSocket_t1342580397 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_get_CustomHandshakeRequestChecker_m1227021650_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Func_2_t636757868 * G_B4_0 = NULL;
	Func_2_t636757868 * G_B1_0 = NULL;
	{
		Func_2_t636757868 * L_0 = __this->get__handshakeRequestChecker_18();
		Func_2_t636757868 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B4_0 = L_1;
			goto IL_002a;
		}
	}
	{
		Func_2_t636757868 * L_2 = ((WebSocket_t1342580397_StaticFields*)WebSocket_t1342580397_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache22_37();
		if (L_2)
		{
			goto IL_0025;
		}
	}
	{
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)WebSocket_U3Cget_CustomHandshakeRequestCheckerU3Em__C_m6268665_MethodInfo_var);
		Func_2_t636757868 * L_4 = (Func_2_t636757868 *)il2cpp_codegen_object_new(Func_2_t636757868_il2cpp_TypeInfo_var);
		Func_2__ctor_m147252604(L_4, NULL, L_3, /*hidden argument*/Func_2__ctor_m147252604_MethodInfo_var);
		((WebSocket_t1342580397_StaticFields*)WebSocket_t1342580397_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache22_37(L_4);
	}

IL_0025:
	{
		Func_2_t636757868 * L_5 = ((WebSocket_t1342580397_StaticFields*)WebSocket_t1342580397_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache22_37();
		G_B4_0 = L_5;
	}

IL_002a:
	{
		return G_B4_0;
	}
}
// System.Void WebSocketSharp.WebSocket::set_CustomHandshakeRequestChecker(System.Func`2<WebSocketSharp.Net.WebSockets.WebSocketContext,System.String>)
extern "C"  void WebSocket_set_CustomHandshakeRequestChecker_m2971497793 (WebSocket_t1342580397 * __this, Func_2_t636757868 * ___value0, const MethodInfo* method)
{
	{
		Func_2_t636757868 * L_0 = ___value0;
		__this->set__handshakeRequestChecker_18(L_0);
		return;
	}
}
// System.Boolean WebSocketSharp.WebSocket::get_IsConnected()
extern "C"  bool WebSocket_get_IsConnected_m2653755679 (WebSocket_t1342580397 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		uint16_t L_0 = __this->get__readyState_27();
		il2cpp_codegen_memory_barrier();
		if ((((int32_t)L_0) == ((int32_t)1)))
		{
			goto IL_001b;
		}
	}
	{
		uint16_t L_1 = __this->get__readyState_27();
		il2cpp_codegen_memory_barrier();
		G_B3_0 = ((((int32_t)L_1) == ((int32_t)2))? 1 : 0);
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 1;
	}

IL_001c:
	{
		return (bool)G_B3_0;
	}
}
// WebSocketSharp.CompressionMethod WebSocketSharp.WebSocket::get_Compression()
extern "C"  uint8_t WebSocket_get_Compression_m3949946905 (WebSocket_t1342580397 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = __this->get__compression_8();
		return L_0;
	}
}
// System.Void WebSocketSharp.WebSocket::set_Compression(WebSocketSharp.CompressionMethod)
extern Il2CppCodeGenString* _stringLiteral3556381300;
extern const uint32_t WebSocket_set_Compression_m1365773706_MetadataUsageId;
extern "C"  void WebSocket_set_Compression_m1365773706 (WebSocket_t1342580397 * __this, uint8_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_set_Compression_m1365773706_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	String_t* V_1 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = __this->get__forConn_14();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_2 = WebSocket_checkIfAvailable_m3972221078(__this, _stringLiteral3556381300, (bool)0, (bool)0, /*hidden argument*/NULL);
			V_1 = L_2;
			String_t* L_3 = V_1;
			if (!L_3)
			{
				goto IL_003b;
			}
		}

IL_0021:
		{
			Logger_t3695440972 * L_4 = __this->get__logger_19();
			il2cpp_codegen_memory_barrier();
			String_t* L_5 = V_1;
			NullCheck(L_4);
			Logger_Error_m3981980524(L_4, L_5, /*hidden argument*/NULL);
			String_t* L_6 = V_1;
			WebSocket_error_m149167037(__this, L_6, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x4E, FINALLY_0047);
		}

IL_003b:
		{
			uint8_t L_7 = ___value0;
			__this->set__compression_8(L_7);
			IL2CPP_LEAVE(0x4E, FINALLY_0047);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0047;
	}

FINALLY_0047:
	{ // begin finally (depth: 1)
		Il2CppObject * L_8 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(71)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(71)
	{
		IL2CPP_JUMP_TBL(0x4E, IL_004e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_004e:
	{
		return;
	}
}
// System.Collections.Generic.IEnumerable`1<WebSocketSharp.Net.Cookie> WebSocketSharp.WebSocket::get_Cookies()
extern Il2CppClass* U3CU3Ec__Iterator1F_t90658481_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_get_Cookies_m1919387799_MetadataUsageId;
extern "C"  Il2CppObject* WebSocket_get_Cookies_m1919387799 (WebSocket_t1342580397 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_get_Cookies_m1919387799_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__Iterator1F_t90658481 * V_0 = NULL;
	{
		U3CU3Ec__Iterator1F_t90658481 * L_0 = (U3CU3Ec__Iterator1F_t90658481 *)il2cpp_codegen_object_new(U3CU3Ec__Iterator1F_t90658481_il2cpp_TypeInfo_var);
		U3CU3Ec__Iterator1F__ctor_m799211274(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__Iterator1F_t90658481 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_5(__this);
		U3CU3Ec__Iterator1F_t90658481 * L_2 = V_0;
		U3CU3Ec__Iterator1F_t90658481 * L_3 = L_2;
		NullCheck(L_3);
		L_3->set_U24PC_3(((int32_t)-2));
		return L_3;
	}
}
// WebSocketSharp.Net.NetworkCredential WebSocketSharp.WebSocket::get_Credentials()
extern "C"  NetworkCredential_t1204099087 * WebSocket_get_Credentials_m1933776316 (WebSocket_t1342580397 * __this, const MethodInfo* method)
{
	{
		NetworkCredential_t1204099087 * L_0 = __this->get__credentials_11();
		return L_0;
	}
}
// System.String WebSocketSharp.WebSocket::get_Extensions()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_get_Extensions_m3314700805_MetadataUsageId;
extern "C"  String_t* WebSocket_get_Extensions_m3314700805 (WebSocket_t1342580397 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_get_Extensions_m3314700805_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	{
		String_t* L_0 = __this->get__extensions_12();
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_0012;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B2_0 = L_2;
	}

IL_0012:
	{
		return G_B2_0;
	}
}
// System.Boolean WebSocketSharp.WebSocket::get_IsAlive()
extern "C"  bool WebSocket_get_IsAlive_m2079412323 (WebSocket_t1342580397 * __this, const MethodInfo* method)
{
	{
		bool L_0 = WebSocket_Ping_m348145387(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean WebSocketSharp.WebSocket::get_IsSecure()
extern "C"  bool WebSocket_get_IsSecure_m3548243843 (WebSocket_t1342580397 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get__secure_29();
		return L_0;
	}
}
// WebSocketSharp.Logger WebSocketSharp.WebSocket::get_Log()
extern "C"  Logger_t3695440972 * WebSocket_get_Log_m257468864 (WebSocket_t1342580397 * __this, const MethodInfo* method)
{
	{
		Logger_t3695440972 * L_0 = __this->get__logger_19();
		il2cpp_codegen_memory_barrier();
		return L_0;
	}
}
// System.Void WebSocketSharp.WebSocket::set_Log(WebSocketSharp.Logger)
extern "C"  void WebSocket_set_Log_m1960622227 (WebSocket_t1342580397 * __this, Logger_t3695440972 * ___value0, const MethodInfo* method)
{
	{
		Logger_t3695440972 * L_0 = ___value0;
		il2cpp_codegen_memory_barrier();
		__this->set__logger_19(L_0);
		return;
	}
}
// System.String WebSocketSharp.WebSocket::get_Origin()
extern "C"  String_t* WebSocket_get_Origin_m4272001175 (WebSocket_t1342580397 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__origin_22();
		return L_0;
	}
}
// System.Void WebSocketSharp.WebSocket::set_Origin(System.String)
extern Il2CppClass* Uri_t1116831938_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t3324145743_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral643433816;
extern Il2CppCodeGenString* _stringLiteral3675054831;
extern const uint32_t WebSocket_set_Origin_m1309570778_MetadataUsageId;
extern "C"  void WebSocket_set_Origin_m1309570778 (WebSocket_t1342580397 * __this, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_set_Origin_m1309570778_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	String_t* V_1 = NULL;
	Uri_t1116831938 * V_2 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = __this->get__forConn_14();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_2 = WebSocket_checkIfAvailable_m3972221078(__this, _stringLiteral643433816, (bool)0, (bool)0, /*hidden argument*/NULL);
			V_1 = L_2;
			String_t* L_3 = V_1;
			if (L_3)
			{
				goto IL_005a;
			}
		}

IL_0021:
		{
			String_t* L_4 = ___value0;
			bool L_5 = Ext_IsNullOrEmpty_m3465800922(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
			if (!L_5)
			{
				goto IL_0038;
			}
		}

IL_002c:
		{
			String_t* L_6 = ___value0;
			__this->set__origin_22(L_6);
			IL2CPP_LEAVE(0x9D, FINALLY_0096);
		}

IL_0038:
		{
			String_t* L_7 = ___value0;
			IL2CPP_RUNTIME_CLASS_INIT(Uri_t1116831938_il2cpp_TypeInfo_var);
			bool L_8 = Uri_TryCreate_m1126538084(NULL /*static, unused*/, L_7, 1, (&V_2), /*hidden argument*/NULL);
			if (!L_8)
			{
				goto IL_0054;
			}
		}

IL_0046:
		{
			Uri_t1116831938 * L_9 = V_2;
			NullCheck(L_9);
			StringU5BU5D_t4054002952* L_10 = Uri_get_Segments_m3672202303(L_9, /*hidden argument*/NULL);
			NullCheck(L_10);
			if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length))))) <= ((int32_t)1)))
			{
				goto IL_005a;
			}
		}

IL_0054:
		{
			V_1 = _stringLiteral3675054831;
		}

IL_005a:
		{
			String_t* L_11 = V_1;
			if (!L_11)
			{
				goto IL_007a;
			}
		}

IL_0060:
		{
			Logger_t3695440972 * L_12 = __this->get__logger_19();
			il2cpp_codegen_memory_barrier();
			String_t* L_13 = V_1;
			NullCheck(L_12);
			Logger_Error_m3981980524(L_12, L_13, /*hidden argument*/NULL);
			String_t* L_14 = V_1;
			WebSocket_error_m149167037(__this, L_14, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x9D, FINALLY_0096);
		}

IL_007a:
		{
			String_t* L_15 = ___value0;
			CharU5BU5D_t3324145743* L_16 = ((CharU5BU5D_t3324145743*)SZArrayNew(CharU5BU5D_t3324145743_il2cpp_TypeInfo_var, (uint32_t)1));
			NullCheck(L_16);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 0);
			(L_16)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)47));
			NullCheck(L_15);
			String_t* L_17 = String_TrimEnd_m3980947229(L_15, L_16, /*hidden argument*/NULL);
			__this->set__origin_22(L_17);
			IL2CPP_LEAVE(0x9D, FINALLY_0096);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0096;
	}

FINALLY_0096:
	{ // begin finally (depth: 1)
		Il2CppObject * L_18 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(150)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(150)
	{
		IL2CPP_JUMP_TBL(0x9D, IL_009d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_009d:
	{
		return;
	}
}
// System.String WebSocketSharp.WebSocket::get_Protocol()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_get_Protocol_m4186218377_MetadataUsageId;
extern "C"  String_t* WebSocket_get_Protocol_m4186218377 (WebSocket_t1342580397 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_get_Protocol_m4186218377_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	{
		String_t* L_0 = __this->get__protocol_25();
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_0012;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B2_0 = L_2;
	}

IL_0012:
	{
		return G_B2_0;
	}
}
// System.Void WebSocketSharp.WebSocket::set_Protocol(System.String)
extern "C"  void WebSocket_set_Protocol_m1846032744 (WebSocket_t1342580397 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set__protocol_25(L_0);
		return;
	}
}
// WebSocketSharp.WebSocketState WebSocketSharp.WebSocket::get_ReadyState()
extern "C"  uint16_t WebSocket_get_ReadyState_m719238074 (WebSocket_t1342580397 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = __this->get__readyState_27();
		il2cpp_codegen_memory_barrier();
		return (uint16_t)(L_0);
	}
}
// System.Net.Security.RemoteCertificateValidationCallback WebSocketSharp.WebSocket::get_ServerCertificateValidationCallback()
extern "C"  RemoteCertificateValidationCallback_t1894914657 * WebSocket_get_ServerCertificateValidationCallback_m1521164420 (WebSocket_t1342580397 * __this, const MethodInfo* method)
{
	{
		RemoteCertificateValidationCallback_t1894914657 * L_0 = __this->get__certValidationCallback_5();
		return L_0;
	}
}
// System.Void WebSocketSharp.WebSocket::set_ServerCertificateValidationCallback(System.Net.Security.RemoteCertificateValidationCallback)
extern Il2CppCodeGenString* _stringLiteral2787124320;
extern const uint32_t WebSocket_set_ServerCertificateValidationCallback_m3368301391_MetadataUsageId;
extern "C"  void WebSocket_set_ServerCertificateValidationCallback_m3368301391 (WebSocket_t1342580397 * __this, RemoteCertificateValidationCallback_t1894914657 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_set_ServerCertificateValidationCallback_m3368301391_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	String_t* V_1 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = __this->get__forConn_14();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_2 = WebSocket_checkIfAvailable_m3972221078(__this, _stringLiteral2787124320, (bool)0, (bool)0, /*hidden argument*/NULL);
			V_1 = L_2;
			String_t* L_3 = V_1;
			if (!L_3)
			{
				goto IL_003b;
			}
		}

IL_0021:
		{
			Logger_t3695440972 * L_4 = __this->get__logger_19();
			il2cpp_codegen_memory_barrier();
			String_t* L_5 = V_1;
			NullCheck(L_4);
			Logger_Error_m3981980524(L_4, L_5, /*hidden argument*/NULL);
			String_t* L_6 = V_1;
			WebSocket_error_m149167037(__this, L_6, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x4E, FINALLY_0047);
		}

IL_003b:
		{
			RemoteCertificateValidationCallback_t1894914657 * L_7 = ___value0;
			__this->set__certValidationCallback_5(L_7);
			IL2CPP_LEAVE(0x4E, FINALLY_0047);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0047;
	}

FINALLY_0047:
	{ // begin finally (depth: 1)
		Il2CppObject * L_8 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(71)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(71)
	{
		IL2CPP_JUMP_TBL(0x4E, IL_004e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_004e:
	{
		return;
	}
}
// System.Uri WebSocketSharp.WebSocket::get_Url()
extern "C"  Uri_t1116831938 * WebSocket_get_Url_m2061398483 (WebSocket_t1342580397 * __this, const MethodInfo* method)
{
	Uri_t1116831938 * G_B3_0 = NULL;
	{
		bool L_0 = __this->get__client_6();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Uri_t1116831938 * L_1 = __this->get__uri_32();
		G_B3_0 = L_1;
		goto IL_0021;
	}

IL_0016:
	{
		WebSocketContext_t763725542 * L_2 = __this->get__context_9();
		NullCheck(L_2);
		Uri_t1116831938 * L_3 = VirtFuncInvoker0< Uri_t1116831938 * >::Invoke(13 /* System.Uri WebSocketSharp.Net.WebSockets.WebSocketContext::get_RequestUri() */, L_2);
		G_B3_0 = L_3;
	}

IL_0021:
	{
		return G_B3_0;
	}
}
// System.Boolean WebSocketSharp.WebSocket::acceptCloseFrame(WebSocketSharp.WebSocketFrame)
extern "C"  bool WebSocket_acceptCloseFrame_m1331471019 (WebSocket_t1342580397 * __this, WebSocketFrame_t778194306 * ___frame0, const MethodInfo* method)
{
	PayloadData_t39926750 * V_0 = NULL;
	{
		WebSocketFrame_t778194306 * L_0 = ___frame0;
		NullCheck(L_0);
		PayloadData_t39926750 * L_1 = WebSocketFrame_get_PayloadData_m4173896503(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		PayloadData_t39926750 * L_2 = V_0;
		PayloadData_t39926750 * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = PayloadData_get_ContainsReservedCloseStatusCode_m2380973759(L_3, /*hidden argument*/NULL);
		WebSocket_close_m707576104(__this, L_2, (bool)((((int32_t)L_4) == ((int32_t)0))? 1 : 0), (bool)0, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Boolean WebSocketSharp.WebSocket::acceptDataFrame(WebSocketSharp.WebSocketFrame)
extern Il2CppClass* MessageEventArgs_t36945740_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_acceptDataFrame_m1452099421_MetadataUsageId;
extern "C"  bool WebSocket_acceptDataFrame_m1452099421 (WebSocket_t1342580397 * __this, WebSocketFrame_t778194306 * ___frame0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_acceptDataFrame_m1452099421_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MessageEventArgs_t36945740 * V_0 = NULL;
	MessageEventArgs_t36945740 * G_B3_0 = NULL;
	{
		WebSocketFrame_t778194306 * L_0 = ___frame0;
		NullCheck(L_0);
		bool L_1 = WebSocketFrame_get_IsCompressed_m3923144746(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		WebSocketFrame_t778194306 * L_2 = ___frame0;
		NullCheck(L_2);
		uint8_t L_3 = WebSocketFrame_get_Opcode_m53528135(L_2, /*hidden argument*/NULL);
		WebSocketFrame_t778194306 * L_4 = ___frame0;
		NullCheck(L_4);
		PayloadData_t39926750 * L_5 = WebSocketFrame_get_PayloadData_m4173896503(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		ByteU5BU5D_t4260760469* L_6 = PayloadData_get_ApplicationData_m3147947059(L_5, /*hidden argument*/NULL);
		uint8_t L_7 = __this->get__compression_8();
		ByteU5BU5D_t4260760469* L_8 = Ext_Decompress_m2803880749(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		MessageEventArgs_t36945740 * L_9 = (MessageEventArgs_t36945740 *)il2cpp_codegen_object_new(MessageEventArgs_t36945740_il2cpp_TypeInfo_var);
		MessageEventArgs__ctor_m2265064076(L_9, L_3, L_8, /*hidden argument*/NULL);
		G_B3_0 = L_9;
		goto IL_0042;
	}

IL_0031:
	{
		WebSocketFrame_t778194306 * L_10 = ___frame0;
		NullCheck(L_10);
		uint8_t L_11 = WebSocketFrame_get_Opcode_m53528135(L_10, /*hidden argument*/NULL);
		WebSocketFrame_t778194306 * L_12 = ___frame0;
		NullCheck(L_12);
		PayloadData_t39926750 * L_13 = WebSocketFrame_get_PayloadData_m4173896503(L_12, /*hidden argument*/NULL);
		MessageEventArgs_t36945740 * L_14 = (MessageEventArgs_t36945740 *)il2cpp_codegen_object_new(MessageEventArgs_t36945740_il2cpp_TypeInfo_var);
		MessageEventArgs__ctor_m2365520848(L_14, L_11, L_13, /*hidden argument*/NULL);
		G_B3_0 = L_14;
	}

IL_0042:
	{
		V_0 = G_B3_0;
		MessageEventArgs_t36945740 * L_15 = V_0;
		WebSocket_enqueueToMessageEventQueue_m3930529079(__this, L_15, /*hidden argument*/NULL);
		return (bool)1;
	}
}
// System.Void WebSocketSharp.WebSocket::acceptException(System.Exception,System.String)
extern Il2CppClass* WebSocketException_t2311987812_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_acceptException_m1180715184_MetadataUsageId;
extern "C"  void WebSocket_acceptException_m1180715184 (WebSocket_t1342580397 * __this, Exception_t3991598821 * ___exception0, String_t* ___message1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_acceptException_m1180715184_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	String_t* V_1 = NULL;
	WebSocketException_t2311987812 * V_2 = NULL;
	String_t* G_B8_0 = NULL;
	WebSocket_t1342580397 * G_B8_1 = NULL;
	String_t* G_B7_0 = NULL;
	WebSocket_t1342580397 * G_B7_1 = NULL;
	String_t* G_B13_0 = NULL;
	uint16_t G_B13_1 = 0;
	WebSocket_t1342580397 * G_B13_2 = NULL;
	String_t* G_B12_0 = NULL;
	uint16_t G_B12_1 = 0;
	WebSocket_t1342580397 * G_B12_2 = NULL;
	{
		V_0 = ((int32_t)1006);
		String_t* L_0 = ___message1;
		V_1 = L_0;
		Exception_t3991598821 * L_1 = ___exception0;
		if (!((WebSocketException_t2311987812 *)IsInstClass(L_1, WebSocketException_t2311987812_il2cpp_TypeInfo_var)))
		{
			goto IL_0028;
		}
	}
	{
		Exception_t3991598821 * L_2 = ___exception0;
		V_2 = ((WebSocketException_t2311987812 *)CastclassClass(L_2, WebSocketException_t2311987812_il2cpp_TypeInfo_var));
		WebSocketException_t2311987812 * L_3 = V_2;
		NullCheck(L_3);
		uint16_t L_4 = WebSocketException_get_Code_m3592681969(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		WebSocketException_t2311987812 * L_5 = V_2;
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_5);
		V_1 = L_6;
	}

IL_0028:
	{
		uint16_t L_7 = V_0;
		if ((((int32_t)L_7) == ((int32_t)((int32_t)1006))))
		{
			goto IL_003e;
		}
	}
	{
		uint16_t L_8 = V_0;
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)1015)))))
		{
			goto IL_0056;
		}
	}

IL_003e:
	{
		Logger_t3695440972 * L_9 = __this->get__logger_19();
		il2cpp_codegen_memory_barrier();
		Exception_t3991598821 * L_10 = ___exception0;
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Exception::ToString() */, L_10);
		NullCheck(L_9);
		Logger_Fatal_m51902704(L_9, L_11, /*hidden argument*/NULL);
		goto IL_0064;
	}

IL_0056:
	{
		Logger_t3695440972 * L_12 = __this->get__logger_19();
		il2cpp_codegen_memory_barrier();
		String_t* L_13 = V_1;
		NullCheck(L_12);
		Logger_Error_m3981980524(L_12, L_13, /*hidden argument*/NULL);
	}

IL_0064:
	{
		String_t* L_14 = ___message1;
		String_t* L_15 = L_14;
		G_B7_0 = L_15;
		G_B7_1 = __this;
		if (L_15)
		{
			G_B8_0 = L_15;
			G_B8_1 = __this;
			goto IL_0073;
		}
	}
	{
		uint16_t L_16 = V_0;
		String_t* L_17 = Ext_GetMessage_m1491341063(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		G_B8_0 = L_17;
		G_B8_1 = G_B7_1;
	}

IL_0073:
	{
		NullCheck(G_B8_1);
		WebSocket_error_m149167037(G_B8_1, G_B8_0, /*hidden argument*/NULL);
		uint16_t L_18 = __this->get__readyState_27();
		il2cpp_codegen_memory_barrier();
		if (L_18)
		{
			goto IL_00a0;
		}
	}
	{
		bool L_19 = __this->get__client_6();
		if (L_19)
		{
			goto IL_00a0;
		}
	}
	{
		WebSocket_Close_m4199074618(__this, ((int32_t)400), /*hidden argument*/NULL);
		goto IL_00b6;
	}

IL_00a0:
	{
		uint16_t L_20 = V_0;
		String_t* L_21 = V_1;
		String_t* L_22 = L_21;
		G_B12_0 = L_22;
		G_B12_1 = L_20;
		G_B12_2 = __this;
		if (L_22)
		{
			G_B13_0 = L_22;
			G_B13_1 = L_20;
			G_B13_2 = __this;
			goto IL_00b0;
		}
	}
	{
		uint16_t L_23 = V_0;
		String_t* L_24 = Ext_GetMessage_m1491341063(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		G_B13_0 = L_24;
		G_B13_1 = G_B12_1;
		G_B13_2 = G_B12_2;
	}

IL_00b0:
	{
		NullCheck(G_B13_2);
		WebSocket_close_m1268085240(G_B13_2, G_B13_1, G_B13_0, (bool)0, /*hidden argument*/NULL);
	}

IL_00b6:
	{
		return;
	}
}
// System.Boolean WebSocketSharp.WebSocket::acceptFragmentedFrame(WebSocketSharp.WebSocketFrame)
extern "C"  bool WebSocket_acceptFragmentedFrame_m496170018 (WebSocket_t1342580397 * __this, WebSocketFrame_t778194306 * ___frame0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		WebSocketFrame_t778194306 * L_0 = ___frame0;
		NullCheck(L_0);
		bool L_1 = WebSocketFrame_get_IsContinuation_m4205121376(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = 1;
		goto IL_0018;
	}

IL_0011:
	{
		WebSocketFrame_t778194306 * L_2 = ___frame0;
		bool L_3 = WebSocket_acceptFragments_m359488285(__this, L_2, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_3));
	}

IL_0018:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean WebSocketSharp.WebSocket::acceptFragments(WebSocketSharp.WebSocketFrame)
extern Il2CppClass* MemoryStream_t418716369_il2cpp_TypeInfo_var;
extern Il2CppClass* MessageEventArgs_t36945740_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_acceptFragments_m359488285_MetadataUsageId;
extern "C"  bool WebSocket_acceptFragments_m359488285 (WebSocket_t1342580397 * __this, WebSocketFrame_t778194306 * ___first0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_acceptFragments_m359488285_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MemoryStream_t418716369 * V_0 = NULL;
	ByteU5BU5D_t4260760469* V_1 = NULL;
	bool V_2 = false;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		MemoryStream_t418716369 * L_0 = (MemoryStream_t418716369 *)il2cpp_codegen_object_new(MemoryStream_t418716369_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m3603177736(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
	}

IL_0006:
	try
	{ // begin try (depth: 1)
		{
			MemoryStream_t418716369 * L_1 = V_0;
			WebSocketFrame_t778194306 * L_2 = ___first0;
			NullCheck(L_2);
			PayloadData_t39926750 * L_3 = WebSocketFrame_get_PayloadData_m4173896503(L_2, /*hidden argument*/NULL);
			NullCheck(L_3);
			ByteU5BU5D_t4260760469* L_4 = PayloadData_get_ApplicationData_m3147947059(L_3, /*hidden argument*/NULL);
			Ext_WriteBytes_m1982528933(NULL /*static, unused*/, L_1, L_4, /*hidden argument*/NULL);
			MemoryStream_t418716369 * L_5 = V_0;
			bool L_6 = WebSocket_concatenateFragmentsInto_m70739806(__this, L_5, /*hidden argument*/NULL);
			if (L_6)
			{
				goto IL_002a;
			}
		}

IL_0023:
		{
			V_2 = (bool)0;
			IL2CPP_LEAVE(0x7F, FINALLY_0072);
		}

IL_002a:
		{
			uint8_t L_7 = __this->get__compression_8();
			if (!L_7)
			{
				goto IL_0047;
			}
		}

IL_0035:
		{
			MemoryStream_t418716369 * L_8 = V_0;
			uint8_t L_9 = __this->get__compression_8();
			ByteU5BU5D_t4260760469* L_10 = Ext_DecompressToArray_m99179775(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
			V_1 = L_10;
			goto IL_0054;
		}

IL_0047:
		{
			MemoryStream_t418716369 * L_11 = V_0;
			NullCheck(L_11);
			VirtActionInvoker0::Invoke(12 /* System.Void System.IO.Stream::Close() */, L_11);
			MemoryStream_t418716369 * L_12 = V_0;
			NullCheck(L_12);
			ByteU5BU5D_t4260760469* L_13 = VirtFuncInvoker0< ByteU5BU5D_t4260760469* >::Invoke(26 /* System.Byte[] System.IO.MemoryStream::ToArray() */, L_12);
			V_1 = L_13;
		}

IL_0054:
		{
			WebSocketFrame_t778194306 * L_14 = ___first0;
			NullCheck(L_14);
			uint8_t L_15 = WebSocketFrame_get_Opcode_m53528135(L_14, /*hidden argument*/NULL);
			ByteU5BU5D_t4260760469* L_16 = V_1;
			MessageEventArgs_t36945740 * L_17 = (MessageEventArgs_t36945740 *)il2cpp_codegen_object_new(MessageEventArgs_t36945740_il2cpp_TypeInfo_var);
			MessageEventArgs__ctor_m2265064076(L_17, L_15, L_16, /*hidden argument*/NULL);
			WebSocket_enqueueToMessageEventQueue_m3930529079(__this, L_17, /*hidden argument*/NULL);
			V_2 = (bool)1;
			IL2CPP_LEAVE(0x7F, FINALLY_0072);
		}

IL_006d:
		{
			; // IL_006d: leave IL_007f
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0072;
	}

FINALLY_0072:
	{ // begin finally (depth: 1)
		{
			MemoryStream_t418716369 * L_18 = V_0;
			if (!L_18)
			{
				goto IL_007e;
			}
		}

IL_0078:
		{
			MemoryStream_t418716369 * L_19 = V_0;
			NullCheck(L_19);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_19);
		}

IL_007e:
		{
			IL2CPP_END_FINALLY(114)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(114)
	{
		IL2CPP_JUMP_TBL(0x7F, IL_007f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_007f:
	{
		bool L_20 = V_2;
		return L_20;
	}
}
// System.Boolean WebSocketSharp.WebSocket::acceptFrame(WebSocketSharp.WebSocketFrame)
extern Il2CppCodeGenString* _stringLiteral2031729340;
extern const uint32_t WebSocket_acceptFrame_m511668371_MetadataUsageId;
extern "C"  bool WebSocket_acceptFrame_m511668371 (WebSocket_t1342580397 * __this, WebSocketFrame_t778194306 * ___frame0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_acceptFrame_m511668371_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool G_B14_0 = false;
	{
		WebSocketFrame_t778194306 * L_0 = ___frame0;
		NullCheck(L_0);
		bool L_1 = WebSocketFrame_get_IsCompressed_m3923144746(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002c;
		}
	}
	{
		uint8_t L_2 = __this->get__compression_8();
		if (L_2)
		{
			goto IL_002c;
		}
	}
	{
		WebSocketFrame_t778194306 * L_3 = ___frame0;
		bool L_4 = WebSocket_acceptUnsupportedFrame_m1827787566(__this, L_3, ((int32_t)1003), _stringLiteral2031729340, /*hidden argument*/NULL);
		G_B14_0 = L_4;
		goto IL_00ac;
	}

IL_002c:
	{
		WebSocketFrame_t778194306 * L_5 = ___frame0;
		NullCheck(L_5);
		bool L_6 = WebSocketFrame_get_IsFragmented_m98582552(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0043;
		}
	}
	{
		WebSocketFrame_t778194306 * L_7 = ___frame0;
		bool L_8 = WebSocket_acceptFragmentedFrame_m496170018(__this, L_7, /*hidden argument*/NULL);
		G_B14_0 = L_8;
		goto IL_00ac;
	}

IL_0043:
	{
		WebSocketFrame_t778194306 * L_9 = ___frame0;
		NullCheck(L_9);
		bool L_10 = WebSocketFrame_get_IsData_m2997444627(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_005a;
		}
	}
	{
		WebSocketFrame_t778194306 * L_11 = ___frame0;
		bool L_12 = WebSocket_acceptDataFrame_m1452099421(__this, L_11, /*hidden argument*/NULL);
		G_B14_0 = L_12;
		goto IL_00ac;
	}

IL_005a:
	{
		WebSocketFrame_t778194306 * L_13 = ___frame0;
		NullCheck(L_13);
		bool L_14 = WebSocketFrame_get_IsPing_m3348209627(L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0071;
		}
	}
	{
		WebSocketFrame_t778194306 * L_15 = ___frame0;
		bool L_16 = WebSocket_acceptPingFrame_m3791361317(__this, L_15, /*hidden argument*/NULL);
		G_B14_0 = L_16;
		goto IL_00ac;
	}

IL_0071:
	{
		WebSocketFrame_t778194306 * L_17 = ___frame0;
		NullCheck(L_17);
		bool L_18 = WebSocketFrame_get_IsPong_m3353750753(L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0088;
		}
	}
	{
		WebSocketFrame_t778194306 * L_19 = ___frame0;
		bool L_20 = WebSocket_acceptPongFrame_m2550227115(__this, L_19, /*hidden argument*/NULL);
		G_B14_0 = L_20;
		goto IL_00ac;
	}

IL_0088:
	{
		WebSocketFrame_t778194306 * L_21 = ___frame0;
		NullCheck(L_21);
		bool L_22 = WebSocketFrame_get_IsClose_m2149864465(L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_009f;
		}
	}
	{
		WebSocketFrame_t778194306 * L_23 = ___frame0;
		bool L_24 = WebSocket_acceptCloseFrame_m1331471019(__this, L_23, /*hidden argument*/NULL);
		G_B14_0 = L_24;
		goto IL_00ac;
	}

IL_009f:
	{
		WebSocketFrame_t778194306 * L_25 = ___frame0;
		bool L_26 = WebSocket_acceptUnsupportedFrame_m1827787566(__this, L_25, ((int32_t)1008), (String_t*)NULL, /*hidden argument*/NULL);
		G_B14_0 = L_26;
	}

IL_00ac:
	{
		return G_B14_0;
	}
}
// System.Boolean WebSocketSharp.WebSocket::acceptHandshake()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t3730860138_il2cpp_TypeInfo_var;
extern const MethodInfo* WebSocket_U3CacceptHandshakeU3Em__D_m2493968471_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3001890441_MethodInfo_var;
extern const MethodInfo* Ext_Contains_TisString_t_m802592955_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4214844074;
extern Il2CppCodeGenString* _stringLiteral534662685;
extern Il2CppCodeGenString* _stringLiteral1023006230;
extern const uint32_t WebSocket_acceptHandshake_m303332664_MetadataUsageId;
extern "C"  bool WebSocket_acceptHandshake_m303332664 (WebSocket_t1342580397 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_acceptHandshake_m303332664_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	{
		Logger_t3695440972 * L_0 = __this->get__logger_19();
		il2cpp_codegen_memory_barrier();
		WebSocketContext_t763725542 * L_1 = __this->get__context_9();
		NullCheck(L_1);
		IPEndPoint_t2123960758 * L_2 = VirtFuncInvoker0< IPEndPoint_t2123960758 * >::Invoke(19 /* System.Net.IPEndPoint WebSocketSharp.Net.WebSockets.WebSocketContext::get_UserEndPoint() */, L_1);
		WebSocketContext_t763725542 * L_3 = __this->get__context_9();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Format_m2398979370(NULL /*static, unused*/, _stringLiteral4214844074, L_2, L_3, /*hidden argument*/NULL);
		NullCheck(L_0);
		Logger_Debug_m2108157889(L_0, L_4, /*hidden argument*/NULL);
		WebSocketContext_t763725542 * L_5 = __this->get__context_9();
		String_t* L_6 = WebSocket_checkIfValidHandshakeRequest_m199201705(__this, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		String_t* L_7 = V_0;
		if (!L_7)
		{
			goto IL_0061;
		}
	}
	{
		Logger_t3695440972 * L_8 = __this->get__logger_19();
		il2cpp_codegen_memory_barrier();
		String_t* L_9 = V_0;
		NullCheck(L_8);
		Logger_Error_m3981980524(L_8, L_9, /*hidden argument*/NULL);
		WebSocket_error_m149167037(__this, _stringLiteral534662685, /*hidden argument*/NULL);
		WebSocket_Close_m4199074618(__this, ((int32_t)400), /*hidden argument*/NULL);
		return (bool)0;
	}

IL_0061:
	{
		String_t* L_10 = __this->get__protocol_25();
		if (!L_10)
		{
			goto IL_0094;
		}
	}
	{
		WebSocketContext_t763725542 * L_11 = __this->get__context_9();
		NullCheck(L_11);
		Il2CppObject* L_12 = VirtFuncInvoker0< Il2CppObject* >::Invoke(15 /* System.Collections.Generic.IEnumerable`1<System.String> WebSocketSharp.Net.WebSockets.WebSocketContext::get_SecWebSocketProtocols() */, L_11);
		IntPtr_t L_13;
		L_13.set_m_value_0((void*)(void*)WebSocket_U3CacceptHandshakeU3Em__D_m2493968471_MethodInfo_var);
		Func_2_t3730860138 * L_14 = (Func_2_t3730860138 *)il2cpp_codegen_object_new(Func_2_t3730860138_il2cpp_TypeInfo_var);
		Func_2__ctor_m3001890441(L_14, __this, L_13, /*hidden argument*/Func_2__ctor_m3001890441_MethodInfo_var);
		bool L_15 = Ext_Contains_TisString_t_m802592955(NULL /*static, unused*/, L_12, L_14, /*hidden argument*/Ext_Contains_TisString_t_m802592955_MethodInfo_var);
		if (L_15)
		{
			goto IL_0094;
		}
	}
	{
		__this->set__protocol_25((String_t*)NULL);
	}

IL_0094:
	{
		WebSocketContext_t763725542 * L_16 = __this->get__context_9();
		NullCheck(L_16);
		NameValueCollection_t2791941106 * L_17 = VirtFuncInvoker0< NameValueCollection_t2791941106 * >::Invoke(5 /* System.Collections.Specialized.NameValueCollection WebSocketSharp.Net.WebSockets.WebSocketContext::get_Headers() */, L_16);
		NullCheck(L_17);
		String_t* L_18 = NameValueCollection_get_Item_m3428939380(L_17, _stringLiteral1023006230, /*hidden argument*/NULL);
		V_1 = L_18;
		String_t* L_19 = V_1;
		if (!L_19)
		{
			goto IL_00c3;
		}
	}
	{
		String_t* L_20 = V_1;
		NullCheck(L_20);
		int32_t L_21 = String_get_Length_m2979997331(L_20, /*hidden argument*/NULL);
		if ((((int32_t)L_21) <= ((int32_t)0)))
		{
			goto IL_00c3;
		}
	}
	{
		String_t* L_22 = V_1;
		WebSocket_acceptSecWebSocketExtensionsHeader_m3976552830(__this, L_22, /*hidden argument*/NULL);
	}

IL_00c3:
	{
		HandshakeResponse_t3229697822 * L_23 = WebSocket_createHandshakeResponse_m4091239465(__this, /*hidden argument*/NULL);
		bool L_24 = WebSocket_send_m288551060(__this, L_23, /*hidden argument*/NULL);
		return L_24;
	}
}
// System.Boolean WebSocketSharp.WebSocket::acceptPingFrame(WebSocketSharp.WebSocketFrame)
extern Il2CppClass* WebSocketFrame_t778194306_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral72965478;
extern const uint32_t WebSocket_acceptPingFrame_m3791361317_MetadataUsageId;
extern "C"  bool WebSocket_acceptPingFrame_m3791361317 (WebSocket_t1342580397 * __this, WebSocketFrame_t778194306 * ___frame0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_acceptPingFrame_m3791361317_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint8_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = __this->get__client_6();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = 1;
		goto IL_0012;
	}

IL_0011:
	{
		G_B3_0 = 0;
	}

IL_0012:
	{
		V_0 = G_B3_0;
		uint8_t L_1 = V_0;
		WebSocketFrame_t778194306 * L_2 = ___frame0;
		NullCheck(L_2);
		PayloadData_t39926750 * L_3 = WebSocketFrame_get_PayloadData_m4173896503(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t778194306_il2cpp_TypeInfo_var);
		WebSocketFrame_t778194306 * L_4 = WebSocketFrame_CreatePongFrame_m950933809(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		bool L_5 = WebSocket_send_m2219182912(__this, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003c;
		}
	}
	{
		Logger_t3695440972 * L_6 = __this->get__logger_19();
		il2cpp_codegen_memory_barrier();
		NullCheck(L_6);
		Logger_Trace_m902107791(L_6, _stringLiteral72965478, /*hidden argument*/NULL);
	}

IL_003c:
	{
		return (bool)1;
	}
}
// System.Boolean WebSocketSharp.WebSocket::acceptPongFrame(WebSocketSharp.WebSocketFrame)
extern Il2CppCodeGenString* _stringLiteral2119522168;
extern const uint32_t WebSocket_acceptPongFrame_m2550227115_MetadataUsageId;
extern "C"  bool WebSocket_acceptPongFrame_m2550227115 (WebSocket_t1342580397 * __this, WebSocketFrame_t778194306 * ___frame0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_acceptPongFrame_m2550227115_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AutoResetEvent_t874642578 * L_0 = __this->get__receivePong_28();
		NullCheck(L_0);
		EventWaitHandle_Set_m224730030(L_0, /*hidden argument*/NULL);
		Logger_t3695440972 * L_1 = __this->get__logger_19();
		il2cpp_codegen_memory_barrier();
		NullCheck(L_1);
		Logger_Trace_m902107791(L_1, _stringLiteral2119522168, /*hidden argument*/NULL);
		return (bool)1;
	}
}
// System.Void WebSocketSharp.WebSocket::acceptSecWebSocketExtensionsHeader(System.String)
extern Il2CppClass* StringBuilder_t243639308_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t3324145743_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t3308144514_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t1919096606_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral194980118;
extern Il2CppCodeGenString* _stringLiteral1396;
extern const uint32_t WebSocket_acceptSecWebSocketExtensionsHeader_m3976552830_MetadataUsageId;
extern "C"  void WebSocket_acceptSecWebSocketExtensionsHeader_m3976552830 (WebSocket_t1342580397 * __this, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_acceptSecWebSocketExtensionsHeader_m3976552830_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t243639308 * V_0 = NULL;
	bool V_1 = false;
	String_t* V_2 = NULL;
	Il2CppObject* V_3 = NULL;
	String_t* V_4 = NULL;
	String_t* V_5 = NULL;
	uint8_t V_6 = 0;
	int32_t V_7 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		StringBuilder_t243639308 * L_0 = (StringBuilder_t243639308 *)il2cpp_codegen_object_new(StringBuilder_t243639308_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3624398269(L_0, ((int32_t)32), /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = (bool)0;
		String_t* L_1 = ___value0;
		CharU5BU5D_t3324145743* L_2 = ((CharU5BU5D_t3324145743*)SZArrayNew(CharU5BU5D_t3324145743_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)44));
		Il2CppObject* L_3 = Ext_SplitHeaderValue_m1919670706(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Il2CppObject* L_4 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.String>::GetEnumerator() */, IEnumerable_1_t3308144514_il2cpp_TypeInfo_var, L_3);
		V_3 = L_4;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		{
			goto IL_008b;
		}

IL_0026:
		{
			Il2CppObject* L_5 = V_3;
			NullCheck(L_5);
			String_t* L_6 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.String>::get_Current() */, IEnumerator_1_t1919096606_il2cpp_TypeInfo_var, L_5);
			V_2 = L_6;
			String_t* L_7 = V_2;
			NullCheck(L_7);
			String_t* L_8 = String_Trim_m1030489823(L_7, /*hidden argument*/NULL);
			V_4 = L_8;
			String_t* L_9 = V_4;
			StringU5BU5D_t4054002952* L_10 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)1));
			NullCheck(L_10);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
			ArrayElementTypeCheck (L_10, _stringLiteral194980118);
			(L_10)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral194980118);
			String_t* L_11 = Ext_RemovePrefix_m3868713732(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
			V_5 = L_11;
			bool L_12 = V_1;
			if (L_12)
			{
				goto IL_008b;
			}
		}

IL_0052:
		{
			String_t* L_13 = V_5;
			bool L_14 = Ext_IsCompressionExtension_m2453074764(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
			if (!L_14)
			{
				goto IL_008b;
			}
		}

IL_005e:
		{
			String_t* L_15 = V_5;
			uint8_t L_16 = Ext_ToCompressionMethod_m4243135028(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
			V_6 = L_16;
			uint8_t L_17 = V_6;
			if (!L_17)
			{
				goto IL_008b;
			}
		}

IL_006e:
		{
			uint8_t L_18 = V_6;
			__this->set__compression_8(L_18);
			V_1 = (bool)1;
			StringBuilder_t243639308 * L_19 = V_0;
			String_t* L_20 = V_4;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_21 = String_Concat_m138640077(NULL /*static, unused*/, L_20, _stringLiteral1396, /*hidden argument*/NULL);
			NullCheck(L_19);
			StringBuilder_Append_m3898090075(L_19, L_21, /*hidden argument*/NULL);
		}

IL_008b:
		{
			Il2CppObject* L_22 = V_3;
			NullCheck(L_22);
			bool L_23 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_22);
			if (L_23)
			{
				goto IL_0026;
			}
		}

IL_0096:
		{
			IL2CPP_LEAVE(0xA6, FINALLY_009b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_009b;
	}

FINALLY_009b:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_24 = V_3;
			if (L_24)
			{
				goto IL_009f;
			}
		}

IL_009e:
		{
			IL2CPP_END_FINALLY(155)
		}

IL_009f:
		{
			Il2CppObject* L_25 = V_3;
			NullCheck(L_25);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_25);
			IL2CPP_END_FINALLY(155)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(155)
	{
		IL2CPP_JUMP_TBL(0xA6, IL_00a6)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_00a6:
	{
		StringBuilder_t243639308 * L_26 = V_0;
		NullCheck(L_26);
		int32_t L_27 = StringBuilder_get_Length_m2443133099(L_26, /*hidden argument*/NULL);
		V_7 = L_27;
		int32_t L_28 = V_7;
		if ((((int32_t)L_28) <= ((int32_t)0)))
		{
			goto IL_00cc;
		}
	}
	{
		StringBuilder_t243639308 * L_29 = V_0;
		int32_t L_30 = V_7;
		NullCheck(L_29);
		StringBuilder_set_Length_m1952332172(L_29, ((int32_t)((int32_t)L_30-(int32_t)2)), /*hidden argument*/NULL);
		StringBuilder_t243639308 * L_31 = V_0;
		NullCheck(L_31);
		String_t* L_32 = StringBuilder_ToString_m350379841(L_31, /*hidden argument*/NULL);
		__this->set__extensions_12(L_32);
	}

IL_00cc:
	{
		return;
	}
}
// System.Boolean WebSocketSharp.WebSocket::acceptUnsupportedFrame(WebSocketSharp.WebSocketFrame,WebSocketSharp.CloseStatusCode,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocketException_t2311987812_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2826340914;
extern const uint32_t WebSocket_acceptUnsupportedFrame_m1827787566_MetadataUsageId;
extern "C"  bool WebSocket_acceptUnsupportedFrame_m1827787566 (WebSocket_t1342580397 * __this, WebSocketFrame_t778194306 * ___frame0, uint16_t ___code1, String_t* ___reason2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_acceptUnsupportedFrame_m1827787566_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Logger_t3695440972 * L_0 = __this->get__logger_19();
		il2cpp_codegen_memory_barrier();
		WebSocketFrame_t778194306 * L_1 = ___frame0;
		NullCheck(L_1);
		String_t* L_2 = WebSocketFrame_PrintToString_m2057514283(L_1, (bool)0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2826340914, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		Logger_Debug_m2108157889(L_0, L_3, /*hidden argument*/NULL);
		uint16_t L_4 = ___code1;
		String_t* L_5 = ___reason2;
		WebSocketException_t2311987812 * L_6 = (WebSocketException_t2311987812 *)il2cpp_codegen_object_new(WebSocketException_t2311987812_il2cpp_TypeInfo_var);
		WebSocketException__ctor_m960972328(L_6, L_4, L_5, /*hidden argument*/NULL);
		WebSocket_acceptException_m1180715184(__this, L_6, (String_t*)NULL, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.String WebSocketSharp.WebSocket::checkIfAvailable(System.String,System.Boolean,System.Boolean)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2666317444;
extern const uint32_t WebSocket_checkIfAvailable_m3972221078_MetadataUsageId;
extern "C"  String_t* WebSocket_checkIfAvailable_m3972221078 (WebSocket_t1342580397 * __this, String_t* ___operation0, bool ___availableAsServer1, bool ___availableAsConnected2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_checkIfAvailable_m3972221078_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B6_0 = NULL;
	{
		bool L_0 = __this->get__client_6();
		if (L_0)
		{
			goto IL_0021;
		}
	}
	{
		bool L_1 = ___availableAsServer1;
		if (L_1)
		{
			goto IL_0021;
		}
	}
	{
		String_t* L_2 = ___operation0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m138640077(NULL /*static, unused*/, L_2, _stringLiteral2666317444, /*hidden argument*/NULL);
		G_B6_0 = L_3;
		goto IL_003a;
	}

IL_0021:
	{
		bool L_4 = ___availableAsConnected2;
		if (L_4)
		{
			goto IL_0039;
		}
	}
	{
		uint16_t L_5 = __this->get__readyState_27();
		il2cpp_codegen_memory_barrier();
		String_t* L_6 = Ext_CheckIfConnectable_m1317068828(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		G_B6_0 = L_6;
		goto IL_003a;
	}

IL_0039:
	{
		G_B6_0 = ((String_t*)(NULL));
	}

IL_003a:
	{
		return G_B6_0;
	}
}
// System.String WebSocketSharp.WebSocket::checkIfCanConnect()
extern Il2CppCodeGenString* _stringLiteral3514210608;
extern const uint32_t WebSocket_checkIfCanConnect_m4087900537_MetadataUsageId;
extern "C"  String_t* WebSocket_checkIfCanConnect_m4087900537 (WebSocket_t1342580397 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_checkIfCanConnect_m4087900537_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B4_0 = NULL;
	{
		bool L_0 = __this->get__client_6();
		if (L_0)
		{
			goto IL_0023;
		}
	}
	{
		uint16_t L_1 = __this->get__readyState_27();
		il2cpp_codegen_memory_barrier();
		if ((!(((uint32_t)L_1) == ((uint32_t)3))))
		{
			goto IL_0023;
		}
	}
	{
		G_B4_0 = _stringLiteral3514210608;
		goto IL_0030;
	}

IL_0023:
	{
		uint16_t L_2 = __this->get__readyState_27();
		il2cpp_codegen_memory_barrier();
		String_t* L_3 = Ext_CheckIfConnectable_m1317068828(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		G_B4_0 = L_3;
	}

IL_0030:
	{
		return G_B4_0;
	}
}
// System.String WebSocketSharp.WebSocket::checkIfValidHandshakeRequest(WebSocketSharp.Net.WebSockets.WebSocketContext)
extern Il2CppClass* Uri_t1116831938_il2cpp_TypeInfo_var;
extern const MethodInfo* Func_2_Invoke_m1882008154_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4055524153;
extern Il2CppCodeGenString* _stringLiteral1755189243;
extern Il2CppCodeGenString* _stringLiteral691835549;
extern Il2CppCodeGenString* _stringLiteral1817079733;
extern Il2CppCodeGenString* _stringLiteral2197799446;
extern Il2CppCodeGenString* _stringLiteral735363758;
extern const uint32_t WebSocket_checkIfValidHandshakeRequest_m199201705_MetadataUsageId;
extern "C"  String_t* WebSocket_checkIfValidHandshakeRequest_m199201705 (WebSocket_t1342580397 * __this, WebSocketContext_t763725542 * ___context0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_checkIfValidHandshakeRequest_m199201705_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	NameValueCollection_t2791941106 * V_0 = NULL;
	String_t* G_B9_0 = NULL;
	{
		WebSocketContext_t763725542 * L_0 = ___context0;
		NullCheck(L_0);
		NameValueCollection_t2791941106 * L_1 = VirtFuncInvoker0< NameValueCollection_t2791941106 * >::Invoke(5 /* System.Collections.Specialized.NameValueCollection WebSocketSharp.Net.WebSockets.WebSocketContext::get_Headers() */, L_0);
		V_0 = L_1;
		WebSocketContext_t763725542 * L_2 = ___context0;
		NullCheck(L_2);
		Uri_t1116831938 * L_3 = VirtFuncInvoker0< Uri_t1116831938 * >::Invoke(13 /* System.Uri WebSocketSharp.Net.WebSockets.WebSocketContext::get_RequestUri() */, L_2);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t1116831938_il2cpp_TypeInfo_var);
		bool L_4 = Uri_op_Equality_m877019543(NULL /*static, unused*/, L_3, (Uri_t1116831938 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0022;
		}
	}
	{
		G_B9_0 = _stringLiteral4055524153;
		goto IL_0083;
	}

IL_0022:
	{
		WebSocketContext_t763725542 * L_5 = ___context0;
		NullCheck(L_5);
		bool L_6 = VirtFuncInvoker0< bool >::Invoke(10 /* System.Boolean WebSocketSharp.Net.WebSockets.WebSocketContext::get_IsWebSocketRequest() */, L_5);
		if (L_6)
		{
			goto IL_0037;
		}
	}
	{
		G_B9_0 = _stringLiteral1755189243;
		goto IL_0083;
	}

IL_0037:
	{
		NameValueCollection_t2791941106 * L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = NameValueCollection_get_Item_m3428939380(L_7, _stringLiteral691835549, /*hidden argument*/NULL);
		bool L_9 = WebSocket_validateSecWebSocketKeyHeader_m1076494873(__this, L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0057;
		}
	}
	{
		G_B9_0 = _stringLiteral1817079733;
		goto IL_0083;
	}

IL_0057:
	{
		NameValueCollection_t2791941106 * L_10 = V_0;
		NullCheck(L_10);
		String_t* L_11 = NameValueCollection_get_Item_m3428939380(L_10, _stringLiteral2197799446, /*hidden argument*/NULL);
		bool L_12 = WebSocket_validateSecWebSocketVersionClientHeader_m2132125493(__this, L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0077;
		}
	}
	{
		G_B9_0 = _stringLiteral735363758;
		goto IL_0083;
	}

IL_0077:
	{
		Func_2_t636757868 * L_13 = WebSocket_get_CustomHandshakeRequestChecker_m1227021650(__this, /*hidden argument*/NULL);
		WebSocketContext_t763725542 * L_14 = ___context0;
		NullCheck(L_13);
		String_t* L_15 = Func_2_Invoke_m1882008154(L_13, L_14, /*hidden argument*/Func_2_Invoke_m1882008154_MethodInfo_var);
		G_B9_0 = L_15;
	}

IL_0083:
	{
		return G_B9_0;
	}
}
// System.String WebSocketSharp.WebSocket::checkIfValidHandshakeResponse(WebSocketSharp.HandshakeResponse)
extern Il2CppClass* AuthenticationSchemes_t3190130368_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3383738096;
extern Il2CppCodeGenString* _stringLiteral217240337;
extern Il2CppCodeGenString* _stringLiteral2931060746;
extern Il2CppCodeGenString* _stringLiteral1229888212;
extern Il2CppCodeGenString* _stringLiteral408195354;
extern Il2CppCodeGenString* _stringLiteral2021189028;
extern Il2CppCodeGenString* _stringLiteral1023006230;
extern Il2CppCodeGenString* _stringLiteral2881763424;
extern Il2CppCodeGenString* _stringLiteral2197799446;
extern Il2CppCodeGenString* _stringLiteral735363758;
extern const uint32_t WebSocket_checkIfValidHandshakeResponse_m1340903150_MetadataUsageId;
extern "C"  String_t* WebSocket_checkIfValidHandshakeResponse_m1340903150 (WebSocket_t1342580397 * __this, HandshakeResponse_t3229697822 * ___response0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_checkIfValidHandshakeResponse_m1340903150_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	NameValueCollection_t2791941106 * V_0 = NULL;
	String_t* G_B13_0 = NULL;
	{
		HandshakeResponse_t3229697822 * L_0 = ___response0;
		NullCheck(L_0);
		NameValueCollection_t2791941106 * L_1 = HandshakeBase_get_Headers_m3632488899(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		HandshakeResponse_t3229697822 * L_2 = ___response0;
		NullCheck(L_2);
		bool L_3 = HandshakeResponse_get_IsUnauthorized_m1179508529(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0031;
		}
	}
	{
		HandshakeResponse_t3229697822 * L_4 = ___response0;
		NullCheck(L_4);
		AuthenticationChallenge_t1782907061 * L_5 = HandshakeResponse_get_AuthChallenge_m1867389680(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = AuthenticationBase_get_Scheme_m3444393929(L_5, /*hidden argument*/NULL);
		int32_t L_7 = L_6;
		Il2CppObject * L_8 = Box(AuthenticationSchemes_t3190130368_il2cpp_TypeInfo_var, &L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral3383738096, L_8, /*hidden argument*/NULL);
		G_B13_0 = L_9;
		goto IL_00c7;
	}

IL_0031:
	{
		HandshakeResponse_t3229697822 * L_10 = ___response0;
		NullCheck(L_10);
		bool L_11 = HandshakeResponse_get_IsWebSocketResponse_m4059370381(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_0046;
		}
	}
	{
		G_B13_0 = _stringLiteral217240337;
		goto IL_00c7;
	}

IL_0046:
	{
		NameValueCollection_t2791941106 * L_12 = V_0;
		NullCheck(L_12);
		String_t* L_13 = NameValueCollection_get_Item_m3428939380(L_12, _stringLiteral2931060746, /*hidden argument*/NULL);
		bool L_14 = WebSocket_validateSecWebSocketAcceptHeader_m2593449224(__this, L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_0066;
		}
	}
	{
		G_B13_0 = _stringLiteral1229888212;
		goto IL_00c7;
	}

IL_0066:
	{
		NameValueCollection_t2791941106 * L_15 = V_0;
		NullCheck(L_15);
		String_t* L_16 = NameValueCollection_get_Item_m3428939380(L_15, _stringLiteral408195354, /*hidden argument*/NULL);
		bool L_17 = WebSocket_validateSecWebSocketProtocolHeader_m3777734264(__this, L_16, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_0086;
		}
	}
	{
		G_B13_0 = _stringLiteral2021189028;
		goto IL_00c7;
	}

IL_0086:
	{
		NameValueCollection_t2791941106 * L_18 = V_0;
		NullCheck(L_18);
		String_t* L_19 = NameValueCollection_get_Item_m3428939380(L_18, _stringLiteral1023006230, /*hidden argument*/NULL);
		bool L_20 = WebSocket_validateSecWebSocketExtensionsHeader_m3169128316(__this, L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_00a6;
		}
	}
	{
		G_B13_0 = _stringLiteral2881763424;
		goto IL_00c7;
	}

IL_00a6:
	{
		NameValueCollection_t2791941106 * L_21 = V_0;
		NullCheck(L_21);
		String_t* L_22 = NameValueCollection_get_Item_m3428939380(L_21, _stringLiteral2197799446, /*hidden argument*/NULL);
		bool L_23 = WebSocket_validateSecWebSocketVersionServerHeader_m3224946365(__this, L_22, /*hidden argument*/NULL);
		if (L_23)
		{
			goto IL_00c6;
		}
	}
	{
		G_B13_0 = _stringLiteral735363758;
		goto IL_00c7;
	}

IL_00c6:
	{
		G_B13_0 = ((String_t*)(NULL));
	}

IL_00c7:
	{
		return G_B13_0;
	}
}
// System.Void WebSocketSharp.WebSocket::close(WebSocketSharp.CloseStatusCode,System.String,System.Boolean)
extern Il2CppClass* PayloadData_t39926750_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_close_m1268085240_MetadataUsageId;
extern "C"  void WebSocket_close_m1268085240 (WebSocket_t1342580397 * __this, uint16_t ___code0, String_t* ___reason1, bool ___wait2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_close_m1268085240_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		uint16_t L_0 = ___code0;
		String_t* L_1 = ___reason1;
		ByteU5BU5D_t4260760469* L_2 = Ext_Append_m501190413(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		PayloadData_t39926750 * L_3 = (PayloadData_t39926750 *)il2cpp_codegen_object_new(PayloadData_t39926750_il2cpp_TypeInfo_var);
		PayloadData__ctor_m719751291(L_3, L_2, /*hidden argument*/NULL);
		uint16_t L_4 = ___code0;
		bool L_5 = Ext_IsReserved_m4250492537(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		bool L_6 = ___wait2;
		WebSocket_close_m707576104(__this, L_3, (bool)((((int32_t)L_5) == ((int32_t)0))? 1 : 0), L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::close(WebSocketSharp.PayloadData,System.Boolean,System.Boolean)
extern Il2CppClass* CloseEventArgs_t2470319931_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocketFrame_t778194306_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern const MethodInfo* WebSocket_closeClientResources_m4025636903_MethodInfo_var;
extern const MethodInfo* WebSocket_closeServerResources_m2248524207_MethodInfo_var;
extern const MethodInfo* Ext_Emit_TisCloseEventArgs_t2470319931_m1774194048_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3390759605;
extern Il2CppCodeGenString* _stringLiteral574002176;
extern Il2CppCodeGenString* _stringLiteral1813654247;
extern Il2CppCodeGenString* _stringLiteral1668644629;
extern const uint32_t WebSocket_close_m707576104_MetadataUsageId;
extern "C"  void WebSocket_close_m707576104 (WebSocket_t1342580397 * __this, PayloadData_t39926750 * ___payload0, bool ___send1, bool ___wait2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_close_m707576104_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	CloseEventArgs_t2470319931 * V_1 = NULL;
	Exception_t3991598821 * V_2 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	CloseEventArgs_t2470319931 * G_B14_0 = NULL;
	CloseEventArgs_t2470319931 * G_B7_0 = NULL;
	WebSocket_t1342580397 * G_B9_0 = NULL;
	CloseEventArgs_t2470319931 * G_B9_1 = NULL;
	WebSocket_t1342580397 * G_B8_0 = NULL;
	CloseEventArgs_t2470319931 * G_B8_1 = NULL;
	ByteU5BU5D_t4260760469* G_B10_0 = NULL;
	WebSocket_t1342580397 * G_B10_1 = NULL;
	CloseEventArgs_t2470319931 * G_B10_2 = NULL;
	ByteU5BU5D_t4260760469* G_B12_0 = NULL;
	WebSocket_t1342580397 * G_B12_1 = NULL;
	CloseEventArgs_t2470319931 * G_B12_2 = NULL;
	ByteU5BU5D_t4260760469* G_B11_0 = NULL;
	WebSocket_t1342580397 * G_B11_1 = NULL;
	CloseEventArgs_t2470319931 * G_B11_2 = NULL;
	int32_t G_B13_0 = 0;
	ByteU5BU5D_t4260760469* G_B13_1 = NULL;
	WebSocket_t1342580397 * G_B13_2 = NULL;
	CloseEventArgs_t2470319931 * G_B13_3 = NULL;
	bool G_B21_0 = false;
	CloseEventArgs_t2470319931 * G_B21_1 = NULL;
	WebSocket_t1342580397 * G_B16_0 = NULL;
	CloseEventArgs_t2470319931 * G_B16_1 = NULL;
	WebSocket_t1342580397 * G_B15_0 = NULL;
	CloseEventArgs_t2470319931 * G_B15_1 = NULL;
	ByteU5BU5D_t4260760469* G_B17_0 = NULL;
	WebSocket_t1342580397 * G_B17_1 = NULL;
	CloseEventArgs_t2470319931 * G_B17_2 = NULL;
	ByteU5BU5D_t4260760469* G_B19_0 = NULL;
	WebSocket_t1342580397 * G_B19_1 = NULL;
	CloseEventArgs_t2470319931 * G_B19_2 = NULL;
	ByteU5BU5D_t4260760469* G_B18_0 = NULL;
	WebSocket_t1342580397 * G_B18_1 = NULL;
	CloseEventArgs_t2470319931 * G_B18_2 = NULL;
	int32_t G_B20_0 = 0;
	ByteU5BU5D_t4260760469* G_B20_1 = NULL;
	WebSocket_t1342580397 * G_B20_2 = NULL;
	CloseEventArgs_t2470319931 * G_B20_3 = NULL;
	{
		Il2CppObject * L_0 = __this->get__forConn_14();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			uint16_t L_2 = __this->get__readyState_27();
			il2cpp_codegen_memory_barrier();
			if ((((int32_t)L_2) == ((int32_t)2)))
			{
				goto IL_0029;
			}
		}

IL_001b:
		{
			uint16_t L_3 = __this->get__readyState_27();
			il2cpp_codegen_memory_barrier();
			if ((!(((uint32_t)L_3) == ((uint32_t)3))))
			{
				goto IL_0040;
			}
		}

IL_0029:
		{
			Logger_t3695440972 * L_4 = __this->get__logger_19();
			il2cpp_codegen_memory_barrier();
			NullCheck(L_4);
			Logger_Info_m861041664(L_4, _stringLiteral3390759605, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x14B, FINALLY_004e);
		}

IL_0040:
		{
			il2cpp_codegen_memory_barrier();
			__this->set__readyState_27(2);
			IL2CPP_LEAVE(0x55, FINALLY_004e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_004e;
	}

FINALLY_004e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(78)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(78)
	{
		IL2CPP_JUMP_TBL(0x14B, IL_014b)
		IL2CPP_JUMP_TBL(0x55, IL_0055)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0055:
	{
		Logger_t3695440972 * L_6 = __this->get__logger_19();
		il2cpp_codegen_memory_barrier();
		NullCheck(L_6);
		Logger_Trace_m902107791(L_6, _stringLiteral574002176, /*hidden argument*/NULL);
		PayloadData_t39926750 * L_7 = ___payload0;
		CloseEventArgs_t2470319931 * L_8 = (CloseEventArgs_t2470319931 *)il2cpp_codegen_object_new(CloseEventArgs_t2470319931_il2cpp_TypeInfo_var);
		CloseEventArgs__ctor_m287930004(L_8, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		CloseEventArgs_t2470319931 * L_9 = V_1;
		bool L_10 = __this->get__client_6();
		G_B7_0 = L_9;
		if (!L_10)
		{
			G_B14_0 = L_9;
			goto IL_00ba;
		}
	}
	{
		bool L_11 = ___send1;
		G_B8_0 = __this;
		G_B8_1 = G_B7_0;
		if (!L_11)
		{
			G_B9_0 = __this;
			G_B9_1 = G_B7_0;
			goto IL_0092;
		}
	}
	{
		PayloadData_t39926750 * L_12 = ___payload0;
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t778194306_il2cpp_TypeInfo_var);
		WebSocketFrame_t778194306 * L_13 = WebSocketFrame_CreateCloseFrame_m431346455(NULL /*static, unused*/, 1, L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		ByteU5BU5D_t4260760469* L_14 = WebSocketFrame_ToByteArray_m3468314104(L_13, /*hidden argument*/NULL);
		G_B10_0 = L_14;
		G_B10_1 = G_B8_0;
		G_B10_2 = G_B8_1;
		goto IL_0093;
	}

IL_0092:
	{
		G_B10_0 = ((ByteU5BU5D_t4260760469*)(NULL));
		G_B10_1 = G_B9_0;
		G_B10_2 = G_B9_1;
	}

IL_0093:
	{
		bool L_15 = ___wait2;
		G_B11_0 = G_B10_0;
		G_B11_1 = G_B10_1;
		G_B11_2 = G_B10_2;
		if (!L_15)
		{
			G_B12_0 = G_B10_0;
			G_B12_1 = G_B10_1;
			G_B12_2 = G_B10_2;
			goto IL_00a3;
		}
	}
	{
		G_B13_0 = ((int32_t)5000);
		G_B13_1 = G_B11_0;
		G_B13_2 = G_B11_1;
		G_B13_3 = G_B11_2;
		goto IL_00a4;
	}

IL_00a3:
	{
		G_B13_0 = 0;
		G_B13_1 = G_B12_0;
		G_B13_2 = G_B12_1;
		G_B13_3 = G_B12_2;
	}

IL_00a4:
	{
		IntPtr_t L_16;
		L_16.set_m_value_0((void*)(void*)WebSocket_closeClientResources_m4025636903_MethodInfo_var);
		Action_t3771233898 * L_17 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_17, __this, L_16, /*hidden argument*/NULL);
		NullCheck(G_B13_2);
		bool L_18 = WebSocket_closeHandshake_m1631040221(G_B13_2, G_B13_1, G_B13_0, L_17, /*hidden argument*/NULL);
		G_B21_0 = L_18;
		G_B21_1 = G_B13_3;
		goto IL_00f5;
	}

IL_00ba:
	{
		bool L_19 = ___send1;
		G_B15_0 = __this;
		G_B15_1 = G_B14_0;
		if (!L_19)
		{
			G_B16_0 = __this;
			G_B16_1 = G_B14_0;
			goto IL_00d2;
		}
	}
	{
		PayloadData_t39926750 * L_20 = ___payload0;
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t778194306_il2cpp_TypeInfo_var);
		WebSocketFrame_t778194306 * L_21 = WebSocketFrame_CreateCloseFrame_m431346455(NULL /*static, unused*/, 0, L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		ByteU5BU5D_t4260760469* L_22 = WebSocketFrame_ToByteArray_m3468314104(L_21, /*hidden argument*/NULL);
		G_B17_0 = L_22;
		G_B17_1 = G_B15_0;
		G_B17_2 = G_B15_1;
		goto IL_00d3;
	}

IL_00d2:
	{
		G_B17_0 = ((ByteU5BU5D_t4260760469*)(NULL));
		G_B17_1 = G_B16_0;
		G_B17_2 = G_B16_1;
	}

IL_00d3:
	{
		bool L_23 = ___wait2;
		G_B18_0 = G_B17_0;
		G_B18_1 = G_B17_1;
		G_B18_2 = G_B17_2;
		if (!L_23)
		{
			G_B19_0 = G_B17_0;
			G_B19_1 = G_B17_1;
			G_B19_2 = G_B17_2;
			goto IL_00e3;
		}
	}
	{
		G_B20_0 = ((int32_t)1000);
		G_B20_1 = G_B18_0;
		G_B20_2 = G_B18_1;
		G_B20_3 = G_B18_2;
		goto IL_00e4;
	}

IL_00e3:
	{
		G_B20_0 = 0;
		G_B20_1 = G_B19_0;
		G_B20_2 = G_B19_1;
		G_B20_3 = G_B19_2;
	}

IL_00e4:
	{
		IntPtr_t L_24;
		L_24.set_m_value_0((void*)(void*)WebSocket_closeServerResources_m2248524207_MethodInfo_var);
		Action_t3771233898 * L_25 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_25, __this, L_24, /*hidden argument*/NULL);
		NullCheck(G_B20_2);
		bool L_26 = WebSocket_closeHandshake_m1631040221(G_B20_2, G_B20_1, G_B20_0, L_25, /*hidden argument*/NULL);
		G_B21_0 = L_26;
		G_B21_1 = G_B20_3;
	}

IL_00f5:
	{
		NullCheck(G_B21_1);
		CloseEventArgs_set_WasClean_m3328282967(G_B21_1, G_B21_0, /*hidden argument*/NULL);
		Logger_t3695440972 * L_27 = __this->get__logger_19();
		il2cpp_codegen_memory_barrier();
		NullCheck(L_27);
		Logger_Trace_m902107791(L_27, _stringLiteral1813654247, /*hidden argument*/NULL);
		il2cpp_codegen_memory_barrier();
		__this->set__readyState_27(3);
	}

IL_0115:
	try
	{ // begin try (depth: 1)
		EventHandler_1_t2615270477 * L_28 = __this->get_OnClose_33();
		CloseEventArgs_t2470319931 * L_29 = V_1;
		Ext_Emit_TisCloseEventArgs_t2470319931_m1774194048(NULL /*static, unused*/, L_28, __this, L_29, /*hidden argument*/Ext_Emit_TisCloseEventArgs_t2470319931_m1774194048_MethodInfo_var);
		goto IL_014b;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0127;
		throw e;
	}

CATCH_0127:
	{ // begin catch(System.Exception)
		V_2 = ((Exception_t3991598821 *)__exception_local);
		Logger_t3695440972 * L_30 = __this->get__logger_19();
		il2cpp_codegen_memory_barrier();
		Exception_t3991598821 * L_31 = V_2;
		NullCheck(L_31);
		String_t* L_32 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Exception::ToString() */, L_31);
		NullCheck(L_30);
		Logger_Fatal_m51902704(L_30, L_32, /*hidden argument*/NULL);
		WebSocket_error_m149167037(__this, _stringLiteral1668644629, /*hidden argument*/NULL);
		goto IL_014b;
	} // end catch (depth: 1)

IL_014b:
	{
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::closeAsync(WebSocketSharp.PayloadData,System.Boolean,System.Boolean)
extern Il2CppClass* U3CcloseAsyncU3Ec__AnonStorey28_t239095580_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_3_t2371519456_il2cpp_TypeInfo_var;
extern Il2CppClass* AsyncCallback_t1369114871_il2cpp_TypeInfo_var;
extern const MethodInfo* WebSocket_close_m707576104_MethodInfo_var;
extern const MethodInfo* Action_3__ctor_m4272236052_MethodInfo_var;
extern const MethodInfo* U3CcloseAsyncU3Ec__AnonStorey28_U3CU3Em__E_m3722936676_MethodInfo_var;
extern const MethodInfo* Action_3_BeginInvoke_m842234001_MethodInfo_var;
extern const uint32_t WebSocket_closeAsync_m1147887708_MetadataUsageId;
extern "C"  void WebSocket_closeAsync_m1147887708 (WebSocket_t1342580397 * __this, PayloadData_t39926750 * ___payload0, bool ___send1, bool ___wait2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_closeAsync_m1147887708_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CcloseAsyncU3Ec__AnonStorey28_t239095580 * V_0 = NULL;
	{
		U3CcloseAsyncU3Ec__AnonStorey28_t239095580 * L_0 = (U3CcloseAsyncU3Ec__AnonStorey28_t239095580 *)il2cpp_codegen_object_new(U3CcloseAsyncU3Ec__AnonStorey28_t239095580_il2cpp_TypeInfo_var);
		U3CcloseAsyncU3Ec__AnonStorey28__ctor_m55241471(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CcloseAsyncU3Ec__AnonStorey28_t239095580 * L_1 = V_0;
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)WebSocket_close_m707576104_MethodInfo_var);
		Action_3_t2371519456 * L_3 = (Action_3_t2371519456 *)il2cpp_codegen_object_new(Action_3_t2371519456_il2cpp_TypeInfo_var);
		Action_3__ctor_m4272236052(L_3, __this, L_2, /*hidden argument*/Action_3__ctor_m4272236052_MethodInfo_var);
		NullCheck(L_1);
		L_1->set_closer_0(L_3);
		U3CcloseAsyncU3Ec__AnonStorey28_t239095580 * L_4 = V_0;
		NullCheck(L_4);
		Action_3_t2371519456 * L_5 = L_4->get_closer_0();
		PayloadData_t39926750 * L_6 = ___payload0;
		bool L_7 = ___send1;
		bool L_8 = ___wait2;
		U3CcloseAsyncU3Ec__AnonStorey28_t239095580 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)U3CcloseAsyncU3Ec__AnonStorey28_U3CU3Em__E_m3722936676_MethodInfo_var);
		AsyncCallback_t1369114871 * L_11 = (AsyncCallback_t1369114871 *)il2cpp_codegen_object_new(AsyncCallback_t1369114871_il2cpp_TypeInfo_var);
		AsyncCallback__ctor_m1986959762(L_11, L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_5);
		Action_3_BeginInvoke_m842234001(L_5, L_6, L_7, L_8, L_11, NULL, /*hidden argument*/Action_3_BeginInvoke_m842234001_MethodInfo_var);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::closeClientResources()
extern "C"  void WebSocket_closeClientResources_m4025636903 (WebSocket_t1342580397 * __this, const MethodInfo* method)
{
	{
		WebSocketStream_t4103435597 * L_0 = __this->get__stream_30();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		WebSocketStream_t4103435597 * L_1 = __this->get__stream_30();
		NullCheck(L_1);
		WebSocketStream_Dispose_m1774972412(L_1, /*hidden argument*/NULL);
		__this->set__stream_30((WebSocketStream_t4103435597 *)NULL);
	}

IL_001d:
	{
		TcpClient_t838416830 * L_2 = __this->get__tcpClient_31();
		if (!L_2)
		{
			goto IL_003a;
		}
	}
	{
		TcpClient_t838416830 * L_3 = __this->get__tcpClient_31();
		NullCheck(L_3);
		TcpClient_Close_m3504618180(L_3, /*hidden argument*/NULL);
		__this->set__tcpClient_31((TcpClient_t838416830 *)NULL);
	}

IL_003a:
	{
		return;
	}
}
// System.Boolean WebSocketSharp.WebSocket::closeHandshake(System.Byte[],System.Int32,System.Action)
extern Il2CppClass* Boolean_t476798718_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2909275539;
extern const uint32_t WebSocket_closeHandshake_m1631040221_MetadataUsageId;
extern "C"  bool WebSocket_closeHandshake_m1631040221 (WebSocket_t1342580397 * __this, ByteU5BU5D_t4260760469* ___frame0, int32_t ___timeout1, Action_t3771233898 * ___release2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_closeHandshake_m1631040221_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	bool V_2 = false;
	int32_t G_B3_0 = 0;
	int32_t G_B8_0 = 0;
	int32_t G_B10_0 = 0;
	int32_t G_B17_0 = 0;
	{
		ByteU5BU5D_t4260760469* L_0 = ___frame0;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		WebSocketStream_t4103435597 * L_1 = __this->get__stream_30();
		ByteU5BU5D_t4260760469* L_2 = ___frame0;
		NullCheck(L_1);
		bool L_3 = WebSocketStream_Write_m2408900417(L_1, L_2, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_0015;
	}

IL_0014:
	{
		G_B3_0 = 0;
	}

IL_0015:
	{
		V_0 = (bool)G_B3_0;
		int32_t L_4 = ___timeout1;
		if (!L_4)
		{
			goto IL_003e;
		}
	}
	{
		bool L_5 = V_0;
		if (!L_5)
		{
			goto IL_003b;
		}
	}
	{
		AutoResetEvent_t874642578 * L_6 = __this->get__exitReceiving_13();
		if (!L_6)
		{
			goto IL_003b;
		}
	}
	{
		AutoResetEvent_t874642578 * L_7 = __this->get__exitReceiving_13();
		int32_t L_8 = ___timeout1;
		NullCheck(L_7);
		bool L_9 = VirtFuncInvoker1< bool, int32_t >::Invoke(11 /* System.Boolean System.Threading.WaitHandle::WaitOne(System.Int32) */, L_7, L_8);
		G_B8_0 = ((int32_t)(L_9));
		goto IL_003c;
	}

IL_003b:
	{
		G_B8_0 = 0;
	}

IL_003c:
	{
		G_B10_0 = G_B8_0;
		goto IL_003f;
	}

IL_003e:
	{
		G_B10_0 = 1;
	}

IL_003f:
	{
		V_1 = (bool)G_B10_0;
		Action_t3771233898 * L_10 = ___release2;
		NullCheck(L_10);
		Action_Invoke_m1445970038(L_10, /*hidden argument*/NULL);
		AutoResetEvent_t874642578 * L_11 = __this->get__receivePong_28();
		if (!L_11)
		{
			goto IL_0063;
		}
	}
	{
		AutoResetEvent_t874642578 * L_12 = __this->get__receivePong_28();
		NullCheck(L_12);
		VirtActionInvoker0::Invoke(5 /* System.Void System.Threading.WaitHandle::Close() */, L_12);
		__this->set__receivePong_28((AutoResetEvent_t874642578 *)NULL);
	}

IL_0063:
	{
		AutoResetEvent_t874642578 * L_13 = __this->get__exitReceiving_13();
		if (!L_13)
		{
			goto IL_0080;
		}
	}
	{
		AutoResetEvent_t874642578 * L_14 = __this->get__exitReceiving_13();
		NullCheck(L_14);
		VirtActionInvoker0::Invoke(5 /* System.Void System.Threading.WaitHandle::Close() */, L_14);
		__this->set__exitReceiving_13((AutoResetEvent_t874642578 *)NULL);
	}

IL_0080:
	{
		bool L_15 = V_0;
		if (!L_15)
		{
			goto IL_0089;
		}
	}
	{
		bool L_16 = V_1;
		G_B17_0 = ((int32_t)(L_16));
		goto IL_008a;
	}

IL_0089:
	{
		G_B17_0 = 0;
	}

IL_008a:
	{
		V_2 = (bool)G_B17_0;
		Logger_t3695440972 * L_17 = __this->get__logger_19();
		il2cpp_codegen_memory_barrier();
		bool L_18 = V_2;
		bool L_19 = L_18;
		Il2CppObject * L_20 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_19);
		bool L_21 = V_0;
		bool L_22 = L_21;
		Il2CppObject * L_23 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_22);
		bool L_24 = V_1;
		bool L_25 = L_24;
		Il2CppObject * L_26 = Box(Boolean_t476798718_il2cpp_TypeInfo_var, &L_25);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Format_m3928391288(NULL /*static, unused*/, _stringLiteral2909275539, L_20, L_23, L_26, /*hidden argument*/NULL);
		NullCheck(L_17);
		Logger_Debug_m2108157889(L_17, L_27, /*hidden argument*/NULL);
		bool L_28 = V_2;
		return L_28;
	}
}
// System.Void WebSocketSharp.WebSocket::closeServerResources()
extern "C"  void WebSocket_closeServerResources_m2248524207 (WebSocket_t1342580397 * __this, const MethodInfo* method)
{
	{
		Action_t3771233898 * L_0 = __this->get__closeContext_7();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Action_t3771233898 * L_1 = __this->get__closeContext_7();
		NullCheck(L_1);
		Action_Invoke_m1445970038(L_1, /*hidden argument*/NULL);
		__this->set__closeContext_7((Action_t3771233898 *)NULL);
		__this->set__stream_30((WebSocketStream_t4103435597 *)NULL);
		__this->set__context_9((WebSocketContext_t763725542 *)NULL);
		return;
	}
}
// System.Boolean WebSocketSharp.WebSocket::concatenateFragmentsInto(System.IO.Stream)
extern Il2CppCodeGenString* _stringLiteral1043452987;
extern const uint32_t WebSocket_concatenateFragmentsInto_m70739806_MetadataUsageId;
extern "C"  bool WebSocket_concatenateFragmentsInto_m70739806 (WebSocket_t1342580397 * __this, Stream_t1561764144 * ___dest0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_concatenateFragmentsInto_m70739806_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	WebSocketFrame_t778194306 * V_0 = NULL;

IL_0000:
	{
		WebSocketStream_t4103435597 * L_0 = __this->get__stream_30();
		NullCheck(L_0);
		WebSocketFrame_t778194306 * L_1 = WebSocketStream_ReadFrame_m2749362418(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		WebSocketFrame_t778194306 * L_2 = V_0;
		NullCheck(L_2);
		bool L_3 = WebSocketFrame_get_IsFinal_m430067727(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0080;
		}
	}
	{
		WebSocketFrame_t778194306 * L_4 = V_0;
		NullCheck(L_4);
		bool L_5 = WebSocketFrame_get_IsContinuation_m4205121376(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0038;
		}
	}
	{
		Stream_t1561764144 * L_6 = ___dest0;
		WebSocketFrame_t778194306 * L_7 = V_0;
		NullCheck(L_7);
		PayloadData_t39926750 * L_8 = WebSocketFrame_get_PayloadData_m4173896503(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		ByteU5BU5D_t4260760469* L_9 = PayloadData_get_ApplicationData_m3147947059(L_8, /*hidden argument*/NULL);
		Ext_WriteBytes_m1982528933(NULL /*static, unused*/, L_6, L_9, /*hidden argument*/NULL);
		goto IL_00b8;
	}

IL_0038:
	{
		WebSocketFrame_t778194306 * L_10 = V_0;
		NullCheck(L_10);
		bool L_11 = WebSocketFrame_get_IsPing_m3348209627(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0050;
		}
	}
	{
		WebSocketFrame_t778194306 * L_12 = V_0;
		WebSocket_acceptPingFrame_m3791361317(__this, L_12, /*hidden argument*/NULL);
		goto IL_0000;
	}

IL_0050:
	{
		WebSocketFrame_t778194306 * L_13 = V_0;
		NullCheck(L_13);
		bool L_14 = WebSocketFrame_get_IsPong_m3353750753(L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0068;
		}
	}
	{
		WebSocketFrame_t778194306 * L_15 = V_0;
		WebSocket_acceptPongFrame_m2550227115(__this, L_15, /*hidden argument*/NULL);
		goto IL_0000;
	}

IL_0068:
	{
		WebSocketFrame_t778194306 * L_16 = V_0;
		NullCheck(L_16);
		bool L_17 = WebSocketFrame_get_IsClose_m2149864465(L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_007b;
		}
	}
	{
		WebSocketFrame_t778194306 * L_18 = V_0;
		bool L_19 = WebSocket_acceptCloseFrame_m1331471019(__this, L_18, /*hidden argument*/NULL);
		return L_19;
	}

IL_007b:
	{
		goto IL_00a1;
	}

IL_0080:
	{
		WebSocketFrame_t778194306 * L_20 = V_0;
		NullCheck(L_20);
		bool L_21 = WebSocketFrame_get_IsContinuation_m4205121376(L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00a1;
		}
	}
	{
		Stream_t1561764144 * L_22 = ___dest0;
		WebSocketFrame_t778194306 * L_23 = V_0;
		NullCheck(L_23);
		PayloadData_t39926750 * L_24 = WebSocketFrame_get_PayloadData_m4173896503(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		ByteU5BU5D_t4260760469* L_25 = PayloadData_get_ApplicationData_m3147947059(L_24, /*hidden argument*/NULL);
		Ext_WriteBytes_m1982528933(NULL /*static, unused*/, L_22, L_25, /*hidden argument*/NULL);
		goto IL_0000;
	}

IL_00a1:
	{
		WebSocketFrame_t778194306 * L_26 = V_0;
		bool L_27 = WebSocket_acceptUnsupportedFrame_m1827787566(__this, L_26, ((int32_t)1003), _stringLiteral1043452987, /*hidden argument*/NULL);
		return L_27;
	}
	// Dead block : IL_00b3: br IL_0000

IL_00b8:
	{
		return (bool)1;
	}
}
// System.Boolean WebSocketSharp.WebSocket::connect()
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1211897206;
extern const uint32_t WebSocket_connect_m1396814739_MetadataUsageId;
extern "C"  bool WebSocket_connect_m1396814739 (WebSocket_t1342580397 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_connect_m1396814739_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	String_t* V_1 = NULL;
	Exception_t3991598821 * V_2 = NULL;
	bool V_3 = false;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	bool G_B6_0 = false;
	{
		Il2CppObject * L_0 = __this->get__forConn_14();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			uint16_t L_2 = __this->get__readyState_27();
			il2cpp_codegen_memory_barrier();
			String_t* L_3 = Ext_CheckIfConnectable_m1317068828(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
			V_1 = L_3;
			String_t* L_4 = V_1;
			if (!L_4)
			{
				goto IL_003d;
			}
		}

IL_0021:
		{
			Logger_t3695440972 * L_5 = __this->get__logger_19();
			il2cpp_codegen_memory_barrier();
			String_t* L_6 = V_1;
			NullCheck(L_5);
			Logger_Error_m3981980524(L_5, L_6, /*hidden argument*/NULL);
			String_t* L_7 = V_1;
			WebSocket_error_m149167037(__this, L_7, /*hidden argument*/NULL);
			V_3 = (bool)0;
			IL2CPP_LEAVE(0x98, FINALLY_0091);
		}

IL_003d:
		try
		{ // begin try (depth: 2)
			{
				bool L_8 = __this->get__client_6();
				if (!L_8)
				{
					goto IL_0053;
				}
			}

IL_0048:
			{
				bool L_9 = WebSocket_doHandshake_m4093491669(__this, /*hidden argument*/NULL);
				G_B6_0 = L_9;
				goto IL_0059;
			}

IL_0053:
			{
				bool L_10 = WebSocket_acceptHandshake_m303332664(__this, /*hidden argument*/NULL);
				G_B6_0 = L_10;
			}

IL_0059:
			{
				if (!G_B6_0)
				{
					goto IL_006e;
				}
			}

IL_005e:
			{
				il2cpp_codegen_memory_barrier();
				__this->set__readyState_27(1);
				V_3 = (bool)1;
				IL2CPP_LEAVE(0x98, FINALLY_0091);
			}

IL_006e:
			{
				goto IL_0085;
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t3991598821 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0073;
			throw e;
		}

CATCH_0073:
		{ // begin catch(System.Exception)
			V_2 = ((Exception_t3991598821 *)__exception_local);
			Exception_t3991598821 * L_11 = V_2;
			WebSocket_acceptException_m1180715184(__this, L_11, _stringLiteral1211897206, /*hidden argument*/NULL);
			goto IL_0085;
		} // end catch (depth: 2)

IL_0085:
		{
			V_3 = (bool)0;
			IL2CPP_LEAVE(0x98, FINALLY_0091);
		}

IL_008c:
		{
			; // IL_008c: leave IL_0098
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0091;
	}

FINALLY_0091:
	{ // begin finally (depth: 1)
		Il2CppObject * L_12 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(145)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(145)
	{
		IL2CPP_JUMP_TBL(0x98, IL_0098)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0098:
	{
		bool L_13 = V_3;
		return L_13;
	}
}
// System.String WebSocketSharp.WebSocket::createExtensionsRequest()
extern Il2CppClass* StringBuilder_t243639308_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_createExtensionsRequest_m292258777_MetadataUsageId;
extern "C"  String_t* WebSocket_createExtensionsRequest_m292258777 (WebSocket_t1342580397 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_createExtensionsRequest_m292258777_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t243639308 * V_0 = NULL;
	String_t* G_B5_0 = NULL;
	{
		StringBuilder_t243639308 * L_0 = (StringBuilder_t243639308 *)il2cpp_codegen_object_new(StringBuilder_t243639308_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3624398269(L_0, ((int32_t)32), /*hidden argument*/NULL);
		V_0 = L_0;
		uint8_t L_1 = __this->get__compression_8();
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		StringBuilder_t243639308 * L_2 = V_0;
		uint8_t L_3 = __this->get__compression_8();
		String_t* L_4 = Ext_ToExtensionString_m2448377977(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		StringBuilder_Append_m3898090075(L_2, L_4, /*hidden argument*/NULL);
	}

IL_0025:
	{
		StringBuilder_t243639308 * L_5 = V_0;
		NullCheck(L_5);
		int32_t L_6 = StringBuilder_get_Length_m2443133099(L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_6) <= ((int32_t)0)))
		{
			goto IL_003c;
		}
	}
	{
		StringBuilder_t243639308 * L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = StringBuilder_ToString_m350379841(L_7, /*hidden argument*/NULL);
		G_B5_0 = L_8;
		goto IL_003d;
	}

IL_003c:
	{
		G_B5_0 = ((String_t*)(NULL));
	}

IL_003d:
	{
		return G_B5_0;
	}
}
// WebSocketSharp.HandshakeRequest WebSocketSharp.WebSocket::createHandshakeRequest()
extern Il2CppClass* HandshakeRequest_t1037477780_il2cpp_TypeInfo_var;
extern Il2CppClass* AuthenticationResponse_t2112712571_il2cpp_TypeInfo_var;
extern const MethodInfo* Ext_ToString_TisString_t_m931729630_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2255304;
extern Il2CppCodeGenString* _stringLiteral2370214726;
extern Il2CppCodeGenString* _stringLiteral691835549;
extern Il2CppCodeGenString* _stringLiteral408195354;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral1023006230;
extern Il2CppCodeGenString* _stringLiteral2197799446;
extern Il2CppCodeGenString* _stringLiteral1570;
extern Il2CppCodeGenString* _stringLiteral3708358745;
extern const uint32_t WebSocket_createHandshakeRequest_m2249726057_MetadataUsageId;
extern "C"  HandshakeRequest_t1037477780 * WebSocket_createHandshakeRequest_m2249726057 (WebSocket_t1342580397 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_createHandshakeRequest_m2249726057_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	HandshakeRequest_t1037477780 * V_2 = NULL;
	NameValueCollection_t2791941106 * V_3 = NULL;
	String_t* V_4 = NULL;
	AuthenticationResponse_t2112712571 * V_5 = NULL;
	String_t* G_B3_0 = NULL;
	{
		Uri_t1116831938 * L_0 = __this->get__uri_32();
		NullCheck(L_0);
		String_t* L_1 = Uri_get_PathAndQuery_m3621173943(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Uri_t1116831938 * L_2 = __this->get__uri_32();
		NullCheck(L_2);
		int32_t L_3 = Uri_get_Port_m2253782543(L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)80)))))
		{
			goto IL_002e;
		}
	}
	{
		Uri_t1116831938 * L_4 = __this->get__uri_32();
		NullCheck(L_4);
		String_t* L_5 = Uri_get_DnsSafeHost_m905428447(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
		goto IL_0039;
	}

IL_002e:
	{
		Uri_t1116831938 * L_6 = __this->get__uri_32();
		NullCheck(L_6);
		String_t* L_7 = Uri_get_Authority_m1914393796(L_6, /*hidden argument*/NULL);
		G_B3_0 = L_7;
	}

IL_0039:
	{
		V_1 = G_B3_0;
		String_t* L_8 = V_0;
		HandshakeRequest_t1037477780 * L_9 = (HandshakeRequest_t1037477780 *)il2cpp_codegen_object_new(HandshakeRequest_t1037477780_il2cpp_TypeInfo_var);
		HandshakeRequest__ctor_m4024060442(L_9, L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		HandshakeRequest_t1037477780 * L_10 = V_2;
		NullCheck(L_10);
		NameValueCollection_t2791941106 * L_11 = HandshakeBase_get_Headers_m3632488899(L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		NameValueCollection_t2791941106 * L_12 = V_3;
		String_t* L_13 = V_1;
		NullCheck(L_12);
		NameValueCollection_set_Item_m2275159743(L_12, _stringLiteral2255304, L_13, /*hidden argument*/NULL);
		String_t* L_14 = __this->get__origin_22();
		bool L_15 = Ext_IsNullOrEmpty_m3465800922(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_0075;
		}
	}
	{
		NameValueCollection_t2791941106 * L_16 = V_3;
		String_t* L_17 = __this->get__origin_22();
		NullCheck(L_16);
		NameValueCollection_set_Item_m2275159743(L_16, _stringLiteral2370214726, L_17, /*hidden argument*/NULL);
	}

IL_0075:
	{
		NameValueCollection_t2791941106 * L_18 = V_3;
		String_t* L_19 = __this->get__base64Key_4();
		NullCheck(L_18);
		NameValueCollection_set_Item_m2275159743(L_18, _stringLiteral691835549, L_19, /*hidden argument*/NULL);
		StringU5BU5D_t4054002952* L_20 = __this->get__protocols_26();
		if (!L_20)
		{
			goto IL_00ac;
		}
	}
	{
		NameValueCollection_t2791941106 * L_21 = V_3;
		StringU5BU5D_t4054002952* L_22 = __this->get__protocols_26();
		String_t* L_23 = Ext_ToString_TisString_t_m931729630(NULL /*static, unused*/, L_22, _stringLiteral1396, /*hidden argument*/Ext_ToString_TisString_t_m931729630_MethodInfo_var);
		NullCheck(L_21);
		NameValueCollection_set_Item_m2275159743(L_21, _stringLiteral408195354, L_23, /*hidden argument*/NULL);
	}

IL_00ac:
	{
		String_t* L_24 = WebSocket_createExtensionsRequest_m292258777(__this, /*hidden argument*/NULL);
		V_4 = L_24;
		String_t* L_25 = V_4;
		if (!L_25)
		{
			goto IL_00c8;
		}
	}
	{
		NameValueCollection_t2791941106 * L_26 = V_3;
		String_t* L_27 = V_4;
		NullCheck(L_26);
		NameValueCollection_set_Item_m2275159743(L_26, _stringLiteral1023006230, L_27, /*hidden argument*/NULL);
	}

IL_00c8:
	{
		NameValueCollection_t2791941106 * L_28 = V_3;
		NullCheck(L_28);
		NameValueCollection_set_Item_m2275159743(L_28, _stringLiteral2197799446, _stringLiteral1570, /*hidden argument*/NULL);
		V_5 = (AuthenticationResponse_t2112712571 *)NULL;
		AuthenticationChallenge_t1782907061 * L_29 = __this->get__authChallenge_3();
		if (!L_29)
		{
			goto IL_011c;
		}
	}
	{
		NetworkCredential_t1204099087 * L_30 = __this->get__credentials_11();
		if (!L_30)
		{
			goto IL_011c;
		}
	}
	{
		AuthenticationChallenge_t1782907061 * L_31 = __this->get__authChallenge_3();
		NetworkCredential_t1204099087 * L_32 = __this->get__credentials_11();
		uint32_t L_33 = __this->get__nonceCount_21();
		AuthenticationResponse_t2112712571 * L_34 = (AuthenticationResponse_t2112712571 *)il2cpp_codegen_object_new(AuthenticationResponse_t2112712571_il2cpp_TypeInfo_var);
		AuthenticationResponse__ctor_m91930666(L_34, L_31, L_32, L_33, /*hidden argument*/NULL);
		V_5 = L_34;
		AuthenticationResponse_t2112712571 * L_35 = V_5;
		NullCheck(L_35);
		uint32_t L_36 = AuthenticationResponse_get_NonceCount_m622951388(L_35, /*hidden argument*/NULL);
		__this->set__nonceCount_21(L_36);
		goto IL_0134;
	}

IL_011c:
	{
		bool L_37 = __this->get__preAuth_24();
		if (!L_37)
		{
			goto IL_0134;
		}
	}
	{
		NetworkCredential_t1204099087 * L_38 = __this->get__credentials_11();
		AuthenticationResponse_t2112712571 * L_39 = (AuthenticationResponse_t2112712571 *)il2cpp_codegen_object_new(AuthenticationResponse_t2112712571_il2cpp_TypeInfo_var);
		AuthenticationResponse__ctor_m4096801069(L_39, L_38, /*hidden argument*/NULL);
		V_5 = L_39;
	}

IL_0134:
	{
		AuthenticationResponse_t2112712571 * L_40 = V_5;
		if (!L_40)
		{
			goto IL_014d;
		}
	}
	{
		NameValueCollection_t2791941106 * L_41 = V_3;
		AuthenticationResponse_t2112712571 * L_42 = V_5;
		NullCheck(L_42);
		String_t* L_43 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String WebSocketSharp.Net.AuthenticationBase::ToString() */, L_42);
		NullCheck(L_41);
		NameValueCollection_set_Item_m2275159743(L_41, _stringLiteral3708358745, L_43, /*hidden argument*/NULL);
	}

IL_014d:
	{
		NameValueCollection_t2791941106 * L_44 = __this->get__customHeaders_23();
		if (!L_44)
		{
			goto IL_0164;
		}
	}
	{
		NameValueCollection_t2791941106 * L_45 = V_3;
		NameValueCollection_t2791941106 * L_46 = __this->get__customHeaders_23();
		NullCheck(L_45);
		NameValueCollection_Add_m1634684301(L_45, L_46, /*hidden argument*/NULL);
	}

IL_0164:
	{
		CookieCollection_t1136277956 * L_47 = __this->get__cookies_10();
		NullCheck(L_47);
		int32_t L_48 = CookieCollection_get_Count_m483044223(L_47, /*hidden argument*/NULL);
		if ((((int32_t)L_48) <= ((int32_t)0)))
		{
			goto IL_0181;
		}
	}
	{
		HandshakeRequest_t1037477780 * L_49 = V_2;
		CookieCollection_t1136277956 * L_50 = __this->get__cookies_10();
		NullCheck(L_49);
		HandshakeRequest_SetCookies_m1719690283(L_49, L_50, /*hidden argument*/NULL);
	}

IL_0181:
	{
		HandshakeRequest_t1037477780 * L_51 = V_2;
		return L_51;
	}
}
// WebSocketSharp.HandshakeResponse WebSocketSharp.WebSocket::createHandshakeResponse()
extern Il2CppClass* HandshakeResponse_t3229697822_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2931060746;
extern Il2CppCodeGenString* _stringLiteral408195354;
extern Il2CppCodeGenString* _stringLiteral1023006230;
extern const uint32_t WebSocket_createHandshakeResponse_m4091239465_MetadataUsageId;
extern "C"  HandshakeResponse_t3229697822 * WebSocket_createHandshakeResponse_m4091239465 (WebSocket_t1342580397 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_createHandshakeResponse_m4091239465_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	HandshakeResponse_t3229697822 * V_0 = NULL;
	NameValueCollection_t2791941106 * V_1 = NULL;
	{
		HandshakeResponse_t3229697822 * L_0 = (HandshakeResponse_t3229697822 *)il2cpp_codegen_object_new(HandshakeResponse_t3229697822_il2cpp_TypeInfo_var);
		HandshakeResponse__ctor_m3348926657(L_0, ((int32_t)101), /*hidden argument*/NULL);
		V_0 = L_0;
		HandshakeResponse_t3229697822 * L_1 = V_0;
		NullCheck(L_1);
		NameValueCollection_t2791941106 * L_2 = HandshakeBase_get_Headers_m3632488899(L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		NameValueCollection_t2791941106 * L_3 = V_1;
		String_t* L_4 = __this->get__base64Key_4();
		String_t* L_5 = WebSocket_CreateResponseKey_m666974534(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		NameValueCollection_set_Item_m2275159743(L_3, _stringLiteral2931060746, L_5, /*hidden argument*/NULL);
		String_t* L_6 = __this->get__protocol_25();
		if (!L_6)
		{
			goto IL_0041;
		}
	}
	{
		NameValueCollection_t2791941106 * L_7 = V_1;
		String_t* L_8 = __this->get__protocol_25();
		NullCheck(L_7);
		NameValueCollection_set_Item_m2275159743(L_7, _stringLiteral408195354, L_8, /*hidden argument*/NULL);
	}

IL_0041:
	{
		String_t* L_9 = __this->get__extensions_12();
		if (!L_9)
		{
			goto IL_005d;
		}
	}
	{
		NameValueCollection_t2791941106 * L_10 = V_1;
		String_t* L_11 = __this->get__extensions_12();
		NullCheck(L_10);
		NameValueCollection_set_Item_m2275159743(L_10, _stringLiteral1023006230, L_11, /*hidden argument*/NULL);
	}

IL_005d:
	{
		CookieCollection_t1136277956 * L_12 = __this->get__cookies_10();
		NullCheck(L_12);
		int32_t L_13 = CookieCollection_get_Count_m483044223(L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)0)))
		{
			goto IL_007a;
		}
	}
	{
		HandshakeResponse_t3229697822 * L_14 = V_0;
		CookieCollection_t1136277956 * L_15 = __this->get__cookies_10();
		NullCheck(L_14);
		HandshakeResponse_SetCookies_m3204268305(L_14, L_15, /*hidden argument*/NULL);
	}

IL_007a:
	{
		HandshakeResponse_t3229697822 * L_16 = V_0;
		return L_16;
	}
}
// WebSocketSharp.HandshakeResponse WebSocketSharp.WebSocket::createHandshakeResponse(WebSocketSharp.Net.HttpStatusCode)
extern Il2CppCodeGenString* _stringLiteral2197799446;
extern Il2CppCodeGenString* _stringLiteral1570;
extern const uint32_t WebSocket_createHandshakeResponse_m1053144262_MetadataUsageId;
extern "C"  HandshakeResponse_t3229697822 * WebSocket_createHandshakeResponse_m1053144262 (WebSocket_t1342580397 * __this, int32_t ___code0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_createHandshakeResponse_m1053144262_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	HandshakeResponse_t3229697822 * V_0 = NULL;
	{
		int32_t L_0 = ___code0;
		HandshakeResponse_t3229697822 * L_1 = HandshakeResponse_CreateCloseResponse_m3277193654(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		HandshakeResponse_t3229697822 * L_2 = V_0;
		NullCheck(L_2);
		NameValueCollection_t2791941106 * L_3 = HandshakeBase_get_Headers_m3632488899(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		NameValueCollection_set_Item_m2275159743(L_3, _stringLiteral2197799446, _stringLiteral1570, /*hidden argument*/NULL);
		HandshakeResponse_t3229697822 * L_4 = V_0;
		return L_4;
	}
}
// WebSocketSharp.MessageEventArgs WebSocketSharp.WebSocket::dequeueFromMessageEventQueue()
extern const MethodInfo* Queue_1_get_Count_m4166990737_MethodInfo_var;
extern const MethodInfo* Queue_1_Dequeue_m4004481531_MethodInfo_var;
extern const uint32_t WebSocket_dequeueFromMessageEventQueue_m4080620833_MetadataUsageId;
extern "C"  MessageEventArgs_t36945740 * WebSocket_dequeueFromMessageEventQueue_m4080620833 (WebSocket_t1342580397 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_dequeueFromMessageEventQueue_m4080620833_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	MessageEventArgs_t36945740 * V_1 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	MessageEventArgs_t36945740 * G_B4_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get__forMessageEventQueue_16();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Queue_1_t2273188169 * L_2 = __this->get__messageEventQueue_20();
			NullCheck(L_2);
			int32_t L_3 = Queue_1_get_Count_m4166990737(L_2, /*hidden argument*/Queue_1_get_Count_m4166990737_MethodInfo_var);
			if ((((int32_t)L_3) <= ((int32_t)0)))
			{
				goto IL_002e;
			}
		}

IL_001e:
		{
			Queue_1_t2273188169 * L_4 = __this->get__messageEventQueue_20();
			NullCheck(L_4);
			MessageEventArgs_t36945740 * L_5 = Queue_1_Dequeue_m4004481531(L_4, /*hidden argument*/Queue_1_Dequeue_m4004481531_MethodInfo_var);
			G_B4_0 = L_5;
			goto IL_002f;
		}

IL_002e:
		{
			G_B4_0 = ((MessageEventArgs_t36945740 *)(NULL));
		}

IL_002f:
		{
			V_1 = G_B4_0;
			IL2CPP_LEAVE(0x41, FINALLY_003a);
		}

IL_0035:
		{
			; // IL_0035: leave IL_0041
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_003a;
	}

FINALLY_003a:
	{ // begin finally (depth: 1)
		Il2CppObject * L_6 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(58)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(58)
	{
		IL2CPP_JUMP_TBL(0x41, IL_0041)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0041:
	{
		MessageEventArgs_t36945740 * L_7 = V_1;
		return L_7;
	}
}
// System.Boolean WebSocketSharp.WebSocket::doHandshake()
extern Il2CppCodeGenString* _stringLiteral534662685;
extern const uint32_t WebSocket_doHandshake_m4093491669_MetadataUsageId;
extern "C"  bool WebSocket_doHandshake_m4093491669 (WebSocket_t1342580397 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_doHandshake_m4093491669_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	HandshakeResponse_t3229697822 * V_0 = NULL;
	String_t* V_1 = NULL;
	CookieCollection_t1136277956 * V_2 = NULL;
	{
		WebSocket_setClientStream_m1110293962(__this, /*hidden argument*/NULL);
		HandshakeResponse_t3229697822 * L_0 = WebSocket_sendHandshakeRequest_m556619861(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		HandshakeResponse_t3229697822 * L_1 = V_0;
		String_t* L_2 = WebSocket_checkIfValidHandshakeResponse_m1340903150(__this, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		String_t* L_3 = V_1;
		if (!L_3)
		{
			goto IL_0045;
		}
	}
	{
		Logger_t3695440972 * L_4 = __this->get__logger_19();
		il2cpp_codegen_memory_barrier();
		String_t* L_5 = V_1;
		NullCheck(L_4);
		Logger_Error_m3981980524(L_4, L_5, /*hidden argument*/NULL);
		V_1 = _stringLiteral534662685;
		String_t* L_6 = V_1;
		WebSocket_error_m149167037(__this, L_6, /*hidden argument*/NULL);
		String_t* L_7 = V_1;
		WebSocket_close_m1268085240(__this, ((int32_t)1006), L_7, (bool)0, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_0045:
	{
		HandshakeResponse_t3229697822 * L_8 = V_0;
		NullCheck(L_8);
		CookieCollection_t1136277956 * L_9 = HandshakeResponse_get_Cookies_m1824763165(L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		CookieCollection_t1136277956 * L_10 = V_2;
		NullCheck(L_10);
		int32_t L_11 = CookieCollection_get_Count_m483044223(L_10, /*hidden argument*/NULL);
		if ((((int32_t)L_11) <= ((int32_t)0)))
		{
			goto IL_0064;
		}
	}
	{
		CookieCollection_t1136277956 * L_12 = __this->get__cookies_10();
		CookieCollection_t1136277956 * L_13 = V_2;
		NullCheck(L_12);
		CookieCollection_SetOrRemove_m4091837664(L_12, L_13, /*hidden argument*/NULL);
	}

IL_0064:
	{
		return (bool)1;
	}
}
// System.Void WebSocketSharp.WebSocket::enqueueToMessageEventQueue(WebSocketSharp.MessageEventArgs)
extern const MethodInfo* Queue_1_Enqueue_m1121821142_MethodInfo_var;
extern const uint32_t WebSocket_enqueueToMessageEventQueue_m3930529079_MetadataUsageId;
extern "C"  void WebSocket_enqueueToMessageEventQueue_m3930529079 (WebSocket_t1342580397 * __this, MessageEventArgs_t36945740 * ___e0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_enqueueToMessageEventQueue_m3930529079_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = __this->get__forMessageEventQueue_16();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		Queue_1_t2273188169 * L_2 = __this->get__messageEventQueue_20();
		MessageEventArgs_t36945740 * L_3 = ___e0;
		NullCheck(L_2);
		Queue_1_Enqueue_m1121821142(L_2, L_3, /*hidden argument*/Queue_1_Enqueue_m1121821142_MethodInfo_var);
		IL2CPP_LEAVE(0x25, FINALLY_001e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_001e;
	}

FINALLY_001e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_4 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(30)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(30)
	{
		IL2CPP_JUMP_TBL(0x25, IL_0025)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0025:
	{
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::error(System.String)
extern Il2CppClass* ErrorEventArgs_t424195371_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Ext_Emit_TisErrorEventArgs_t424195371_m3920859536_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2137822489;
extern const uint32_t WebSocket_error_m149167037_MetadataUsageId;
extern "C"  void WebSocket_error_m149167037 (WebSocket_t1342580397 * __this, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_error_m149167037_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * V_0 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		EventHandler_1_t569145917 * L_0 = __this->get_OnError_34();
		String_t* L_1 = ___message0;
		ErrorEventArgs_t424195371 * L_2 = (ErrorEventArgs_t424195371 *)il2cpp_codegen_object_new(ErrorEventArgs_t424195371_il2cpp_TypeInfo_var);
		ErrorEventArgs__ctor_m1036100209(L_2, L_1, /*hidden argument*/NULL);
		Ext_Emit_TisErrorEventArgs_t424195371_m3920859536(NULL /*static, unused*/, L_0, __this, L_2, /*hidden argument*/Ext_Emit_TisErrorEventArgs_t424195371_m3920859536_MethodInfo_var);
		goto IL_003a;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.Exception)
		V_0 = ((Exception_t3991598821 *)__exception_local);
		Logger_t3695440972 * L_3 = __this->get__logger_19();
		il2cpp_codegen_memory_barrier();
		Exception_t3991598821 * L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Exception::ToString() */, L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2137822489, L_5, /*hidden argument*/NULL);
		NullCheck(L_3);
		Logger_Fatal_m51902704(L_3, L_6, /*hidden argument*/NULL);
		goto IL_003a;
	} // end catch (depth: 1)

IL_003a:
	{
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::init()
extern Il2CppClass* CookieCollection_t1136277956_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* Queue_1_t2273188169_il2cpp_TypeInfo_var;
extern Il2CppClass* ICollection_t2643922881_il2cpp_TypeInfo_var;
extern const MethodInfo* Queue_1__ctor_m885798143_MethodInfo_var;
extern const uint32_t WebSocket_init_m2877534261_MetadataUsageId;
extern "C"  void WebSocket_init_m2877534261 (WebSocket_t1342580397 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_init_m2877534261_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set__compression_8(0);
		CookieCollection_t1136277956 * L_0 = (CookieCollection_t1136277956 *)il2cpp_codegen_object_new(CookieCollection_t1136277956_il2cpp_TypeInfo_var);
		CookieCollection__ctor_m2379289581(L_0, /*hidden argument*/NULL);
		__this->set__cookies_10(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_1, /*hidden argument*/NULL);
		__this->set__forConn_14(L_1);
		Il2CppObject * L_2 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_2, /*hidden argument*/NULL);
		__this->set__forEvent_15(L_2);
		Il2CppObject * L_3 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_3, /*hidden argument*/NULL);
		__this->set__forSend_17(L_3);
		Queue_1_t2273188169 * L_4 = (Queue_1_t2273188169 *)il2cpp_codegen_object_new(Queue_1_t2273188169_il2cpp_TypeInfo_var);
		Queue_1__ctor_m885798143(L_4, /*hidden argument*/Queue_1__ctor_m885798143_MethodInfo_var);
		__this->set__messageEventQueue_20(L_4);
		Queue_1_t2273188169 * L_5 = __this->get__messageEventQueue_20();
		NullCheck(L_5);
		Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t2643922881_il2cpp_TypeInfo_var, L_5);
		__this->set__forMessageEventQueue_16(L_6);
		il2cpp_codegen_memory_barrier();
		__this->set__readyState_27(0);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::open()
extern Il2CppClass* EventArgs_t2540831021_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral480660837;
extern Il2CppCodeGenString* _stringLiteral59095254;
extern const uint32_t WebSocket_open_m3051031279_MetadataUsageId;
extern "C"  void WebSocket_open_m3051031279 (WebSocket_t1342580397 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_open_m3051031279_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t3991598821 * V_1 = NULL;
	Exception_t3991598821 * V_2 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			WebSocket_startReceiving_m3343765315(__this, /*hidden argument*/NULL);
			Il2CppObject * L_0 = __this->get__forEvent_15();
			V_0 = L_0;
			Il2CppObject * L_1 = V_0;
			Monitor_Enter_m476686225(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		}

IL_0013:
		try
		{ // begin try (depth: 2)
			try
			{ // begin try (depth: 3)
				EventHandler_t2463957060 * L_2 = __this->get_OnOpen_36();
				IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t2540831021_il2cpp_TypeInfo_var);
				EventArgs_t2540831021 * L_3 = ((EventArgs_t2540831021_StaticFields*)EventArgs_t2540831021_il2cpp_TypeInfo_var->static_fields)->get_Empty_0();
				Ext_Emit_m3145536047(NULL /*static, unused*/, L_2, __this, L_3, /*hidden argument*/NULL);
				goto IL_003b;
			} // end try (depth: 3)
			catch(Il2CppExceptionWrapper& e)
			{
				__exception_local = (Exception_t3991598821 *)e.ex;
				if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
					goto CATCH_0029;
				throw e;
			}

CATCH_0029:
			{ // begin catch(System.Exception)
				V_1 = ((Exception_t3991598821 *)__exception_local);
				Exception_t3991598821 * L_4 = V_1;
				WebSocket_acceptException_m1180715184(__this, L_4, _stringLiteral480660837, /*hidden argument*/NULL);
				goto IL_003b;
			} // end catch (depth: 3)

IL_003b:
			{
				IL2CPP_LEAVE(0x47, FINALLY_0040);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
			goto FINALLY_0040;
		}

FINALLY_0040:
		{ // begin finally (depth: 2)
			Il2CppObject * L_5 = V_0;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(64)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(64)
		{
			IL2CPP_JUMP_TBL(0x47, IL_0047)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
		}

IL_0047:
		{
			goto IL_005e;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_004c;
		throw e;
	}

CATCH_004c:
	{ // begin catch(System.Exception)
		V_2 = ((Exception_t3991598821 *)__exception_local);
		Exception_t3991598821 * L_6 = V_2;
		WebSocket_acceptException_m1180715184(__this, L_6, _stringLiteral59095254, /*hidden argument*/NULL);
		goto IL_005e;
	} // end catch (depth: 1)

IL_005e:
	{
		return;
	}
}
// WebSocketSharp.HandshakeResponse WebSocketSharp.WebSocket::receiveHandshakeResponse()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3148391251;
extern const uint32_t WebSocket_receiveHandshakeResponse_m1807057802_MetadataUsageId;
extern "C"  HandshakeResponse_t3229697822 * WebSocket_receiveHandshakeResponse_m1807057802 (WebSocket_t1342580397 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_receiveHandshakeResponse_m1807057802_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	HandshakeResponse_t3229697822 * V_0 = NULL;
	{
		WebSocketStream_t4103435597 * L_0 = __this->get__stream_30();
		NullCheck(L_0);
		HandshakeResponse_t3229697822 * L_1 = WebSocketStream_ReadHandshakeResponse_m569574287(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Logger_t3695440972 * L_2 = __this->get__logger_19();
		il2cpp_codegen_memory_barrier();
		HandshakeResponse_t3229697822 * L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String WebSocketSharp.HandshakeResponse::ToString() */, L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral3148391251, L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		Logger_Debug_m2108157889(L_2, L_5, /*hidden argument*/NULL);
		HandshakeResponse_t3229697822 * L_6 = V_0;
		return L_6;
	}
}
// System.Boolean WebSocketSharp.WebSocket::send(System.Byte[])
extern Il2CppCodeGenString* _stringLiteral3867820340;
extern const uint32_t WebSocket_send_m1644284424_MetadataUsageId;
extern "C"  bool WebSocket_send_m1644284424 (WebSocket_t1342580397 * __this, ByteU5BU5D_t4260760469* ___frame0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_send_m1644284424_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = __this->get__forConn_14();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			uint16_t L_2 = __this->get__readyState_27();
			il2cpp_codegen_memory_barrier();
			if ((((int32_t)L_2) == ((int32_t)1)))
			{
				goto IL_0034;
			}
		}

IL_001b:
		{
			Logger_t3695440972 * L_3 = __this->get__logger_19();
			il2cpp_codegen_memory_barrier();
			NullCheck(L_3);
			Logger_Warn_m1570757416(L_3, _stringLiteral3867820340, /*hidden argument*/NULL);
			V_1 = (bool)0;
			IL2CPP_LEAVE(0x52, FINALLY_004b);
		}

IL_0034:
		{
			WebSocketStream_t4103435597 * L_4 = __this->get__stream_30();
			ByteU5BU5D_t4260760469* L_5 = ___frame0;
			NullCheck(L_4);
			bool L_6 = WebSocketStream_Write_m2408900417(L_4, L_5, /*hidden argument*/NULL);
			V_1 = L_6;
			IL2CPP_LEAVE(0x52, FINALLY_004b);
		}

IL_0046:
		{
			; // IL_0046: leave IL_0052
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_004b;
	}

FINALLY_004b:
	{ // begin finally (depth: 1)
		Il2CppObject * L_7 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(75)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(75)
	{
		IL2CPP_JUMP_TBL(0x52, IL_0052)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0052:
	{
		bool L_8 = V_1;
		return L_8;
	}
}
// System.Void WebSocketSharp.WebSocket::send(WebSocketSharp.HandshakeRequest)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2233467961;
extern const uint32_t WebSocket_send_m346101410_MetadataUsageId;
extern "C"  void WebSocket_send_m346101410 (WebSocket_t1342580397 * __this, HandshakeRequest_t1037477780 * ___request0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_send_m346101410_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Logger_t3695440972 * L_0 = __this->get__logger_19();
		il2cpp_codegen_memory_barrier();
		Uri_t1116831938 * L_1 = __this->get__uri_32();
		HandshakeRequest_t1037477780 * L_2 = ___request0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m2398979370(NULL /*static, unused*/, _stringLiteral2233467961, L_1, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		Logger_Debug_m2108157889(L_0, L_3, /*hidden argument*/NULL);
		WebSocketStream_t4103435597 * L_4 = __this->get__stream_30();
		HandshakeRequest_t1037477780 * L_5 = ___request0;
		NullCheck(L_4);
		WebSocketStream_WriteHandshake_m511451412(L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean WebSocketSharp.WebSocket::send(WebSocketSharp.HandshakeResponse)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2311109706;
extern const uint32_t WebSocket_send_m288551060_MetadataUsageId;
extern "C"  bool WebSocket_send_m288551060 (WebSocket_t1342580397 * __this, HandshakeResponse_t3229697822 * ___response0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_send_m288551060_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Logger_t3695440972 * L_0 = __this->get__logger_19();
		il2cpp_codegen_memory_barrier();
		HandshakeResponse_t3229697822 * L_1 = ___response0;
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String WebSocketSharp.HandshakeResponse::ToString() */, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2311109706, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		Logger_Debug_m2108157889(L_0, L_3, /*hidden argument*/NULL);
		WebSocketStream_t4103435597 * L_4 = __this->get__stream_30();
		HandshakeResponse_t3229697822 * L_5 = ___response0;
		NullCheck(L_4);
		bool L_6 = WebSocketStream_WriteHandshake_m511451412(L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean WebSocketSharp.WebSocket::send(WebSocketSharp.WebSocketFrame)
extern Il2CppCodeGenString* _stringLiteral3867820340;
extern const uint32_t WebSocket_send_m2219182912_MetadataUsageId;
extern "C"  bool WebSocket_send_m2219182912 (WebSocket_t1342580397 * __this, WebSocketFrame_t778194306 * ___frame0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_send_m2219182912_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = __this->get__forConn_14();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			uint16_t L_2 = __this->get__readyState_27();
			il2cpp_codegen_memory_barrier();
			if ((((int32_t)L_2) == ((int32_t)1)))
			{
				goto IL_0034;
			}
		}

IL_001b:
		{
			Logger_t3695440972 * L_3 = __this->get__logger_19();
			il2cpp_codegen_memory_barrier();
			NullCheck(L_3);
			Logger_Warn_m1570757416(L_3, _stringLiteral3867820340, /*hidden argument*/NULL);
			V_1 = (bool)0;
			IL2CPP_LEAVE(0x57, FINALLY_0050);
		}

IL_0034:
		{
			WebSocketStream_t4103435597 * L_4 = __this->get__stream_30();
			WebSocketFrame_t778194306 * L_5 = ___frame0;
			NullCheck(L_5);
			ByteU5BU5D_t4260760469* L_6 = WebSocketFrame_ToByteArray_m3468314104(L_5, /*hidden argument*/NULL);
			NullCheck(L_4);
			bool L_7 = WebSocketStream_Write_m2408900417(L_4, L_6, /*hidden argument*/NULL);
			V_1 = L_7;
			IL2CPP_LEAVE(0x57, FINALLY_0050);
		}

IL_004b:
		{
			; // IL_004b: leave IL_0057
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0050;
	}

FINALLY_0050:
	{ // begin finally (depth: 1)
		Il2CppObject * L_8 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(80)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(80)
	{
		IL2CPP_JUMP_TBL(0x57, IL_0057)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0057:
	{
		bool L_9 = V_1;
		return L_9;
	}
}
// System.Boolean WebSocketSharp.WebSocket::send(WebSocketSharp.Opcode,System.Byte[])
extern Il2CppClass* WebSocketFrame_t778194306_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4246579071;
extern const uint32_t WebSocket_send_m2882270907_MetadataUsageId;
extern "C"  bool WebSocket_send_m2882270907 (WebSocket_t1342580397 * __this, uint8_t ___opcode0, ByteU5BU5D_t4260760469* ___data1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_send_m2882270907_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	uint8_t V_3 = 0;
	Exception_t3991598821 * V_4 = NULL;
	bool V_5 = false;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = __this->get__forSend_17();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			V_1 = (bool)0;
		}

IL_000f:
		try
		{ // begin try (depth: 2)
			{
				V_2 = (bool)0;
				uint8_t L_2 = __this->get__compression_8();
				if (!L_2)
				{
					goto IL_002c;
				}
			}

IL_001c:
			{
				ByteU5BU5D_t4260760469* L_3 = ___data1;
				uint8_t L_4 = __this->get__compression_8();
				ByteU5BU5D_t4260760469* L_5 = Ext_Compress_m2695677868(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
				___data1 = L_5;
				V_2 = (bool)1;
			}

IL_002c:
			{
				bool L_6 = __this->get__client_6();
				if (!L_6)
				{
					goto IL_003d;
				}
			}

IL_0037:
			{
				G_B7_0 = 1;
				goto IL_003e;
			}

IL_003d:
			{
				G_B7_0 = 0;
			}

IL_003e:
			{
				V_3 = G_B7_0;
				uint8_t L_7 = ___opcode0;
				uint8_t L_8 = V_3;
				ByteU5BU5D_t4260760469* L_9 = ___data1;
				bool L_10 = V_2;
				IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t778194306_il2cpp_TypeInfo_var);
				WebSocketFrame_t778194306 * L_11 = WebSocketFrame_CreateFrame_m309663431(NULL /*static, unused*/, 1, L_7, L_8, L_9, L_10, /*hidden argument*/NULL);
				bool L_12 = WebSocket_send_m2219182912(__this, L_11, /*hidden argument*/NULL);
				V_1 = L_12;
				goto IL_007b;
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t3991598821 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0055;
			throw e;
		}

CATCH_0055:
		{ // begin catch(System.Exception)
			V_4 = ((Exception_t3991598821 *)__exception_local);
			Logger_t3695440972 * L_13 = __this->get__logger_19();
			il2cpp_codegen_memory_barrier();
			Exception_t3991598821 * L_14 = V_4;
			NullCheck(L_14);
			String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Exception::ToString() */, L_14);
			NullCheck(L_13);
			Logger_Fatal_m51902704(L_13, L_15, /*hidden argument*/NULL);
			WebSocket_error_m149167037(__this, _stringLiteral4246579071, /*hidden argument*/NULL);
			goto IL_007b;
		} // end catch (depth: 2)

IL_007b:
		{
			bool L_16 = V_1;
			V_5 = L_16;
			IL2CPP_LEAVE(0x8F, FINALLY_0088);
		}

IL_0083:
		{
			; // IL_0083: leave IL_008f
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0088;
	}

FINALLY_0088:
	{ // begin finally (depth: 1)
		Il2CppObject * L_17 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(136)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(136)
	{
		IL2CPP_JUMP_TBL(0x8F, IL_008f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_008f:
	{
		bool L_18 = V_5;
		return L_18;
	}
}
// System.Boolean WebSocketSharp.WebSocket::send(WebSocketSharp.Opcode,System.IO.Stream)
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4246579071;
extern const uint32_t WebSocket_send_m2473628261_MetadataUsageId;
extern "C"  bool WebSocket_send_m2473628261 (WebSocket_t1342580397 * __this, uint8_t ___opcode0, Stream_t1561764144 * ___stream1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_send_m2473628261_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	Stream_t1561764144 * V_2 = NULL;
	bool V_3 = false;
	uint8_t V_4 = 0;
	Exception_t3991598821 * V_5 = NULL;
	bool V_6 = false;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = __this->get__forSend_17();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			V_1 = (bool)0;
			Stream_t1561764144 * L_2 = ___stream1;
			V_2 = L_2;
			V_3 = (bool)0;
		}

IL_0013:
		try
		{ // begin try (depth: 2)
			try
			{ // begin try (depth: 3)
				{
					uint8_t L_3 = __this->get__compression_8();
					if (!L_3)
					{
						goto IL_002e;
					}
				}

IL_001e:
				{
					Stream_t1561764144 * L_4 = ___stream1;
					uint8_t L_5 = __this->get__compression_8();
					Stream_t1561764144 * L_6 = Ext_Compress_m4045017470(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
					___stream1 = L_6;
					V_3 = (bool)1;
				}

IL_002e:
				{
					bool L_7 = __this->get__client_6();
					if (!L_7)
					{
						goto IL_003f;
					}
				}

IL_0039:
				{
					G_B7_0 = 1;
					goto IL_0040;
				}

IL_003f:
				{
					G_B7_0 = 0;
				}

IL_0040:
				{
					V_4 = G_B7_0;
					uint8_t L_8 = ___opcode0;
					Stream_t1561764144 * L_9 = ___stream1;
					uint8_t L_10 = V_4;
					bool L_11 = V_3;
					bool L_12 = WebSocket_sendFragmented_m3902794260(__this, L_8, L_9, L_10, L_11, /*hidden argument*/NULL);
					V_1 = L_12;
					IL2CPP_LEAVE(0x8C, FINALLY_0079);
				}
			} // end try (depth: 3)
			catch(Il2CppExceptionWrapper& e)
			{
				__exception_local = (Exception_t3991598821 *)e.ex;
				if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
					goto CATCH_0053;
				throw e;
			}

CATCH_0053:
			{ // begin catch(System.Exception)
				V_5 = ((Exception_t3991598821 *)__exception_local);
				Logger_t3695440972 * L_13 = __this->get__logger_19();
				il2cpp_codegen_memory_barrier();
				Exception_t3991598821 * L_14 = V_5;
				NullCheck(L_14);
				String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Exception::ToString() */, L_14);
				NullCheck(L_13);
				Logger_Fatal_m51902704(L_13, L_15, /*hidden argument*/NULL);
				WebSocket_error_m149167037(__this, _stringLiteral4246579071, /*hidden argument*/NULL);
				IL2CPP_LEAVE(0x8C, FINALLY_0079);
			} // end catch (depth: 3)
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
			goto FINALLY_0079;
		}

FINALLY_0079:
		{ // begin finally (depth: 2)
			{
				bool L_16 = V_3;
				if (!L_16)
				{
					goto IL_0085;
				}
			}

IL_007f:
			{
				Stream_t1561764144 * L_17 = ___stream1;
				NullCheck(L_17);
				Stream_Dispose_m2904306374(L_17, /*hidden argument*/NULL);
			}

IL_0085:
			{
				Stream_t1561764144 * L_18 = V_2;
				NullCheck(L_18);
				Stream_Dispose_m2904306374(L_18, /*hidden argument*/NULL);
				IL2CPP_END_FINALLY(121)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(121)
		{
			IL2CPP_JUMP_TBL(0x8C, IL_008c)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
		}

IL_008c:
		{
			bool L_19 = V_1;
			V_6 = L_19;
			IL2CPP_LEAVE(0xA0, FINALLY_0099);
		}

IL_0094:
		{
			; // IL_0094: leave IL_00a0
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0099;
	}

FINALLY_0099:
	{ // begin finally (depth: 1)
		Il2CppObject * L_20 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(153)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(153)
	{
		IL2CPP_JUMP_TBL(0xA0, IL_00a0)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_00a0:
	{
		bool L_21 = V_6;
		return L_21;
	}
}
// System.Void WebSocketSharp.WebSocket::sendAsync(WebSocketSharp.Opcode,System.Byte[],System.Action`1<System.Boolean>)
extern Il2CppClass* U3CsendAsyncU3Ec__AnonStorey29_t861700891_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t2548063815_il2cpp_TypeInfo_var;
extern Il2CppClass* AsyncCallback_t1369114871_il2cpp_TypeInfo_var;
extern const MethodInfo* WebSocket_send_m2882270907_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m3214153488_MethodInfo_var;
extern const MethodInfo* U3CsendAsyncU3Ec__AnonStorey29_U3CU3Em__F_m2428161332_MethodInfo_var;
extern const MethodInfo* Func_3_BeginInvoke_m2113884034_MethodInfo_var;
extern const uint32_t WebSocket_sendAsync_m2588890200_MetadataUsageId;
extern "C"  void WebSocket_sendAsync_m2588890200 (WebSocket_t1342580397 * __this, uint8_t ___opcode0, ByteU5BU5D_t4260760469* ___data1, Action_1_t872614854 * ___completed2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_sendAsync_m2588890200_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CsendAsyncU3Ec__AnonStorey29_t861700891 * V_0 = NULL;
	{
		U3CsendAsyncU3Ec__AnonStorey29_t861700891 * L_0 = (U3CsendAsyncU3Ec__AnonStorey29_t861700891 *)il2cpp_codegen_object_new(U3CsendAsyncU3Ec__AnonStorey29_t861700891_il2cpp_TypeInfo_var);
		U3CsendAsyncU3Ec__AnonStorey29__ctor_m2579381936(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CsendAsyncU3Ec__AnonStorey29_t861700891 * L_1 = V_0;
		Action_1_t872614854 * L_2 = ___completed2;
		NullCheck(L_1);
		L_1->set_completed_1(L_2);
		U3CsendAsyncU3Ec__AnonStorey29_t861700891 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_2(__this);
		U3CsendAsyncU3Ec__AnonStorey29_t861700891 * L_4 = V_0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)WebSocket_send_m2882270907_MethodInfo_var);
		Func_3_t2548063815 * L_6 = (Func_3_t2548063815 *)il2cpp_codegen_object_new(Func_3_t2548063815_il2cpp_TypeInfo_var);
		Func_3__ctor_m3214153488(L_6, __this, L_5, /*hidden argument*/Func_3__ctor_m3214153488_MethodInfo_var);
		NullCheck(L_4);
		L_4->set_sender_0(L_6);
		U3CsendAsyncU3Ec__AnonStorey29_t861700891 * L_7 = V_0;
		NullCheck(L_7);
		Func_3_t2548063815 * L_8 = L_7->get_sender_0();
		uint8_t L_9 = ___opcode0;
		ByteU5BU5D_t4260760469* L_10 = ___data1;
		U3CsendAsyncU3Ec__AnonStorey29_t861700891 * L_11 = V_0;
		IntPtr_t L_12;
		L_12.set_m_value_0((void*)(void*)U3CsendAsyncU3Ec__AnonStorey29_U3CU3Em__F_m2428161332_MethodInfo_var);
		AsyncCallback_t1369114871 * L_13 = (AsyncCallback_t1369114871 *)il2cpp_codegen_object_new(AsyncCallback_t1369114871_il2cpp_TypeInfo_var);
		AsyncCallback__ctor_m1986959762(L_13, L_11, L_12, /*hidden argument*/NULL);
		NullCheck(L_8);
		Func_3_BeginInvoke_m2113884034(L_8, L_9, L_10, L_13, NULL, /*hidden argument*/Func_3_BeginInvoke_m2113884034_MethodInfo_var);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::sendAsync(WebSocketSharp.Opcode,System.IO.Stream,System.Action`1<System.Boolean>)
extern Il2CppClass* U3CsendAsyncU3Ec__AnonStorey2A_t861700899_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_3_t2764730336_il2cpp_TypeInfo_var;
extern Il2CppClass* AsyncCallback_t1369114871_il2cpp_TypeInfo_var;
extern const MethodInfo* WebSocket_send_m2473628261_MethodInfo_var;
extern const MethodInfo* Func_3__ctor_m979856946_MethodInfo_var;
extern const MethodInfo* U3CsendAsyncU3Ec__AnonStorey2A_U3CU3Em__10_m112891155_MethodInfo_var;
extern const MethodInfo* Func_3_BeginInvoke_m249269144_MethodInfo_var;
extern const uint32_t WebSocket_sendAsync_m1251110818_MetadataUsageId;
extern "C"  void WebSocket_sendAsync_m1251110818 (WebSocket_t1342580397 * __this, uint8_t ___opcode0, Stream_t1561764144 * ___stream1, Action_1_t872614854 * ___completed2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_sendAsync_m1251110818_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CsendAsyncU3Ec__AnonStorey2A_t861700899 * V_0 = NULL;
	{
		U3CsendAsyncU3Ec__AnonStorey2A_t861700899 * L_0 = (U3CsendAsyncU3Ec__AnonStorey2A_t861700899 *)il2cpp_codegen_object_new(U3CsendAsyncU3Ec__AnonStorey2A_t861700899_il2cpp_TypeInfo_var);
		U3CsendAsyncU3Ec__AnonStorey2A__ctor_m1007273896(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CsendAsyncU3Ec__AnonStorey2A_t861700899 * L_1 = V_0;
		Action_1_t872614854 * L_2 = ___completed2;
		NullCheck(L_1);
		L_1->set_completed_1(L_2);
		U3CsendAsyncU3Ec__AnonStorey2A_t861700899 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_2(__this);
		U3CsendAsyncU3Ec__AnonStorey2A_t861700899 * L_4 = V_0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)WebSocket_send_m2473628261_MethodInfo_var);
		Func_3_t2764730336 * L_6 = (Func_3_t2764730336 *)il2cpp_codegen_object_new(Func_3_t2764730336_il2cpp_TypeInfo_var);
		Func_3__ctor_m979856946(L_6, __this, L_5, /*hidden argument*/Func_3__ctor_m979856946_MethodInfo_var);
		NullCheck(L_4);
		L_4->set_sender_0(L_6);
		U3CsendAsyncU3Ec__AnonStorey2A_t861700899 * L_7 = V_0;
		NullCheck(L_7);
		Func_3_t2764730336 * L_8 = L_7->get_sender_0();
		uint8_t L_9 = ___opcode0;
		Stream_t1561764144 * L_10 = ___stream1;
		U3CsendAsyncU3Ec__AnonStorey2A_t861700899 * L_11 = V_0;
		IntPtr_t L_12;
		L_12.set_m_value_0((void*)(void*)U3CsendAsyncU3Ec__AnonStorey2A_U3CU3Em__10_m112891155_MethodInfo_var);
		AsyncCallback_t1369114871 * L_13 = (AsyncCallback_t1369114871 *)il2cpp_codegen_object_new(AsyncCallback_t1369114871_il2cpp_TypeInfo_var);
		AsyncCallback__ctor_m1986959762(L_13, L_11, L_12, /*hidden argument*/NULL);
		NullCheck(L_8);
		Func_3_BeginInvoke_m249269144(L_8, L_9, L_10, L_13, NULL, /*hidden argument*/Func_3_BeginInvoke_m249269144_MethodInfo_var);
		return;
	}
}
// System.Boolean WebSocketSharp.WebSocket::sendFragmented(WebSocketSharp.Opcode,System.IO.Stream,WebSocketSharp.Mask,System.Boolean)
extern Il2CppClass* ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocketFrame_t778194306_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_sendFragmented_m3902794260_MetadataUsageId;
extern "C"  bool WebSocket_sendFragmented_m3902794260 (WebSocket_t1342580397 * __this, uint8_t ___opcode0, Stream_t1561764144 * ___stream1, uint8_t ___mask2, bool ___compressed3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_sendFragmented_m3902794260_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int64_t V_0 = 0;
	int64_t V_1 = 0;
	int32_t V_2 = 0;
	int64_t V_3 = 0;
	ByteU5BU5D_t4260760469* V_4 = NULL;
	int64_t V_5 = 0;
	int32_t V_6 = 0;
	int64_t G_B3_0 = 0;
	int32_t G_B7_0 = 0;
	int32_t G_B22_0 = 0;
	{
		Stream_t1561764144 * L_0 = ___stream1;
		NullCheck(L_0);
		int64_t L_1 = VirtFuncInvoker0< int64_t >::Invoke(8 /* System.Int64 System.IO.Stream::get_Length() */, L_0);
		V_0 = L_1;
		int64_t L_2 = V_0;
		V_1 = ((int64_t)((int64_t)L_2/(int64_t)(((int64_t)((int64_t)((int32_t)1016))))));
		int64_t L_3 = V_0;
		V_2 = (((int32_t)((int32_t)((int64_t)((int64_t)L_3%(int64_t)(((int64_t)((int64_t)((int32_t)1016)))))))));
		int32_t L_4 = V_2;
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		int64_t L_5 = V_1;
		G_B3_0 = ((int64_t)((int64_t)L_5-(int64_t)(((int64_t)((int64_t)2)))));
		goto IL_002d;
	}

IL_0029:
	{
		int64_t L_6 = V_1;
		G_B3_0 = ((int64_t)((int64_t)L_6-(int64_t)(((int64_t)((int64_t)1)))));
	}

IL_002d:
	{
		V_3 = G_B3_0;
		V_4 = (ByteU5BU5D_t4260760469*)NULL;
		int64_t L_7 = V_1;
		if (L_7)
		{
			goto IL_0065;
		}
	}
	{
		int32_t L_8 = V_2;
		V_4 = ((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)L_8));
		Stream_t1561764144 * L_9 = ___stream1;
		ByteU5BU5D_t4260760469* L_10 = V_4;
		int32_t L_11 = V_2;
		NullCheck(L_9);
		int32_t L_12 = VirtFuncInvoker3< int32_t, ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(14 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_9, L_10, 0, L_11);
		int32_t L_13 = V_2;
		if ((!(((uint32_t)L_12) == ((uint32_t)L_13))))
		{
			goto IL_0063;
		}
	}
	{
		uint8_t L_14 = ___opcode0;
		uint8_t L_15 = ___mask2;
		ByteU5BU5D_t4260760469* L_16 = V_4;
		bool L_17 = ___compressed3;
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t778194306_il2cpp_TypeInfo_var);
		WebSocketFrame_t778194306 * L_18 = WebSocketFrame_CreateFrame_m309663431(NULL /*static, unused*/, 1, L_14, L_15, L_16, L_17, /*hidden argument*/NULL);
		bool L_19 = WebSocket_send_m2219182912(__this, L_18, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_19));
		goto IL_0064;
	}

IL_0063:
	{
		G_B7_0 = 0;
	}

IL_0064:
	{
		return (bool)G_B7_0;
	}

IL_0065:
	{
		V_4 = ((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)((int32_t)1016)));
		Stream_t1561764144 * L_20 = ___stream1;
		ByteU5BU5D_t4260760469* L_21 = V_4;
		NullCheck(L_20);
		int32_t L_22 = VirtFuncInvoker3< int32_t, ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(14 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_20, L_21, 0, ((int32_t)1016));
		if ((!(((uint32_t)L_22) == ((uint32_t)((int32_t)1016)))))
		{
			goto IL_00a0;
		}
	}
	{
		uint8_t L_23 = ___opcode0;
		uint8_t L_24 = ___mask2;
		ByteU5BU5D_t4260760469* L_25 = V_4;
		bool L_26 = ___compressed3;
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t778194306_il2cpp_TypeInfo_var);
		WebSocketFrame_t778194306 * L_27 = WebSocketFrame_CreateFrame_m309663431(NULL /*static, unused*/, 0, L_23, L_24, L_25, L_26, /*hidden argument*/NULL);
		bool L_28 = WebSocket_send_m2219182912(__this, L_27, /*hidden argument*/NULL);
		if (L_28)
		{
			goto IL_00a2;
		}
	}

IL_00a0:
	{
		return (bool)0;
	}

IL_00a2:
	{
		V_5 = (((int64_t)((int64_t)0)));
		goto IL_00e3;
	}

IL_00ab:
	{
		Stream_t1561764144 * L_29 = ___stream1;
		ByteU5BU5D_t4260760469* L_30 = V_4;
		NullCheck(L_29);
		int32_t L_31 = VirtFuncInvoker3< int32_t, ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(14 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_29, L_30, 0, ((int32_t)1016));
		if ((!(((uint32_t)L_31) == ((uint32_t)((int32_t)1016)))))
		{
			goto IL_00da;
		}
	}
	{
		uint8_t L_32 = ___mask2;
		ByteU5BU5D_t4260760469* L_33 = V_4;
		bool L_34 = ___compressed3;
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t778194306_il2cpp_TypeInfo_var);
		WebSocketFrame_t778194306 * L_35 = WebSocketFrame_CreateFrame_m309663431(NULL /*static, unused*/, 0, 0, L_32, L_33, L_34, /*hidden argument*/NULL);
		bool L_36 = WebSocket_send_m2219182912(__this, L_35, /*hidden argument*/NULL);
		if (L_36)
		{
			goto IL_00dc;
		}
	}

IL_00da:
	{
		return (bool)0;
	}

IL_00dc:
	{
		int64_t L_37 = V_5;
		V_5 = ((int64_t)((int64_t)L_37+(int64_t)(((int64_t)((int64_t)1)))));
	}

IL_00e3:
	{
		int64_t L_38 = V_5;
		int64_t L_39 = V_3;
		if ((((int64_t)L_38) < ((int64_t)L_39)))
		{
			goto IL_00ab;
		}
	}
	{
		V_6 = ((int32_t)1016);
		int32_t L_40 = V_2;
		if (!L_40)
		{
			goto IL_0103;
		}
	}
	{
		int32_t L_41 = V_2;
		int32_t L_42 = L_41;
		V_6 = L_42;
		V_4 = ((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)L_42));
	}

IL_0103:
	{
		Stream_t1561764144 * L_43 = ___stream1;
		ByteU5BU5D_t4260760469* L_44 = V_4;
		int32_t L_45 = V_6;
		NullCheck(L_43);
		int32_t L_46 = VirtFuncInvoker3< int32_t, ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(14 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_43, L_44, 0, L_45);
		int32_t L_47 = V_6;
		if ((!(((uint32_t)L_46) == ((uint32_t)L_47))))
		{
			goto IL_0129;
		}
	}
	{
		uint8_t L_48 = ___mask2;
		ByteU5BU5D_t4260760469* L_49 = V_4;
		bool L_50 = ___compressed3;
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t778194306_il2cpp_TypeInfo_var);
		WebSocketFrame_t778194306 * L_51 = WebSocketFrame_CreateFrame_m309663431(NULL /*static, unused*/, 1, 0, L_48, L_49, L_50, /*hidden argument*/NULL);
		bool L_52 = WebSocket_send_m2219182912(__this, L_51, /*hidden argument*/NULL);
		G_B22_0 = ((int32_t)(L_52));
		goto IL_012a;
	}

IL_0129:
	{
		G_B22_0 = 0;
	}

IL_012a:
	{
		return (bool)G_B22_0;
	}
}
// WebSocketSharp.HandshakeResponse WebSocketSharp.WebSocket::sendHandshakeRequest()
extern Il2CppClass* AuthenticationResponse_t2112712571_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1217813246;
extern Il2CppCodeGenString* _stringLiteral94756344;
extern Il2CppCodeGenString* _stringLiteral3708358745;
extern const uint32_t WebSocket_sendHandshakeRequest_m556619861_MetadataUsageId;
extern "C"  HandshakeResponse_t3229697822 * WebSocket_sendHandshakeRequest_m556619861 (WebSocket_t1342580397 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_sendHandshakeRequest_m556619861_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	HandshakeRequest_t1037477780 * V_0 = NULL;
	HandshakeResponse_t3229697822 * V_1 = NULL;
	AuthenticationResponse_t2112712571 * V_2 = NULL;
	{
		HandshakeRequest_t1037477780 * L_0 = WebSocket_createHandshakeRequest_m2249726057(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		HandshakeRequest_t1037477780 * L_1 = V_0;
		HandshakeResponse_t3229697822 * L_2 = WebSocket_sendHandshakeRequest_m4214597114(__this, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		HandshakeResponse_t3229697822 * L_3 = V_1;
		NullCheck(L_3);
		bool L_4 = HandshakeResponse_get_IsUnauthorized_m1179508529(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_00b5;
		}
	}
	{
		HandshakeResponse_t3229697822 * L_5 = V_1;
		NullCheck(L_5);
		AuthenticationChallenge_t1782907061 * L_6 = HandshakeResponse_get_AuthChallenge_m1867389680(L_5, /*hidden argument*/NULL);
		__this->set__authChallenge_3(L_6);
		NetworkCredential_t1204099087 * L_7 = __this->get__credentials_11();
		if (!L_7)
		{
			goto IL_00b5;
		}
	}
	{
		bool L_8 = __this->get__preAuth_24();
		if (!L_8)
		{
			goto IL_004d;
		}
	}
	{
		AuthenticationChallenge_t1782907061 * L_9 = __this->get__authChallenge_3();
		NullCheck(L_9);
		int32_t L_10 = AuthenticationBase_get_Scheme_m3444393929(L_9, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_10) == ((uint32_t)1))))
		{
			goto IL_00b5;
		}
	}

IL_004d:
	{
		HandshakeResponse_t3229697822 * L_11 = V_1;
		NullCheck(L_11);
		NameValueCollection_t2791941106 * L_12 = HandshakeBase_get_Headers_m3632488899(L_11, /*hidden argument*/NULL);
		bool L_13 = Ext_Contains_m1419354755(NULL /*static, unused*/, L_12, _stringLiteral1217813246, _stringLiteral94756344, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0073;
		}
	}
	{
		WebSocket_closeClientResources_m4025636903(__this, /*hidden argument*/NULL);
		WebSocket_setClientStream_m1110293962(__this, /*hidden argument*/NULL);
	}

IL_0073:
	{
		AuthenticationChallenge_t1782907061 * L_14 = __this->get__authChallenge_3();
		NetworkCredential_t1204099087 * L_15 = __this->get__credentials_11();
		uint32_t L_16 = __this->get__nonceCount_21();
		AuthenticationResponse_t2112712571 * L_17 = (AuthenticationResponse_t2112712571 *)il2cpp_codegen_object_new(AuthenticationResponse_t2112712571_il2cpp_TypeInfo_var);
		AuthenticationResponse__ctor_m91930666(L_17, L_14, L_15, L_16, /*hidden argument*/NULL);
		V_2 = L_17;
		AuthenticationResponse_t2112712571 * L_18 = V_2;
		NullCheck(L_18);
		uint32_t L_19 = AuthenticationResponse_get_NonceCount_m622951388(L_18, /*hidden argument*/NULL);
		__this->set__nonceCount_21(L_19);
		HandshakeRequest_t1037477780 * L_20 = V_0;
		NullCheck(L_20);
		NameValueCollection_t2791941106 * L_21 = HandshakeBase_get_Headers_m3632488899(L_20, /*hidden argument*/NULL);
		AuthenticationResponse_t2112712571 * L_22 = V_2;
		NullCheck(L_22);
		String_t* L_23 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String WebSocketSharp.Net.AuthenticationBase::ToString() */, L_22);
		NullCheck(L_21);
		NameValueCollection_set_Item_m2275159743(L_21, _stringLiteral3708358745, L_23, /*hidden argument*/NULL);
		HandshakeRequest_t1037477780 * L_24 = V_0;
		HandshakeResponse_t3229697822 * L_25 = WebSocket_sendHandshakeRequest_m4214597114(__this, L_24, /*hidden argument*/NULL);
		V_1 = L_25;
	}

IL_00b5:
	{
		HandshakeResponse_t3229697822 * L_26 = V_1;
		return L_26;
	}
}
// WebSocketSharp.HandshakeResponse WebSocketSharp.WebSocket::sendHandshakeRequest(WebSocketSharp.HandshakeRequest)
extern "C"  HandshakeResponse_t3229697822 * WebSocket_sendHandshakeRequest_m4214597114 (WebSocket_t1342580397 * __this, HandshakeRequest_t1037477780 * ___request0, const MethodInfo* method)
{
	{
		HandshakeRequest_t1037477780 * L_0 = ___request0;
		WebSocket_send_m346101410(__this, L_0, /*hidden argument*/NULL);
		HandshakeResponse_t3229697822 * L_1 = WebSocket_receiveHandshakeResponse_m1807057802(__this, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void WebSocketSharp.WebSocket::setClientStream()
extern Il2CppClass* TcpClient_t838416830_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_setClientStream_m1110293962_MetadataUsageId;
extern "C"  void WebSocket_setClientStream_m1110293962 (WebSocket_t1342580397 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_setClientStream_m1110293962_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		Uri_t1116831938 * L_0 = __this->get__uri_32();
		NullCheck(L_0);
		String_t* L_1 = Uri_get_DnsSafeHost_m905428447(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Uri_t1116831938 * L_2 = __this->get__uri_32();
		NullCheck(L_2);
		int32_t L_3 = Uri_get_Port_m2253782543(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		String_t* L_4 = V_0;
		int32_t L_5 = V_1;
		TcpClient_t838416830 * L_6 = (TcpClient_t838416830 *)il2cpp_codegen_object_new(TcpClient_t838416830_il2cpp_TypeInfo_var);
		TcpClient__ctor_m547039555(L_6, L_4, L_5, /*hidden argument*/NULL);
		__this->set__tcpClient_31(L_6);
		TcpClient_t838416830 * L_7 = __this->get__tcpClient_31();
		bool L_8 = __this->get__secure_29();
		String_t* L_9 = V_0;
		RemoteCertificateValidationCallback_t1894914657 * L_10 = __this->get__certValidationCallback_5();
		WebSocketStream_t4103435597 * L_11 = WebSocketStream_CreateClientStream_m3072197941(NULL /*static, unused*/, L_7, L_8, L_9, L_10, /*hidden argument*/NULL);
		__this->set__stream_30(L_11);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::startReceiving()
extern Il2CppClass* U3CstartReceivingU3Ec__AnonStorey2B_t802946016_il2cpp_TypeInfo_var;
extern Il2CppClass* AutoResetEvent_t874642578_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern const MethodInfo* Queue_1_get_Count_m4166990737_MethodInfo_var;
extern const MethodInfo* Queue_1_Clear_m2586898730_MethodInfo_var;
extern const MethodInfo* U3CstartReceivingU3Ec__AnonStorey2B_U3CU3Em__11_m792573156_MethodInfo_var;
extern const uint32_t WebSocket_startReceiving_m3343765315_MetadataUsageId;
extern "C"  void WebSocket_startReceiving_m3343765315 (WebSocket_t1342580397 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_startReceiving_m3343765315_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CstartReceivingU3Ec__AnonStorey2B_t802946016 * V_0 = NULL;
	{
		U3CstartReceivingU3Ec__AnonStorey2B_t802946016 * L_0 = (U3CstartReceivingU3Ec__AnonStorey2B_t802946016 *)il2cpp_codegen_object_new(U3CstartReceivingU3Ec__AnonStorey2B_t802946016_il2cpp_TypeInfo_var);
		U3CstartReceivingU3Ec__AnonStorey2B__ctor_m2023001787(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CstartReceivingU3Ec__AnonStorey2B_t802946016 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_1(__this);
		Queue_1_t2273188169 * L_2 = __this->get__messageEventQueue_20();
		NullCheck(L_2);
		int32_t L_3 = Queue_1_get_Count_m4166990737(L_2, /*hidden argument*/Queue_1_get_Count_m4166990737_MethodInfo_var);
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_0029;
		}
	}
	{
		Queue_1_t2273188169 * L_4 = __this->get__messageEventQueue_20();
		NullCheck(L_4);
		Queue_1_Clear_m2586898730(L_4, /*hidden argument*/Queue_1_Clear_m2586898730_MethodInfo_var);
	}

IL_0029:
	{
		AutoResetEvent_t874642578 * L_5 = (AutoResetEvent_t874642578 *)il2cpp_codegen_object_new(AutoResetEvent_t874642578_il2cpp_TypeInfo_var);
		AutoResetEvent__ctor_m279516(L_5, (bool)0, /*hidden argument*/NULL);
		__this->set__exitReceiving_13(L_5);
		AutoResetEvent_t874642578 * L_6 = (AutoResetEvent_t874642578 *)il2cpp_codegen_object_new(AutoResetEvent_t874642578_il2cpp_TypeInfo_var);
		AutoResetEvent__ctor_m279516(L_6, (bool)0, /*hidden argument*/NULL);
		__this->set__receivePong_28(L_6);
		U3CstartReceivingU3Ec__AnonStorey2B_t802946016 * L_7 = V_0;
		NullCheck(L_7);
		L_7->set_receive_0((Action_t3771233898 *)NULL);
		U3CstartReceivingU3Ec__AnonStorey2B_t802946016 * L_8 = V_0;
		U3CstartReceivingU3Ec__AnonStorey2B_t802946016 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)U3CstartReceivingU3Ec__AnonStorey2B_U3CU3Em__11_m792573156_MethodInfo_var);
		Action_t3771233898 * L_11 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_11, L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		L_8->set_receive_0(L_11);
		U3CstartReceivingU3Ec__AnonStorey2B_t802946016 * L_12 = V_0;
		NullCheck(L_12);
		Action_t3771233898 * L_13 = L_12->get_receive_0();
		NullCheck(L_13);
		Action_Invoke_m1445970038(L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean WebSocketSharp.WebSocket::validateSecWebSocketAcceptHeader(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_validateSecWebSocketAcceptHeader_m2593449224_MetadataUsageId;
extern "C"  bool WebSocket_validateSecWebSocketAcceptHeader_m2593449224 (WebSocket_t1342580397 * __this, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_validateSecWebSocketAcceptHeader_m2593449224_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = ___value0;
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		String_t* L_1 = ___value0;
		String_t* L_2 = __this->get__base64Key_4();
		String_t* L_3 = WebSocket_CreateResponseKey_m666974534(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_4));
		goto IL_001a;
	}

IL_0019:
	{
		G_B3_0 = 0;
	}

IL_001a:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean WebSocketSharp.WebSocket::validateSecWebSocketExtensionsHeader(System.String)
extern Il2CppClass* CharU5BU5D_t3324145743_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t3730860138_il2cpp_TypeInfo_var;
extern const MethodInfo* WebSocket_U3CvalidateSecWebSocketExtensionsHeaderU3Em__12_m468975296_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3001890441_MethodInfo_var;
extern const MethodInfo* Ext_Contains_TisString_t_m802592955_MethodInfo_var;
extern const uint32_t WebSocket_validateSecWebSocketExtensionsHeader_m3169128316_MetadataUsageId;
extern "C"  bool WebSocket_validateSecWebSocketExtensionsHeader_m3169128316 (WebSocket_t1342580397 * __this, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_validateSecWebSocketExtensionsHeader_m3169128316_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Il2CppObject* V_1 = NULL;
	{
		uint8_t L_0 = __this->get__compression_8();
		V_0 = (bool)((((int32_t)((((int32_t)L_0) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		String_t* L_1 = ___value0;
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		String_t* L_2 = ___value0;
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m2979997331(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_002d;
		}
	}

IL_001e:
	{
		bool L_4 = V_0;
		if (!L_4)
		{
			goto IL_002b;
		}
	}
	{
		__this->set__compression_8(0);
	}

IL_002b:
	{
		return (bool)1;
	}

IL_002d:
	{
		bool L_5 = V_0;
		if (L_5)
		{
			goto IL_0035;
		}
	}
	{
		return (bool)0;
	}

IL_0035:
	{
		String_t* L_6 = ___value0;
		CharU5BU5D_t3324145743* L_7 = ((CharU5BU5D_t3324145743*)SZArrayNew(CharU5BU5D_t3324145743_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)44));
		Il2CppObject* L_8 = Ext_SplitHeaderValue_m1919670706(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		Il2CppObject* L_9 = V_1;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)WebSocket_U3CvalidateSecWebSocketExtensionsHeaderU3Em__12_m468975296_MethodInfo_var);
		Func_2_t3730860138 * L_11 = (Func_2_t3730860138 *)il2cpp_codegen_object_new(Func_2_t3730860138_il2cpp_TypeInfo_var);
		Func_2__ctor_m3001890441(L_11, __this, L_10, /*hidden argument*/Func_2__ctor_m3001890441_MethodInfo_var);
		bool L_12 = Ext_Contains_TisString_t_m802592955(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/Ext_Contains_TisString_t_m802592955_MethodInfo_var);
		if (!L_12)
		{
			goto IL_0060;
		}
	}
	{
		return (bool)0;
	}

IL_0060:
	{
		String_t* L_13 = ___value0;
		__this->set__extensions_12(L_13);
		return (bool)1;
	}
}
// System.Boolean WebSocketSharp.WebSocket::validateSecWebSocketKeyHeader(System.String)
extern "C"  bool WebSocket_validateSecWebSocketKeyHeader_m1076494873 (WebSocket_t1342580397 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		String_t* L_1 = ___value0;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m2979997331(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0013;
		}
	}

IL_0011:
	{
		return (bool)0;
	}

IL_0013:
	{
		String_t* L_3 = ___value0;
		__this->set__base64Key_4(L_3);
		return (bool)1;
	}
}
// System.Boolean WebSocketSharp.WebSocket::validateSecWebSocketProtocolHeader(System.String)
extern Il2CppClass* U3CvalidateSecWebSocketProtocolHeaderU3Ec__AnonStorey2C_t467005428_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t3730860138_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CvalidateSecWebSocketProtocolHeaderU3Ec__AnonStorey2C_U3CU3Em__13_m253440292_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3001890441_MethodInfo_var;
extern const MethodInfo* Ext_Contains_TisString_t_m802592955_MethodInfo_var;
extern const uint32_t WebSocket_validateSecWebSocketProtocolHeader_m3777734264_MetadataUsageId;
extern "C"  bool WebSocket_validateSecWebSocketProtocolHeader_m3777734264 (WebSocket_t1342580397 * __this, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_validateSecWebSocketProtocolHeader_m3777734264_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CvalidateSecWebSocketProtocolHeaderU3Ec__AnonStorey2C_t467005428 * V_0 = NULL;
	{
		U3CvalidateSecWebSocketProtocolHeaderU3Ec__AnonStorey2C_t467005428 * L_0 = (U3CvalidateSecWebSocketProtocolHeaderU3Ec__AnonStorey2C_t467005428 *)il2cpp_codegen_object_new(U3CvalidateSecWebSocketProtocolHeaderU3Ec__AnonStorey2C_t467005428_il2cpp_TypeInfo_var);
		U3CvalidateSecWebSocketProtocolHeaderU3Ec__AnonStorey2C__ctor_m2077357351(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CvalidateSecWebSocketProtocolHeaderU3Ec__AnonStorey2C_t467005428 * L_1 = V_0;
		String_t* L_2 = ___value0;
		NullCheck(L_1);
		L_1->set_value_0(L_2);
		U3CvalidateSecWebSocketProtocolHeaderU3Ec__AnonStorey2C_t467005428 * L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = L_3->get_value_0();
		if (L_4)
		{
			goto IL_0022;
		}
	}
	{
		StringU5BU5D_t4054002952* L_5 = __this->get__protocols_26();
		return (bool)((((Il2CppObject*)(StringU5BU5D_t4054002952*)L_5) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
	}

IL_0022:
	{
		StringU5BU5D_t4054002952* L_6 = __this->get__protocols_26();
		if (!L_6)
		{
			goto IL_0049;
		}
	}
	{
		StringU5BU5D_t4054002952* L_7 = __this->get__protocols_26();
		U3CvalidateSecWebSocketProtocolHeaderU3Ec__AnonStorey2C_t467005428 * L_8 = V_0;
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)U3CvalidateSecWebSocketProtocolHeaderU3Ec__AnonStorey2C_U3CU3Em__13_m253440292_MethodInfo_var);
		Func_2_t3730860138 * L_10 = (Func_2_t3730860138 *)il2cpp_codegen_object_new(Func_2_t3730860138_il2cpp_TypeInfo_var);
		Func_2__ctor_m3001890441(L_10, L_8, L_9, /*hidden argument*/Func_2__ctor_m3001890441_MethodInfo_var);
		bool L_11 = Ext_Contains_TisString_t_m802592955(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)L_7, L_10, /*hidden argument*/Ext_Contains_TisString_t_m802592955_MethodInfo_var);
		if (L_11)
		{
			goto IL_004b;
		}
	}

IL_0049:
	{
		return (bool)0;
	}

IL_004b:
	{
		U3CvalidateSecWebSocketProtocolHeaderU3Ec__AnonStorey2C_t467005428 * L_12 = V_0;
		NullCheck(L_12);
		String_t* L_13 = L_12->get_value_0();
		__this->set__protocol_25(L_13);
		return (bool)1;
	}
}
// System.Boolean WebSocketSharp.WebSocket::validateSecWebSocketVersionClientHeader(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1570;
extern const uint32_t WebSocket_validateSecWebSocketVersionClientHeader_m2132125493_MetadataUsageId;
extern "C"  bool WebSocket_validateSecWebSocketVersionClientHeader_m2132125493 (WebSocket_t1342580397 * __this, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_validateSecWebSocketVersionClientHeader_m2132125493_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = ___value0;
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		String_t* L_1 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_1, _stringLiteral1570, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_0014;
	}

IL_0013:
	{
		G_B3_0 = 0;
	}

IL_0014:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean WebSocketSharp.WebSocket::validateSecWebSocketVersionServerHeader(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1570;
extern const uint32_t WebSocket_validateSecWebSocketVersionServerHeader_m3224946365_MetadataUsageId;
extern "C"  bool WebSocket_validateSecWebSocketVersionServerHeader_m3224946365 (WebSocket_t1342580397 * __this, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_validateSecWebSocketVersionServerHeader_m3224946365_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = ___value0;
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		String_t* L_1 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_1, _stringLiteral1570, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_0014;
	}

IL_0013:
	{
		G_B3_0 = 1;
	}

IL_0014:
	{
		return (bool)G_B3_0;
	}
}
// System.Void WebSocketSharp.WebSocket::Close(WebSocketSharp.HandshakeResponse)
extern "C"  void WebSocket_Close_m320732264 (WebSocket_t1342580397 * __this, HandshakeResponse_t3229697822 * ___response0, const MethodInfo* method)
{
	{
		il2cpp_codegen_memory_barrier();
		__this->set__readyState_27(2);
		HandshakeResponse_t3229697822 * L_0 = ___response0;
		WebSocket_send_m288551060(__this, L_0, /*hidden argument*/NULL);
		WebSocket_closeServerResources_m2248524207(__this, /*hidden argument*/NULL);
		il2cpp_codegen_memory_barrier();
		__this->set__readyState_27(3);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::Close(WebSocketSharp.Net.HttpStatusCode)
extern "C"  void WebSocket_Close_m4199074618 (WebSocket_t1342580397 * __this, int32_t ___code0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___code0;
		HandshakeResponse_t3229697822 * L_1 = WebSocket_createHandshakeResponse_m1053144262(__this, L_0, /*hidden argument*/NULL);
		WebSocket_Close_m320732264(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::Close(WebSocketSharp.CloseEventArgs,System.Byte[],System.Int32)
extern Il2CppClass* Action_t3771233898_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* WebSocket_closeServerResources_m2248524207_MethodInfo_var;
extern const MethodInfo* Ext_Emit_TisCloseEventArgs_t2470319931_m1774194048_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3390759605;
extern Il2CppCodeGenString* _stringLiteral188376329;
extern const uint32_t WebSocket_Close_m1253619585_MetadataUsageId;
extern "C"  void WebSocket_Close_m1253619585 (WebSocket_t1342580397 * __this, CloseEventArgs_t2470319931 * ___e0, ByteU5BU5D_t4260760469* ___frame1, int32_t ___timeout2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_Close_m1253619585_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Exception_t3991598821 * V_1 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = __this->get__forConn_14();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			uint16_t L_2 = __this->get__readyState_27();
			il2cpp_codegen_memory_barrier();
			if ((((int32_t)L_2) == ((int32_t)2)))
			{
				goto IL_0029;
			}
		}

IL_001b:
		{
			uint16_t L_3 = __this->get__readyState_27();
			il2cpp_codegen_memory_barrier();
			if ((!(((uint32_t)L_3) == ((uint32_t)3))))
			{
				goto IL_0040;
			}
		}

IL_0029:
		{
			Logger_t3695440972 * L_4 = __this->get__logger_19();
			il2cpp_codegen_memory_barrier();
			NullCheck(L_4);
			Logger_Info_m861041664(L_4, _stringLiteral3390759605, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0xAD, FINALLY_004e);
		}

IL_0040:
		{
			il2cpp_codegen_memory_barrier();
			__this->set__readyState_27(2);
			IL2CPP_LEAVE(0x55, FINALLY_004e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_004e;
	}

FINALLY_004e:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(78)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(78)
	{
		IL2CPP_JUMP_TBL(0xAD, IL_00ad)
		IL2CPP_JUMP_TBL(0x55, IL_0055)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0055:
	{
		CloseEventArgs_t2470319931 * L_6 = ___e0;
		ByteU5BU5D_t4260760469* L_7 = ___frame1;
		int32_t L_8 = ___timeout2;
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)WebSocket_closeServerResources_m2248524207_MethodInfo_var);
		Action_t3771233898 * L_10 = (Action_t3771233898 *)il2cpp_codegen_object_new(Action_t3771233898_il2cpp_TypeInfo_var);
		Action__ctor_m2957240604(L_10, __this, L_9, /*hidden argument*/NULL);
		bool L_11 = WebSocket_closeHandshake_m1631040221(__this, L_7, L_8, L_10, /*hidden argument*/NULL);
		NullCheck(L_6);
		CloseEventArgs_set_WasClean_m3328282967(L_6, L_11, /*hidden argument*/NULL);
		il2cpp_codegen_memory_barrier();
		__this->set__readyState_27(3);
	}

IL_0078:
	try
	{ // begin try (depth: 1)
		EventHandler_1_t2615270477 * L_12 = __this->get_OnClose_33();
		CloseEventArgs_t2470319931 * L_13 = ___e0;
		Ext_Emit_TisCloseEventArgs_t2470319931_m1774194048(NULL /*static, unused*/, L_12, __this, L_13, /*hidden argument*/Ext_Emit_TisCloseEventArgs_t2470319931_m1774194048_MethodInfo_var);
		goto IL_00ad;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_008a;
		throw e;
	}

CATCH_008a:
	{ // begin catch(System.Exception)
		V_1 = ((Exception_t3991598821 *)__exception_local);
		Logger_t3695440972 * L_14 = __this->get__logger_19();
		il2cpp_codegen_memory_barrier();
		Exception_t3991598821 * L_15 = V_1;
		NullCheck(L_15);
		String_t* L_16 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Exception::ToString() */, L_15);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral188376329, L_16, /*hidden argument*/NULL);
		NullCheck(L_14);
		Logger_Fatal_m51902704(L_14, L_17, /*hidden argument*/NULL);
		goto IL_00ad;
	} // end catch (depth: 1)

IL_00ad:
	{
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::ConnectAsServer()
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1211897206;
extern const uint32_t WebSocket_ConnectAsServer_m789659036_MetadataUsageId;
extern "C"  void WebSocket_ConnectAsServer_m789659036 (WebSocket_t1342580397 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_ConnectAsServer_m789659036_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * V_0 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			bool L_0 = WebSocket_acceptHandshake_m303332664(__this, /*hidden argument*/NULL);
			if (!L_0)
			{
				goto IL_001a;
			}
		}

IL_000b:
		{
			il2cpp_codegen_memory_barrier();
			__this->set__readyState_27(1);
			WebSocket_open_m3051031279(__this, /*hidden argument*/NULL);
		}

IL_001a:
		{
			goto IL_0031;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001f;
		throw e;
	}

CATCH_001f:
	{ // begin catch(System.Exception)
		V_0 = ((Exception_t3991598821 *)__exception_local);
		Exception_t3991598821 * L_1 = V_0;
		WebSocket_acceptException_m1180715184(__this, L_1, _stringLiteral1211897206, /*hidden argument*/NULL);
		goto IL_0031;
	} // end catch (depth: 1)

IL_0031:
	{
		return;
	}
}
// System.String WebSocketSharp.WebSocket::CreateBase64Key()
extern Il2CppClass* ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var;
extern Il2CppClass* Random_t4255898871_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_CreateBase64Key_m565389870_MetadataUsageId;
extern "C"  String_t* WebSocket_CreateBase64Key_m565389870 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_CreateBase64Key_m565389870_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t4260760469* V_0 = NULL;
	Random_t4255898871 * V_1 = NULL;
	{
		V_0 = ((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16)));
		Random_t4255898871 * L_0 = (Random_t4255898871 *)il2cpp_codegen_object_new(Random_t4255898871_il2cpp_TypeInfo_var);
		Random__ctor_m2490522898(L_0, /*hidden argument*/NULL);
		V_1 = L_0;
		Random_t4255898871 * L_1 = V_1;
		ByteU5BU5D_t4260760469* L_2 = V_0;
		NullCheck(L_1);
		VirtActionInvoker1< ByteU5BU5D_t4260760469* >::Invoke(5 /* System.Void System.Random::NextBytes(System.Byte[]) */, L_1, L_2);
		ByteU5BU5D_t4260760469* L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		String_t* L_4 = Convert_ToBase64String_m1841808901(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.String WebSocketSharp.WebSocket::CreateResponseKey(System.String)
extern Il2CppClass* StringBuilder_t243639308_il2cpp_TypeInfo_var;
extern Il2CppClass* SHA1CryptoServiceProvider_t3725620262_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t2012439129_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3835136033;
extern const uint32_t WebSocket_CreateResponseKey_m666974534_MetadataUsageId;
extern "C"  String_t* WebSocket_CreateResponseKey_m666974534 (Il2CppObject * __this /* static, unused */, String_t* ___base64Key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_CreateResponseKey_m666974534_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t243639308 * V_0 = NULL;
	SHA1_t3976944113 * V_1 = NULL;
	ByteU5BU5D_t4260760469* V_2 = NULL;
	{
		String_t* L_0 = ___base64Key0;
		StringBuilder_t243639308 * L_1 = (StringBuilder_t243639308 *)il2cpp_codegen_object_new(StringBuilder_t243639308_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1310751873(L_1, L_0, ((int32_t)64), /*hidden argument*/NULL);
		V_0 = L_1;
		StringBuilder_t243639308 * L_2 = V_0;
		NullCheck(L_2);
		StringBuilder_Append_m3898090075(L_2, _stringLiteral3835136033, /*hidden argument*/NULL);
		SHA1CryptoServiceProvider_t3725620262 * L_3 = (SHA1CryptoServiceProvider_t3725620262 *)il2cpp_codegen_object_new(SHA1CryptoServiceProvider_t3725620262_il2cpp_TypeInfo_var);
		SHA1CryptoServiceProvider__ctor_m2094228389(L_3, /*hidden argument*/NULL);
		V_1 = L_3;
		SHA1_t3976944113 * L_4 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t2012439129_il2cpp_TypeInfo_var);
		Encoding_t2012439129 * L_5 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		StringBuilder_t243639308 * L_6 = V_0;
		NullCheck(L_6);
		String_t* L_7 = StringBuilder_ToString_m350379841(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		ByteU5BU5D_t4260760469* L_8 = VirtFuncInvoker1< ByteU5BU5D_t4260760469*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_5, L_7);
		NullCheck(L_4);
		ByteU5BU5D_t4260760469* L_9 = HashAlgorithm_ComputeHash_m1325366732(L_4, L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		ByteU5BU5D_t4260760469* L_10 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		String_t* L_11 = Convert_ToBase64String_m1841808901(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.Boolean WebSocketSharp.WebSocket::Ping(System.Byte[],System.Int32)
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2395734818;
extern const uint32_t WebSocket_Ping_m3619812057_MetadataUsageId;
extern "C"  bool WebSocket_Ping_m3619812057 (WebSocket_t1342580397 * __this, ByteU5BU5D_t4260760469* ___frame0, int32_t ___timeout1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_Ping_m3619812057_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AutoResetEvent_t874642578 * V_0 = NULL;
	Exception_t3991598821 * V_1 = NULL;
	bool V_2 = false;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B5_0 = 0;

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			uint16_t L_0 = __this->get__readyState_27();
			il2cpp_codegen_memory_barrier();
			if ((!(((uint32_t)L_0) == ((uint32_t)1))))
			{
				goto IL_0030;
			}
		}

IL_000e:
		{
			ByteU5BU5D_t4260760469* L_1 = ___frame0;
			bool L_2 = WebSocket_send_m1644284424(__this, L_1, /*hidden argument*/NULL);
			if (!L_2)
			{
				goto IL_0030;
			}
		}

IL_001a:
		{
			AutoResetEvent_t874642578 * L_3 = __this->get__receivePong_28();
			AutoResetEvent_t874642578 * L_4 = L_3;
			V_0 = L_4;
			if (!L_4)
			{
				goto IL_0030;
			}
		}

IL_0027:
		{
			AutoResetEvent_t874642578 * L_5 = V_0;
			int32_t L_6 = ___timeout1;
			NullCheck(L_5);
			bool L_7 = VirtFuncInvoker1< bool, int32_t >::Invoke(11 /* System.Boolean System.Threading.WaitHandle::WaitOne(System.Int32) */, L_5, L_6);
			G_B5_0 = ((int32_t)(L_7));
			goto IL_0031;
		}

IL_0030:
		{
			G_B5_0 = 0;
		}

IL_0031:
		{
			V_2 = (bool)G_B5_0;
			goto IL_0066;
		}

IL_0037:
		{
			; // IL_0037: leave IL_0066
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_003c;
		throw e;
	}

CATCH_003c:
	{ // begin catch(System.Exception)
		{
			V_1 = ((Exception_t3991598821 *)__exception_local);
			Logger_t3695440972 * L_8 = __this->get__logger_19();
			il2cpp_codegen_memory_barrier();
			Exception_t3991598821 * L_9 = V_1;
			NullCheck(L_9);
			String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Exception::ToString() */, L_9);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_11 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2395734818, L_10, /*hidden argument*/NULL);
			NullCheck(L_8);
			Logger_Fatal_m51902704(L_8, L_11, /*hidden argument*/NULL);
			V_2 = (bool)0;
			goto IL_0066;
		}

IL_0061:
		{
			; // IL_0061: leave IL_0066
		}
	} // end catch (depth: 1)

IL_0066:
	{
		bool L_12 = V_2;
		return L_12;
	}
}
// System.Void WebSocketSharp.WebSocket::Send(WebSocketSharp.Opcode,System.Byte[],System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Byte[]>)
extern Il2CppClass* WebSocketFrame_t778194306_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_TryGetValue_m1941982266_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m3439871131_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4246579071;
extern const uint32_t WebSocket_Send_m2771733563_MetadataUsageId;
extern "C"  void WebSocket_Send_m2771733563 (WebSocket_t1342580397 * __this, uint8_t ___opcode0, ByteU5BU5D_t4260760469* ___data1, Dictionary_2_t1115770063 * ___cache2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_Send_m2771733563_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ByteU5BU5D_t4260760469* V_2 = NULL;
	Exception_t3991598821 * V_3 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = __this->get__forSend_17();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_2 = __this->get__forConn_14();
			V_1 = L_2;
			Il2CppObject * L_3 = V_1;
			Monitor_Enter_m476686225(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		}

IL_001a:
		try
		{ // begin try (depth: 2)
			{
				uint16_t L_4 = __this->get__readyState_27();
				il2cpp_codegen_memory_barrier();
				if ((((int32_t)L_4) == ((int32_t)1)))
				{
					goto IL_002d;
				}
			}

IL_0028:
			{
				IL2CPP_LEAVE(0xC1, FINALLY_00ae);
			}

IL_002d:
			try
			{ // begin try (depth: 3)
				{
					Dictionary_2_t1115770063 * L_5 = ___cache2;
					uint8_t L_6 = __this->get__compression_8();
					NullCheck(L_5);
					bool L_7 = Dictionary_2_TryGetValue_m1941982266(L_5, L_6, (&V_2), /*hidden argument*/Dictionary_2_TryGetValue_m1941982266_MethodInfo_var);
					if (L_7)
					{
						goto IL_0073;
					}
				}

IL_0040:
				{
					uint8_t L_8 = ___opcode0;
					ByteU5BU5D_t4260760469* L_9 = ___data1;
					uint8_t L_10 = __this->get__compression_8();
					ByteU5BU5D_t4260760469* L_11 = Ext_Compress_m2695677868(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
					uint8_t L_12 = __this->get__compression_8();
					IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t778194306_il2cpp_TypeInfo_var);
					WebSocketFrame_t778194306 * L_13 = WebSocketFrame_CreateFrame_m309663431(NULL /*static, unused*/, 1, L_8, 0, L_11, (bool)((((int32_t)((((int32_t)L_12) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
					NullCheck(L_13);
					ByteU5BU5D_t4260760469* L_14 = WebSocketFrame_ToByteArray_m3468314104(L_13, /*hidden argument*/NULL);
					V_2 = L_14;
					Dictionary_2_t1115770063 * L_15 = ___cache2;
					uint8_t L_16 = __this->get__compression_8();
					ByteU5BU5D_t4260760469* L_17 = V_2;
					NullCheck(L_15);
					Dictionary_2_Add_m3439871131(L_15, L_16, L_17, /*hidden argument*/Dictionary_2_Add_m3439871131_MethodInfo_var);
				}

IL_0073:
				{
					WebSocketStream_t4103435597 * L_18 = __this->get__stream_30();
					ByteU5BU5D_t4260760469* L_19 = V_2;
					NullCheck(L_18);
					WebSocketStream_Write_m2408900417(L_18, L_19, /*hidden argument*/NULL);
					goto IL_00a9;
				}
			} // end try (depth: 3)
			catch(Il2CppExceptionWrapper& e)
			{
				__exception_local = (Exception_t3991598821 *)e.ex;
				if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
					goto CATCH_0085;
				throw e;
			}

CATCH_0085:
			{ // begin catch(System.Exception)
				V_3 = ((Exception_t3991598821 *)__exception_local);
				Logger_t3695440972 * L_20 = __this->get__logger_19();
				il2cpp_codegen_memory_barrier();
				Exception_t3991598821 * L_21 = V_3;
				NullCheck(L_21);
				String_t* L_22 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Exception::ToString() */, L_21);
				NullCheck(L_20);
				Logger_Fatal_m51902704(L_20, L_22, /*hidden argument*/NULL);
				WebSocket_error_m149167037(__this, _stringLiteral4246579071, /*hidden argument*/NULL);
				goto IL_00a9;
			} // end catch (depth: 3)

IL_00a9:
			{
				IL2CPP_LEAVE(0xB5, FINALLY_00ae);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
			goto FINALLY_00ae;
		}

FINALLY_00ae:
		{ // begin finally (depth: 2)
			Il2CppObject * L_23 = V_1;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(174)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(174)
		{
			IL2CPP_END_CLEANUP(0xC1, FINALLY_00ba);
			IL2CPP_JUMP_TBL(0xB5, IL_00b5)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
		}

IL_00b5:
		{
			IL2CPP_LEAVE(0xC1, FINALLY_00ba);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_00ba;
	}

FINALLY_00ba:
	{ // begin finally (depth: 1)
		Il2CppObject * L_24 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(186)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(186)
	{
		IL2CPP_JUMP_TBL(0xC1, IL_00c1)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_00c1:
	{
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::Send(WebSocketSharp.Opcode,System.IO.Stream,System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.IO.Stream>)
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_TryGetValue_m343797240_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m4118348341_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4246579071;
extern const uint32_t WebSocket_Send_m698872459_MetadataUsageId;
extern "C"  void WebSocket_Send_m698872459 (WebSocket_t1342580397 * __this, uint8_t ___opcode0, Stream_t1561764144 * ___stream1, Dictionary_2_t2711741034 * ___cache2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_Send_m698872459_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Stream_t1561764144 * V_1 = NULL;
	Exception_t3991598821 * V_2 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = __this->get__forSend_17();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			{
				Dictionary_2_t2711741034 * L_2 = ___cache2;
				uint8_t L_3 = __this->get__compression_8();
				NullCheck(L_2);
				bool L_4 = Dictionary_2_TryGetValue_m343797240(L_2, L_3, (&V_1), /*hidden argument*/Dictionary_2_TryGetValue_m343797240_MethodInfo_var);
				if (L_4)
				{
					goto IL_003f;
				}
			}

IL_0020:
			{
				Stream_t1561764144 * L_5 = ___stream1;
				uint8_t L_6 = __this->get__compression_8();
				Stream_t1561764144 * L_7 = Ext_Compress_m4045017470(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
				V_1 = L_7;
				Dictionary_2_t2711741034 * L_8 = ___cache2;
				uint8_t L_9 = __this->get__compression_8();
				Stream_t1561764144 * L_10 = V_1;
				NullCheck(L_8);
				Dictionary_2_Add_m4118348341(L_8, L_9, L_10, /*hidden argument*/Dictionary_2_Add_m4118348341_MethodInfo_var);
				goto IL_0047;
			}

IL_003f:
			{
				Stream_t1561764144 * L_11 = V_1;
				NullCheck(L_11);
				VirtActionInvoker1< int64_t >::Invoke(10 /* System.Void System.IO.Stream::set_Position(System.Int64) */, L_11, (((int64_t)((int64_t)0))));
			}

IL_0047:
			{
				uint16_t L_12 = __this->get__readyState_27();
				il2cpp_codegen_memory_barrier();
				if ((!(((uint32_t)L_12) == ((uint32_t)1))))
				{
					goto IL_006b;
				}
			}

IL_0055:
			{
				uint8_t L_13 = ___opcode0;
				Stream_t1561764144 * L_14 = V_1;
				uint8_t L_15 = __this->get__compression_8();
				WebSocket_sendFragmented_m3902794260(__this, L_13, L_14, 0, (bool)((((int32_t)((((int32_t)L_15) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
			}

IL_006b:
			{
				goto IL_0094;
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t3991598821 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0070;
			throw e;
		}

CATCH_0070:
		{ // begin catch(System.Exception)
			V_2 = ((Exception_t3991598821 *)__exception_local);
			Logger_t3695440972 * L_16 = __this->get__logger_19();
			il2cpp_codegen_memory_barrier();
			Exception_t3991598821 * L_17 = V_2;
			NullCheck(L_17);
			String_t* L_18 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Exception::ToString() */, L_17);
			NullCheck(L_16);
			Logger_Fatal_m51902704(L_16, L_18, /*hidden argument*/NULL);
			WebSocket_error_m149167037(__this, _stringLiteral4246579071, /*hidden argument*/NULL);
			goto IL_0094;
		} // end catch (depth: 2)

IL_0094:
		{
			IL2CPP_LEAVE(0xA0, FINALLY_0099);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0099;
	}

FINALLY_0099:
	{ // begin finally (depth: 1)
		Il2CppObject * L_19 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(153)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(153)
	{
		IL2CPP_JUMP_TBL(0xA0, IL_00a0)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_00a0:
	{
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::Close()
extern Il2CppClass* PayloadData_t39926750_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_Close_m3887126325_MetadataUsageId;
extern "C"  void WebSocket_Close_m3887126325 (WebSocket_t1342580397 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_Close_m3887126325_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	{
		uint16_t L_0 = __this->get__readyState_27();
		il2cpp_codegen_memory_barrier();
		String_t* L_1 = Ext_CheckIfClosable_m3222359761(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		if (!L_2)
		{
			goto IL_002a;
		}
	}
	{
		Logger_t3695440972 * L_3 = __this->get__logger_19();
		il2cpp_codegen_memory_barrier();
		String_t* L_4 = V_0;
		NullCheck(L_3);
		Logger_Error_m3981980524(L_3, L_4, /*hidden argument*/NULL);
		String_t* L_5 = V_0;
		WebSocket_error_m149167037(__this, L_5, /*hidden argument*/NULL);
		return;
	}

IL_002a:
	{
		uint16_t L_6 = __this->get__readyState_27();
		il2cpp_codegen_memory_barrier();
		V_1 = (bool)((((int32_t)L_6) == ((int32_t)1))? 1 : 0);
		PayloadData_t39926750 * L_7 = (PayloadData_t39926750 *)il2cpp_codegen_object_new(PayloadData_t39926750_il2cpp_TypeInfo_var);
		PayloadData__ctor_m1953054222(L_7, /*hidden argument*/NULL);
		bool L_8 = V_1;
		bool L_9 = V_1;
		WebSocket_close_m707576104(__this, L_7, L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::Close(System.UInt16)
extern "C"  void WebSocket_Close_m338211039 (WebSocket_t1342580397 * __this, uint16_t ___code0, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___code0;
		WebSocket_Close_m3721357019(__this, L_0, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::Close(WebSocketSharp.CloseStatusCode)
extern "C"  void WebSocket_Close_m3195629129 (WebSocket_t1342580397 * __this, uint16_t ___code0, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___code0;
		WebSocket_Close_m1852531397(__this, L_0, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::Close(System.UInt16,System.String)
extern Il2CppClass* UInt16_t24667923_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* PayloadData_t39926750_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3360002628;
extern Il2CppCodeGenString* _stringLiteral4090995502;
extern const uint32_t WebSocket_Close_m3721357019_MetadataUsageId;
extern "C"  void WebSocket_Close_m3721357019 (WebSocket_t1342580397 * __this, uint16_t ___code0, String_t* ___reason1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_Close_m3721357019_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t4260760469* V_0 = NULL;
	String_t* V_1 = NULL;
	bool V_2 = false;
	String_t* G_B3_0 = NULL;
	String_t* G_B1_0 = NULL;
	String_t* G_B2_0 = NULL;
	int32_t G_B8_0 = 0;
	{
		V_0 = (ByteU5BU5D_t4260760469*)NULL;
		uint16_t L_0 = __this->get__readyState_27();
		il2cpp_codegen_memory_barrier();
		String_t* L_1 = Ext_CheckIfClosable_m3222359761(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		String_t* L_2 = L_1;
		G_B1_0 = L_2;
		if (L_2)
		{
			G_B3_0 = L_2;
			goto IL_0036;
		}
	}
	{
		uint16_t L_3 = ___code0;
		String_t* L_4 = Ext_CheckIfValidCloseStatusCode_m3292340564(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		String_t* L_5 = L_4;
		G_B2_0 = L_5;
		if (L_5)
		{
			G_B3_0 = L_5;
			goto IL_0036;
		}
	}
	{
		uint16_t L_6 = ___code0;
		String_t* L_7 = ___reason1;
		ByteU5BU5D_t4260760469* L_8 = Ext_Append_m501190413(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		ByteU5BU5D_t4260760469* L_9 = L_8;
		V_0 = L_9;
		String_t* L_10 = Ext_CheckIfValidControlData_m912712725(NULL /*static, unused*/, L_9, _stringLiteral3360002628, /*hidden argument*/NULL);
		G_B3_0 = L_10;
	}

IL_0036:
	{
		V_1 = G_B3_0;
		String_t* L_11 = V_1;
		if (!L_11)
		{
			goto IL_0064;
		}
	}
	{
		Logger_t3695440972 * L_12 = __this->get__logger_19();
		il2cpp_codegen_memory_barrier();
		String_t* L_13 = V_1;
		uint16_t L_14 = ___code0;
		uint16_t L_15 = L_14;
		Il2CppObject * L_16 = Box(UInt16_t24667923_il2cpp_TypeInfo_var, &L_15);
		String_t* L_17 = ___reason1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Format_m3928391288(NULL /*static, unused*/, _stringLiteral4090995502, L_13, L_16, L_17, /*hidden argument*/NULL);
		NullCheck(L_12);
		Logger_Error_m3981980524(L_12, L_18, /*hidden argument*/NULL);
		String_t* L_19 = V_1;
		WebSocket_error_m149167037(__this, L_19, /*hidden argument*/NULL);
		return;
	}

IL_0064:
	{
		uint16_t L_20 = __this->get__readyState_27();
		il2cpp_codegen_memory_barrier();
		if ((!(((uint32_t)L_20) == ((uint32_t)1))))
		{
			goto IL_007d;
		}
	}
	{
		uint16_t L_21 = ___code0;
		bool L_22 = Ext_IsReserved_m627022511(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		G_B8_0 = ((((int32_t)L_22) == ((int32_t)0))? 1 : 0);
		goto IL_007e;
	}

IL_007d:
	{
		G_B8_0 = 0;
	}

IL_007e:
	{
		V_2 = (bool)G_B8_0;
		ByteU5BU5D_t4260760469* L_23 = V_0;
		PayloadData_t39926750 * L_24 = (PayloadData_t39926750 *)il2cpp_codegen_object_new(PayloadData_t39926750_il2cpp_TypeInfo_var);
		PayloadData__ctor_m719751291(L_24, L_23, /*hidden argument*/NULL);
		bool L_25 = V_2;
		bool L_26 = V_2;
		WebSocket_close_m707576104(__this, L_24, L_25, L_26, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::Close(WebSocketSharp.CloseStatusCode,System.String)
extern Il2CppClass* CloseStatusCode_t3936110621_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* PayloadData_t39926750_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3360002628;
extern Il2CppCodeGenString* _stringLiteral4090995502;
extern const uint32_t WebSocket_Close_m1852531397_MetadataUsageId;
extern "C"  void WebSocket_Close_m1852531397 (WebSocket_t1342580397 * __this, uint16_t ___code0, String_t* ___reason1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_Close_m1852531397_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t4260760469* V_0 = NULL;
	String_t* V_1 = NULL;
	bool V_2 = false;
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	int32_t G_B7_0 = 0;
	{
		V_0 = (ByteU5BU5D_t4260760469*)NULL;
		uint16_t L_0 = __this->get__readyState_27();
		il2cpp_codegen_memory_barrier();
		String_t* L_1 = Ext_CheckIfClosable_m3222359761(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		String_t* L_2 = L_1;
		G_B1_0 = L_2;
		if (L_2)
		{
			G_B2_0 = L_2;
			goto IL_0029;
		}
	}
	{
		uint16_t L_3 = ___code0;
		String_t* L_4 = ___reason1;
		ByteU5BU5D_t4260760469* L_5 = Ext_Append_m501190413(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		ByteU5BU5D_t4260760469* L_6 = L_5;
		V_0 = L_6;
		String_t* L_7 = Ext_CheckIfValidControlData_m912712725(NULL /*static, unused*/, L_6, _stringLiteral3360002628, /*hidden argument*/NULL);
		G_B2_0 = L_7;
	}

IL_0029:
	{
		V_1 = G_B2_0;
		String_t* L_8 = V_1;
		if (!L_8)
		{
			goto IL_0057;
		}
	}
	{
		Logger_t3695440972 * L_9 = __this->get__logger_19();
		il2cpp_codegen_memory_barrier();
		String_t* L_10 = V_1;
		uint16_t L_11 = ___code0;
		uint16_t L_12 = L_11;
		Il2CppObject * L_13 = Box(CloseStatusCode_t3936110621_il2cpp_TypeInfo_var, &L_12);
		String_t* L_14 = ___reason1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Format_m3928391288(NULL /*static, unused*/, _stringLiteral4090995502, L_10, L_13, L_14, /*hidden argument*/NULL);
		NullCheck(L_9);
		Logger_Error_m3981980524(L_9, L_15, /*hidden argument*/NULL);
		String_t* L_16 = V_1;
		WebSocket_error_m149167037(__this, L_16, /*hidden argument*/NULL);
		return;
	}

IL_0057:
	{
		uint16_t L_17 = __this->get__readyState_27();
		il2cpp_codegen_memory_barrier();
		if ((!(((uint32_t)L_17) == ((uint32_t)1))))
		{
			goto IL_0070;
		}
	}
	{
		uint16_t L_18 = ___code0;
		bool L_19 = Ext_IsReserved_m4250492537(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		G_B7_0 = ((((int32_t)L_19) == ((int32_t)0))? 1 : 0);
		goto IL_0071;
	}

IL_0070:
	{
		G_B7_0 = 0;
	}

IL_0071:
	{
		V_2 = (bool)G_B7_0;
		ByteU5BU5D_t4260760469* L_20 = V_0;
		PayloadData_t39926750 * L_21 = (PayloadData_t39926750 *)il2cpp_codegen_object_new(PayloadData_t39926750_il2cpp_TypeInfo_var);
		PayloadData__ctor_m719751291(L_21, L_20, /*hidden argument*/NULL);
		bool L_22 = V_2;
		bool L_23 = V_2;
		WebSocket_close_m707576104(__this, L_21, L_22, L_23, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::CloseAsync()
extern Il2CppClass* PayloadData_t39926750_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_CloseAsync_m1380802217_MetadataUsageId;
extern "C"  void WebSocket_CloseAsync_m1380802217 (WebSocket_t1342580397 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_CloseAsync_m1380802217_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	{
		uint16_t L_0 = __this->get__readyState_27();
		il2cpp_codegen_memory_barrier();
		String_t* L_1 = Ext_CheckIfClosable_m3222359761(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		if (!L_2)
		{
			goto IL_002a;
		}
	}
	{
		Logger_t3695440972 * L_3 = __this->get__logger_19();
		il2cpp_codegen_memory_barrier();
		String_t* L_4 = V_0;
		NullCheck(L_3);
		Logger_Error_m3981980524(L_3, L_4, /*hidden argument*/NULL);
		String_t* L_5 = V_0;
		WebSocket_error_m149167037(__this, L_5, /*hidden argument*/NULL);
		return;
	}

IL_002a:
	{
		uint16_t L_6 = __this->get__readyState_27();
		il2cpp_codegen_memory_barrier();
		V_1 = (bool)((((int32_t)L_6) == ((int32_t)1))? 1 : 0);
		PayloadData_t39926750 * L_7 = (PayloadData_t39926750 *)il2cpp_codegen_object_new(PayloadData_t39926750_il2cpp_TypeInfo_var);
		PayloadData__ctor_m1953054222(L_7, /*hidden argument*/NULL);
		bool L_8 = V_1;
		bool L_9 = V_1;
		WebSocket_closeAsync_m1147887708(__this, L_7, L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::CloseAsync(System.UInt16)
extern "C"  void WebSocket_CloseAsync_m350152171 (WebSocket_t1342580397 * __this, uint16_t ___code0, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___code0;
		WebSocket_CloseAsync_m189650663(__this, L_0, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::CloseAsync(WebSocketSharp.CloseStatusCode)
extern "C"  void WebSocket_CloseAsync_m3945427645 (WebSocket_t1342580397 * __this, uint16_t ___code0, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___code0;
		WebSocket_CloseAsync_m913411129(__this, L_0, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::CloseAsync(System.UInt16,System.String)
extern Il2CppClass* UInt16_t24667923_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* PayloadData_t39926750_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3360002628;
extern Il2CppCodeGenString* _stringLiteral4090995502;
extern const uint32_t WebSocket_CloseAsync_m189650663_MetadataUsageId;
extern "C"  void WebSocket_CloseAsync_m189650663 (WebSocket_t1342580397 * __this, uint16_t ___code0, String_t* ___reason1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_CloseAsync_m189650663_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t4260760469* V_0 = NULL;
	String_t* V_1 = NULL;
	bool V_2 = false;
	String_t* G_B3_0 = NULL;
	String_t* G_B1_0 = NULL;
	String_t* G_B2_0 = NULL;
	int32_t G_B8_0 = 0;
	{
		V_0 = (ByteU5BU5D_t4260760469*)NULL;
		uint16_t L_0 = __this->get__readyState_27();
		il2cpp_codegen_memory_barrier();
		String_t* L_1 = Ext_CheckIfClosable_m3222359761(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		String_t* L_2 = L_1;
		G_B1_0 = L_2;
		if (L_2)
		{
			G_B3_0 = L_2;
			goto IL_0036;
		}
	}
	{
		uint16_t L_3 = ___code0;
		String_t* L_4 = Ext_CheckIfValidCloseStatusCode_m3292340564(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		String_t* L_5 = L_4;
		G_B2_0 = L_5;
		if (L_5)
		{
			G_B3_0 = L_5;
			goto IL_0036;
		}
	}
	{
		uint16_t L_6 = ___code0;
		String_t* L_7 = ___reason1;
		ByteU5BU5D_t4260760469* L_8 = Ext_Append_m501190413(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		ByteU5BU5D_t4260760469* L_9 = L_8;
		V_0 = L_9;
		String_t* L_10 = Ext_CheckIfValidControlData_m912712725(NULL /*static, unused*/, L_9, _stringLiteral3360002628, /*hidden argument*/NULL);
		G_B3_0 = L_10;
	}

IL_0036:
	{
		V_1 = G_B3_0;
		String_t* L_11 = V_1;
		if (!L_11)
		{
			goto IL_0064;
		}
	}
	{
		Logger_t3695440972 * L_12 = __this->get__logger_19();
		il2cpp_codegen_memory_barrier();
		String_t* L_13 = V_1;
		uint16_t L_14 = ___code0;
		uint16_t L_15 = L_14;
		Il2CppObject * L_16 = Box(UInt16_t24667923_il2cpp_TypeInfo_var, &L_15);
		String_t* L_17 = ___reason1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Format_m3928391288(NULL /*static, unused*/, _stringLiteral4090995502, L_13, L_16, L_17, /*hidden argument*/NULL);
		NullCheck(L_12);
		Logger_Error_m3981980524(L_12, L_18, /*hidden argument*/NULL);
		String_t* L_19 = V_1;
		WebSocket_error_m149167037(__this, L_19, /*hidden argument*/NULL);
		return;
	}

IL_0064:
	{
		uint16_t L_20 = __this->get__readyState_27();
		il2cpp_codegen_memory_barrier();
		if ((!(((uint32_t)L_20) == ((uint32_t)1))))
		{
			goto IL_007d;
		}
	}
	{
		uint16_t L_21 = ___code0;
		bool L_22 = Ext_IsReserved_m627022511(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		G_B8_0 = ((((int32_t)L_22) == ((int32_t)0))? 1 : 0);
		goto IL_007e;
	}

IL_007d:
	{
		G_B8_0 = 0;
	}

IL_007e:
	{
		V_2 = (bool)G_B8_0;
		ByteU5BU5D_t4260760469* L_23 = V_0;
		PayloadData_t39926750 * L_24 = (PayloadData_t39926750 *)il2cpp_codegen_object_new(PayloadData_t39926750_il2cpp_TypeInfo_var);
		PayloadData__ctor_m719751291(L_24, L_23, /*hidden argument*/NULL);
		bool L_25 = V_2;
		bool L_26 = V_2;
		WebSocket_closeAsync_m1147887708(__this, L_24, L_25, L_26, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::CloseAsync(WebSocketSharp.CloseStatusCode,System.String)
extern Il2CppClass* CloseStatusCode_t3936110621_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* PayloadData_t39926750_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3360002628;
extern Il2CppCodeGenString* _stringLiteral4090995502;
extern const uint32_t WebSocket_CloseAsync_m913411129_MetadataUsageId;
extern "C"  void WebSocket_CloseAsync_m913411129 (WebSocket_t1342580397 * __this, uint16_t ___code0, String_t* ___reason1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_CloseAsync_m913411129_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t4260760469* V_0 = NULL;
	String_t* V_1 = NULL;
	bool V_2 = false;
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	int32_t G_B7_0 = 0;
	{
		V_0 = (ByteU5BU5D_t4260760469*)NULL;
		uint16_t L_0 = __this->get__readyState_27();
		il2cpp_codegen_memory_barrier();
		String_t* L_1 = Ext_CheckIfClosable_m3222359761(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		String_t* L_2 = L_1;
		G_B1_0 = L_2;
		if (L_2)
		{
			G_B2_0 = L_2;
			goto IL_0029;
		}
	}
	{
		uint16_t L_3 = ___code0;
		String_t* L_4 = ___reason1;
		ByteU5BU5D_t4260760469* L_5 = Ext_Append_m501190413(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		ByteU5BU5D_t4260760469* L_6 = L_5;
		V_0 = L_6;
		String_t* L_7 = Ext_CheckIfValidControlData_m912712725(NULL /*static, unused*/, L_6, _stringLiteral3360002628, /*hidden argument*/NULL);
		G_B2_0 = L_7;
	}

IL_0029:
	{
		V_1 = G_B2_0;
		String_t* L_8 = V_1;
		if (!L_8)
		{
			goto IL_0057;
		}
	}
	{
		Logger_t3695440972 * L_9 = __this->get__logger_19();
		il2cpp_codegen_memory_barrier();
		String_t* L_10 = V_1;
		uint16_t L_11 = ___code0;
		uint16_t L_12 = L_11;
		Il2CppObject * L_13 = Box(CloseStatusCode_t3936110621_il2cpp_TypeInfo_var, &L_12);
		String_t* L_14 = ___reason1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Format_m3928391288(NULL /*static, unused*/, _stringLiteral4090995502, L_10, L_13, L_14, /*hidden argument*/NULL);
		NullCheck(L_9);
		Logger_Error_m3981980524(L_9, L_15, /*hidden argument*/NULL);
		String_t* L_16 = V_1;
		WebSocket_error_m149167037(__this, L_16, /*hidden argument*/NULL);
		return;
	}

IL_0057:
	{
		uint16_t L_17 = __this->get__readyState_27();
		il2cpp_codegen_memory_barrier();
		if ((!(((uint32_t)L_17) == ((uint32_t)1))))
		{
			goto IL_0070;
		}
	}
	{
		uint16_t L_18 = ___code0;
		bool L_19 = Ext_IsReserved_m4250492537(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		G_B7_0 = ((((int32_t)L_19) == ((int32_t)0))? 1 : 0);
		goto IL_0071;
	}

IL_0070:
	{
		G_B7_0 = 0;
	}

IL_0071:
	{
		V_2 = (bool)G_B7_0;
		ByteU5BU5D_t4260760469* L_20 = V_0;
		PayloadData_t39926750 * L_21 = (PayloadData_t39926750 *)il2cpp_codegen_object_new(PayloadData_t39926750_il2cpp_TypeInfo_var);
		PayloadData__ctor_m719751291(L_21, L_20, /*hidden argument*/NULL);
		bool L_22 = V_2;
		bool L_23 = V_2;
		WebSocket_closeAsync_m1147887708(__this, L_21, L_22, L_23, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::SetHeader(System.String,System.String)
extern Il2CppClass* NameValueCollection_t2791941106_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_SetHeader_m50374962_MetadataUsageId;
extern "C"  void WebSocket_SetHeader_m50374962 (WebSocket_t1342580397 * __this, String_t* ___header0, String_t* ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_SetHeader_m50374962_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NameValueCollection_t2791941106 * L_0 = __this->get__customHeaders_23();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		NameValueCollection_t2791941106 * L_1 = (NameValueCollection_t2791941106 *)il2cpp_codegen_object_new(NameValueCollection_t2791941106_il2cpp_TypeInfo_var);
		NameValueCollection__ctor_m2553202389(L_1, /*hidden argument*/NULL);
		__this->set__customHeaders_23(L_1);
	}

IL_0016:
	{
		NameValueCollection_t2791941106 * L_2 = __this->get__customHeaders_23();
		String_t* L_3 = ___header0;
		String_t* L_4 = ___value1;
		NullCheck(L_2);
		NameValueCollection_set_Item_m2275159743(L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::Connect()
extern "C"  void WebSocket_Connect_m3106463399 (WebSocket_t1342580397 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = WebSocket_checkIfCanConnect_m4087900537(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		Logger_t3695440972 * L_2 = __this->get__logger_19();
		il2cpp_codegen_memory_barrier();
		String_t* L_3 = V_0;
		NullCheck(L_2);
		Logger_Error_m3981980524(L_2, L_3, /*hidden argument*/NULL);
		String_t* L_4 = V_0;
		WebSocket_error_m149167037(__this, L_4, /*hidden argument*/NULL);
		return;
	}

IL_0023:
	{
		bool L_5 = WebSocket_connect_m1396814739(__this, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0034;
		}
	}
	{
		WebSocket_open_m3051031279(__this, /*hidden argument*/NULL);
	}

IL_0034:
	{
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::ConnectAsync()
extern Il2CppClass* U3CConnectAsyncU3Ec__AnonStorey2D_t4104301910_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_1_t1601960292_il2cpp_TypeInfo_var;
extern Il2CppClass* AsyncCallback_t1369114871_il2cpp_TypeInfo_var;
extern const MethodInfo* WebSocket_connect_m1396814739_MethodInfo_var;
extern const MethodInfo* Func_1__ctor_m3425941956_MethodInfo_var;
extern const MethodInfo* U3CConnectAsyncU3Ec__AnonStorey2D_U3CU3Em__14_m19811154_MethodInfo_var;
extern const MethodInfo* Func_1_BeginInvoke_m867564461_MethodInfo_var;
extern const uint32_t WebSocket_ConnectAsync_m1615474295_MetadataUsageId;
extern "C"  void WebSocket_ConnectAsync_m1615474295 (WebSocket_t1342580397 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_ConnectAsync_m1615474295_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	U3CConnectAsyncU3Ec__AnonStorey2D_t4104301910 * V_1 = NULL;
	{
		U3CConnectAsyncU3Ec__AnonStorey2D_t4104301910 * L_0 = (U3CConnectAsyncU3Ec__AnonStorey2D_t4104301910 *)il2cpp_codegen_object_new(U3CConnectAsyncU3Ec__AnonStorey2D_t4104301910_il2cpp_TypeInfo_var);
		U3CConnectAsyncU3Ec__AnonStorey2D__ctor_m2906769413(L_0, /*hidden argument*/NULL);
		V_1 = L_0;
		U3CConnectAsyncU3Ec__AnonStorey2D_t4104301910 * L_1 = V_1;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_1(__this);
		String_t* L_2 = WebSocket_checkIfCanConnect_m4087900537(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = V_0;
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		Logger_t3695440972 * L_4 = __this->get__logger_19();
		il2cpp_codegen_memory_barrier();
		String_t* L_5 = V_0;
		NullCheck(L_4);
		Logger_Error_m3981980524(L_4, L_5, /*hidden argument*/NULL);
		String_t* L_6 = V_0;
		WebSocket_error_m149167037(__this, L_6, /*hidden argument*/NULL);
		return;
	}

IL_0030:
	{
		U3CConnectAsyncU3Ec__AnonStorey2D_t4104301910 * L_7 = V_1;
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)WebSocket_connect_m1396814739_MethodInfo_var);
		Func_1_t1601960292 * L_9 = (Func_1_t1601960292 *)il2cpp_codegen_object_new(Func_1_t1601960292_il2cpp_TypeInfo_var);
		Func_1__ctor_m3425941956(L_9, __this, L_8, /*hidden argument*/Func_1__ctor_m3425941956_MethodInfo_var);
		NullCheck(L_7);
		L_7->set_connector_0(L_9);
		U3CConnectAsyncU3Ec__AnonStorey2D_t4104301910 * L_10 = V_1;
		NullCheck(L_10);
		Func_1_t1601960292 * L_11 = L_10->get_connector_0();
		U3CConnectAsyncU3Ec__AnonStorey2D_t4104301910 * L_12 = V_1;
		IntPtr_t L_13;
		L_13.set_m_value_0((void*)(void*)U3CConnectAsyncU3Ec__AnonStorey2D_U3CU3Em__14_m19811154_MethodInfo_var);
		AsyncCallback_t1369114871 * L_14 = (AsyncCallback_t1369114871 *)il2cpp_codegen_object_new(AsyncCallback_t1369114871_il2cpp_TypeInfo_var);
		AsyncCallback__ctor_m1986959762(L_14, L_12, L_13, /*hidden argument*/NULL);
		NullCheck(L_11);
		Func_1_BeginInvoke_m867564461(L_11, L_14, NULL, /*hidden argument*/Func_1_BeginInvoke_m867564461_MethodInfo_var);
		return;
	}
}
// System.Boolean WebSocketSharp.WebSocket::Ping()
extern Il2CppClass* WebSocketFrame_t778194306_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_Ping_m348145387_MetadataUsageId;
extern "C"  bool WebSocket_Ping_m348145387 (WebSocket_t1342580397 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_Ping_m348145387_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool G_B3_0 = false;
	{
		bool L_0 = __this->get__client_6();
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t778194306_il2cpp_TypeInfo_var);
		WebSocketFrame_t778194306 * L_1 = WebSocketFrame_CreatePingFrame_m982549890(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		NullCheck(L_1);
		ByteU5BU5D_t4260760469* L_2 = WebSocketFrame_ToByteArray_m3468314104(L_1, /*hidden argument*/NULL);
		bool L_3 = WebSocket_Ping_m3619812057(__this, L_2, ((int32_t)5000), /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_0036;
	}

IL_0026:
	{
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t778194306_il2cpp_TypeInfo_var);
		ByteU5BU5D_t4260760469* L_4 = ((WebSocketFrame_t778194306_StaticFields*)WebSocketFrame_t778194306_il2cpp_TypeInfo_var->static_fields)->get_EmptyUnmaskPingData_10();
		bool L_5 = WebSocket_Ping_m3619812057(__this, L_4, ((int32_t)1000), /*hidden argument*/NULL);
		G_B3_0 = L_5;
	}

IL_0036:
	{
		return G_B3_0;
	}
}
// System.Boolean WebSocketSharp.WebSocket::Ping(System.String)
extern Il2CppClass* Encoding_t2012439129_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocketFrame_t778194306_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral954925063;
extern const uint32_t WebSocket_Ping_m1968909815_MetadataUsageId;
extern "C"  bool WebSocket_Ping_m1968909815 (WebSocket_t1342580397 * __this, String_t* ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_Ping_m1968909815_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t4260760469* V_0 = NULL;
	String_t* V_1 = NULL;
	bool G_B8_0 = false;
	{
		String_t* L_0 = ___message0;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		String_t* L_1 = ___message0;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m2979997331(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0018;
		}
	}

IL_0011:
	{
		bool L_3 = WebSocket_Ping_m348145387(__this, /*hidden argument*/NULL);
		return L_3;
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t2012439129_il2cpp_TypeInfo_var);
		Encoding_t2012439129 * L_4 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_5 = ___message0;
		NullCheck(L_4);
		ByteU5BU5D_t4260760469* L_6 = VirtFuncInvoker1< ByteU5BU5D_t4260760469*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_4, L_5);
		V_0 = L_6;
		ByteU5BU5D_t4260760469* L_7 = V_0;
		String_t* L_8 = Ext_CheckIfValidControlData_m912712725(NULL /*static, unused*/, L_7, _stringLiteral954925063, /*hidden argument*/NULL);
		V_1 = L_8;
		String_t* L_9 = V_1;
		if (!L_9)
		{
			goto IL_004d;
		}
	}
	{
		Logger_t3695440972 * L_10 = __this->get__logger_19();
		il2cpp_codegen_memory_barrier();
		String_t* L_11 = V_1;
		NullCheck(L_10);
		Logger_Error_m3981980524(L_10, L_11, /*hidden argument*/NULL);
		String_t* L_12 = V_1;
		WebSocket_error_m149167037(__this, L_12, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_004d:
	{
		bool L_13 = __this->get__client_6();
		if (!L_13)
		{
			goto IL_0074;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_14 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t778194306_il2cpp_TypeInfo_var);
		WebSocketFrame_t778194306 * L_15 = WebSocketFrame_CreatePingFrame_m3019981573(NULL /*static, unused*/, 1, L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		ByteU5BU5D_t4260760469* L_16 = WebSocketFrame_ToByteArray_m3468314104(L_15, /*hidden argument*/NULL);
		bool L_17 = WebSocket_Ping_m3619812057(__this, L_16, ((int32_t)5000), /*hidden argument*/NULL);
		G_B8_0 = L_17;
		goto IL_008b;
	}

IL_0074:
	{
		ByteU5BU5D_t4260760469* L_18 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t778194306_il2cpp_TypeInfo_var);
		WebSocketFrame_t778194306 * L_19 = WebSocketFrame_CreatePingFrame_m3019981573(NULL /*static, unused*/, 0, L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		ByteU5BU5D_t4260760469* L_20 = WebSocketFrame_ToByteArray_m3468314104(L_19, /*hidden argument*/NULL);
		bool L_21 = WebSocket_Ping_m3619812057(__this, L_20, ((int32_t)1000), /*hidden argument*/NULL);
		G_B8_0 = L_21;
	}

IL_008b:
	{
		return G_B8_0;
	}
}
// System.Void WebSocketSharp.WebSocket::Send(System.Byte[])
extern Il2CppClass* MemoryStream_t418716369_il2cpp_TypeInfo_var;
extern const MethodInfo* Ext_Copy_TisByte_t2862609660_m4188581816_MethodInfo_var;
extern const uint32_t WebSocket_Send_m3742182428_MetadataUsageId;
extern "C"  void WebSocket_Send_m3742182428 (WebSocket_t1342580397 * __this, ByteU5BU5D_t4260760469* ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_Send_m3742182428_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	int64_t V_1 = 0;
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	int32_t G_B9_0 = 0;
	WebSocket_t1342580397 * G_B9_1 = NULL;
	int32_t G_B6_0 = 0;
	WebSocket_t1342580397 * G_B6_1 = NULL;
	int32_t G_B7_0 = 0;
	WebSocket_t1342580397 * G_B7_1 = NULL;
	int32_t G_B8_0 = 0;
	WebSocket_t1342580397 * G_B8_1 = NULL;
	ByteU5BU5D_t4260760469* G_B10_0 = NULL;
	int32_t G_B10_1 = 0;
	WebSocket_t1342580397 * G_B10_2 = NULL;
	{
		uint16_t L_0 = __this->get__readyState_27();
		il2cpp_codegen_memory_barrier();
		String_t* L_1 = Ext_CheckIfOpen_m3949729038(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		String_t* L_2 = L_1;
		G_B1_0 = L_2;
		if (L_2)
		{
			G_B2_0 = L_2;
			goto IL_001a;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_3 = ___data0;
		String_t* L_4 = Ext_CheckIfValidSendData_m503248158(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		G_B2_0 = L_4;
	}

IL_001a:
	{
		V_0 = G_B2_0;
		String_t* L_5 = V_0;
		if (!L_5)
		{
			goto IL_0037;
		}
	}
	{
		Logger_t3695440972 * L_6 = __this->get__logger_19();
		il2cpp_codegen_memory_barrier();
		String_t* L_7 = V_0;
		NullCheck(L_6);
		Logger_Error_m3981980524(L_6, L_7, /*hidden argument*/NULL);
		String_t* L_8 = V_0;
		WebSocket_error_m149167037(__this, L_8, /*hidden argument*/NULL);
		return;
	}

IL_0037:
	{
		ByteU5BU5D_t4260760469* L_9 = ___data0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_9);
		int64_t L_10 = Array_get_LongLength_m1209728916((Il2CppArray *)(Il2CppArray *)L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		int64_t L_11 = V_1;
		if ((((int64_t)L_11) > ((int64_t)(((int64_t)((int64_t)((int32_t)1016)))))))
		{
			goto IL_0082;
		}
	}
	{
		int64_t L_12 = V_1;
		G_B6_0 = 2;
		G_B6_1 = __this;
		if ((((int64_t)L_12) <= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			G_B9_0 = 2;
			G_B9_1 = __this;
			goto IL_0076;
		}
	}
	{
		bool L_13 = __this->get__client_6();
		G_B7_0 = G_B6_0;
		G_B7_1 = G_B6_1;
		if (!L_13)
		{
			G_B9_0 = G_B6_0;
			G_B9_1 = G_B6_1;
			goto IL_0076;
		}
	}
	{
		uint8_t L_14 = __this->get__compression_8();
		G_B8_0 = G_B7_0;
		G_B8_1 = G_B7_1;
		if (L_14)
		{
			G_B9_0 = G_B7_0;
			G_B9_1 = G_B7_1;
			goto IL_0076;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_15 = ___data0;
		int64_t L_16 = V_1;
		ByteU5BU5D_t4260760469* L_17 = Ext_Copy_TisByte_t2862609660_m4188581816(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/Ext_Copy_TisByte_t2862609660_m4188581816_MethodInfo_var);
		G_B10_0 = L_17;
		G_B10_1 = G_B8_0;
		G_B10_2 = G_B8_1;
		goto IL_0077;
	}

IL_0076:
	{
		ByteU5BU5D_t4260760469* L_18 = ___data0;
		G_B10_0 = L_18;
		G_B10_1 = G_B9_0;
		G_B10_2 = G_B9_1;
	}

IL_0077:
	{
		NullCheck(G_B10_2);
		WebSocket_send_m2882270907(G_B10_2, G_B10_1, G_B10_0, /*hidden argument*/NULL);
		goto IL_0090;
	}

IL_0082:
	{
		ByteU5BU5D_t4260760469* L_19 = ___data0;
		MemoryStream_t418716369 * L_20 = (MemoryStream_t418716369 *)il2cpp_codegen_object_new(MemoryStream_t418716369_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1231145921(L_20, L_19, /*hidden argument*/NULL);
		WebSocket_send_m2473628261(__this, 2, L_20, /*hidden argument*/NULL);
	}

IL_0090:
	{
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::Send(System.IO.FileInfo)
extern "C"  void WebSocket_Send_m7785242 (WebSocket_t1342580397 * __this, FileInfo_t3233670074 * ___file0, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	{
		uint16_t L_0 = __this->get__readyState_27();
		il2cpp_codegen_memory_barrier();
		String_t* L_1 = Ext_CheckIfOpen_m3949729038(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		String_t* L_2 = L_1;
		G_B1_0 = L_2;
		if (L_2)
		{
			G_B2_0 = L_2;
			goto IL_001a;
		}
	}
	{
		FileInfo_t3233670074 * L_3 = ___file0;
		String_t* L_4 = Ext_CheckIfValidSendData_m1906594648(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		G_B2_0 = L_4;
	}

IL_001a:
	{
		V_0 = G_B2_0;
		String_t* L_5 = V_0;
		if (!L_5)
		{
			goto IL_0037;
		}
	}
	{
		Logger_t3695440972 * L_6 = __this->get__logger_19();
		il2cpp_codegen_memory_barrier();
		String_t* L_7 = V_0;
		NullCheck(L_6);
		Logger_Error_m3981980524(L_6, L_7, /*hidden argument*/NULL);
		String_t* L_8 = V_0;
		WebSocket_error_m149167037(__this, L_8, /*hidden argument*/NULL);
		return;
	}

IL_0037:
	{
		FileInfo_t3233670074 * L_9 = ___file0;
		NullCheck(L_9);
		FileStream_t2141505868 * L_10 = FileInfo_OpenRead_m2980452971(L_9, /*hidden argument*/NULL);
		WebSocket_send_m2473628261(__this, 2, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::Send(System.String)
extern Il2CppClass* Encoding_t2012439129_il2cpp_TypeInfo_var;
extern Il2CppClass* MemoryStream_t418716369_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_Send_m1505020757_MetadataUsageId;
extern "C"  void WebSocket_Send_m1505020757 (WebSocket_t1342580397 * __this, String_t* ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_Send_m1505020757_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	ByteU5BU5D_t4260760469* V_1 = NULL;
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	{
		uint16_t L_0 = __this->get__readyState_27();
		il2cpp_codegen_memory_barrier();
		String_t* L_1 = Ext_CheckIfOpen_m3949729038(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		String_t* L_2 = L_1;
		G_B1_0 = L_2;
		if (L_2)
		{
			G_B2_0 = L_2;
			goto IL_001a;
		}
	}
	{
		String_t* L_3 = ___data0;
		String_t* L_4 = Ext_CheckIfValidSendData_m2561053783(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		G_B2_0 = L_4;
	}

IL_001a:
	{
		V_0 = G_B2_0;
		String_t* L_5 = V_0;
		if (!L_5)
		{
			goto IL_0037;
		}
	}
	{
		Logger_t3695440972 * L_6 = __this->get__logger_19();
		il2cpp_codegen_memory_barrier();
		String_t* L_7 = V_0;
		NullCheck(L_6);
		Logger_Error_m3981980524(L_6, L_7, /*hidden argument*/NULL);
		String_t* L_8 = V_0;
		WebSocket_error_m149167037(__this, L_8, /*hidden argument*/NULL);
		return;
	}

IL_0037:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t2012439129_il2cpp_TypeInfo_var);
		Encoding_t2012439129 * L_9 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_10 = ___data0;
		NullCheck(L_9);
		ByteU5BU5D_t4260760469* L_11 = VirtFuncInvoker1< ByteU5BU5D_t4260760469*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_9, L_10);
		V_1 = L_11;
		ByteU5BU5D_t4260760469* L_12 = V_1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_12);
		int64_t L_13 = Array_get_LongLength_m1209728916((Il2CppArray *)(Il2CppArray *)L_12, /*hidden argument*/NULL);
		if ((((int64_t)L_13) > ((int64_t)(((int64_t)((int64_t)((int32_t)1016)))))))
		{
			goto IL_0062;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_14 = V_1;
		WebSocket_send_m2882270907(__this, 1, L_14, /*hidden argument*/NULL);
		goto IL_0070;
	}

IL_0062:
	{
		ByteU5BU5D_t4260760469* L_15 = V_1;
		MemoryStream_t418716369 * L_16 = (MemoryStream_t418716369 *)il2cpp_codegen_object_new(MemoryStream_t418716369_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1231145921(L_16, L_15, /*hidden argument*/NULL);
		WebSocket_send_m2473628261(__this, 1, L_16, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::SendAsync(System.Byte[],System.Action`1<System.Boolean>)
extern Il2CppClass* MemoryStream_t418716369_il2cpp_TypeInfo_var;
extern const MethodInfo* Ext_Copy_TisByte_t2862609660_m4188581816_MethodInfo_var;
extern const uint32_t WebSocket_SendAsync_m1734499269_MetadataUsageId;
extern "C"  void WebSocket_SendAsync_m1734499269 (WebSocket_t1342580397 * __this, ByteU5BU5D_t4260760469* ___data0, Action_1_t872614854 * ___completed1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_SendAsync_m1734499269_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	int64_t V_1 = 0;
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	int32_t G_B9_0 = 0;
	WebSocket_t1342580397 * G_B9_1 = NULL;
	int32_t G_B6_0 = 0;
	WebSocket_t1342580397 * G_B6_1 = NULL;
	int32_t G_B7_0 = 0;
	WebSocket_t1342580397 * G_B7_1 = NULL;
	int32_t G_B8_0 = 0;
	WebSocket_t1342580397 * G_B8_1 = NULL;
	ByteU5BU5D_t4260760469* G_B10_0 = NULL;
	int32_t G_B10_1 = 0;
	WebSocket_t1342580397 * G_B10_2 = NULL;
	{
		uint16_t L_0 = __this->get__readyState_27();
		il2cpp_codegen_memory_barrier();
		String_t* L_1 = Ext_CheckIfOpen_m3949729038(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		String_t* L_2 = L_1;
		G_B1_0 = L_2;
		if (L_2)
		{
			G_B2_0 = L_2;
			goto IL_001a;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_3 = ___data0;
		String_t* L_4 = Ext_CheckIfValidSendData_m503248158(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		G_B2_0 = L_4;
	}

IL_001a:
	{
		V_0 = G_B2_0;
		String_t* L_5 = V_0;
		if (!L_5)
		{
			goto IL_0037;
		}
	}
	{
		Logger_t3695440972 * L_6 = __this->get__logger_19();
		il2cpp_codegen_memory_barrier();
		String_t* L_7 = V_0;
		NullCheck(L_6);
		Logger_Error_m3981980524(L_6, L_7, /*hidden argument*/NULL);
		String_t* L_8 = V_0;
		WebSocket_error_m149167037(__this, L_8, /*hidden argument*/NULL);
		return;
	}

IL_0037:
	{
		ByteU5BU5D_t4260760469* L_9 = ___data0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_9);
		int64_t L_10 = Array_get_LongLength_m1209728916((Il2CppArray *)(Il2CppArray *)L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		int64_t L_11 = V_1;
		if ((((int64_t)L_11) > ((int64_t)(((int64_t)((int64_t)((int32_t)1016)))))))
		{
			goto IL_0082;
		}
	}
	{
		int64_t L_12 = V_1;
		G_B6_0 = 2;
		G_B6_1 = __this;
		if ((((int64_t)L_12) <= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			G_B9_0 = 2;
			G_B9_1 = __this;
			goto IL_0076;
		}
	}
	{
		bool L_13 = __this->get__client_6();
		G_B7_0 = G_B6_0;
		G_B7_1 = G_B6_1;
		if (!L_13)
		{
			G_B9_0 = G_B6_0;
			G_B9_1 = G_B6_1;
			goto IL_0076;
		}
	}
	{
		uint8_t L_14 = __this->get__compression_8();
		G_B8_0 = G_B7_0;
		G_B8_1 = G_B7_1;
		if (L_14)
		{
			G_B9_0 = G_B7_0;
			G_B9_1 = G_B7_1;
			goto IL_0076;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_15 = ___data0;
		int64_t L_16 = V_1;
		ByteU5BU5D_t4260760469* L_17 = Ext_Copy_TisByte_t2862609660_m4188581816(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/Ext_Copy_TisByte_t2862609660_m4188581816_MethodInfo_var);
		G_B10_0 = L_17;
		G_B10_1 = G_B8_0;
		G_B10_2 = G_B8_1;
		goto IL_0077;
	}

IL_0076:
	{
		ByteU5BU5D_t4260760469* L_18 = ___data0;
		G_B10_0 = L_18;
		G_B10_1 = G_B9_0;
		G_B10_2 = G_B9_1;
	}

IL_0077:
	{
		Action_1_t872614854 * L_19 = ___completed1;
		NullCheck(G_B10_2);
		WebSocket_sendAsync_m2588890200(G_B10_2, G_B10_1, G_B10_0, L_19, /*hidden argument*/NULL);
		goto IL_0090;
	}

IL_0082:
	{
		ByteU5BU5D_t4260760469* L_20 = ___data0;
		MemoryStream_t418716369 * L_21 = (MemoryStream_t418716369 *)il2cpp_codegen_object_new(MemoryStream_t418716369_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1231145921(L_21, L_20, /*hidden argument*/NULL);
		Action_1_t872614854 * L_22 = ___completed1;
		WebSocket_sendAsync_m1251110818(__this, 2, L_21, L_22, /*hidden argument*/NULL);
	}

IL_0090:
	{
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::SendAsync(System.IO.FileInfo,System.Action`1<System.Boolean>)
extern "C"  void WebSocket_SendAsync_m1663627723 (WebSocket_t1342580397 * __this, FileInfo_t3233670074 * ___file0, Action_1_t872614854 * ___completed1, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	{
		uint16_t L_0 = __this->get__readyState_27();
		il2cpp_codegen_memory_barrier();
		String_t* L_1 = Ext_CheckIfOpen_m3949729038(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		String_t* L_2 = L_1;
		G_B1_0 = L_2;
		if (L_2)
		{
			G_B2_0 = L_2;
			goto IL_001a;
		}
	}
	{
		FileInfo_t3233670074 * L_3 = ___file0;
		String_t* L_4 = Ext_CheckIfValidSendData_m1906594648(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		G_B2_0 = L_4;
	}

IL_001a:
	{
		V_0 = G_B2_0;
		String_t* L_5 = V_0;
		if (!L_5)
		{
			goto IL_0037;
		}
	}
	{
		Logger_t3695440972 * L_6 = __this->get__logger_19();
		il2cpp_codegen_memory_barrier();
		String_t* L_7 = V_0;
		NullCheck(L_6);
		Logger_Error_m3981980524(L_6, L_7, /*hidden argument*/NULL);
		String_t* L_8 = V_0;
		WebSocket_error_m149167037(__this, L_8, /*hidden argument*/NULL);
		return;
	}

IL_0037:
	{
		FileInfo_t3233670074 * L_9 = ___file0;
		NullCheck(L_9);
		FileStream_t2141505868 * L_10 = FileInfo_OpenRead_m2980452971(L_9, /*hidden argument*/NULL);
		Action_1_t872614854 * L_11 = ___completed1;
		WebSocket_sendAsync_m1251110818(__this, 2, L_10, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::SendAsync(System.String,System.Action`1<System.Boolean>)
extern Il2CppClass* Encoding_t2012439129_il2cpp_TypeInfo_var;
extern Il2CppClass* MemoryStream_t418716369_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_SendAsync_m2295781118_MetadataUsageId;
extern "C"  void WebSocket_SendAsync_m2295781118 (WebSocket_t1342580397 * __this, String_t* ___data0, Action_1_t872614854 * ___completed1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_SendAsync_m2295781118_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	ByteU5BU5D_t4260760469* V_1 = NULL;
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	{
		uint16_t L_0 = __this->get__readyState_27();
		il2cpp_codegen_memory_barrier();
		String_t* L_1 = Ext_CheckIfOpen_m3949729038(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		String_t* L_2 = L_1;
		G_B1_0 = L_2;
		if (L_2)
		{
			G_B2_0 = L_2;
			goto IL_001a;
		}
	}
	{
		String_t* L_3 = ___data0;
		String_t* L_4 = Ext_CheckIfValidSendData_m2561053783(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		G_B2_0 = L_4;
	}

IL_001a:
	{
		V_0 = G_B2_0;
		String_t* L_5 = V_0;
		if (!L_5)
		{
			goto IL_0037;
		}
	}
	{
		Logger_t3695440972 * L_6 = __this->get__logger_19();
		il2cpp_codegen_memory_barrier();
		String_t* L_7 = V_0;
		NullCheck(L_6);
		Logger_Error_m3981980524(L_6, L_7, /*hidden argument*/NULL);
		String_t* L_8 = V_0;
		WebSocket_error_m149167037(__this, L_8, /*hidden argument*/NULL);
		return;
	}

IL_0037:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t2012439129_il2cpp_TypeInfo_var);
		Encoding_t2012439129 * L_9 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_10 = ___data0;
		NullCheck(L_9);
		ByteU5BU5D_t4260760469* L_11 = VirtFuncInvoker1< ByteU5BU5D_t4260760469*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_9, L_10);
		V_1 = L_11;
		ByteU5BU5D_t4260760469* L_12 = V_1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_12);
		int64_t L_13 = Array_get_LongLength_m1209728916((Il2CppArray *)(Il2CppArray *)L_12, /*hidden argument*/NULL);
		if ((((int64_t)L_13) > ((int64_t)(((int64_t)((int64_t)((int32_t)1016)))))))
		{
			goto IL_0062;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_14 = V_1;
		Action_1_t872614854 * L_15 = ___completed1;
		WebSocket_sendAsync_m2588890200(__this, 1, L_14, L_15, /*hidden argument*/NULL);
		goto IL_0070;
	}

IL_0062:
	{
		ByteU5BU5D_t4260760469* L_16 = V_1;
		MemoryStream_t418716369 * L_17 = (MemoryStream_t418716369 *)il2cpp_codegen_object_new(MemoryStream_t418716369_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1231145921(L_17, L_16, /*hidden argument*/NULL);
		Action_1_t872614854 * L_18 = ___completed1;
		WebSocket_sendAsync_m1251110818(__this, 1, L_17, L_18, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::SendAsync(System.IO.Stream,System.Int32,System.Action`1<System.Boolean>)
extern Il2CppClass* U3CSendAsyncU3Ec__AnonStorey2E_t2907890439_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t361609309_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t92447661_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CSendAsyncU3Ec__AnonStorey2E_U3CU3Em__15_m1497102392_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2858737810_MethodInfo_var;
extern const MethodInfo* U3CSendAsyncU3Ec__AnonStorey2E_U3CU3Em__16_m2671865762_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3857796591_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3421947435;
extern const uint32_t WebSocket_SendAsync_m172934556_MetadataUsageId;
extern "C"  void WebSocket_SendAsync_m172934556 (WebSocket_t1342580397 * __this, Stream_t1561764144 * ___stream0, int32_t ___length1, Action_1_t872614854 * ___completed2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_SendAsync_m172934556_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CSendAsyncU3Ec__AnonStorey2E_t2907890439 * V_0 = NULL;
	String_t* G_B5_0 = NULL;
	U3CSendAsyncU3Ec__AnonStorey2E_t2907890439 * G_B5_1 = NULL;
	String_t* G_B1_0 = NULL;
	U3CSendAsyncU3Ec__AnonStorey2E_t2907890439 * G_B1_1 = NULL;
	String_t* G_B2_0 = NULL;
	U3CSendAsyncU3Ec__AnonStorey2E_t2907890439 * G_B2_1 = NULL;
	U3CSendAsyncU3Ec__AnonStorey2E_t2907890439 * G_B4_0 = NULL;
	U3CSendAsyncU3Ec__AnonStorey2E_t2907890439 * G_B3_0 = NULL;
	{
		U3CSendAsyncU3Ec__AnonStorey2E_t2907890439 * L_0 = (U3CSendAsyncU3Ec__AnonStorey2E_t2907890439 *)il2cpp_codegen_object_new(U3CSendAsyncU3Ec__AnonStorey2E_t2907890439_il2cpp_TypeInfo_var);
		U3CSendAsyncU3Ec__AnonStorey2E__ctor_m1765214276(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CSendAsyncU3Ec__AnonStorey2E_t2907890439 * L_1 = V_0;
		int32_t L_2 = ___length1;
		NullCheck(L_1);
		L_1->set_length_1(L_2);
		U3CSendAsyncU3Ec__AnonStorey2E_t2907890439 * L_3 = V_0;
		Action_1_t872614854 * L_4 = ___completed2;
		NullCheck(L_3);
		L_3->set_completed_2(L_4);
		U3CSendAsyncU3Ec__AnonStorey2E_t2907890439 * L_5 = V_0;
		NullCheck(L_5);
		L_5->set_U3CU3Ef__this_3(__this);
		U3CSendAsyncU3Ec__AnonStorey2E_t2907890439 * L_6 = V_0;
		uint16_t L_7 = __this->get__readyState_27();
		il2cpp_codegen_memory_barrier();
		String_t* L_8 = Ext_CheckIfOpen_m3949729038(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		String_t* L_9 = L_8;
		G_B1_0 = L_9;
		G_B1_1 = L_6;
		if (L_9)
		{
			G_B5_0 = L_9;
			G_B5_1 = L_6;
			goto IL_0054;
		}
	}
	{
		Stream_t1561764144 * L_10 = ___stream0;
		String_t* L_11 = Ext_CheckIfCanRead_m1870979642(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		String_t* L_12 = L_11;
		G_B2_0 = L_12;
		G_B2_1 = G_B1_1;
		if (L_12)
		{
			G_B5_0 = L_12;
			G_B5_1 = G_B1_1;
			goto IL_0054;
		}
	}
	{
		U3CSendAsyncU3Ec__AnonStorey2E_t2907890439 * L_13 = V_0;
		NullCheck(L_13);
		int32_t L_14 = L_13->get_length_1();
		G_B3_0 = G_B2_1;
		if ((((int32_t)L_14) >= ((int32_t)1)))
		{
			G_B4_0 = G_B2_1;
			goto IL_0053;
		}
	}
	{
		G_B5_0 = _stringLiteral3421947435;
		G_B5_1 = G_B3_0;
		goto IL_0054;
	}

IL_0053:
	{
		G_B5_0 = ((String_t*)(NULL));
		G_B5_1 = G_B4_0;
	}

IL_0054:
	{
		NullCheck(G_B5_1);
		G_B5_1->set_msg_0(G_B5_0);
		U3CSendAsyncU3Ec__AnonStorey2E_t2907890439 * L_15 = V_0;
		NullCheck(L_15);
		String_t* L_16 = L_15->get_msg_0();
		if (!L_16)
		{
			goto IL_0084;
		}
	}
	{
		Logger_t3695440972 * L_17 = __this->get__logger_19();
		il2cpp_codegen_memory_barrier();
		U3CSendAsyncU3Ec__AnonStorey2E_t2907890439 * L_18 = V_0;
		NullCheck(L_18);
		String_t* L_19 = L_18->get_msg_0();
		NullCheck(L_17);
		Logger_Error_m3981980524(L_17, L_19, /*hidden argument*/NULL);
		U3CSendAsyncU3Ec__AnonStorey2E_t2907890439 * L_20 = V_0;
		NullCheck(L_20);
		String_t* L_21 = L_20->get_msg_0();
		WebSocket_error_m149167037(__this, L_21, /*hidden argument*/NULL);
		return;
	}

IL_0084:
	{
		Stream_t1561764144 * L_22 = ___stream0;
		U3CSendAsyncU3Ec__AnonStorey2E_t2907890439 * L_23 = V_0;
		NullCheck(L_23);
		int32_t L_24 = L_23->get_length_1();
		U3CSendAsyncU3Ec__AnonStorey2E_t2907890439 * L_25 = V_0;
		IntPtr_t L_26;
		L_26.set_m_value_0((void*)(void*)U3CSendAsyncU3Ec__AnonStorey2E_U3CU3Em__15_m1497102392_MethodInfo_var);
		Action_1_t361609309 * L_27 = (Action_1_t361609309 *)il2cpp_codegen_object_new(Action_1_t361609309_il2cpp_TypeInfo_var);
		Action_1__ctor_m2858737810(L_27, L_25, L_26, /*hidden argument*/Action_1__ctor_m2858737810_MethodInfo_var);
		U3CSendAsyncU3Ec__AnonStorey2E_t2907890439 * L_28 = V_0;
		IntPtr_t L_29;
		L_29.set_m_value_0((void*)(void*)U3CSendAsyncU3Ec__AnonStorey2E_U3CU3Em__16_m2671865762_MethodInfo_var);
		Action_1_t92447661 * L_30 = (Action_1_t92447661 *)il2cpp_codegen_object_new(Action_1_t92447661_il2cpp_TypeInfo_var);
		Action_1__ctor_m3857796591(L_30, L_28, L_29, /*hidden argument*/Action_1__ctor_m3857796591_MethodInfo_var);
		Ext_ReadBytesAsync_m2558951989(NULL /*static, unused*/, L_22, L_24, L_27, L_30, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::SetCookie(WebSocketSharp.Net.Cookie)
extern Il2CppCodeGenString* _stringLiteral2130608454;
extern Il2CppCodeGenString* _stringLiteral1934580202;
extern const uint32_t WebSocket_SetCookie_m3263691247_MetadataUsageId;
extern "C"  void WebSocket_SetCookie_m3263691247 (WebSocket_t1342580397 * __this, Cookie_t2077085446 * ___cookie0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_SetCookie_m3263691247_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	String_t* V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	String_t* G_B5_0 = NULL;
	String_t* G_B2_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get__forConn_14();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_2 = WebSocket_checkIfAvailable_m3972221078(__this, _stringLiteral2130608454, (bool)0, (bool)0, /*hidden argument*/NULL);
			String_t* L_3 = L_2;
			G_B2_0 = L_3;
			if (L_3)
			{
				G_B5_0 = L_3;
				goto IL_0032;
			}
		}

IL_0020:
		{
			Cookie_t2077085446 * L_4 = ___cookie0;
			if (L_4)
			{
				goto IL_0031;
			}
		}

IL_0027:
		{
			G_B5_0 = _stringLiteral1934580202;
			goto IL_0032;
		}

IL_0031:
		{
			G_B5_0 = ((String_t*)(NULL));
		}

IL_0032:
		{
			V_1 = G_B5_0;
			String_t* L_5 = V_1;
			if (!L_5)
			{
				goto IL_0053;
			}
		}

IL_0039:
		{
			Logger_t3695440972 * L_6 = __this->get__logger_19();
			il2cpp_codegen_memory_barrier();
			String_t* L_7 = V_1;
			NullCheck(L_6);
			Logger_Error_m3981980524(L_6, L_7, /*hidden argument*/NULL);
			String_t* L_8 = V_1;
			WebSocket_error_m149167037(__this, L_8, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x89, FINALLY_0082);
		}

IL_0053:
		{
			CookieCollection_t1136277956 * L_9 = __this->get__cookies_10();
			NullCheck(L_9);
			Il2CppObject * L_10 = CookieCollection_get_SyncRoot_m1445844552(L_9, /*hidden argument*/NULL);
			V_2 = L_10;
			Il2CppObject * L_11 = V_2;
			Monitor_Enter_m476686225(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		}

IL_0065:
		try
		{ // begin try (depth: 2)
			CookieCollection_t1136277956 * L_12 = __this->get__cookies_10();
			Cookie_t2077085446 * L_13 = ___cookie0;
			NullCheck(L_12);
			CookieCollection_SetOrRemove_m2794184222(L_12, L_13, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x7D, FINALLY_0076);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
			goto FINALLY_0076;
		}

FINALLY_0076:
		{ // begin finally (depth: 2)
			Il2CppObject * L_14 = V_2;
			Monitor_Exit_m2088237919(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(118)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(118)
		{
			IL2CPP_JUMP_TBL(0x7D, IL_007d)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
		}

IL_007d:
		{
			IL2CPP_LEAVE(0x89, FINALLY_0082);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0082;
	}

FINALLY_0082:
	{ // begin finally (depth: 1)
		Il2CppObject * L_15 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(130)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(130)
	{
		IL2CPP_JUMP_TBL(0x89, IL_0089)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0089:
	{
		return;
	}
}
// System.Void WebSocketSharp.WebSocket::SetCredentials(System.String,System.String,System.Boolean)
extern Il2CppClass* CharU5BU5D_t3324145743_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* NetworkCredential_t1204099087_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1169926490;
extern Il2CppCodeGenString* _stringLiteral2976356929;
extern Il2CppCodeGenString* _stringLiteral4052169878;
extern Il2CppCodeGenString* _stringLiteral3957789275;
extern const uint32_t WebSocket_SetCredentials_m4000006718_MetadataUsageId;
extern "C"  void WebSocket_SetCredentials_m4000006718 (WebSocket_t1342580397 * __this, String_t* ___username0, String_t* ___password1, bool ___preAuth2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_SetCredentials_m4000006718_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	String_t* V_1 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	String_t* G_B11_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get__forConn_14();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_2 = WebSocket_checkIfAvailable_m3972221078(__this, _stringLiteral1169926490, (bool)0, (bool)0, /*hidden argument*/NULL);
			V_1 = L_2;
			String_t* L_3 = V_1;
			if (L_3)
			{
				goto IL_009e;
			}
		}

IL_0021:
		{
			String_t* L_4 = ___username0;
			bool L_5 = Ext_IsNullOrEmpty_m3465800922(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
			if (!L_5)
			{
				goto IL_0051;
			}
		}

IL_002c:
		{
			__this->set__credentials_11((NetworkCredential_t1204099087 *)NULL);
			__this->set__preAuth_24((bool)0);
			Logger_t3695440972 * L_6 = __this->get__logger_19();
			il2cpp_codegen_memory_barrier();
			NullCheck(L_6);
			Logger_Warn_m1570757416(L_6, _stringLiteral2976356929, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0xEF, FINALLY_00e8);
		}

IL_0051:
		{
			String_t* L_7 = ___username0;
			CharU5BU5D_t3324145743* L_8 = ((CharU5BU5D_t3324145743*)SZArrayNew(CharU5BU5D_t3324145743_il2cpp_TypeInfo_var, (uint32_t)1));
			NullCheck(L_8);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
			(L_8)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)58));
			bool L_9 = Ext_Contains_m664592357(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
			if (L_9)
			{
				goto IL_0072;
			}
		}

IL_0067:
		{
			String_t* L_10 = ___username0;
			bool L_11 = Ext_IsText_m2931158296(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
			if (L_11)
			{
				goto IL_007c;
			}
		}

IL_0072:
		{
			G_B11_0 = _stringLiteral4052169878;
			goto IL_009d;
		}

IL_007c:
		{
			String_t* L_12 = ___password1;
			bool L_13 = Ext_IsNullOrEmpty_m3465800922(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
			if (L_13)
			{
				goto IL_009c;
			}
		}

IL_0087:
		{
			String_t* L_14 = ___password1;
			bool L_15 = Ext_IsText_m2931158296(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
			if (L_15)
			{
				goto IL_009c;
			}
		}

IL_0092:
		{
			G_B11_0 = _stringLiteral3957789275;
			goto IL_009d;
		}

IL_009c:
		{
			G_B11_0 = ((String_t*)(NULL));
		}

IL_009d:
		{
			V_1 = G_B11_0;
		}

IL_009e:
		{
			String_t* L_16 = V_1;
			if (!L_16)
			{
				goto IL_00be;
			}
		}

IL_00a4:
		{
			Logger_t3695440972 * L_17 = __this->get__logger_19();
			il2cpp_codegen_memory_barrier();
			String_t* L_18 = V_1;
			NullCheck(L_17);
			Logger_Error_m3981980524(L_17, L_18, /*hidden argument*/NULL);
			String_t* L_19 = V_1;
			WebSocket_error_m149167037(__this, L_19, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0xEF, FINALLY_00e8);
		}

IL_00be:
		{
			String_t* L_20 = ___username0;
			String_t* L_21 = ___password1;
			Uri_t1116831938 * L_22 = __this->get__uri_32();
			NullCheck(L_22);
			String_t* L_23 = Uri_get_PathAndQuery_m3621173943(L_22, /*hidden argument*/NULL);
			NetworkCredential_t1204099087 * L_24 = (NetworkCredential_t1204099087 *)il2cpp_codegen_object_new(NetworkCredential_t1204099087_il2cpp_TypeInfo_var);
			NetworkCredential__ctor_m332947170(L_24, L_20, L_21, L_23, ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
			__this->set__credentials_11(L_24);
			bool L_25 = ___preAuth2;
			__this->set__preAuth_24(L_25);
			IL2CPP_LEAVE(0xEF, FINALLY_00e8);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_00e8;
	}

FINALLY_00e8:
	{ // begin finally (depth: 1)
		Il2CppObject * L_26 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(232)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(232)
	{
		IL2CPP_JUMP_TBL(0xEF, IL_00ef)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_00ef:
	{
		return;
	}
}
// System.String WebSocketSharp.WebSocket::<get_CustomHandshakeRequestChecker>m__C(WebSocketSharp.Net.WebSockets.WebSocketContext)
extern "C"  String_t* WebSocket_U3Cget_CustomHandshakeRequestCheckerU3Em__C_m6268665 (Il2CppObject * __this /* static, unused */, WebSocketContext_t763725542 * ___context0, const MethodInfo* method)
{
	{
		return (String_t*)NULL;
	}
}
// System.Boolean WebSocketSharp.WebSocket::<acceptHandshake>m__D(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_U3CacceptHandshakeU3Em__D_m2493968471_MetadataUsageId;
extern "C"  bool WebSocket_U3CacceptHandshakeU3Em__D_m2493968471 (WebSocket_t1342580397 * __this, String_t* ___protocol0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_U3CacceptHandshakeU3Em__D_m2493968471_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___protocol0;
		String_t* L_1 = __this->get__protocol_25();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean WebSocketSharp.WebSocket::<validateSecWebSocketExtensionsHeader>m__12(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_U3CvalidateSecWebSocketExtensionsHeaderU3Em__12_m468975296_MetadataUsageId;
extern "C"  bool WebSocket_U3CvalidateSecWebSocketExtensionsHeaderU3Em__12_m468975296 (WebSocket_t1342580397 * __this, String_t* ___extension0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_U3CvalidateSecWebSocketExtensionsHeaderU3Em__12_m468975296_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___extension0;
		NullCheck(L_0);
		String_t* L_1 = String_Trim_m1030489823(L_0, /*hidden argument*/NULL);
		uint8_t L_2 = __this->get__compression_8();
		String_t* L_3 = Ext_ToExtensionString_m2448377977(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void WebSocketSharp.WebSocket/<>c__Iterator1F::.ctor()
extern "C"  void U3CU3Ec__Iterator1F__ctor_m799211274 (U3CU3Ec__Iterator1F_t90658481 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// WebSocketSharp.Net.Cookie WebSocketSharp.WebSocket/<>c__Iterator1F::System.Collections.Generic.IEnumerator<WebSocketSharp.Net.Cookie>.get_Current()
extern "C"  Cookie_t2077085446 * U3CU3Ec__Iterator1F_System_Collections_Generic_IEnumeratorU3CWebSocketSharp_Net_CookieU3E_get_Current_m3316606067 (U3CU3Ec__Iterator1F_t90658481 * __this, const MethodInfo* method)
{
	{
		Cookie_t2077085446 * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Object WebSocketSharp.WebSocket/<>c__Iterator1F::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CU3Ec__Iterator1F_System_Collections_IEnumerator_get_Current_m3517517734 (U3CU3Ec__Iterator1F_t90658481 * __this, const MethodInfo* method)
{
	{
		Cookie_t2077085446 * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Collections.IEnumerator WebSocketSharp.WebSocket/<>c__Iterator1F::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CU3Ec__Iterator1F_System_Collections_IEnumerable_GetEnumerator_m4202140327 (U3CU3Ec__Iterator1F_t90658481 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = U3CU3Ec__Iterator1F_System_Collections_Generic_IEnumerableU3CWebSocketSharp_Net_CookieU3E_GetEnumerator_m2683495812(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<WebSocketSharp.Net.Cookie> WebSocketSharp.WebSocket/<>c__Iterator1F::System.Collections.Generic.IEnumerable<WebSocketSharp.Net.Cookie>.GetEnumerator()
extern Il2CppClass* U3CU3Ec__Iterator1F_t90658481_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec__Iterator1F_System_Collections_Generic_IEnumerableU3CWebSocketSharp_Net_CookieU3E_GetEnumerator_m2683495812_MetadataUsageId;
extern "C"  Il2CppObject* U3CU3Ec__Iterator1F_System_Collections_Generic_IEnumerableU3CWebSocketSharp_Net_CookieU3E_GetEnumerator_m2683495812 (U3CU3Ec__Iterator1F_t90658481 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__Iterator1F_System_Collections_Generic_IEnumerableU3CWebSocketSharp_Net_CookieU3E_GetEnumerator_m2683495812_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__Iterator1F_t90658481 * V_0 = NULL;
	{
		int32_t* L_0 = __this->get_address_of_U24PC_3();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, L_0, 0, ((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CU3Ec__Iterator1F_t90658481 * L_2 = (U3CU3Ec__Iterator1F_t90658481 *)il2cpp_codegen_object_new(U3CU3Ec__Iterator1F_t90658481_il2cpp_TypeInfo_var);
		U3CU3Ec__Iterator1F__ctor_m799211274(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		U3CU3Ec__Iterator1F_t90658481 * L_3 = V_0;
		WebSocket_t1342580397 * L_4 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_5(L_4);
		U3CU3Ec__Iterator1F_t90658481 * L_5 = V_0;
		return L_5;
	}
}
// System.Boolean WebSocketSharp.WebSocket/<>c__Iterator1F::MoveNext()
extern Il2CppClass* IEnumerator_t3464575207_il2cpp_TypeInfo_var;
extern Il2CppClass* Cookie_t2077085446_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec__Iterator1F_MoveNext_m964271762_MetadataUsageId;
extern "C"  bool U3CU3Ec__Iterator1F_MoveNext_m964271762 (U3CU3Ec__Iterator1F_t90658481 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__Iterator1F_MoveNext_m964271762_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	Il2CppObject * V_2 = NULL;
	bool V_3 = false;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_0047;
		}
	}
	{
		goto IL_00f9;
	}

IL_0023:
	{
		WebSocket_t1342580397 * L_2 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_2);
		CookieCollection_t1136277956 * L_3 = L_2->get__cookies_10();
		NullCheck(L_3);
		Il2CppObject * L_4 = CookieCollection_get_SyncRoot_m1445844552(L_3, /*hidden argument*/NULL);
		__this->set_U3CU24s_146U3E__2_2(L_4);
		Il2CppObject * L_5 = __this->get_U3CU24s_146U3E__2_2();
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_0 = ((int32_t)-3);
	}

IL_0047:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_6 = V_0;
			if (((int32_t)((int32_t)L_6-(int32_t)1)) == 0)
			{
				goto IL_006c;
			}
		}

IL_0053:
		{
			WebSocket_t1342580397 * L_7 = __this->get_U3CU3Ef__this_5();
			NullCheck(L_7);
			CookieCollection_t1136277956 * L_8 = L_7->get__cookies_10();
			NullCheck(L_8);
			Il2CppObject * L_9 = CookieCollection_GetEnumerator_m3330523789(L_8, /*hidden argument*/NULL);
			__this->set_U3CU24s_145U3E__0_0(L_9);
			V_0 = ((int32_t)-3);
		}

IL_006c:
		try
		{ // begin try (depth: 2)
			{
				uint32_t L_10 = V_0;
				if (((int32_t)((int32_t)L_10-(int32_t)1)) == 0)
				{
					goto IL_00ad;
				}
			}

IL_0078:
			{
				goto IL_00ad;
			}

IL_007d:
			{
				Il2CppObject * L_11 = __this->get_U3CU24s_145U3E__0_0();
				NullCheck(L_11);
				Il2CppObject * L_12 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_11);
				__this->set_U3CcookieU3E__1_1(((Cookie_t2077085446 *)CastclassSealed(L_12, Cookie_t2077085446_il2cpp_TypeInfo_var)));
				Cookie_t2077085446 * L_13 = __this->get_U3CcookieU3E__1_1();
				__this->set_U24current_4(L_13);
				__this->set_U24PC_3(1);
				V_1 = (bool)1;
				IL2CPP_LEAVE(0xFB, FINALLY_00c2);
			}

IL_00ad:
			{
				Il2CppObject * L_14 = __this->get_U3CU24s_145U3E__0_0();
				NullCheck(L_14);
				bool L_15 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t3464575207_il2cpp_TypeInfo_var, L_14);
				if (L_15)
				{
					goto IL_007d;
				}
			}

IL_00bd:
			{
				IL2CPP_LEAVE(0xDD, FINALLY_00c2);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
			goto FINALLY_00c2;
		}

FINALLY_00c2:
		{ // begin finally (depth: 2)
			{
				bool L_16 = V_1;
				if (!L_16)
				{
					goto IL_00c6;
				}
			}

IL_00c5:
			{
				IL2CPP_END_FINALLY(194)
			}

IL_00c6:
			{
				Il2CppObject * L_17 = __this->get_U3CU24s_145U3E__0_0();
				V_2 = ((Il2CppObject *)IsInst(L_17, IDisposable_t1423340799_il2cpp_TypeInfo_var));
				Il2CppObject * L_18 = V_2;
				if (L_18)
				{
					goto IL_00d6;
				}
			}

IL_00d5:
			{
				IL2CPP_END_FINALLY(194)
			}

IL_00d6:
			{
				Il2CppObject * L_19 = V_2;
				NullCheck(L_19);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_19);
				IL2CPP_END_FINALLY(194)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(194)
		{
			IL2CPP_END_CLEANUP(0xFB, FINALLY_00e2);
			IL2CPP_JUMP_TBL(0xDD, IL_00dd)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
		}

IL_00dd:
		{
			IL2CPP_LEAVE(0xF2, FINALLY_00e2);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_00e2;
	}

FINALLY_00e2:
	{ // begin finally (depth: 1)
		{
			bool L_20 = V_1;
			if (!L_20)
			{
				goto IL_00e6;
			}
		}

IL_00e5:
		{
			IL2CPP_END_FINALLY(226)
		}

IL_00e6:
		{
			Il2CppObject * L_21 = __this->get_U3CU24s_146U3E__2_2();
			Monitor_Exit_m2088237919(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(226)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(226)
	{
		IL2CPP_JUMP_TBL(0xFB, IL_00fb)
		IL2CPP_JUMP_TBL(0xF2, IL_00f2)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_00f2:
	{
		__this->set_U24PC_3((-1));
	}

IL_00f9:
	{
		return (bool)0;
	}

IL_00fb:
	{
		return (bool)1;
	}
	// Dead block : IL_00fd: ldloc.3
}
// System.Void WebSocketSharp.WebSocket/<>c__Iterator1F::Dispose()
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec__Iterator1F_Dispose_m3436138695_MetadataUsageId;
extern "C"  void U3CU3Ec__Iterator1F_Dispose_m3436138695 (U3CU3Ec__Iterator1F_t90658481 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__Iterator1F_Dispose_m3436138695_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_004e;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_004e;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			IL2CPP_LEAVE(0x3D, FINALLY_0026);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
			goto FINALLY_0026;
		}

FINALLY_0026:
		{ // begin finally (depth: 2)
			{
				Il2CppObject * L_2 = __this->get_U3CU24s_145U3E__0_0();
				V_1 = ((Il2CppObject *)IsInst(L_2, IDisposable_t1423340799_il2cpp_TypeInfo_var));
				Il2CppObject * L_3 = V_1;
				if (L_3)
				{
					goto IL_0036;
				}
			}

IL_0035:
			{
				IL2CPP_END_FINALLY(38)
			}

IL_0036:
			{
				Il2CppObject * L_4 = V_1;
				NullCheck(L_4);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_4);
				IL2CPP_END_FINALLY(38)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(38)
		{
			IL2CPP_JUMP_TBL(0x3D, IL_003d)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
		}

IL_003d:
		{
			IL2CPP_LEAVE(0x4E, FINALLY_0042);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0042;
	}

FINALLY_0042:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = __this->get_U3CU24s_146U3E__2_2();
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(66)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(66)
	{
		IL2CPP_JUMP_TBL(0x4E, IL_004e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_004e:
	{
		return;
	}
}
// System.Void WebSocketSharp.WebSocket/<>c__Iterator1F::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CU3Ec__Iterator1F_Reset_m2740611511_MetadataUsageId;
extern "C"  void U3CU3Ec__Iterator1F_Reset_m2740611511 (U3CU3Ec__Iterator1F_t90658481 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__Iterator1F_Reset_m2740611511_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void WebSocketSharp.WebSocket/<closeAsync>c__AnonStorey28::.ctor()
extern "C"  void U3CcloseAsyncU3Ec__AnonStorey28__ctor_m55241471 (U3CcloseAsyncU3Ec__AnonStorey28_t239095580 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket/<closeAsync>c__AnonStorey28::<>m__E(System.IAsyncResult)
extern const MethodInfo* Action_3_EndInvoke_m2416362020_MethodInfo_var;
extern const uint32_t U3CcloseAsyncU3Ec__AnonStorey28_U3CU3Em__E_m3722936676_MetadataUsageId;
extern "C"  void U3CcloseAsyncU3Ec__AnonStorey28_U3CU3Em__E_m3722936676 (U3CcloseAsyncU3Ec__AnonStorey28_t239095580 * __this, Il2CppObject * ___ar0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CcloseAsyncU3Ec__AnonStorey28_U3CU3Em__E_m3722936676_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_3_t2371519456 * L_0 = __this->get_closer_0();
		Il2CppObject * L_1 = ___ar0;
		NullCheck(L_0);
		Action_3_EndInvoke_m2416362020(L_0, L_1, /*hidden argument*/Action_3_EndInvoke_m2416362020_MethodInfo_var);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket/<ConnectAsync>c__AnonStorey2D::.ctor()
extern "C"  void U3CConnectAsyncU3Ec__AnonStorey2D__ctor_m2906769413 (U3CConnectAsyncU3Ec__AnonStorey2D_t4104301910 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket/<ConnectAsync>c__AnonStorey2D::<>m__14(System.IAsyncResult)
extern const MethodInfo* Func_1_EndInvoke_m1958676070_MethodInfo_var;
extern const uint32_t U3CConnectAsyncU3Ec__AnonStorey2D_U3CU3Em__14_m19811154_MetadataUsageId;
extern "C"  void U3CConnectAsyncU3Ec__AnonStorey2D_U3CU3Em__14_m19811154 (U3CConnectAsyncU3Ec__AnonStorey2D_t4104301910 * __this, Il2CppObject * ___ar0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CConnectAsyncU3Ec__AnonStorey2D_U3CU3Em__14_m19811154_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Func_1_t1601960292 * L_0 = __this->get_connector_0();
		Il2CppObject * L_1 = ___ar0;
		NullCheck(L_0);
		bool L_2 = Func_1_EndInvoke_m1958676070(L_0, L_1, /*hidden argument*/Func_1_EndInvoke_m1958676070_MethodInfo_var);
		if (!L_2)
		{
			goto IL_001c;
		}
	}
	{
		WebSocket_t1342580397 * L_3 = __this->get_U3CU3Ef__this_1();
		NullCheck(L_3);
		WebSocket_open_m3051031279(L_3, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void WebSocketSharp.WebSocket/<sendAsync>c__AnonStorey29::.ctor()
extern "C"  void U3CsendAsyncU3Ec__AnonStorey29__ctor_m2579381936 (U3CsendAsyncU3Ec__AnonStorey29_t861700891 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket/<sendAsync>c__AnonStorey29::<>m__F(System.IAsyncResult)
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern const MethodInfo* Func_3_EndInvoke_m1073281076_MethodInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3527665033;
extern const uint32_t U3CsendAsyncU3Ec__AnonStorey29_U3CU3Em__F_m2428161332_MetadataUsageId;
extern "C"  void U3CsendAsyncU3Ec__AnonStorey29_U3CU3Em__F_m2428161332 (U3CsendAsyncU3Ec__AnonStorey29_t861700891 * __this, Il2CppObject * ___ar0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CsendAsyncU3Ec__AnonStorey29_U3CU3Em__F_m2428161332_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t3991598821 * V_1 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Func_3_t2548063815 * L_0 = __this->get_sender_0();
			Il2CppObject * L_1 = ___ar0;
			NullCheck(L_0);
			bool L_2 = Func_3_EndInvoke_m1073281076(L_0, L_1, /*hidden argument*/Func_3_EndInvoke_m1073281076_MethodInfo_var);
			V_0 = L_2;
			Action_1_t872614854 * L_3 = __this->get_completed_1();
			if (!L_3)
			{
				goto IL_0024;
			}
		}

IL_0018:
		{
			Action_1_t872614854 * L_4 = __this->get_completed_1();
			bool L_5 = V_0;
			NullCheck(L_4);
			Action_1_Invoke_m3594021162(L_4, L_5, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
		}

IL_0024:
		{
			goto IL_0057;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0029;
		throw e;
	}

CATCH_0029:
	{ // begin catch(System.Exception)
		V_1 = ((Exception_t3991598821 *)__exception_local);
		WebSocket_t1342580397 * L_6 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_6);
		Logger_t3695440972 * L_7 = L_6->get__logger_19();
		il2cpp_codegen_memory_barrier();
		Exception_t3991598821 * L_8 = V_1;
		NullCheck(L_8);
		String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Exception::ToString() */, L_8);
		NullCheck(L_7);
		Logger_Fatal_m51902704(L_7, L_9, /*hidden argument*/NULL);
		WebSocket_t1342580397 * L_10 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_10);
		WebSocket_error_m149167037(L_10, _stringLiteral3527665033, /*hidden argument*/NULL);
		goto IL_0057;
	} // end catch (depth: 1)

IL_0057:
	{
		return;
	}
}
// System.Void WebSocketSharp.WebSocket/<sendAsync>c__AnonStorey2A::.ctor()
extern "C"  void U3CsendAsyncU3Ec__AnonStorey2A__ctor_m1007273896 (U3CsendAsyncU3Ec__AnonStorey2A_t861700899 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket/<sendAsync>c__AnonStorey2A::<>m__10(System.IAsyncResult)
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern const MethodInfo* Func_3_EndInvoke_m3777894830_MethodInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3527665033;
extern const uint32_t U3CsendAsyncU3Ec__AnonStorey2A_U3CU3Em__10_m112891155_MetadataUsageId;
extern "C"  void U3CsendAsyncU3Ec__AnonStorey2A_U3CU3Em__10_m112891155 (U3CsendAsyncU3Ec__AnonStorey2A_t861700899 * __this, Il2CppObject * ___ar0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CsendAsyncU3Ec__AnonStorey2A_U3CU3Em__10_m112891155_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t3991598821 * V_1 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Func_3_t2764730336 * L_0 = __this->get_sender_0();
			Il2CppObject * L_1 = ___ar0;
			NullCheck(L_0);
			bool L_2 = Func_3_EndInvoke_m3777894830(L_0, L_1, /*hidden argument*/Func_3_EndInvoke_m3777894830_MethodInfo_var);
			V_0 = L_2;
			Action_1_t872614854 * L_3 = __this->get_completed_1();
			if (!L_3)
			{
				goto IL_0024;
			}
		}

IL_0018:
		{
			Action_1_t872614854 * L_4 = __this->get_completed_1();
			bool L_5 = V_0;
			NullCheck(L_4);
			Action_1_Invoke_m3594021162(L_4, L_5, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
		}

IL_0024:
		{
			goto IL_0057;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0029;
		throw e;
	}

CATCH_0029:
	{ // begin catch(System.Exception)
		V_1 = ((Exception_t3991598821 *)__exception_local);
		WebSocket_t1342580397 * L_6 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_6);
		Logger_t3695440972 * L_7 = L_6->get__logger_19();
		il2cpp_codegen_memory_barrier();
		Exception_t3991598821 * L_8 = V_1;
		NullCheck(L_8);
		String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Exception::ToString() */, L_8);
		NullCheck(L_7);
		Logger_Fatal_m51902704(L_7, L_9, /*hidden argument*/NULL);
		WebSocket_t1342580397 * L_10 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_10);
		WebSocket_error_m149167037(L_10, _stringLiteral3527665033, /*hidden argument*/NULL);
		goto IL_0057;
	} // end catch (depth: 1)

IL_0057:
	{
		return;
	}
}
// System.Void WebSocketSharp.WebSocket/<SendAsync>c__AnonStorey2E::.ctor()
extern "C"  void U3CSendAsyncU3Ec__AnonStorey2E__ctor_m1765214276 (U3CSendAsyncU3Ec__AnonStorey2E_t2907890439 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket/<SendAsync>c__AnonStorey2E::<>m__15(System.Byte[])
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* MemoryStream_t418716369_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3594021162_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral93075211;
extern Il2CppCodeGenString* _stringLiteral138456946;
extern const uint32_t U3CSendAsyncU3Ec__AnonStorey2E_U3CU3Em__15_m1497102392_MetadataUsageId;
extern "C"  void U3CSendAsyncU3Ec__AnonStorey2E_U3CU3Em__15_m1497102392 (U3CSendAsyncU3Ec__AnonStorey2E_t2907890439 * __this, ByteU5BU5D_t4260760469* ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CSendAsyncU3Ec__AnonStorey2E_U3CU3Em__15_m1497102392_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	bool G_B7_0 = false;
	{
		ByteU5BU5D_t4260760469* L_0 = ___data0;
		NullCheck(L_0);
		V_0 = (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
		int32_t L_1 = V_0;
		if (L_1)
		{
			goto IL_003f;
		}
	}
	{
		__this->set_msg_0(_stringLiteral93075211);
		WebSocket_t1342580397 * L_2 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		Logger_t3695440972 * L_3 = L_2->get__logger_19();
		il2cpp_codegen_memory_barrier();
		String_t* L_4 = __this->get_msg_0();
		NullCheck(L_3);
		Logger_Error_m3981980524(L_3, L_4, /*hidden argument*/NULL);
		WebSocket_t1342580397 * L_5 = __this->get_U3CU3Ef__this_3();
		String_t* L_6 = __this->get_msg_0();
		NullCheck(L_5);
		WebSocket_error_m149167037(L_5, L_6, /*hidden argument*/NULL);
		return;
	}

IL_003f:
	{
		int32_t L_7 = V_0;
		int32_t L_8 = __this->get_length_1();
		if ((((int32_t)L_7) >= ((int32_t)L_8)))
		{
			goto IL_0078;
		}
	}
	{
		WebSocket_t1342580397 * L_9 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_9);
		Logger_t3695440972 * L_10 = L_9->get__logger_19();
		il2cpp_codegen_memory_barrier();
		int32_t L_11 = __this->get_length_1();
		int32_t L_12 = L_11;
		Il2CppObject * L_13 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_12);
		int32_t L_14 = V_0;
		int32_t L_15 = L_14;
		Il2CppObject * L_16 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_15);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Format_m2398979370(NULL /*static, unused*/, _stringLiteral138456946, L_13, L_16, /*hidden argument*/NULL);
		NullCheck(L_10);
		Logger_Warn_m1570757416(L_10, L_17, /*hidden argument*/NULL);
	}

IL_0078:
	{
		int32_t L_18 = V_0;
		if ((((int32_t)L_18) > ((int32_t)((int32_t)1016))))
		{
			goto IL_0095;
		}
	}
	{
		WebSocket_t1342580397 * L_19 = __this->get_U3CU3Ef__this_3();
		ByteU5BU5D_t4260760469* L_20 = ___data0;
		NullCheck(L_19);
		bool L_21 = WebSocket_send_m2882270907(L_19, 2, L_20, /*hidden argument*/NULL);
		G_B7_0 = L_21;
		goto IL_00a7;
	}

IL_0095:
	{
		WebSocket_t1342580397 * L_22 = __this->get_U3CU3Ef__this_3();
		ByteU5BU5D_t4260760469* L_23 = ___data0;
		MemoryStream_t418716369 * L_24 = (MemoryStream_t418716369 *)il2cpp_codegen_object_new(MemoryStream_t418716369_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1231145921(L_24, L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		bool L_25 = WebSocket_send_m2473628261(L_22, 2, L_24, /*hidden argument*/NULL);
		G_B7_0 = L_25;
	}

IL_00a7:
	{
		V_1 = G_B7_0;
		Action_1_t872614854 * L_26 = __this->get_completed_2();
		if (!L_26)
		{
			goto IL_00bf;
		}
	}
	{
		Action_1_t872614854 * L_27 = __this->get_completed_2();
		bool L_28 = V_1;
		NullCheck(L_27);
		Action_1_Invoke_m3594021162(L_27, L_28, /*hidden argument*/Action_1_Invoke_m3594021162_MethodInfo_var);
	}

IL_00bf:
	{
		return;
	}
}
// System.Void WebSocketSharp.WebSocket/<SendAsync>c__AnonStorey2E::<>m__16(System.Exception)
extern Il2CppCodeGenString* _stringLiteral4246579071;
extern const uint32_t U3CSendAsyncU3Ec__AnonStorey2E_U3CU3Em__16_m2671865762_MetadataUsageId;
extern "C"  void U3CSendAsyncU3Ec__AnonStorey2E_U3CU3Em__16_m2671865762 (U3CSendAsyncU3Ec__AnonStorey2E_t2907890439 * __this, Exception_t3991598821 * ___ex0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CSendAsyncU3Ec__AnonStorey2E_U3CU3Em__16_m2671865762_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		WebSocket_t1342580397 * L_0 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_0);
		Logger_t3695440972 * L_1 = L_0->get__logger_19();
		il2cpp_codegen_memory_barrier();
		Exception_t3991598821 * L_2 = ___ex0;
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Exception::ToString() */, L_2);
		NullCheck(L_1);
		Logger_Fatal_m51902704(L_1, L_3, /*hidden argument*/NULL);
		WebSocket_t1342580397 * L_4 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_4);
		WebSocket_error_m149167037(L_4, _stringLiteral4246579071, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket/<startReceiving>c__AnonStorey2B::.ctor()
extern "C"  void U3CstartReceivingU3Ec__AnonStorey2B__ctor_m2023001787 (U3CstartReceivingU3Ec__AnonStorey2B_t802946016 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket/<startReceiving>c__AnonStorey2B::<>m__11()
extern Il2CppClass* Action_1_t1174010442_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t92447661_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CstartReceivingU3Ec__AnonStorey2B_U3CU3Em__17_m1383973847_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2876881354_MethodInfo_var;
extern const MethodInfo* U3CstartReceivingU3Ec__AnonStorey2B_U3CU3Em__18_m3591481819_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3857796591_MethodInfo_var;
extern const uint32_t U3CstartReceivingU3Ec__AnonStorey2B_U3CU3Em__11_m792573156_MetadataUsageId;
extern "C"  void U3CstartReceivingU3Ec__AnonStorey2B_U3CU3Em__11_m792573156 (U3CstartReceivingU3Ec__AnonStorey2B_t802946016 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CstartReceivingU3Ec__AnonStorey2B_U3CU3Em__11_m792573156_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		WebSocket_t1342580397 * L_0 = __this->get_U3CU3Ef__this_1();
		NullCheck(L_0);
		WebSocketStream_t4103435597 * L_1 = L_0->get__stream_30();
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)U3CstartReceivingU3Ec__AnonStorey2B_U3CU3Em__17_m1383973847_MethodInfo_var);
		Action_1_t1174010442 * L_3 = (Action_1_t1174010442 *)il2cpp_codegen_object_new(Action_1_t1174010442_il2cpp_TypeInfo_var);
		Action_1__ctor_m2876881354(L_3, __this, L_2, /*hidden argument*/Action_1__ctor_m2876881354_MethodInfo_var);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CstartReceivingU3Ec__AnonStorey2B_U3CU3Em__18_m3591481819_MethodInfo_var);
		Action_1_t92447661 * L_5 = (Action_1_t92447661 *)il2cpp_codegen_object_new(Action_1_t92447661_il2cpp_TypeInfo_var);
		Action_1__ctor_m3857796591(L_5, __this, L_4, /*hidden argument*/Action_1__ctor_m3857796591_MethodInfo_var);
		NullCheck(L_1);
		WebSocketStream_ReadFrameAsync_m3409431931(L_1, L_3, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket/<startReceiving>c__AnonStorey2B::<>m__17(WebSocketSharp.WebSocketFrame)
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern const MethodInfo* Ext_Emit_TisMessageEventArgs_t36945740_m2913779343_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4284785446;
extern const uint32_t U3CstartReceivingU3Ec__AnonStorey2B_U3CU3Em__17_m1383973847_MetadataUsageId;
extern "C"  void U3CstartReceivingU3Ec__AnonStorey2B_U3CU3Em__17_m1383973847 (U3CstartReceivingU3Ec__AnonStorey2B_t802946016 * __this, WebSocketFrame_t778194306 * ___frame0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CstartReceivingU3Ec__AnonStorey2B_U3CU3Em__17_m1383973847_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	MessageEventArgs_t36945740 * V_1 = NULL;
	Exception_t3991598821 * V_2 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		WebSocket_t1342580397 * L_0 = __this->get_U3CU3Ef__this_1();
		WebSocketFrame_t778194306 * L_1 = ___frame0;
		NullCheck(L_0);
		bool L_2 = WebSocket_acceptFrame_m511668371(L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00b6;
		}
	}
	{
		WebSocket_t1342580397 * L_3 = __this->get_U3CU3Ef__this_1();
		NullCheck(L_3);
		uint16_t L_4 = L_3->get__readyState_27();
		il2cpp_codegen_memory_barrier();
		if ((((int32_t)L_4) == ((int32_t)3)))
		{
			goto IL_00b6;
		}
	}
	{
		Action_t3771233898 * L_5 = __this->get_receive_0();
		NullCheck(L_5);
		Action_Invoke_m1445970038(L_5, /*hidden argument*/NULL);
		WebSocketFrame_t778194306 * L_6 = ___frame0;
		NullCheck(L_6);
		bool L_7 = WebSocketFrame_get_IsData_m2997444627(L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_003b;
		}
	}
	{
		return;
	}

IL_003b:
	{
		WebSocket_t1342580397 * L_8 = __this->get_U3CU3Ef__this_1();
		NullCheck(L_8);
		Il2CppObject * L_9 = L_8->get__forEvent_15();
		V_0 = L_9;
		Il2CppObject * L_10 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
	}

IL_004d:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			{
				WebSocket_t1342580397 * L_11 = __this->get_U3CU3Ef__this_1();
				NullCheck(L_11);
				MessageEventArgs_t36945740 * L_12 = WebSocket_dequeueFromMessageEventQueue_m4080620833(L_11, /*hidden argument*/NULL);
				V_1 = L_12;
				MessageEventArgs_t36945740 * L_13 = V_1;
				if (!L_13)
				{
					goto IL_0089;
				}
			}

IL_005f:
			{
				WebSocket_t1342580397 * L_14 = __this->get_U3CU3Ef__this_1();
				NullCheck(L_14);
				uint16_t L_15 = L_14->get__readyState_27();
				il2cpp_codegen_memory_barrier();
				if ((!(((uint32_t)L_15) == ((uint32_t)1))))
				{
					goto IL_0089;
				}
			}

IL_0072:
			{
				WebSocket_t1342580397 * L_16 = __this->get_U3CU3Ef__this_1();
				NullCheck(L_16);
				EventHandler_1_t181896286 * L_17 = L_16->get_OnMessage_35();
				WebSocket_t1342580397 * L_18 = __this->get_U3CU3Ef__this_1();
				MessageEventArgs_t36945740 * L_19 = V_1;
				Ext_Emit_TisMessageEventArgs_t36945740_m2913779343(NULL /*static, unused*/, L_17, L_18, L_19, /*hidden argument*/Ext_Emit_TisMessageEventArgs_t36945740_m2913779343_MethodInfo_var);
			}

IL_0089:
			{
				goto IL_00a5;
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t3991598821 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_008e;
			throw e;
		}

CATCH_008e:
		{ // begin catch(System.Exception)
			V_2 = ((Exception_t3991598821 *)__exception_local);
			WebSocket_t1342580397 * L_20 = __this->get_U3CU3Ef__this_1();
			Exception_t3991598821 * L_21 = V_2;
			NullCheck(L_20);
			WebSocket_acceptException_m1180715184(L_20, L_21, _stringLiteral4284785446, /*hidden argument*/NULL);
			goto IL_00a5;
		} // end catch (depth: 2)

IL_00a5:
		{
			IL2CPP_LEAVE(0xB1, FINALLY_00aa);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_00aa;
	}

FINALLY_00aa:
	{ // begin finally (depth: 1)
		Il2CppObject * L_22 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(170)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(170)
	{
		IL2CPP_JUMP_TBL(0xB1, IL_00b1)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_00b1:
	{
		goto IL_00d7;
	}

IL_00b6:
	{
		WebSocket_t1342580397 * L_23 = __this->get_U3CU3Ef__this_1();
		NullCheck(L_23);
		AutoResetEvent_t874642578 * L_24 = L_23->get__exitReceiving_13();
		if (!L_24)
		{
			goto IL_00d7;
		}
	}
	{
		WebSocket_t1342580397 * L_25 = __this->get_U3CU3Ef__this_1();
		NullCheck(L_25);
		AutoResetEvent_t874642578 * L_26 = L_25->get__exitReceiving_13();
		NullCheck(L_26);
		EventWaitHandle_Set_m224730030(L_26, /*hidden argument*/NULL);
	}

IL_00d7:
	{
		return;
	}
}
// System.Void WebSocketSharp.WebSocket/<startReceiving>c__AnonStorey2B::<>m__18(System.Exception)
extern Il2CppCodeGenString* _stringLiteral3639544358;
extern const uint32_t U3CstartReceivingU3Ec__AnonStorey2B_U3CU3Em__18_m3591481819_MetadataUsageId;
extern "C"  void U3CstartReceivingU3Ec__AnonStorey2B_U3CU3Em__18_m3591481819 (U3CstartReceivingU3Ec__AnonStorey2B_t802946016 * __this, Exception_t3991598821 * ___ex0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CstartReceivingU3Ec__AnonStorey2B_U3CU3Em__18_m3591481819_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		WebSocket_t1342580397 * L_0 = __this->get_U3CU3Ef__this_1();
		Exception_t3991598821 * L_1 = ___ex0;
		NullCheck(L_0);
		WebSocket_acceptException_m1180715184(L_0, L_1, _stringLiteral3639544358, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocket/<validateSecWebSocketProtocolHeader>c__AnonStorey2C::.ctor()
extern "C"  void U3CvalidateSecWebSocketProtocolHeaderU3Ec__AnonStorey2C__ctor_m2077357351 (U3CvalidateSecWebSocketProtocolHeaderU3Ec__AnonStorey2C_t467005428 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean WebSocketSharp.WebSocket/<validateSecWebSocketProtocolHeader>c__AnonStorey2C::<>m__13(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t U3CvalidateSecWebSocketProtocolHeaderU3Ec__AnonStorey2C_U3CU3Em__13_m253440292_MetadataUsageId;
extern "C"  bool U3CvalidateSecWebSocketProtocolHeaderU3Ec__AnonStorey2C_U3CU3Em__13_m253440292 (U3CvalidateSecWebSocketProtocolHeaderU3Ec__AnonStorey2C_t467005428 * __this, String_t* ___protocol0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CvalidateSecWebSocketProtocolHeaderU3Ec__AnonStorey2C_U3CU3Em__13_m253440292_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___protocol0;
		String_t* L_1 = __this->get_value_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void WebSocketSharp.WebSocketException::.ctor()
extern "C"  void WebSocketException__ctor_m469025560 (WebSocketException_t2311987812 * __this, const MethodInfo* method)
{
	{
		WebSocketException__ctor_m1257018382(__this, ((int32_t)1006), (String_t*)NULL, (Exception_t3991598821 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketException::.ctor(System.String)
extern "C"  void WebSocketException__ctor_m3830227690 (WebSocketException_t2311987812 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		WebSocketException__ctor_m1257018382(__this, ((int32_t)1006), L_0, (Exception_t3991598821 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketException::.ctor(WebSocketSharp.CloseStatusCode)
extern "C"  void WebSocketException__ctor_m2511973100 (WebSocketException_t2311987812 * __this, uint16_t ___code0, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___code0;
		WebSocketException__ctor_m1257018382(__this, L_0, (String_t*)NULL, (Exception_t3991598821 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketException::.ctor(System.String,System.Exception)
extern "C"  void WebSocketException__ctor_m3461878668 (WebSocketException_t2311987812 * __this, String_t* ___message0, Exception_t3991598821 * ___innerException1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t3991598821 * L_1 = ___innerException1;
		WebSocketException__ctor_m1257018382(__this, ((int32_t)1006), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketException::.ctor(WebSocketSharp.CloseStatusCode,System.String)
extern "C"  void WebSocketException__ctor_m960972328 (WebSocketException_t2311987812 * __this, uint16_t ___code0, String_t* ___message1, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___code0;
		String_t* L_1 = ___message1;
		WebSocketException__ctor_m1257018382(__this, L_0, L_1, (Exception_t3991598821 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketException::.ctor(WebSocketSharp.CloseStatusCode,System.String,System.Exception)
extern "C"  void WebSocketException__ctor_m1257018382 (WebSocketException_t2311987812 * __this, uint16_t ___code0, String_t* ___message1, Exception_t3991598821 * ___innerException2, const MethodInfo* method)
{
	String_t* G_B2_0 = NULL;
	WebSocketException_t2311987812 * G_B2_1 = NULL;
	String_t* G_B1_0 = NULL;
	WebSocketException_t2311987812 * G_B1_1 = NULL;
	{
		String_t* L_0 = ___message1;
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		G_B1_1 = __this;
		if (L_1)
		{
			G_B2_0 = L_1;
			G_B2_1 = __this;
			goto IL_000f;
		}
	}
	{
		uint16_t L_2 = ___code0;
		String_t* L_3 = Ext_GetMessage_m1491341063(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		G_B2_0 = L_3;
		G_B2_1 = G_B1_1;
	}

IL_000f:
	{
		Exception_t3991598821 * L_4 = ___innerException2;
		NullCheck(G_B2_1);
		Exception__ctor_m1328171222(G_B2_1, G_B2_0, L_4, /*hidden argument*/NULL);
		uint16_t L_5 = ___code0;
		WebSocketException_set_Code_m346900010(__this, L_5, /*hidden argument*/NULL);
		return;
	}
}
// WebSocketSharp.CloseStatusCode WebSocketSharp.WebSocketException::get_Code()
extern "C"  uint16_t WebSocketException_get_Code_m3592681969 (WebSocketException_t2311987812 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = __this->get_U3CCodeU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void WebSocketSharp.WebSocketException::set_Code(WebSocketSharp.CloseStatusCode)
extern "C"  void WebSocketException_set_Code_m346900010 (WebSocketException_t2311987812 * __this, uint16_t ___value0, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___value0;
		__this->set_U3CCodeU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketFrame::.ctor()
extern "C"  void WebSocketFrame__ctor_m2272980410 (WebSocketFrame_t778194306 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketFrame::.ctor(WebSocketSharp.Opcode,WebSocketSharp.PayloadData)
extern "C"  void WebSocketFrame__ctor_m4163479642 (WebSocketFrame_t778194306 * __this, uint8_t ___opcode0, PayloadData_t39926750 * ___payload1, const MethodInfo* method)
{
	{
		uint8_t L_0 = ___opcode0;
		PayloadData_t39926750 * L_1 = ___payload1;
		WebSocketFrame__ctor_m3336031814(__this, 1, L_0, 1, L_1, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketFrame::.ctor(WebSocketSharp.Opcode,WebSocketSharp.Mask,WebSocketSharp.PayloadData)
extern "C"  void WebSocketFrame__ctor_m1992914661 (WebSocketFrame_t778194306 * __this, uint8_t ___opcode0, uint8_t ___mask1, PayloadData_t39926750 * ___payload2, const MethodInfo* method)
{
	{
		uint8_t L_0 = ___opcode0;
		uint8_t L_1 = ___mask1;
		PayloadData_t39926750 * L_2 = ___payload2;
		WebSocketFrame__ctor_m3336031814(__this, 1, L_0, L_1, L_2, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketFrame::.ctor(WebSocketSharp.Fin,WebSocketSharp.Opcode,WebSocketSharp.Mask,WebSocketSharp.PayloadData)
extern "C"  void WebSocketFrame__ctor_m771058775 (WebSocketFrame_t778194306 * __this, uint8_t ___fin0, uint8_t ___opcode1, uint8_t ___mask2, PayloadData_t39926750 * ___payload3, const MethodInfo* method)
{
	{
		uint8_t L_0 = ___fin0;
		uint8_t L_1 = ___opcode1;
		uint8_t L_2 = ___mask2;
		PayloadData_t39926750 * L_3 = ___payload3;
		WebSocketFrame__ctor_m3336031814(__this, L_0, L_1, L_2, L_3, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketFrame::.ctor(WebSocketSharp.Fin,WebSocketSharp.Opcode,WebSocketSharp.Mask,WebSocketSharp.PayloadData,System.Boolean)
extern Il2CppClass* WebSocketFrame_t778194306_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketFrame__ctor_m3336031814_MetadataUsageId;
extern "C"  void WebSocketFrame__ctor_m3336031814 (WebSocketFrame_t778194306 * __this, uint8_t ___fin0, uint8_t ___opcode1, uint8_t ___mask2, PayloadData_t39926750 * ___payload3, bool ___compressed4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame__ctor_m3336031814_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint64_t V_0 = 0;
	WebSocketFrame_t778194306 * G_B3_0 = NULL;
	WebSocketFrame_t778194306 * G_B1_0 = NULL;
	WebSocketFrame_t778194306 * G_B2_0 = NULL;
	int32_t G_B4_0 = 0;
	WebSocketFrame_t778194306 * G_B4_1 = NULL;
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		uint8_t L_0 = ___fin0;
		__this->set__fin_1(L_0);
		uint8_t L_1 = ___opcode1;
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t778194306_il2cpp_TypeInfo_var);
		bool L_2 = WebSocketFrame_isData_m69248783(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if (!L_2)
		{
			G_B3_0 = __this;
			goto IL_0026;
		}
	}
	{
		bool L_3 = ___compressed4;
		G_B2_0 = G_B1_0;
		if (!L_3)
		{
			G_B3_0 = G_B1_0;
			goto IL_0026;
		}
	}
	{
		G_B4_0 = 1;
		G_B4_1 = G_B2_0;
		goto IL_0027;
	}

IL_0026:
	{
		G_B4_0 = 0;
		G_B4_1 = G_B3_0;
	}

IL_0027:
	{
		NullCheck(G_B4_1);
		G_B4_1->set__rsv1_7(G_B4_0);
		__this->set__rsv2_8(0);
		__this->set__rsv3_9(0);
		uint8_t L_4 = ___opcode1;
		__this->set__opcode_4(L_4);
		uint8_t L_5 = ___mask2;
		__this->set__mask_2(L_5);
		PayloadData_t39926750 * L_6 = ___payload3;
		NullCheck(L_6);
		uint64_t L_7 = PayloadData_get_Length_m3166044545(L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		uint64_t L_8 = V_0;
		if ((!(((uint64_t)L_8) < ((uint64_t)(((int64_t)((int64_t)((int32_t)126))))))))
		{
			goto IL_0072;
		}
	}
	{
		uint64_t L_9 = V_0;
		__this->set__payloadLength_6((((int32_t)((uint8_t)L_9))));
		__this->set__extPayloadLength_0(((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)0)));
		goto IL_00ae;
	}

IL_0072:
	{
		uint64_t L_10 = V_0;
		if ((!(((uint64_t)L_10) < ((uint64_t)(((int64_t)((int64_t)((int32_t)65536))))))))
		{
			goto IL_0099;
		}
	}
	{
		__this->set__payloadLength_6(((int32_t)126));
		uint64_t L_11 = V_0;
		ByteU5BU5D_t4260760469* L_12 = Ext_ToByteArrayInternally_m3284354672(NULL /*static, unused*/, (((int32_t)((uint16_t)L_11))), 1, /*hidden argument*/NULL);
		__this->set__extPayloadLength_0(L_12);
		goto IL_00ae;
	}

IL_0099:
	{
		__this->set__payloadLength_6(((int32_t)127));
		uint64_t L_13 = V_0;
		ByteU5BU5D_t4260760469* L_14 = Ext_ToByteArrayInternally_m87443913(NULL /*static, unused*/, L_13, 1, /*hidden argument*/NULL);
		__this->set__extPayloadLength_0(L_14);
	}

IL_00ae:
	{
		uint8_t L_15 = ___mask2;
		if ((!(((uint32_t)L_15) == ((uint32_t)1))))
		{
			goto IL_00d2;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t778194306_il2cpp_TypeInfo_var);
		ByteU5BU5D_t4260760469* L_16 = WebSocketFrame_createMaskingKey_m991693541(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set__maskingKey_3(L_16);
		PayloadData_t39926750 * L_17 = ___payload3;
		ByteU5BU5D_t4260760469* L_18 = __this->get__maskingKey_3();
		NullCheck(L_17);
		PayloadData_Mask_m2120375719(L_17, L_18, /*hidden argument*/NULL);
		goto IL_00de;
	}

IL_00d2:
	{
		__this->set__maskingKey_3(((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)0)));
	}

IL_00de:
	{
		PayloadData_t39926750 * L_19 = ___payload3;
		__this->set__payloadData_5(L_19);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketFrame::.cctor()
extern Il2CppClass* WebSocketFrame_t778194306_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketFrame__cctor_m1260819763_MetadataUsageId;
extern "C"  void WebSocketFrame__cctor_m1260819763 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame__cctor_m1260819763_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		WebSocketFrame_t778194306 * L_0 = WebSocketFrame_CreatePingFrame_m982549890(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		NullCheck(L_0);
		ByteU5BU5D_t4260760469* L_1 = WebSocketFrame_ToByteArray_m3468314104(L_0, /*hidden argument*/NULL);
		((WebSocketFrame_t778194306_StaticFields*)WebSocketFrame_t778194306_il2cpp_TypeInfo_var->static_fields)->set_EmptyUnmaskPingData_10(L_1);
		return;
	}
}
// System.Collections.IEnumerator WebSocketSharp.WebSocketFrame::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * WebSocketFrame_System_Collections_IEnumerable_GetEnumerator_m3591556711 (WebSocketFrame_t778194306 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = WebSocketFrame_GetEnumerator_m2490913985(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Byte[] WebSocketSharp.WebSocketFrame::get_ExtendedPayloadLength()
extern "C"  ByteU5BU5D_t4260760469* WebSocketFrame_get_ExtendedPayloadLength_m2660771956 (WebSocketFrame_t778194306 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t4260760469* L_0 = __this->get__extPayloadLength_0();
		return L_0;
	}
}
// WebSocketSharp.Fin WebSocketSharp.WebSocketFrame::get_Fin()
extern "C"  uint8_t WebSocketFrame_get_Fin_m1975553495 (WebSocketFrame_t778194306 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = __this->get__fin_1();
		return L_0;
	}
}
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsBinary()
extern "C"  bool WebSocketFrame_get_IsBinary_m2066147498 (WebSocketFrame_t778194306 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = __this->get__opcode_4();
		return (bool)((((int32_t)L_0) == ((int32_t)2))? 1 : 0);
	}
}
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsClose()
extern "C"  bool WebSocketFrame_get_IsClose_m2149864465 (WebSocketFrame_t778194306 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = __this->get__opcode_4();
		return (bool)((((int32_t)L_0) == ((int32_t)8))? 1 : 0);
	}
}
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsCompressed()
extern "C"  bool WebSocketFrame_get_IsCompressed_m3923144746 (WebSocketFrame_t778194306 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = __this->get__rsv1_7();
		return (bool)((((int32_t)L_0) == ((int32_t)1))? 1 : 0);
	}
}
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsContinuation()
extern "C"  bool WebSocketFrame_get_IsContinuation_m4205121376 (WebSocketFrame_t778194306 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = __this->get__opcode_4();
		return (bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsControl()
extern "C"  bool WebSocketFrame_get_IsControl_m229257270 (WebSocketFrame_t778194306 * __this, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	{
		uint8_t L_0 = __this->get__opcode_4();
		if ((((int32_t)L_0) == ((int32_t)8)))
		{
			goto IL_0025;
		}
	}
	{
		uint8_t L_1 = __this->get__opcode_4();
		if ((((int32_t)L_1) == ((int32_t)((int32_t)9))))
		{
			goto IL_0025;
		}
	}
	{
		uint8_t L_2 = __this->get__opcode_4();
		G_B4_0 = ((((int32_t)L_2) == ((int32_t)((int32_t)10)))? 1 : 0);
		goto IL_0026;
	}

IL_0025:
	{
		G_B4_0 = 1;
	}

IL_0026:
	{
		return (bool)G_B4_0;
	}
}
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsData()
extern "C"  bool WebSocketFrame_get_IsData_m2997444627 (WebSocketFrame_t778194306 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		uint8_t L_0 = __this->get__opcode_4();
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_0017;
		}
	}
	{
		uint8_t L_1 = __this->get__opcode_4();
		G_B3_0 = ((((int32_t)L_1) == ((int32_t)1))? 1 : 0);
		goto IL_0018;
	}

IL_0017:
	{
		G_B3_0 = 1;
	}

IL_0018:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsFinal()
extern "C"  bool WebSocketFrame_get_IsFinal_m430067727 (WebSocketFrame_t778194306 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = __this->get__fin_1();
		return (bool)((((int32_t)L_0) == ((int32_t)1))? 1 : 0);
	}
}
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsFragmented()
extern "C"  bool WebSocketFrame_get_IsFragmented_m98582552 (WebSocketFrame_t778194306 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		uint8_t L_0 = __this->get__fin_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		uint8_t L_1 = __this->get__opcode_4();
		G_B3_0 = ((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
		goto IL_0017;
	}

IL_0016:
	{
		G_B3_0 = 1;
	}

IL_0017:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsMasked()
extern "C"  bool WebSocketFrame_get_IsMasked_m1404103348 (WebSocketFrame_t778194306 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = __this->get__mask_2();
		return (bool)((((int32_t)L_0) == ((int32_t)1))? 1 : 0);
	}
}
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsPerMessageCompressed()
extern "C"  bool WebSocketFrame_get_IsPerMessageCompressed_m1925771124 (WebSocketFrame_t778194306 * __this, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	{
		uint8_t L_0 = __this->get__opcode_4();
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_0018;
		}
	}
	{
		uint8_t L_1 = __this->get__opcode_4();
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0023;
		}
	}

IL_0018:
	{
		uint8_t L_2 = __this->get__rsv1_7();
		G_B4_0 = ((((int32_t)L_2) == ((int32_t)1))? 1 : 0);
		goto IL_0024;
	}

IL_0023:
	{
		G_B4_0 = 0;
	}

IL_0024:
	{
		return (bool)G_B4_0;
	}
}
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsPing()
extern "C"  bool WebSocketFrame_get_IsPing_m3348209627 (WebSocketFrame_t778194306 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = __this->get__opcode_4();
		return (bool)((((int32_t)L_0) == ((int32_t)((int32_t)9)))? 1 : 0);
	}
}
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsPong()
extern "C"  bool WebSocketFrame_get_IsPong_m3353750753 (WebSocketFrame_t778194306 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = __this->get__opcode_4();
		return (bool)((((int32_t)L_0) == ((int32_t)((int32_t)10)))? 1 : 0);
	}
}
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsText()
extern "C"  bool WebSocketFrame_get_IsText_m3459342550 (WebSocketFrame_t778194306 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = __this->get__opcode_4();
		return (bool)((((int32_t)L_0) == ((int32_t)1))? 1 : 0);
	}
}
// System.UInt64 WebSocketSharp.WebSocketFrame::get_Length()
extern "C"  uint64_t WebSocketFrame_get_Length_m132165789 (WebSocketFrame_t778194306 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t4260760469* L_0 = __this->get__extPayloadLength_0();
		NullCheck(L_0);
		ByteU5BU5D_t4260760469* L_1 = __this->get__maskingKey_3();
		NullCheck(L_1);
		PayloadData_t39926750 * L_2 = __this->get__payloadData_5();
		NullCheck(L_2);
		uint64_t L_3 = PayloadData_get_Length_m3166044545(L_2, /*hidden argument*/NULL);
		return ((int64_t)((int64_t)((int64_t)((int64_t)(((int64_t)((int64_t)2)))+(int64_t)(((int64_t)((int64_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))+(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))))))))))+(int64_t)L_3));
	}
}
// WebSocketSharp.Mask WebSocketSharp.WebSocketFrame::get_Mask()
extern "C"  uint8_t WebSocketFrame_get_Mask_m1377172099 (WebSocketFrame_t778194306 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = __this->get__mask_2();
		return L_0;
	}
}
// System.Byte[] WebSocketSharp.WebSocketFrame::get_MaskingKey()
extern "C"  ByteU5BU5D_t4260760469* WebSocketFrame_get_MaskingKey_m1393028594 (WebSocketFrame_t778194306 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t4260760469* L_0 = __this->get__maskingKey_3();
		return L_0;
	}
}
// WebSocketSharp.Opcode WebSocketSharp.WebSocketFrame::get_Opcode()
extern "C"  uint8_t WebSocketFrame_get_Opcode_m53528135 (WebSocketFrame_t778194306 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = __this->get__opcode_4();
		return L_0;
	}
}
// WebSocketSharp.PayloadData WebSocketSharp.WebSocketFrame::get_PayloadData()
extern "C"  PayloadData_t39926750 * WebSocketFrame_get_PayloadData_m4173896503 (WebSocketFrame_t778194306 * __this, const MethodInfo* method)
{
	{
		PayloadData_t39926750 * L_0 = __this->get__payloadData_5();
		return L_0;
	}
}
// System.Byte WebSocketSharp.WebSocketFrame::get_PayloadLength()
extern "C"  uint8_t WebSocketFrame_get_PayloadLength_m2711758479 (WebSocketFrame_t778194306 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = __this->get__payloadLength_6();
		return L_0;
	}
}
// WebSocketSharp.Rsv WebSocketSharp.WebSocketFrame::get_Rsv1()
extern "C"  uint8_t WebSocketFrame_get_Rsv1_m1579988508 (WebSocketFrame_t778194306 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = __this->get__rsv1_7();
		return L_0;
	}
}
// WebSocketSharp.Rsv WebSocketSharp.WebSocketFrame::get_Rsv2()
extern "C"  uint8_t WebSocketFrame_get_Rsv2_m1579989469 (WebSocketFrame_t778194306 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = __this->get__rsv2_8();
		return L_0;
	}
}
// WebSocketSharp.Rsv WebSocketSharp.WebSocketFrame::get_Rsv3()
extern "C"  uint8_t WebSocketFrame_get_Rsv3_m1579990430 (WebSocketFrame_t778194306 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = __this->get__rsv3_9();
		return L_0;
	}
}
// System.Byte[] WebSocketSharp.WebSocketFrame::createMaskingKey()
extern Il2CppClass* ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var;
extern Il2CppClass* Random_t4255898871_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketFrame_createMaskingKey_m991693541_MetadataUsageId;
extern "C"  ByteU5BU5D_t4260760469* WebSocketFrame_createMaskingKey_m991693541 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_createMaskingKey_m991693541_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t4260760469* V_0 = NULL;
	Random_t4255898871 * V_1 = NULL;
	{
		V_0 = ((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)4));
		Random_t4255898871 * L_0 = (Random_t4255898871 *)il2cpp_codegen_object_new(Random_t4255898871_il2cpp_TypeInfo_var);
		Random__ctor_m2490522898(L_0, /*hidden argument*/NULL);
		V_1 = L_0;
		Random_t4255898871 * L_1 = V_1;
		ByteU5BU5D_t4260760469* L_2 = V_0;
		NullCheck(L_1);
		VirtActionInvoker1< ByteU5BU5D_t4260760469* >::Invoke(5 /* System.Void System.Random::NextBytes(System.Byte[]) */, L_1, L_2);
		ByteU5BU5D_t4260760469* L_3 = V_0;
		return L_3;
	}
}
// System.String WebSocketSharp.WebSocketFrame::dump(WebSocketSharp.WebSocketFrame)
extern Il2CppClass* U3CdumpU3Ec__AnonStorey2F_t681710991_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StringBuilder_t243639308_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_1_t1953705719_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1363677321_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CdumpU3Ec__AnonStorey2F_U3CU3Em__19_m1858461973_MethodInfo_var;
extern const MethodInfo* Func_1__ctor_m1345252792_MethodInfo_var;
extern const MethodInfo* Func_1_Invoke_m3035645860_MethodInfo_var;
extern const MethodInfo* Action_4_Invoke_m4047132558_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral115067072;
extern Il2CppCodeGenString* _stringLiteral3205212258;
extern Il2CppCodeGenString* _stringLiteral3205331422;
extern Il2CppCodeGenString* _stringLiteral574412611;
extern Il2CppCodeGenString* _stringLiteral1664040396;
extern Il2CppCodeGenString* _stringLiteral1200250743;
extern Il2CppCodeGenString* _stringLiteral882870836;
extern Il2CppCodeGenString* _stringLiteral2517246051;
extern const uint32_t WebSocketFrame_dump_m3839803398_MetadataUsageId;
extern "C"  String_t* WebSocketFrame_dump_m3839803398 (Il2CppObject * __this /* static, unused */, WebSocketFrame_t778194306 * ___frame0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_dump_m3839803398_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint64_t V_0 = 0;
	int64_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	String_t* V_4 = NULL;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	String_t* V_7 = NULL;
	Func_1_t1953705719 * V_8 = NULL;
	Action_4_t828544145 * V_9 = NULL;
	ByteU5BU5D_t4260760469* V_10 = NULL;
	int64_t V_11 = 0;
	int64_t V_12 = 0;
	U3CdumpU3Ec__AnonStorey2F_t681710991 * V_13 = NULL;
	String_t* G_B13_0 = NULL;
	Action_4_t828544145 * G_B13_1 = NULL;
	String_t* G_B12_0 = NULL;
	Action_4_t828544145 * G_B12_1 = NULL;
	String_t* G_B14_0 = NULL;
	String_t* G_B14_1 = NULL;
	Action_4_t828544145 * G_B14_2 = NULL;
	String_t* G_B16_0 = NULL;
	String_t* G_B16_1 = NULL;
	Action_4_t828544145 * G_B16_2 = NULL;
	String_t* G_B15_0 = NULL;
	String_t* G_B15_1 = NULL;
	Action_4_t828544145 * G_B15_2 = NULL;
	String_t* G_B17_0 = NULL;
	String_t* G_B17_1 = NULL;
	String_t* G_B17_2 = NULL;
	Action_4_t828544145 * G_B17_3 = NULL;
	{
		U3CdumpU3Ec__AnonStorey2F_t681710991 * L_0 = (U3CdumpU3Ec__AnonStorey2F_t681710991 *)il2cpp_codegen_object_new(U3CdumpU3Ec__AnonStorey2F_t681710991_il2cpp_TypeInfo_var);
		U3CdumpU3Ec__AnonStorey2F__ctor_m1441017532(L_0, /*hidden argument*/NULL);
		V_13 = L_0;
		WebSocketFrame_t778194306 * L_1 = ___frame0;
		NullCheck(L_1);
		uint64_t L_2 = WebSocketFrame_get_Length_m132165789(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		uint64_t L_3 = V_0;
		V_1 = ((int64_t)((uint64_t)(int64_t)L_3/(uint64_t)(int64_t)(((int64_t)((int64_t)4)))));
		uint64_t L_4 = V_0;
		V_2 = (((int32_t)((int32_t)((int64_t)((uint64_t)(int64_t)L_4%(uint64_t)(int64_t)(((int64_t)((int64_t)4))))))));
		int64_t L_5 = V_1;
		if ((((int64_t)L_5) >= ((int64_t)(((int64_t)((int64_t)((int32_t)10000)))))))
		{
			goto IL_0033;
		}
	}
	{
		V_3 = 4;
		V_4 = _stringLiteral115067072;
		goto IL_0074;
	}

IL_0033:
	{
		int64_t L_6 = V_1;
		if ((((int64_t)L_6) >= ((int64_t)(((int64_t)((int64_t)((int32_t)65536)))))))
		{
			goto IL_004d;
		}
	}
	{
		V_3 = 4;
		V_4 = _stringLiteral3205212258;
		goto IL_0074;
	}

IL_004d:
	{
		int64_t L_7 = V_1;
		if ((((int64_t)L_7) >= ((int64_t)((int64_t)4294967296LL))))
		{
			goto IL_006a;
		}
	}
	{
		V_3 = 8;
		V_4 = _stringLiteral3205331422;
		goto IL_0074;
	}

IL_006a:
	{
		V_3 = ((int32_t)16);
		V_4 = _stringLiteral574412611;
	}

IL_0074:
	{
		int32_t L_8 = V_3;
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_9);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral1664040396, L_10, /*hidden argument*/NULL);
		V_5 = L_11;
		String_t* L_12 = V_5;
		String_t* L_13 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral1200250743, L_12, /*hidden argument*/NULL);
		V_6 = L_13;
		U3CdumpU3Ec__AnonStorey2F_t681710991 * L_14 = V_13;
		String_t* L_15 = V_4;
		String_t* L_16 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral882870836, L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		L_14->set_lineFmt_1(L_16);
		String_t* L_17 = V_5;
		String_t* L_18 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral2517246051, L_17, /*hidden argument*/NULL);
		V_7 = L_18;
		U3CdumpU3Ec__AnonStorey2F_t681710991 * L_19 = V_13;
		StringBuilder_t243639308 * L_20 = (StringBuilder_t243639308 *)il2cpp_codegen_object_new(StringBuilder_t243639308_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3624398269(L_20, ((int32_t)64), /*hidden argument*/NULL);
		NullCheck(L_19);
		L_19->set_output_0(L_20);
		U3CdumpU3Ec__AnonStorey2F_t681710991 * L_21 = V_13;
		IntPtr_t L_22;
		L_22.set_m_value_0((void*)(void*)U3CdumpU3Ec__AnonStorey2F_U3CU3Em__19_m1858461973_MethodInfo_var);
		Func_1_t1953705719 * L_23 = (Func_1_t1953705719 *)il2cpp_codegen_object_new(Func_1_t1953705719_il2cpp_TypeInfo_var);
		Func_1__ctor_m1345252792(L_23, L_21, L_22, /*hidden argument*/Func_1__ctor_m1345252792_MethodInfo_var);
		V_8 = L_23;
		U3CdumpU3Ec__AnonStorey2F_t681710991 * L_24 = V_13;
		NullCheck(L_24);
		StringBuilder_t243639308 * L_25 = L_24->get_output_0();
		String_t* L_26 = V_6;
		String_t* L_27 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_25);
		StringBuilder_AppendFormat_m3723191730(L_25, L_26, L_27, /*hidden argument*/NULL);
		Func_1_t1953705719 * L_28 = V_8;
		NullCheck(L_28);
		Action_4_t828544145 * L_29 = Func_1_Invoke_m3035645860(L_28, /*hidden argument*/Func_1_Invoke_m3035645860_MethodInfo_var);
		V_9 = L_29;
		WebSocketFrame_t778194306 * L_30 = ___frame0;
		NullCheck(L_30);
		ByteU5BU5D_t4260760469* L_31 = WebSocketFrame_ToByteArray_m3468314104(L_30, /*hidden argument*/NULL);
		V_10 = L_31;
		V_11 = (((int64_t)((int64_t)0)));
		goto IL_01f2;
	}

IL_0100:
	{
		int64_t L_32 = V_11;
		V_12 = ((int64_t)((int64_t)L_32*(int64_t)(((int64_t)((int64_t)4)))));
		int64_t L_33 = V_11;
		int64_t L_34 = V_1;
		if ((((int64_t)L_33) >= ((int64_t)L_34)))
		{
			goto IL_0174;
		}
	}
	{
		Action_4_t828544145 * L_35 = V_9;
		ByteU5BU5D_t4260760469* L_36 = V_10;
		int64_t L_37 = V_12;
		if ((int64_t)(L_37) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, (il2cpp_array_size_t)(uintptr_t)(((intptr_t)L_37)));
		intptr_t L_38 = (((intptr_t)L_37));
		uint8_t L_39 = (L_36)->GetAt(static_cast<il2cpp_array_size_t>(L_38));
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		String_t* L_40 = Convert_ToString_m1313468513(NULL /*static, unused*/, L_39, 2, /*hidden argument*/NULL);
		NullCheck(L_40);
		String_t* L_41 = String_PadLeft_m3268206439(L_40, 8, ((int32_t)48), /*hidden argument*/NULL);
		ByteU5BU5D_t4260760469* L_42 = V_10;
		int64_t L_43 = V_12;
		if ((int64_t)(((int64_t)((int64_t)L_43+(int64_t)(((int64_t)((int64_t)1)))))) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, (il2cpp_array_size_t)(uintptr_t)(((intptr_t)((int64_t)((int64_t)L_43+(int64_t)(((int64_t)((int64_t)1))))))));
		intptr_t L_44 = (((intptr_t)((int64_t)((int64_t)L_43+(int64_t)(((int64_t)((int64_t)1)))))));
		uint8_t L_45 = (L_42)->GetAt(static_cast<il2cpp_array_size_t>(L_44));
		String_t* L_46 = Convert_ToString_m1313468513(NULL /*static, unused*/, L_45, 2, /*hidden argument*/NULL);
		NullCheck(L_46);
		String_t* L_47 = String_PadLeft_m3268206439(L_46, 8, ((int32_t)48), /*hidden argument*/NULL);
		ByteU5BU5D_t4260760469* L_48 = V_10;
		int64_t L_49 = V_12;
		if ((int64_t)(((int64_t)((int64_t)L_49+(int64_t)(((int64_t)((int64_t)2)))))) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, (il2cpp_array_size_t)(uintptr_t)(((intptr_t)((int64_t)((int64_t)L_49+(int64_t)(((int64_t)((int64_t)2))))))));
		intptr_t L_50 = (((intptr_t)((int64_t)((int64_t)L_49+(int64_t)(((int64_t)((int64_t)2)))))));
		uint8_t L_51 = (L_48)->GetAt(static_cast<il2cpp_array_size_t>(L_50));
		String_t* L_52 = Convert_ToString_m1313468513(NULL /*static, unused*/, L_51, 2, /*hidden argument*/NULL);
		NullCheck(L_52);
		String_t* L_53 = String_PadLeft_m3268206439(L_52, 8, ((int32_t)48), /*hidden argument*/NULL);
		ByteU5BU5D_t4260760469* L_54 = V_10;
		int64_t L_55 = V_12;
		if ((int64_t)(((int64_t)((int64_t)L_55+(int64_t)(((int64_t)((int64_t)3)))))) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, (il2cpp_array_size_t)(uintptr_t)(((intptr_t)((int64_t)((int64_t)L_55+(int64_t)(((int64_t)((int64_t)3))))))));
		intptr_t L_56 = (((intptr_t)((int64_t)((int64_t)L_55+(int64_t)(((int64_t)((int64_t)3)))))));
		uint8_t L_57 = (L_54)->GetAt(static_cast<il2cpp_array_size_t>(L_56));
		String_t* L_58 = Convert_ToString_m1313468513(NULL /*static, unused*/, L_57, 2, /*hidden argument*/NULL);
		NullCheck(L_58);
		String_t* L_59 = String_PadLeft_m3268206439(L_58, 8, ((int32_t)48), /*hidden argument*/NULL);
		NullCheck(L_35);
		Action_4_Invoke_m4047132558(L_35, L_41, L_47, L_53, L_59, /*hidden argument*/Action_4_Invoke_m4047132558_MethodInfo_var);
		goto IL_01eb;
	}

IL_0174:
	{
		int32_t L_60 = V_2;
		if ((((int32_t)L_60) <= ((int32_t)0)))
		{
			goto IL_01eb;
		}
	}
	{
		Action_4_t828544145 * L_61 = V_9;
		ByteU5BU5D_t4260760469* L_62 = V_10;
		int64_t L_63 = V_12;
		if ((int64_t)(L_63) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		NullCheck(L_62);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_62, (il2cpp_array_size_t)(uintptr_t)(((intptr_t)L_63)));
		intptr_t L_64 = (((intptr_t)L_63));
		uint8_t L_65 = (L_62)->GetAt(static_cast<il2cpp_array_size_t>(L_64));
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		String_t* L_66 = Convert_ToString_m1313468513(NULL /*static, unused*/, L_65, 2, /*hidden argument*/NULL);
		NullCheck(L_66);
		String_t* L_67 = String_PadLeft_m3268206439(L_66, 8, ((int32_t)48), /*hidden argument*/NULL);
		int32_t L_68 = V_2;
		G_B12_0 = L_67;
		G_B12_1 = L_61;
		if ((((int32_t)L_68) < ((int32_t)2)))
		{
			G_B13_0 = L_67;
			G_B13_1 = L_61;
			goto IL_01b4;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_69 = V_10;
		int64_t L_70 = V_12;
		if ((int64_t)(((int64_t)((int64_t)L_70+(int64_t)(((int64_t)((int64_t)1)))))) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		NullCheck(L_69);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_69, (il2cpp_array_size_t)(uintptr_t)(((intptr_t)((int64_t)((int64_t)L_70+(int64_t)(((int64_t)((int64_t)1))))))));
		intptr_t L_71 = (((intptr_t)((int64_t)((int64_t)L_70+(int64_t)(((int64_t)((int64_t)1)))))));
		uint8_t L_72 = (L_69)->GetAt(static_cast<il2cpp_array_size_t>(L_71));
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		String_t* L_73 = Convert_ToString_m1313468513(NULL /*static, unused*/, L_72, 2, /*hidden argument*/NULL);
		NullCheck(L_73);
		String_t* L_74 = String_PadLeft_m3268206439(L_73, 8, ((int32_t)48), /*hidden argument*/NULL);
		G_B14_0 = L_74;
		G_B14_1 = G_B12_0;
		G_B14_2 = G_B12_1;
		goto IL_01b9;
	}

IL_01b4:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_75 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B14_0 = L_75;
		G_B14_1 = G_B13_0;
		G_B14_2 = G_B13_1;
	}

IL_01b9:
	{
		int32_t L_76 = V_2;
		G_B15_0 = G_B14_0;
		G_B15_1 = G_B14_1;
		G_B15_2 = G_B14_2;
		if ((!(((uint32_t)L_76) == ((uint32_t)3))))
		{
			G_B16_0 = G_B14_0;
			G_B16_1 = G_B14_1;
			G_B16_2 = G_B14_2;
			goto IL_01dc;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_77 = V_10;
		int64_t L_78 = V_12;
		if ((int64_t)(((int64_t)((int64_t)L_78+(int64_t)(((int64_t)((int64_t)2)))))) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		NullCheck(L_77);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_77, (il2cpp_array_size_t)(uintptr_t)(((intptr_t)((int64_t)((int64_t)L_78+(int64_t)(((int64_t)((int64_t)2))))))));
		intptr_t L_79 = (((intptr_t)((int64_t)((int64_t)L_78+(int64_t)(((int64_t)((int64_t)2)))))));
		uint8_t L_80 = (L_77)->GetAt(static_cast<il2cpp_array_size_t>(L_79));
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1363677321_il2cpp_TypeInfo_var);
		String_t* L_81 = Convert_ToString_m1313468513(NULL /*static, unused*/, L_80, 2, /*hidden argument*/NULL);
		NullCheck(L_81);
		String_t* L_82 = String_PadLeft_m3268206439(L_81, 8, ((int32_t)48), /*hidden argument*/NULL);
		G_B17_0 = L_82;
		G_B17_1 = G_B15_0;
		G_B17_2 = G_B15_1;
		G_B17_3 = G_B15_2;
		goto IL_01e1;
	}

IL_01dc:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_83 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B17_0 = L_83;
		G_B17_1 = G_B16_0;
		G_B17_2 = G_B16_1;
		G_B17_3 = G_B16_2;
	}

IL_01e1:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_84 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(G_B17_3);
		Action_4_Invoke_m4047132558(G_B17_3, G_B17_2, G_B17_1, G_B17_0, L_84, /*hidden argument*/Action_4_Invoke_m4047132558_MethodInfo_var);
	}

IL_01eb:
	{
		int64_t L_85 = V_11;
		V_11 = ((int64_t)((int64_t)L_85+(int64_t)(((int64_t)((int64_t)1)))));
	}

IL_01f2:
	{
		int64_t L_86 = V_11;
		int64_t L_87 = V_1;
		if ((((int64_t)L_86) <= ((int64_t)L_87)))
		{
			goto IL_0100;
		}
	}
	{
		U3CdumpU3Ec__AnonStorey2F_t681710991 * L_88 = V_13;
		NullCheck(L_88);
		StringBuilder_t243639308 * L_89 = L_88->get_output_0();
		String_t* L_90 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_91 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_89);
		StringBuilder_AppendFormat_m3723191730(L_89, L_90, L_91, /*hidden argument*/NULL);
		U3CdumpU3Ec__AnonStorey2F_t681710991 * L_92 = V_13;
		NullCheck(L_92);
		StringBuilder_t243639308 * L_93 = L_92->get_output_0();
		NullCheck(L_93);
		String_t* L_94 = StringBuilder_ToString_m350379841(L_93, /*hidden argument*/NULL);
		return L_94;
	}
}
// System.Boolean WebSocketSharp.WebSocketFrame::isControl(WebSocketSharp.Opcode)
extern "C"  bool WebSocketFrame_isControl_m3029256346 (Il2CppObject * __this /* static, unused */, uint8_t ___opcode0, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	{
		uint8_t L_0 = ___opcode0;
		if ((((int32_t)L_0) == ((int32_t)8)))
		{
			goto IL_0016;
		}
	}
	{
		uint8_t L_1 = ___opcode0;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)9))))
		{
			goto IL_0016;
		}
	}
	{
		uint8_t L_2 = ___opcode0;
		G_B4_0 = ((((int32_t)L_2) == ((int32_t)((int32_t)10)))? 1 : 0);
		goto IL_0017;
	}

IL_0016:
	{
		G_B4_0 = 1;
	}

IL_0017:
	{
		return (bool)G_B4_0;
	}
}
// System.Boolean WebSocketSharp.WebSocketFrame::isData(WebSocketSharp.Opcode)
extern "C"  bool WebSocketFrame_isData_m69248783 (Il2CppObject * __this /* static, unused */, uint8_t ___opcode0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		uint8_t L_0 = ___opcode0;
		if ((((int32_t)L_0) == ((int32_t)1)))
		{
			goto IL_000d;
		}
	}
	{
		uint8_t L_1 = ___opcode0;
		G_B3_0 = ((((int32_t)L_1) == ((int32_t)2))? 1 : 0);
		goto IL_000e;
	}

IL_000d:
	{
		G_B3_0 = 1;
	}

IL_000e:
	{
		return (bool)G_B3_0;
	}
}
// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::parse(System.Byte[],System.IO.Stream,System.Boolean)
extern Il2CppClass* WebSocketFrame_t778194306_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocketException_t2311987812_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var;
extern Il2CppClass* PayloadData_t39926750_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3033030302;
extern Il2CppCodeGenString* _stringLiteral3081821550;
extern Il2CppCodeGenString* _stringLiteral2490559370;
extern Il2CppCodeGenString* _stringLiteral1200591507;
extern Il2CppCodeGenString* _stringLiteral1241564285;
extern Il2CppCodeGenString* _stringLiteral1980370780;
extern Il2CppCodeGenString* _stringLiteral4176728934;
extern const uint32_t WebSocketFrame_parse_m2392405532_MetadataUsageId;
extern "C"  WebSocketFrame_t778194306 * WebSocketFrame_parse_m2392405532 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___header0, Stream_t1561764144 * ___stream1, bool ___unmask2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_parse_m2392405532_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint8_t V_0 = 0;
	uint8_t V_1 = 0;
	uint8_t V_2 = 0;
	uint8_t V_3 = 0;
	uint8_t V_4 = 0;
	uint8_t V_5 = 0;
	uint8_t V_6 = 0x0;
	String_t* V_7 = NULL;
	WebSocketFrame_t778194306 * V_8 = NULL;
	int32_t V_9 = 0;
	ByteU5BU5D_t4260760469* V_10 = NULL;
	bool V_11 = false;
	ByteU5BU5D_t4260760469* V_12 = NULL;
	uint64_t V_13 = 0;
	ByteU5BU5D_t4260760469* V_14 = NULL;
	PayloadData_t39926750 * V_15 = NULL;
	int32_t G_B3_0 = 0;
	int32_t G_B6_0 = 0;
	int32_t G_B9_0 = 0;
	int32_t G_B12_0 = 0;
	int32_t G_B15_0 = 0;
	String_t* G_B22_0 = NULL;
	int32_t G_B32_0 = 0;
	ByteU5BU5D_t4260760469* G_B35_0 = NULL;
	ByteU5BU5D_t4260760469* G_B41_0 = NULL;
	uint64_t G_B49_0 = 0;
	ByteU5BU5D_t4260760469* G_B56_0 = NULL;
	{
		ByteU5BU5D_t4260760469* L_0 = ___header0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		int32_t L_1 = 0;
		uint8_t L_2 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_1));
		if ((!(((uint32_t)((int32_t)((int32_t)L_2&(int32_t)((int32_t)128)))) == ((uint32_t)((int32_t)128)))))
		{
			goto IL_0019;
		}
	}
	{
		G_B3_0 = 1;
		goto IL_001a;
	}

IL_0019:
	{
		G_B3_0 = 0;
	}

IL_001a:
	{
		V_0 = G_B3_0;
		ByteU5BU5D_t4260760469* L_3 = ___header0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		int32_t L_4 = 0;
		uint8_t L_5 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		if ((!(((uint32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)64)))) == ((uint32_t)((int32_t)64)))))
		{
			goto IL_002e;
		}
	}
	{
		G_B6_0 = 1;
		goto IL_002f;
	}

IL_002e:
	{
		G_B6_0 = 0;
	}

IL_002f:
	{
		V_1 = G_B6_0;
		ByteU5BU5D_t4260760469* L_6 = ___header0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		int32_t L_7 = 0;
		uint8_t L_8 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		if ((!(((uint32_t)((int32_t)((int32_t)L_8&(int32_t)((int32_t)32)))) == ((uint32_t)((int32_t)32)))))
		{
			goto IL_0043;
		}
	}
	{
		G_B9_0 = 1;
		goto IL_0044;
	}

IL_0043:
	{
		G_B9_0 = 0;
	}

IL_0044:
	{
		V_2 = G_B9_0;
		ByteU5BU5D_t4260760469* L_9 = ___header0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		int32_t L_10 = 0;
		uint8_t L_11 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		if ((!(((uint32_t)((int32_t)((int32_t)L_11&(int32_t)((int32_t)16)))) == ((uint32_t)((int32_t)16)))))
		{
			goto IL_0058;
		}
	}
	{
		G_B12_0 = 1;
		goto IL_0059;
	}

IL_0058:
	{
		G_B12_0 = 0;
	}

IL_0059:
	{
		V_3 = G_B12_0;
		ByteU5BU5D_t4260760469* L_12 = ___header0;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 0);
		int32_t L_13 = 0;
		uint8_t L_14 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		V_4 = (((int32_t)((uint8_t)((int32_t)((int32_t)L_14&(int32_t)((int32_t)15))))));
		ByteU5BU5D_t4260760469* L_15 = ___header0;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 1);
		int32_t L_16 = 1;
		uint8_t L_17 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		if ((!(((uint32_t)((int32_t)((int32_t)L_17&(int32_t)((int32_t)128)))) == ((uint32_t)((int32_t)128)))))
		{
			goto IL_007c;
		}
	}
	{
		G_B15_0 = 1;
		goto IL_007d;
	}

IL_007c:
	{
		G_B15_0 = 0;
	}

IL_007d:
	{
		V_5 = G_B15_0;
		ByteU5BU5D_t4260760469* L_18 = ___header0;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 1);
		int32_t L_19 = 1;
		uint8_t L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		V_6 = (((int32_t)((uint8_t)((int32_t)((int32_t)L_20&(int32_t)((int32_t)127))))));
		uint8_t L_21 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t778194306_il2cpp_TypeInfo_var);
		bool L_22 = WebSocketFrame_isControl_m3029256346(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00a4;
		}
	}
	{
		uint8_t L_23 = V_0;
		if (L_23)
		{
			goto IL_00a4;
		}
	}
	{
		G_B22_0 = _stringLiteral3033030302;
		goto IL_00c2;
	}

IL_00a4:
	{
		uint8_t L_24 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t778194306_il2cpp_TypeInfo_var);
		bool L_25 = WebSocketFrame_isData_m69248783(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		if (L_25)
		{
			goto IL_00c1;
		}
	}
	{
		uint8_t L_26 = V_1;
		if ((!(((uint32_t)L_26) == ((uint32_t)1))))
		{
			goto IL_00c1;
		}
	}
	{
		G_B22_0 = _stringLiteral3081821550;
		goto IL_00c2;
	}

IL_00c1:
	{
		G_B22_0 = ((String_t*)(NULL));
	}

IL_00c2:
	{
		V_7 = G_B22_0;
		String_t* L_27 = V_7;
		if (!L_27)
		{
			goto IL_00d8;
		}
	}
	{
		String_t* L_28 = V_7;
		WebSocketException_t2311987812 * L_29 = (WebSocketException_t2311987812 *)il2cpp_codegen_object_new(WebSocketException_t2311987812_il2cpp_TypeInfo_var);
		WebSocketException__ctor_m960972328(L_29, ((int32_t)1003), L_28, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_29);
	}

IL_00d8:
	{
		uint8_t L_30 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t778194306_il2cpp_TypeInfo_var);
		bool L_31 = WebSocketFrame_isControl_m3029256346(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_00fd;
		}
	}
	{
		uint8_t L_32 = V_6;
		if ((((int32_t)L_32) <= ((int32_t)((int32_t)125))))
		{
			goto IL_00fd;
		}
	}
	{
		WebSocketException_t2311987812 * L_33 = (WebSocketException_t2311987812 *)il2cpp_codegen_object_new(WebSocketException_t2311987812_il2cpp_TypeInfo_var);
		WebSocketException__ctor_m960972328(L_33, ((int32_t)1007), _stringLiteral2490559370, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_33);
	}

IL_00fd:
	{
		WebSocketFrame_t778194306 * L_34 = (WebSocketFrame_t778194306 *)il2cpp_codegen_object_new(WebSocketFrame_t778194306_il2cpp_TypeInfo_var);
		WebSocketFrame__ctor_m2272980410(L_34, /*hidden argument*/NULL);
		V_8 = L_34;
		WebSocketFrame_t778194306 * L_35 = V_8;
		uint8_t L_36 = V_0;
		NullCheck(L_35);
		L_35->set__fin_1(L_36);
		WebSocketFrame_t778194306 * L_37 = V_8;
		uint8_t L_38 = V_1;
		NullCheck(L_37);
		L_37->set__rsv1_7(L_38);
		WebSocketFrame_t778194306 * L_39 = V_8;
		uint8_t L_40 = V_2;
		NullCheck(L_39);
		L_39->set__rsv2_8(L_40);
		WebSocketFrame_t778194306 * L_41 = V_8;
		uint8_t L_42 = V_3;
		NullCheck(L_41);
		L_41->set__rsv3_9(L_42);
		WebSocketFrame_t778194306 * L_43 = V_8;
		uint8_t L_44 = V_4;
		NullCheck(L_43);
		L_43->set__opcode_4(L_44);
		WebSocketFrame_t778194306 * L_45 = V_8;
		uint8_t L_46 = V_5;
		NullCheck(L_45);
		L_45->set__mask_2(L_46);
		WebSocketFrame_t778194306 * L_47 = V_8;
		uint8_t L_48 = V_6;
		NullCheck(L_47);
		L_47->set__payloadLength_6(L_48);
		uint8_t L_49 = V_6;
		if ((((int32_t)L_49) >= ((int32_t)((int32_t)126))))
		{
			goto IL_014e;
		}
	}
	{
		G_B32_0 = 0;
		goto IL_015e;
	}

IL_014e:
	{
		uint8_t L_50 = V_6;
		if ((!(((uint32_t)L_50) == ((uint32_t)((int32_t)126)))))
		{
			goto IL_015d;
		}
	}
	{
		G_B32_0 = 2;
		goto IL_015e;
	}

IL_015d:
	{
		G_B32_0 = 8;
	}

IL_015e:
	{
		V_9 = G_B32_0;
		int32_t L_51 = V_9;
		if ((((int32_t)L_51) <= ((int32_t)0)))
		{
			goto IL_0175;
		}
	}
	{
		Stream_t1561764144 * L_52 = ___stream1;
		int32_t L_53 = V_9;
		ByteU5BU5D_t4260760469* L_54 = Ext_ReadBytes_m3416233938(NULL /*static, unused*/, L_52, L_53, /*hidden argument*/NULL);
		G_B35_0 = L_54;
		goto IL_017b;
	}

IL_0175:
	{
		G_B35_0 = ((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)0));
	}

IL_017b:
	{
		V_10 = G_B35_0;
		int32_t L_55 = V_9;
		if ((((int32_t)L_55) <= ((int32_t)0)))
		{
			goto IL_019b;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_56 = V_10;
		NullCheck(L_56);
		int32_t L_57 = V_9;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_56)->max_length))))) == ((int32_t)L_57)))
		{
			goto IL_019b;
		}
	}
	{
		WebSocketException_t2311987812 * L_58 = (WebSocketException_t2311987812 *)il2cpp_codegen_object_new(WebSocketException_t2311987812_il2cpp_TypeInfo_var);
		WebSocketException__ctor_m3830227690(L_58, _stringLiteral1200591507, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_58);
	}

IL_019b:
	{
		WebSocketFrame_t778194306 * L_59 = V_8;
		ByteU5BU5D_t4260760469* L_60 = V_10;
		NullCheck(L_59);
		L_59->set__extPayloadLength_0(L_60);
		uint8_t L_61 = V_5;
		V_11 = (bool)((((int32_t)L_61) == ((int32_t)1))? 1 : 0);
		bool L_62 = V_11;
		if (!L_62)
		{
			goto IL_01be;
		}
	}
	{
		Stream_t1561764144 * L_63 = ___stream1;
		ByteU5BU5D_t4260760469* L_64 = Ext_ReadBytes_m3416233938(NULL /*static, unused*/, L_63, 4, /*hidden argument*/NULL);
		G_B41_0 = L_64;
		goto IL_01c4;
	}

IL_01be:
	{
		G_B41_0 = ((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)0));
	}

IL_01c4:
	{
		V_12 = G_B41_0;
		bool L_65 = V_11;
		if (!L_65)
		{
			goto IL_01e2;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_66 = V_12;
		NullCheck(L_66);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_66)->max_length))))) == ((int32_t)4)))
		{
			goto IL_01e2;
		}
	}
	{
		WebSocketException_t2311987812 * L_67 = (WebSocketException_t2311987812 *)il2cpp_codegen_object_new(WebSocketException_t2311987812_il2cpp_TypeInfo_var);
		WebSocketException__ctor_m3830227690(L_67, _stringLiteral1241564285, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_67);
	}

IL_01e2:
	{
		WebSocketFrame_t778194306 * L_68 = V_8;
		ByteU5BU5D_t4260760469* L_69 = V_12;
		NullCheck(L_68);
		L_68->set__maskingKey_3(L_69);
		uint8_t L_70 = V_6;
		if ((((int32_t)L_70) >= ((int32_t)((int32_t)126))))
		{
			goto IL_01fc;
		}
	}
	{
		uint8_t L_71 = V_6;
		G_B49_0 = ((uint64_t)((((int64_t)((uint64_t)L_71)))));
		goto IL_021b;
	}

IL_01fc:
	{
		uint8_t L_72 = V_6;
		if ((!(((uint32_t)L_72) == ((uint32_t)((int32_t)126)))))
		{
			goto IL_0213;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_73 = V_10;
		uint16_t L_74 = Ext_ToUInt16_m2711859920(NULL /*static, unused*/, L_73, 1, /*hidden argument*/NULL);
		G_B49_0 = ((uint64_t)((((int64_t)((uint64_t)L_74)))));
		goto IL_021b;
	}

IL_0213:
	{
		ByteU5BU5D_t4260760469* L_75 = V_10;
		uint64_t L_76 = Ext_ToUInt64_m2876712304(NULL /*static, unused*/, L_75, 1, /*hidden argument*/NULL);
		G_B49_0 = L_76;
	}

IL_021b:
	{
		V_13 = G_B49_0;
		V_14 = (ByteU5BU5D_t4260760469*)NULL;
		uint64_t L_77 = V_13;
		if ((!(((uint64_t)L_77) > ((uint64_t)(((int64_t)((int64_t)0)))))))
		{
			goto IL_0296;
		}
	}
	{
		uint8_t L_78 = V_6;
		if ((((int32_t)L_78) <= ((int32_t)((int32_t)126))))
		{
			goto IL_0252;
		}
	}
	{
		uint64_t L_79 = V_13;
		if ((!(((uint64_t)L_79) > ((uint64_t)((int64_t)std::numeric_limits<int64_t>::max())))))
		{
			goto IL_0252;
		}
	}
	{
		WebSocketException_t2311987812 * L_80 = (WebSocketException_t2311987812 *)il2cpp_codegen_object_new(WebSocketException_t2311987812_il2cpp_TypeInfo_var);
		WebSocketException__ctor_m960972328(L_80, ((int32_t)1009), _stringLiteral1980370780, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_80);
	}

IL_0252:
	{
		uint8_t L_81 = V_6;
		if ((((int32_t)L_81) <= ((int32_t)((int32_t)126))))
		{
			goto IL_026d;
		}
	}
	{
		Stream_t1561764144 * L_82 = ___stream1;
		uint64_t L_83 = V_13;
		ByteU5BU5D_t4260760469* L_84 = Ext_ReadBytes_m2955720804(NULL /*static, unused*/, L_82, L_83, ((int32_t)1024), /*hidden argument*/NULL);
		G_B56_0 = L_84;
		goto IL_0276;
	}

IL_026d:
	{
		Stream_t1561764144 * L_85 = ___stream1;
		uint64_t L_86 = V_13;
		ByteU5BU5D_t4260760469* L_87 = Ext_ReadBytes_m3416233938(NULL /*static, unused*/, L_85, (((int32_t)((int32_t)L_86))), /*hidden argument*/NULL);
		G_B56_0 = L_87;
	}

IL_0276:
	{
		V_14 = G_B56_0;
		ByteU5BU5D_t4260760469* L_88 = V_14;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_88);
		int64_t L_89 = Array_get_LongLength_m1209728916((Il2CppArray *)(Il2CppArray *)L_88, /*hidden argument*/NULL);
		uint64_t L_90 = V_13;
		if ((((int64_t)L_89) == ((int64_t)L_90)))
		{
			goto IL_0291;
		}
	}
	{
		WebSocketException_t2311987812 * L_91 = (WebSocketException_t2311987812 *)il2cpp_codegen_object_new(WebSocketException_t2311987812_il2cpp_TypeInfo_var);
		WebSocketException__ctor_m3830227690(L_91, _stringLiteral4176728934, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_91);
	}

IL_0291:
	{
		goto IL_029e;
	}

IL_0296:
	{
		V_14 = ((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)0));
	}

IL_029e:
	{
		ByteU5BU5D_t4260760469* L_92 = V_14;
		bool L_93 = V_11;
		PayloadData_t39926750 * L_94 = (PayloadData_t39926750 *)il2cpp_codegen_object_new(PayloadData_t39926750_il2cpp_TypeInfo_var);
		PayloadData__ctor_m3599656354(L_94, L_92, L_93, /*hidden argument*/NULL);
		V_15 = L_94;
		bool L_95 = V_11;
		if (!L_95)
		{
			goto IL_02d4;
		}
	}
	{
		bool L_96 = ___unmask2;
		if (!L_96)
		{
			goto IL_02d4;
		}
	}
	{
		PayloadData_t39926750 * L_97 = V_15;
		ByteU5BU5D_t4260760469* L_98 = V_12;
		NullCheck(L_97);
		PayloadData_Mask_m2120375719(L_97, L_98, /*hidden argument*/NULL);
		WebSocketFrame_t778194306 * L_99 = V_8;
		NullCheck(L_99);
		L_99->set__mask_2(0);
		WebSocketFrame_t778194306 * L_100 = V_8;
		NullCheck(L_100);
		L_100->set__maskingKey_3(((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)0)));
	}

IL_02d4:
	{
		WebSocketFrame_t778194306 * L_101 = V_8;
		PayloadData_t39926750 * L_102 = V_15;
		NullCheck(L_101);
		L_101->set__payloadData_5(L_102);
		WebSocketFrame_t778194306 * L_103 = V_8;
		return L_103;
	}
}
// System.String WebSocketSharp.WebSocketFrame::print(WebSocketSharp.WebSocketFrame)
extern Il2CppClass* Opcode_t3782140426_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* BitConverter_t1260277767_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t2012439129_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Fin_t3262160529_il2cpp_TypeInfo_var;
extern Il2CppClass* Rsv_t3262172379_il2cpp_TypeInfo_var;
extern Il2CppClass* Mask_t3422653544_il2cpp_TypeInfo_var;
extern Il2CppClass* Byte_t2862609660_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3339474104;
extern Il2CppCodeGenString* _stringLiteral711258247;
extern const uint32_t WebSocketFrame_print_m1807623609_MetadataUsageId;
extern "C"  String_t* WebSocketFrame_print_m1807623609 (Il2CppObject * __this /* static, unused */, WebSocketFrame_t778194306 * ___frame0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_print_m1807623609_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	uint8_t V_1 = 0x0;
	ByteU5BU5D_t4260760469* V_2 = NULL;
	int32_t V_3 = 0;
	String_t* V_4 = NULL;
	bool V_5 = false;
	String_t* V_6 = NULL;
	String_t* V_7 = NULL;
	String_t* V_8 = NULL;
	uint16_t V_9 = 0;
	uint64_t V_10 = 0;
	String_t* G_B5_0 = NULL;
	String_t* G_B8_0 = NULL;
	String_t* G_B17_0 = NULL;
	{
		WebSocketFrame_t778194306 * L_0 = ___frame0;
		NullCheck(L_0);
		uint8_t L_1 = L_0->get__opcode_4();
		uint8_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Opcode_t3782140426_il2cpp_TypeInfo_var, &L_2);
		NullCheck((Enum_t2862688501 *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2862688501 *)L_3);
		V_0 = L_4;
		WebSocketFrame_t778194306 * L_5 = ___frame0;
		NullCheck(L_5);
		uint8_t L_6 = L_5->get__payloadLength_6();
		V_1 = L_6;
		WebSocketFrame_t778194306 * L_7 = ___frame0;
		NullCheck(L_7);
		ByteU5BU5D_t4260760469* L_8 = L_7->get__extPayloadLength_0();
		V_2 = L_8;
		ByteU5BU5D_t4260760469* L_9 = V_2;
		NullCheck(L_9);
		V_3 = (((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length))));
		int32_t L_10 = V_3;
		if ((!(((uint32_t)L_10) == ((uint32_t)2))))
		{
			goto IL_003f;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_11 = V_2;
		uint16_t L_12 = Ext_ToUInt16_m2711859920(NULL /*static, unused*/, L_11, 1, /*hidden argument*/NULL);
		V_9 = L_12;
		String_t* L_13 = UInt16_ToString_m741885559((&V_9), /*hidden argument*/NULL);
		G_B5_0 = L_13;
		goto IL_0060;
	}

IL_003f:
	{
		int32_t L_14 = V_3;
		if ((!(((uint32_t)L_14) == ((uint32_t)8))))
		{
			goto IL_005b;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_15 = V_2;
		uint64_t L_16 = Ext_ToUInt64_m2876712304(NULL /*static, unused*/, L_15, 1, /*hidden argument*/NULL);
		V_10 = L_16;
		String_t* L_17 = UInt64_ToString_m3095865744((&V_10), /*hidden argument*/NULL);
		G_B5_0 = L_17;
		goto IL_0060;
	}

IL_005b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B5_0 = L_18;
	}

IL_0060:
	{
		V_4 = G_B5_0;
		WebSocketFrame_t778194306 * L_19 = ___frame0;
		NullCheck(L_19);
		bool L_20 = WebSocketFrame_get_IsMasked_m1404103348(L_19, /*hidden argument*/NULL);
		V_5 = L_20;
		bool L_21 = V_5;
		if (!L_21)
		{
			goto IL_0081;
		}
	}
	{
		WebSocketFrame_t778194306 * L_22 = ___frame0;
		NullCheck(L_22);
		ByteU5BU5D_t4260760469* L_23 = L_22->get__maskingKey_3();
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t1260277767_il2cpp_TypeInfo_var);
		String_t* L_24 = BitConverter_ToString_m1865594174(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		G_B8_0 = L_24;
		goto IL_0086;
	}

IL_0081:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_25 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B8_0 = L_25;
	}

IL_0086:
	{
		V_6 = G_B8_0;
		uint8_t L_26 = V_1;
		if (L_26)
		{
			goto IL_0098;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B17_0 = L_27;
		goto IL_00f6;
	}

IL_0098:
	{
		int32_t L_28 = V_3;
		if ((((int32_t)L_28) <= ((int32_t)0)))
		{
			goto IL_00b4;
		}
	}
	{
		String_t* L_29 = V_0;
		NullCheck(L_29);
		String_t* L_30 = String_ToLower_m2421900555(L_29, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_31 = String_Format_m2471250780(NULL /*static, unused*/, _stringLiteral3339474104, L_30, /*hidden argument*/NULL);
		G_B17_0 = L_31;
		goto IL_00f6;
	}

IL_00b4:
	{
		bool L_32 = V_5;
		if (L_32)
		{
			goto IL_00eb;
		}
	}
	{
		WebSocketFrame_t778194306 * L_33 = ___frame0;
		NullCheck(L_33);
		bool L_34 = WebSocketFrame_get_IsFragmented_m98582552(L_33, /*hidden argument*/NULL);
		if (L_34)
		{
			goto IL_00eb;
		}
	}
	{
		WebSocketFrame_t778194306 * L_35 = ___frame0;
		NullCheck(L_35);
		bool L_36 = WebSocketFrame_get_IsText_m3459342550(L_35, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_00eb;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t2012439129_il2cpp_TypeInfo_var);
		Encoding_t2012439129 * L_37 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		WebSocketFrame_t778194306 * L_38 = ___frame0;
		NullCheck(L_38);
		PayloadData_t39926750 * L_39 = L_38->get__payloadData_5();
		NullCheck(L_39);
		ByteU5BU5D_t4260760469* L_40 = PayloadData_get_ApplicationData_m3147947059(L_39, /*hidden argument*/NULL);
		NullCheck(L_37);
		String_t* L_41 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t4260760469* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_37, L_40);
		G_B17_0 = L_41;
		goto IL_00f6;
	}

IL_00eb:
	{
		WebSocketFrame_t778194306 * L_42 = ___frame0;
		NullCheck(L_42);
		PayloadData_t39926750 * L_43 = L_42->get__payloadData_5();
		NullCheck(L_43);
		String_t* L_44 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String WebSocketSharp.PayloadData::ToString() */, L_43);
		G_B17_0 = L_44;
	}

IL_00f6:
	{
		V_7 = G_B17_0;
		V_8 = _stringLiteral711258247;
		String_t* L_45 = V_8;
		ObjectU5BU5D_t1108656482* L_46 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)((int32_t)10)));
		WebSocketFrame_t778194306 * L_47 = ___frame0;
		NullCheck(L_47);
		uint8_t L_48 = L_47->get__fin_1();
		uint8_t L_49 = L_48;
		Il2CppObject * L_50 = Box(Fin_t3262160529_il2cpp_TypeInfo_var, &L_49);
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, 0);
		ArrayElementTypeCheck (L_46, L_50);
		(L_46)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_50);
		ObjectU5BU5D_t1108656482* L_51 = L_46;
		WebSocketFrame_t778194306 * L_52 = ___frame0;
		NullCheck(L_52);
		uint8_t L_53 = L_52->get__rsv1_7();
		uint8_t L_54 = L_53;
		Il2CppObject * L_55 = Box(Rsv_t3262172379_il2cpp_TypeInfo_var, &L_54);
		NullCheck(L_51);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_51, 1);
		ArrayElementTypeCheck (L_51, L_55);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_55);
		ObjectU5BU5D_t1108656482* L_56 = L_51;
		WebSocketFrame_t778194306 * L_57 = ___frame0;
		NullCheck(L_57);
		uint8_t L_58 = L_57->get__rsv2_8();
		uint8_t L_59 = L_58;
		Il2CppObject * L_60 = Box(Rsv_t3262172379_il2cpp_TypeInfo_var, &L_59);
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, 2);
		ArrayElementTypeCheck (L_56, L_60);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_60);
		ObjectU5BU5D_t1108656482* L_61 = L_56;
		WebSocketFrame_t778194306 * L_62 = ___frame0;
		NullCheck(L_62);
		uint8_t L_63 = L_62->get__rsv3_9();
		uint8_t L_64 = L_63;
		Il2CppObject * L_65 = Box(Rsv_t3262172379_il2cpp_TypeInfo_var, &L_64);
		NullCheck(L_61);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_61, 3);
		ArrayElementTypeCheck (L_61, L_65);
		(L_61)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_65);
		ObjectU5BU5D_t1108656482* L_66 = L_61;
		String_t* L_67 = V_0;
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, 4);
		ArrayElementTypeCheck (L_66, L_67);
		(L_66)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_67);
		ObjectU5BU5D_t1108656482* L_68 = L_66;
		WebSocketFrame_t778194306 * L_69 = ___frame0;
		NullCheck(L_69);
		uint8_t L_70 = L_69->get__mask_2();
		uint8_t L_71 = L_70;
		Il2CppObject * L_72 = Box(Mask_t3422653544_il2cpp_TypeInfo_var, &L_71);
		NullCheck(L_68);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_68, 5);
		ArrayElementTypeCheck (L_68, L_72);
		(L_68)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_72);
		ObjectU5BU5D_t1108656482* L_73 = L_68;
		uint8_t L_74 = V_1;
		uint8_t L_75 = L_74;
		Il2CppObject * L_76 = Box(Byte_t2862609660_il2cpp_TypeInfo_var, &L_75);
		NullCheck(L_73);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_73, 6);
		ArrayElementTypeCheck (L_73, L_76);
		(L_73)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)L_76);
		ObjectU5BU5D_t1108656482* L_77 = L_73;
		String_t* L_78 = V_4;
		NullCheck(L_77);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_77, 7);
		ArrayElementTypeCheck (L_77, L_78);
		(L_77)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)L_78);
		ObjectU5BU5D_t1108656482* L_79 = L_77;
		String_t* L_80 = V_6;
		NullCheck(L_79);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_79, 8);
		ArrayElementTypeCheck (L_79, L_80);
		(L_79)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)L_80);
		ObjectU5BU5D_t1108656482* L_81 = L_79;
		String_t* L_82 = V_7;
		NullCheck(L_81);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_81, ((int32_t)9));
		ArrayElementTypeCheck (L_81, L_82);
		(L_81)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)L_82);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_83 = String_Format_m4050103162(NULL /*static, unused*/, L_45, L_81, /*hidden argument*/NULL);
		return L_83;
	}
}
// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::CreateCloseFrame(WebSocketSharp.Mask,WebSocketSharp.PayloadData)
extern Il2CppClass* WebSocketFrame_t778194306_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketFrame_CreateCloseFrame_m431346455_MetadataUsageId;
extern "C"  WebSocketFrame_t778194306 * WebSocketFrame_CreateCloseFrame_m431346455 (Il2CppObject * __this /* static, unused */, uint8_t ___mask0, PayloadData_t39926750 * ___payload1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_CreateCloseFrame_m431346455_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		uint8_t L_0 = ___mask0;
		PayloadData_t39926750 * L_1 = ___payload1;
		WebSocketFrame_t778194306 * L_2 = (WebSocketFrame_t778194306 *)il2cpp_codegen_object_new(WebSocketFrame_t778194306_il2cpp_TypeInfo_var);
		WebSocketFrame__ctor_m1992914661(L_2, 8, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::CreatePongFrame(WebSocketSharp.Mask,WebSocketSharp.PayloadData)
extern Il2CppClass* WebSocketFrame_t778194306_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketFrame_CreatePongFrame_m950933809_MetadataUsageId;
extern "C"  WebSocketFrame_t778194306 * WebSocketFrame_CreatePongFrame_m950933809 (Il2CppObject * __this /* static, unused */, uint8_t ___mask0, PayloadData_t39926750 * ___payload1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_CreatePongFrame_m950933809_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		uint8_t L_0 = ___mask0;
		PayloadData_t39926750 * L_1 = ___payload1;
		WebSocketFrame_t778194306 * L_2 = (WebSocketFrame_t778194306 *)il2cpp_codegen_object_new(WebSocketFrame_t778194306_il2cpp_TypeInfo_var);
		WebSocketFrame__ctor_m1992914661(L_2, ((int32_t)10), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::CreateCloseFrame(WebSocketSharp.Mask,System.Byte[])
extern Il2CppClass* PayloadData_t39926750_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocketFrame_t778194306_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketFrame_CreateCloseFrame_m3543925733_MetadataUsageId;
extern "C"  WebSocketFrame_t778194306 * WebSocketFrame_CreateCloseFrame_m3543925733 (Il2CppObject * __this /* static, unused */, uint8_t ___mask0, ByteU5BU5D_t4260760469* ___data1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_CreateCloseFrame_m3543925733_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		uint8_t L_0 = ___mask0;
		ByteU5BU5D_t4260760469* L_1 = ___data1;
		PayloadData_t39926750 * L_2 = (PayloadData_t39926750 *)il2cpp_codegen_object_new(PayloadData_t39926750_il2cpp_TypeInfo_var);
		PayloadData__ctor_m719751291(L_2, L_1, /*hidden argument*/NULL);
		WebSocketFrame_t778194306 * L_3 = (WebSocketFrame_t778194306 *)il2cpp_codegen_object_new(WebSocketFrame_t778194306_il2cpp_TypeInfo_var);
		WebSocketFrame__ctor_m1992914661(L_3, 8, L_0, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::CreateCloseFrame(WebSocketSharp.Mask,WebSocketSharp.CloseStatusCode,System.String)
extern Il2CppClass* PayloadData_t39926750_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocketFrame_t778194306_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketFrame_CreateCloseFrame_m110968692_MetadataUsageId;
extern "C"  WebSocketFrame_t778194306 * WebSocketFrame_CreateCloseFrame_m110968692 (Il2CppObject * __this /* static, unused */, uint8_t ___mask0, uint16_t ___code1, String_t* ___reason2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_CreateCloseFrame_m110968692_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		uint8_t L_0 = ___mask0;
		uint16_t L_1 = ___code1;
		String_t* L_2 = ___reason2;
		ByteU5BU5D_t4260760469* L_3 = Ext_Append_m501190413(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		PayloadData_t39926750 * L_4 = (PayloadData_t39926750 *)il2cpp_codegen_object_new(PayloadData_t39926750_il2cpp_TypeInfo_var);
		PayloadData__ctor_m719751291(L_4, L_3, /*hidden argument*/NULL);
		WebSocketFrame_t778194306 * L_5 = (WebSocketFrame_t778194306 *)il2cpp_codegen_object_new(WebSocketFrame_t778194306_il2cpp_TypeInfo_var);
		WebSocketFrame__ctor_m1992914661(L_5, 8, L_0, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::CreateFrame(WebSocketSharp.Fin,WebSocketSharp.Opcode,WebSocketSharp.Mask,System.Byte[],System.Boolean)
extern Il2CppClass* PayloadData_t39926750_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocketFrame_t778194306_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketFrame_CreateFrame_m309663431_MetadataUsageId;
extern "C"  WebSocketFrame_t778194306 * WebSocketFrame_CreateFrame_m309663431 (Il2CppObject * __this /* static, unused */, uint8_t ___fin0, uint8_t ___opcode1, uint8_t ___mask2, ByteU5BU5D_t4260760469* ___data3, bool ___compressed4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_CreateFrame_m309663431_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		uint8_t L_0 = ___fin0;
		uint8_t L_1 = ___opcode1;
		uint8_t L_2 = ___mask2;
		ByteU5BU5D_t4260760469* L_3 = ___data3;
		PayloadData_t39926750 * L_4 = (PayloadData_t39926750 *)il2cpp_codegen_object_new(PayloadData_t39926750_il2cpp_TypeInfo_var);
		PayloadData__ctor_m719751291(L_4, L_3, /*hidden argument*/NULL);
		bool L_5 = ___compressed4;
		WebSocketFrame_t778194306 * L_6 = (WebSocketFrame_t778194306 *)il2cpp_codegen_object_new(WebSocketFrame_t778194306_il2cpp_TypeInfo_var);
		WebSocketFrame__ctor_m3336031814(L_6, L_0, L_1, L_2, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::CreatePingFrame(WebSocketSharp.Mask)
extern Il2CppClass* PayloadData_t39926750_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocketFrame_t778194306_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketFrame_CreatePingFrame_m982549890_MetadataUsageId;
extern "C"  WebSocketFrame_t778194306 * WebSocketFrame_CreatePingFrame_m982549890 (Il2CppObject * __this /* static, unused */, uint8_t ___mask0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_CreatePingFrame_m982549890_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		uint8_t L_0 = ___mask0;
		PayloadData_t39926750 * L_1 = (PayloadData_t39926750 *)il2cpp_codegen_object_new(PayloadData_t39926750_il2cpp_TypeInfo_var);
		PayloadData__ctor_m1953054222(L_1, /*hidden argument*/NULL);
		WebSocketFrame_t778194306 * L_2 = (WebSocketFrame_t778194306 *)il2cpp_codegen_object_new(WebSocketFrame_t778194306_il2cpp_TypeInfo_var);
		WebSocketFrame__ctor_m1992914661(L_2, ((int32_t)9), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::CreatePingFrame(WebSocketSharp.Mask,System.Byte[])
extern Il2CppClass* PayloadData_t39926750_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocketFrame_t778194306_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketFrame_CreatePingFrame_m3019981573_MetadataUsageId;
extern "C"  WebSocketFrame_t778194306 * WebSocketFrame_CreatePingFrame_m3019981573 (Il2CppObject * __this /* static, unused */, uint8_t ___mask0, ByteU5BU5D_t4260760469* ___data1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_CreatePingFrame_m3019981573_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		uint8_t L_0 = ___mask0;
		ByteU5BU5D_t4260760469* L_1 = ___data1;
		PayloadData_t39926750 * L_2 = (PayloadData_t39926750 *)il2cpp_codegen_object_new(PayloadData_t39926750_il2cpp_TypeInfo_var);
		PayloadData__ctor_m719751291(L_2, L_1, /*hidden argument*/NULL);
		WebSocketFrame_t778194306 * L_3 = (WebSocketFrame_t778194306 *)il2cpp_codegen_object_new(WebSocketFrame_t778194306_il2cpp_TypeInfo_var);
		WebSocketFrame__ctor_m1992914661(L_3, ((int32_t)9), L_0, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Byte> WebSocketSharp.WebSocketFrame::GetEnumerator()
extern Il2CppClass* U3CGetEnumeratorU3Ec__Iterator20_t4155755117_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketFrame_GetEnumerator_m2490913985_MetadataUsageId;
extern "C"  Il2CppObject* WebSocketFrame_GetEnumerator_m2490913985 (WebSocketFrame_t778194306 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_GetEnumerator_m2490913985_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetEnumeratorU3Ec__Iterator20_t4155755117 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator20_t4155755117 * L_0 = (U3CGetEnumeratorU3Ec__Iterator20_t4155755117 *)il2cpp_codegen_object_new(U3CGetEnumeratorU3Ec__Iterator20_t4155755117_il2cpp_TypeInfo_var);
		U3CGetEnumeratorU3Ec__Iterator20__ctor_m2089199822(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetEnumeratorU3Ec__Iterator20_t4155755117 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_5(__this);
		U3CGetEnumeratorU3Ec__Iterator20_t4155755117 * L_2 = V_0;
		return L_2;
	}
}
// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::Parse(System.Byte[])
extern Il2CppClass* WebSocketFrame_t778194306_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketFrame_Parse_m3362537340_MetadataUsageId;
extern "C"  WebSocketFrame_t778194306 * WebSocketFrame_Parse_m3362537340 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___src0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_Parse_m3362537340_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ByteU5BU5D_t4260760469* L_0 = ___src0;
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t778194306_il2cpp_TypeInfo_var);
		WebSocketFrame_t778194306 * L_1 = WebSocketFrame_Parse_m93695105(NULL /*static, unused*/, L_0, (bool)1, /*hidden argument*/NULL);
		return L_1;
	}
}
// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::Parse(System.IO.Stream)
extern Il2CppClass* WebSocketFrame_t778194306_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketFrame_Parse_m3554870788_MetadataUsageId;
extern "C"  WebSocketFrame_t778194306 * WebSocketFrame_Parse_m3554870788 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___stream0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_Parse_m3554870788_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Stream_t1561764144 * L_0 = ___stream0;
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t778194306_il2cpp_TypeInfo_var);
		WebSocketFrame_t778194306 * L_1 = WebSocketFrame_Parse_m2256012537(NULL /*static, unused*/, L_0, (bool)1, /*hidden argument*/NULL);
		return L_1;
	}
}
// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::Parse(System.Byte[],System.Boolean)
extern Il2CppClass* MemoryStream_t418716369_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocketFrame_t778194306_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketFrame_Parse_m93695105_MetadataUsageId;
extern "C"  WebSocketFrame_t778194306 * WebSocketFrame_Parse_m93695105 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___src0, bool ___unmask1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_Parse_m93695105_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MemoryStream_t418716369 * V_0 = NULL;
	WebSocketFrame_t778194306 * V_1 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ByteU5BU5D_t4260760469* L_0 = ___src0;
		MemoryStream_t418716369 * L_1 = (MemoryStream_t418716369 *)il2cpp_codegen_object_new(MemoryStream_t418716369_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1231145921(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			MemoryStream_t418716369 * L_2 = V_0;
			bool L_3 = ___unmask1;
			IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t778194306_il2cpp_TypeInfo_var);
			WebSocketFrame_t778194306 * L_4 = WebSocketFrame_Parse_m2256012537(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
			V_1 = L_4;
			IL2CPP_LEAVE(0x26, FINALLY_0019);
		}

IL_0014:
		{
			; // IL_0014: leave IL_0026
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0019;
	}

FINALLY_0019:
	{ // begin finally (depth: 1)
		{
			MemoryStream_t418716369 * L_5 = V_0;
			if (!L_5)
			{
				goto IL_0025;
			}
		}

IL_001f:
		{
			MemoryStream_t418716369 * L_6 = V_0;
			NullCheck(L_6);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_6);
		}

IL_0025:
		{
			IL2CPP_END_FINALLY(25)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(25)
	{
		IL2CPP_JUMP_TBL(0x26, IL_0026)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0026:
	{
		WebSocketFrame_t778194306 * L_7 = V_1;
		return L_7;
	}
}
// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::Parse(System.IO.Stream,System.Boolean)
extern Il2CppClass* WebSocketException_t2311987812_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocketFrame_t778194306_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3470785642;
extern const uint32_t WebSocketFrame_Parse_m2256012537_MetadataUsageId;
extern "C"  WebSocketFrame_t778194306 * WebSocketFrame_Parse_m2256012537 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___stream0, bool ___unmask1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_Parse_m2256012537_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t4260760469* V_0 = NULL;
	{
		Stream_t1561764144 * L_0 = ___stream0;
		ByteU5BU5D_t4260760469* L_1 = Ext_ReadBytes_m3416233938(NULL /*static, unused*/, L_0, 2, /*hidden argument*/NULL);
		V_0 = L_1;
		ByteU5BU5D_t4260760469* L_2 = V_0;
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))) == ((int32_t)2)))
		{
			goto IL_001c;
		}
	}
	{
		WebSocketException_t2311987812 * L_3 = (WebSocketException_t2311987812 *)il2cpp_codegen_object_new(WebSocketException_t2311987812_il2cpp_TypeInfo_var);
		WebSocketException__ctor_m3830227690(L_3, _stringLiteral3470785642, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001c:
	{
		ByteU5BU5D_t4260760469* L_4 = V_0;
		Stream_t1561764144 * L_5 = ___stream0;
		bool L_6 = ___unmask1;
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t778194306_il2cpp_TypeInfo_var);
		WebSocketFrame_t778194306 * L_7 = WebSocketFrame_parse_m2392405532(NULL /*static, unused*/, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void WebSocketSharp.WebSocketFrame::ParseAsync(System.IO.Stream,System.Action`1<WebSocketSharp.WebSocketFrame>)
extern Il2CppClass* WebSocketFrame_t778194306_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketFrame_ParseAsync_m1958002905_MetadataUsageId;
extern "C"  void WebSocketFrame_ParseAsync_m1958002905 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___stream0, Action_1_t1174010442 * ___completed1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_ParseAsync_m1958002905_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Stream_t1561764144 * L_0 = ___stream0;
		Action_1_t1174010442 * L_1 = ___completed1;
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t778194306_il2cpp_TypeInfo_var);
		WebSocketFrame_ParseAsync_m1471908516(NULL /*static, unused*/, L_0, (bool)1, L_1, (Action_1_t92447661 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketFrame::ParseAsync(System.IO.Stream,System.Action`1<WebSocketSharp.WebSocketFrame>,System.Action`1<System.Exception>)
extern Il2CppClass* WebSocketFrame_t778194306_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketFrame_ParseAsync_m3960604397_MetadataUsageId;
extern "C"  void WebSocketFrame_ParseAsync_m3960604397 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___stream0, Action_1_t1174010442 * ___completed1, Action_1_t92447661 * ___error2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_ParseAsync_m3960604397_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Stream_t1561764144 * L_0 = ___stream0;
		Action_1_t1174010442 * L_1 = ___completed1;
		Action_1_t92447661 * L_2 = ___error2;
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t778194306_il2cpp_TypeInfo_var);
		WebSocketFrame_ParseAsync_m1471908516(NULL /*static, unused*/, L_0, (bool)1, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketFrame::ParseAsync(System.IO.Stream,System.Boolean,System.Action`1<WebSocketSharp.WebSocketFrame>,System.Action`1<System.Exception>)
extern Il2CppClass* U3CParseAsyncU3Ec__AnonStorey31_t1758512846_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t361609309_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CParseAsyncU3Ec__AnonStorey31_U3CU3Em__1A_m283814771_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m2858737810_MethodInfo_var;
extern const uint32_t WebSocketFrame_ParseAsync_m1471908516_MetadataUsageId;
extern "C"  void WebSocketFrame_ParseAsync_m1471908516 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___stream0, bool ___unmask1, Action_1_t1174010442 * ___completed2, Action_1_t92447661 * ___error3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_ParseAsync_m1471908516_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CParseAsyncU3Ec__AnonStorey31_t1758512846 * V_0 = NULL;
	{
		U3CParseAsyncU3Ec__AnonStorey31_t1758512846 * L_0 = (U3CParseAsyncU3Ec__AnonStorey31_t1758512846 *)il2cpp_codegen_object_new(U3CParseAsyncU3Ec__AnonStorey31_t1758512846_il2cpp_TypeInfo_var);
		U3CParseAsyncU3Ec__AnonStorey31__ctor_m332244573(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CParseAsyncU3Ec__AnonStorey31_t1758512846 * L_1 = V_0;
		Stream_t1561764144 * L_2 = ___stream0;
		NullCheck(L_1);
		L_1->set_stream_0(L_2);
		U3CParseAsyncU3Ec__AnonStorey31_t1758512846 * L_3 = V_0;
		bool L_4 = ___unmask1;
		NullCheck(L_3);
		L_3->set_unmask_1(L_4);
		U3CParseAsyncU3Ec__AnonStorey31_t1758512846 * L_5 = V_0;
		Action_1_t1174010442 * L_6 = ___completed2;
		NullCheck(L_5);
		L_5->set_completed_2(L_6);
		U3CParseAsyncU3Ec__AnonStorey31_t1758512846 * L_7 = V_0;
		NullCheck(L_7);
		Stream_t1561764144 * L_8 = L_7->get_stream_0();
		U3CParseAsyncU3Ec__AnonStorey31_t1758512846 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)U3CParseAsyncU3Ec__AnonStorey31_U3CU3Em__1A_m283814771_MethodInfo_var);
		Action_1_t361609309 * L_11 = (Action_1_t361609309 *)il2cpp_codegen_object_new(Action_1_t361609309_il2cpp_TypeInfo_var);
		Action_1__ctor_m2858737810(L_11, L_9, L_10, /*hidden argument*/Action_1__ctor_m2858737810_MethodInfo_var);
		Action_1_t92447661 * L_12 = ___error3;
		Ext_ReadBytesAsync_m2558951989(NULL /*static, unused*/, L_8, 2, L_11, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketFrame::Print(System.Boolean)
extern Il2CppClass* WebSocketFrame_t778194306_il2cpp_TypeInfo_var;
extern Il2CppClass* Console_t1363597357_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketFrame_Print_m2623923964_MetadataUsageId;
extern "C"  void WebSocketFrame_Print_m2623923964 (WebSocketFrame_t778194306 * __this, bool ___dumped0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_Print_m2623923964_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	{
		bool L_0 = ___dumped0;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t778194306_il2cpp_TypeInfo_var);
		String_t* L_1 = WebSocketFrame_dump_m3839803398(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t778194306_il2cpp_TypeInfo_var);
		String_t* L_2 = WebSocketFrame_print_m1807623609(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_0017:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t1363597357_il2cpp_TypeInfo_var);
		Console_WriteLine_m2829201975(NULL /*static, unused*/, G_B3_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String WebSocketSharp.WebSocketFrame::PrintToString(System.Boolean)
extern Il2CppClass* WebSocketFrame_t778194306_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketFrame_PrintToString_m2057514283_MetadataUsageId;
extern "C"  String_t* WebSocketFrame_PrintToString_m2057514283 (WebSocketFrame_t778194306 * __this, bool ___dumped0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_PrintToString_m2057514283_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	{
		bool L_0 = ___dumped0;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t778194306_il2cpp_TypeInfo_var);
		String_t* L_1 = WebSocketFrame_dump_m3839803398(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_0017;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t778194306_il2cpp_TypeInfo_var);
		String_t* L_2 = WebSocketFrame_print_m1807623609(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
// System.Byte[] WebSocketSharp.WebSocketFrame::ToByteArray()
extern Il2CppClass* MemoryStream_t418716369_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketFrame_ToByteArray_m3468314104_MetadataUsageId;
extern "C"  ByteU5BU5D_t4260760469* WebSocketFrame_ToByteArray_m3468314104 (WebSocketFrame_t778194306 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_ToByteArray_m3468314104_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MemoryStream_t418716369 * V_0 = NULL;
	int32_t V_1 = 0;
	ByteU5BU5D_t4260760469* V_2 = NULL;
	ByteU5BU5D_t4260760469* V_3 = NULL;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		MemoryStream_t418716369 * L_0 = (MemoryStream_t418716369 *)il2cpp_codegen_object_new(MemoryStream_t418716369_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m3603177736(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
	}

IL_0006:
	try
	{ // begin try (depth: 1)
		{
			uint8_t L_1 = __this->get__fin_1();
			V_1 = L_1;
			int32_t L_2 = V_1;
			uint8_t L_3 = __this->get__rsv1_7();
			V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)L_2<<(int32_t)1))+(int32_t)L_3));
			int32_t L_4 = V_1;
			uint8_t L_5 = __this->get__rsv2_8();
			V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)L_4<<(int32_t)1))+(int32_t)L_5));
			int32_t L_6 = V_1;
			uint8_t L_7 = __this->get__rsv3_9();
			V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6<<(int32_t)1))+(int32_t)L_7));
			int32_t L_8 = V_1;
			uint8_t L_9 = __this->get__opcode_4();
			V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)L_8<<(int32_t)4))+(int32_t)L_9));
			int32_t L_10 = V_1;
			uint8_t L_11 = __this->get__mask_2();
			V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)L_10<<(int32_t)1))+(int32_t)L_11));
			int32_t L_12 = V_1;
			uint8_t L_13 = __this->get__payloadLength_6();
			V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)L_12<<(int32_t)7))+(int32_t)L_13));
			MemoryStream_t418716369 * L_14 = V_0;
			int32_t L_15 = V_1;
			ByteU5BU5D_t4260760469* L_16 = Ext_ToByteArrayInternally_m3284354672(NULL /*static, unused*/, (((int32_t)((uint16_t)L_15))), 1, /*hidden argument*/NULL);
			NullCheck(L_14);
			VirtActionInvoker3< ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_14, L_16, 0, 2);
			uint8_t L_17 = __this->get__payloadLength_6();
			if ((((int32_t)L_17) <= ((int32_t)((int32_t)125))))
			{
				goto IL_0081;
			}
		}

IL_006c:
		{
			MemoryStream_t418716369 * L_18 = V_0;
			ByteU5BU5D_t4260760469* L_19 = __this->get__extPayloadLength_0();
			ByteU5BU5D_t4260760469* L_20 = __this->get__extPayloadLength_0();
			NullCheck(L_20);
			NullCheck(L_18);
			VirtActionInvoker3< ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_18, L_19, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))));
		}

IL_0081:
		{
			uint8_t L_21 = __this->get__mask_2();
			if ((!(((uint32_t)L_21) == ((uint32_t)1))))
			{
				goto IL_00a2;
			}
		}

IL_008d:
		{
			MemoryStream_t418716369 * L_22 = V_0;
			ByteU5BU5D_t4260760469* L_23 = __this->get__maskingKey_3();
			ByteU5BU5D_t4260760469* L_24 = __this->get__maskingKey_3();
			NullCheck(L_24);
			NullCheck(L_22);
			VirtActionInvoker3< ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_22, L_23, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_24)->max_length)))));
		}

IL_00a2:
		{
			uint8_t L_25 = __this->get__payloadLength_6();
			if ((((int32_t)L_25) <= ((int32_t)0)))
			{
				goto IL_00de;
			}
		}

IL_00ae:
		{
			PayloadData_t39926750 * L_26 = __this->get__payloadData_5();
			NullCheck(L_26);
			ByteU5BU5D_t4260760469* L_27 = PayloadData_ToByteArray_m147904440(L_26, /*hidden argument*/NULL);
			V_2 = L_27;
			uint8_t L_28 = __this->get__payloadLength_6();
			if ((((int32_t)L_28) >= ((int32_t)((int32_t)127))))
			{
				goto IL_00d7;
			}
		}

IL_00c7:
		{
			MemoryStream_t418716369 * L_29 = V_0;
			ByteU5BU5D_t4260760469* L_30 = V_2;
			ByteU5BU5D_t4260760469* L_31 = V_2;
			NullCheck(L_31);
			NullCheck(L_29);
			VirtActionInvoker3< ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_29, L_30, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_31)->max_length)))));
			goto IL_00de;
		}

IL_00d7:
		{
			MemoryStream_t418716369 * L_32 = V_0;
			ByteU5BU5D_t4260760469* L_33 = V_2;
			Ext_WriteBytes_m1982528933(NULL /*static, unused*/, L_32, L_33, /*hidden argument*/NULL);
		}

IL_00de:
		{
			MemoryStream_t418716369 * L_34 = V_0;
			NullCheck(L_34);
			VirtActionInvoker0::Invoke(12 /* System.Void System.IO.Stream::Close() */, L_34);
			MemoryStream_t418716369 * L_35 = V_0;
			NullCheck(L_35);
			ByteU5BU5D_t4260760469* L_36 = VirtFuncInvoker0< ByteU5BU5D_t4260760469* >::Invoke(26 /* System.Byte[] System.IO.MemoryStream::ToArray() */, L_35);
			V_3 = L_36;
			IL2CPP_LEAVE(0x102, FINALLY_00f5);
		}

IL_00f0:
		{
			; // IL_00f0: leave IL_0102
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_00f5;
	}

FINALLY_00f5:
	{ // begin finally (depth: 1)
		{
			MemoryStream_t418716369 * L_37 = V_0;
			if (!L_37)
			{
				goto IL_0101;
			}
		}

IL_00fb:
		{
			MemoryStream_t418716369 * L_38 = V_0;
			NullCheck(L_38);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_38);
		}

IL_0101:
		{
			IL2CPP_END_FINALLY(245)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(245)
	{
		IL2CPP_JUMP_TBL(0x102, IL_0102)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0102:
	{
		ByteU5BU5D_t4260760469* L_39 = V_3;
		return L_39;
	}
}
// System.String WebSocketSharp.WebSocketFrame::ToString()
extern Il2CppClass* BitConverter_t1260277767_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketFrame_ToString_m2245624947_MetadataUsageId;
extern "C"  String_t* WebSocketFrame_ToString_m2245624947 (WebSocketFrame_t778194306 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocketFrame_ToString_m2245624947_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ByteU5BU5D_t4260760469* L_0 = WebSocketFrame_ToByteArray_m3468314104(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t1260277767_il2cpp_TypeInfo_var);
		String_t* L_1 = BitConverter_ToString_m1865594174(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey2F::.ctor()
extern "C"  void U3CdumpU3Ec__AnonStorey2F__ctor_m1441017532 (U3CdumpU3Ec__AnonStorey2F_t681710991 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Action`4<System.String,System.String,System.String,System.String> WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey2F::<>m__19()
extern Il2CppClass* U3CdumpU3Ec__AnonStorey30_t1917037014_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_4_t828544145_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CdumpU3Ec__AnonStorey30_U3CU3Em__1B_m412092807_MethodInfo_var;
extern const MethodInfo* Action_4__ctor_m2666262812_MethodInfo_var;
extern const uint32_t U3CdumpU3Ec__AnonStorey2F_U3CU3Em__19_m1858461973_MetadataUsageId;
extern "C"  Action_4_t828544145 * U3CdumpU3Ec__AnonStorey2F_U3CU3Em__19_m1858461973 (U3CdumpU3Ec__AnonStorey2F_t681710991 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CdumpU3Ec__AnonStorey2F_U3CU3Em__19_m1858461973_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CdumpU3Ec__AnonStorey30_t1917037014 * V_0 = NULL;
	{
		U3CdumpU3Ec__AnonStorey30_t1917037014 * L_0 = (U3CdumpU3Ec__AnonStorey30_t1917037014 *)il2cpp_codegen_object_new(U3CdumpU3Ec__AnonStorey30_t1917037014_il2cpp_TypeInfo_var);
		U3CdumpU3Ec__AnonStorey30__ctor_m1114561621(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CdumpU3Ec__AnonStorey30_t1917037014 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__refU2447_1(__this);
		U3CdumpU3Ec__AnonStorey30_t1917037014 * L_2 = V_0;
		NullCheck(L_2);
		L_2->set_lineCnt_0((((int64_t)((int64_t)0))));
		U3CdumpU3Ec__AnonStorey30_t1917037014 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CdumpU3Ec__AnonStorey30_U3CU3Em__1B_m412092807_MethodInfo_var);
		Action_4_t828544145 * L_5 = (Action_4_t828544145 *)il2cpp_codegen_object_new(Action_4_t828544145_il2cpp_TypeInfo_var);
		Action_4__ctor_m2666262812(L_5, L_3, L_4, /*hidden argument*/Action_4__ctor_m2666262812_MethodInfo_var);
		return L_5;
	}
}
// System.Void WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey2F/<dump>c__AnonStorey30::.ctor()
extern "C"  void U3CdumpU3Ec__AnonStorey30__ctor_m1114561621 (U3CdumpU3Ec__AnonStorey30_t1917037014 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey2F/<dump>c__AnonStorey30::<>m__1B(System.String,System.String,System.String,System.String)
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t1153838595_il2cpp_TypeInfo_var;
extern const uint32_t U3CdumpU3Ec__AnonStorey30_U3CU3Em__1B_m412092807_MetadataUsageId;
extern "C"  void U3CdumpU3Ec__AnonStorey30_U3CU3Em__1B_m412092807 (U3CdumpU3Ec__AnonStorey30_t1917037014 * __this, String_t* ___arg10, String_t* ___arg21, String_t* ___arg32, String_t* ___arg43, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CdumpU3Ec__AnonStorey30_U3CU3Em__1B_m412092807_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int64_t V_0 = 0;
	{
		U3CdumpU3Ec__AnonStorey2F_t681710991 * L_0 = __this->get_U3CU3Ef__refU2447_1();
		NullCheck(L_0);
		StringBuilder_t243639308 * L_1 = L_0->get_output_0();
		U3CdumpU3Ec__AnonStorey2F_t681710991 * L_2 = __this->get_U3CU3Ef__refU2447_1();
		NullCheck(L_2);
		String_t* L_3 = L_2->get_lineFmt_1();
		ObjectU5BU5D_t1108656482* L_4 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)5));
		int64_t L_5 = __this->get_lineCnt_0();
		int64_t L_6 = ((int64_t)((int64_t)L_5+(int64_t)(((int64_t)((int64_t)1)))));
		V_0 = L_6;
		__this->set_lineCnt_0(L_6);
		int64_t L_7 = V_0;
		int64_t L_8 = L_7;
		Il2CppObject * L_9 = Box(Int64_t1153838595_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_9);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_9);
		ObjectU5BU5D_t1108656482* L_10 = L_4;
		String_t* L_11 = ___arg10;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 1);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t1108656482* L_12 = L_10;
		String_t* L_13 = ___arg21;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 2);
		ArrayElementTypeCheck (L_12, L_13);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_13);
		ObjectU5BU5D_t1108656482* L_14 = L_12;
		String_t* L_15 = ___arg32;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 3);
		ArrayElementTypeCheck (L_14, L_15);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		ObjectU5BU5D_t1108656482* L_16 = L_14;
		String_t* L_17 = ___arg43;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 4);
		ArrayElementTypeCheck (L_16, L_17);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_17);
		NullCheck(L_1);
		StringBuilder_AppendFormat_m279545936(L_1, L_3, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator20::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator20__ctor_m2089199822 (U3CGetEnumeratorU3Ec__Iterator20_t4155755117 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Byte WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator20::System.Collections.Generic.IEnumerator<byte>.get_Current()
extern "C"  uint8_t U3CGetEnumeratorU3Ec__Iterator20_System_Collections_Generic_IEnumeratorU3CbyteU3E_get_Current_m674939580 (U3CGetEnumeratorU3Ec__Iterator20_t4155755117 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Object WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator20::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* Byte_t2862609660_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator20_System_Collections_IEnumerator_get_Current_m1232272674_MetadataUsageId;
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator20_System_Collections_IEnumerator_get_Current_m1232272674 (U3CGetEnumeratorU3Ec__Iterator20_t4155755117 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator20_System_Collections_IEnumerator_get_Current_m1232272674_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		uint8_t L_0 = __this->get_U24current_4();
		uint8_t L_1 = L_0;
		Il2CppObject * L_2 = Box(Byte_t2862609660_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Boolean WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator20::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator20_MoveNext_m3719833934 (U3CGetEnumeratorU3Ec__Iterator20_t4155755117 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0069;
		}
	}
	{
		goto IL_0091;
	}

IL_0021:
	{
		WebSocketFrame_t778194306 * L_2 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_2);
		ByteU5BU5D_t4260760469* L_3 = WebSocketFrame_ToByteArray_m3468314104(L_2, /*hidden argument*/NULL);
		__this->set_U3CU24s_168U3E__0_0(L_3);
		__this->set_U3CU24s_169U3E__1_1(0);
		goto IL_0077;
	}

IL_003e:
	{
		ByteU5BU5D_t4260760469* L_4 = __this->get_U3CU24s_168U3E__0_0();
		int32_t L_5 = __this->get_U3CU24s_169U3E__1_1();
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		uint8_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		__this->set_U3CbU3E__2_2(L_7);
		uint8_t L_8 = __this->get_U3CbU3E__2_2();
		__this->set_U24current_4(L_8);
		__this->set_U24PC_3(1);
		goto IL_0093;
	}

IL_0069:
	{
		int32_t L_9 = __this->get_U3CU24s_169U3E__1_1();
		__this->set_U3CU24s_169U3E__1_1(((int32_t)((int32_t)L_9+(int32_t)1)));
	}

IL_0077:
	{
		int32_t L_10 = __this->get_U3CU24s_169U3E__1_1();
		ByteU5BU5D_t4260760469* L_11 = __this->get_U3CU24s_168U3E__0_0();
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_003e;
		}
	}
	{
		__this->set_U24PC_3((-1));
	}

IL_0091:
	{
		return (bool)0;
	}

IL_0093:
	{
		return (bool)1;
	}
	// Dead block : IL_0095: ldloc.1
}
// System.Void WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator20::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator20_Dispose_m1869584779 (U3CGetEnumeratorU3Ec__Iterator20_t4155755117 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator20::Reset()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator20_Reset_m4030600059_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator20_Reset_m4030600059 (U3CGetEnumeratorU3Ec__Iterator20_t4155755117 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator20_Reset_m4030600059_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void WebSocketSharp.WebSocketFrame/<ParseAsync>c__AnonStorey31::.ctor()
extern "C"  void U3CParseAsyncU3Ec__AnonStorey31__ctor_m332244573 (U3CParseAsyncU3Ec__AnonStorey31_t1758512846 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketFrame/<ParseAsync>c__AnonStorey31::<>m__1A(System.Byte[])
extern Il2CppClass* WebSocketException_t2311987812_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocketFrame_t778194306_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m66117013_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3470785642;
extern const uint32_t U3CParseAsyncU3Ec__AnonStorey31_U3CU3Em__1A_m283814771_MetadataUsageId;
extern "C"  void U3CParseAsyncU3Ec__AnonStorey31_U3CU3Em__1A_m283814771 (U3CParseAsyncU3Ec__AnonStorey31_t1758512846 * __this, ByteU5BU5D_t4260760469* ___header0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CParseAsyncU3Ec__AnonStorey31_U3CU3Em__1A_m283814771_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	WebSocketFrame_t778194306 * V_0 = NULL;
	{
		ByteU5BU5D_t4260760469* L_0 = ___header0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)2)))
		{
			goto IL_0014;
		}
	}
	{
		WebSocketException_t2311987812 * L_1 = (WebSocketException_t2311987812 *)il2cpp_codegen_object_new(WebSocketException_t2311987812_il2cpp_TypeInfo_var);
		WebSocketException__ctor_m3830227690(L_1, _stringLiteral3470785642, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0014:
	{
		ByteU5BU5D_t4260760469* L_2 = ___header0;
		Stream_t1561764144 * L_3 = __this->get_stream_0();
		bool L_4 = __this->get_unmask_1();
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t778194306_il2cpp_TypeInfo_var);
		WebSocketFrame_t778194306 * L_5 = WebSocketFrame_parse_m2392405532(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		Action_1_t1174010442 * L_6 = __this->get_completed_2();
		if (!L_6)
		{
			goto IL_003e;
		}
	}
	{
		Action_1_t1174010442 * L_7 = __this->get_completed_2();
		WebSocketFrame_t778194306 * L_8 = V_0;
		NullCheck(L_7);
		Action_1_Invoke_m66117013(L_7, L_8, /*hidden argument*/Action_1_Invoke_m66117013_MethodInfo_var);
	}

IL_003e:
	{
		return;
	}
}
// System.Void WebSocketSharp.WebSocketStream::.ctor(System.IO.Stream,System.Boolean)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketStream__ctor_m20502183_MetadataUsageId;
extern "C"  void WebSocketStream__ctor_m20502183 (WebSocketStream_t4103435597 * __this, Stream_t1561764144 * ___innerStream0, bool ___secure1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocketStream__ctor_m20502183_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Stream_t1561764144 * L_0 = ___innerStream0;
		__this->set__innerStream_2(L_0);
		bool L_1 = ___secure1;
		__this->set__secure_3(L_1);
		Il2CppObject * L_2 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_2, /*hidden argument*/NULL);
		__this->set__forWrite_1(L_2);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketStream::.ctor(System.Net.Sockets.NetworkStream)
extern "C"  void WebSocketStream__ctor_m4292048655 (WebSocketStream_t4103435597 * __this, NetworkStream_t3953762560 * ___innerStream0, const MethodInfo* method)
{
	{
		NetworkStream_t3953762560 * L_0 = ___innerStream0;
		WebSocketStream__ctor_m20502183(__this, L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketStream::.ctor(WebSocketSharp.Net.Security.SslStream)
extern "C"  void WebSocketStream__ctor_m1977349721 (WebSocketStream_t4103435597 * __this, SslStream_t3623155758 * ___innerStream0, const MethodInfo* method)
{
	{
		SslStream_t3623155758 * L_0 = ___innerStream0;
		WebSocketStream__ctor_m20502183(__this, L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean WebSocketSharp.WebSocketStream::get_DataAvailable()
extern Il2CppClass* SslStream_t3623155758_il2cpp_TypeInfo_var;
extern Il2CppClass* NetworkStream_t3953762560_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketStream_get_DataAvailable_m2361437439_MetadataUsageId;
extern "C"  bool WebSocketStream_get_DataAvailable_m2361437439 (WebSocketStream_t4103435597 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocketStream_get_DataAvailable_m2361437439_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool G_B3_0 = false;
	{
		bool L_0 = __this->get__secure_3();
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		Stream_t1561764144 * L_1 = __this->get__innerStream_2();
		NullCheck(((SslStream_t3623155758 *)CastclassClass(L_1, SslStream_t3623155758_il2cpp_TypeInfo_var)));
		bool L_2 = SslStream_get_DataAvailable_m1180989081(((SslStream_t3623155758 *)CastclassClass(L_1, SslStream_t3623155758_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_0030;
	}

IL_0020:
	{
		Stream_t1561764144 * L_3 = __this->get__innerStream_2();
		NullCheck(((NetworkStream_t3953762560 *)CastclassClass(L_3, NetworkStream_t3953762560_il2cpp_TypeInfo_var)));
		bool L_4 = VirtFuncInvoker0< bool >::Invoke(24 /* System.Boolean System.Net.Sockets.NetworkStream::get_DataAvailable() */, ((NetworkStream_t3953762560 *)CastclassClass(L_3, NetworkStream_t3953762560_il2cpp_TypeInfo_var)));
		G_B3_0 = L_4;
	}

IL_0030:
	{
		return G_B3_0;
	}
}
// System.Boolean WebSocketSharp.WebSocketStream::get_IsSecure()
extern "C"  bool WebSocketStream_get_IsSecure_m3197194531 (WebSocketStream_t4103435597 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get__secure_3();
		return L_0;
	}
}
// System.Byte[] WebSocketSharp.WebSocketStream::readHandshakeEntityBody(System.IO.Stream,System.String)
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2452785789;
extern Il2CppCodeGenString* _stringLiteral3188603622;
extern Il2CppCodeGenString* _stringLiteral1102667790;
extern const uint32_t WebSocketStream_readHandshakeEntityBody_m1812677996_MetadataUsageId;
extern "C"  ByteU5BU5D_t4260760469* WebSocketStream_readHandshakeEntityBody_m1812677996 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___stream0, String_t* ___length1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocketStream_readHandshakeEntityBody_m1812677996_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int64_t V_0 = 0;
	ByteU5BU5D_t4260760469* G_B9_0 = NULL;
	{
		String_t* L_0 = ___length1;
		bool L_1 = Int64_TryParse_m2106581948(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001d;
		}
	}
	{
		ArgumentException_t928607144 * L_2 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m732321503(L_2, _stringLiteral2452785789, _stringLiteral3188603622, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001d:
	{
		int64_t L_3 = V_0;
		if ((((int64_t)L_3) >= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_0035;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_4 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1193970951(L_4, _stringLiteral3188603622, _stringLiteral1102667790, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0035:
	{
		int64_t L_5 = V_0;
		if ((((int64_t)L_5) <= ((int64_t)(((int64_t)((int64_t)((int32_t)1024)))))))
		{
			goto IL_0052;
		}
	}
	{
		Stream_t1561764144 * L_6 = ___stream0;
		int64_t L_7 = V_0;
		ByteU5BU5D_t4260760469* L_8 = Ext_ReadBytes_m2955720804(NULL /*static, unused*/, L_6, L_7, ((int32_t)1024), /*hidden argument*/NULL);
		G_B9_0 = L_8;
		goto IL_0068;
	}

IL_0052:
	{
		int64_t L_9 = V_0;
		if ((((int64_t)L_9) <= ((int64_t)(((int64_t)((int64_t)0))))))
		{
			goto IL_0067;
		}
	}
	{
		Stream_t1561764144 * L_10 = ___stream0;
		int64_t L_11 = V_0;
		ByteU5BU5D_t4260760469* L_12 = Ext_ReadBytes_m3416233938(NULL /*static, unused*/, L_10, (((int32_t)((int32_t)L_11))), /*hidden argument*/NULL);
		G_B9_0 = L_12;
		goto IL_0068;
	}

IL_0067:
	{
		G_B9_0 = ((ByteU5BU5D_t4260760469*)(NULL));
	}

IL_0068:
	{
		return G_B9_0;
	}
}
// System.String[] WebSocketSharp.WebSocketStream::readHandshakeHeaders(System.IO.Stream)
extern Il2CppClass* U3CreadHandshakeHeadersU3Ec__AnonStorey32_t3526000246_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t4230795212_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t1549654636_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocketException_t2311987812_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t2012439129_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m67678022_MethodInfo_var;
extern const MethodInfo* U3CreadHandshakeHeadersU3Ec__AnonStorey32_U3CU3Em__1C_m2472820081_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m375071632_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m3980858669_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral552529929;
extern Il2CppCodeGenString* _stringLiteral413;
extern Il2CppCodeGenString* _stringLiteral32;
extern Il2CppCodeGenString* _stringLiteral9;
extern const uint32_t WebSocketStream_readHandshakeHeaders_m3212613090_MetadataUsageId;
extern "C"  StringU5BU5D_t4054002952* WebSocketStream_readHandshakeHeaders_m3212613090 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___stream0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocketStream_readHandshakeHeaders_m3212613090_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t1549654636 * V_0 = NULL;
	bool V_1 = false;
	String_t* V_2 = NULL;
	U3CreadHandshakeHeadersU3Ec__AnonStorey32_t3526000246 * V_3 = NULL;
	{
		U3CreadHandshakeHeadersU3Ec__AnonStorey32_t3526000246 * L_0 = (U3CreadHandshakeHeadersU3Ec__AnonStorey32_t3526000246 *)il2cpp_codegen_object_new(U3CreadHandshakeHeadersU3Ec__AnonStorey32_t3526000246_il2cpp_TypeInfo_var);
		U3CreadHandshakeHeadersU3Ec__AnonStorey32__ctor_m1526739173(L_0, /*hidden argument*/NULL);
		V_3 = L_0;
		U3CreadHandshakeHeadersU3Ec__AnonStorey32_t3526000246 * L_1 = V_3;
		List_1_t4230795212 * L_2 = (List_1_t4230795212 *)il2cpp_codegen_object_new(List_1_t4230795212_il2cpp_TypeInfo_var);
		List_1__ctor_m67678022(L_2, /*hidden argument*/List_1__ctor_m67678022_MethodInfo_var);
		NullCheck(L_1);
		L_1->set_buff_0(L_2);
		U3CreadHandshakeHeadersU3Ec__AnonStorey32_t3526000246 * L_3 = V_3;
		NullCheck(L_3);
		L_3->set_count_1(0);
		U3CreadHandshakeHeadersU3Ec__AnonStorey32_t3526000246 * L_4 = V_3;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)U3CreadHandshakeHeadersU3Ec__AnonStorey32_U3CU3Em__1C_m2472820081_MethodInfo_var);
		Action_1_t1549654636 * L_6 = (Action_1_t1549654636 *)il2cpp_codegen_object_new(Action_1_t1549654636_il2cpp_TypeInfo_var);
		Action_1__ctor_m375071632(L_6, L_4, L_5, /*hidden argument*/Action_1__ctor_m375071632_MethodInfo_var);
		V_0 = L_6;
		V_1 = (bool)0;
		goto IL_007f;
	}

IL_002c:
	{
		Stream_t1561764144 * L_7 = ___stream0;
		NullCheck(L_7);
		int32_t L_8 = VirtFuncInvoker0< int32_t >::Invoke(15 /* System.Int32 System.IO.Stream::ReadByte() */, L_7);
		Action_1_t1549654636 * L_9 = V_0;
		bool L_10 = Ext_EqualsWith_m83190675(NULL /*static, unused*/, L_8, ((int32_t)13), L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_007f;
		}
	}
	{
		Stream_t1561764144 * L_11 = ___stream0;
		NullCheck(L_11);
		int32_t L_12 = VirtFuncInvoker0< int32_t >::Invoke(15 /* System.Int32 System.IO.Stream::ReadByte() */, L_11);
		Action_1_t1549654636 * L_13 = V_0;
		bool L_14 = Ext_EqualsWith_m83190675(NULL /*static, unused*/, L_12, ((int32_t)10), L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_007f;
		}
	}
	{
		Stream_t1561764144 * L_15 = ___stream0;
		NullCheck(L_15);
		int32_t L_16 = VirtFuncInvoker0< int32_t >::Invoke(15 /* System.Int32 System.IO.Stream::ReadByte() */, L_15);
		Action_1_t1549654636 * L_17 = V_0;
		bool L_18 = Ext_EqualsWith_m83190675(NULL /*static, unused*/, L_16, ((int32_t)13), L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_007f;
		}
	}
	{
		Stream_t1561764144 * L_19 = ___stream0;
		NullCheck(L_19);
		int32_t L_20 = VirtFuncInvoker0< int32_t >::Invoke(15 /* System.Int32 System.IO.Stream::ReadByte() */, L_19);
		Action_1_t1549654636 * L_21 = V_0;
		bool L_22 = Ext_EqualsWith_m83190675(NULL /*static, unused*/, L_20, ((int32_t)10), L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_007f;
		}
	}
	{
		V_1 = (bool)1;
		goto IL_008f;
	}

IL_007f:
	{
		U3CreadHandshakeHeadersU3Ec__AnonStorey32_t3526000246 * L_23 = V_3;
		NullCheck(L_23);
		int32_t L_24 = L_23->get_count_1();
		if ((((int32_t)L_24) < ((int32_t)((int32_t)8192))))
		{
			goto IL_002c;
		}
	}

IL_008f:
	{
		bool L_25 = V_1;
		if (L_25)
		{
			goto IL_00a0;
		}
	}
	{
		WebSocketException_t2311987812 * L_26 = (WebSocketException_t2311987812 *)il2cpp_codegen_object_new(WebSocketException_t2311987812_il2cpp_TypeInfo_var);
		WebSocketException__ctor_m3830227690(L_26, _stringLiteral552529929, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_26);
	}

IL_00a0:
	{
		V_2 = _stringLiteral413;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t2012439129_il2cpp_TypeInfo_var);
		Encoding_t2012439129 * L_27 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		U3CreadHandshakeHeadersU3Ec__AnonStorey32_t3526000246 * L_28 = V_3;
		NullCheck(L_28);
		List_1_t4230795212 * L_29 = L_28->get_buff_0();
		NullCheck(L_29);
		ByteU5BU5D_t4260760469* L_30 = List_1_ToArray_m3980858669(L_29, /*hidden argument*/List_1_ToArray_m3980858669_MethodInfo_var);
		NullCheck(L_27);
		String_t* L_31 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t4260760469* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_27, L_30);
		String_t* L_32 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_33 = String_Concat_m138640077(NULL /*static, unused*/, L_32, _stringLiteral32, /*hidden argument*/NULL);
		NullCheck(L_31);
		String_t* L_34 = String_Replace_m2915759397(L_31, L_33, _stringLiteral32, /*hidden argument*/NULL);
		String_t* L_35 = V_2;
		String_t* L_36 = String_Concat_m138640077(NULL /*static, unused*/, L_35, _stringLiteral9, /*hidden argument*/NULL);
		NullCheck(L_34);
		String_t* L_37 = String_Replace_m2915759397(L_34, L_36, _stringLiteral32, /*hidden argument*/NULL);
		StringU5BU5D_t4054002952* L_38 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)1));
		String_t* L_39 = V_2;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		ArrayElementTypeCheck (L_38, L_39);
		(L_38)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_39);
		NullCheck(L_37);
		StringU5BU5D_t4054002952* L_40 = String_Split_m459616251(L_37, L_38, 1, /*hidden argument*/NULL);
		return L_40;
	}
}
// System.Boolean WebSocketSharp.WebSocketStream::Write(System.Byte[])
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketStream_Write_m2408900417_MetadataUsageId;
extern "C"  bool WebSocketStream_Write_m2408900417 (WebSocketStream_t4103435597 * __this, ByteU5BU5D_t4260760469* ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocketStream_Write_m2408900417_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = __this->get__forWrite_1();
		V_0 = L_0;
		Il2CppObject * L_1 = V_0;
		Monitor_Enter_m476686225(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			{
				Stream_t1561764144 * L_2 = __this->get__innerStream_2();
				ByteU5BU5D_t4260760469* L_3 = ___data0;
				ByteU5BU5D_t4260760469* L_4 = ___data0;
				NullCheck(L_4);
				NullCheck(L_2);
				VirtActionInvoker3< ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_2, L_3, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length)))));
				V_1 = (bool)1;
				IL2CPP_LEAVE(0x42, FINALLY_003b);
			}

IL_0024:
			{
				; // IL_0024: leave IL_0036
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t3991598821 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0029;
			throw e;
		}

CATCH_0029:
		{ // begin catch(System.Object)
			{
				V_1 = (bool)0;
				IL2CPP_LEAVE(0x42, FINALLY_003b);
			}

IL_0031:
			{
				; // IL_0031: leave IL_0036
			}
		} // end catch (depth: 2)

IL_0036:
		{
			IL2CPP_LEAVE(0x42, FINALLY_003b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_003b;
	}

FINALLY_003b:
	{ // begin finally (depth: 1)
		Il2CppObject * L_5 = V_0;
		Monitor_Exit_m2088237919(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(59)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(59)
	{
		IL2CPP_JUMP_TBL(0x42, IL_0042)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_0042:
	{
		bool L_6 = V_1;
		return L_6;
	}
}
// System.Void WebSocketSharp.WebSocketStream::Close()
extern "C"  void WebSocketStream_Close_m3759737493 (WebSocketStream_t4103435597 * __this, const MethodInfo* method)
{
	{
		Stream_t1561764144 * L_0 = __this->get__innerStream_2();
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(12 /* System.Void System.IO.Stream::Close() */, L_0);
		return;
	}
}
// WebSocketSharp.WebSocketStream WebSocketSharp.WebSocketStream::CreateClientStream(System.Net.Sockets.TcpClient,System.Boolean,System.String,System.Net.Security.RemoteCertificateValidationCallback)
extern Il2CppClass* WebSocketStream_t4103435597_il2cpp_TypeInfo_var;
extern Il2CppClass* RemoteCertificateValidationCallback_t1894914657_il2cpp_TypeInfo_var;
extern Il2CppClass* SslStream_t3623155758_il2cpp_TypeInfo_var;
extern const MethodInfo* WebSocketStream_U3CCreateClientStreamU3Em__1E_m1962976644_MethodInfo_var;
extern const uint32_t WebSocketStream_CreateClientStream_m3072197941_MetadataUsageId;
extern "C"  WebSocketStream_t4103435597 * WebSocketStream_CreateClientStream_m3072197941 (Il2CppObject * __this /* static, unused */, TcpClient_t838416830 * ___client0, bool ___secure1, String_t* ___host2, RemoteCertificateValidationCallback_t1894914657 * ___validationCallback3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocketStream_CreateClientStream_m3072197941_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	NetworkStream_t3953762560 * V_0 = NULL;
	SslStream_t3623155758 * V_1 = NULL;
	{
		TcpClient_t838416830 * L_0 = ___client0;
		NullCheck(L_0);
		NetworkStream_t3953762560 * L_1 = TcpClient_GetStream_m1239965747(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = ___secure1;
		if (!L_2)
		{
			goto IL_0049;
		}
	}
	{
		RemoteCertificateValidationCallback_t1894914657 * L_3 = ___validationCallback3;
		if (L_3)
		{
			goto IL_0032;
		}
	}
	{
		RemoteCertificateValidationCallback_t1894914657 * L_4 = ((WebSocketStream_t4103435597_StaticFields*)WebSocketStream_t4103435597_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache3_4();
		if (L_4)
		{
			goto IL_002b;
		}
	}
	{
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)WebSocketStream_U3CCreateClientStreamU3Em__1E_m1962976644_MethodInfo_var);
		RemoteCertificateValidationCallback_t1894914657 * L_6 = (RemoteCertificateValidationCallback_t1894914657 *)il2cpp_codegen_object_new(RemoteCertificateValidationCallback_t1894914657_il2cpp_TypeInfo_var);
		RemoteCertificateValidationCallback__ctor_m1684204841(L_6, NULL, L_5, /*hidden argument*/NULL);
		((WebSocketStream_t4103435597_StaticFields*)WebSocketStream_t4103435597_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache3_4(L_6);
	}

IL_002b:
	{
		RemoteCertificateValidationCallback_t1894914657 * L_7 = ((WebSocketStream_t4103435597_StaticFields*)WebSocketStream_t4103435597_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache3_4();
		___validationCallback3 = L_7;
	}

IL_0032:
	{
		NetworkStream_t3953762560 * L_8 = V_0;
		RemoteCertificateValidationCallback_t1894914657 * L_9 = ___validationCallback3;
		SslStream_t3623155758 * L_10 = (SslStream_t3623155758 *)il2cpp_codegen_object_new(SslStream_t3623155758_il2cpp_TypeInfo_var);
		SslStream__ctor_m4181297623(L_10, L_8, (bool)0, L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		SslStream_t3623155758 * L_11 = V_1;
		String_t* L_12 = ___host2;
		NullCheck(L_11);
		VirtActionInvoker1< String_t* >::Invoke(27 /* System.Void System.Net.Security.SslStream::AuthenticateAsClient(System.String) */, L_11, L_12);
		SslStream_t3623155758 * L_13 = V_1;
		WebSocketStream_t4103435597 * L_14 = (WebSocketStream_t4103435597 *)il2cpp_codegen_object_new(WebSocketStream_t4103435597_il2cpp_TypeInfo_var);
		WebSocketStream__ctor_m1977349721(L_14, L_13, /*hidden argument*/NULL);
		return L_14;
	}

IL_0049:
	{
		NetworkStream_t3953762560 * L_15 = V_0;
		WebSocketStream_t4103435597 * L_16 = (WebSocketStream_t4103435597 *)il2cpp_codegen_object_new(WebSocketStream_t4103435597_il2cpp_TypeInfo_var);
		WebSocketStream__ctor_m4292048655(L_16, L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
// WebSocketSharp.WebSocketStream WebSocketSharp.WebSocketStream::CreateServerStream(System.Net.Sockets.TcpClient,System.Boolean,System.Security.Cryptography.X509Certificates.X509Certificate)
extern Il2CppClass* SslStream_t3623155758_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocketStream_t4103435597_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketStream_CreateServerStream_m1643920762_MetadataUsageId;
extern "C"  WebSocketStream_t4103435597 * WebSocketStream_CreateServerStream_m1643920762 (Il2CppObject * __this /* static, unused */, TcpClient_t838416830 * ___client0, bool ___secure1, X509Certificate_t3076817455 * ___cert2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocketStream_CreateServerStream_m1643920762_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	NetworkStream_t3953762560 * V_0 = NULL;
	SslStream_t3623155758 * V_1 = NULL;
	{
		TcpClient_t838416830 * L_0 = ___client0;
		NullCheck(L_0);
		NetworkStream_t3953762560 * L_1 = TcpClient_GetStream_m1239965747(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = ___secure1;
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		NetworkStream_t3953762560 * L_3 = V_0;
		SslStream_t3623155758 * L_4 = (SslStream_t3623155758 *)il2cpp_codegen_object_new(SslStream_t3623155758_il2cpp_TypeInfo_var);
		SslStream__ctor_m3865439900(L_4, L_3, (bool)0, /*hidden argument*/NULL);
		V_1 = L_4;
		SslStream_t3623155758 * L_5 = V_1;
		X509Certificate_t3076817455 * L_6 = ___cert2;
		NullCheck(L_5);
		VirtActionInvoker1< X509Certificate_t3076817455 * >::Invoke(29 /* System.Void System.Net.Security.SslStream::AuthenticateAsServer(System.Security.Cryptography.X509Certificates.X509Certificate) */, L_5, L_6);
		SslStream_t3623155758 * L_7 = V_1;
		WebSocketStream_t4103435597 * L_8 = (WebSocketStream_t4103435597 *)il2cpp_codegen_object_new(WebSocketStream_t4103435597_il2cpp_TypeInfo_var);
		WebSocketStream__ctor_m1977349721(L_8, L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0023:
	{
		NetworkStream_t3953762560 * L_9 = V_0;
		WebSocketStream_t4103435597 * L_10 = (WebSocketStream_t4103435597 *)il2cpp_codegen_object_new(WebSocketStream_t4103435597_il2cpp_TypeInfo_var);
		WebSocketStream__ctor_m4292048655(L_10, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Void WebSocketSharp.WebSocketStream::Dispose()
extern "C"  void WebSocketStream_Dispose_m1774972412 (WebSocketStream_t4103435597 * __this, const MethodInfo* method)
{
	{
		Stream_t1561764144 * L_0 = __this->get__innerStream_2();
		NullCheck(L_0);
		Stream_Dispose_m2904306374(L_0, /*hidden argument*/NULL);
		return;
	}
}
// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketStream::ReadFrame()
extern Il2CppClass* WebSocketFrame_t778194306_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketStream_ReadFrame_m2749362418_MetadataUsageId;
extern "C"  WebSocketFrame_t778194306 * WebSocketStream_ReadFrame_m2749362418 (WebSocketStream_t4103435597 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocketStream_ReadFrame_m2749362418_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Stream_t1561764144 * L_0 = __this->get__innerStream_2();
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t778194306_il2cpp_TypeInfo_var);
		WebSocketFrame_t778194306 * L_1 = WebSocketFrame_Parse_m2256012537(NULL /*static, unused*/, L_0, (bool)1, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void WebSocketSharp.WebSocketStream::ReadFrameAsync(System.Action`1<WebSocketSharp.WebSocketFrame>,System.Action`1<System.Exception>)
extern Il2CppClass* WebSocketFrame_t778194306_il2cpp_TypeInfo_var;
extern const uint32_t WebSocketStream_ReadFrameAsync_m3409431931_MetadataUsageId;
extern "C"  void WebSocketStream_ReadFrameAsync_m3409431931 (WebSocketStream_t4103435597 * __this, Action_1_t1174010442 * ___completed0, Action_1_t92447661 * ___error1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocketStream_ReadFrameAsync_m3409431931_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Stream_t1561764144 * L_0 = __this->get__innerStream_2();
		Action_1_t1174010442 * L_1 = ___completed0;
		Action_1_t92447661 * L_2 = ___error1;
		IL2CPP_RUNTIME_CLASS_INIT(WebSocketFrame_t778194306_il2cpp_TypeInfo_var);
		WebSocketFrame_ParseAsync_m1471908516(NULL /*static, unused*/, L_0, (bool)1, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// WebSocketSharp.HandshakeRequest WebSocketSharp.WebSocketStream::ReadHandshakeRequest()
extern Il2CppClass* Func_2_t3775863249_il2cpp_TypeInfo_var;
extern const MethodInfo* HandshakeRequest_Parse_m2674338295_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m310205795_MethodInfo_var;
extern const MethodInfo* WebSocketStream_ReadHandshake_TisHandshakeRequest_t1037477780_m1150541384_MethodInfo_var;
extern const uint32_t WebSocketStream_ReadHandshakeRequest_m4135427139_MetadataUsageId;
extern "C"  HandshakeRequest_t1037477780 * WebSocketStream_ReadHandshakeRequest_m4135427139 (WebSocketStream_t4103435597 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocketStream_ReadHandshakeRequest_m4135427139_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)HandshakeRequest_Parse_m2674338295_MethodInfo_var);
		Func_2_t3775863249 * L_1 = (Func_2_t3775863249 *)il2cpp_codegen_object_new(Func_2_t3775863249_il2cpp_TypeInfo_var);
		Func_2__ctor_m310205795(L_1, NULL, L_0, /*hidden argument*/Func_2__ctor_m310205795_MethodInfo_var);
		HandshakeRequest_t1037477780 * L_2 = WebSocketStream_ReadHandshake_TisHandshakeRequest_t1037477780_m1150541384(__this, L_1, ((int32_t)90000), /*hidden argument*/WebSocketStream_ReadHandshake_TisHandshakeRequest_t1037477780_m1150541384_MethodInfo_var);
		return L_2;
	}
}
// WebSocketSharp.HandshakeResponse WebSocketSharp.WebSocketStream::ReadHandshakeResponse()
extern Il2CppClass* Func_2_t1673115995_il2cpp_TypeInfo_var;
extern const MethodInfo* HandshakeResponse_Parse_m1951366865_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m2730669671_MethodInfo_var;
extern const MethodInfo* WebSocketStream_ReadHandshake_TisHandshakeResponse_t3229697822_m385670550_MethodInfo_var;
extern const uint32_t WebSocketStream_ReadHandshakeResponse_m569574287_MetadataUsageId;
extern "C"  HandshakeResponse_t3229697822 * WebSocketStream_ReadHandshakeResponse_m569574287 (WebSocketStream_t4103435597 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebSocketStream_ReadHandshakeResponse_m569574287_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)HandshakeResponse_Parse_m1951366865_MethodInfo_var);
		Func_2_t1673115995 * L_1 = (Func_2_t1673115995 *)il2cpp_codegen_object_new(Func_2_t1673115995_il2cpp_TypeInfo_var);
		Func_2__ctor_m2730669671(L_1, NULL, L_0, /*hidden argument*/Func_2__ctor_m2730669671_MethodInfo_var);
		HandshakeResponse_t3229697822 * L_2 = WebSocketStream_ReadHandshake_TisHandshakeResponse_t3229697822_m385670550(__this, L_1, ((int32_t)90000), /*hidden argument*/WebSocketStream_ReadHandshake_TisHandshakeResponse_t3229697822_m385670550_MethodInfo_var);
		return L_2;
	}
}
// System.Boolean WebSocketSharp.WebSocketStream::WriteFrame(WebSocketSharp.WebSocketFrame)
extern "C"  bool WebSocketStream_WriteFrame_m665530746 (WebSocketStream_t4103435597 * __this, WebSocketFrame_t778194306 * ___frame0, const MethodInfo* method)
{
	{
		WebSocketFrame_t778194306 * L_0 = ___frame0;
		NullCheck(L_0);
		ByteU5BU5D_t4260760469* L_1 = WebSocketFrame_ToByteArray_m3468314104(L_0, /*hidden argument*/NULL);
		bool L_2 = WebSocketStream_Write_m2408900417(__this, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean WebSocketSharp.WebSocketStream::WriteHandshake(WebSocketSharp.HandshakeBase)
extern "C"  bool WebSocketStream_WriteHandshake_m511451412 (WebSocketStream_t4103435597 * __this, HandshakeBase_t1248407470 * ___handshake0, const MethodInfo* method)
{
	{
		HandshakeBase_t1248407470 * L_0 = ___handshake0;
		NullCheck(L_0);
		ByteU5BU5D_t4260760469* L_1 = HandshakeBase_ToByteArray_m3444172392(L_0, /*hidden argument*/NULL);
		bool L_2 = WebSocketStream_Write_m2408900417(__this, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean WebSocketSharp.WebSocketStream::<CreateClientStream>m__1E(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern "C"  bool WebSocketStream_U3CCreateClientStreamU3Em__1E_m1962976644 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___sender0, X509Certificate_t3076817455 * ___certificate1, X509Chain_t1111884825 * ___chain2, int32_t ___sslPolicyErrors3, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void WebSocketSharp.WebSocketStream/<readHandshakeHeaders>c__AnonStorey32::.ctor()
extern "C"  void U3CreadHandshakeHeadersU3Ec__AnonStorey32__ctor_m1526739173 (U3CreadHandshakeHeadersU3Ec__AnonStorey32_t3526000246 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WebSocketSharp.WebSocketStream/<readHandshakeHeaders>c__AnonStorey32::<>m__1C(System.Int32)
extern const MethodInfo* List_1_Add_m4057006134_MethodInfo_var;
extern const uint32_t U3CreadHandshakeHeadersU3Ec__AnonStorey32_U3CU3Em__1C_m2472820081_MetadataUsageId;
extern "C"  void U3CreadHandshakeHeadersU3Ec__AnonStorey32_U3CU3Em__1C_m2472820081 (U3CreadHandshakeHeadersU3Ec__AnonStorey32_t3526000246 * __this, int32_t ___i0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CreadHandshakeHeadersU3Ec__AnonStorey32_U3CU3Em__1C_m2472820081_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t4230795212 * L_0 = __this->get_buff_0();
		int32_t L_1 = ___i0;
		NullCheck(L_0);
		List_1_Add_m4057006134(L_0, (((int32_t)((uint8_t)L_1))), /*hidden argument*/List_1_Add_m4057006134_MethodInfo_var);
		int32_t L_2 = __this->get_count_1();
		__this->set_count_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		return;
	}
}
// System.Void West::.ctor()
extern "C"  void West__ctor_m2837482924 (West_t2692559 * __this, const MethodInfo* method)
{
	{
		Enemy__ctor_m1781972739(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void West::Start()
extern "C"  void West_Start_m1784620716 (West_t2692559 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void West::Awake()
extern const MethodInfo* SingletonMonoBehaviour_1_get_Instance_m877975176_MethodInfo_var;
extern const uint32_t West_Awake_m3075088143_MetadataUsageId;
extern "C"  void West_Awake_m3075088143 (West_t2692559 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (West_Awake_m3075088143_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Vector3_t4282066566  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m2926210380(&L_0, (11.0f), (20.0f), (0.0f), /*hidden argument*/NULL);
		((Charctor_t1500651146 *)__this)->set_m_pos_2(L_0);
		GameObject_t3674682005 * L_1 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t1659122786 * L_2 = GameObject_get_transform_m1278640159(L_1, /*hidden argument*/NULL);
		Vector3_t4282066566  L_3 = ((Charctor_t1500651146 *)__this)->get_m_pos_2();
		NullCheck(L_2);
		Transform_set_position_m3111394108(L_2, L_3, /*hidden argument*/NULL);
		((Enemy_t67100520 *)__this)->set__ainum_11(0);
		((Charctor_t1500651146 *)__this)->set_ismove_3((bool)0);
		((Enemy_t67100520 *)__this)->set_isStart_13((bool)1);
		((Enemy_t67100520 *)__this)->set_shotPar_16((GameObject_t3674682005 *)NULL);
		((Enemy_t67100520 *)__this)->set_isEnd_17((bool)0);
		Factory_t572770538 * L_4 = SingletonMonoBehaviour_1_get_Instance_m877975176(NULL /*static, unused*/, /*hidden argument*/SingletonMonoBehaviour_1_get_Instance_m877975176_MethodInfo_var);
		((Enemy_t67100520 *)__this)->set_f_12(L_4);
		((Charctor_t1500651146 *)__this)->set_gameSpeed_5((1.0f));
		((Enemy_t67100520 *)__this)->set__bullettechnich_15((0.0f));
		Transform_t1659122786 * L_5 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t4282066566  L_6 = Transform_get_localPosition_m668140784(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		float L_7 = (&V_1)->get_y_2();
		V_0 = L_7;
		float L_8 = V_0;
		((Enemy_t67100520 *)__this)->set_hayasa_14(((float)((float)L_8/(float)(60.0f))));
		GameObject_t3674682005 * L_9 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		GameObject_set_layer_m1872241535(L_9, ((int32_t)13), /*hidden argument*/NULL);
		return;
	}
}
// System.Void West::Update()
extern "C"  void West_Update_m3789486817 (West_t2692559 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void West::FixedUpdate()
extern "C"  void West_FixedUpdate_m2155641383 (West_t2692559 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((Enemy_t67100520 *)__this)->get_isStart_13();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		VirtActionInvoker0::Invoke(22 /* System.Void Enemy::StartAction() */, __this);
	}

IL_0011:
	{
		bool L_1 = ((Charctor_t1500651146 *)__this)->get_ismove_3();
		if (!L_1)
		{
			goto IL_002d;
		}
	}
	{
		bool L_2 = ((Enemy_t67100520 *)__this)->get_isEnd_17();
		if (L_2)
		{
			goto IL_002d;
		}
	}
	{
		VirtActionInvoker0::Invoke(11 /* System.Void West::Move() */, __this);
	}

IL_002d:
	{
		return;
	}
}
// System.Void West::MoveSatrtPosition()
extern const MethodInfo* SingletonMonoBehaviour_1_get_Instance_m4038794971_MethodInfo_var;
extern const uint32_t West_MoveSatrtPosition_m1256805098_MetadataUsageId;
extern "C"  void West_MoveSatrtPosition_m1256805098 (West_t2692559 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (West_MoveSatrtPosition_m1256805098_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((Enemy_t67100520 *)__this)->set__ainum_11(0);
		ActionManager_t3022458999 * L_0 = SingletonMonoBehaviour_1_get_Instance_m4038794971(NULL /*static, unused*/, /*hidden argument*/SingletonMonoBehaviour_1_get_Instance_m4038794971_MethodInfo_var);
		GameObject_t3674682005 * L_1 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector3__ctor_m2926210380(&L_2, (11.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_0);
		ActionManager_StartMoveAction_m4230752139(L_0, L_1, L_2, (1.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void West::MoveStart()
extern const MethodInfo* SingletonMonoBehaviour_1_get_Instance_m877975176_MethodInfo_var;
extern const uint32_t West_MoveStart_m2541992219_MetadataUsageId;
extern "C"  void West_MoveStart_m2541992219 (West_t2692559 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (West_MoveStart_m2541992219_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AIData_t1930434546 * L_0 = ((Enemy_t67100520 *)__this)->get_address_of__aidata_10();
		Int32U5BU5D_t3230847821* L_1 = L_0->get__position_3();
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		int32_t L_2 = 0;
		int32_t L_3 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		Vector3_t4282066566 * L_4 = ((Charctor_t1500651146 *)__this)->get_address_of_m_pos_2();
		float L_5 = L_4->get_z_3();
		if ((!(((float)(((float)((float)L_3)))) < ((float)L_5))))
		{
			goto IL_0035;
		}
	}
	{
		CharData_t1499709504 * L_6 = ((Charctor_t1500651146 *)__this)->get_address_of__chardata_4();
		CharData_t1499709504 * L_7 = L_6;
		float L_8 = L_7->get__spd_1();
		L_7->set__spd_1(((float)((float)L_8*(float)(-1.0f))));
	}

IL_0035:
	{
		Vector3_t4282066566 * L_9 = ((Charctor_t1500651146 *)__this)->get_address_of_m_pos_2();
		AIData_t1930434546 * L_10 = ((Enemy_t67100520 *)__this)->get_address_of__aidata_10();
		Int32U5BU5D_t3230847821* L_11 = L_10->get__position_3();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		int32_t L_12 = 0;
		int32_t L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		L_9->set_z_3((((float)((float)((int32_t)((int32_t)L_13*(int32_t)4))))));
		((Charctor_t1500651146 *)__this)->set_ismove_3((bool)1);
		((Enemy_t67100520 *)__this)->set_isEnd_17((bool)0);
		Factory_t572770538 * L_14 = SingletonMonoBehaviour_1_get_Instance_m877975176(NULL /*static, unused*/, /*hidden argument*/SingletonMonoBehaviour_1_get_Instance_m877975176_MethodInfo_var);
		NullCheck(L_14);
		Factory_BulletisStop_m1808191848(L_14, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void West::ChangeMove()
extern const MethodInfo* Component_GetComponentInParent_TisEnemyManager_t1035150117_m1180037263_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1984785235;
extern const uint32_t West_ChangeMove_m133452857_MetadataUsageId;
extern "C"  void West_ChangeMove_m133452857 (West_t2692559 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (West_ChangeMove_m133452857_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AIData_t1930434546 * L_0 = ((Enemy_t67100520 *)__this)->get_address_of__aidata_10();
		Int32U5BU5D_t3230847821* L_1 = L_0->get__position_3();
		NullCheck(L_1);
		int32_t L_2 = ((Enemy_t67100520 *)__this)->get__ainum_11();
		if ((!(((uint32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))-(int32_t)1))) == ((uint32_t)L_2))))
		{
			goto IL_0038;
		}
	}
	{
		((Charctor_t1500651146 *)__this)->set_ismove_3((bool)0);
		((Enemy_t67100520 *)__this)->set__ainum_11(0);
		EnemyManager_t1035150117 * L_3 = Component_GetComponentInParent_TisEnemyManager_t1035150117_m1180037263(__this, /*hidden argument*/Component_GetComponentInParent_TisEnemyManager_t1035150117_m1180037263_MethodInfo_var);
		NullCheck(L_3);
		VirtActionInvoker0::Invoke(4 /* System.Void EnemyManager::AIEndCount() */, L_3);
		goto IL_0109;
	}

IL_0038:
	{
		int32_t L_4 = ((Enemy_t67100520 *)__this)->get__ainum_11();
		((Enemy_t67100520 *)__this)->set__ainum_11(((int32_t)((int32_t)L_4+(int32_t)1)));
		CharData_t1499709504 * L_5 = ((Charctor_t1500651146 *)__this)->get_address_of__chardata_4();
		float L_6 = L_5->get__spd_1();
		if ((!(((float)L_6) < ((float)(0.0f)))))
		{
			goto IL_0072;
		}
	}
	{
		CharData_t1499709504 * L_7 = ((Charctor_t1500651146 *)__this)->get_address_of__chardata_4();
		CharData_t1499709504 * L_8 = L_7;
		float L_9 = L_8->get__spd_1();
		L_8->set__spd_1(((float)((float)L_9*(float)(-1.0f))));
	}

IL_0072:
	{
		AIData_t1930434546 * L_10 = ((Enemy_t67100520 *)__this)->get_address_of__aidata_10();
		Int32U5BU5D_t3230847821* L_11 = L_10->get__position_3();
		int32_t L_12 = ((Enemy_t67100520 *)__this)->get__ainum_11();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = L_12;
		int32_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		Vector3_t4282066566 * L_15 = ((Charctor_t1500651146 *)__this)->get_address_of_m_pos_2();
		float L_16 = L_15->get_z_3();
		if ((!(((float)(((float)((float)((int32_t)((int32_t)L_14*(int32_t)4)))))) < ((float)L_16))))
		{
			goto IL_00ae;
		}
	}
	{
		CharData_t1499709504 * L_17 = ((Charctor_t1500651146 *)__this)->get_address_of__chardata_4();
		CharData_t1499709504 * L_18 = L_17;
		float L_19 = L_18->get__spd_1();
		L_18->set__spd_1(((float)((float)L_19*(float)(-1.0f))));
	}

IL_00ae:
	{
		Vector3_t4282066566 * L_20 = ((Charctor_t1500651146 *)__this)->get_address_of_m_pos_2();
		AIData_t1930434546 * L_21 = ((Enemy_t67100520 *)__this)->get_address_of__aidata_10();
		Int32U5BU5D_t3230847821* L_22 = L_21->get__position_3();
		int32_t L_23 = ((Enemy_t67100520 *)__this)->get__ainum_11();
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = L_23;
		int32_t L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		L_20->set_z_3((((float)((float)((int32_t)((int32_t)L_25*(int32_t)4))))));
		float L_26 = ((Enemy_t67100520 *)__this)->get__bullettechnich_15();
		CharData_t1499709504 * L_27 = ((Charctor_t1500651146 *)__this)->get_address_of__chardata_4();
		float L_28 = L_27->get__technique_2();
		float L_29 = ((Charctor_t1500651146 *)__this)->get_gameSpeed_5();
		MonoBehaviour_Invoke_m2825545578(__this, _stringLiteral1984785235, ((float)((float)L_26-(float)((float)((float)L_28/(float)L_29)))), /*hidden argument*/NULL);
		CharData_t1499709504 * L_30 = ((Charctor_t1500651146 *)__this)->get_address_of__chardata_4();
		float L_31 = L_30->get__technique_2();
		((Enemy_t67100520 *)__this)->set__bullettechnich_15(((float)((float)L_31*(float)(2.0f))));
	}

IL_0109:
	{
		return;
	}
}
// System.Void West::setCoordinate()
extern "C"  void West_setCoordinate_m3530483428 (West_t2692559 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void West::Shot()
extern const MethodInfo* SingletonMonoBehaviour_1_get_Instance_m2019541994_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisBullet_t2000900386_m4055895035_MethodInfo_var;
extern const uint32_t West_Shot_m185450130_MetadataUsageId;
extern "C"  void West_Shot_m185450130 (West_t2692559 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (West_Shot_m185450130_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t3674682005 * V_0 = NULL;
	{
		SoundManager_t2444342206 * L_0 = SingletonMonoBehaviour_1_get_Instance_m2019541994(NULL /*static, unused*/, /*hidden argument*/SingletonMonoBehaviour_1_get_Instance_m2019541994_MethodInfo_var);
		NullCheck(L_0);
		SoundManager_SEShotPlay_m2178939431(L_0, /*hidden argument*/NULL);
		West_ShotParticle_m2505334456(__this, /*hidden argument*/NULL);
		Factory_t572770538 * L_1 = ((Enemy_t67100520 *)__this)->get_f_12();
		AIData_t1930434546 * L_2 = ((Enemy_t67100520 *)__this)->get_address_of__aidata_10();
		Int32U5BU5D_t3230847821* L_3 = L_2->get__bullet_2();
		int32_t L_4 = ((Enemy_t67100520 *)__this)->get__ainum_11();
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		int32_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_1);
		GameObject_t3674682005 * L_7 = Factory_CreateBullet_m576246781(L_1, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		GameObject_t3674682005 * L_8 = V_0;
		GameObject_t3674682005 * L_9 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_10 = GameObject_get_layer_m1648550306(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		GameObject_set_layer_m1872241535(L_8, L_10, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_11 = V_0;
		NullCheck(L_11);
		Transform_t1659122786 * L_12 = GameObject_get_transform_m1278640159(L_11, /*hidden argument*/NULL);
		Vector3_t4282066566  L_13 = ((Charctor_t1500651146 *)__this)->get_m_pos_2();
		NullCheck(L_12);
		Transform_set_localPosition_m3504330903(L_12, L_13, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_14 = V_0;
		NullCheck(L_14);
		Bullet_t2000900386 * L_15 = GameObject_GetComponent_TisBullet_t2000900386_m4055895035(L_14, /*hidden argument*/GameObject_GetComponent_TisBullet_t2000900386_m4055895035_MethodInfo_var);
		NullCheck(L_15);
		VirtActionInvoker0::Invoke(10 /* System.Void Bullet::Shot() */, L_15);
		GameObject_t3674682005 * L_16 = V_0;
		NullCheck(L_16);
		Bullet_t2000900386 * L_17 = GameObject_GetComponent_TisBullet_t2000900386_m4055895035(L_16, /*hidden argument*/GameObject_GetComponent_TisBullet_t2000900386_m4055895035_MethodInfo_var);
		NullCheck(L_17);
		VirtActionInvoker1< int32_t >::Invoke(9 /* System.Void Bullet::set_direction(Direction) */, L_17, 1);
		GameObject_t3674682005 * L_18 = V_0;
		NullCheck(L_18);
		Bullet_t2000900386 * L_19 = GameObject_GetComponent_TisBullet_t2000900386_m4055895035(L_18, /*hidden argument*/GameObject_GetComponent_TisBullet_t2000900386_m4055895035_MethodInfo_var);
		float L_20 = ((Charctor_t1500651146 *)__this)->get_gameSpeed_5();
		NullCheck(L_19);
		Bullet_SetGameSpeed_m279588749(L_19, L_20, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_21 = V_0;
		NullCheck(L_21);
		Bullet_t2000900386 * L_22 = GameObject_GetComponent_TisBullet_t2000900386_m4055895035(L_21, /*hidden argument*/GameObject_GetComponent_TisBullet_t2000900386_m4055895035_MethodInfo_var);
		NullCheck(L_22);
		float L_23 = VirtFuncInvoker0< float >::Invoke(8 /* System.Single Bullet::get_technique() */, L_22);
		((Enemy_t67100520 *)__this)->set__bullettechnich_15(L_23);
		VirtActionInvoker0::Invoke(18 /* System.Void West::ChangeMove() */, __this);
		return;
	}
}
// System.Void West::Move()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_FindObjectOfType_TisStagePanelManager_t334945031_m3286582442_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2576154;
extern const uint32_t West_Move_m20333993_MetadataUsageId;
extern "C"  void West_Move_m20333993 (West_t2692559 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (West_Move_m20333993_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	bool V_1 = false;
	{
		Transform_t1659122786 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t4282066566  L_1 = Transform_get_localPosition_m668140784(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		V_1 = (bool)0;
		CharData_t1499709504 * L_2 = ((Charctor_t1500651146 *)__this)->get_address_of__chardata_4();
		float L_3 = L_2->get__spd_1();
		if ((!(((float)L_3) >= ((float)(0.0f)))))
		{
			goto IL_0041;
		}
	}
	{
		Vector3_t4282066566 * L_4 = ((Charctor_t1500651146 *)__this)->get_address_of_m_pos_2();
		float L_5 = L_4->get_z_3();
		float L_6 = (&V_0)->get_z_3();
		if ((!(((float)L_5) <= ((float)L_6))))
		{
			goto IL_003c;
		}
	}
	{
		V_1 = (bool)1;
	}

IL_003c:
	{
		goto IL_005a;
	}

IL_0041:
	{
		Vector3_t4282066566 * L_7 = ((Charctor_t1500651146 *)__this)->get_address_of_m_pos_2();
		float L_8 = L_7->get_z_3();
		float L_9 = (&V_0)->get_z_3();
		if ((!(((float)L_8) >= ((float)L_9))))
		{
			goto IL_005a;
		}
	}
	{
		V_1 = (bool)1;
	}

IL_005a:
	{
		bool L_10 = V_1;
		if (!L_10)
		{
			goto IL_00ad;
		}
	}
	{
		CharData_t1499709504 * L_11 = ((Charctor_t1500651146 *)__this)->get_address_of__chardata_4();
		float L_12 = L_11->get__technique_2();
		float L_13 = ((Charctor_t1500651146 *)__this)->get_gameSpeed_5();
		MonoBehaviour_Invoke_m2825545578(__this, _stringLiteral2576154, ((float)((float)L_12/(float)L_13)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		StagePanelManager_t334945031 * L_14 = Object_FindObjectOfType_TisStagePanelManager_t334945031_m3286582442(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisStagePanelManager_t334945031_m3286582442_MethodInfo_var);
		Vector3_t4282066566 * L_15 = ((Charctor_t1500651146 *)__this)->get_address_of_m_pos_2();
		float L_16 = L_15->get_z_3();
		CharData_t1499709504 * L_17 = ((Charctor_t1500651146 *)__this)->get_address_of__chardata_4();
		float L_18 = L_17->get__technique_2();
		float L_19 = ((Charctor_t1500651146 *)__this)->get_gameSpeed_5();
		NullCheck(L_14);
		StagePanelManager_LightAction_m1023904617(L_14, 1, L_16, ((float)((float)L_18/(float)L_19)), /*hidden argument*/NULL);
		Charctor_set_IsMove_m2640039202(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}

IL_00ad:
	{
		Transform_t1659122786 * L_20 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		float L_21 = (&V_0)->get_x_1();
		float L_22 = (&V_0)->get_y_2();
		float L_23 = (&V_0)->get_z_3();
		CharData_t1499709504 * L_24 = ((Charctor_t1500651146 *)__this)->get_address_of__chardata_4();
		float L_25 = L_24->get__spd_1();
		float L_26 = ((Charctor_t1500651146 *)__this)->get_gameSpeed_5();
		Vector3_t4282066566  L_27;
		memset(&L_27, 0, sizeof(L_27));
		Vector3__ctor_m2926210380(&L_27, L_21, L_22, ((float)((float)L_23+(float)((float)((float)L_25*(float)L_26)))), /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_set_localPosition_m3504330903(L_20, L_27, /*hidden argument*/NULL);
		return;
	}
}
// System.Void West::ShotParticle()
extern Il2CppClass* Object_t3071478659_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_FindObjectOfType_TisParticleManager_t73248295_m857524746_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisParticleSystem_t381473177_m2450916872_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2450281669;
extern const uint32_t West_ShotParticle_m2505334456_MetadataUsageId;
extern "C"  void West_ShotParticle_m2505334456 (West_t2692559 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (West_ShotParticle_m2505334456_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = ((Enemy_t67100520 *)__this)->get_shotPar_16();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		ParticleManager_t73248295 * L_2 = Object_FindObjectOfType_TisParticleManager_t73248295_m857524746(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisParticleManager_t73248295_m857524746_MethodInfo_var);
		GameObject_t3674682005 * L_3 = ((Enemy_t67100520 *)__this)->get_shotPar_16();
		NullCheck(L_2);
		ParticleManager_ParticleStop_m3909569472(L_2, L_3, /*hidden argument*/NULL);
	}

IL_0020:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t3071478659_il2cpp_TypeInfo_var);
		ParticleManager_t73248295 * L_4 = Object_FindObjectOfType_TisParticleManager_t73248295_m857524746(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisParticleManager_t73248295_m857524746_MethodInfo_var);
		Vector3_t4282066566 * L_5 = ((Charctor_t1500651146 *)__this)->get_address_of_m_pos_2();
		float L_6 = L_5->get_y_2();
		Vector3_t4282066566 * L_7 = ((Charctor_t1500651146 *)__this)->get_address_of_m_pos_2();
		float L_8 = L_7->get_z_3();
		Vector3_t4282066566  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m2926210380(&L_9, (9.5f), L_6, L_8, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_t3674682005 * L_10 = ParticleManager_ShotParticleStart_m3278722592(L_4, L_9, /*hidden argument*/NULL);
		((Enemy_t67100520 *)__this)->set_shotPar_16(L_10);
		GameObject_t3674682005 * L_11 = ((Enemy_t67100520 *)__this)->get_shotPar_16();
		NullCheck(L_11);
		ParticleSystem_t381473177 * L_12 = GameObject_GetComponent_TisParticleSystem_t381473177_m2450916872(L_11, /*hidden argument*/GameObject_GetComponent_TisParticleSystem_t381473177_m2450916872_MethodInfo_var);
		NullCheck(L_12);
		float L_13 = ParticleSystem_get_duration_m4044642069(L_12, /*hidden argument*/NULL);
		MonoBehaviour_Invoke_m2825545578(__this, _stringLiteral2450281669, L_13, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
