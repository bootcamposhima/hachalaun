﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_3_gen1621897645MethodDeclarations.h"

// System.Void System.Action`3<WebSocketSharp.PayloadData,System.Boolean,System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Action_3__ctor_m4272236052(__this, ___object0, ___method1, method) ((  void (*) (Action_3_t2371519456 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_3__ctor_m1093315275_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`3<WebSocketSharp.PayloadData,System.Boolean,System.Boolean>::Invoke(T1,T2,T3)
#define Action_3_Invoke_m1373574848(__this, ___arg10, ___arg21, ___arg32, method) ((  void (*) (Action_3_t2371519456 *, PayloadData_t39926750 *, bool, bool, const MethodInfo*))Action_3_Invoke_m3280101959_gshared)(__this, ___arg10, ___arg21, ___arg32, method)
// System.IAsyncResult System.Action`3<WebSocketSharp.PayloadData,System.Boolean,System.Boolean>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
#define Action_3_BeginInvoke_m842234001(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method) ((  Il2CppObject * (*) (Action_3_t2371519456 *, PayloadData_t39926750 *, bool, bool, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Action_3_BeginInvoke_m257344126_gshared)(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method)
// System.Void System.Action`3<WebSocketSharp.PayloadData,System.Boolean,System.Boolean>::EndInvoke(System.IAsyncResult)
#define Action_3_EndInvoke_m2416362020(__this, ___result0, method) ((  void (*) (Action_3_t2371519456 *, Il2CppObject *, const MethodInfo*))Action_3_EndInvoke_m2172729947_gshared)(__this, ___result0, method)
