﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object
struct Il2CppObject;
// ActionManager
struct ActionManager_t3022458999;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ActionManager/<ForeverMoveAction>c__Iterator5
struct  U3CForeverMoveActionU3Ec__Iterator5_t899850968  : public Il2CppObject
{
public:
	// UnityEngine.GameObject ActionManager/<ForeverMoveAction>c__Iterator5::_ob
	GameObject_t3674682005 * ____ob_0;
	// UnityEngine.Vector2 ActionManager/<ForeverMoveAction>c__Iterator5::<defaultpos>__0
	Vector2_t4282066565  ___U3CdefaultposU3E__0_1;
	// UnityEngine.Vector3 ActionManager/<ForeverMoveAction>c__Iterator5::destination
	Vector3_t4282066566  ___destination_2;
	// System.Single ActionManager/<ForeverMoveAction>c__Iterator5::deltatime
	float ___deltatime_3;
	// System.Single ActionManager/<ForeverMoveAction>c__Iterator5::waittime
	float ___waittime_4;
	// System.Int32 ActionManager/<ForeverMoveAction>c__Iterator5::$PC
	int32_t ___U24PC_5;
	// System.Object ActionManager/<ForeverMoveAction>c__Iterator5::$current
	Il2CppObject * ___U24current_6;
	// UnityEngine.GameObject ActionManager/<ForeverMoveAction>c__Iterator5::<$>_ob
	GameObject_t3674682005 * ___U3CU24U3E_ob_7;
	// UnityEngine.Vector3 ActionManager/<ForeverMoveAction>c__Iterator5::<$>destination
	Vector3_t4282066566  ___U3CU24U3Edestination_8;
	// System.Single ActionManager/<ForeverMoveAction>c__Iterator5::<$>deltatime
	float ___U3CU24U3Edeltatime_9;
	// System.Single ActionManager/<ForeverMoveAction>c__Iterator5::<$>waittime
	float ___U3CU24U3Ewaittime_10;
	// ActionManager ActionManager/<ForeverMoveAction>c__Iterator5::<>f__this
	ActionManager_t3022458999 * ___U3CU3Ef__this_11;

public:
	inline static int32_t get_offset_of__ob_0() { return static_cast<int32_t>(offsetof(U3CForeverMoveActionU3Ec__Iterator5_t899850968, ____ob_0)); }
	inline GameObject_t3674682005 * get__ob_0() const { return ____ob_0; }
	inline GameObject_t3674682005 ** get_address_of__ob_0() { return &____ob_0; }
	inline void set__ob_0(GameObject_t3674682005 * value)
	{
		____ob_0 = value;
		Il2CppCodeGenWriteBarrier(&____ob_0, value);
	}

	inline static int32_t get_offset_of_U3CdefaultposU3E__0_1() { return static_cast<int32_t>(offsetof(U3CForeverMoveActionU3Ec__Iterator5_t899850968, ___U3CdefaultposU3E__0_1)); }
	inline Vector2_t4282066565  get_U3CdefaultposU3E__0_1() const { return ___U3CdefaultposU3E__0_1; }
	inline Vector2_t4282066565 * get_address_of_U3CdefaultposU3E__0_1() { return &___U3CdefaultposU3E__0_1; }
	inline void set_U3CdefaultposU3E__0_1(Vector2_t4282066565  value)
	{
		___U3CdefaultposU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_destination_2() { return static_cast<int32_t>(offsetof(U3CForeverMoveActionU3Ec__Iterator5_t899850968, ___destination_2)); }
	inline Vector3_t4282066566  get_destination_2() const { return ___destination_2; }
	inline Vector3_t4282066566 * get_address_of_destination_2() { return &___destination_2; }
	inline void set_destination_2(Vector3_t4282066566  value)
	{
		___destination_2 = value;
	}

	inline static int32_t get_offset_of_deltatime_3() { return static_cast<int32_t>(offsetof(U3CForeverMoveActionU3Ec__Iterator5_t899850968, ___deltatime_3)); }
	inline float get_deltatime_3() const { return ___deltatime_3; }
	inline float* get_address_of_deltatime_3() { return &___deltatime_3; }
	inline void set_deltatime_3(float value)
	{
		___deltatime_3 = value;
	}

	inline static int32_t get_offset_of_waittime_4() { return static_cast<int32_t>(offsetof(U3CForeverMoveActionU3Ec__Iterator5_t899850968, ___waittime_4)); }
	inline float get_waittime_4() const { return ___waittime_4; }
	inline float* get_address_of_waittime_4() { return &___waittime_4; }
	inline void set_waittime_4(float value)
	{
		___waittime_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CForeverMoveActionU3Ec__Iterator5_t899850968, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CForeverMoveActionU3Ec__Iterator5_t899850968, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U3CU24U3E_ob_7() { return static_cast<int32_t>(offsetof(U3CForeverMoveActionU3Ec__Iterator5_t899850968, ___U3CU24U3E_ob_7)); }
	inline GameObject_t3674682005 * get_U3CU24U3E_ob_7() const { return ___U3CU24U3E_ob_7; }
	inline GameObject_t3674682005 ** get_address_of_U3CU24U3E_ob_7() { return &___U3CU24U3E_ob_7; }
	inline void set_U3CU24U3E_ob_7(GameObject_t3674682005 * value)
	{
		___U3CU24U3E_ob_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3E_ob_7, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Edestination_8() { return static_cast<int32_t>(offsetof(U3CForeverMoveActionU3Ec__Iterator5_t899850968, ___U3CU24U3Edestination_8)); }
	inline Vector3_t4282066566  get_U3CU24U3Edestination_8() const { return ___U3CU24U3Edestination_8; }
	inline Vector3_t4282066566 * get_address_of_U3CU24U3Edestination_8() { return &___U3CU24U3Edestination_8; }
	inline void set_U3CU24U3Edestination_8(Vector3_t4282066566  value)
	{
		___U3CU24U3Edestination_8 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Edeltatime_9() { return static_cast<int32_t>(offsetof(U3CForeverMoveActionU3Ec__Iterator5_t899850968, ___U3CU24U3Edeltatime_9)); }
	inline float get_U3CU24U3Edeltatime_9() const { return ___U3CU24U3Edeltatime_9; }
	inline float* get_address_of_U3CU24U3Edeltatime_9() { return &___U3CU24U3Edeltatime_9; }
	inline void set_U3CU24U3Edeltatime_9(float value)
	{
		___U3CU24U3Edeltatime_9 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Ewaittime_10() { return static_cast<int32_t>(offsetof(U3CForeverMoveActionU3Ec__Iterator5_t899850968, ___U3CU24U3Ewaittime_10)); }
	inline float get_U3CU24U3Ewaittime_10() const { return ___U3CU24U3Ewaittime_10; }
	inline float* get_address_of_U3CU24U3Ewaittime_10() { return &___U3CU24U3Ewaittime_10; }
	inline void set_U3CU24U3Ewaittime_10(float value)
	{
		___U3CU24U3Ewaittime_10 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_11() { return static_cast<int32_t>(offsetof(U3CForeverMoveActionU3Ec__Iterator5_t899850968, ___U3CU3Ef__this_11)); }
	inline ActionManager_t3022458999 * get_U3CU3Ef__this_11() const { return ___U3CU3Ef__this_11; }
	inline ActionManager_t3022458999 ** get_address_of_U3CU3Ef__this_11() { return &___U3CU3Ef__this_11; }
	inline void set_U3CU3Ef__this_11(ActionManager_t3022458999 * value)
	{
		___U3CU3Ef__this_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
