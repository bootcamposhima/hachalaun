﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V4021398974MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.CompressionMethod,System.IO.Stream>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m4247150531(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t1412346747 *, Dictionary_2_t2711741034 *, const MethodInfo*))ValueCollection__ctor_m2651207012_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.CompressionMethod,System.IO.Stream>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1669027855(__this, ___item0, method) ((  void (*) (ValueCollection_t1412346747 *, Stream_t1561764144 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1617545806_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.CompressionMethod,System.IO.Stream>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2764416088(__this, method) ((  void (*) (ValueCollection_t1412346747 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1259146775_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.CompressionMethod,System.IO.Stream>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m693273335(__this, ___item0, method) ((  bool (*) (ValueCollection_t1412346747 *, Stream_t1561764144 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2949579100_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.CompressionMethod,System.IO.Stream>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2920148316(__this, ___item0, method) ((  bool (*) (ValueCollection_t1412346747 *, Stream_t1561764144 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2716909825_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.CompressionMethod,System.IO.Stream>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1648539174(__this, method) ((  Il2CppObject* (*) (ValueCollection_t1412346747 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1427265367_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.CompressionMethod,System.IO.Stream>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m802994012(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1412346747 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3444981339_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.CompressionMethod,System.IO.Stream>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1576231895(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1412346747 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m560456234_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.CompressionMethod,System.IO.Stream>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3263416490(__this, method) ((  bool (*) (ValueCollection_t1412346747 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1224754959_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.CompressionMethod,System.IO.Stream>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3175234954(__this, method) ((  bool (*) (ValueCollection_t1412346747 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1021668207_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.CompressionMethod,System.IO.Stream>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2764873910(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1412346747 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m3513245089_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.CompressionMethod,System.IO.Stream>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m3105950538(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1412346747 *, StreamU5BU5D_t2637972497*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1119338667_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.CompressionMethod,System.IO.Stream>::GetEnumerator()
#define ValueCollection_GetEnumerator_m2665665005(__this, method) ((  Enumerator_t643574442  (*) (ValueCollection_t1412346747 *, const MethodInfo*))ValueCollection_GetEnumerator_m345604308_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.CompressionMethod,System.IO.Stream>::get_Count()
#define ValueCollection_get_Count_m4126180176(__this, method) ((  int32_t (*) (ValueCollection_t1412346747 *, const MethodInfo*))ValueCollection_get_Count_m1761333417_gshared)(__this, method)
