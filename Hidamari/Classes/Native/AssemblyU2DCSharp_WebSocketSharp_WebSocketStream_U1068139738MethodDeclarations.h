﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.WebSocketStream/<ReadHandshake>c__AnonStorey33`1<System.Object>
struct U3CReadHandshakeU3Ec__AnonStorey33_1_t1068139738;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void WebSocketSharp.WebSocketStream/<ReadHandshake>c__AnonStorey33`1<System.Object>::.ctor()
extern "C"  void U3CReadHandshakeU3Ec__AnonStorey33_1__ctor_m1644562545_gshared (U3CReadHandshakeU3Ec__AnonStorey33_1_t1068139738 * __this, const MethodInfo* method);
#define U3CReadHandshakeU3Ec__AnonStorey33_1__ctor_m1644562545(__this, method) ((  void (*) (U3CReadHandshakeU3Ec__AnonStorey33_1_t1068139738 *, const MethodInfo*))U3CReadHandshakeU3Ec__AnonStorey33_1__ctor_m1644562545_gshared)(__this, method)
// System.Void WebSocketSharp.WebSocketStream/<ReadHandshake>c__AnonStorey33`1<System.Object>::<>m__1D(System.Object)
extern "C"  void U3CReadHandshakeU3Ec__AnonStorey33_1_U3CU3Em__1D_m782350919_gshared (U3CReadHandshakeU3Ec__AnonStorey33_1_t1068139738 * __this, Il2CppObject * ___state0, const MethodInfo* method);
#define U3CReadHandshakeU3Ec__AnonStorey33_1_U3CU3Em__1D_m782350919(__this, ___state0, method) ((  void (*) (U3CReadHandshakeU3Ec__AnonStorey33_1_t1068139738 *, Il2CppObject *, const MethodInfo*))U3CReadHandshakeU3Ec__AnonStorey33_1_U3CU3Em__1D_m782350919_gshared)(__this, ___state0, method)
