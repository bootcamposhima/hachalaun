﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GodTouches.ClickLongPressDrag
struct ClickLongPressDrag_t90319163;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1848751023;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GodTouches_ClickLongPressDrag_Ev1380888277.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1848751023.h"

// System.Void GodTouches.ClickLongPressDrag::.ctor()
extern "C"  void ClickLongPressDrag__ctor_m2481387035 (ClickLongPressDrag_t90319163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GodTouches.ClickLongPressDrag::get_IsRunning()
extern "C"  bool ClickLongPressDrag_get_IsRunning_m1409902457 (ClickLongPressDrag_t90319163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GodTouches.ClickLongPressDrag::SetType(GodTouches.ClickLongPressDrag/EventType)
extern "C"  void ClickLongPressDrag_SetType_m16773544 (ClickLongPressDrag_t90319163 * __this, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GodTouches.ClickLongPressDrag::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ClickLongPressDrag_OnPointerDown_m1464858949 (ClickLongPressDrag_t90319163 * __this, PointerEventData_t1848751023 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GodTouches.ClickLongPressDrag::Update()
extern "C"  void ClickLongPressDrag_Update_m1340448850 (ClickLongPressDrag_t90319163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GodTouches.ClickLongPressDrag::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C"  void ClickLongPressDrag_OnPointerUp_m873646124 (ClickLongPressDrag_t90319163 * __this, PointerEventData_t1848751023 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
