﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TextLodaer
struct TextLodaer_t3639884186;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void TextLodaer::.ctor()
extern "C"  void TextLodaer__ctor_m3820325953 (TextLodaer_t3639884186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TextLodaer::Start()
extern "C"  void TextLodaer_Start_m2767463745 (TextLodaer_t3639884186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TextLodaer::Awake()
extern "C"  void TextLodaer_Awake_m4057931172 (TextLodaer_t3639884186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TextLodaer::Update()
extern "C"  void TextLodaer_Update_m4192849644 (TextLodaer_t3639884186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TextLodaer::ReadTextFile(System.String)
extern "C"  String_t* TextLodaer_ReadTextFile_m651006877 (TextLodaer_t3639884186 * __this, String_t* ___filename0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String TextLodaer::ReadTextFile2(System.String)
extern "C"  String_t* TextLodaer_ReadTextFile2_m902875315 (TextLodaer_t3639884186 * __this, String_t* ___filename0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TextLodaer::WriteTextFile(System.String,System.String)
extern "C"  void TextLodaer_WriteTextFile_m1706503351 (TextLodaer_t3639884186 * __this, String_t* ___filename0, String_t* ___jstr1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TextLodaer::WriteTextFile2(System.String,System.String)
extern "C"  void TextLodaer_WriteTextFile2_m2378868689 (TextLodaer_t3639884186 * __this, String_t* ___filename0, String_t* ___jstr1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
