﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefenceManager
struct  DefenceManager_t3329977693  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject DefenceManager::_hpgauge
	GameObject_t3674682005 * ____hpgauge_2;
	// UnityEngine.GameObject DefenceManager::_hptext
	GameObject_t3674682005 * ____hptext_3;
	// UnityEngine.GameObject DefenceManager::_AI
	GameObject_t3674682005 * ____AI_4;
	// UnityEngine.GameObject DefenceManager::round
	GameObject_t3674682005 * ___round_5;
	// UnityEngine.GameObject DefenceManager::ob
	GameObject_t3674682005 * ___ob_6;
	// System.Boolean DefenceManager::_isAI
	bool ____isAI_7;

public:
	inline static int32_t get_offset_of__hpgauge_2() { return static_cast<int32_t>(offsetof(DefenceManager_t3329977693, ____hpgauge_2)); }
	inline GameObject_t3674682005 * get__hpgauge_2() const { return ____hpgauge_2; }
	inline GameObject_t3674682005 ** get_address_of__hpgauge_2() { return &____hpgauge_2; }
	inline void set__hpgauge_2(GameObject_t3674682005 * value)
	{
		____hpgauge_2 = value;
		Il2CppCodeGenWriteBarrier(&____hpgauge_2, value);
	}

	inline static int32_t get_offset_of__hptext_3() { return static_cast<int32_t>(offsetof(DefenceManager_t3329977693, ____hptext_3)); }
	inline GameObject_t3674682005 * get__hptext_3() const { return ____hptext_3; }
	inline GameObject_t3674682005 ** get_address_of__hptext_3() { return &____hptext_3; }
	inline void set__hptext_3(GameObject_t3674682005 * value)
	{
		____hptext_3 = value;
		Il2CppCodeGenWriteBarrier(&____hptext_3, value);
	}

	inline static int32_t get_offset_of__AI_4() { return static_cast<int32_t>(offsetof(DefenceManager_t3329977693, ____AI_4)); }
	inline GameObject_t3674682005 * get__AI_4() const { return ____AI_4; }
	inline GameObject_t3674682005 ** get_address_of__AI_4() { return &____AI_4; }
	inline void set__AI_4(GameObject_t3674682005 * value)
	{
		____AI_4 = value;
		Il2CppCodeGenWriteBarrier(&____AI_4, value);
	}

	inline static int32_t get_offset_of_round_5() { return static_cast<int32_t>(offsetof(DefenceManager_t3329977693, ___round_5)); }
	inline GameObject_t3674682005 * get_round_5() const { return ___round_5; }
	inline GameObject_t3674682005 ** get_address_of_round_5() { return &___round_5; }
	inline void set_round_5(GameObject_t3674682005 * value)
	{
		___round_5 = value;
		Il2CppCodeGenWriteBarrier(&___round_5, value);
	}

	inline static int32_t get_offset_of_ob_6() { return static_cast<int32_t>(offsetof(DefenceManager_t3329977693, ___ob_6)); }
	inline GameObject_t3674682005 * get_ob_6() const { return ___ob_6; }
	inline GameObject_t3674682005 ** get_address_of_ob_6() { return &___ob_6; }
	inline void set_ob_6(GameObject_t3674682005 * value)
	{
		___ob_6 = value;
		Il2CppCodeGenWriteBarrier(&___ob_6, value);
	}

	inline static int32_t get_offset_of__isAI_7() { return static_cast<int32_t>(offsetof(DefenceManager_t3329977693, ____isAI_7)); }
	inline bool get__isAI_7() const { return ____isAI_7; }
	inline bool* get_address_of__isAI_7() { return &____isAI_7; }
	inline void set__isAI_7(bool value)
	{
		____isAI_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
