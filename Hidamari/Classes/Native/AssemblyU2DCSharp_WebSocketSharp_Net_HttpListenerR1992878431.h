﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Text.Encoding
struct Encoding_t2012439129;
// System.String
struct String_t;
// WebSocketSharp.Net.HttpListenerContext
struct HttpListenerContext_t3744659101;
// WebSocketSharp.Net.CookieCollection
struct CookieCollection_t1136277956;
// WebSocketSharp.Net.WebHeaderCollection
struct WebHeaderCollection_t288332393;
// WebSocketSharp.Net.ResponseStream
struct ResponseStream_t1796293571;
// System.Version
struct Version_t763695022;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpListenerResponse
struct  HttpListenerResponse_t1992878431  : public Il2CppObject
{
public:
	// System.Boolean WebSocketSharp.Net.HttpListenerResponse::_chunked
	bool ____chunked_0;
	// System.Text.Encoding WebSocketSharp.Net.HttpListenerResponse::_contentEncoding
	Encoding_t2012439129 * ____contentEncoding_1;
	// System.Int64 WebSocketSharp.Net.HttpListenerResponse::_contentLength
	int64_t ____contentLength_2;
	// System.Boolean WebSocketSharp.Net.HttpListenerResponse::_contentLengthSet
	bool ____contentLengthSet_3;
	// System.String WebSocketSharp.Net.HttpListenerResponse::_contentType
	String_t* ____contentType_4;
	// WebSocketSharp.Net.HttpListenerContext WebSocketSharp.Net.HttpListenerResponse::_context
	HttpListenerContext_t3744659101 * ____context_5;
	// WebSocketSharp.Net.CookieCollection WebSocketSharp.Net.HttpListenerResponse::_cookies
	CookieCollection_t1136277956 * ____cookies_6;
	// System.Boolean WebSocketSharp.Net.HttpListenerResponse::_disposed
	bool ____disposed_7;
	// System.Boolean WebSocketSharp.Net.HttpListenerResponse::_forceCloseChunked
	bool ____forceCloseChunked_8;
	// WebSocketSharp.Net.WebHeaderCollection WebSocketSharp.Net.HttpListenerResponse::_headers
	WebHeaderCollection_t288332393 * ____headers_9;
	// System.Boolean WebSocketSharp.Net.HttpListenerResponse::_headersSent
	bool ____headersSent_10;
	// System.Boolean WebSocketSharp.Net.HttpListenerResponse::_keepAlive
	bool ____keepAlive_11;
	// System.String WebSocketSharp.Net.HttpListenerResponse::_location
	String_t* ____location_12;
	// WebSocketSharp.Net.ResponseStream WebSocketSharp.Net.HttpListenerResponse::_outputStream
	ResponseStream_t1796293571 * ____outputStream_13;
	// System.Int32 WebSocketSharp.Net.HttpListenerResponse::_statusCode
	int32_t ____statusCode_14;
	// System.String WebSocketSharp.Net.HttpListenerResponse::_statusDescription
	String_t* ____statusDescription_15;
	// System.Version WebSocketSharp.Net.HttpListenerResponse::_version
	Version_t763695022 * ____version_16;

public:
	inline static int32_t get_offset_of__chunked_0() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t1992878431, ____chunked_0)); }
	inline bool get__chunked_0() const { return ____chunked_0; }
	inline bool* get_address_of__chunked_0() { return &____chunked_0; }
	inline void set__chunked_0(bool value)
	{
		____chunked_0 = value;
	}

	inline static int32_t get_offset_of__contentEncoding_1() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t1992878431, ____contentEncoding_1)); }
	inline Encoding_t2012439129 * get__contentEncoding_1() const { return ____contentEncoding_1; }
	inline Encoding_t2012439129 ** get_address_of__contentEncoding_1() { return &____contentEncoding_1; }
	inline void set__contentEncoding_1(Encoding_t2012439129 * value)
	{
		____contentEncoding_1 = value;
		Il2CppCodeGenWriteBarrier(&____contentEncoding_1, value);
	}

	inline static int32_t get_offset_of__contentLength_2() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t1992878431, ____contentLength_2)); }
	inline int64_t get__contentLength_2() const { return ____contentLength_2; }
	inline int64_t* get_address_of__contentLength_2() { return &____contentLength_2; }
	inline void set__contentLength_2(int64_t value)
	{
		____contentLength_2 = value;
	}

	inline static int32_t get_offset_of__contentLengthSet_3() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t1992878431, ____contentLengthSet_3)); }
	inline bool get__contentLengthSet_3() const { return ____contentLengthSet_3; }
	inline bool* get_address_of__contentLengthSet_3() { return &____contentLengthSet_3; }
	inline void set__contentLengthSet_3(bool value)
	{
		____contentLengthSet_3 = value;
	}

	inline static int32_t get_offset_of__contentType_4() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t1992878431, ____contentType_4)); }
	inline String_t* get__contentType_4() const { return ____contentType_4; }
	inline String_t** get_address_of__contentType_4() { return &____contentType_4; }
	inline void set__contentType_4(String_t* value)
	{
		____contentType_4 = value;
		Il2CppCodeGenWriteBarrier(&____contentType_4, value);
	}

	inline static int32_t get_offset_of__context_5() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t1992878431, ____context_5)); }
	inline HttpListenerContext_t3744659101 * get__context_5() const { return ____context_5; }
	inline HttpListenerContext_t3744659101 ** get_address_of__context_5() { return &____context_5; }
	inline void set__context_5(HttpListenerContext_t3744659101 * value)
	{
		____context_5 = value;
		Il2CppCodeGenWriteBarrier(&____context_5, value);
	}

	inline static int32_t get_offset_of__cookies_6() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t1992878431, ____cookies_6)); }
	inline CookieCollection_t1136277956 * get__cookies_6() const { return ____cookies_6; }
	inline CookieCollection_t1136277956 ** get_address_of__cookies_6() { return &____cookies_6; }
	inline void set__cookies_6(CookieCollection_t1136277956 * value)
	{
		____cookies_6 = value;
		Il2CppCodeGenWriteBarrier(&____cookies_6, value);
	}

	inline static int32_t get_offset_of__disposed_7() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t1992878431, ____disposed_7)); }
	inline bool get__disposed_7() const { return ____disposed_7; }
	inline bool* get_address_of__disposed_7() { return &____disposed_7; }
	inline void set__disposed_7(bool value)
	{
		____disposed_7 = value;
	}

	inline static int32_t get_offset_of__forceCloseChunked_8() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t1992878431, ____forceCloseChunked_8)); }
	inline bool get__forceCloseChunked_8() const { return ____forceCloseChunked_8; }
	inline bool* get_address_of__forceCloseChunked_8() { return &____forceCloseChunked_8; }
	inline void set__forceCloseChunked_8(bool value)
	{
		____forceCloseChunked_8 = value;
	}

	inline static int32_t get_offset_of__headers_9() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t1992878431, ____headers_9)); }
	inline WebHeaderCollection_t288332393 * get__headers_9() const { return ____headers_9; }
	inline WebHeaderCollection_t288332393 ** get_address_of__headers_9() { return &____headers_9; }
	inline void set__headers_9(WebHeaderCollection_t288332393 * value)
	{
		____headers_9 = value;
		Il2CppCodeGenWriteBarrier(&____headers_9, value);
	}

	inline static int32_t get_offset_of__headersSent_10() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t1992878431, ____headersSent_10)); }
	inline bool get__headersSent_10() const { return ____headersSent_10; }
	inline bool* get_address_of__headersSent_10() { return &____headersSent_10; }
	inline void set__headersSent_10(bool value)
	{
		____headersSent_10 = value;
	}

	inline static int32_t get_offset_of__keepAlive_11() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t1992878431, ____keepAlive_11)); }
	inline bool get__keepAlive_11() const { return ____keepAlive_11; }
	inline bool* get_address_of__keepAlive_11() { return &____keepAlive_11; }
	inline void set__keepAlive_11(bool value)
	{
		____keepAlive_11 = value;
	}

	inline static int32_t get_offset_of__location_12() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t1992878431, ____location_12)); }
	inline String_t* get__location_12() const { return ____location_12; }
	inline String_t** get_address_of__location_12() { return &____location_12; }
	inline void set__location_12(String_t* value)
	{
		____location_12 = value;
		Il2CppCodeGenWriteBarrier(&____location_12, value);
	}

	inline static int32_t get_offset_of__outputStream_13() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t1992878431, ____outputStream_13)); }
	inline ResponseStream_t1796293571 * get__outputStream_13() const { return ____outputStream_13; }
	inline ResponseStream_t1796293571 ** get_address_of__outputStream_13() { return &____outputStream_13; }
	inline void set__outputStream_13(ResponseStream_t1796293571 * value)
	{
		____outputStream_13 = value;
		Il2CppCodeGenWriteBarrier(&____outputStream_13, value);
	}

	inline static int32_t get_offset_of__statusCode_14() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t1992878431, ____statusCode_14)); }
	inline int32_t get__statusCode_14() const { return ____statusCode_14; }
	inline int32_t* get_address_of__statusCode_14() { return &____statusCode_14; }
	inline void set__statusCode_14(int32_t value)
	{
		____statusCode_14 = value;
	}

	inline static int32_t get_offset_of__statusDescription_15() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t1992878431, ____statusDescription_15)); }
	inline String_t* get__statusDescription_15() const { return ____statusDescription_15; }
	inline String_t** get_address_of__statusDescription_15() { return &____statusDescription_15; }
	inline void set__statusDescription_15(String_t* value)
	{
		____statusDescription_15 = value;
		Il2CppCodeGenWriteBarrier(&____statusDescription_15, value);
	}

	inline static int32_t get_offset_of__version_16() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t1992878431, ____version_16)); }
	inline Version_t763695022 * get__version_16() const { return ____version_16; }
	inline Version_t763695022 ** get_address_of__version_16() { return &____version_16; }
	inline void set__version_16(Version_t763695022 * value)
	{
		____version_16 = value;
		Il2CppCodeGenWriteBarrier(&____version_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
