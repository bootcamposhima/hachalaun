﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// FixedID
struct FixedID_t820738927;
struct FixedID_t820738927_marshaled_pinvoke;
struct FixedID_t820738927_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FixedID820738927.h"
#include "mscorlib_System_String7231557.h"

// System.Void FixedID::.ctor(System.String,System.String)
extern "C"  void FixedID__ctor_m1737331042 (FixedID_t820738927 * __this, String_t* ___i0, String_t* ___n1, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct FixedID_t820738927;
struct FixedID_t820738927_marshaled_pinvoke;

extern "C" void FixedID_t820738927_marshal_pinvoke(const FixedID_t820738927& unmarshaled, FixedID_t820738927_marshaled_pinvoke& marshaled);
extern "C" void FixedID_t820738927_marshal_pinvoke_back(const FixedID_t820738927_marshaled_pinvoke& marshaled, FixedID_t820738927& unmarshaled);
extern "C" void FixedID_t820738927_marshal_pinvoke_cleanup(FixedID_t820738927_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct FixedID_t820738927;
struct FixedID_t820738927_marshaled_com;

extern "C" void FixedID_t820738927_marshal_com(const FixedID_t820738927& unmarshaled, FixedID_t820738927_marshaled_com& marshaled);
extern "C" void FixedID_t820738927_marshal_com_back(const FixedID_t820738927_marshaled_com& marshaled, FixedID_t820738927& unmarshaled);
extern "C" void FixedID_t820738927_marshal_com_cleanup(FixedID_t820738927_marshaled_com& marshaled);
