﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.Net.ListenerPrefix
struct ListenerPrefix_t2663314696;
// System.String
struct String_t;
// System.Net.IPAddress[]
struct IPAddressU5BU5D_t1215594974;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void WebSocketSharp.Net.ListenerPrefix::.ctor(System.String)
extern "C"  void ListenerPrefix__ctor_m1715923577 (ListenerPrefix_t2663314696 * __this, String_t* ___uriPrefix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IPAddress[] WebSocketSharp.Net.ListenerPrefix::get_Addresses()
extern "C"  IPAddressU5BU5D_t1215594974* ListenerPrefix_get_Addresses_m4100199934 (ListenerPrefix_t2663314696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.ListenerPrefix::set_Addresses(System.Net.IPAddress[])
extern "C"  void ListenerPrefix_set_Addresses_m951742541 (ListenerPrefix_t2663314696 * __this, IPAddressU5BU5D_t1215594974* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.Net.ListenerPrefix::get_Host()
extern "C"  String_t* ListenerPrefix_get_Host_m2981656425 (ListenerPrefix_t2663314696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.Net.ListenerPrefix::get_Path()
extern "C"  String_t* ListenerPrefix_get_Path_m3197778598 (ListenerPrefix_t2663314696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 WebSocketSharp.Net.ListenerPrefix::get_Port()
extern "C"  int32_t ListenerPrefix_get_Port_m2433466871 (ListenerPrefix_t2663314696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.Net.ListenerPrefix::get_Secure()
extern "C"  bool ListenerPrefix_get_Secure_m4149539015 (ListenerPrefix_t2663314696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.ListenerPrefix::parse(System.String)
extern "C"  void ListenerPrefix_parse_m3351698152 (ListenerPrefix_t2663314696 * __this, String_t* ___uriPrefix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.ListenerPrefix::CheckUriPrefix(System.String)
extern "C"  void ListenerPrefix_CheckUriPrefix_m2837076145 (Il2CppObject * __this /* static, unused */, String_t* ___uriPrefix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.Net.ListenerPrefix::Equals(System.Object)
extern "C"  bool ListenerPrefix_Equals_m3907598 (ListenerPrefix_t2663314696 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 WebSocketSharp.Net.ListenerPrefix::GetHashCode()
extern "C"  int32_t ListenerPrefix_GetHashCode_m2076184358 (ListenerPrefix_t2663314696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.Net.ListenerPrefix::ToString()
extern "C"  String_t* ListenerPrefix_ToString_m2502033956 (ListenerPrefix_t2663314696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
