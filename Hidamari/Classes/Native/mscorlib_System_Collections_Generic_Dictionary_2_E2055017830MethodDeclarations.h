﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Char>
struct Dictionary_2_t737694438;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2055017830.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_636475144.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Char>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1734237432_gshared (Enumerator_t2055017830 * __this, Dictionary_2_t737694438 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m1734237432(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2055017830 *, Dictionary_2_t737694438 *, const MethodInfo*))Enumerator__ctor_m1734237432_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Char>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m4171576169_gshared (Enumerator_t2055017830 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m4171576169(__this, method) ((  Il2CppObject * (*) (Enumerator_t2055017830 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m4171576169_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Char>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3218868413_gshared (Enumerator_t2055017830 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3218868413(__this, method) ((  void (*) (Enumerator_t2055017830 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3218868413_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Char>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t1751606614  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m174895814_gshared (Enumerator_t2055017830 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m174895814(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t2055017830 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m174895814_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Char>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2259713413_gshared (Enumerator_t2055017830 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2259713413(__this, method) ((  Il2CppObject * (*) (Enumerator_t2055017830 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2259713413_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Char>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3674541783_gshared (Enumerator_t2055017830 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3674541783(__this, method) ((  Il2CppObject * (*) (Enumerator_t2055017830 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3674541783_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Char>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2277225129_gshared (Enumerator_t2055017830 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2277225129(__this, method) ((  bool (*) (Enumerator_t2055017830 *, const MethodInfo*))Enumerator_MoveNext_m2277225129_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Char>::get_Current()
extern "C"  KeyValuePair_2_t636475144  Enumerator_get_Current_m3086119271_gshared (Enumerator_t2055017830 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3086119271(__this, method) ((  KeyValuePair_2_t636475144  (*) (Enumerator_t2055017830 *, const MethodInfo*))Enumerator_get_Current_m3086119271_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Char>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m1875117110_gshared (Enumerator_t2055017830 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m1875117110(__this, method) ((  Il2CppObject * (*) (Enumerator_t2055017830 *, const MethodInfo*))Enumerator_get_CurrentKey_m1875117110_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Char>::get_CurrentValue()
extern "C"  Il2CppChar Enumerator_get_CurrentValue_m3733999578_gshared (Enumerator_t2055017830 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m3733999578(__this, method) ((  Il2CppChar (*) (Enumerator_t2055017830 *, const MethodInfo*))Enumerator_get_CurrentValue_m3733999578_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Char>::Reset()
extern "C"  void Enumerator_Reset_m3137012554_gshared (Enumerator_t2055017830 * __this, const MethodInfo* method);
#define Enumerator_Reset_m3137012554(__this, method) ((  void (*) (Enumerator_t2055017830 *, const MethodInfo*))Enumerator_Reset_m3137012554_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Char>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2181903315_gshared (Enumerator_t2055017830 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2181903315(__this, method) ((  void (*) (Enumerator_t2055017830 *, const MethodInfo*))Enumerator_VerifyState_m2181903315_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Char>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m3609917051_gshared (Enumerator_t2055017830 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m3609917051(__this, method) ((  void (*) (Enumerator_t2055017830 *, const MethodInfo*))Enumerator_VerifyCurrent_m3609917051_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Char>::Dispose()
extern "C"  void Enumerator_Dispose_m2125451674_gshared (Enumerator_t2055017830 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2125451674(__this, method) ((  void (*) (Enumerator_t2055017830 *, const MethodInfo*))Enumerator_Dispose_m2125451674_gshared)(__this, method)
