﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_WebSocketSharp_HandshakeBase1248407470.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.HandshakeResponse
struct  HandshakeResponse_t3229697822  : public HandshakeBase_t1248407470
{
public:
	// System.String WebSocketSharp.HandshakeResponse::_code
	String_t* ____code_4;
	// System.String WebSocketSharp.HandshakeResponse::_reason
	String_t* ____reason_5;

public:
	inline static int32_t get_offset_of__code_4() { return static_cast<int32_t>(offsetof(HandshakeResponse_t3229697822, ____code_4)); }
	inline String_t* get__code_4() const { return ____code_4; }
	inline String_t** get_address_of__code_4() { return &____code_4; }
	inline void set__code_4(String_t* value)
	{
		____code_4 = value;
		Il2CppCodeGenWriteBarrier(&____code_4, value);
	}

	inline static int32_t get_offset_of__reason_5() { return static_cast<int32_t>(offsetof(HandshakeResponse_t3229697822, ____reason_5)); }
	inline String_t* get__reason_5() const { return ____reason_5; }
	inline String_t** get_address_of__reason_5() { return &____reason_5; }
	inline void set__reason_5(String_t* value)
	{
		____reason_5 = value;
		Il2CppCodeGenWriteBarrier(&____reason_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
