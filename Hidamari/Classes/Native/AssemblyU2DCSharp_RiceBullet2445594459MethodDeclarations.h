﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RiceBullet
struct RiceBullet_t2445594459;

#include "codegen/il2cpp-codegen.h"

// System.Void RiceBullet::.ctor()
extern "C"  void RiceBullet__ctor_m4279951264 (RiceBullet_t2445594459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RiceBullet::Start()
extern "C"  void RiceBullet_Start_m3227089056 (RiceBullet_t2445594459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RiceBullet::Update()
extern "C"  void RiceBullet_Update_m1261365101 (RiceBullet_t2445594459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
