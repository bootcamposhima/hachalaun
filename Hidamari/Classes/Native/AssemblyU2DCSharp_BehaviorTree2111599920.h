﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<TaskOrder>
struct List_1_t3538225017;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BehaviorTree
struct  BehaviorTree_t2111599920  : public MonoBehaviour_t667441552
{
public:
	// System.Collections.Generic.List`1<TaskOrder> BehaviorTree::_taskorder
	List_1_t3538225017 * ____taskorder_2;
	// System.Collections.Generic.List`1<TaskOrder> BehaviorTree::_cleantaskorder
	List_1_t3538225017 * ____cleantaskorder_3;
	// System.Int32 BehaviorTree::_count
	int32_t ____count_4;

public:
	inline static int32_t get_offset_of__taskorder_2() { return static_cast<int32_t>(offsetof(BehaviorTree_t2111599920, ____taskorder_2)); }
	inline List_1_t3538225017 * get__taskorder_2() const { return ____taskorder_2; }
	inline List_1_t3538225017 ** get_address_of__taskorder_2() { return &____taskorder_2; }
	inline void set__taskorder_2(List_1_t3538225017 * value)
	{
		____taskorder_2 = value;
		Il2CppCodeGenWriteBarrier(&____taskorder_2, value);
	}

	inline static int32_t get_offset_of__cleantaskorder_3() { return static_cast<int32_t>(offsetof(BehaviorTree_t2111599920, ____cleantaskorder_3)); }
	inline List_1_t3538225017 * get__cleantaskorder_3() const { return ____cleantaskorder_3; }
	inline List_1_t3538225017 ** get_address_of__cleantaskorder_3() { return &____cleantaskorder_3; }
	inline void set__cleantaskorder_3(List_1_t3538225017 * value)
	{
		____cleantaskorder_3 = value;
		Il2CppCodeGenWriteBarrier(&____cleantaskorder_3, value);
	}

	inline static int32_t get_offset_of__count_4() { return static_cast<int32_t>(offsetof(BehaviorTree_t2111599920, ____count_4)); }
	inline int32_t get__count_4() const { return ____count_4; }
	inline int32_t* get_address_of__count_4() { return &____count_4; }
	inline void set__count_4(int32_t value)
	{
		____count_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
