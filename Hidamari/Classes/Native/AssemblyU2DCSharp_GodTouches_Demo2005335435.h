﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t1659122786;
// GodTouches.ClickLongPressDrag
struct ClickLongPressDrag_t90319163;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GodTouches.Demo
struct  Demo_t2005335435  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Transform GodTouches.Demo::Move
	Transform_t1659122786 * ___Move_2;
	// GodTouches.ClickLongPressDrag GodTouches.Demo::ClickLongPressDrag
	ClickLongPressDrag_t90319163 * ___ClickLongPressDrag_3;
	// UnityEngine.Vector3 GodTouches.Demo::startPos
	Vector3_t4282066566  ___startPos_4;

public:
	inline static int32_t get_offset_of_Move_2() { return static_cast<int32_t>(offsetof(Demo_t2005335435, ___Move_2)); }
	inline Transform_t1659122786 * get_Move_2() const { return ___Move_2; }
	inline Transform_t1659122786 ** get_address_of_Move_2() { return &___Move_2; }
	inline void set_Move_2(Transform_t1659122786 * value)
	{
		___Move_2 = value;
		Il2CppCodeGenWriteBarrier(&___Move_2, value);
	}

	inline static int32_t get_offset_of_ClickLongPressDrag_3() { return static_cast<int32_t>(offsetof(Demo_t2005335435, ___ClickLongPressDrag_3)); }
	inline ClickLongPressDrag_t90319163 * get_ClickLongPressDrag_3() const { return ___ClickLongPressDrag_3; }
	inline ClickLongPressDrag_t90319163 ** get_address_of_ClickLongPressDrag_3() { return &___ClickLongPressDrag_3; }
	inline void set_ClickLongPressDrag_3(ClickLongPressDrag_t90319163 * value)
	{
		___ClickLongPressDrag_3 = value;
		Il2CppCodeGenWriteBarrier(&___ClickLongPressDrag_3, value);
	}

	inline static int32_t get_offset_of_startPos_4() { return static_cast<int32_t>(offsetof(Demo_t2005335435, ___startPos_4)); }
	inline Vector3_t4282066566  get_startPos_4() const { return ___startPos_4; }
	inline Vector3_t4282066566 * get_address_of_startPos_4() { return &___startPos_4; }
	inline void set_startPos_4(Vector3_t4282066566  value)
	{
		___startPos_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
