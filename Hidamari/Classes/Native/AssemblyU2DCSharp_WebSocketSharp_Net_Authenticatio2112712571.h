﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`2<System.String,System.Boolean>
struct Func_2_t3730860138;

#include "AssemblyU2DCSharp_WebSocketSharp_Net_Authenticatio1650711563.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.AuthenticationResponse
struct  AuthenticationResponse_t2112712571  : public AuthenticationBase_t1650711563
{
public:
	// System.UInt32 WebSocketSharp.Net.AuthenticationResponse::_nonceCount
	uint32_t ____nonceCount_2;

public:
	inline static int32_t get_offset_of__nonceCount_2() { return static_cast<int32_t>(offsetof(AuthenticationResponse_t2112712571, ____nonceCount_2)); }
	inline uint32_t get__nonceCount_2() const { return ____nonceCount_2; }
	inline uint32_t* get_address_of__nonceCount_2() { return &____nonceCount_2; }
	inline void set__nonceCount_2(uint32_t value)
	{
		____nonceCount_2 = value;
	}
};

struct AuthenticationResponse_t2112712571_StaticFields
{
public:
	// System.Func`2<System.String,System.Boolean> WebSocketSharp.Net.AuthenticationResponse::<>f__am$cache1
	Func_2_t3730860138 * ___U3CU3Ef__amU24cache1_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_3() { return static_cast<int32_t>(offsetof(AuthenticationResponse_t2112712571_StaticFields, ___U3CU3Ef__amU24cache1_3)); }
	inline Func_2_t3730860138 * get_U3CU3Ef__amU24cache1_3() const { return ___U3CU3Ef__amU24cache1_3; }
	inline Func_2_t3730860138 ** get_address_of_U3CU3Ef__amU24cache1_3() { return &___U3CU3Ef__amU24cache1_3; }
	inline void set_U3CU3Ef__amU24cache1_3(Func_2_t3730860138 * value)
	{
		___U3CU3Ef__amU24cache1_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
