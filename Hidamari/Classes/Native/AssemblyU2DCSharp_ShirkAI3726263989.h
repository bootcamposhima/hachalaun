﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// BlackBoard
struct BlackBoard_t328561223;

#include "AssemblyU2DCSharp_Charctor1500651146.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShirkAI
struct  ShirkAI_t3726263989  : public Charctor_t1500651146
{
public:
	// System.Single ShirkAI::m_y
	float ___m_y_10;
	// System.Int32 ShirkAI::_movestate
	int32_t ____movestate_11;
	// System.Single ShirkAI::_anim
	float ____anim_12;
	// System.Boolean ShirkAI::isanim
	bool ___isanim_13;
	// UnityEngine.Color ShirkAI::_initcolor
	Color_t4194546905  ____initcolor_14;
	// System.Int32 ShirkAI::_hp
	int32_t ____hp_15;
	// System.Int32 ShirkAI::_angle
	int32_t ____angle_16;
	// UnityEngine.GameObject ShirkAI::_hpgauge
	GameObject_t3674682005 * ____hpgauge_17;
	// UnityEngine.GameObject ShirkAI::_hptext
	GameObject_t3674682005 * ____hptext_18;
	// BlackBoard ShirkAI::bb
	BlackBoard_t328561223 * ___bb_19;
	// System.Boolean ShirkAI::istitle
	bool ___istitle_20;

public:
	inline static int32_t get_offset_of_m_y_10() { return static_cast<int32_t>(offsetof(ShirkAI_t3726263989, ___m_y_10)); }
	inline float get_m_y_10() const { return ___m_y_10; }
	inline float* get_address_of_m_y_10() { return &___m_y_10; }
	inline void set_m_y_10(float value)
	{
		___m_y_10 = value;
	}

	inline static int32_t get_offset_of__movestate_11() { return static_cast<int32_t>(offsetof(ShirkAI_t3726263989, ____movestate_11)); }
	inline int32_t get__movestate_11() const { return ____movestate_11; }
	inline int32_t* get_address_of__movestate_11() { return &____movestate_11; }
	inline void set__movestate_11(int32_t value)
	{
		____movestate_11 = value;
	}

	inline static int32_t get_offset_of__anim_12() { return static_cast<int32_t>(offsetof(ShirkAI_t3726263989, ____anim_12)); }
	inline float get__anim_12() const { return ____anim_12; }
	inline float* get_address_of__anim_12() { return &____anim_12; }
	inline void set__anim_12(float value)
	{
		____anim_12 = value;
	}

	inline static int32_t get_offset_of_isanim_13() { return static_cast<int32_t>(offsetof(ShirkAI_t3726263989, ___isanim_13)); }
	inline bool get_isanim_13() const { return ___isanim_13; }
	inline bool* get_address_of_isanim_13() { return &___isanim_13; }
	inline void set_isanim_13(bool value)
	{
		___isanim_13 = value;
	}

	inline static int32_t get_offset_of__initcolor_14() { return static_cast<int32_t>(offsetof(ShirkAI_t3726263989, ____initcolor_14)); }
	inline Color_t4194546905  get__initcolor_14() const { return ____initcolor_14; }
	inline Color_t4194546905 * get_address_of__initcolor_14() { return &____initcolor_14; }
	inline void set__initcolor_14(Color_t4194546905  value)
	{
		____initcolor_14 = value;
	}

	inline static int32_t get_offset_of__hp_15() { return static_cast<int32_t>(offsetof(ShirkAI_t3726263989, ____hp_15)); }
	inline int32_t get__hp_15() const { return ____hp_15; }
	inline int32_t* get_address_of__hp_15() { return &____hp_15; }
	inline void set__hp_15(int32_t value)
	{
		____hp_15 = value;
	}

	inline static int32_t get_offset_of__angle_16() { return static_cast<int32_t>(offsetof(ShirkAI_t3726263989, ____angle_16)); }
	inline int32_t get__angle_16() const { return ____angle_16; }
	inline int32_t* get_address_of__angle_16() { return &____angle_16; }
	inline void set__angle_16(int32_t value)
	{
		____angle_16 = value;
	}

	inline static int32_t get_offset_of__hpgauge_17() { return static_cast<int32_t>(offsetof(ShirkAI_t3726263989, ____hpgauge_17)); }
	inline GameObject_t3674682005 * get__hpgauge_17() const { return ____hpgauge_17; }
	inline GameObject_t3674682005 ** get_address_of__hpgauge_17() { return &____hpgauge_17; }
	inline void set__hpgauge_17(GameObject_t3674682005 * value)
	{
		____hpgauge_17 = value;
		Il2CppCodeGenWriteBarrier(&____hpgauge_17, value);
	}

	inline static int32_t get_offset_of__hptext_18() { return static_cast<int32_t>(offsetof(ShirkAI_t3726263989, ____hptext_18)); }
	inline GameObject_t3674682005 * get__hptext_18() const { return ____hptext_18; }
	inline GameObject_t3674682005 ** get_address_of__hptext_18() { return &____hptext_18; }
	inline void set__hptext_18(GameObject_t3674682005 * value)
	{
		____hptext_18 = value;
		Il2CppCodeGenWriteBarrier(&____hptext_18, value);
	}

	inline static int32_t get_offset_of_bb_19() { return static_cast<int32_t>(offsetof(ShirkAI_t3726263989, ___bb_19)); }
	inline BlackBoard_t328561223 * get_bb_19() const { return ___bb_19; }
	inline BlackBoard_t328561223 ** get_address_of_bb_19() { return &___bb_19; }
	inline void set_bb_19(BlackBoard_t328561223 * value)
	{
		___bb_19 = value;
		Il2CppCodeGenWriteBarrier(&___bb_19, value);
	}

	inline static int32_t get_offset_of_istitle_20() { return static_cast<int32_t>(offsetof(ShirkAI_t3726263989, ___istitle_20)); }
	inline bool get_istitle_20() const { return ___istitle_20; }
	inline bool* get_address_of_istitle_20() { return &___istitle_20; }
	inline void set_istitle_20(bool value)
	{
		___istitle_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
