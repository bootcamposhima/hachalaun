﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_HPManager3198084485.h"
#include "AssemblyU2DCSharp_HPManager_U3CDamagetAnimationU3E1065517605.h"
#include "AssemblyU2DCSharp_HPManager_U3CDamagetAnimation2U3E529218454.h"
#include "AssemblyU2DCSharp_NormalRound1394582727.h"
#include "AssemblyU2DCSharp_Round79151470.h"
#include "AssemblyU2DCSharp_Round_U3CRoundTextAnimationU3Ec_2209827310.h"
#include "AssemblyU2DCSharp_StagePanelManager334945031.h"
#include "AssemblyU2DCSharp_StagePanelManager_U3CStagePanelL3565141640.h"
#include "AssemblyU2DCSharp_AIChoiceAction410758015.h"
#include "AssemblyU2DCSharp_AIChoiceManager1845660548.h"
#include "AssemblyU2DCSharp_AICreaterAction2388459908.h"
#include "AssemblyU2DCSharp_AICreaterManager3024877087.h"
#include "AssemblyU2DCSharp_AIManager3650593381.h"
#include "AssemblyU2DCSharp_CharManager988180311.h"
#include "AssemblyU2DCSharp_DirectionManager2568308558.h"
#include "AssemblyU2DCSharp_PracticeManager2478642642.h"
#include "AssemblyU2DCSharp_UIManager1861242489.h"
#include "AssemblyU2DCSharp_InterimResult3822800669.h"
#include "AssemblyU2DCSharp_Result2444407869.h"
#include "AssemblyU2DCSharp_ResultData1421128071.h"
#include "AssemblyU2DCSharp_ExplanationText3365505734.h"
#include "AssemblyU2DCSharp_PlaySelect3562688880.h"
#include "AssemblyU2DCSharp_PlaySelect_U3CShowStartColU3Ec__3835832655.h"
#include "AssemblyU2DCSharp_SCImage2637275371.h"
#include "AssemblyU2DCSharp_SCImage_U3CImagecolU3Ec__Iterato1482518325.h"
#include "AssemblyU2DCSharp_AndroidBlueToothAdapter3952648528.h"
#include "AssemblyU2DCSharp_Tag83834.h"
#include "AssemblyU2DCSharp_JSONObject1752376903.h"
#include "AssemblyU2DCSharp_JSONObject_Type997917218.h"
#include "AssemblyU2DCSharp_JSONObject_AddJSONConents3226131721.h"
#include "AssemblyU2DCSharp_JSONObject_FieldNotFound2426242465.h"
#include "AssemblyU2DCSharp_JSONObject_GetFieldResponse2312868493.h"
#include "AssemblyU2DCSharp_JSONObject_U3CBakeAsyncU3Ec__Iter926549320.h"
#include "AssemblyU2DCSharp_JSONObject_U3CPrintAsyncU3Ec__It4069916573.h"
#include "AssemblyU2DCSharp_JSONObject_U3CStringifyAsyncU3Ec3659654720.h"
#include "AssemblyU2DCSharp_JSONTemplates3204252465.h"
#include "AssemblyU2DCSharp_SocketIO_Ack4240269559.h"
#include "AssemblyU2DCSharp_SocketIO_Decoder2133550898.h"
#include "AssemblyU2DCSharp_SocketIO_Encoder3278716938.h"
#include "AssemblyU2DCSharp_SocketIO_EnginePacketType2017318204.h"
#include "AssemblyU2DCSharp_SocketIO_Packet1660110912.h"
#include "AssemblyU2DCSharp_SocketIO_Parser1660565463.h"
#include "AssemblyU2DCSharp_SocketIO_SocketIOComponent3095476818.h"
#include "AssemblyU2DCSharp_SocketIO_SocketIOEvent4011854063.h"
#include "AssemblyU2DCSharp_SocketIO_SocketIOException1682042276.h"
#include "AssemblyU2DCSharp_SocketIO_SocketPacketType4056543533.h"
#include "AssemblyU2DCSharp_WebSocketSharp_ByteOrder422296044.h"
#include "AssemblyU2DCSharp_WebSocketSharp_CloseEventArgs2470319931.h"
#include "AssemblyU2DCSharp_WebSocketSharp_CloseStatusCode3936110621.h"
#include "AssemblyU2DCSharp_WebSocketSharp_CompressionMethod2226596781.h"
#include "AssemblyU2DCSharp_WebSocketSharp_ErrorEventArgs424195371.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Ext3262160039.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Ext_U3CSplitHeade3349152944.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Ext_U3CContainsTw3499061934.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Ext_U3CReadBytesA3077023315.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Fin3262160529.h"
#include "AssemblyU2DCSharp_WebSocketSharp_HandshakeBase1248407470.h"
#include "AssemblyU2DCSharp_WebSocketSharp_HandshakeRequest1037477780.h"
#include "AssemblyU2DCSharp_WebSocketSharp_HandshakeResponse3229697822.h"
#include "AssemblyU2DCSharp_WebSocketSharp_LogData3968197908.h"
#include "AssemblyU2DCSharp_WebSocketSharp_LogLevel3842284188.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Logger3695440972.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Mask3422653544.h"
#include "AssemblyU2DCSharp_WebSocketSharp_MessageEventArgs36945740.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_Authenticatio1650711563.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_Authenticatio1782907061.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_Authenticatio2112712571.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_Authenticatio3190130368.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_Chunk1399190359.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_ChunkStream4213968439.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_ChunkedRequest849747493.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_Cookie2077085446.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_CookieCollect1136277956.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_CookieExceptio967178805.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_EndPointListe3188089579.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_EndPointManag3612541026.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_HttpBasicIden3688512078.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_HttpConnection602292776.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_HttpDigestIde2083794924.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_HttpHeaderInf3355144229.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_HttpHeaderTyp3355482801.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_HttpListener398944510.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_HttpListenerC3744659101.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_HttpListenerE2442008381.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_HttpListenerP1379363822.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_HttpListenerR3888821117.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_HttpListenerR1992878431.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_HttpListenerR3494975687.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_HttpListenerRe788655901.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_HttpStatusCod1625451593.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_HttpStreamAsyn303264603.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_HttpUtility1048309166.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_HttpVersion1514895642.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_InputChunkSta4273164664.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_InputState2025202057.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_LineState793115623.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_ListenerAsyncR609216079.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_ListenerPrefi2663314696.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_NetworkCreden1204099087.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1900 = { sizeof (HPManager_t3198084485), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1900[10] = 
{
	HPManager_t3198084485::get_offset_of__defaulthpnum_2(),
	HPManager_t3198084485::get_offset_of__hpnum_3(),
	HPManager_t3198084485::get_offset_of__hpgauge_4(),
	HPManager_t3198084485::get_offset_of__redhpgauge_5(),
	HPManager_t3198084485::get_offset_of__hphert_6(),
	HPManager_t3198084485::get_offset_of__redhphert_7(),
	HPManager_t3198084485::get_offset_of__hptext_8(),
	HPManager_t3198084485::get_offset_of__ie_9(),
	HPManager_t3198084485::get_offset_of__ie2_10(),
	HPManager_t3198084485::get_offset_of__bomPar_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1901 = { sizeof (U3CDamagetAnimationU3Ec__Iterator12_t1065517605), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1901[5] = 
{
	U3CDamagetAnimationU3Ec__Iterator12_t1065517605::get_offset_of_U3CsizeU3E__0_0(),
	U3CDamagetAnimationU3Ec__Iterator12_t1065517605::get_offset_of_U3CsU3E__1_1(),
	U3CDamagetAnimationU3Ec__Iterator12_t1065517605::get_offset_of_U24PC_2(),
	U3CDamagetAnimationU3Ec__Iterator12_t1065517605::get_offset_of_U24current_3(),
	U3CDamagetAnimationU3Ec__Iterator12_t1065517605::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1902 = { sizeof (U3CDamagetAnimation2U3Ec__Iterator13_t529218454), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1902[3] = 
{
	U3CDamagetAnimation2U3Ec__Iterator13_t529218454::get_offset_of_U24PC_0(),
	U3CDamagetAnimation2U3Ec__Iterator13_t529218454::get_offset_of_U24current_1(),
	U3CDamagetAnimation2U3Ec__Iterator13_t529218454::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1903 = { sizeof (NormalRound_t1394582727), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1903[7] = 
{
	NormalRound_t1394582727::get_offset_of_GameCount_11(),
	NormalRound_t1394582727::get_offset_of__SkipSpeed_12(),
	NormalRound_t1394582727::get_offset_of__ModeCanvas_13(),
	NormalRound_t1394582727::get_offset_of__Go_14(),
	NormalRound_t1394582727::get_offset_of_isNextGame_15(),
	NormalRound_t1394582727::get_offset_of_senkoHP_16(),
	NormalRound_t1394582727::get_offset_of_mode_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1904 = { sizeof (Round_t79151470), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1904[9] = 
{
	Round_t79151470::get_offset_of_NowRound_2(),
	Round_t79151470::get_offset_of__EnemyManager_3(),
	Round_t79151470::get_offset_of__DefenceManager_4(),
	Round_t79151470::get_offset_of__Result_5(),
	Round_t79151470::get_offset_of__HP_6(),
	Round_t79151470::get_offset_of__PlaySelect_7(),
	Round_t79151470::get_offset_of__RoundText_8(),
	Round_t79151470::get_offset_of__TernText_9(),
	Round_t79151470::get_offset_of_isAttack_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1905 = { sizeof (U3CRoundTextAnimationU3Ec__IteratorE_t2209827310), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1905[4] = 
{
	U3CRoundTextAnimationU3Ec__IteratorE_t2209827310::get_offset_of_U3CtimeU3E__0_0(),
	U3CRoundTextAnimationU3Ec__IteratorE_t2209827310::get_offset_of_U24PC_1(),
	U3CRoundTextAnimationU3Ec__IteratorE_t2209827310::get_offset_of_U24current_2(),
	U3CRoundTextAnimationU3Ec__IteratorE_t2209827310::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1906 = { sizeof (StagePanelManager_t334945031), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1906[4] = 
{
	StagePanelManager_t334945031::get_offset_of__stagepanel_2(),
	StagePanelManager_t334945031::get_offset_of__listobjects_3(),
	StagePanelManager_t334945031::get_offset_of__listienumerator_4(),
	StagePanelManager_t334945031::get_offset_of_isstop_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1907 = { sizeof (U3CStagePanelLightActionU3Ec__Iterator14_t3565141640), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1907[10] = 
{
	U3CStagePanelLightActionU3Ec__Iterator14_t3565141640::get_offset_of_ob_0(),
	U3CStagePanelLightActionU3Ec__Iterator14_t3565141640::get_offset_of_direction_1(),
	U3CStagePanelLightActionU3Ec__Iterator14_t3565141640::get_offset_of_position_2(),
	U3CStagePanelLightActionU3Ec__Iterator14_t3565141640::get_offset_of_time_3(),
	U3CStagePanelLightActionU3Ec__Iterator14_t3565141640::get_offset_of_U24PC_4(),
	U3CStagePanelLightActionU3Ec__Iterator14_t3565141640::get_offset_of_U24current_5(),
	U3CStagePanelLightActionU3Ec__Iterator14_t3565141640::get_offset_of_U3CU24U3Eob_6(),
	U3CStagePanelLightActionU3Ec__Iterator14_t3565141640::get_offset_of_U3CU24U3Edirection_7(),
	U3CStagePanelLightActionU3Ec__Iterator14_t3565141640::get_offset_of_U3CU24U3Eposition_8(),
	U3CStagePanelLightActionU3Ec__Iterator14_t3565141640::get_offset_of_U3CU24U3Etime_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1908 = { sizeof (AIChoiceAction_t410758015), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1908[8] = 
{
	AIChoiceAction_t410758015::get_offset_of__createAIbutton_2(),
	AIChoiceAction_t410758015::get_offset_of__editingbutton_3(),
	AIChoiceAction_t410758015::get_offset_of__east_4(),
	AIChoiceAction_t410758015::get_offset_of__weat_5(),
	AIChoiceAction_t410758015::get_offset_of__south_6(),
	AIChoiceAction_t410758015::get_offset_of__north_7(),
	AIChoiceAction_t410758015::get_offset_of__ta_8(),
	AIChoiceAction_t410758015::get_offset_of__topnum_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1909 = { sizeof (AIChoiceManager_t1845660548), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1909[11] = 
{
	AIChoiceManager_t1845660548::get_offset_of__scroll_2(),
	AIChoiceManager_t1845660548::get_offset_of__AIscroll_3(),
	AIChoiceManager_t1845660548::get_offset_of__okbutton_4(),
	AIChoiceManager_t1845660548::get_offset_of__backbutton_5(),
	AIChoiceManager_t1845660548::get_offset_of__directionobject_6(),
	AIChoiceManager_t1845660548::get_offset_of__aichoiceaction_7(),
	AIChoiceManager_t1845660548::get_offset_of__list_8(),
	AIChoiceManager_t1845660548::get_offset_of_fc_9(),
	AIChoiceManager_t1845660548::get_offset_of__direction_10(),
	AIChoiceManager_t1845660548::get_offset_of__node_11(),
	AIChoiceManager_t1845660548::get_offset_of__top_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1910 = { sizeof (AICreaterAction_t2388459908), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1910[4] = 
{
	AICreaterAction_t2388459908::get_offset_of__formationButton_2(),
	AICreaterAction_t2388459908::get_offset_of__arrows_3(),
	AICreaterAction_t2388459908::get_offset_of__ta_4(),
	AICreaterAction_t2388459908::get_offset_of__am_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1911 = { sizeof (AICreaterManager_t3024877087), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1911[24] = 
{
	AICreaterManager_t3024877087::get_offset_of__aidatas_2(),
	AICreaterManager_t3024877087::get_offset_of__aiscroll_3(),
	AICreaterManager_t3024877087::get_offset_of__bulletscroll_4(),
	AICreaterManager_t3024877087::get_offset_of__charactorscroll_5(),
	AICreaterManager_t3024877087::get_offset_of__addbutton_6(),
	AICreaterManager_t3024877087::get_offset_of__panels_7(),
	AICreaterManager_t3024877087::get_offset_of__arrows_8(),
	AICreaterManager_t3024877087::get_offset_of__maincamera_9(),
	AICreaterManager_t3024877087::get_offset_of__directionobject_10(),
	AICreaterManager_t3024877087::get_offset_of__node_11(),
	AICreaterManager_t3024877087::get_offset_of__aicreateaction_12(),
	AICreaterManager_t3024877087::get_offset_of__direction_13(),
	AICreaterManager_t3024877087::get_offset_of__charactornum_14(),
	AICreaterManager_t3024877087::get_offset_of__fc_15(),
	AICreaterManager_t3024877087::get_offset_of__topainum_16(),
	AICreaterManager_t3024877087::get_offset_of__topbulletnum_17(),
	AICreaterManager_t3024877087::get_offset_of__toppositionnum_18(),
	AICreaterManager_t3024877087::get_offset_of__formation_19(),
	AICreaterManager_t3024877087::get_offset_of__count_20(),
	AICreaterManager_t3024877087::get_offset_of__istop_21(),
	AICreaterManager_t3024877087::get_offset_of__north_22(),
	AICreaterManager_t3024877087::get_offset_of__south_23(),
	AICreaterManager_t3024877087::get_offset_of__east_24(),
	AICreaterManager_t3024877087::get_offset_of__west_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1912 = { sizeof (AIManager_t3650593381), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1912[12] = 
{
	AIManager_t3650593381::get_offset_of__button_7(),
	AIManager_t3650593381::get_offset_of__scroll_8(),
	AIManager_t3650593381::get_offset_of__scrollbullet_9(),
	AIManager_t3650593381::get_offset_of__scrollfirstpos_10(),
	AIManager_t3650593381::get_offset_of__scrollsecpundpos_11(),
	AIManager_t3650593381::get_offset_of__scrolldestination_12(),
	AIManager_t3650593381::get_offset_of__moveobjrct_13(),
	AIManager_t3650593381::get_offset_of__scrollspd_14(),
	AIManager_t3650593381::get_offset_of__scrollismove_15(),
	AIManager_t3650593381::get_offset_of_istop_16(),
	AIManager_t3650593381::get_offset_of_isre_17(),
	AIManager_t3650593381::get_offset_of_ismsb_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1913 = { sizeof (CharManager_t988180311), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1913[9] = 
{
	CharManager_t988180311::get_offset_of__button_7(),
	CharManager_t988180311::get_offset_of__scroll_8(),
	CharManager_t988180311::get_offset_of__content_9(),
	CharManager_t988180311::get_offset_of__scrollfirstpos_10(),
	CharManager_t988180311::get_offset_of__scrollsecpundpos_11(),
	CharManager_t988180311::get_offset_of__scrolldestination_12(),
	CharManager_t988180311::get_offset_of__scrollspd_13(),
	CharManager_t988180311::get_offset_of__scrollismove_14(),
	CharManager_t988180311::get_offset_of_istop_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1914 = { sizeof (DirectionManager_t2568308558), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1914[5] = 
{
	DirectionManager_t2568308558::get_offset_of__thirdbuttonpos_7(),
	DirectionManager_t2568308558::get_offset_of__rotate_8(),
	DirectionManager_t2568308558::get_offset_of__rotatespd_9(),
	DirectionManager_t2568308558::get_offset_of__DirectionStageCanvas_10(),
	DirectionManager_t2568308558::get_offset_of__DirectionImage_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1915 = { sizeof (PracticeManager_t2478642642), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1915[4] = 
{
	PracticeManager_t2478642642::get_offset_of__list_2(),
	PracticeManager_t2478642642::get_offset_of__uimaincamera_3(),
	PracticeManager_t2478642642::get_offset_of_fs_4(),
	PracticeManager_t2478642642::get_offset_of__top_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1916 = { sizeof (UIManager_t1861242489), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1916[5] = 
{
	UIManager_t1861242489::get_offset_of__firstposbuttonpos_2(),
	UIManager_t1861242489::get_offset_of__secoundbuttonpos_3(),
	UIManager_t1861242489::get_offset_of__destination_4(),
	UIManager_t1861242489::get_offset_of__ismove_5(),
	UIManager_t1861242489::get_offset_of__time_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1917 = { sizeof (InterimResult_t3822800669), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1917[7] = 
{
	InterimResult_t3822800669::get_offset_of_RoundText_2(),
	InterimResult_t3822800669::get_offset_of_player1_3(),
	InterimResult_t3822800669::get_offset_of_player2_4(),
	InterimResult_t3822800669::get_offset_of_UIcamera_5(),
	InterimResult_t3822800669::get_offset_of_Resultcamera_6(),
	InterimResult_t3822800669::get_offset_of_isWin_7(),
	InterimResult_t3822800669::get_offset_of_nowRound_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1918 = { sizeof (Result_t2444407869), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1918[11] = 
{
	Result_t2444407869::get_offset_of_player1_2(),
	Result_t2444407869::get_offset_of_player2_3(),
	Result_t2444407869::get_offset_of_roundCamera_4(),
	Result_t2444407869::get_offset_of_canvas_5(),
	Result_t2444407869::get_offset_of_i_result_6(),
	Result_t2444407869::get_offset_of_sp_result_7(),
	Result_t2444407869::get_offset_of__player1Win_8(),
	Result_t2444407869::get_offset_of__player2Win_9(),
	Result_t2444407869::get_offset_of__player1HP_10(),
	Result_t2444407869::get_offset_of__player2HP_11(),
	Result_t2444407869::get_offset_of_isFight_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1919 = { sizeof (ResultData_t1421128071), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1919[5] = 
{
	ResultData_t1421128071::get_offset_of_Player1HP_2(),
	ResultData_t1421128071::get_offset_of_Player2HP_3(),
	ResultData_t1421128071::get_offset_of_roundResult_4(),
	ResultData_t1421128071::get_offset_of_isretire_5(),
	ResultData_t1421128071::get_offset_of_isdisconnection_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1920 = { sizeof (ExplanationText_t3365505734), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1920[3] = 
{
	ExplanationText_t3365505734::get_offset_of_selectButton_2(),
	ExplanationText_t3365505734::get_offset_of__Go_3(),
	ExplanationText_t3365505734::get_offset_of__SCimage_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1921 = { sizeof (PlaySelect_t3562688880), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1921[30] = 
{
	PlaySelect_t3562688880::get_offset_of__SelectUICamera_2(),
	PlaySelect_t3562688880::get_offset_of__MainCamera_3(),
	PlaySelect_t3562688880::get_offset_of__UICamera_4(),
	PlaySelect_t3562688880::get_offset_of__EnemyManager_5(),
	PlaySelect_t3562688880::get_offset_of__DefenceManager_6(),
	PlaySelect_t3562688880::get_offset_of__round_7(),
	PlaySelect_t3562688880::get_offset_of__StartText_8(),
	PlaySelect_t3562688880::get_offset_of__SkipSpeed_9(),
	PlaySelect_t3562688880::get_offset_of__NotSelectImage_10(),
	PlaySelect_t3562688880::get_offset_of__AIScroll_11(),
	PlaySelect_t3562688880::get_offset_of__Go_12(),
	PlaySelect_t3562688880::get_offset_of_waku1_13(),
	PlaySelect_t3562688880::get_offset_of_waku2_14(),
	PlaySelect_t3562688880::get_offset_of__demoobject_15(),
	PlaySelect_t3562688880::get_offset_of_list_16(),
	PlaySelect_t3562688880::get_offset_of__ai_17(),
	PlaySelect_t3562688880::get_offset_of__pl_18(),
	PlaySelect_t3562688880::get_offset_of__isAttack_19(),
	PlaySelect_t3562688880::get_offset_of__isFight_20(),
	PlaySelect_t3562688880::get_offset_of__isStartOK_21(),
	PlaySelect_t3562688880::get_offset_of__isStart_22(),
	PlaySelect_t3562688880::get_offset_of__isChangeGameStart_23(),
	PlaySelect_t3562688880::get_offset_of__isChangeShow_24(),
	PlaySelect_t3562688880::get_offset_of_rectX_s_25(),
	PlaySelect_t3562688880::get_offset_of_rectX_m_26(),
	PlaySelect_t3562688880::get_offset_of_s_rectX_m_27(),
	PlaySelect_t3562688880::get_offset_of_s_rectX_s_28(),
	PlaySelect_t3562688880::get_offset_of_s_view_m_29(),
	PlaySelect_t3562688880::get_offset_of__isFarst_30(),
	PlaySelect_t3562688880::get_offset_of_ie_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1922 = { sizeof (U3CShowStartColU3Ec__Iterator15_t3835832655), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1922[4] = 
{
	U3CShowStartColU3Ec__Iterator15_t3835832655::get_offset_of_U3CtimeU3E__0_0(),
	U3CShowStartColU3Ec__Iterator15_t3835832655::get_offset_of_U24PC_1(),
	U3CShowStartColU3Ec__Iterator15_t3835832655::get_offset_of_U24current_2(),
	U3CShowStartColU3Ec__Iterator15_t3835832655::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1923 = { sizeof (SCImage_t2637275371), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1923[3] = 
{
	SCImage_t2637275371::get_offset_of__Normal_2(),
	SCImage_t2637275371::get_offset_of__Fight_3(),
	SCImage_t2637275371::get_offset_of__Edit_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1924 = { sizeof (U3CImagecolU3Ec__Iterator16_t1482518325), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1924[9] = 
{
	U3CImagecolU3Ec__Iterator16_t1482518325::get_offset_of_U3CtimeU3E__0_0(),
	U3CImagecolU3Ec__Iterator16_t1482518325::get_offset_of_U3CaU3E__1_1(),
	U3CImagecolU3Ec__Iterator16_t1482518325::get_offset_of_U3CimU3E__2_2(),
	U3CImagecolU3Ec__Iterator16_t1482518325::get_offset_of_U3CiU3E__3_3(),
	U3CImagecolU3Ec__Iterator16_t1482518325::get_offset_of_image_4(),
	U3CImagecolU3Ec__Iterator16_t1482518325::get_offset_of_U24PC_5(),
	U3CImagecolU3Ec__Iterator16_t1482518325::get_offset_of_U24current_6(),
	U3CImagecolU3Ec__Iterator16_t1482518325::get_offset_of_U3CU24U3Eimage_7(),
	U3CImagecolU3Ec__Iterator16_t1482518325::get_offset_of_U3CU3Ef__this_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1925 = { sizeof (AndroidBlueToothAdapter_t3952648528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1925[11] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	AndroidBlueToothAdapter_t3952648528::get_offset_of_cls_10(),
	AndroidBlueToothAdapter_t3952648528::get_offset_of_serverFlag_11(),
	AndroidBlueToothAdapter_t3952648528::get_offset_of_retryRequest_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1926 = { sizeof (Tag_t83834)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1926[2] = 
{
	Tag_t83834::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1927 = { sizeof (JSONObject_t1752376903), -1, sizeof(JSONObject_t1752376903_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1927[13] = 
{
	0,
	0,
	0,
	0,
	0,
	JSONObject_t1752376903_StaticFields::get_offset_of_WHITESPACE_5(),
	JSONObject_t1752376903::get_offset_of_type_6(),
	JSONObject_t1752376903::get_offset_of_list_7(),
	JSONObject_t1752376903::get_offset_of_keys_8(),
	JSONObject_t1752376903::get_offset_of_str_9(),
	JSONObject_t1752376903::get_offset_of_n_10(),
	JSONObject_t1752376903::get_offset_of_b_11(),
	JSONObject_t1752376903_StaticFields::get_offset_of_printWatch_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1928 = { sizeof (Type_t997917218)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1928[8] = 
{
	Type_t997917218::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1929 = { sizeof (AddJSONConents_t3226131721), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1930 = { sizeof (FieldNotFound_t2426242465), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1931 = { sizeof (GetFieldResponse_t2312868493), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1932 = { sizeof (U3CBakeAsyncU3Ec__Iterator17_t926549320), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1932[5] = 
{
	U3CBakeAsyncU3Ec__Iterator17_t926549320::get_offset_of_U3CU24s_12U3E__0_0(),
	U3CBakeAsyncU3Ec__Iterator17_t926549320::get_offset_of_U3CsU3E__1_1(),
	U3CBakeAsyncU3Ec__Iterator17_t926549320::get_offset_of_U24PC_2(),
	U3CBakeAsyncU3Ec__Iterator17_t926549320::get_offset_of_U24current_3(),
	U3CBakeAsyncU3Ec__Iterator17_t926549320::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1933 = { sizeof (U3CPrintAsyncU3Ec__Iterator18_t4069916573), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1933[8] = 
{
	U3CPrintAsyncU3Ec__Iterator18_t4069916573::get_offset_of_U3CbuilderU3E__0_0(),
	U3CPrintAsyncU3Ec__Iterator18_t4069916573::get_offset_of_pretty_1(),
	U3CPrintAsyncU3Ec__Iterator18_t4069916573::get_offset_of_U3CU24s_13U3E__1_2(),
	U3CPrintAsyncU3Ec__Iterator18_t4069916573::get_offset_of_U3CeU3E__2_3(),
	U3CPrintAsyncU3Ec__Iterator18_t4069916573::get_offset_of_U24PC_4(),
	U3CPrintAsyncU3Ec__Iterator18_t4069916573::get_offset_of_U24current_5(),
	U3CPrintAsyncU3Ec__Iterator18_t4069916573::get_offset_of_U3CU24U3Epretty_6(),
	U3CPrintAsyncU3Ec__Iterator18_t4069916573::get_offset_of_U3CU3Ef__this_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1934 = { sizeof (U3CStringifyAsyncU3Ec__Iterator19_t3659654720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1934[21] = 
{
	U3CStringifyAsyncU3Ec__Iterator19_t3659654720::get_offset_of_depth_0(),
	U3CStringifyAsyncU3Ec__Iterator19_t3659654720::get_offset_of_builder_1(),
	U3CStringifyAsyncU3Ec__Iterator19_t3659654720::get_offset_of_pretty_2(),
	U3CStringifyAsyncU3Ec__Iterator19_t3659654720::get_offset_of_U3CiU3E__0_3(),
	U3CStringifyAsyncU3Ec__Iterator19_t3659654720::get_offset_of_U3CkeyU3E__1_4(),
	U3CStringifyAsyncU3Ec__Iterator19_t3659654720::get_offset_of_U3CobjU3E__2_5(),
	U3CStringifyAsyncU3Ec__Iterator19_t3659654720::get_offset_of_U3CjU3E__3_6(),
	U3CStringifyAsyncU3Ec__Iterator19_t3659654720::get_offset_of_U3CU24s_14U3E__4_7(),
	U3CStringifyAsyncU3Ec__Iterator19_t3659654720::get_offset_of_U3CeU3E__5_8(),
	U3CStringifyAsyncU3Ec__Iterator19_t3659654720::get_offset_of_U3CjU3E__6_9(),
	U3CStringifyAsyncU3Ec__Iterator19_t3659654720::get_offset_of_U3CiU3E__7_10(),
	U3CStringifyAsyncU3Ec__Iterator19_t3659654720::get_offset_of_U3CjU3E__8_11(),
	U3CStringifyAsyncU3Ec__Iterator19_t3659654720::get_offset_of_U3CU24s_15U3E__9_12(),
	U3CStringifyAsyncU3Ec__Iterator19_t3659654720::get_offset_of_U3CeU3E__10_13(),
	U3CStringifyAsyncU3Ec__Iterator19_t3659654720::get_offset_of_U3CjU3E__11_14(),
	U3CStringifyAsyncU3Ec__Iterator19_t3659654720::get_offset_of_U24PC_15(),
	U3CStringifyAsyncU3Ec__Iterator19_t3659654720::get_offset_of_U24current_16(),
	U3CStringifyAsyncU3Ec__Iterator19_t3659654720::get_offset_of_U3CU24U3Edepth_17(),
	U3CStringifyAsyncU3Ec__Iterator19_t3659654720::get_offset_of_U3CU24U3Ebuilder_18(),
	U3CStringifyAsyncU3Ec__Iterator19_t3659654720::get_offset_of_U3CU24U3Epretty_19(),
	U3CStringifyAsyncU3Ec__Iterator19_t3659654720::get_offset_of_U3CU3Ef__this_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1935 = { sizeof (JSONTemplates_t3204252465), -1, sizeof(JSONTemplates_t3204252465_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1935[4] = 
{
	JSONTemplates_t3204252465_StaticFields::get_offset_of_touched_0(),
	JSONTemplates_t3204252465_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_1(),
	JSONTemplates_t3204252465_StaticFields::get_offset_of_U3CU3Ef__switchU24map1_2(),
	JSONTemplates_t3204252465_StaticFields::get_offset_of_U3CU3Ef__switchU24map2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1936 = { sizeof (Ack_t4240269559), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1936[3] = 
{
	Ack_t4240269559::get_offset_of_packetId_0(),
	Ack_t4240269559::get_offset_of_time_1(),
	Ack_t4240269559::get_offset_of_action_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1937 = { sizeof (Decoder_t2133550898), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1938 = { sizeof (Encoder_t3278716938), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1939 = { sizeof (EnginePacketType_t2017318204)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1939[9] = 
{
	EnginePacketType_t2017318204::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1940 = { sizeof (Packet_t1660110912), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1940[6] = 
{
	Packet_t1660110912::get_offset_of_enginePacketType_0(),
	Packet_t1660110912::get_offset_of_socketPacketType_1(),
	Packet_t1660110912::get_offset_of_attachments_2(),
	Packet_t1660110912::get_offset_of_nsp_3(),
	Packet_t1660110912::get_offset_of_id_4(),
	Packet_t1660110912::get_offset_of_json_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1941 = { sizeof (Parser_t1660565463), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1942 = { sizeof (SocketIOComponent_t3095476818), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1942[24] = 
{
	SocketIOComponent_t3095476818::get_offset_of_url_3(),
	SocketIOComponent_t3095476818::get_offset_of_autoConnect_4(),
	SocketIOComponent_t3095476818::get_offset_of_reconnectDelay_5(),
	SocketIOComponent_t3095476818::get_offset_of_ackExpirationTime_6(),
	SocketIOComponent_t3095476818::get_offset_of_pingInterval_7(),
	SocketIOComponent_t3095476818::get_offset_of_pingTimeout_8(),
	SocketIOComponent_t3095476818::get_offset_of_connected_9(),
	SocketIOComponent_t3095476818::get_offset_of_thPinging_10(),
	SocketIOComponent_t3095476818::get_offset_of_thPong_11(),
	SocketIOComponent_t3095476818::get_offset_of_wsConnected_12(),
	SocketIOComponent_t3095476818::get_offset_of_socketThread_13(),
	SocketIOComponent_t3095476818::get_offset_of_pingThread_14(),
	SocketIOComponent_t3095476818::get_offset_of_ws_15(),
	SocketIOComponent_t3095476818::get_offset_of_encoder_16(),
	SocketIOComponent_t3095476818::get_offset_of_decoder_17(),
	SocketIOComponent_t3095476818::get_offset_of_parser_18(),
	SocketIOComponent_t3095476818::get_offset_of_handlers_19(),
	SocketIOComponent_t3095476818::get_offset_of_ackList_20(),
	SocketIOComponent_t3095476818::get_offset_of_packetId_21(),
	SocketIOComponent_t3095476818::get_offset_of_eventQueueLock_22(),
	SocketIOComponent_t3095476818::get_offset_of_eventQueue_23(),
	SocketIOComponent_t3095476818::get_offset_of_ackQueueLock_24(),
	SocketIOComponent_t3095476818::get_offset_of_ackQueue_25(),
	SocketIOComponent_t3095476818::get_offset_of_U3CsidU3Ek__BackingField_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1943 = { sizeof (SocketIOEvent_t4011854063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1943[2] = 
{
	SocketIOEvent_t4011854063::get_offset_of_U3CnameU3Ek__BackingField_0(),
	SocketIOEvent_t4011854063::get_offset_of_U3CdataU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1944 = { sizeof (SocketIOException_t1682042276), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1945 = { sizeof (SocketPacketType_t4056543533)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1945[10] = 
{
	SocketPacketType_t4056543533::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1946 = { sizeof (ByteOrder_t422296044)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1946[3] = 
{
	ByteOrder_t422296044::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1947 = { sizeof (CloseEventArgs_t2470319931), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1947[3] = 
{
	CloseEventArgs_t2470319931::get_offset_of__clean_1(),
	CloseEventArgs_t2470319931::get_offset_of__code_2(),
	CloseEventArgs_t2470319931::get_offset_of__reason_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1948 = { sizeof (CloseStatusCode_t3936110621)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1948[14] = 
{
	CloseStatusCode_t3936110621::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1949 = { sizeof (CompressionMethod_t2226596781)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1949[3] = 
{
	CompressionMethod_t2226596781::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1950 = { sizeof (ErrorEventArgs_t424195371), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1950[1] = 
{
	ErrorEventArgs_t424195371::get_offset_of__message_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1951 = { sizeof (Ext_t3262160039), -1, sizeof(Ext_t3262160039_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1951[2] = 
{
	0,
	Ext_t3262160039_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1952 = { sizeof (U3CSplitHeaderValueU3Ec__Iterator1A_t3349152944), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1952[13] = 
{
	U3CSplitHeaderValueU3Ec__Iterator1A_t3349152944::get_offset_of_value_0(),
	U3CSplitHeaderValueU3Ec__Iterator1A_t3349152944::get_offset_of_U3ClenU3E__0_1(),
	U3CSplitHeaderValueU3Ec__Iterator1A_t3349152944::get_offset_of_separator_2(),
	U3CSplitHeaderValueU3Ec__Iterator1A_t3349152944::get_offset_of_U3CseparatorsU3E__1_3(),
	U3CSplitHeaderValueU3Ec__Iterator1A_t3349152944::get_offset_of_U3CbufferU3E__2_4(),
	U3CSplitHeaderValueU3Ec__Iterator1A_t3349152944::get_offset_of_U3CquotedU3E__3_5(),
	U3CSplitHeaderValueU3Ec__Iterator1A_t3349152944::get_offset_of_U3CescapedU3E__4_6(),
	U3CSplitHeaderValueU3Ec__Iterator1A_t3349152944::get_offset_of_U3CiU3E__5_7(),
	U3CSplitHeaderValueU3Ec__Iterator1A_t3349152944::get_offset_of_U3CcU3E__6_8(),
	U3CSplitHeaderValueU3Ec__Iterator1A_t3349152944::get_offset_of_U24PC_9(),
	U3CSplitHeaderValueU3Ec__Iterator1A_t3349152944::get_offset_of_U24current_10(),
	U3CSplitHeaderValueU3Ec__Iterator1A_t3349152944::get_offset_of_U3CU24U3Evalue_11(),
	U3CSplitHeaderValueU3Ec__Iterator1A_t3349152944::get_offset_of_U3CU24U3Eseparator_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1953 = { sizeof (U3CContainsTwiceU3Ec__AnonStorey21_t3499061934), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1953[3] = 
{
	U3CContainsTwiceU3Ec__AnonStorey21_t3499061934::get_offset_of_len_0(),
	U3CContainsTwiceU3Ec__AnonStorey21_t3499061934::get_offset_of_values_1(),
	U3CContainsTwiceU3Ec__AnonStorey21_t3499061934::get_offset_of_contains_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1954 = { sizeof (U3CReadBytesAsyncU3Ec__AnonStorey22_t3077023315), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1954[5] = 
{
	U3CReadBytesAsyncU3Ec__AnonStorey22_t3077023315::get_offset_of_stream_0(),
	U3CReadBytesAsyncU3Ec__AnonStorey22_t3077023315::get_offset_of_length_1(),
	U3CReadBytesAsyncU3Ec__AnonStorey22_t3077023315::get_offset_of_buffer_2(),
	U3CReadBytesAsyncU3Ec__AnonStorey22_t3077023315::get_offset_of_completed_3(),
	U3CReadBytesAsyncU3Ec__AnonStorey22_t3077023315::get_offset_of_error_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1955 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1955[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1956 = { sizeof (Fin_t3262160529)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1956[3] = 
{
	Fin_t3262160529::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1957 = { sizeof (HandshakeBase_t1248407470), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1957[4] = 
{
	0,
	HandshakeBase_t1248407470::get_offset_of__headers_1(),
	HandshakeBase_t1248407470::get_offset_of__version_2(),
	HandshakeBase_t1248407470::get_offset_of_EntityBodyData_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1958 = { sizeof (HandshakeRequest_t1037477780), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1958[4] = 
{
	HandshakeRequest_t1037477780::get_offset_of__method_4(),
	HandshakeRequest_t1037477780::get_offset_of__uri_5(),
	HandshakeRequest_t1037477780::get_offset_of__websocketRequest_6(),
	HandshakeRequest_t1037477780::get_offset_of__websocketRequestWasSet_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1959 = { sizeof (HandshakeResponse_t3229697822), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1959[2] = 
{
	HandshakeResponse_t3229697822::get_offset_of__code_4(),
	HandshakeResponse_t3229697822::get_offset_of__reason_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1960 = { sizeof (LogData_t3968197908), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1960[4] = 
{
	LogData_t3968197908::get_offset_of__caller_0(),
	LogData_t3968197908::get_offset_of__date_1(),
	LogData_t3968197908::get_offset_of__level_2(),
	LogData_t3968197908::get_offset_of__message_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1961 = { sizeof (LogLevel_t3842284188)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1961[7] = 
{
	LogLevel_t3842284188::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1962 = { sizeof (Logger_t3695440972), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1962[4] = 
{
	Logger_t3695440972::get_offset_of__file_0(),
	Logger_t3695440972::get_offset_of__level_1(),
	Logger_t3695440972::get_offset_of__output_2(),
	Logger_t3695440972::get_offset_of__sync_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1963 = { sizeof (Mask_t3422653544)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1963[3] = 
{
	Mask_t3422653544::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1964 = { sizeof (MessageEventArgs_t36945740), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1964[3] = 
{
	MessageEventArgs_t36945740::get_offset_of__data_1(),
	MessageEventArgs_t36945740::get_offset_of__opcode_2(),
	MessageEventArgs_t36945740::get_offset_of__rawData_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1965 = { sizeof (AuthenticationBase_t1650711563), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1965[2] = 
{
	AuthenticationBase_t1650711563::get_offset_of__scheme_0(),
	AuthenticationBase_t1650711563::get_offset_of_Parameters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1966 = { sizeof (AuthenticationChallenge_t1782907061), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1967 = { sizeof (AuthenticationResponse_t2112712571), -1, sizeof(AuthenticationResponse_t2112712571_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1967[2] = 
{
	AuthenticationResponse_t2112712571::get_offset_of__nonceCount_2(),
	AuthenticationResponse_t2112712571_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1968 = { sizeof (AuthenticationSchemes_t3190130368)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1968[5] = 
{
	AuthenticationSchemes_t3190130368::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1969 = { sizeof (Chunk_t1399190359), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1969[2] = 
{
	Chunk_t1399190359::get_offset_of__data_0(),
	Chunk_t1399190359::get_offset_of__offset_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1970 = { sizeof (ChunkStream_t4213968439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1970[9] = 
{
	ChunkStream_t4213968439::get_offset_of__chunkRead_0(),
	ChunkStream_t4213968439::get_offset_of__chunkSize_1(),
	ChunkStream_t4213968439::get_offset_of__chunks_2(),
	ChunkStream_t4213968439::get_offset_of__gotit_3(),
	ChunkStream_t4213968439::get_offset_of__headers_4(),
	ChunkStream_t4213968439::get_offset_of__saved_5(),
	ChunkStream_t4213968439::get_offset_of__sawCR_6(),
	ChunkStream_t4213968439::get_offset_of__state_7(),
	ChunkStream_t4213968439::get_offset_of__trailerState_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1971 = { sizeof (ChunkedRequestStream_t849747493), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1971[5] = 
{
	0,
	ChunkedRequestStream_t849747493::get_offset_of__context_8(),
	ChunkedRequestStream_t849747493::get_offset_of__decoder_9(),
	ChunkedRequestStream_t849747493::get_offset_of__disposed_10(),
	ChunkedRequestStream_t849747493::get_offset_of__noMoreData_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1972 = { sizeof (Cookie_t2077085446), -1, sizeof(Cookie_t2077085446_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1972[17] = 
{
	Cookie_t2077085446_StaticFields::get_offset_of__reservedCharsForName_0(),
	Cookie_t2077085446_StaticFields::get_offset_of__reservedCharsForValue_1(),
	Cookie_t2077085446::get_offset_of__comment_2(),
	Cookie_t2077085446::get_offset_of__commentUri_3(),
	Cookie_t2077085446::get_offset_of__discard_4(),
	Cookie_t2077085446::get_offset_of__domain_5(),
	Cookie_t2077085446::get_offset_of__expires_6(),
	Cookie_t2077085446::get_offset_of__httpOnly_7(),
	Cookie_t2077085446::get_offset_of__name_8(),
	Cookie_t2077085446::get_offset_of__path_9(),
	Cookie_t2077085446::get_offset_of__port_10(),
	Cookie_t2077085446::get_offset_of__ports_11(),
	Cookie_t2077085446::get_offset_of__secure_12(),
	Cookie_t2077085446::get_offset_of__timestamp_13(),
	Cookie_t2077085446::get_offset_of__value_14(),
	Cookie_t2077085446::get_offset_of__version_15(),
	Cookie_t2077085446::get_offset_of_U3CExactDomainU3Ek__BackingField_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1973 = { sizeof (CookieCollection_t1136277956), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1973[2] = 
{
	CookieCollection_t1136277956::get_offset_of__list_0(),
	CookieCollection_t1136277956::get_offset_of__sync_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1974 = { sizeof (CookieException_t967178805), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1975 = { sizeof (EndPointListener_t3188089579), -1, sizeof(EndPointListener_t3188089579_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1975[9] = 
{
	EndPointListener_t3188089579_StaticFields::get_offset_of__defaultCertFolderPath_0(),
	EndPointListener_t3188089579::get_offset_of__all_1(),
	EndPointListener_t3188089579::get_offset_of__cert_2(),
	EndPointListener_t3188089579::get_offset_of__endpoint_3(),
	EndPointListener_t3188089579::get_offset_of__prefixes_4(),
	EndPointListener_t3188089579::get_offset_of__secure_5(),
	EndPointListener_t3188089579::get_offset_of__socket_6(),
	EndPointListener_t3188089579::get_offset_of__unhandled_7(),
	EndPointListener_t3188089579::get_offset_of__unregistered_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1976 = { sizeof (EndPointManager_t3612541026), -1, sizeof(EndPointManager_t3612541026_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1976[1] = 
{
	EndPointManager_t3612541026_StaticFields::get_offset_of__ipToEndpoints_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1977 = { sizeof (HttpBasicIdentity_t3688512078), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1977[1] = 
{
	HttpBasicIdentity_t3688512078::get_offset_of__password_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1978 = { sizeof (HttpConnection_t602292776), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1978[23] = 
{
	0,
	HttpConnection_t602292776::get_offset_of__buffer_1(),
	HttpConnection_t602292776::get_offset_of__chunked_2(),
	HttpConnection_t602292776::get_offset_of__context_3(),
	HttpConnection_t602292776::get_offset_of__contextWasBound_4(),
	HttpConnection_t602292776::get_offset_of__currentLine_5(),
	HttpConnection_t602292776::get_offset_of__inputState_6(),
	HttpConnection_t602292776::get_offset_of__inputStream_7(),
	HttpConnection_t602292776::get_offset_of__lastListener_8(),
	HttpConnection_t602292776::get_offset_of__lineState_9(),
	HttpConnection_t602292776::get_offset_of__listener_10(),
	HttpConnection_t602292776::get_offset_of__outputStream_11(),
	HttpConnection_t602292776::get_offset_of__position_12(),
	HttpConnection_t602292776::get_offset_of__prefix_13(),
	HttpConnection_t602292776::get_offset_of__requestBuffer_14(),
	HttpConnection_t602292776::get_offset_of__reuses_15(),
	HttpConnection_t602292776::get_offset_of__secure_16(),
	HttpConnection_t602292776::get_offset_of__socket_17(),
	HttpConnection_t602292776::get_offset_of__stream_18(),
	HttpConnection_t602292776::get_offset_of__sync_19(),
	HttpConnection_t602292776::get_offset_of__timeout_20(),
	HttpConnection_t602292776::get_offset_of__timer_21(),
	HttpConnection_t602292776::get_offset_of__websocketStream_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1979 = { sizeof (HttpDigestIdentity_t2083794924), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1979[1] = 
{
	HttpDigestIdentity_t2083794924::get_offset_of__parameters_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1980 = { sizeof (HttpHeaderInfo_t3355144229), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1980[2] = 
{
	HttpHeaderInfo_t3355144229::get_offset_of__type_0(),
	HttpHeaderInfo_t3355144229::get_offset_of_U3CNameU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1981 = { sizeof (HttpHeaderType_t3355482801)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1981[8] = 
{
	HttpHeaderType_t3355482801::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1982 = { sizeof (HttpListener_t398944510), -1, sizeof(HttpListener_t398944510_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1982[15] = 
{
	HttpListener_t398944510::get_offset_of__authSchemes_0(),
	HttpListener_t398944510::get_offset_of__authSchemeSelector_1(),
	HttpListener_t398944510::get_offset_of__certFolderPath_2(),
	HttpListener_t398944510::get_offset_of__connections_3(),
	HttpListener_t398944510::get_offset_of__contextQueue_4(),
	HttpListener_t398944510::get_offset_of__credentialsFinder_5(),
	HttpListener_t398944510::get_offset_of__defaultCert_6(),
	HttpListener_t398944510::get_offset_of__disposed_7(),
	HttpListener_t398944510::get_offset_of__ignoreWriteExceptions_8(),
	HttpListener_t398944510::get_offset_of__listening_9(),
	HttpListener_t398944510::get_offset_of__prefixes_10(),
	HttpListener_t398944510::get_offset_of__realm_11(),
	HttpListener_t398944510::get_offset_of__registry_12(),
	HttpListener_t398944510::get_offset_of__waitQueue_13(),
	HttpListener_t398944510_StaticFields::get_offset_of_U3CU3Ef__amU24cacheE_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1983 = { sizeof (HttpListenerContext_t3744659101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1983[7] = 
{
	HttpListenerContext_t3744659101::get_offset_of__connection_0(),
	HttpListenerContext_t3744659101::get_offset_of__error_1(),
	HttpListenerContext_t3744659101::get_offset_of__errorStatus_2(),
	HttpListenerContext_t3744659101::get_offset_of__request_3(),
	HttpListenerContext_t3744659101::get_offset_of__response_4(),
	HttpListenerContext_t3744659101::get_offset_of__user_5(),
	HttpListenerContext_t3744659101::get_offset_of_Listener_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1984 = { sizeof (HttpListenerException_t2442008381), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1985 = { sizeof (HttpListenerPrefixCollection_t1379363822), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1985[2] = 
{
	HttpListenerPrefixCollection_t1379363822::get_offset_of__listener_0(),
	HttpListenerPrefixCollection_t1379363822::get_offset_of__prefixes_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1986 = { sizeof (HttpListenerRequest_t3888821117), -1, sizeof(HttpListenerRequest_t3888821117_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1986[22] = 
{
	HttpListenerRequest_t3888821117_StaticFields::get_offset_of__100continue_0(),
	HttpListenerRequest_t3888821117::get_offset_of__acceptTypes_1(),
	HttpListenerRequest_t3888821117::get_offset_of__chunked_2(),
	HttpListenerRequest_t3888821117::get_offset_of__contentEncoding_3(),
	HttpListenerRequest_t3888821117::get_offset_of__contentLength_4(),
	HttpListenerRequest_t3888821117::get_offset_of__contentLengthWasSet_5(),
	HttpListenerRequest_t3888821117::get_offset_of__context_6(),
	HttpListenerRequest_t3888821117::get_offset_of__cookies_7(),
	HttpListenerRequest_t3888821117::get_offset_of__headers_8(),
	HttpListenerRequest_t3888821117::get_offset_of__identifier_9(),
	HttpListenerRequest_t3888821117::get_offset_of__inputStream_10(),
	HttpListenerRequest_t3888821117::get_offset_of__keepAlive_11(),
	HttpListenerRequest_t3888821117::get_offset_of__keepAliveWasSet_12(),
	HttpListenerRequest_t3888821117::get_offset_of__method_13(),
	HttpListenerRequest_t3888821117::get_offset_of__queryString_14(),
	HttpListenerRequest_t3888821117::get_offset_of__referer_15(),
	HttpListenerRequest_t3888821117::get_offset_of__uri_16(),
	HttpListenerRequest_t3888821117::get_offset_of__url_17(),
	HttpListenerRequest_t3888821117::get_offset_of__userLanguages_18(),
	HttpListenerRequest_t3888821117::get_offset_of__version_19(),
	HttpListenerRequest_t3888821117::get_offset_of__websocketRequest_20(),
	HttpListenerRequest_t3888821117::get_offset_of__websocketRequestWasSet_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1987 = { sizeof (HttpListenerResponse_t1992878431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1987[17] = 
{
	HttpListenerResponse_t1992878431::get_offset_of__chunked_0(),
	HttpListenerResponse_t1992878431::get_offset_of__contentEncoding_1(),
	HttpListenerResponse_t1992878431::get_offset_of__contentLength_2(),
	HttpListenerResponse_t1992878431::get_offset_of__contentLengthSet_3(),
	HttpListenerResponse_t1992878431::get_offset_of__contentType_4(),
	HttpListenerResponse_t1992878431::get_offset_of__context_5(),
	HttpListenerResponse_t1992878431::get_offset_of__cookies_6(),
	HttpListenerResponse_t1992878431::get_offset_of__disposed_7(),
	HttpListenerResponse_t1992878431::get_offset_of__forceCloseChunked_8(),
	HttpListenerResponse_t1992878431::get_offset_of__headers_9(),
	HttpListenerResponse_t1992878431::get_offset_of__headersSent_10(),
	HttpListenerResponse_t1992878431::get_offset_of__keepAlive_11(),
	HttpListenerResponse_t1992878431::get_offset_of__location_12(),
	HttpListenerResponse_t1992878431::get_offset_of__outputStream_13(),
	HttpListenerResponse_t1992878431::get_offset_of__statusCode_14(),
	HttpListenerResponse_t1992878431::get_offset_of__statusDescription_15(),
	HttpListenerResponse_t1992878431::get_offset_of__version_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1988 = { sizeof (U3CfindCookieU3Ec__Iterator1B_t3494975687), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1988[10] = 
{
	U3CfindCookieU3Ec__Iterator1B_t3494975687::get_offset_of_cookie_0(),
	U3CfindCookieU3Ec__Iterator1B_t3494975687::get_offset_of_U3CnameU3E__0_1(),
	U3CfindCookieU3Ec__Iterator1B_t3494975687::get_offset_of_U3CdomainU3E__1_2(),
	U3CfindCookieU3Ec__Iterator1B_t3494975687::get_offset_of_U3CpathU3E__2_3(),
	U3CfindCookieU3Ec__Iterator1B_t3494975687::get_offset_of_U3CU24s_96U3E__3_4(),
	U3CfindCookieU3Ec__Iterator1B_t3494975687::get_offset_of_U3CcU3E__4_5(),
	U3CfindCookieU3Ec__Iterator1B_t3494975687::get_offset_of_U24PC_6(),
	U3CfindCookieU3Ec__Iterator1B_t3494975687::get_offset_of_U24current_7(),
	U3CfindCookieU3Ec__Iterator1B_t3494975687::get_offset_of_U3CU24U3Ecookie_8(),
	U3CfindCookieU3Ec__Iterator1B_t3494975687::get_offset_of_U3CU3Ef__this_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1989 = { sizeof (U3CCloseU3Ec__AnonStorey24_t788655901), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1989[2] = 
{
	U3CCloseU3Ec__AnonStorey24_t788655901::get_offset_of_output_0(),
	U3CCloseU3Ec__AnonStorey24_t788655901::get_offset_of_U3CU3Ef__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1990 = { sizeof (HttpStatusCode_t1625451593)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1990[47] = 
{
	HttpStatusCode_t1625451593::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1991 = { sizeof (HttpStreamAsyncResult_t303264603), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1991[10] = 
{
	HttpStreamAsyncResult_t303264603::get_offset_of__callback_0(),
	HttpStreamAsyncResult_t303264603::get_offset_of__completed_1(),
	HttpStreamAsyncResult_t303264603::get_offset_of__state_2(),
	HttpStreamAsyncResult_t303264603::get_offset_of__sync_3(),
	HttpStreamAsyncResult_t303264603::get_offset_of__waitHandle_4(),
	HttpStreamAsyncResult_t303264603::get_offset_of_Buffer_5(),
	HttpStreamAsyncResult_t303264603::get_offset_of_Count_6(),
	HttpStreamAsyncResult_t303264603::get_offset_of_Error_7(),
	HttpStreamAsyncResult_t303264603::get_offset_of_Offset_8(),
	HttpStreamAsyncResult_t303264603::get_offset_of_SyncRead_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1992 = { sizeof (HttpUtility_t1048309166), -1, sizeof(HttpUtility_t1048309166_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1992[3] = 
{
	HttpUtility_t1048309166_StaticFields::get_offset_of__entities_0(),
	HttpUtility_t1048309166_StaticFields::get_offset_of__hexChars_1(),
	HttpUtility_t1048309166_StaticFields::get_offset_of__sync_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1993 = { sizeof (HttpVersion_t1514895642), -1, sizeof(HttpVersion_t1514895642_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1993[2] = 
{
	HttpVersion_t1514895642_StaticFields::get_offset_of_Version10_0(),
	HttpVersion_t1514895642_StaticFields::get_offset_of_Version11_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1994 = { sizeof (InputChunkState_t4273164664)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1994[5] = 
{
	InputChunkState_t4273164664::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1995 = { sizeof (InputState_t2025202057)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1995[3] = 
{
	InputState_t2025202057::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1996 = { sizeof (LineState_t793115623)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1996[4] = 
{
	LineState_t793115623::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1997 = { sizeof (ListenerAsyncResult_t609216079), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1997[10] = 
{
	ListenerAsyncResult_t609216079::get_offset_of__callback_0(),
	ListenerAsyncResult_t609216079::get_offset_of__completed_1(),
	ListenerAsyncResult_t609216079::get_offset_of__context_2(),
	ListenerAsyncResult_t609216079::get_offset_of__exception_3(),
	ListenerAsyncResult_t609216079::get_offset_of__waitHandle_4(),
	ListenerAsyncResult_t609216079::get_offset_of__state_5(),
	ListenerAsyncResult_t609216079::get_offset_of__sync_6(),
	ListenerAsyncResult_t609216079::get_offset_of__syncCompleted_7(),
	ListenerAsyncResult_t609216079::get_offset_of_EndCalled_8(),
	ListenerAsyncResult_t609216079::get_offset_of_InGet_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1998 = { sizeof (ListenerPrefix_t2663314696), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1998[7] = 
{
	ListenerPrefix_t2663314696::get_offset_of__addresses_0(),
	ListenerPrefix_t2663314696::get_offset_of__host_1(),
	ListenerPrefix_t2663314696::get_offset_of__original_2(),
	ListenerPrefix_t2663314696::get_offset_of__path_3(),
	ListenerPrefix_t2663314696::get_offset_of__port_4(),
	ListenerPrefix_t2663314696::get_offset_of__secure_5(),
	ListenerPrefix_t2663314696::get_offset_of_Listener_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1999 = { sizeof (NetworkCredential_t1204099087), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1999[4] = 
{
	NetworkCredential_t1204099087::get_offset_of__domain_0(),
	NetworkCredential_t1204099087::get_offset_of__password_1(),
	NetworkCredential_t1204099087::get_offset_of__roles_2(),
	NetworkCredential_t1204099087::get_offset_of__username_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
