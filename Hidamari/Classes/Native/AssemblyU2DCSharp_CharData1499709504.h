﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharData
struct  CharData_t1499709504 
{
public:
	// System.Int32 CharData::_hp
	int32_t ____hp_0;
	// System.Single CharData::_spd
	float ____spd_1;
	// System.Single CharData::_technique
	float ____technique_2;

public:
	inline static int32_t get_offset_of__hp_0() { return static_cast<int32_t>(offsetof(CharData_t1499709504, ____hp_0)); }
	inline int32_t get__hp_0() const { return ____hp_0; }
	inline int32_t* get_address_of__hp_0() { return &____hp_0; }
	inline void set__hp_0(int32_t value)
	{
		____hp_0 = value;
	}

	inline static int32_t get_offset_of__spd_1() { return static_cast<int32_t>(offsetof(CharData_t1499709504, ____spd_1)); }
	inline float get__spd_1() const { return ____spd_1; }
	inline float* get_address_of__spd_1() { return &____spd_1; }
	inline void set__spd_1(float value)
	{
		____spd_1 = value;
	}

	inline static int32_t get_offset_of__technique_2() { return static_cast<int32_t>(offsetof(CharData_t1499709504, ____technique_2)); }
	inline float get__technique_2() const { return ____technique_2; }
	inline float* get_address_of__technique_2() { return &____technique_2; }
	inline void set__technique_2(float value)
	{
		____technique_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: CharData
struct CharData_t1499709504_marshaled_pinvoke
{
	int32_t ____hp_0;
	float ____spd_1;
	float ____technique_2;
};
// Native definition for marshalling of: CharData
struct CharData_t1499709504_marshaled_com
{
	int32_t ____hp_0;
	float ____spd_1;
	float ____technique_2;
};
