﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.GameObject>>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m4170766559(__this, ___l0, method) ((  void (*) (Enumerator_t2135758583 *, List_1_t2116085813 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.GameObject>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3759926291(__this, method) ((  void (*) (Enumerator_t2135758583 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.GameObject>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3335896703(__this, method) ((  Il2CppObject * (*) (Enumerator_t2135758583 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.GameObject>>::Dispose()
#define Enumerator_Dispose_m992366212(__this, method) ((  void (*) (Enumerator_t2135758583 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.GameObject>>::VerifyState()
#define Enumerator_VerifyState_m4089916349(__this, method) ((  void (*) (Enumerator_t2135758583 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.GameObject>>::MoveNext()
#define Enumerator_MoveNext_m818946431(__this, method) ((  bool (*) (Enumerator_t2135758583 *, const MethodInfo*))Enumerator_MoveNext_m844464217_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.GameObject>>::get_Current()
#define Enumerator_get_Current_m997523764(__this, method) ((  List_1_t747900261 * (*) (Enumerator_t2135758583 *, const MethodInfo*))Enumerator_get_Current_m4198990746_gshared)(__this, method)
