﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FightManager
struct FightManager_t1489248669;
// SocketIO.SocketIOEvent
struct SocketIOEvent_t4011854063;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SocketIO_SocketIOEvent4011854063.h"

// System.Void FightManager::.ctor()
extern "C"  void FightManager__ctor_m337858974 (FightManager_t1489248669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightManager::Start()
extern "C"  void FightManager_Start_m3579964062 (FightManager_t1489248669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightManager::Update()
extern "C"  void FightManager_Update_m3610555695 (FightManager_t1489248669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightManager::Online(SocketIO.SocketIOEvent)
extern "C"  void FightManager_Online_m1505902797 (FightManager_t1489248669 * __this, SocketIOEvent_t4011854063 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightManager::Join(SocketIO.SocketIOEvent)
extern "C"  void FightManager_Join_m2626930660 (FightManager_t1489248669 * __this, SocketIOEvent_t4011854063 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightManager::Offline(SocketIO.SocketIOEvent)
extern "C"  void FightManager_Offline_m3580251443 (FightManager_t1489248669 * __this, SocketIOEvent_t4011854063 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightManager::Message(SocketIO.SocketIOEvent)
extern "C"  void FightManager_Message_m2184450903 (FightManager_t1489248669 * __this, SocketIOEvent_t4011854063 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightManager::Onlinenotification(SocketIO.SocketIOEvent)
extern "C"  void FightManager_Onlinenotification_m387409080 (FightManager_t1489248669 * __this, SocketIOEvent_t4011854063 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightManager::Endnotification(SocketIO.SocketIOEvent)
extern "C"  void FightManager_Endnotification_m3945577622 (FightManager_t1489248669 * __this, SocketIOEvent_t4011854063 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightManager::Error(SocketIO.SocketIOEvent)
extern "C"  void FightManager_Error_m3851625752 (FightManager_t1489248669 * __this, SocketIOEvent_t4011854063 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
