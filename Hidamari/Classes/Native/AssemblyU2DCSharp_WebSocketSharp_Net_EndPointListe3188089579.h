﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>
struct List_1_t4031500248;
// System.Security.Cryptography.X509Certificates.X509Certificate2
struct X509Certificate2_t160474609;
// System.Net.IPEndPoint
struct IPEndPoint_t2123960758;
// System.Collections.Generic.Dictionary`2<WebSocketSharp.Net.ListenerPrefix,WebSocketSharp.Net.HttpListener>
struct Dictionary_2_t625312017;
// System.Net.Sockets.Socket
struct Socket_t2157335841;
// System.Collections.Generic.Dictionary`2<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>
struct Dictionary_2_t3390149787;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.EndPointListener
struct  EndPointListener_t3188089579  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix> WebSocketSharp.Net.EndPointListener::_all
	List_1_t4031500248 * ____all_1;
	// System.Security.Cryptography.X509Certificates.X509Certificate2 WebSocketSharp.Net.EndPointListener::_cert
	X509Certificate2_t160474609 * ____cert_2;
	// System.Net.IPEndPoint WebSocketSharp.Net.EndPointListener::_endpoint
	IPEndPoint_t2123960758 * ____endpoint_3;
	// System.Collections.Generic.Dictionary`2<WebSocketSharp.Net.ListenerPrefix,WebSocketSharp.Net.HttpListener> WebSocketSharp.Net.EndPointListener::_prefixes
	Dictionary_2_t625312017 * ____prefixes_4;
	// System.Boolean WebSocketSharp.Net.EndPointListener::_secure
	bool ____secure_5;
	// System.Net.Sockets.Socket WebSocketSharp.Net.EndPointListener::_socket
	Socket_t2157335841 * ____socket_6;
	// System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix> WebSocketSharp.Net.EndPointListener::_unhandled
	List_1_t4031500248 * ____unhandled_7;
	// System.Collections.Generic.Dictionary`2<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection> WebSocketSharp.Net.EndPointListener::_unregistered
	Dictionary_2_t3390149787 * ____unregistered_8;

public:
	inline static int32_t get_offset_of__all_1() { return static_cast<int32_t>(offsetof(EndPointListener_t3188089579, ____all_1)); }
	inline List_1_t4031500248 * get__all_1() const { return ____all_1; }
	inline List_1_t4031500248 ** get_address_of__all_1() { return &____all_1; }
	inline void set__all_1(List_1_t4031500248 * value)
	{
		____all_1 = value;
		Il2CppCodeGenWriteBarrier(&____all_1, value);
	}

	inline static int32_t get_offset_of__cert_2() { return static_cast<int32_t>(offsetof(EndPointListener_t3188089579, ____cert_2)); }
	inline X509Certificate2_t160474609 * get__cert_2() const { return ____cert_2; }
	inline X509Certificate2_t160474609 ** get_address_of__cert_2() { return &____cert_2; }
	inline void set__cert_2(X509Certificate2_t160474609 * value)
	{
		____cert_2 = value;
		Il2CppCodeGenWriteBarrier(&____cert_2, value);
	}

	inline static int32_t get_offset_of__endpoint_3() { return static_cast<int32_t>(offsetof(EndPointListener_t3188089579, ____endpoint_3)); }
	inline IPEndPoint_t2123960758 * get__endpoint_3() const { return ____endpoint_3; }
	inline IPEndPoint_t2123960758 ** get_address_of__endpoint_3() { return &____endpoint_3; }
	inline void set__endpoint_3(IPEndPoint_t2123960758 * value)
	{
		____endpoint_3 = value;
		Il2CppCodeGenWriteBarrier(&____endpoint_3, value);
	}

	inline static int32_t get_offset_of__prefixes_4() { return static_cast<int32_t>(offsetof(EndPointListener_t3188089579, ____prefixes_4)); }
	inline Dictionary_2_t625312017 * get__prefixes_4() const { return ____prefixes_4; }
	inline Dictionary_2_t625312017 ** get_address_of__prefixes_4() { return &____prefixes_4; }
	inline void set__prefixes_4(Dictionary_2_t625312017 * value)
	{
		____prefixes_4 = value;
		Il2CppCodeGenWriteBarrier(&____prefixes_4, value);
	}

	inline static int32_t get_offset_of__secure_5() { return static_cast<int32_t>(offsetof(EndPointListener_t3188089579, ____secure_5)); }
	inline bool get__secure_5() const { return ____secure_5; }
	inline bool* get_address_of__secure_5() { return &____secure_5; }
	inline void set__secure_5(bool value)
	{
		____secure_5 = value;
	}

	inline static int32_t get_offset_of__socket_6() { return static_cast<int32_t>(offsetof(EndPointListener_t3188089579, ____socket_6)); }
	inline Socket_t2157335841 * get__socket_6() const { return ____socket_6; }
	inline Socket_t2157335841 ** get_address_of__socket_6() { return &____socket_6; }
	inline void set__socket_6(Socket_t2157335841 * value)
	{
		____socket_6 = value;
		Il2CppCodeGenWriteBarrier(&____socket_6, value);
	}

	inline static int32_t get_offset_of__unhandled_7() { return static_cast<int32_t>(offsetof(EndPointListener_t3188089579, ____unhandled_7)); }
	inline List_1_t4031500248 * get__unhandled_7() const { return ____unhandled_7; }
	inline List_1_t4031500248 ** get_address_of__unhandled_7() { return &____unhandled_7; }
	inline void set__unhandled_7(List_1_t4031500248 * value)
	{
		____unhandled_7 = value;
		Il2CppCodeGenWriteBarrier(&____unhandled_7, value);
	}

	inline static int32_t get_offset_of__unregistered_8() { return static_cast<int32_t>(offsetof(EndPointListener_t3188089579, ____unregistered_8)); }
	inline Dictionary_2_t3390149787 * get__unregistered_8() const { return ____unregistered_8; }
	inline Dictionary_2_t3390149787 ** get_address_of__unregistered_8() { return &____unregistered_8; }
	inline void set__unregistered_8(Dictionary_2_t3390149787 * value)
	{
		____unregistered_8 = value;
		Il2CppCodeGenWriteBarrier(&____unregistered_8, value);
	}
};

struct EndPointListener_t3188089579_StaticFields
{
public:
	// System.String WebSocketSharp.Net.EndPointListener::_defaultCertFolderPath
	String_t* ____defaultCertFolderPath_0;

public:
	inline static int32_t get_offset_of__defaultCertFolderPath_0() { return static_cast<int32_t>(offsetof(EndPointListener_t3188089579_StaticFields, ____defaultCertFolderPath_0)); }
	inline String_t* get__defaultCertFolderPath_0() const { return ____defaultCertFolderPath_0; }
	inline String_t** get_address_of__defaultCertFolderPath_0() { return &____defaultCertFolderPath_0; }
	inline void set__defaultCertFolderPath_0(String_t* value)
	{
		____defaultCertFolderPath_0 = value;
		Il2CppCodeGenWriteBarrier(&____defaultCertFolderPath_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
