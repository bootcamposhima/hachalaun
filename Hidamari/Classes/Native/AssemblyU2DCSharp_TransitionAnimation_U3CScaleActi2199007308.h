﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TransitionAnimation/<ScaleAction>c__IteratorC
struct  U3CScaleActionU3Ec__IteratorC_t2199007308  : public Il2CppObject
{
public:
	// System.Single TransitionAnimation/<ScaleAction>c__IteratorC::defoult
	float ___defoult_0;
	// System.Single TransitionAnimation/<ScaleAction>c__IteratorC::<defoultscale>__0
	float ___U3CdefoultscaleU3E__0_1;
	// System.Single TransitionAnimation/<ScaleAction>c__IteratorC::nextscale
	float ___nextscale_2;
	// System.Single TransitionAnimation/<ScaleAction>c__IteratorC::<s>__1
	float ___U3CsU3E__1_3;
	// System.Single TransitionAnimation/<ScaleAction>c__IteratorC::speed
	float ___speed_4;
	// System.Single TransitionAnimation/<ScaleAction>c__IteratorC::<num>__2
	float ___U3CnumU3E__2_5;
	// System.Single TransitionAnimation/<ScaleAction>c__IteratorC::<ss>__3
	float ___U3CssU3E__3_6;
	// System.Boolean TransitionAnimation/<ScaleAction>c__IteratorC::<isup>__4
	bool ___U3CisupU3E__4_7;
	// UnityEngine.GameObject TransitionAnimation/<ScaleAction>c__IteratorC::g
	GameObject_t3674682005 * ___g_8;
	// System.Int32 TransitionAnimation/<ScaleAction>c__IteratorC::$PC
	int32_t ___U24PC_9;
	// System.Object TransitionAnimation/<ScaleAction>c__IteratorC::$current
	Il2CppObject * ___U24current_10;
	// System.Single TransitionAnimation/<ScaleAction>c__IteratorC::<$>defoult
	float ___U3CU24U3Edefoult_11;
	// System.Single TransitionAnimation/<ScaleAction>c__IteratorC::<$>nextscale
	float ___U3CU24U3Enextscale_12;
	// System.Single TransitionAnimation/<ScaleAction>c__IteratorC::<$>speed
	float ___U3CU24U3Espeed_13;
	// UnityEngine.GameObject TransitionAnimation/<ScaleAction>c__IteratorC::<$>g
	GameObject_t3674682005 * ___U3CU24U3Eg_14;

public:
	inline static int32_t get_offset_of_defoult_0() { return static_cast<int32_t>(offsetof(U3CScaleActionU3Ec__IteratorC_t2199007308, ___defoult_0)); }
	inline float get_defoult_0() const { return ___defoult_0; }
	inline float* get_address_of_defoult_0() { return &___defoult_0; }
	inline void set_defoult_0(float value)
	{
		___defoult_0 = value;
	}

	inline static int32_t get_offset_of_U3CdefoultscaleU3E__0_1() { return static_cast<int32_t>(offsetof(U3CScaleActionU3Ec__IteratorC_t2199007308, ___U3CdefoultscaleU3E__0_1)); }
	inline float get_U3CdefoultscaleU3E__0_1() const { return ___U3CdefoultscaleU3E__0_1; }
	inline float* get_address_of_U3CdefoultscaleU3E__0_1() { return &___U3CdefoultscaleU3E__0_1; }
	inline void set_U3CdefoultscaleU3E__0_1(float value)
	{
		___U3CdefoultscaleU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_nextscale_2() { return static_cast<int32_t>(offsetof(U3CScaleActionU3Ec__IteratorC_t2199007308, ___nextscale_2)); }
	inline float get_nextscale_2() const { return ___nextscale_2; }
	inline float* get_address_of_nextscale_2() { return &___nextscale_2; }
	inline void set_nextscale_2(float value)
	{
		___nextscale_2 = value;
	}

	inline static int32_t get_offset_of_U3CsU3E__1_3() { return static_cast<int32_t>(offsetof(U3CScaleActionU3Ec__IteratorC_t2199007308, ___U3CsU3E__1_3)); }
	inline float get_U3CsU3E__1_3() const { return ___U3CsU3E__1_3; }
	inline float* get_address_of_U3CsU3E__1_3() { return &___U3CsU3E__1_3; }
	inline void set_U3CsU3E__1_3(float value)
	{
		___U3CsU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(U3CScaleActionU3Ec__IteratorC_t2199007308, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_U3CnumU3E__2_5() { return static_cast<int32_t>(offsetof(U3CScaleActionU3Ec__IteratorC_t2199007308, ___U3CnumU3E__2_5)); }
	inline float get_U3CnumU3E__2_5() const { return ___U3CnumU3E__2_5; }
	inline float* get_address_of_U3CnumU3E__2_5() { return &___U3CnumU3E__2_5; }
	inline void set_U3CnumU3E__2_5(float value)
	{
		___U3CnumU3E__2_5 = value;
	}

	inline static int32_t get_offset_of_U3CssU3E__3_6() { return static_cast<int32_t>(offsetof(U3CScaleActionU3Ec__IteratorC_t2199007308, ___U3CssU3E__3_6)); }
	inline float get_U3CssU3E__3_6() const { return ___U3CssU3E__3_6; }
	inline float* get_address_of_U3CssU3E__3_6() { return &___U3CssU3E__3_6; }
	inline void set_U3CssU3E__3_6(float value)
	{
		___U3CssU3E__3_6 = value;
	}

	inline static int32_t get_offset_of_U3CisupU3E__4_7() { return static_cast<int32_t>(offsetof(U3CScaleActionU3Ec__IteratorC_t2199007308, ___U3CisupU3E__4_7)); }
	inline bool get_U3CisupU3E__4_7() const { return ___U3CisupU3E__4_7; }
	inline bool* get_address_of_U3CisupU3E__4_7() { return &___U3CisupU3E__4_7; }
	inline void set_U3CisupU3E__4_7(bool value)
	{
		___U3CisupU3E__4_7 = value;
	}

	inline static int32_t get_offset_of_g_8() { return static_cast<int32_t>(offsetof(U3CScaleActionU3Ec__IteratorC_t2199007308, ___g_8)); }
	inline GameObject_t3674682005 * get_g_8() const { return ___g_8; }
	inline GameObject_t3674682005 ** get_address_of_g_8() { return &___g_8; }
	inline void set_g_8(GameObject_t3674682005 * value)
	{
		___g_8 = value;
		Il2CppCodeGenWriteBarrier(&___g_8, value);
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CScaleActionU3Ec__IteratorC_t2199007308, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}

	inline static int32_t get_offset_of_U24current_10() { return static_cast<int32_t>(offsetof(U3CScaleActionU3Ec__IteratorC_t2199007308, ___U24current_10)); }
	inline Il2CppObject * get_U24current_10() const { return ___U24current_10; }
	inline Il2CppObject ** get_address_of_U24current_10() { return &___U24current_10; }
	inline void set_U24current_10(Il2CppObject * value)
	{
		___U24current_10 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_10, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Edefoult_11() { return static_cast<int32_t>(offsetof(U3CScaleActionU3Ec__IteratorC_t2199007308, ___U3CU24U3Edefoult_11)); }
	inline float get_U3CU24U3Edefoult_11() const { return ___U3CU24U3Edefoult_11; }
	inline float* get_address_of_U3CU24U3Edefoult_11() { return &___U3CU24U3Edefoult_11; }
	inline void set_U3CU24U3Edefoult_11(float value)
	{
		___U3CU24U3Edefoult_11 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Enextscale_12() { return static_cast<int32_t>(offsetof(U3CScaleActionU3Ec__IteratorC_t2199007308, ___U3CU24U3Enextscale_12)); }
	inline float get_U3CU24U3Enextscale_12() const { return ___U3CU24U3Enextscale_12; }
	inline float* get_address_of_U3CU24U3Enextscale_12() { return &___U3CU24U3Enextscale_12; }
	inline void set_U3CU24U3Enextscale_12(float value)
	{
		___U3CU24U3Enextscale_12 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Espeed_13() { return static_cast<int32_t>(offsetof(U3CScaleActionU3Ec__IteratorC_t2199007308, ___U3CU24U3Espeed_13)); }
	inline float get_U3CU24U3Espeed_13() const { return ___U3CU24U3Espeed_13; }
	inline float* get_address_of_U3CU24U3Espeed_13() { return &___U3CU24U3Espeed_13; }
	inline void set_U3CU24U3Espeed_13(float value)
	{
		___U3CU24U3Espeed_13 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Eg_14() { return static_cast<int32_t>(offsetof(U3CScaleActionU3Ec__IteratorC_t2199007308, ___U3CU24U3Eg_14)); }
	inline GameObject_t3674682005 * get_U3CU24U3Eg_14() const { return ___U3CU24U3Eg_14; }
	inline GameObject_t3674682005 ** get_address_of_U3CU24U3Eg_14() { return &___U3CU24U3Eg_14; }
	inline void set_U3CU24U3Eg_14(GameObject_t3674682005 * value)
	{
		___U3CU24U3Eg_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eg_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
