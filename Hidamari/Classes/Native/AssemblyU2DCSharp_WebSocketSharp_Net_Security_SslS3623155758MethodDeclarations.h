﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.Net.Security.SslStream
struct SslStream_t3623155758;
// System.Net.Sockets.NetworkStream
struct NetworkStream_t3953762560;
// System.Net.Security.RemoteCertificateValidationCallback
struct RemoteCertificateValidationCallback_t1894914657;
// System.Net.Security.LocalCertificateSelectionCallback
struct LocalCertificateSelectionCallback_t2431285719;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_Sockets_NetworkStream3953762560.h"
#include "System_System_Net_Security_RemoteCertificateValida1894914657.h"
#include "System_System_Net_Security_LocalCertificateSelecti2431285719.h"

// System.Void WebSocketSharp.Net.Security.SslStream::.ctor(System.Net.Sockets.NetworkStream)
extern "C"  void SslStream__ctor_m2777770689 (SslStream_t3623155758 * __this, NetworkStream_t3953762560 * ___innerStream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.Security.SslStream::.ctor(System.Net.Sockets.NetworkStream,System.Boolean)
extern "C"  void SslStream__ctor_m3865439900 (SslStream_t3623155758 * __this, NetworkStream_t3953762560 * ___innerStream0, bool ___leaveInnerStreamOpen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.Security.SslStream::.ctor(System.Net.Sockets.NetworkStream,System.Boolean,System.Net.Security.RemoteCertificateValidationCallback)
extern "C"  void SslStream__ctor_m4181297623 (SslStream_t3623155758 * __this, NetworkStream_t3953762560 * ___innerStream0, bool ___leaveInnerStreamOpen1, RemoteCertificateValidationCallback_t1894914657 * ___userCertificateValidationCallback2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.Security.SslStream::.ctor(System.Net.Sockets.NetworkStream,System.Boolean,System.Net.Security.RemoteCertificateValidationCallback,System.Net.Security.LocalCertificateSelectionCallback)
extern "C"  void SslStream__ctor_m4118869468 (SslStream_t3623155758 * __this, NetworkStream_t3953762560 * ___innerStream0, bool ___leaveInnerStreamOpen1, RemoteCertificateValidationCallback_t1894914657 * ___userCertificateValidationCallback2, LocalCertificateSelectionCallback_t2431285719 * ___userCertificateSelectionCallback3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.Net.Security.SslStream::get_DataAvailable()
extern "C"  bool SslStream_get_DataAvailable_m1180989081 (SslStream_t3623155758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
