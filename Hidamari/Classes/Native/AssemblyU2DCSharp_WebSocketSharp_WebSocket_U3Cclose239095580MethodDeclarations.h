﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.WebSocket/<closeAsync>c__AnonStorey28
struct U3CcloseAsyncU3Ec__AnonStorey28_t239095580;
// System.IAsyncResult
struct IAsyncResult_t2754620036;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocketSharp.WebSocket/<closeAsync>c__AnonStorey28::.ctor()
extern "C"  void U3CcloseAsyncU3Ec__AnonStorey28__ctor_m55241471 (U3CcloseAsyncU3Ec__AnonStorey28_t239095580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket/<closeAsync>c__AnonStorey28::<>m__E(System.IAsyncResult)
extern "C"  void U3CcloseAsyncU3Ec__AnonStorey28_U3CU3Em__E_m3722936676 (U3CcloseAsyncU3Ec__AnonStorey28_t239095580 * __this, Il2CppObject * ___ar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
