﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.WebSocketFrame
struct WebSocketFrame_t778194306;
// WebSocketSharp.PayloadData
struct PayloadData_t39926750;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.String
struct String_t;
// System.IO.Stream
struct Stream_t1561764144;
// System.Collections.Generic.IEnumerator`1<System.Byte>
struct IEnumerator_1_t479507413;
// System.Action`1<WebSocketSharp.WebSocketFrame>
struct Action_1_t1174010442;
// System.Action`1<System.Exception>
struct Action_1_t92447661;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Opcode3782140426.h"
#include "AssemblyU2DCSharp_WebSocketSharp_PayloadData39926750.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Mask3422653544.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Fin3262160529.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Rsv3262172379.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocketFrame778194306.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "AssemblyU2DCSharp_WebSocketSharp_CloseStatusCode3936110621.h"
#include "mscorlib_System_String7231557.h"

// System.Void WebSocketSharp.WebSocketFrame::.ctor()
extern "C"  void WebSocketFrame__ctor_m2272980410 (WebSocketFrame_t778194306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocketFrame::.ctor(WebSocketSharp.Opcode,WebSocketSharp.PayloadData)
extern "C"  void WebSocketFrame__ctor_m4163479642 (WebSocketFrame_t778194306 * __this, uint8_t ___opcode0, PayloadData_t39926750 * ___payload1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocketFrame::.ctor(WebSocketSharp.Opcode,WebSocketSharp.Mask,WebSocketSharp.PayloadData)
extern "C"  void WebSocketFrame__ctor_m1992914661 (WebSocketFrame_t778194306 * __this, uint8_t ___opcode0, uint8_t ___mask1, PayloadData_t39926750 * ___payload2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocketFrame::.ctor(WebSocketSharp.Fin,WebSocketSharp.Opcode,WebSocketSharp.Mask,WebSocketSharp.PayloadData)
extern "C"  void WebSocketFrame__ctor_m771058775 (WebSocketFrame_t778194306 * __this, uint8_t ___fin0, uint8_t ___opcode1, uint8_t ___mask2, PayloadData_t39926750 * ___payload3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocketFrame::.ctor(WebSocketSharp.Fin,WebSocketSharp.Opcode,WebSocketSharp.Mask,WebSocketSharp.PayloadData,System.Boolean)
extern "C"  void WebSocketFrame__ctor_m3336031814 (WebSocketFrame_t778194306 * __this, uint8_t ___fin0, uint8_t ___opcode1, uint8_t ___mask2, PayloadData_t39926750 * ___payload3, bool ___compressed4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocketFrame::.cctor()
extern "C"  void WebSocketFrame__cctor_m1260819763 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator WebSocketSharp.WebSocketFrame::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * WebSocketFrame_System_Collections_IEnumerable_GetEnumerator_m3591556711 (WebSocketFrame_t778194306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] WebSocketSharp.WebSocketFrame::get_ExtendedPayloadLength()
extern "C"  ByteU5BU5D_t4260760469* WebSocketFrame_get_ExtendedPayloadLength_m2660771956 (WebSocketFrame_t778194306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.Fin WebSocketSharp.WebSocketFrame::get_Fin()
extern "C"  uint8_t WebSocketFrame_get_Fin_m1975553495 (WebSocketFrame_t778194306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsBinary()
extern "C"  bool WebSocketFrame_get_IsBinary_m2066147498 (WebSocketFrame_t778194306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsClose()
extern "C"  bool WebSocketFrame_get_IsClose_m2149864465 (WebSocketFrame_t778194306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsCompressed()
extern "C"  bool WebSocketFrame_get_IsCompressed_m3923144746 (WebSocketFrame_t778194306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsContinuation()
extern "C"  bool WebSocketFrame_get_IsContinuation_m4205121376 (WebSocketFrame_t778194306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsControl()
extern "C"  bool WebSocketFrame_get_IsControl_m229257270 (WebSocketFrame_t778194306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsData()
extern "C"  bool WebSocketFrame_get_IsData_m2997444627 (WebSocketFrame_t778194306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsFinal()
extern "C"  bool WebSocketFrame_get_IsFinal_m430067727 (WebSocketFrame_t778194306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsFragmented()
extern "C"  bool WebSocketFrame_get_IsFragmented_m98582552 (WebSocketFrame_t778194306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsMasked()
extern "C"  bool WebSocketFrame_get_IsMasked_m1404103348 (WebSocketFrame_t778194306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsPerMessageCompressed()
extern "C"  bool WebSocketFrame_get_IsPerMessageCompressed_m1925771124 (WebSocketFrame_t778194306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsPing()
extern "C"  bool WebSocketFrame_get_IsPing_m3348209627 (WebSocketFrame_t778194306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsPong()
extern "C"  bool WebSocketFrame_get_IsPong_m3353750753 (WebSocketFrame_t778194306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocketFrame::get_IsText()
extern "C"  bool WebSocketFrame_get_IsText_m3459342550 (WebSocketFrame_t778194306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 WebSocketSharp.WebSocketFrame::get_Length()
extern "C"  uint64_t WebSocketFrame_get_Length_m132165789 (WebSocketFrame_t778194306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.Mask WebSocketSharp.WebSocketFrame::get_Mask()
extern "C"  uint8_t WebSocketFrame_get_Mask_m1377172099 (WebSocketFrame_t778194306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] WebSocketSharp.WebSocketFrame::get_MaskingKey()
extern "C"  ByteU5BU5D_t4260760469* WebSocketFrame_get_MaskingKey_m1393028594 (WebSocketFrame_t778194306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.Opcode WebSocketSharp.WebSocketFrame::get_Opcode()
extern "C"  uint8_t WebSocketFrame_get_Opcode_m53528135 (WebSocketFrame_t778194306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.PayloadData WebSocketSharp.WebSocketFrame::get_PayloadData()
extern "C"  PayloadData_t39926750 * WebSocketFrame_get_PayloadData_m4173896503 (WebSocketFrame_t778194306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte WebSocketSharp.WebSocketFrame::get_PayloadLength()
extern "C"  uint8_t WebSocketFrame_get_PayloadLength_m2711758479 (WebSocketFrame_t778194306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.Rsv WebSocketSharp.WebSocketFrame::get_Rsv1()
extern "C"  uint8_t WebSocketFrame_get_Rsv1_m1579988508 (WebSocketFrame_t778194306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.Rsv WebSocketSharp.WebSocketFrame::get_Rsv2()
extern "C"  uint8_t WebSocketFrame_get_Rsv2_m1579989469 (WebSocketFrame_t778194306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.Rsv WebSocketSharp.WebSocketFrame::get_Rsv3()
extern "C"  uint8_t WebSocketFrame_get_Rsv3_m1579990430 (WebSocketFrame_t778194306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] WebSocketSharp.WebSocketFrame::createMaskingKey()
extern "C"  ByteU5BU5D_t4260760469* WebSocketFrame_createMaskingKey_m991693541 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.WebSocketFrame::dump(WebSocketSharp.WebSocketFrame)
extern "C"  String_t* WebSocketFrame_dump_m3839803398 (Il2CppObject * __this /* static, unused */, WebSocketFrame_t778194306 * ___frame0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocketFrame::isControl(WebSocketSharp.Opcode)
extern "C"  bool WebSocketFrame_isControl_m3029256346 (Il2CppObject * __this /* static, unused */, uint8_t ___opcode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocketFrame::isData(WebSocketSharp.Opcode)
extern "C"  bool WebSocketFrame_isData_m69248783 (Il2CppObject * __this /* static, unused */, uint8_t ___opcode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::parse(System.Byte[],System.IO.Stream,System.Boolean)
extern "C"  WebSocketFrame_t778194306 * WebSocketFrame_parse_m2392405532 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___header0, Stream_t1561764144 * ___stream1, bool ___unmask2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.WebSocketFrame::print(WebSocketSharp.WebSocketFrame)
extern "C"  String_t* WebSocketFrame_print_m1807623609 (Il2CppObject * __this /* static, unused */, WebSocketFrame_t778194306 * ___frame0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::CreateCloseFrame(WebSocketSharp.Mask,WebSocketSharp.PayloadData)
extern "C"  WebSocketFrame_t778194306 * WebSocketFrame_CreateCloseFrame_m431346455 (Il2CppObject * __this /* static, unused */, uint8_t ___mask0, PayloadData_t39926750 * ___payload1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::CreatePongFrame(WebSocketSharp.Mask,WebSocketSharp.PayloadData)
extern "C"  WebSocketFrame_t778194306 * WebSocketFrame_CreatePongFrame_m950933809 (Il2CppObject * __this /* static, unused */, uint8_t ___mask0, PayloadData_t39926750 * ___payload1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::CreateCloseFrame(WebSocketSharp.Mask,System.Byte[])
extern "C"  WebSocketFrame_t778194306 * WebSocketFrame_CreateCloseFrame_m3543925733 (Il2CppObject * __this /* static, unused */, uint8_t ___mask0, ByteU5BU5D_t4260760469* ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::CreateCloseFrame(WebSocketSharp.Mask,WebSocketSharp.CloseStatusCode,System.String)
extern "C"  WebSocketFrame_t778194306 * WebSocketFrame_CreateCloseFrame_m110968692 (Il2CppObject * __this /* static, unused */, uint8_t ___mask0, uint16_t ___code1, String_t* ___reason2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::CreateFrame(WebSocketSharp.Fin,WebSocketSharp.Opcode,WebSocketSharp.Mask,System.Byte[],System.Boolean)
extern "C"  WebSocketFrame_t778194306 * WebSocketFrame_CreateFrame_m309663431 (Il2CppObject * __this /* static, unused */, uint8_t ___fin0, uint8_t ___opcode1, uint8_t ___mask2, ByteU5BU5D_t4260760469* ___data3, bool ___compressed4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::CreatePingFrame(WebSocketSharp.Mask)
extern "C"  WebSocketFrame_t778194306 * WebSocketFrame_CreatePingFrame_m982549890 (Il2CppObject * __this /* static, unused */, uint8_t ___mask0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::CreatePingFrame(WebSocketSharp.Mask,System.Byte[])
extern "C"  WebSocketFrame_t778194306 * WebSocketFrame_CreatePingFrame_m3019981573 (Il2CppObject * __this /* static, unused */, uint8_t ___mask0, ByteU5BU5D_t4260760469* ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Byte> WebSocketSharp.WebSocketFrame::GetEnumerator()
extern "C"  Il2CppObject* WebSocketFrame_GetEnumerator_m2490913985 (WebSocketFrame_t778194306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::Parse(System.Byte[])
extern "C"  WebSocketFrame_t778194306 * WebSocketFrame_Parse_m3362537340 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___src0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::Parse(System.IO.Stream)
extern "C"  WebSocketFrame_t778194306 * WebSocketFrame_Parse_m3554870788 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___stream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::Parse(System.Byte[],System.Boolean)
extern "C"  WebSocketFrame_t778194306 * WebSocketFrame_Parse_m93695105 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___src0, bool ___unmask1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::Parse(System.IO.Stream,System.Boolean)
extern "C"  WebSocketFrame_t778194306 * WebSocketFrame_Parse_m2256012537 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___stream0, bool ___unmask1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocketFrame::ParseAsync(System.IO.Stream,System.Action`1<WebSocketSharp.WebSocketFrame>)
extern "C"  void WebSocketFrame_ParseAsync_m1958002905 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___stream0, Action_1_t1174010442 * ___completed1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocketFrame::ParseAsync(System.IO.Stream,System.Action`1<WebSocketSharp.WebSocketFrame>,System.Action`1<System.Exception>)
extern "C"  void WebSocketFrame_ParseAsync_m3960604397 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___stream0, Action_1_t1174010442 * ___completed1, Action_1_t92447661 * ___error2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocketFrame::ParseAsync(System.IO.Stream,System.Boolean,System.Action`1<WebSocketSharp.WebSocketFrame>,System.Action`1<System.Exception>)
extern "C"  void WebSocketFrame_ParseAsync_m1471908516 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___stream0, bool ___unmask1, Action_1_t1174010442 * ___completed2, Action_1_t92447661 * ___error3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocketFrame::Print(System.Boolean)
extern "C"  void WebSocketFrame_Print_m2623923964 (WebSocketFrame_t778194306 * __this, bool ___dumped0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.WebSocketFrame::PrintToString(System.Boolean)
extern "C"  String_t* WebSocketFrame_PrintToString_m2057514283 (WebSocketFrame_t778194306 * __this, bool ___dumped0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] WebSocketSharp.WebSocketFrame::ToByteArray()
extern "C"  ByteU5BU5D_t4260760469* WebSocketFrame_ToByteArray_m3468314104 (WebSocketFrame_t778194306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.WebSocketFrame::ToString()
extern "C"  String_t* WebSocketFrame_ToString_m2245624947 (WebSocketFrame_t778194306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
