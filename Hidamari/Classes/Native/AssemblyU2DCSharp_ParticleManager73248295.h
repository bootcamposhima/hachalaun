﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen3698390444.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ParticleManager
struct  ParticleManager_t73248295  : public SingletonMonoBehaviour_1_t3698390444
{
public:
	// UnityEngine.GameObject ParticleManager::p_sortiePar
	GameObject_t3674682005 * ___p_sortiePar_3;
	// UnityEngine.GameObject ParticleManager::p_admissionPar
	GameObject_t3674682005 * ___p_admissionPar_4;
	// UnityEngine.GameObject ParticleManager::p_lossPar
	GameObject_t3674682005 * ___p_lossPar_5;
	// UnityEngine.GameObject ParticleManager::p_winPar
	GameObject_t3674682005 * ___p_winPar_6;
	// UnityEngine.GameObject ParticleManager::p_changePar
	GameObject_t3674682005 * ___p_changePar_7;
	// UnityEngine.GameObject ParticleManager::p_damegePar
	GameObject_t3674682005 * ___p_damegePar_8;
	// UnityEngine.GameObject ParticleManager::p_bloodPar
	GameObject_t3674682005 * ___p_bloodPar_9;
	// UnityEngine.GameObject ParticleManager::p_shotPar
	GameObject_t3674682005 * ___p_shotPar_10;
	// UnityEngine.GameObject ParticleManager::p_explosionsPar
	GameObject_t3674682005 * ___p_explosionsPar_11;

public:
	inline static int32_t get_offset_of_p_sortiePar_3() { return static_cast<int32_t>(offsetof(ParticleManager_t73248295, ___p_sortiePar_3)); }
	inline GameObject_t3674682005 * get_p_sortiePar_3() const { return ___p_sortiePar_3; }
	inline GameObject_t3674682005 ** get_address_of_p_sortiePar_3() { return &___p_sortiePar_3; }
	inline void set_p_sortiePar_3(GameObject_t3674682005 * value)
	{
		___p_sortiePar_3 = value;
		Il2CppCodeGenWriteBarrier(&___p_sortiePar_3, value);
	}

	inline static int32_t get_offset_of_p_admissionPar_4() { return static_cast<int32_t>(offsetof(ParticleManager_t73248295, ___p_admissionPar_4)); }
	inline GameObject_t3674682005 * get_p_admissionPar_4() const { return ___p_admissionPar_4; }
	inline GameObject_t3674682005 ** get_address_of_p_admissionPar_4() { return &___p_admissionPar_4; }
	inline void set_p_admissionPar_4(GameObject_t3674682005 * value)
	{
		___p_admissionPar_4 = value;
		Il2CppCodeGenWriteBarrier(&___p_admissionPar_4, value);
	}

	inline static int32_t get_offset_of_p_lossPar_5() { return static_cast<int32_t>(offsetof(ParticleManager_t73248295, ___p_lossPar_5)); }
	inline GameObject_t3674682005 * get_p_lossPar_5() const { return ___p_lossPar_5; }
	inline GameObject_t3674682005 ** get_address_of_p_lossPar_5() { return &___p_lossPar_5; }
	inline void set_p_lossPar_5(GameObject_t3674682005 * value)
	{
		___p_lossPar_5 = value;
		Il2CppCodeGenWriteBarrier(&___p_lossPar_5, value);
	}

	inline static int32_t get_offset_of_p_winPar_6() { return static_cast<int32_t>(offsetof(ParticleManager_t73248295, ___p_winPar_6)); }
	inline GameObject_t3674682005 * get_p_winPar_6() const { return ___p_winPar_6; }
	inline GameObject_t3674682005 ** get_address_of_p_winPar_6() { return &___p_winPar_6; }
	inline void set_p_winPar_6(GameObject_t3674682005 * value)
	{
		___p_winPar_6 = value;
		Il2CppCodeGenWriteBarrier(&___p_winPar_6, value);
	}

	inline static int32_t get_offset_of_p_changePar_7() { return static_cast<int32_t>(offsetof(ParticleManager_t73248295, ___p_changePar_7)); }
	inline GameObject_t3674682005 * get_p_changePar_7() const { return ___p_changePar_7; }
	inline GameObject_t3674682005 ** get_address_of_p_changePar_7() { return &___p_changePar_7; }
	inline void set_p_changePar_7(GameObject_t3674682005 * value)
	{
		___p_changePar_7 = value;
		Il2CppCodeGenWriteBarrier(&___p_changePar_7, value);
	}

	inline static int32_t get_offset_of_p_damegePar_8() { return static_cast<int32_t>(offsetof(ParticleManager_t73248295, ___p_damegePar_8)); }
	inline GameObject_t3674682005 * get_p_damegePar_8() const { return ___p_damegePar_8; }
	inline GameObject_t3674682005 ** get_address_of_p_damegePar_8() { return &___p_damegePar_8; }
	inline void set_p_damegePar_8(GameObject_t3674682005 * value)
	{
		___p_damegePar_8 = value;
		Il2CppCodeGenWriteBarrier(&___p_damegePar_8, value);
	}

	inline static int32_t get_offset_of_p_bloodPar_9() { return static_cast<int32_t>(offsetof(ParticleManager_t73248295, ___p_bloodPar_9)); }
	inline GameObject_t3674682005 * get_p_bloodPar_9() const { return ___p_bloodPar_9; }
	inline GameObject_t3674682005 ** get_address_of_p_bloodPar_9() { return &___p_bloodPar_9; }
	inline void set_p_bloodPar_9(GameObject_t3674682005 * value)
	{
		___p_bloodPar_9 = value;
		Il2CppCodeGenWriteBarrier(&___p_bloodPar_9, value);
	}

	inline static int32_t get_offset_of_p_shotPar_10() { return static_cast<int32_t>(offsetof(ParticleManager_t73248295, ___p_shotPar_10)); }
	inline GameObject_t3674682005 * get_p_shotPar_10() const { return ___p_shotPar_10; }
	inline GameObject_t3674682005 ** get_address_of_p_shotPar_10() { return &___p_shotPar_10; }
	inline void set_p_shotPar_10(GameObject_t3674682005 * value)
	{
		___p_shotPar_10 = value;
		Il2CppCodeGenWriteBarrier(&___p_shotPar_10, value);
	}

	inline static int32_t get_offset_of_p_explosionsPar_11() { return static_cast<int32_t>(offsetof(ParticleManager_t73248295, ___p_explosionsPar_11)); }
	inline GameObject_t3674682005 * get_p_explosionsPar_11() const { return ___p_explosionsPar_11; }
	inline GameObject_t3674682005 ** get_address_of_p_explosionsPar_11() { return &___p_explosionsPar_11; }
	inline void set_p_explosionsPar_11(GameObject_t3674682005 * value)
	{
		___p_explosionsPar_11 = value;
		Il2CppCodeGenWriteBarrier(&___p_explosionsPar_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
