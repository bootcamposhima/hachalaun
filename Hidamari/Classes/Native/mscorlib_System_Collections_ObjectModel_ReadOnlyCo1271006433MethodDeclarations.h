﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<AIDatas>
struct ReadOnlyCollection_1_t1271006433;
// System.Collections.Generic.IList`1<AIDatas>
struct IList_1_t2408576100;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// AIDatas[]
struct AIDatasU5BU5D_t2487978268;
// System.Collections.Generic.IEnumerator`1<AIDatas>
struct IEnumerator_1_t1625793946;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AIDatas4008896193.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AIDatas>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m1883613071_gshared (ReadOnlyCollection_1_t1271006433 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m1883613071(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t1271006433 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1883613071_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AIDatas>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m4108155385_gshared (ReadOnlyCollection_1_t1271006433 * __this, AIDatas_t4008896193  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m4108155385(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t1271006433 *, AIDatas_t4008896193 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m4108155385_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AIDatas>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m510489073_gshared (ReadOnlyCollection_1_t1271006433 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m510489073(__this, method) ((  void (*) (ReadOnlyCollection_1_t1271006433 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m510489073_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AIDatas>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3507001760_gshared (ReadOnlyCollection_1_t1271006433 * __this, int32_t ___index0, AIDatas_t4008896193  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3507001760(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t1271006433 *, int32_t, AIDatas_t4008896193 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3507001760_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AIDatas>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1390981082_gshared (ReadOnlyCollection_1_t1271006433 * __this, AIDatas_t4008896193  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1390981082(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t1271006433 *, AIDatas_t4008896193 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1390981082_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AIDatas>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1380854630_gshared (ReadOnlyCollection_1_t1271006433 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1380854630(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1271006433 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1380854630_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<AIDatas>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  AIDatas_t4008896193  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m259676906_gshared (ReadOnlyCollection_1_t1271006433 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m259676906(__this, ___index0, method) ((  AIDatas_t4008896193  (*) (ReadOnlyCollection_1_t1271006433 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m259676906_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AIDatas>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m488960631_gshared (ReadOnlyCollection_1_t1271006433 * __this, int32_t ___index0, AIDatas_t4008896193  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m488960631(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1271006433 *, int32_t, AIDatas_t4008896193 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m488960631_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AIDatas>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3900466293_gshared (ReadOnlyCollection_1_t1271006433 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3900466293(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1271006433 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3900466293_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AIDatas>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2308906174_gshared (ReadOnlyCollection_1_t1271006433 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2308906174(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1271006433 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2308906174_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<AIDatas>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m986289209_gshared (ReadOnlyCollection_1_t1271006433 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m986289209(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1271006433 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m986289209_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<AIDatas>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m1817796696_gshared (ReadOnlyCollection_1_t1271006433 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1817796696(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1271006433 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1817796696_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AIDatas>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m658416588_gshared (ReadOnlyCollection_1_t1271006433 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m658416588(__this, method) ((  void (*) (ReadOnlyCollection_1_t1271006433 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m658416588_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AIDatas>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m3406673268_gshared (ReadOnlyCollection_1_t1271006433 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m3406673268(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1271006433 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m3406673268_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<AIDatas>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2495720112_gshared (ReadOnlyCollection_1_t1271006433 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2495720112(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1271006433 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2495720112_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AIDatas>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m3152832731_gshared (ReadOnlyCollection_1_t1271006433 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m3152832731(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1271006433 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m3152832731_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AIDatas>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m1070004333_gshared (ReadOnlyCollection_1_t1271006433 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1070004333(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t1271006433 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m1070004333_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AIDatas>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m395751659_gshared (ReadOnlyCollection_1_t1271006433 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m395751659(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1271006433 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m395751659_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AIDatas>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2416065896_gshared (ReadOnlyCollection_1_t1271006433 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2416065896(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1271006433 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2416065896_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<AIDatas>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m718669268_gshared (ReadOnlyCollection_1_t1271006433 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m718669268(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1271006433 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m718669268_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AIDatas>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1024953315_gshared (ReadOnlyCollection_1_t1271006433 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1024953315(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1271006433 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1024953315_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AIDatas>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3258018486_gshared (ReadOnlyCollection_1_t1271006433 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3258018486(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1271006433 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3258018486_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<AIDatas>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m217043547_gshared (ReadOnlyCollection_1_t1271006433 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m217043547(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1271006433 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m217043547_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AIDatas>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m2534263282_gshared (ReadOnlyCollection_1_t1271006433 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m2534263282(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1271006433 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m2534263282_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<AIDatas>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m1298702111_gshared (ReadOnlyCollection_1_t1271006433 * __this, AIDatas_t4008896193  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m1298702111(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1271006433 *, AIDatas_t4008896193 , const MethodInfo*))ReadOnlyCollection_1_Contains_m1298702111_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<AIDatas>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m1347807273_gshared (ReadOnlyCollection_1_t1271006433 * __this, AIDatasU5BU5D_t2487978268* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m1347807273(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1271006433 *, AIDatasU5BU5D_t2487978268*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m1347807273_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<AIDatas>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m34950402_gshared (ReadOnlyCollection_1_t1271006433 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m34950402(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t1271006433 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m34950402_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<AIDatas>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m4126979949_gshared (ReadOnlyCollection_1_t1271006433 * __this, AIDatas_t4008896193  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m4126979949(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1271006433 *, AIDatas_t4008896193 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m4126979949_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<AIDatas>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m3199634350_gshared (ReadOnlyCollection_1_t1271006433 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m3199634350(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t1271006433 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m3199634350_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<AIDatas>::get_Item(System.Int32)
extern "C"  AIDatas_t4008896193  ReadOnlyCollection_1_get_Item_m309668266_gshared (ReadOnlyCollection_1_t1271006433 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m309668266(__this, ___index0, method) ((  AIDatas_t4008896193  (*) (ReadOnlyCollection_1_t1271006433 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m309668266_gshared)(__this, ___index0, method)
