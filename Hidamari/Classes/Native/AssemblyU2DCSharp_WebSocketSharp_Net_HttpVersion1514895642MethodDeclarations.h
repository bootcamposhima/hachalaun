﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.Net.HttpVersion
struct HttpVersion_t1514895642;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocketSharp.Net.HttpVersion::.ctor()
extern "C"  void HttpVersion__ctor_m1159859847 (HttpVersion_t1514895642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.HttpVersion::.cctor()
extern "C"  void HttpVersion__cctor_m1113820678 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
