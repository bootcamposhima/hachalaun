﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Threading.Thread
struct Thread_t1973216770;
// WebSocketSharp.WebSocket
struct WebSocket_t1342580397;
// SocketIO.Encoder
struct Encoder_t3278716938;
// SocketIO.Decoder
struct Decoder_t2133550898;
// SocketIO.Parser
struct Parser_t1660565463;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>>
struct Dictionary_2_t2301306825;
// System.Collections.Generic.List`1<SocketIO.Ack>
struct List_1_t1313487815;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.Queue`1<SocketIO.SocketIOEvent>
struct Queue_1_t1953129196;
// System.Collections.Generic.Queue`1<SocketIO.Packet>
struct Queue_1_t3896353341;

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen2425651671.h"
#include "mscorlib_System_Boolean476798718.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SocketIO.SocketIOComponent
struct  SocketIOComponent_t3095476818  : public SingletonMonoBehaviour_1_t2425651671
{
public:
	// System.String SocketIO.SocketIOComponent::url
	String_t* ___url_3;
	// System.Boolean SocketIO.SocketIOComponent::autoConnect
	bool ___autoConnect_4;
	// System.Int32 SocketIO.SocketIOComponent::reconnectDelay
	int32_t ___reconnectDelay_5;
	// System.Single SocketIO.SocketIOComponent::ackExpirationTime
	float ___ackExpirationTime_6;
	// System.Single SocketIO.SocketIOComponent::pingInterval
	float ___pingInterval_7;
	// System.Single SocketIO.SocketIOComponent::pingTimeout
	float ___pingTimeout_8;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) SocketIO.SocketIOComponent::connected
	bool ___connected_9;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) SocketIO.SocketIOComponent::thPinging
	bool ___thPinging_10;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) SocketIO.SocketIOComponent::thPong
	bool ___thPong_11;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) SocketIO.SocketIOComponent::wsConnected
	bool ___wsConnected_12;
	// System.Threading.Thread SocketIO.SocketIOComponent::socketThread
	Thread_t1973216770 * ___socketThread_13;
	// System.Threading.Thread SocketIO.SocketIOComponent::pingThread
	Thread_t1973216770 * ___pingThread_14;
	// WebSocketSharp.WebSocket SocketIO.SocketIOComponent::ws
	WebSocket_t1342580397 * ___ws_15;
	// SocketIO.Encoder SocketIO.SocketIOComponent::encoder
	Encoder_t3278716938 * ___encoder_16;
	// SocketIO.Decoder SocketIO.SocketIOComponent::decoder
	Decoder_t2133550898 * ___decoder_17;
	// SocketIO.Parser SocketIO.SocketIOComponent::parser
	Parser_t1660565463 * ___parser_18;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Action`1<SocketIO.SocketIOEvent>>> SocketIO.SocketIOComponent::handlers
	Dictionary_2_t2301306825 * ___handlers_19;
	// System.Collections.Generic.List`1<SocketIO.Ack> SocketIO.SocketIOComponent::ackList
	List_1_t1313487815 * ___ackList_20;
	// System.Int32 SocketIO.SocketIOComponent::packetId
	int32_t ___packetId_21;
	// System.Object SocketIO.SocketIOComponent::eventQueueLock
	Il2CppObject * ___eventQueueLock_22;
	// System.Collections.Generic.Queue`1<SocketIO.SocketIOEvent> SocketIO.SocketIOComponent::eventQueue
	Queue_1_t1953129196 * ___eventQueue_23;
	// System.Object SocketIO.SocketIOComponent::ackQueueLock
	Il2CppObject * ___ackQueueLock_24;
	// System.Collections.Generic.Queue`1<SocketIO.Packet> SocketIO.SocketIOComponent::ackQueue
	Queue_1_t3896353341 * ___ackQueue_25;
	// System.String SocketIO.SocketIOComponent::<sid>k__BackingField
	String_t* ___U3CsidU3Ek__BackingField_26;

public:
	inline static int32_t get_offset_of_url_3() { return static_cast<int32_t>(offsetof(SocketIOComponent_t3095476818, ___url_3)); }
	inline String_t* get_url_3() const { return ___url_3; }
	inline String_t** get_address_of_url_3() { return &___url_3; }
	inline void set_url_3(String_t* value)
	{
		___url_3 = value;
		Il2CppCodeGenWriteBarrier(&___url_3, value);
	}

	inline static int32_t get_offset_of_autoConnect_4() { return static_cast<int32_t>(offsetof(SocketIOComponent_t3095476818, ___autoConnect_4)); }
	inline bool get_autoConnect_4() const { return ___autoConnect_4; }
	inline bool* get_address_of_autoConnect_4() { return &___autoConnect_4; }
	inline void set_autoConnect_4(bool value)
	{
		___autoConnect_4 = value;
	}

	inline static int32_t get_offset_of_reconnectDelay_5() { return static_cast<int32_t>(offsetof(SocketIOComponent_t3095476818, ___reconnectDelay_5)); }
	inline int32_t get_reconnectDelay_5() const { return ___reconnectDelay_5; }
	inline int32_t* get_address_of_reconnectDelay_5() { return &___reconnectDelay_5; }
	inline void set_reconnectDelay_5(int32_t value)
	{
		___reconnectDelay_5 = value;
	}

	inline static int32_t get_offset_of_ackExpirationTime_6() { return static_cast<int32_t>(offsetof(SocketIOComponent_t3095476818, ___ackExpirationTime_6)); }
	inline float get_ackExpirationTime_6() const { return ___ackExpirationTime_6; }
	inline float* get_address_of_ackExpirationTime_6() { return &___ackExpirationTime_6; }
	inline void set_ackExpirationTime_6(float value)
	{
		___ackExpirationTime_6 = value;
	}

	inline static int32_t get_offset_of_pingInterval_7() { return static_cast<int32_t>(offsetof(SocketIOComponent_t3095476818, ___pingInterval_7)); }
	inline float get_pingInterval_7() const { return ___pingInterval_7; }
	inline float* get_address_of_pingInterval_7() { return &___pingInterval_7; }
	inline void set_pingInterval_7(float value)
	{
		___pingInterval_7 = value;
	}

	inline static int32_t get_offset_of_pingTimeout_8() { return static_cast<int32_t>(offsetof(SocketIOComponent_t3095476818, ___pingTimeout_8)); }
	inline float get_pingTimeout_8() const { return ___pingTimeout_8; }
	inline float* get_address_of_pingTimeout_8() { return &___pingTimeout_8; }
	inline void set_pingTimeout_8(float value)
	{
		___pingTimeout_8 = value;
	}

	inline static int32_t get_offset_of_connected_9() { return static_cast<int32_t>(offsetof(SocketIOComponent_t3095476818, ___connected_9)); }
	inline bool get_connected_9() const { return ___connected_9; }
	inline bool* get_address_of_connected_9() { return &___connected_9; }
	inline void set_connected_9(bool value)
	{
		___connected_9 = value;
	}

	inline static int32_t get_offset_of_thPinging_10() { return static_cast<int32_t>(offsetof(SocketIOComponent_t3095476818, ___thPinging_10)); }
	inline bool get_thPinging_10() const { return ___thPinging_10; }
	inline bool* get_address_of_thPinging_10() { return &___thPinging_10; }
	inline void set_thPinging_10(bool value)
	{
		___thPinging_10 = value;
	}

	inline static int32_t get_offset_of_thPong_11() { return static_cast<int32_t>(offsetof(SocketIOComponent_t3095476818, ___thPong_11)); }
	inline bool get_thPong_11() const { return ___thPong_11; }
	inline bool* get_address_of_thPong_11() { return &___thPong_11; }
	inline void set_thPong_11(bool value)
	{
		___thPong_11 = value;
	}

	inline static int32_t get_offset_of_wsConnected_12() { return static_cast<int32_t>(offsetof(SocketIOComponent_t3095476818, ___wsConnected_12)); }
	inline bool get_wsConnected_12() const { return ___wsConnected_12; }
	inline bool* get_address_of_wsConnected_12() { return &___wsConnected_12; }
	inline void set_wsConnected_12(bool value)
	{
		___wsConnected_12 = value;
	}

	inline static int32_t get_offset_of_socketThread_13() { return static_cast<int32_t>(offsetof(SocketIOComponent_t3095476818, ___socketThread_13)); }
	inline Thread_t1973216770 * get_socketThread_13() const { return ___socketThread_13; }
	inline Thread_t1973216770 ** get_address_of_socketThread_13() { return &___socketThread_13; }
	inline void set_socketThread_13(Thread_t1973216770 * value)
	{
		___socketThread_13 = value;
		Il2CppCodeGenWriteBarrier(&___socketThread_13, value);
	}

	inline static int32_t get_offset_of_pingThread_14() { return static_cast<int32_t>(offsetof(SocketIOComponent_t3095476818, ___pingThread_14)); }
	inline Thread_t1973216770 * get_pingThread_14() const { return ___pingThread_14; }
	inline Thread_t1973216770 ** get_address_of_pingThread_14() { return &___pingThread_14; }
	inline void set_pingThread_14(Thread_t1973216770 * value)
	{
		___pingThread_14 = value;
		Il2CppCodeGenWriteBarrier(&___pingThread_14, value);
	}

	inline static int32_t get_offset_of_ws_15() { return static_cast<int32_t>(offsetof(SocketIOComponent_t3095476818, ___ws_15)); }
	inline WebSocket_t1342580397 * get_ws_15() const { return ___ws_15; }
	inline WebSocket_t1342580397 ** get_address_of_ws_15() { return &___ws_15; }
	inline void set_ws_15(WebSocket_t1342580397 * value)
	{
		___ws_15 = value;
		Il2CppCodeGenWriteBarrier(&___ws_15, value);
	}

	inline static int32_t get_offset_of_encoder_16() { return static_cast<int32_t>(offsetof(SocketIOComponent_t3095476818, ___encoder_16)); }
	inline Encoder_t3278716938 * get_encoder_16() const { return ___encoder_16; }
	inline Encoder_t3278716938 ** get_address_of_encoder_16() { return &___encoder_16; }
	inline void set_encoder_16(Encoder_t3278716938 * value)
	{
		___encoder_16 = value;
		Il2CppCodeGenWriteBarrier(&___encoder_16, value);
	}

	inline static int32_t get_offset_of_decoder_17() { return static_cast<int32_t>(offsetof(SocketIOComponent_t3095476818, ___decoder_17)); }
	inline Decoder_t2133550898 * get_decoder_17() const { return ___decoder_17; }
	inline Decoder_t2133550898 ** get_address_of_decoder_17() { return &___decoder_17; }
	inline void set_decoder_17(Decoder_t2133550898 * value)
	{
		___decoder_17 = value;
		Il2CppCodeGenWriteBarrier(&___decoder_17, value);
	}

	inline static int32_t get_offset_of_parser_18() { return static_cast<int32_t>(offsetof(SocketIOComponent_t3095476818, ___parser_18)); }
	inline Parser_t1660565463 * get_parser_18() const { return ___parser_18; }
	inline Parser_t1660565463 ** get_address_of_parser_18() { return &___parser_18; }
	inline void set_parser_18(Parser_t1660565463 * value)
	{
		___parser_18 = value;
		Il2CppCodeGenWriteBarrier(&___parser_18, value);
	}

	inline static int32_t get_offset_of_handlers_19() { return static_cast<int32_t>(offsetof(SocketIOComponent_t3095476818, ___handlers_19)); }
	inline Dictionary_2_t2301306825 * get_handlers_19() const { return ___handlers_19; }
	inline Dictionary_2_t2301306825 ** get_address_of_handlers_19() { return &___handlers_19; }
	inline void set_handlers_19(Dictionary_2_t2301306825 * value)
	{
		___handlers_19 = value;
		Il2CppCodeGenWriteBarrier(&___handlers_19, value);
	}

	inline static int32_t get_offset_of_ackList_20() { return static_cast<int32_t>(offsetof(SocketIOComponent_t3095476818, ___ackList_20)); }
	inline List_1_t1313487815 * get_ackList_20() const { return ___ackList_20; }
	inline List_1_t1313487815 ** get_address_of_ackList_20() { return &___ackList_20; }
	inline void set_ackList_20(List_1_t1313487815 * value)
	{
		___ackList_20 = value;
		Il2CppCodeGenWriteBarrier(&___ackList_20, value);
	}

	inline static int32_t get_offset_of_packetId_21() { return static_cast<int32_t>(offsetof(SocketIOComponent_t3095476818, ___packetId_21)); }
	inline int32_t get_packetId_21() const { return ___packetId_21; }
	inline int32_t* get_address_of_packetId_21() { return &___packetId_21; }
	inline void set_packetId_21(int32_t value)
	{
		___packetId_21 = value;
	}

	inline static int32_t get_offset_of_eventQueueLock_22() { return static_cast<int32_t>(offsetof(SocketIOComponent_t3095476818, ___eventQueueLock_22)); }
	inline Il2CppObject * get_eventQueueLock_22() const { return ___eventQueueLock_22; }
	inline Il2CppObject ** get_address_of_eventQueueLock_22() { return &___eventQueueLock_22; }
	inline void set_eventQueueLock_22(Il2CppObject * value)
	{
		___eventQueueLock_22 = value;
		Il2CppCodeGenWriteBarrier(&___eventQueueLock_22, value);
	}

	inline static int32_t get_offset_of_eventQueue_23() { return static_cast<int32_t>(offsetof(SocketIOComponent_t3095476818, ___eventQueue_23)); }
	inline Queue_1_t1953129196 * get_eventQueue_23() const { return ___eventQueue_23; }
	inline Queue_1_t1953129196 ** get_address_of_eventQueue_23() { return &___eventQueue_23; }
	inline void set_eventQueue_23(Queue_1_t1953129196 * value)
	{
		___eventQueue_23 = value;
		Il2CppCodeGenWriteBarrier(&___eventQueue_23, value);
	}

	inline static int32_t get_offset_of_ackQueueLock_24() { return static_cast<int32_t>(offsetof(SocketIOComponent_t3095476818, ___ackQueueLock_24)); }
	inline Il2CppObject * get_ackQueueLock_24() const { return ___ackQueueLock_24; }
	inline Il2CppObject ** get_address_of_ackQueueLock_24() { return &___ackQueueLock_24; }
	inline void set_ackQueueLock_24(Il2CppObject * value)
	{
		___ackQueueLock_24 = value;
		Il2CppCodeGenWriteBarrier(&___ackQueueLock_24, value);
	}

	inline static int32_t get_offset_of_ackQueue_25() { return static_cast<int32_t>(offsetof(SocketIOComponent_t3095476818, ___ackQueue_25)); }
	inline Queue_1_t3896353341 * get_ackQueue_25() const { return ___ackQueue_25; }
	inline Queue_1_t3896353341 ** get_address_of_ackQueue_25() { return &___ackQueue_25; }
	inline void set_ackQueue_25(Queue_1_t3896353341 * value)
	{
		___ackQueue_25 = value;
		Il2CppCodeGenWriteBarrier(&___ackQueue_25, value);
	}

	inline static int32_t get_offset_of_U3CsidU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(SocketIOComponent_t3095476818, ___U3CsidU3Ek__BackingField_26)); }
	inline String_t* get_U3CsidU3Ek__BackingField_26() const { return ___U3CsidU3Ek__BackingField_26; }
	inline String_t** get_address_of_U3CsidU3Ek__BackingField_26() { return &___U3CsidU3Ek__BackingField_26; }
	inline void set_U3CsidU3Ek__BackingField_26(String_t* value)
	{
		___U3CsidU3Ek__BackingField_26 = value;
		Il2CppCodeGenWriteBarrier(&___U3CsidU3Ek__BackingField_26, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
