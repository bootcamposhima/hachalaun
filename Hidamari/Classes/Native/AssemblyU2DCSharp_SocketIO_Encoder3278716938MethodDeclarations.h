﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SocketIO.Encoder
struct Encoder_t3278716938;
// System.String
struct String_t;
// SocketIO.Packet
struct Packet_t1660110912;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SocketIO_Packet1660110912.h"

// System.Void SocketIO.Encoder::.ctor()
extern "C"  void Encoder__ctor_m1661836916 (Encoder_t3278716938 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SocketIO.Encoder::Encode(SocketIO.Packet)
extern "C"  String_t* Encoder_Encode_m2271774252 (Encoder_t3278716938 * __this, Packet_t1660110912 * ___packet0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
