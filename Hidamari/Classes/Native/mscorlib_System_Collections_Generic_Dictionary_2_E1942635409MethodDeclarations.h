﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3363211663MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.ListenerPrefix,WebSocketSharp.Net.HttpListener>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1203493407(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1942635409 *, Dictionary_2_t625312017 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.ListenerPrefix,WebSocketSharp.Net.HttpListener>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3201164834(__this, method) ((  Il2CppObject * (*) (Enumerator_t1942635409 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.ListenerPrefix,WebSocketSharp.Net.HttpListener>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3138830646(__this, method) ((  void (*) (Enumerator_t1942635409 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.ListenerPrefix,WebSocketSharp.Net.HttpListener>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4112668287(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t1942635409 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.ListenerPrefix,WebSocketSharp.Net.HttpListener>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1584169342(__this, method) ((  Il2CppObject * (*) (Enumerator_t1942635409 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.ListenerPrefix,WebSocketSharp.Net.HttpListener>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3016751248(__this, method) ((  Il2CppObject * (*) (Enumerator_t1942635409 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.ListenerPrefix,WebSocketSharp.Net.HttpListener>::MoveNext()
#define Enumerator_MoveNext_m3058767010(__this, method) ((  bool (*) (Enumerator_t1942635409 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.ListenerPrefix,WebSocketSharp.Net.HttpListener>::get_Current()
#define Enumerator_get_Current_m423067342(__this, method) ((  KeyValuePair_2_t524092723  (*) (Enumerator_t1942635409 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.ListenerPrefix,WebSocketSharp.Net.HttpListener>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3759775087(__this, method) ((  ListenerPrefix_t2663314696 * (*) (Enumerator_t1942635409 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.ListenerPrefix,WebSocketSharp.Net.HttpListener>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2289239251(__this, method) ((  HttpListener_t398944510 * (*) (Enumerator_t1942635409 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.ListenerPrefix,WebSocketSharp.Net.HttpListener>::Reset()
#define Enumerator_Reset_m2929964529(__this, method) ((  void (*) (Enumerator_t1942635409 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.ListenerPrefix,WebSocketSharp.Net.HttpListener>::VerifyState()
#define Enumerator_VerifyState_m2937654586(__this, method) ((  void (*) (Enumerator_t1942635409 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.ListenerPrefix,WebSocketSharp.Net.HttpListener>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m4037415458(__this, method) ((  void (*) (Enumerator_t1942635409 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.Net.ListenerPrefix,WebSocketSharp.Net.HttpListener>::Dispose()
#define Enumerator_Dispose_m720795265(__this, method) ((  void (*) (Enumerator_t1942635409 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
