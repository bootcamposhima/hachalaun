﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.WebSocketStream/<readHandshakeHeaders>c__AnonStorey32
struct U3CreadHandshakeHeadersU3Ec__AnonStorey32_t3526000246;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocketSharp.WebSocketStream/<readHandshakeHeaders>c__AnonStorey32::.ctor()
extern "C"  void U3CreadHandshakeHeadersU3Ec__AnonStorey32__ctor_m1526739173 (U3CreadHandshakeHeadersU3Ec__AnonStorey32_t3526000246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocketStream/<readHandshakeHeaders>c__AnonStorey32::<>m__1C(System.Int32)
extern "C"  void U3CreadHandshakeHeadersU3Ec__AnonStorey32_U3CU3Em__1C_m2472820081 (U3CreadHandshakeHeadersU3Ec__AnonStorey32_t3526000246 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
