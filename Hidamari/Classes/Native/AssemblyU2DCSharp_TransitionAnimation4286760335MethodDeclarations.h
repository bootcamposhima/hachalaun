﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TransitionAnimation
struct TransitionAnimation_t4286760335;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void TransitionAnimation::.ctor()
extern "C"  void TransitionAnimation__ctor_m3991871164 (TransitionAnimation_t4286760335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransitionAnimation::Start()
extern "C"  void TransitionAnimation_Start_m2939008956 (TransitionAnimation_t4286760335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransitionAnimation::Update()
extern "C"  void TransitionAnimation_Update_m920816593 (TransitionAnimation_t4286760335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransitionAnimation::Awake()
extern "C"  void TransitionAnimation_Awake_m4229476383 (TransitionAnimation_t4286760335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransitionAnimation::OnGUI()
extern "C"  void TransitionAnimation_OnGUI_m3487269814 (TransitionAnimation_t4286760335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransitionAnimation::LoadScene(System.String,System.Single)
extern "C"  void TransitionAnimation_LoadScene_m1656869191 (TransitionAnimation_t4286760335 * __this, String_t* ___scene0, float ___interval1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TransitionAnimation::SceneAnimation(System.String,System.Single)
extern "C"  Il2CppObject * TransitionAnimation_SceneAnimation_m2690032655 (TransitionAnimation_t4286760335 * __this, String_t* ___scene0, float ___interval1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TransitionAnimation::Fadein(System.Single)
extern "C"  Il2CppObject * TransitionAnimation_Fadein_m99348202 (TransitionAnimation_t4286760335 * __this, float ___interval0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TransitionAnimation::Fadeout(System.Single)
extern "C"  Il2CppObject * TransitionAnimation_Fadeout_m1811929463 (TransitionAnimation_t4286760335 * __this, float ___interval0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TransitionAnimation::FadeAnimation(UnityEngine.GameObject,System.Single)
extern "C"  Il2CppObject * TransitionAnimation_FadeAnimation_m3241219815 (TransitionAnimation_t4286760335 * __this, GameObject_t3674682005 * ___gb0, float ___interval1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransitionAnimation::StartScaleAnimation(UnityEngine.GameObject,System.Single,System.Single,System.Single)
extern "C"  void TransitionAnimation_StartScaleAnimation_m3061689181 (TransitionAnimation_t4286760335 * __this, GameObject_t3674682005 * ___g0, float ___scale1, float ___defoult2, float ___speed3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransitionAnimation::StopScaleAnimation(UnityEngine.GameObject)
extern "C"  void TransitionAnimation_StopScaleAnimation_m2782089788 (TransitionAnimation_t4286760335 * __this, GameObject_t3674682005 * ___g0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransitionAnimation::StopAllScaleAnimations()
extern "C"  void TransitionAnimation_StopAllScaleAnimations_m3861883618 (TransitionAnimation_t4286760335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TransitionAnimation::ScaleAction(UnityEngine.GameObject,System.Single,System.Single,System.Single)
extern "C"  Il2CppObject * TransitionAnimation_ScaleAction_m3442970985 (TransitionAnimation_t4286760335 * __this, GameObject_t3674682005 * ___g0, float ___defoult1, float ___nextscale2, float ___speed3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransitionAnimation::AllScaleAnimationClear()
extern "C"  void TransitionAnimation_AllScaleAnimationClear_m2768689274 (TransitionAnimation_t4286760335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransitionAnimation::AddScaleAnimationGameObject(UnityEngine.GameObject)
extern "C"  void TransitionAnimation_AddScaleAnimationGameObject_m2894456734 (TransitionAnimation_t4286760335 * __this, GameObject_t3674682005 * ___g0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransitionAnimation::RemoveScaleAnimationGameObject(UnityEngine.GameObject)
extern "C"  void TransitionAnimation_RemoveScaleAnimationGameObject_m367235119 (TransitionAnimation_t4286760335 * __this, GameObject_t3674682005 * ___g0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TransitionAnimation::isAnimation()
extern "C"  bool TransitionAnimation_isAnimation_m2178495976 (TransitionAnimation_t4286760335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransitionAnimation::StartAllScaleAnimation(System.Single,System.Single,System.Single)
extern "C"  void TransitionAnimation_StartAllScaleAnimation_m1148707636 (TransitionAnimation_t4286760335 * __this, float ___defoult0, float ___nextscale1, float ___speed2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransitionAnimation::StopAllScaleAnimation()
extern "C"  void TransitionAnimation_StopAllScaleAnimation_m1648595219 (TransitionAnimation_t4286760335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TransitionAnimation::AllScaleAction(System.Single,System.Single,System.Single)
extern "C"  Il2CppObject * TransitionAnimation_AllScaleAction_m4054697494 (TransitionAnimation_t4286760335 * __this, float ___defoult0, float ___nextscale1, float ___speed2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
