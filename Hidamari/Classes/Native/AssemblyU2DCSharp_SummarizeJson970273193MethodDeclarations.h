﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SummarizeJson
struct SummarizeJson_t970273193;
struct SummarizeJson_t970273193_marshaled_pinvoke;
struct SummarizeJson_t970273193_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SummarizeJson970273193.h"
#include "AssemblyU2DCSharp_FightJson649175288.h"
#include "AssemblyU2DCSharp_PlayerJson4089020297.h"

// System.Void SummarizeJson::.ctor(System.Int32,FightJson)
extern "C"  void SummarizeJson__ctor_m3286377191 (SummarizeJson_t970273193 * __this, int32_t ___n0, FightJson_t649175288  ___j1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SummarizeJson::.ctor(System.Int32,PlayerJson)
extern "C"  void SummarizeJson__ctor_m4211827658 (SummarizeJson_t970273193 * __this, int32_t ___n0, PlayerJson_t4089020297  ___j1, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct SummarizeJson_t970273193;
struct SummarizeJson_t970273193_marshaled_pinvoke;

extern "C" void SummarizeJson_t970273193_marshal_pinvoke(const SummarizeJson_t970273193& unmarshaled, SummarizeJson_t970273193_marshaled_pinvoke& marshaled);
extern "C" void SummarizeJson_t970273193_marshal_pinvoke_back(const SummarizeJson_t970273193_marshaled_pinvoke& marshaled, SummarizeJson_t970273193& unmarshaled);
extern "C" void SummarizeJson_t970273193_marshal_pinvoke_cleanup(SummarizeJson_t970273193_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct SummarizeJson_t970273193;
struct SummarizeJson_t970273193_marshaled_com;

extern "C" void SummarizeJson_t970273193_marshal_com(const SummarizeJson_t970273193& unmarshaled, SummarizeJson_t970273193_marshaled_com& marshaled);
extern "C" void SummarizeJson_t970273193_marshal_com_back(const SummarizeJson_t970273193_marshaled_com& marshaled, SummarizeJson_t970273193& unmarshaled);
extern "C" void SummarizeJson_t970273193_marshal_com_cleanup(SummarizeJson_t970273193_marshaled_com& marshaled);
