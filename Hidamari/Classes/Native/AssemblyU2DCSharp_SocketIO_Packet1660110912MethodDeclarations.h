﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SocketIO.Packet
struct Packet_t1660110912;
// System.String
struct String_t;
// JSONObject
struct JSONObject_t1752376903;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SocketIO_EnginePacketType2017318204.h"
#include "AssemblyU2DCSharp_SocketIO_SocketPacketType4056543533.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSONObject1752376903.h"

// System.Void SocketIO.Packet::.ctor()
extern "C"  void Packet__ctor_m2535527374 (Packet_t1660110912 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIO.Packet::.ctor(SocketIO.EnginePacketType)
extern "C"  void Packet__ctor_m3499446795 (Packet_t1660110912 * __this, int32_t ___enginePacketType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIO.Packet::.ctor(SocketIO.EnginePacketType,SocketIO.SocketPacketType,System.Int32,System.String,System.Int32,JSONObject)
extern "C"  void Packet__ctor_m605347726 (Packet_t1660110912 * __this, int32_t ___enginePacketType0, int32_t ___socketPacketType1, int32_t ___attachments2, String_t* ___nsp3, int32_t ___id4, JSONObject_t1752376903 * ___json5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SocketIO.Packet::ToString()
extern "C"  String_t* Packet_ToString_m226686111 (Packet_t1660110912 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
