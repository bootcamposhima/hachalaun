﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.Net.EndPointListener
struct EndPointListener_t3188089579;
// System.Net.IPAddress
struct IPAddress_t3525271463;
// System.String
struct String_t;
// System.Security.Cryptography.X509Certificates.X509Certificate2
struct X509Certificate2_t160474609;
// System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>
struct List_1_t4031500248;
// WebSocketSharp.Net.ListenerPrefix
struct ListenerPrefix_t2663314696;
// System.Security.Cryptography.RSACryptoServiceProvider
struct RSACryptoServiceProvider_t742318033;
// WebSocketSharp.Net.HttpListener
struct HttpListener_t398944510;
// System.Object
struct Il2CppObject;
// System.EventArgs
struct EventArgs_t2540831021;
// System.Uri
struct Uri_t1116831938;
// WebSocketSharp.Net.HttpConnection
struct HttpConnection_t602292776;
// WebSocketSharp.Net.HttpListenerContext
struct HttpListenerContext_t3744659101;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_IPAddress3525271463.h"
#include "mscorlib_System_String7231557.h"
#include "System_System_Security_Cryptography_X509Certificate160474609.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_ListenerPrefi2663314696.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_EventArgs2540831021.h"
#include "System_System_Uri1116831938.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_HttpConnection602292776.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_HttpListener398944510.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_HttpListenerC3744659101.h"

// System.Void WebSocketSharp.Net.EndPointListener::.ctor(System.Net.IPAddress,System.Int32,System.Boolean,System.String,System.Security.Cryptography.X509Certificates.X509Certificate2)
extern "C"  void EndPointListener__ctor_m1564736097 (EndPointListener_t3188089579 * __this, IPAddress_t3525271463 * ___address0, int32_t ___port1, bool ___secure2, String_t* ___certFolderPath3, X509Certificate2_t160474609 * ___defaultCert4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.EndPointListener::.cctor()
extern "C"  void EndPointListener__cctor_m1784240583 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.X509Certificates.X509Certificate2 WebSocketSharp.Net.EndPointListener::get_Certificate()
extern "C"  X509Certificate2_t160474609 * EndPointListener_get_Certificate_m1125447477 (EndPointListener_t3188089579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.Net.EndPointListener::get_IsSecure()
extern "C"  bool EndPointListener_get_IsSecure_m3872894772 (EndPointListener_t3188089579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.EndPointListener::addSpecial(System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>,WebSocketSharp.Net.ListenerPrefix)
extern "C"  void EndPointListener_addSpecial_m4179389702 (Il2CppObject * __this /* static, unused */, List_1_t4031500248 * ___prefixes0, ListenerPrefix_t2663314696 * ___prefix1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.EndPointListener::checkIfRemove()
extern "C"  void EndPointListener_checkIfRemove_m228361165 (EndPointListener_t3188089579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RSACryptoServiceProvider WebSocketSharp.Net.EndPointListener::createRSAFromFile(System.String)
extern "C"  RSACryptoServiceProvider_t742318033 * EndPointListener_createRSAFromFile_m3892065145 (Il2CppObject * __this /* static, unused */, String_t* ___filename0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.X509Certificates.X509Certificate2 WebSocketSharp.Net.EndPointListener::getCertificate(System.Int32,System.String,System.Security.Cryptography.X509Certificates.X509Certificate2)
extern "C"  X509Certificate2_t160474609 * EndPointListener_getCertificate_m1039450851 (Il2CppObject * __this /* static, unused */, int32_t ___port0, String_t* ___certFolderPath1, X509Certificate2_t160474609 * ___defaultCert2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.Net.HttpListener WebSocketSharp.Net.EndPointListener::matchFromList(System.String,System.String,System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>,WebSocketSharp.Net.ListenerPrefix&)
extern "C"  HttpListener_t398944510 * EndPointListener_matchFromList_m3727925512 (Il2CppObject * __this /* static, unused */, String_t* ___host0, String_t* ___path1, List_1_t4031500248 * ___list2, ListenerPrefix_t2663314696 ** ___prefix3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.EndPointListener::onAccept(System.Object,System.EventArgs)
extern "C"  void EndPointListener_onAccept_m1777103711 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___sender0, EventArgs_t2540831021 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.Net.EndPointListener::removeSpecial(System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>,WebSocketSharp.Net.ListenerPrefix)
extern "C"  bool EndPointListener_removeSpecial_m215993565 (Il2CppObject * __this /* static, unused */, List_1_t4031500248 * ___prefixes0, ListenerPrefix_t2663314696 * ___prefix1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.Net.HttpListener WebSocketSharp.Net.EndPointListener::searchListener(System.Uri,WebSocketSharp.Net.ListenerPrefix&)
extern "C"  HttpListener_t398944510 * EndPointListener_searchListener_m4092730992 (EndPointListener_t3188089579 * __this, Uri_t1116831938 * ___uri0, ListenerPrefix_t2663314696 ** ___prefix1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.Net.EndPointListener::CertificateExists(System.Int32,System.String)
extern "C"  bool EndPointListener_CertificateExists_m352010776 (Il2CppObject * __this /* static, unused */, int32_t ___port0, String_t* ___certFolderPath1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.EndPointListener::RemoveConnection(WebSocketSharp.Net.HttpConnection)
extern "C"  void EndPointListener_RemoveConnection_m4132871536 (EndPointListener_t3188089579 * __this, HttpConnection_t602292776 * ___connection0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.EndPointListener::AddPrefix(WebSocketSharp.Net.ListenerPrefix,WebSocketSharp.Net.HttpListener)
extern "C"  void EndPointListener_AddPrefix_m3714808941 (EndPointListener_t3188089579 * __this, ListenerPrefix_t2663314696 * ___prefix0, HttpListener_t398944510 * ___listener1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.Net.EndPointListener::BindContext(WebSocketSharp.Net.HttpListenerContext)
extern "C"  bool EndPointListener_BindContext_m45151603 (EndPointListener_t3188089579 * __this, HttpListenerContext_t3744659101 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.EndPointListener::Close()
extern "C"  void EndPointListener_Close_m1229777852 (EndPointListener_t3188089579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.EndPointListener::RemovePrefix(WebSocketSharp.Net.ListenerPrefix,WebSocketSharp.Net.HttpListener)
extern "C"  void EndPointListener_RemovePrefix_m1323084944 (EndPointListener_t3188089579 * __this, ListenerPrefix_t2663314696 * ___prefix0, HttpListener_t398944510 * ___listener1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.EndPointListener::UnbindContext(WebSocketSharp.Net.HttpListenerContext)
extern "C"  void EndPointListener_UnbindContext_m2265805510 (EndPointListener_t3188089579 * __this, HttpListenerContext_t3744659101 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
