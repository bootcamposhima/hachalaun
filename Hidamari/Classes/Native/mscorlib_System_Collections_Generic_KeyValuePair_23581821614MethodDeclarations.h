﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_636475144MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Char>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3407984822(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3581821614 *, String_t*, Il2CppChar, const MethodInfo*))KeyValuePair_2__ctor_m274731848_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Char>::get_Key()
#define KeyValuePair_2_get_Key_m1384773106(__this, method) ((  String_t* (*) (KeyValuePair_2_t3581821614 *, const MethodInfo*))KeyValuePair_2_get_Key_m2659847968_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Char>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2476640179(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3581821614 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m1114790369_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Char>::get_Value()
#define KeyValuePair_2_get_Value_m3213633174(__this, method) ((  Il2CppChar (*) (KeyValuePair_2_t3581821614 *, const MethodInfo*))KeyValuePair_2_get_Value_m199928900_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Char>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m20498995(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3581821614 *, Il2CppChar, const MethodInfo*))KeyValuePair_2_set_Value_m2690372961_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Char>::ToString()
#define KeyValuePair_2_ToString_m1178027253(__this, method) ((  String_t* (*) (KeyValuePair_2_t3581821614 *, const MethodInfo*))KeyValuePair_2_ToString_m2050642311_gshared)(__this, method)
