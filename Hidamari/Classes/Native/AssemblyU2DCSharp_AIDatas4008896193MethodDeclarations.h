﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<AIData>
struct List_1_t3298620098;
// AIDatas
struct AIDatas_t4008896193;
struct AIDatas_t4008896193_marshaled_pinvoke;
struct AIDatas_t4008896193_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AIDatas4008896193.h"

// System.Void AIDatas::.ctor(System.Collections.Generic.List`1<AIData>)
extern "C"  void AIDatas__ctor_m2735443674 (AIDatas_t4008896193 * __this, List_1_t3298620098 * ___dataList0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIDatas::.ctor(System.Int32)
extern "C"  void AIDatas__ctor_m1593223195 (AIDatas_t4008896193 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct AIDatas_t4008896193;
struct AIDatas_t4008896193_marshaled_pinvoke;

extern "C" void AIDatas_t4008896193_marshal_pinvoke(const AIDatas_t4008896193& unmarshaled, AIDatas_t4008896193_marshaled_pinvoke& marshaled);
extern "C" void AIDatas_t4008896193_marshal_pinvoke_back(const AIDatas_t4008896193_marshaled_pinvoke& marshaled, AIDatas_t4008896193& unmarshaled);
extern "C" void AIDatas_t4008896193_marshal_pinvoke_cleanup(AIDatas_t4008896193_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct AIDatas_t4008896193;
struct AIDatas_t4008896193_marshaled_com;

extern "C" void AIDatas_t4008896193_marshal_com(const AIDatas_t4008896193& unmarshaled, AIDatas_t4008896193_marshaled_com& marshaled);
extern "C" void AIDatas_t4008896193_marshal_com_back(const AIDatas_t4008896193_marshaled_com& marshaled, AIDatas_t4008896193& unmarshaled);
extern "C" void AIDatas_t4008896193_marshal_com_cleanup(AIDatas_t4008896193_marshaled_com& marshaled);
