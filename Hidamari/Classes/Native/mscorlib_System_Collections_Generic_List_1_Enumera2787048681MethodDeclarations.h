﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<WebSocketSharp.Net.Chunk>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1886586788(__this, ___l0, method) ((  void (*) (Enumerator_t2787048681 *, List_1_t2767375911 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<WebSocketSharp.Net.Chunk>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m546756142(__this, method) ((  void (*) (Enumerator_t2787048681 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<WebSocketSharp.Net.Chunk>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1412710820(__this, method) ((  Il2CppObject * (*) (Enumerator_t2787048681 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<WebSocketSharp.Net.Chunk>::Dispose()
#define Enumerator_Dispose_m1801502345(__this, method) ((  void (*) (Enumerator_t2787048681 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<WebSocketSharp.Net.Chunk>::VerifyState()
#define Enumerator_VerifyState_m2710573378(__this, method) ((  void (*) (Enumerator_t2787048681 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<WebSocketSharp.Net.Chunk>::MoveNext()
#define Enumerator_MoveNext_m2810478302(__this, method) ((  bool (*) (Enumerator_t2787048681 *, const MethodInfo*))Enumerator_MoveNext_m844464217_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<WebSocketSharp.Net.Chunk>::get_Current()
#define Enumerator_get_Current_m2320536539(__this, method) ((  Chunk_t1399190359 * (*) (Enumerator_t2787048681 *, const MethodInfo*))Enumerator_get_Current_m4198990746_gshared)(__this, method)
