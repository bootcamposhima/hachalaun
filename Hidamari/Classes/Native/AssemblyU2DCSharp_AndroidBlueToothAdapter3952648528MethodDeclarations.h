﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AndroidBlueToothAdapter
struct AndroidBlueToothAdapter_t3952648528;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Tag83834.h"

// System.Void AndroidBlueToothAdapter::.ctor()
extern "C"  void AndroidBlueToothAdapter__ctor_m3700307995 (AndroidBlueToothAdapter_t3952648528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidBlueToothAdapter::Update()
extern "C"  void AndroidBlueToothAdapter_Update_m472292946 (AndroidBlueToothAdapter_t3952648528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidBlueToothAdapter::Create()
extern "C"  void AndroidBlueToothAdapter_Create_m970114501 (AndroidBlueToothAdapter_t3952648528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidBlueToothAdapter::BlueToothEnable()
extern "C"  void AndroidBlueToothAdapter_BlueToothEnable_m3106033834 (AndroidBlueToothAdapter_t3952648528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AndroidBlueToothAdapter::StartServer()
extern "C"  String_t* AndroidBlueToothAdapter_StartServer_m2588912673 (AndroidBlueToothAdapter_t3952648528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidBlueToothAdapter::Connect(System.String)
extern "C"  void AndroidBlueToothAdapter_Connect_m2348075583 (AndroidBlueToothAdapter_t3952648528 * __this, String_t* ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AndroidBlueToothAdapter::isConnected()
extern "C"  bool AndroidBlueToothAdapter_isConnected_m1145609292 (AndroidBlueToothAdapter_t3952648528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidBlueToothAdapter::Cancel()
extern "C"  void AndroidBlueToothAdapter_Cancel_m3321490371 (AndroidBlueToothAdapter_t3952648528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidBlueToothAdapter::disConnect()
extern "C"  void AndroidBlueToothAdapter_disConnect_m4047322405 (AndroidBlueToothAdapter_t3952648528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidBlueToothAdapter::SendIntergerData(System.Int32,Tag)
extern "C"  void AndroidBlueToothAdapter_SendIntergerData_m1906565366 (AndroidBlueToothAdapter_t3952648528 * __this, int32_t ___data0, int32_t ___tag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidBlueToothAdapter::SendFloatData(System.Single,Tag)
extern "C"  void AndroidBlueToothAdapter_SendFloatData_m1590876230 (AndroidBlueToothAdapter_t3952648528 * __this, float ___data0, int32_t ___tag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidBlueToothAdapter::SendStringData(System.String,Tag)
extern "C"  void AndroidBlueToothAdapter_SendStringData_m3075877352 (AndroidBlueToothAdapter_t3952648528 * __this, String_t* ___data0, int32_t ___tag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidBlueToothAdapter::onCallReceiveData(System.String)
extern "C"  void AndroidBlueToothAdapter_onCallReceiveData_m3774271545 (AndroidBlueToothAdapter_t3952648528 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidBlueToothAdapter::onCallReceiveIntegerData(System.Int32,System.Int32)
extern "C"  void AndroidBlueToothAdapter_onCallReceiveIntegerData_m3708722555 (AndroidBlueToothAdapter_t3952648528 * __this, int32_t ___intData0, int32_t ___tag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidBlueToothAdapter::onCallReceiveFloatData(System.Single,System.Int32)
extern "C"  void AndroidBlueToothAdapter_onCallReceiveFloatData_m2323598069 (AndroidBlueToothAdapter_t3952648528 * __this, float ___floatData0, int32_t ___tag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidBlueToothAdapter::onCallReceiveStringData(System.String,System.Int32)
extern "C"  void AndroidBlueToothAdapter_onCallReceiveStringData_m2166311663 (AndroidBlueToothAdapter_t3952648528 * __this, String_t* ___stringData0, int32_t ___tag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidBlueToothAdapter::onCallClickOK(System.String)
extern "C"  void AndroidBlueToothAdapter_onCallClickOK_m558241186 (AndroidBlueToothAdapter_t3952648528 * __this, String_t* ___tagstr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AndroidBlueToothAdapter::onCallDisConnect(System.String)
extern "C"  void AndroidBlueToothAdapter_onCallDisConnect_m3015455136 (AndroidBlueToothAdapter_t3952648528 * __this, String_t* ___dummydata0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
