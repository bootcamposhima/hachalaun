﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen3500991224MethodDeclarations.h"

// System.Void SingletonMonoBehaviour`1<SocketIO.SocketIOComponent>::.ctor()
#define SingletonMonoBehaviour_1__ctor_m1678667496(__this, method) ((  void (*) (SingletonMonoBehaviour_1_t2425651671 *, const MethodInfo*))SingletonMonoBehaviour_1__ctor_m2524367503_gshared)(__this, method)
// T SingletonMonoBehaviour`1<SocketIO.SocketIOComponent>::get_Instance()
#define SingletonMonoBehaviour_1_get_Instance_m3239631417(__this /* static, unused */, method) ((  SocketIOComponent_t3095476818 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SingletonMonoBehaviour_1_get_Instance_m4273216084_gshared)(__this /* static, unused */, method)
