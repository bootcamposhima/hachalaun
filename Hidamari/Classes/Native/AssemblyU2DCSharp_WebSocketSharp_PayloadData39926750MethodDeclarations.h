﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.PayloadData
struct PayloadData_t39926750;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Byte>
struct IEnumerator_1_t479507413;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void WebSocketSharp.PayloadData::.ctor()
extern "C"  void PayloadData__ctor_m1953054222 (PayloadData_t39926750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.PayloadData::.ctor(System.Byte[])
extern "C"  void PayloadData__ctor_m719751291 (PayloadData_t39926750 * __this, ByteU5BU5D_t4260760469* ___applicationData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.PayloadData::.ctor(System.String)
extern "C"  void PayloadData__ctor_m2777556916 (PayloadData_t39926750 * __this, String_t* ___applicationData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.PayloadData::.ctor(System.Byte[],System.Boolean)
extern "C"  void PayloadData__ctor_m3599656354 (PayloadData_t39926750 * __this, ByteU5BU5D_t4260760469* ___applicationData0, bool ___masked1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.PayloadData::.ctor(System.Byte[],System.Byte[],System.Boolean)
extern "C"  void PayloadData__ctor_m46018943 (PayloadData_t39926750 * __this, ByteU5BU5D_t4260760469* ___extensionData0, ByteU5BU5D_t4260760469* ___applicationData1, bool ___masked2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator WebSocketSharp.PayloadData::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * PayloadData_System_Collections_IEnumerable_GetEnumerator_m1737577763 (PayloadData_t39926750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.PayloadData::get_ContainsReservedCloseStatusCode()
extern "C"  bool PayloadData_get_ContainsReservedCloseStatusCode_m2380973759 (PayloadData_t39926750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] WebSocketSharp.PayloadData::get_ApplicationData()
extern "C"  ByteU5BU5D_t4260760469* PayloadData_get_ApplicationData_m3147947059 (PayloadData_t39926750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] WebSocketSharp.PayloadData::get_ExtensionData()
extern "C"  ByteU5BU5D_t4260760469* PayloadData_get_ExtensionData_m2164787682 (PayloadData_t39926750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.PayloadData::get_IsMasked()
extern "C"  bool PayloadData_get_IsMasked_m2863479752 (PayloadData_t39926750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 WebSocketSharp.PayloadData::get_Length()
extern "C"  uint64_t PayloadData_get_Length_m3166044545 (PayloadData_t39926750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.PayloadData::mask(System.Byte[],System.Byte[])
extern "C"  void PayloadData_mask_m1332147850 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___src0, ByteU5BU5D_t4260760469* ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Byte> WebSocketSharp.PayloadData::GetEnumerator()
extern "C"  Il2CppObject* PayloadData_GetEnumerator_m2795194391 (PayloadData_t39926750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.PayloadData::Mask(System.Byte[])
extern "C"  void PayloadData_Mask_m2120375719 (PayloadData_t39926750 * __this, ByteU5BU5D_t4260760469* ___maskingKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] WebSocketSharp.PayloadData::ToByteArray()
extern "C"  ByteU5BU5D_t4260760469* PayloadData_ToByteArray_m147904440 (PayloadData_t39926750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.PayloadData::ToString()
extern "C"  String_t* PayloadData_ToString_m441557573 (PayloadData_t39926750 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
