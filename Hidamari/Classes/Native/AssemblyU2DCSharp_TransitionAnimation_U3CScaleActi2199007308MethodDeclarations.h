﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TransitionAnimation/<ScaleAction>c__IteratorC
struct U3CScaleActionU3Ec__IteratorC_t2199007308;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void TransitionAnimation/<ScaleAction>c__IteratorC::.ctor()
extern "C"  void U3CScaleActionU3Ec__IteratorC__ctor_m420357791 (U3CScaleActionU3Ec__IteratorC_t2199007308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TransitionAnimation/<ScaleAction>c__IteratorC::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CScaleActionU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2137086675 (U3CScaleActionU3Ec__IteratorC_t2199007308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TransitionAnimation/<ScaleAction>c__IteratorC::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CScaleActionU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m3411108967 (U3CScaleActionU3Ec__IteratorC_t2199007308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TransitionAnimation/<ScaleAction>c__IteratorC::MoveNext()
extern "C"  bool U3CScaleActionU3Ec__IteratorC_MoveNext_m374548981 (U3CScaleActionU3Ec__IteratorC_t2199007308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransitionAnimation/<ScaleAction>c__IteratorC::Dispose()
extern "C"  void U3CScaleActionU3Ec__IteratorC_Dispose_m135194396 (U3CScaleActionU3Ec__IteratorC_t2199007308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransitionAnimation/<ScaleAction>c__IteratorC::Reset()
extern "C"  void U3CScaleActionU3Ec__IteratorC_Reset_m2361758028 (U3CScaleActionU3Ec__IteratorC_t2199007308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
