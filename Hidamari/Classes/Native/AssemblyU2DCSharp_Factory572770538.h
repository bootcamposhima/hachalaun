﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2662109048;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2761310900;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.GameObject>>
struct List_1_t2116085813;

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen4197912687.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Factory
struct  Factory_t572770538  : public SingletonMonoBehaviour_1_t4197912687
{
public:
	// UnityEngine.GameObject[] Factory::_bullets
	GameObjectU5BU5D_t2662109048* ____bullets_3;
	// UnityEngine.GameObject[] Factory::_charactors
	GameObjectU5BU5D_t2662109048* ____charactors_4;
	// UnityEngine.Sprite[] Factory::_charactorspritedatas
	SpriteU5BU5D_t2761310900* ____charactorspritedatas_5;
	// UnityEngine.Sprite[] Factory::_bulletspritedatas
	SpriteU5BU5D_t2761310900* ____bulletspritedatas_6;
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.GameObject>> Factory::_bullet
	List_1_t2116085813 * ____bullet_7;
	// System.Boolean Factory::isbulletstop
	bool ___isbulletstop_8;

public:
	inline static int32_t get_offset_of__bullets_3() { return static_cast<int32_t>(offsetof(Factory_t572770538, ____bullets_3)); }
	inline GameObjectU5BU5D_t2662109048* get__bullets_3() const { return ____bullets_3; }
	inline GameObjectU5BU5D_t2662109048** get_address_of__bullets_3() { return &____bullets_3; }
	inline void set__bullets_3(GameObjectU5BU5D_t2662109048* value)
	{
		____bullets_3 = value;
		Il2CppCodeGenWriteBarrier(&____bullets_3, value);
	}

	inline static int32_t get_offset_of__charactors_4() { return static_cast<int32_t>(offsetof(Factory_t572770538, ____charactors_4)); }
	inline GameObjectU5BU5D_t2662109048* get__charactors_4() const { return ____charactors_4; }
	inline GameObjectU5BU5D_t2662109048** get_address_of__charactors_4() { return &____charactors_4; }
	inline void set__charactors_4(GameObjectU5BU5D_t2662109048* value)
	{
		____charactors_4 = value;
		Il2CppCodeGenWriteBarrier(&____charactors_4, value);
	}

	inline static int32_t get_offset_of__charactorspritedatas_5() { return static_cast<int32_t>(offsetof(Factory_t572770538, ____charactorspritedatas_5)); }
	inline SpriteU5BU5D_t2761310900* get__charactorspritedatas_5() const { return ____charactorspritedatas_5; }
	inline SpriteU5BU5D_t2761310900** get_address_of__charactorspritedatas_5() { return &____charactorspritedatas_5; }
	inline void set__charactorspritedatas_5(SpriteU5BU5D_t2761310900* value)
	{
		____charactorspritedatas_5 = value;
		Il2CppCodeGenWriteBarrier(&____charactorspritedatas_5, value);
	}

	inline static int32_t get_offset_of__bulletspritedatas_6() { return static_cast<int32_t>(offsetof(Factory_t572770538, ____bulletspritedatas_6)); }
	inline SpriteU5BU5D_t2761310900* get__bulletspritedatas_6() const { return ____bulletspritedatas_6; }
	inline SpriteU5BU5D_t2761310900** get_address_of__bulletspritedatas_6() { return &____bulletspritedatas_6; }
	inline void set__bulletspritedatas_6(SpriteU5BU5D_t2761310900* value)
	{
		____bulletspritedatas_6 = value;
		Il2CppCodeGenWriteBarrier(&____bulletspritedatas_6, value);
	}

	inline static int32_t get_offset_of__bullet_7() { return static_cast<int32_t>(offsetof(Factory_t572770538, ____bullet_7)); }
	inline List_1_t2116085813 * get__bullet_7() const { return ____bullet_7; }
	inline List_1_t2116085813 ** get_address_of__bullet_7() { return &____bullet_7; }
	inline void set__bullet_7(List_1_t2116085813 * value)
	{
		____bullet_7 = value;
		Il2CppCodeGenWriteBarrier(&____bullet_7, value);
	}

	inline static int32_t get_offset_of_isbulletstop_8() { return static_cast<int32_t>(offsetof(Factory_t572770538, ___isbulletstop_8)); }
	inline bool get_isbulletstop_8() const { return ___isbulletstop_8; }
	inline bool* get_address_of_isbulletstop_8() { return &___isbulletstop_8; }
	inline void set_isbulletstop_8(bool value)
	{
		___isbulletstop_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
