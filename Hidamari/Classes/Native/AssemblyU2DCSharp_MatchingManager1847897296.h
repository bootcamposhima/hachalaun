﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2662109048;
// UnityEngine.UI.Text
struct Text_t9039225;

#include "AssemblyU2DCSharp_SocketManager2142175386.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MatchingManager
struct  MatchingManager_t1847897296  : public SocketManager_t2142175386
{
public:
	// System.Int32 MatchingManager::createcount
	int32_t ___createcount_3;
	// System.Int32 MatchingManager::deletecount
	int32_t ___deletecount_4;
	// System.Int32 MatchingManager::time
	int32_t ___time_5;
	// UnityEngine.GameObject MatchingManager::_button
	GameObject_t3674682005 * ____button_6;
	// UnityEngine.GameObject[] MatchingManager::par
	GameObjectU5BU5D_t2662109048* ___par_7;
	// UnityEngine.UI.Text MatchingManager::_text
	Text_t9039225 * ____text_8;

public:
	inline static int32_t get_offset_of_createcount_3() { return static_cast<int32_t>(offsetof(MatchingManager_t1847897296, ___createcount_3)); }
	inline int32_t get_createcount_3() const { return ___createcount_3; }
	inline int32_t* get_address_of_createcount_3() { return &___createcount_3; }
	inline void set_createcount_3(int32_t value)
	{
		___createcount_3 = value;
	}

	inline static int32_t get_offset_of_deletecount_4() { return static_cast<int32_t>(offsetof(MatchingManager_t1847897296, ___deletecount_4)); }
	inline int32_t get_deletecount_4() const { return ___deletecount_4; }
	inline int32_t* get_address_of_deletecount_4() { return &___deletecount_4; }
	inline void set_deletecount_4(int32_t value)
	{
		___deletecount_4 = value;
	}

	inline static int32_t get_offset_of_time_5() { return static_cast<int32_t>(offsetof(MatchingManager_t1847897296, ___time_5)); }
	inline int32_t get_time_5() const { return ___time_5; }
	inline int32_t* get_address_of_time_5() { return &___time_5; }
	inline void set_time_5(int32_t value)
	{
		___time_5 = value;
	}

	inline static int32_t get_offset_of__button_6() { return static_cast<int32_t>(offsetof(MatchingManager_t1847897296, ____button_6)); }
	inline GameObject_t3674682005 * get__button_6() const { return ____button_6; }
	inline GameObject_t3674682005 ** get_address_of__button_6() { return &____button_6; }
	inline void set__button_6(GameObject_t3674682005 * value)
	{
		____button_6 = value;
		Il2CppCodeGenWriteBarrier(&____button_6, value);
	}

	inline static int32_t get_offset_of_par_7() { return static_cast<int32_t>(offsetof(MatchingManager_t1847897296, ___par_7)); }
	inline GameObjectU5BU5D_t2662109048* get_par_7() const { return ___par_7; }
	inline GameObjectU5BU5D_t2662109048** get_address_of_par_7() { return &___par_7; }
	inline void set_par_7(GameObjectU5BU5D_t2662109048* value)
	{
		___par_7 = value;
		Il2CppCodeGenWriteBarrier(&___par_7, value);
	}

	inline static int32_t get_offset_of__text_8() { return static_cast<int32_t>(offsetof(MatchingManager_t1847897296, ____text_8)); }
	inline Text_t9039225 * get__text_8() const { return ____text_8; }
	inline Text_t9039225 ** get_address_of__text_8() { return &____text_8; }
	inline void set__text_8(Text_t9039225 * value)
	{
		____text_8 = value;
		Il2CppCodeGenWriteBarrier(&____text_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
