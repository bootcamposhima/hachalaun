﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen712777222.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_AIData1930434546.h"

// System.Void System.Array/InternalEnumerator`1<AIData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4065500931_gshared (InternalEnumerator_1_t712777222 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m4065500931(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t712777222 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m4065500931_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<AIData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3754342845_gshared (InternalEnumerator_1_t712777222 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3754342845(__this, method) ((  void (*) (InternalEnumerator_1_t712777222 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3754342845_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<AIData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2573106665_gshared (InternalEnumerator_1_t712777222 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2573106665(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t712777222 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2573106665_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<AIData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m293044378_gshared (InternalEnumerator_1_t712777222 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m293044378(__this, method) ((  void (*) (InternalEnumerator_1_t712777222 *, const MethodInfo*))InternalEnumerator_1_Dispose_m293044378_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<AIData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3807095465_gshared (InternalEnumerator_1_t712777222 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3807095465(__this, method) ((  bool (*) (InternalEnumerator_1_t712777222 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3807095465_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<AIData>::get_Current()
extern "C"  AIData_t1930434546  InternalEnumerator_1_get_Current_m1029262474_gshared (InternalEnumerator_1_t712777222 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1029262474(__this, method) ((  AIData_t1930434546  (*) (InternalEnumerator_1_t712777222 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1029262474_gshared)(__this, method)
