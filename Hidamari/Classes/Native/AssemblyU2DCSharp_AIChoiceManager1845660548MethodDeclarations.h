﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AIChoiceManager
struct AIChoiceManager_t1845660548;
// System.Collections.Generic.List`1<AIDatas>
struct List_1_t1082114449;

#include "codegen/il2cpp-codegen.h"

// System.Void AIChoiceManager::.ctor()
extern "C"  void AIChoiceManager__ctor_m3702517863 (AIChoiceManager_t1845660548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIChoiceManager::Start()
extern "C"  void AIChoiceManager_Start_m2649655655 (AIChoiceManager_t1845660548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIChoiceManager::Awake()
extern "C"  void AIChoiceManager_Awake_m3940123082 (AIChoiceManager_t1845660548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIChoiceManager::Update()
extern "C"  void AIChoiceManager_Update_m540798854 (AIChoiceManager_t1845660548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AIChoiceManager::get_Direction()
extern "C"  int32_t AIChoiceManager_get_Direction_m3194661865 (AIChoiceManager_t1845660548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIChoiceManager::initAIButton()
extern "C"  void AIChoiceManager_initAIButton_m2435990503 (AIChoiceManager_t1845660548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIChoiceManager::setAIDatas(System.Collections.Generic.List`1<AIDatas>)
extern "C"  void AIChoiceManager_setAIDatas_m454636801 (AIChoiceManager_t1845660548 * __this, List_1_t1082114449 * ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIChoiceManager::TopAIButton(System.Int32)
extern "C"  void AIChoiceManager_TopAIButton_m1927420901 (AIChoiceManager_t1845660548 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIChoiceManager::TopOKButton()
extern "C"  void AIChoiceManager_TopOKButton_m3690443336 (AIChoiceManager_t1845660548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIChoiceManager::TopAddButton()
extern "C"  void AIChoiceManager_TopAddButton_m2383533531 (AIChoiceManager_t1845660548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIChoiceManager::setDirection(System.Int32)
extern "C"  void AIChoiceManager_setDirection_m242919467 (AIChoiceManager_t1845660548 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
