﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EnemyManager
struct EnemyManager_t1035150117;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AIDatas4008896193.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void EnemyManager::.ctor()
extern "C"  void EnemyManager__ctor_m1552517398 (EnemyManager_t1035150117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyManager::Start()
extern "C"  void EnemyManager_Start_m499655190 (EnemyManager_t1035150117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyManager::Update()
extern "C"  void EnemyManager_Update_m2610261175 (EnemyManager_t1035150117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AIDatas EnemyManager::get_aidatas()
extern "C"  AIDatas_t4008896193  EnemyManager_get_aidatas_m2369288026 (EnemyManager_t1035150117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyManager::DemoStart()
extern "C"  void EnemyManager_DemoStart_m1061688627 (EnemyManager_t1035150117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyManager::createEnemys(AIDatas,System.Boolean)
extern "C"  void EnemyManager_createEnemys_m1655669857 (EnemyManager_t1035150117 * __this, AIDatas_t4008896193  ___aidatas0, bool ___isDemoPlay1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyManager::DestroyEnemys()
extern "C"  void EnemyManager_DestroyEnemys_m1481623673 (EnemyManager_t1035150117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyManager::EndAction()
extern "C"  void EnemyManager_EndAction_m3635857253 (EnemyManager_t1035150117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyManager::MoveSatrtPosition()
extern "C"  void EnemyManager_MoveSatrtPosition_m1542618964 (EnemyManager_t1035150117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyManager::MoveStart()
extern "C"  void EnemyManager_MoveStart_m768405381 (EnemyManager_t1035150117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyManager::MoveStop()
extern "C"  void EnemyManager_MoveStop_m3211788705 (EnemyManager_t1035150117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyManager::MoveReStart()
extern "C"  void EnemyManager_MoveReStart_m1640139090 (EnemyManager_t1035150117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyManager::SetGameSpeed(System.Single)
extern "C"  void EnemyManager_SetGameSpeed_m1863790186 (EnemyManager_t1035150117 * __this, float ___speed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyManager::AIEndCount()
extern "C"  void EnemyManager_AIEndCount_m1741888970 (EnemyManager_t1035150117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyManager::setScript(UnityEngine.GameObject,System.Int32)
extern "C"  void EnemyManager_setScript_m863104862 (EnemyManager_t1035150117 * __this, GameObject_t3674682005 * ____enemy0, int32_t ___num1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
