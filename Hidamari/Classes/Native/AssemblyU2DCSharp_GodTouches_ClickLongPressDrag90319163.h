﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t9039225;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_GodTouches_ClickLongPressDrag_Ev1380888277.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GodTouches.ClickLongPressDrag
struct  ClickLongPressDrag_t90319163  : public MonoBehaviour_t667441552
{
public:
	// System.Single GodTouches.ClickLongPressDrag::CheckDistance
	float ___CheckDistance_2;
	// System.Single GodTouches.ClickLongPressDrag::CheckTime
	float ___CheckTime_3;
	// UnityEngine.UI.Text GodTouches.ClickLongPressDrag::Text
	Text_t9039225 * ___Text_4;
	// GodTouches.ClickLongPressDrag/EventType GodTouches.ClickLongPressDrag::type
	int32_t ___type_5;
	// System.Boolean GodTouches.ClickLongPressDrag::isRunning
	bool ___isRunning_6;
	// UnityEngine.Vector3 GodTouches.ClickLongPressDrag::startPos
	Vector3_t4282066566  ___startPos_7;
	// System.Single GodTouches.ClickLongPressDrag::startTime
	float ___startTime_8;

public:
	inline static int32_t get_offset_of_CheckDistance_2() { return static_cast<int32_t>(offsetof(ClickLongPressDrag_t90319163, ___CheckDistance_2)); }
	inline float get_CheckDistance_2() const { return ___CheckDistance_2; }
	inline float* get_address_of_CheckDistance_2() { return &___CheckDistance_2; }
	inline void set_CheckDistance_2(float value)
	{
		___CheckDistance_2 = value;
	}

	inline static int32_t get_offset_of_CheckTime_3() { return static_cast<int32_t>(offsetof(ClickLongPressDrag_t90319163, ___CheckTime_3)); }
	inline float get_CheckTime_3() const { return ___CheckTime_3; }
	inline float* get_address_of_CheckTime_3() { return &___CheckTime_3; }
	inline void set_CheckTime_3(float value)
	{
		___CheckTime_3 = value;
	}

	inline static int32_t get_offset_of_Text_4() { return static_cast<int32_t>(offsetof(ClickLongPressDrag_t90319163, ___Text_4)); }
	inline Text_t9039225 * get_Text_4() const { return ___Text_4; }
	inline Text_t9039225 ** get_address_of_Text_4() { return &___Text_4; }
	inline void set_Text_4(Text_t9039225 * value)
	{
		___Text_4 = value;
		Il2CppCodeGenWriteBarrier(&___Text_4, value);
	}

	inline static int32_t get_offset_of_type_5() { return static_cast<int32_t>(offsetof(ClickLongPressDrag_t90319163, ___type_5)); }
	inline int32_t get_type_5() const { return ___type_5; }
	inline int32_t* get_address_of_type_5() { return &___type_5; }
	inline void set_type_5(int32_t value)
	{
		___type_5 = value;
	}

	inline static int32_t get_offset_of_isRunning_6() { return static_cast<int32_t>(offsetof(ClickLongPressDrag_t90319163, ___isRunning_6)); }
	inline bool get_isRunning_6() const { return ___isRunning_6; }
	inline bool* get_address_of_isRunning_6() { return &___isRunning_6; }
	inline void set_isRunning_6(bool value)
	{
		___isRunning_6 = value;
	}

	inline static int32_t get_offset_of_startPos_7() { return static_cast<int32_t>(offsetof(ClickLongPressDrag_t90319163, ___startPos_7)); }
	inline Vector3_t4282066566  get_startPos_7() const { return ___startPos_7; }
	inline Vector3_t4282066566 * get_address_of_startPos_7() { return &___startPos_7; }
	inline void set_startPos_7(Vector3_t4282066566  value)
	{
		___startPos_7 = value;
	}

	inline static int32_t get_offset_of_startTime_8() { return static_cast<int32_t>(offsetof(ClickLongPressDrag_t90319163, ___startTime_8)); }
	inline float get_startTime_8() const { return ___startTime_8; }
	inline float* get_address_of_startTime_8() { return &___startTime_8; }
	inline void set_startTime_8(float value)
	{
		___startTime_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
