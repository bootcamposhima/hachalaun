﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Charctor/<EndActionCol>c__Iterator2
struct U3CEndActionColU3Ec__Iterator2_t2737953241;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Charctor/<EndActionCol>c__Iterator2::.ctor()
extern "C"  void U3CEndActionColU3Ec__Iterator2__ctor_m1053715890 (U3CEndActionColU3Ec__Iterator2_t2737953241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Charctor/<EndActionCol>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CEndActionColU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3311928160 (U3CEndActionColU3Ec__Iterator2_t2737953241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Charctor/<EndActionCol>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CEndActionColU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m4073506548 (U3CEndActionColU3Ec__Iterator2_t2737953241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Charctor/<EndActionCol>c__Iterator2::MoveNext()
extern "C"  bool U3CEndActionColU3Ec__Iterator2_MoveNext_m3932566786 (U3CEndActionColU3Ec__Iterator2_t2737953241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Charctor/<EndActionCol>c__Iterator2::Dispose()
extern "C"  void U3CEndActionColU3Ec__Iterator2_Dispose_m3201938799 (U3CEndActionColU3Ec__Iterator2_t2737953241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Charctor/<EndActionCol>c__Iterator2::Reset()
extern "C"  void U3CEndActionColU3Ec__Iterator2_Reset_m2995116127 (U3CEndActionColU3Ec__Iterator2_t2737953241 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
