﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviorTree
struct BehaviorTree_t2111599920;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void BehaviorTree::.ctor()
extern "C"  void BehaviorTree__ctor_m1071928171 (BehaviorTree_t2111599920 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviorTree::Start()
extern "C"  void BehaviorTree_Start_m19065963 (BehaviorTree_t2111599920 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviorTree::Awake()
extern "C"  void BehaviorTree_Awake_m1309533390 (BehaviorTree_t2111599920 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviorTree::Update()
extern "C"  void BehaviorTree_Update_m596897026 (BehaviorTree_t2111599920 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviorTree::Play()
extern "C"  void BehaviorTree_Play_m184438445 (BehaviorTree_t2111599920 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviorTree::Stop()
extern "C"  void BehaviorTree_Stop_m278122491 (BehaviorTree_t2111599920 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator BehaviorTree::AIPlay()
extern "C"  Il2CppObject * BehaviorTree_AIPlay_m432552733 (BehaviorTree_t2111599920 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
