﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Net.Sockets.TcpClient
struct TcpClient_t838416830;
// WebSocketSharp.Net.CookieCollection
struct CookieCollection_t1136277956;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t2791941106;
// WebSocketSharp.HandshakeRequest
struct HandshakeRequest_t1037477780;
// WebSocketSharp.WebSocketStream
struct WebSocketStream_t4103435597;
// System.Uri
struct Uri_t1116831938;
// System.Security.Principal.IPrincipal
struct IPrincipal_t1899242073;
// WebSocketSharp.WebSocket
struct WebSocket_t1342580397;

#include "AssemblyU2DCSharp_WebSocketSharp_Net_WebSockets_Web763725542.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext
struct  TcpListenerWebSocketContext_t3782393199  : public WebSocketContext_t763725542
{
public:
	// System.Net.Sockets.TcpClient WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext::_client
	TcpClient_t838416830 * ____client_0;
	// WebSocketSharp.Net.CookieCollection WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext::_cookies
	CookieCollection_t1136277956 * ____cookies_1;
	// System.Collections.Specialized.NameValueCollection WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext::_queryString
	NameValueCollection_t2791941106 * ____queryString_2;
	// WebSocketSharp.HandshakeRequest WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext::_request
	HandshakeRequest_t1037477780 * ____request_3;
	// System.Boolean WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext::_secure
	bool ____secure_4;
	// WebSocketSharp.WebSocketStream WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext::_stream
	WebSocketStream_t4103435597 * ____stream_5;
	// System.Uri WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext::_uri
	Uri_t1116831938 * ____uri_6;
	// System.Security.Principal.IPrincipal WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext::_user
	Il2CppObject * ____user_7;
	// WebSocketSharp.WebSocket WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext::_websocket
	WebSocket_t1342580397 * ____websocket_8;

public:
	inline static int32_t get_offset_of__client_0() { return static_cast<int32_t>(offsetof(TcpListenerWebSocketContext_t3782393199, ____client_0)); }
	inline TcpClient_t838416830 * get__client_0() const { return ____client_0; }
	inline TcpClient_t838416830 ** get_address_of__client_0() { return &____client_0; }
	inline void set__client_0(TcpClient_t838416830 * value)
	{
		____client_0 = value;
		Il2CppCodeGenWriteBarrier(&____client_0, value);
	}

	inline static int32_t get_offset_of__cookies_1() { return static_cast<int32_t>(offsetof(TcpListenerWebSocketContext_t3782393199, ____cookies_1)); }
	inline CookieCollection_t1136277956 * get__cookies_1() const { return ____cookies_1; }
	inline CookieCollection_t1136277956 ** get_address_of__cookies_1() { return &____cookies_1; }
	inline void set__cookies_1(CookieCollection_t1136277956 * value)
	{
		____cookies_1 = value;
		Il2CppCodeGenWriteBarrier(&____cookies_1, value);
	}

	inline static int32_t get_offset_of__queryString_2() { return static_cast<int32_t>(offsetof(TcpListenerWebSocketContext_t3782393199, ____queryString_2)); }
	inline NameValueCollection_t2791941106 * get__queryString_2() const { return ____queryString_2; }
	inline NameValueCollection_t2791941106 ** get_address_of__queryString_2() { return &____queryString_2; }
	inline void set__queryString_2(NameValueCollection_t2791941106 * value)
	{
		____queryString_2 = value;
		Il2CppCodeGenWriteBarrier(&____queryString_2, value);
	}

	inline static int32_t get_offset_of__request_3() { return static_cast<int32_t>(offsetof(TcpListenerWebSocketContext_t3782393199, ____request_3)); }
	inline HandshakeRequest_t1037477780 * get__request_3() const { return ____request_3; }
	inline HandshakeRequest_t1037477780 ** get_address_of__request_3() { return &____request_3; }
	inline void set__request_3(HandshakeRequest_t1037477780 * value)
	{
		____request_3 = value;
		Il2CppCodeGenWriteBarrier(&____request_3, value);
	}

	inline static int32_t get_offset_of__secure_4() { return static_cast<int32_t>(offsetof(TcpListenerWebSocketContext_t3782393199, ____secure_4)); }
	inline bool get__secure_4() const { return ____secure_4; }
	inline bool* get_address_of__secure_4() { return &____secure_4; }
	inline void set__secure_4(bool value)
	{
		____secure_4 = value;
	}

	inline static int32_t get_offset_of__stream_5() { return static_cast<int32_t>(offsetof(TcpListenerWebSocketContext_t3782393199, ____stream_5)); }
	inline WebSocketStream_t4103435597 * get__stream_5() const { return ____stream_5; }
	inline WebSocketStream_t4103435597 ** get_address_of__stream_5() { return &____stream_5; }
	inline void set__stream_5(WebSocketStream_t4103435597 * value)
	{
		____stream_5 = value;
		Il2CppCodeGenWriteBarrier(&____stream_5, value);
	}

	inline static int32_t get_offset_of__uri_6() { return static_cast<int32_t>(offsetof(TcpListenerWebSocketContext_t3782393199, ____uri_6)); }
	inline Uri_t1116831938 * get__uri_6() const { return ____uri_6; }
	inline Uri_t1116831938 ** get_address_of__uri_6() { return &____uri_6; }
	inline void set__uri_6(Uri_t1116831938 * value)
	{
		____uri_6 = value;
		Il2CppCodeGenWriteBarrier(&____uri_6, value);
	}

	inline static int32_t get_offset_of__user_7() { return static_cast<int32_t>(offsetof(TcpListenerWebSocketContext_t3782393199, ____user_7)); }
	inline Il2CppObject * get__user_7() const { return ____user_7; }
	inline Il2CppObject ** get_address_of__user_7() { return &____user_7; }
	inline void set__user_7(Il2CppObject * value)
	{
		____user_7 = value;
		Il2CppCodeGenWriteBarrier(&____user_7, value);
	}

	inline static int32_t get_offset_of__websocket_8() { return static_cast<int32_t>(offsetof(TcpListenerWebSocketContext_t3782393199, ____websocket_8)); }
	inline WebSocket_t1342580397 * get__websocket_8() const { return ____websocket_8; }
	inline WebSocket_t1342580397 ** get_address_of__websocket_8() { return &____websocket_8; }
	inline void set__websocket_8(WebSocket_t1342580397 * value)
	{
		____websocket_8 = value;
		Il2CppCodeGenWriteBarrier(&____websocket_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
