﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3713785116.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_636475144.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Char>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m48311799_gshared (InternalEnumerator_1_t3713785116 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m48311799(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3713785116 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m48311799_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Char>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1134673481_gshared (InternalEnumerator_1_t3713785116 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1134673481(__this, method) ((  void (*) (InternalEnumerator_1_t3713785116 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1134673481_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Char>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3083243573_gshared (InternalEnumerator_1_t3713785116 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3083243573(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3713785116 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3083243573_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Char>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1269461134_gshared (InternalEnumerator_1_t3713785116 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1269461134(__this, method) ((  void (*) (InternalEnumerator_1_t3713785116 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1269461134_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Char>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m818954421_gshared (InternalEnumerator_1_t3713785116 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m818954421(__this, method) ((  bool (*) (InternalEnumerator_1_t3713785116 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m818954421_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Char>>::get_Current()
extern "C"  KeyValuePair_2_t636475144  InternalEnumerator_1_get_Current_m1235553854_gshared (InternalEnumerator_1_t3713785116 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1235553854(__this, method) ((  KeyValuePair_2_t636475144  (*) (InternalEnumerator_1_t3713785116 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1235553854_gshared)(__this, method)
