﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// FightRound
struct FightRound_t2951839614;

#include "AssemblyU2DCSharp_SocketManager2142175386.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FightManager
struct  FightManager_t1489248669  : public SocketManager_t2142175386
{
public:
	// FightRound FightManager::_fm
	FightRound_t2951839614 * ____fm_3;

public:
	inline static int32_t get_offset_of__fm_3() { return static_cast<int32_t>(offsetof(FightManager_t1489248669, ____fm_3)); }
	inline FightRound_t2951839614 * get__fm_3() const { return ____fm_3; }
	inline FightRound_t2951839614 ** get_address_of__fm_3() { return &____fm_3; }
	inline void set__fm_3(FightRound_t2951839614 * value)
	{
		____fm_3 = value;
		Il2CppCodeGenWriteBarrier(&____fm_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
