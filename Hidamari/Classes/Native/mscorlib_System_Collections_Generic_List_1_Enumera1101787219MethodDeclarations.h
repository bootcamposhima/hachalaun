﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<AIDatas>
struct List_1_t1082114449;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1101787219.h"
#include "AssemblyU2DCSharp_AIDatas4008896193.h"

// System.Void System.Collections.Generic.List`1/Enumerator<AIDatas>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3401528168_gshared (Enumerator_t1101787219 * __this, List_1_t1082114449 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m3401528168(__this, ___l0, method) ((  void (*) (Enumerator_t1101787219 *, List_1_t1082114449 *, const MethodInfo*))Enumerator__ctor_m3401528168_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<AIDatas>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m352804842_gshared (Enumerator_t1101787219 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m352804842(__this, method) ((  void (*) (Enumerator_t1101787219 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m352804842_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<AIDatas>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1595049430_gshared (Enumerator_t1101787219 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1595049430(__this, method) ((  Il2CppObject * (*) (Enumerator_t1101787219 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1595049430_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<AIDatas>::Dispose()
extern "C"  void Enumerator_Dispose_m4275116877_gshared (Enumerator_t1101787219 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m4275116877(__this, method) ((  void (*) (Enumerator_t1101787219 *, const MethodInfo*))Enumerator_Dispose_m4275116877_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<AIDatas>::VerifyState()
extern "C"  void Enumerator_VerifyState_m406612998_gshared (Enumerator_t1101787219 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m406612998(__this, method) ((  void (*) (Enumerator_t1101787219 *, const MethodInfo*))Enumerator_VerifyState_m406612998_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<AIDatas>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m108263766_gshared (Enumerator_t1101787219 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m108263766(__this, method) ((  bool (*) (Enumerator_t1101787219 *, const MethodInfo*))Enumerator_MoveNext_m108263766_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<AIDatas>::get_Current()
extern "C"  AIDatas_t4008896193  Enumerator_get_Current_m1563209533_gshared (Enumerator_t1101787219 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1563209533(__this, method) ((  AIDatas_t4008896193  (*) (Enumerator_t1101787219 *, const MethodInfo*))Enumerator_get_Current_m1563209533_gshared)(__this, method)
