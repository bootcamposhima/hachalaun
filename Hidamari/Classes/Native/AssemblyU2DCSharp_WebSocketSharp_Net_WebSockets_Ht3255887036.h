﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t4054002952;
// WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext
struct HttpListenerWebSocketContext_t1074545506;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext/<>c__Iterator1C
struct  U3CU3Ec__Iterator1C_t3255887036  : public Il2CppObject
{
public:
	// System.String WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext/<>c__Iterator1C::<protocols>__0
	String_t* ___U3CprotocolsU3E__0_0;
	// System.String[] WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext/<>c__Iterator1C::<$s_136>__1
	StringU5BU5D_t4054002952* ___U3CU24s_136U3E__1_1;
	// System.Int32 WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext/<>c__Iterator1C::<$s_137>__2
	int32_t ___U3CU24s_137U3E__2_2;
	// System.String WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext/<>c__Iterator1C::<protocol>__3
	String_t* ___U3CprotocolU3E__3_3;
	// System.Int32 WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext/<>c__Iterator1C::$PC
	int32_t ___U24PC_4;
	// System.String WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext/<>c__Iterator1C::$current
	String_t* ___U24current_5;
	// WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext/<>c__Iterator1C::<>f__this
	HttpListenerWebSocketContext_t1074545506 * ___U3CU3Ef__this_6;

public:
	inline static int32_t get_offset_of_U3CprotocolsU3E__0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1C_t3255887036, ___U3CprotocolsU3E__0_0)); }
	inline String_t* get_U3CprotocolsU3E__0_0() const { return ___U3CprotocolsU3E__0_0; }
	inline String_t** get_address_of_U3CprotocolsU3E__0_0() { return &___U3CprotocolsU3E__0_0; }
	inline void set_U3CprotocolsU3E__0_0(String_t* value)
	{
		___U3CprotocolsU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CprotocolsU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CU24s_136U3E__1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1C_t3255887036, ___U3CU24s_136U3E__1_1)); }
	inline StringU5BU5D_t4054002952* get_U3CU24s_136U3E__1_1() const { return ___U3CU24s_136U3E__1_1; }
	inline StringU5BU5D_t4054002952** get_address_of_U3CU24s_136U3E__1_1() { return &___U3CU24s_136U3E__1_1; }
	inline void set_U3CU24s_136U3E__1_1(StringU5BU5D_t4054002952* value)
	{
		___U3CU24s_136U3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_136U3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CU24s_137U3E__2_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1C_t3255887036, ___U3CU24s_137U3E__2_2)); }
	inline int32_t get_U3CU24s_137U3E__2_2() const { return ___U3CU24s_137U3E__2_2; }
	inline int32_t* get_address_of_U3CU24s_137U3E__2_2() { return &___U3CU24s_137U3E__2_2; }
	inline void set_U3CU24s_137U3E__2_2(int32_t value)
	{
		___U3CU24s_137U3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U3CprotocolU3E__3_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1C_t3255887036, ___U3CprotocolU3E__3_3)); }
	inline String_t* get_U3CprotocolU3E__3_3() const { return ___U3CprotocolU3E__3_3; }
	inline String_t** get_address_of_U3CprotocolU3E__3_3() { return &___U3CprotocolU3E__3_3; }
	inline void set_U3CprotocolU3E__3_3(String_t* value)
	{
		___U3CprotocolU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CprotocolU3E__3_3, value);
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1C_t3255887036, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1C_t3255887036, ___U24current_5)); }
	inline String_t* get_U24current_5() const { return ___U24current_5; }
	inline String_t** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(String_t* value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1C_t3255887036, ___U3CU3Ef__this_6)); }
	inline HttpListenerWebSocketContext_t1074545506 * get_U3CU3Ef__this_6() const { return ___U3CU3Ef__this_6; }
	inline HttpListenerWebSocketContext_t1074545506 ** get_address_of_U3CU3Ef__this_6() { return &___U3CU3Ef__this_6; }
	inline void set_U3CU3Ef__this_6(HttpListenerWebSocketContext_t1074545506 * value)
	{
		___U3CU3Ef__this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
