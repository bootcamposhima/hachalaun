﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.WebSocket/<startReceiving>c__AnonStorey2B
struct U3CstartReceivingU3Ec__AnonStorey2B_t802946016;
// WebSocketSharp.WebSocketFrame
struct WebSocketFrame_t778194306;
// System.Exception
struct Exception_t3991598821;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocketFrame778194306.h"
#include "mscorlib_System_Exception3991598821.h"

// System.Void WebSocketSharp.WebSocket/<startReceiving>c__AnonStorey2B::.ctor()
extern "C"  void U3CstartReceivingU3Ec__AnonStorey2B__ctor_m2023001787 (U3CstartReceivingU3Ec__AnonStorey2B_t802946016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket/<startReceiving>c__AnonStorey2B::<>m__11()
extern "C"  void U3CstartReceivingU3Ec__AnonStorey2B_U3CU3Em__11_m792573156 (U3CstartReceivingU3Ec__AnonStorey2B_t802946016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket/<startReceiving>c__AnonStorey2B::<>m__17(WebSocketSharp.WebSocketFrame)
extern "C"  void U3CstartReceivingU3Ec__AnonStorey2B_U3CU3Em__17_m1383973847 (U3CstartReceivingU3Ec__AnonStorey2B_t802946016 * __this, WebSocketFrame_t778194306 * ___frame0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket/<startReceiving>c__AnonStorey2B::<>m__18(System.Exception)
extern "C"  void U3CstartReceivingU3Ec__AnonStorey2B_U3CU3Em__18_m3591481819 (U3CstartReceivingU3Ec__AnonStorey2B_t802946016 * __this, Exception_t3991598821 * ___ex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
