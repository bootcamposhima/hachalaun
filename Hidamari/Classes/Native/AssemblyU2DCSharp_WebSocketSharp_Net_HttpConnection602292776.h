﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t4260760469;
// WebSocketSharp.Net.HttpListenerContext
struct HttpListenerContext_t3744659101;
// System.Text.StringBuilder
struct StringBuilder_t243639308;
// WebSocketSharp.Net.RequestStream
struct RequestStream_t2929193945;
// WebSocketSharp.Net.HttpListener
struct HttpListener_t398944510;
// WebSocketSharp.Net.EndPointListener
struct EndPointListener_t3188089579;
// WebSocketSharp.Net.ResponseStream
struct ResponseStream_t1796293571;
// WebSocketSharp.Net.ListenerPrefix
struct ListenerPrefix_t2663314696;
// System.IO.MemoryStream
struct MemoryStream_t418716369;
// System.Net.Sockets.Socket
struct Socket_t2157335841;
// System.IO.Stream
struct Stream_t1561764144;
// System.Object
struct Il2CppObject;
// System.Threading.Timer
struct Timer_t1893171827;
// WebSocketSharp.WebSocketStream
struct WebSocketStream_t4103435597;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_InputState2025202057.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_LineState793115623.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpConnection
struct  HttpConnection_t602292776  : public Il2CppObject
{
public:
	// System.Byte[] WebSocketSharp.Net.HttpConnection::_buffer
	ByteU5BU5D_t4260760469* ____buffer_1;
	// System.Boolean WebSocketSharp.Net.HttpConnection::_chunked
	bool ____chunked_2;
	// WebSocketSharp.Net.HttpListenerContext WebSocketSharp.Net.HttpConnection::_context
	HttpListenerContext_t3744659101 * ____context_3;
	// System.Boolean WebSocketSharp.Net.HttpConnection::_contextWasBound
	bool ____contextWasBound_4;
	// System.Text.StringBuilder WebSocketSharp.Net.HttpConnection::_currentLine
	StringBuilder_t243639308 * ____currentLine_5;
	// WebSocketSharp.Net.InputState WebSocketSharp.Net.HttpConnection::_inputState
	int32_t ____inputState_6;
	// WebSocketSharp.Net.RequestStream WebSocketSharp.Net.HttpConnection::_inputStream
	RequestStream_t2929193945 * ____inputStream_7;
	// WebSocketSharp.Net.HttpListener WebSocketSharp.Net.HttpConnection::_lastListener
	HttpListener_t398944510 * ____lastListener_8;
	// WebSocketSharp.Net.LineState WebSocketSharp.Net.HttpConnection::_lineState
	int32_t ____lineState_9;
	// WebSocketSharp.Net.EndPointListener WebSocketSharp.Net.HttpConnection::_listener
	EndPointListener_t3188089579 * ____listener_10;
	// WebSocketSharp.Net.ResponseStream WebSocketSharp.Net.HttpConnection::_outputStream
	ResponseStream_t1796293571 * ____outputStream_11;
	// System.Int32 WebSocketSharp.Net.HttpConnection::_position
	int32_t ____position_12;
	// WebSocketSharp.Net.ListenerPrefix WebSocketSharp.Net.HttpConnection::_prefix
	ListenerPrefix_t2663314696 * ____prefix_13;
	// System.IO.MemoryStream WebSocketSharp.Net.HttpConnection::_requestBuffer
	MemoryStream_t418716369 * ____requestBuffer_14;
	// System.Int32 WebSocketSharp.Net.HttpConnection::_reuses
	int32_t ____reuses_15;
	// System.Boolean WebSocketSharp.Net.HttpConnection::_secure
	bool ____secure_16;
	// System.Net.Sockets.Socket WebSocketSharp.Net.HttpConnection::_socket
	Socket_t2157335841 * ____socket_17;
	// System.IO.Stream WebSocketSharp.Net.HttpConnection::_stream
	Stream_t1561764144 * ____stream_18;
	// System.Object WebSocketSharp.Net.HttpConnection::_sync
	Il2CppObject * ____sync_19;
	// System.Int32 WebSocketSharp.Net.HttpConnection::_timeout
	int32_t ____timeout_20;
	// System.Threading.Timer WebSocketSharp.Net.HttpConnection::_timer
	Timer_t1893171827 * ____timer_21;
	// WebSocketSharp.WebSocketStream WebSocketSharp.Net.HttpConnection::_websocketStream
	WebSocketStream_t4103435597 * ____websocketStream_22;

public:
	inline static int32_t get_offset_of__buffer_1() { return static_cast<int32_t>(offsetof(HttpConnection_t602292776, ____buffer_1)); }
	inline ByteU5BU5D_t4260760469* get__buffer_1() const { return ____buffer_1; }
	inline ByteU5BU5D_t4260760469** get_address_of__buffer_1() { return &____buffer_1; }
	inline void set__buffer_1(ByteU5BU5D_t4260760469* value)
	{
		____buffer_1 = value;
		Il2CppCodeGenWriteBarrier(&____buffer_1, value);
	}

	inline static int32_t get_offset_of__chunked_2() { return static_cast<int32_t>(offsetof(HttpConnection_t602292776, ____chunked_2)); }
	inline bool get__chunked_2() const { return ____chunked_2; }
	inline bool* get_address_of__chunked_2() { return &____chunked_2; }
	inline void set__chunked_2(bool value)
	{
		____chunked_2 = value;
	}

	inline static int32_t get_offset_of__context_3() { return static_cast<int32_t>(offsetof(HttpConnection_t602292776, ____context_3)); }
	inline HttpListenerContext_t3744659101 * get__context_3() const { return ____context_3; }
	inline HttpListenerContext_t3744659101 ** get_address_of__context_3() { return &____context_3; }
	inline void set__context_3(HttpListenerContext_t3744659101 * value)
	{
		____context_3 = value;
		Il2CppCodeGenWriteBarrier(&____context_3, value);
	}

	inline static int32_t get_offset_of__contextWasBound_4() { return static_cast<int32_t>(offsetof(HttpConnection_t602292776, ____contextWasBound_4)); }
	inline bool get__contextWasBound_4() const { return ____contextWasBound_4; }
	inline bool* get_address_of__contextWasBound_4() { return &____contextWasBound_4; }
	inline void set__contextWasBound_4(bool value)
	{
		____contextWasBound_4 = value;
	}

	inline static int32_t get_offset_of__currentLine_5() { return static_cast<int32_t>(offsetof(HttpConnection_t602292776, ____currentLine_5)); }
	inline StringBuilder_t243639308 * get__currentLine_5() const { return ____currentLine_5; }
	inline StringBuilder_t243639308 ** get_address_of__currentLine_5() { return &____currentLine_5; }
	inline void set__currentLine_5(StringBuilder_t243639308 * value)
	{
		____currentLine_5 = value;
		Il2CppCodeGenWriteBarrier(&____currentLine_5, value);
	}

	inline static int32_t get_offset_of__inputState_6() { return static_cast<int32_t>(offsetof(HttpConnection_t602292776, ____inputState_6)); }
	inline int32_t get__inputState_6() const { return ____inputState_6; }
	inline int32_t* get_address_of__inputState_6() { return &____inputState_6; }
	inline void set__inputState_6(int32_t value)
	{
		____inputState_6 = value;
	}

	inline static int32_t get_offset_of__inputStream_7() { return static_cast<int32_t>(offsetof(HttpConnection_t602292776, ____inputStream_7)); }
	inline RequestStream_t2929193945 * get__inputStream_7() const { return ____inputStream_7; }
	inline RequestStream_t2929193945 ** get_address_of__inputStream_7() { return &____inputStream_7; }
	inline void set__inputStream_7(RequestStream_t2929193945 * value)
	{
		____inputStream_7 = value;
		Il2CppCodeGenWriteBarrier(&____inputStream_7, value);
	}

	inline static int32_t get_offset_of__lastListener_8() { return static_cast<int32_t>(offsetof(HttpConnection_t602292776, ____lastListener_8)); }
	inline HttpListener_t398944510 * get__lastListener_8() const { return ____lastListener_8; }
	inline HttpListener_t398944510 ** get_address_of__lastListener_8() { return &____lastListener_8; }
	inline void set__lastListener_8(HttpListener_t398944510 * value)
	{
		____lastListener_8 = value;
		Il2CppCodeGenWriteBarrier(&____lastListener_8, value);
	}

	inline static int32_t get_offset_of__lineState_9() { return static_cast<int32_t>(offsetof(HttpConnection_t602292776, ____lineState_9)); }
	inline int32_t get__lineState_9() const { return ____lineState_9; }
	inline int32_t* get_address_of__lineState_9() { return &____lineState_9; }
	inline void set__lineState_9(int32_t value)
	{
		____lineState_9 = value;
	}

	inline static int32_t get_offset_of__listener_10() { return static_cast<int32_t>(offsetof(HttpConnection_t602292776, ____listener_10)); }
	inline EndPointListener_t3188089579 * get__listener_10() const { return ____listener_10; }
	inline EndPointListener_t3188089579 ** get_address_of__listener_10() { return &____listener_10; }
	inline void set__listener_10(EndPointListener_t3188089579 * value)
	{
		____listener_10 = value;
		Il2CppCodeGenWriteBarrier(&____listener_10, value);
	}

	inline static int32_t get_offset_of__outputStream_11() { return static_cast<int32_t>(offsetof(HttpConnection_t602292776, ____outputStream_11)); }
	inline ResponseStream_t1796293571 * get__outputStream_11() const { return ____outputStream_11; }
	inline ResponseStream_t1796293571 ** get_address_of__outputStream_11() { return &____outputStream_11; }
	inline void set__outputStream_11(ResponseStream_t1796293571 * value)
	{
		____outputStream_11 = value;
		Il2CppCodeGenWriteBarrier(&____outputStream_11, value);
	}

	inline static int32_t get_offset_of__position_12() { return static_cast<int32_t>(offsetof(HttpConnection_t602292776, ____position_12)); }
	inline int32_t get__position_12() const { return ____position_12; }
	inline int32_t* get_address_of__position_12() { return &____position_12; }
	inline void set__position_12(int32_t value)
	{
		____position_12 = value;
	}

	inline static int32_t get_offset_of__prefix_13() { return static_cast<int32_t>(offsetof(HttpConnection_t602292776, ____prefix_13)); }
	inline ListenerPrefix_t2663314696 * get__prefix_13() const { return ____prefix_13; }
	inline ListenerPrefix_t2663314696 ** get_address_of__prefix_13() { return &____prefix_13; }
	inline void set__prefix_13(ListenerPrefix_t2663314696 * value)
	{
		____prefix_13 = value;
		Il2CppCodeGenWriteBarrier(&____prefix_13, value);
	}

	inline static int32_t get_offset_of__requestBuffer_14() { return static_cast<int32_t>(offsetof(HttpConnection_t602292776, ____requestBuffer_14)); }
	inline MemoryStream_t418716369 * get__requestBuffer_14() const { return ____requestBuffer_14; }
	inline MemoryStream_t418716369 ** get_address_of__requestBuffer_14() { return &____requestBuffer_14; }
	inline void set__requestBuffer_14(MemoryStream_t418716369 * value)
	{
		____requestBuffer_14 = value;
		Il2CppCodeGenWriteBarrier(&____requestBuffer_14, value);
	}

	inline static int32_t get_offset_of__reuses_15() { return static_cast<int32_t>(offsetof(HttpConnection_t602292776, ____reuses_15)); }
	inline int32_t get__reuses_15() const { return ____reuses_15; }
	inline int32_t* get_address_of__reuses_15() { return &____reuses_15; }
	inline void set__reuses_15(int32_t value)
	{
		____reuses_15 = value;
	}

	inline static int32_t get_offset_of__secure_16() { return static_cast<int32_t>(offsetof(HttpConnection_t602292776, ____secure_16)); }
	inline bool get__secure_16() const { return ____secure_16; }
	inline bool* get_address_of__secure_16() { return &____secure_16; }
	inline void set__secure_16(bool value)
	{
		____secure_16 = value;
	}

	inline static int32_t get_offset_of__socket_17() { return static_cast<int32_t>(offsetof(HttpConnection_t602292776, ____socket_17)); }
	inline Socket_t2157335841 * get__socket_17() const { return ____socket_17; }
	inline Socket_t2157335841 ** get_address_of__socket_17() { return &____socket_17; }
	inline void set__socket_17(Socket_t2157335841 * value)
	{
		____socket_17 = value;
		Il2CppCodeGenWriteBarrier(&____socket_17, value);
	}

	inline static int32_t get_offset_of__stream_18() { return static_cast<int32_t>(offsetof(HttpConnection_t602292776, ____stream_18)); }
	inline Stream_t1561764144 * get__stream_18() const { return ____stream_18; }
	inline Stream_t1561764144 ** get_address_of__stream_18() { return &____stream_18; }
	inline void set__stream_18(Stream_t1561764144 * value)
	{
		____stream_18 = value;
		Il2CppCodeGenWriteBarrier(&____stream_18, value);
	}

	inline static int32_t get_offset_of__sync_19() { return static_cast<int32_t>(offsetof(HttpConnection_t602292776, ____sync_19)); }
	inline Il2CppObject * get__sync_19() const { return ____sync_19; }
	inline Il2CppObject ** get_address_of__sync_19() { return &____sync_19; }
	inline void set__sync_19(Il2CppObject * value)
	{
		____sync_19 = value;
		Il2CppCodeGenWriteBarrier(&____sync_19, value);
	}

	inline static int32_t get_offset_of__timeout_20() { return static_cast<int32_t>(offsetof(HttpConnection_t602292776, ____timeout_20)); }
	inline int32_t get__timeout_20() const { return ____timeout_20; }
	inline int32_t* get_address_of__timeout_20() { return &____timeout_20; }
	inline void set__timeout_20(int32_t value)
	{
		____timeout_20 = value;
	}

	inline static int32_t get_offset_of__timer_21() { return static_cast<int32_t>(offsetof(HttpConnection_t602292776, ____timer_21)); }
	inline Timer_t1893171827 * get__timer_21() const { return ____timer_21; }
	inline Timer_t1893171827 ** get_address_of__timer_21() { return &____timer_21; }
	inline void set__timer_21(Timer_t1893171827 * value)
	{
		____timer_21 = value;
		Il2CppCodeGenWriteBarrier(&____timer_21, value);
	}

	inline static int32_t get_offset_of__websocketStream_22() { return static_cast<int32_t>(offsetof(HttpConnection_t602292776, ____websocketStream_22)); }
	inline WebSocketStream_t4103435597 * get__websocketStream_22() const { return ____websocketStream_22; }
	inline WebSocketStream_t4103435597 ** get_address_of__websocketStream_22() { return &____websocketStream_22; }
	inline void set__websocketStream_22(WebSocketStream_t4103435597 * value)
	{
		____websocketStream_22 = value;
		Il2CppCodeGenWriteBarrier(&____websocketStream_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
