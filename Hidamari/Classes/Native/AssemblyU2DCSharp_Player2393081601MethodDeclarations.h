﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Player
struct Player_t2393081601;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.String
struct String_t;
// SocketIO.SocketIOEvent
struct SocketIOEvent_t4011854063;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_MoveJson4254904441.h"
#include "AssemblyU2DCSharp_CharData1499709504.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Direction1041377119.h"
#include "AssemblyU2DCSharp_SocketIO_SocketIOEvent4011854063.h"

// System.Void Player::.ctor()
extern "C"  void Player__ctor_m871706298 (Player_t2393081601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::Start()
extern "C"  void Player_Start_m4113811386 (Player_t2393081601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::Awake()
extern "C"  void Player_Awake_m1109311517 (Player_t2393081601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::set_FightPlayer(System.Boolean)
extern "C"  void Player_set_FightPlayer_m1219240259 (Player_t2393081601 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::set_StartTime(System.Single)
extern "C"  void Player_set_StartTime_m299678433 (Player_t2393081601 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::set_Play(System.Boolean)
extern "C"  void Player_set_Play_m4158030258 (Player_t2393081601 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::SetHP(UnityEngine.GameObject,UnityEngine.GameObject)
extern "C"  void Player_SetHP_m1004756484 (Player_t2393081601 * __this, GameObject_t3674682005 * ___hpgaug0, GameObject_t3674682005 * ___hptext1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::Update()
extern "C"  void Player_Update_m2979953555 (Player_t2393081601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::TopScreen(UnityEngine.Vector2)
extern "C"  void Player_TopScreen_m2856289505 (Player_t2393081601 * __this, Vector2_t4282066565  ___top0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Player::TriangleCollision(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  bool Player_TriangleCollision_m404750248 (Player_t2393081601 * __this, Vector2_t4282066565  ___a0, Vector2_t4282066565  ___b1, Vector2_t4282066565  ___c2, Vector2_t4282066565  ___p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::MoveStart()
extern "C"  void Player_MoveStart_m1977951017 (Player_t2393081601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::FightMoveStart()
extern "C"  void Player_FightMoveStart_m140468395 (Player_t2393081601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Player::DelayMove(MoveJson)
extern "C"  bool Player_DelayMove_m4031127487 (Player_t2393081601 * __this, MoveJson_t4254904441  ___mj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::Move()
extern "C"  void Player_Move_m3420605147 (Player_t2393081601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::Flashing()
extern "C"  void Player_Flashing_m2398756924 (Player_t2393081601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::RecoveryRound()
extern "C"  void Player_RecoveryRound_m2757011857 (Player_t2393081601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::ChangeHP()
extern "C"  void Player_ChangeHP_m1452152194 (Player_t2393081601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::CreateCharctorData(CharData,System.String)
extern "C"  void Player_CreateCharctorData_m295994710 (Player_t2393081601 * __this, CharData_t1499709504  ___ca0, String_t* ___str1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Player::GetHP()
extern "C"  int32_t Player_GetHP_m1356279656 (Player_t2393081601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::HitBullet(System.Int32,Direction)
extern "C"  void Player_HitBullet_m4273519307 (Player_t2393081601 * __this, int32_t ___damage0, int32_t ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::StartParticle()
extern "C"  void Player_StartParticle_m2350039008 (Player_t2393081601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::DamegeParticle()
extern "C"  void Player_DamegeParticle_m3462684643 (Player_t2393081601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::Massage(SocketIO.SocketIOEvent)
extern "C"  void Player_Massage_m2752818679 (Player_t2393081601 * __this, SocketIOEvent_t4011854063 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::MoveParticle()
extern "C"  void Player_MoveParticle_m4133523969 (Player_t2393081601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
