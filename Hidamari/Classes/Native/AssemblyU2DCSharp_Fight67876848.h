﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_FixedID820738927.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Fight
struct  Fight_t67876848  : public MonoBehaviour_t667441552
{
public:
	// System.String Fight::_roomid
	String_t* ____roomid_2;
	// FixedID Fight::_enemyfid
	FixedID_t820738927  ____enemyfid_3;
	// System.Boolean Fight::_attack
	bool ____attack_4;

public:
	inline static int32_t get_offset_of__roomid_2() { return static_cast<int32_t>(offsetof(Fight_t67876848, ____roomid_2)); }
	inline String_t* get__roomid_2() const { return ____roomid_2; }
	inline String_t** get_address_of__roomid_2() { return &____roomid_2; }
	inline void set__roomid_2(String_t* value)
	{
		____roomid_2 = value;
		Il2CppCodeGenWriteBarrier(&____roomid_2, value);
	}

	inline static int32_t get_offset_of__enemyfid_3() { return static_cast<int32_t>(offsetof(Fight_t67876848, ____enemyfid_3)); }
	inline FixedID_t820738927  get__enemyfid_3() const { return ____enemyfid_3; }
	inline FixedID_t820738927 * get_address_of__enemyfid_3() { return &____enemyfid_3; }
	inline void set__enemyfid_3(FixedID_t820738927  value)
	{
		____enemyfid_3 = value;
	}

	inline static int32_t get_offset_of__attack_4() { return static_cast<int32_t>(offsetof(Fight_t67876848, ____attack_4)); }
	inline bool get__attack_4() const { return ____attack_4; }
	inline bool* get_address_of__attack_4() { return &____attack_4; }
	inline void set__attack_4(bool value)
	{
		____attack_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
