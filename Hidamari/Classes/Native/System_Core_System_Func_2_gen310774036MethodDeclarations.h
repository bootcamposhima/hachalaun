﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen3498845318MethodDeclarations.h"

// System.Void System.Func`2<WebSocketSharp.Net.HttpListenerRequest,WebSocketSharp.Net.AuthenticationSchemes>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m2067510317(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t310774036 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m3049940406_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<WebSocketSharp.Net.HttpListenerRequest,WebSocketSharp.Net.AuthenticationSchemes>::Invoke(T)
#define Func_2_Invoke_m2741384693(__this, ___arg10, method) ((  int32_t (*) (Func_2_t310774036 *, HttpListenerRequest_t3888821117 *, const MethodInfo*))Func_2_Invoke_m2795163504_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<WebSocketSharp.Net.HttpListenerRequest,WebSocketSharp.Net.AuthenticationSchemes>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m1565557828(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t310774036 *, HttpListenerRequest_t3888821117 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m1692252707_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<WebSocketSharp.Net.HttpListenerRequest,WebSocketSharp.Net.AuthenticationSchemes>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m1137842911(__this, ___result0, method) ((  int32_t (*) (Func_2_t310774036 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m2564979300_gshared)(__this, ___result0, method)
