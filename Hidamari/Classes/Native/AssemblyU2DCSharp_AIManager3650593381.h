﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "AssemblyU2DCSharp_UIManager1861242489.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AIManager
struct  AIManager_t3650593381  : public UIManager_t1861242489
{
public:
	// UnityEngine.GameObject AIManager::_button
	GameObject_t3674682005 * ____button_7;
	// UnityEngine.GameObject AIManager::_scroll
	GameObject_t3674682005 * ____scroll_8;
	// UnityEngine.GameObject AIManager::_scrollbullet
	GameObject_t3674682005 * ____scrollbullet_9;
	// UnityEngine.Vector3 AIManager::_scrollfirstpos
	Vector3_t4282066566  ____scrollfirstpos_10;
	// UnityEngine.Vector3 AIManager::_scrollsecpundpos
	Vector3_t4282066566  ____scrollsecpundpos_11;
	// UnityEngine.Vector3 AIManager::_scrolldestination
	Vector3_t4282066566  ____scrolldestination_12;
	// UnityEngine.GameObject AIManager::_moveobjrct
	GameObject_t3674682005 * ____moveobjrct_13;
	// System.Single AIManager::_scrollspd
	float ____scrollspd_14;
	// System.Boolean AIManager::_scrollismove
	bool ____scrollismove_15;
	// System.Boolean AIManager::istop
	bool ___istop_16;
	// System.Boolean AIManager::isre
	bool ___isre_17;
	// System.Boolean AIManager::ismsb
	bool ___ismsb_18;

public:
	inline static int32_t get_offset_of__button_7() { return static_cast<int32_t>(offsetof(AIManager_t3650593381, ____button_7)); }
	inline GameObject_t3674682005 * get__button_7() const { return ____button_7; }
	inline GameObject_t3674682005 ** get_address_of__button_7() { return &____button_7; }
	inline void set__button_7(GameObject_t3674682005 * value)
	{
		____button_7 = value;
		Il2CppCodeGenWriteBarrier(&____button_7, value);
	}

	inline static int32_t get_offset_of__scroll_8() { return static_cast<int32_t>(offsetof(AIManager_t3650593381, ____scroll_8)); }
	inline GameObject_t3674682005 * get__scroll_8() const { return ____scroll_8; }
	inline GameObject_t3674682005 ** get_address_of__scroll_8() { return &____scroll_8; }
	inline void set__scroll_8(GameObject_t3674682005 * value)
	{
		____scroll_8 = value;
		Il2CppCodeGenWriteBarrier(&____scroll_8, value);
	}

	inline static int32_t get_offset_of__scrollbullet_9() { return static_cast<int32_t>(offsetof(AIManager_t3650593381, ____scrollbullet_9)); }
	inline GameObject_t3674682005 * get__scrollbullet_9() const { return ____scrollbullet_9; }
	inline GameObject_t3674682005 ** get_address_of__scrollbullet_9() { return &____scrollbullet_9; }
	inline void set__scrollbullet_9(GameObject_t3674682005 * value)
	{
		____scrollbullet_9 = value;
		Il2CppCodeGenWriteBarrier(&____scrollbullet_9, value);
	}

	inline static int32_t get_offset_of__scrollfirstpos_10() { return static_cast<int32_t>(offsetof(AIManager_t3650593381, ____scrollfirstpos_10)); }
	inline Vector3_t4282066566  get__scrollfirstpos_10() const { return ____scrollfirstpos_10; }
	inline Vector3_t4282066566 * get_address_of__scrollfirstpos_10() { return &____scrollfirstpos_10; }
	inline void set__scrollfirstpos_10(Vector3_t4282066566  value)
	{
		____scrollfirstpos_10 = value;
	}

	inline static int32_t get_offset_of__scrollsecpundpos_11() { return static_cast<int32_t>(offsetof(AIManager_t3650593381, ____scrollsecpundpos_11)); }
	inline Vector3_t4282066566  get__scrollsecpundpos_11() const { return ____scrollsecpundpos_11; }
	inline Vector3_t4282066566 * get_address_of__scrollsecpundpos_11() { return &____scrollsecpundpos_11; }
	inline void set__scrollsecpundpos_11(Vector3_t4282066566  value)
	{
		____scrollsecpundpos_11 = value;
	}

	inline static int32_t get_offset_of__scrolldestination_12() { return static_cast<int32_t>(offsetof(AIManager_t3650593381, ____scrolldestination_12)); }
	inline Vector3_t4282066566  get__scrolldestination_12() const { return ____scrolldestination_12; }
	inline Vector3_t4282066566 * get_address_of__scrolldestination_12() { return &____scrolldestination_12; }
	inline void set__scrolldestination_12(Vector3_t4282066566  value)
	{
		____scrolldestination_12 = value;
	}

	inline static int32_t get_offset_of__moveobjrct_13() { return static_cast<int32_t>(offsetof(AIManager_t3650593381, ____moveobjrct_13)); }
	inline GameObject_t3674682005 * get__moveobjrct_13() const { return ____moveobjrct_13; }
	inline GameObject_t3674682005 ** get_address_of__moveobjrct_13() { return &____moveobjrct_13; }
	inline void set__moveobjrct_13(GameObject_t3674682005 * value)
	{
		____moveobjrct_13 = value;
		Il2CppCodeGenWriteBarrier(&____moveobjrct_13, value);
	}

	inline static int32_t get_offset_of__scrollspd_14() { return static_cast<int32_t>(offsetof(AIManager_t3650593381, ____scrollspd_14)); }
	inline float get__scrollspd_14() const { return ____scrollspd_14; }
	inline float* get_address_of__scrollspd_14() { return &____scrollspd_14; }
	inline void set__scrollspd_14(float value)
	{
		____scrollspd_14 = value;
	}

	inline static int32_t get_offset_of__scrollismove_15() { return static_cast<int32_t>(offsetof(AIManager_t3650593381, ____scrollismove_15)); }
	inline bool get__scrollismove_15() const { return ____scrollismove_15; }
	inline bool* get_address_of__scrollismove_15() { return &____scrollismove_15; }
	inline void set__scrollismove_15(bool value)
	{
		____scrollismove_15 = value;
	}

	inline static int32_t get_offset_of_istop_16() { return static_cast<int32_t>(offsetof(AIManager_t3650593381, ___istop_16)); }
	inline bool get_istop_16() const { return ___istop_16; }
	inline bool* get_address_of_istop_16() { return &___istop_16; }
	inline void set_istop_16(bool value)
	{
		___istop_16 = value;
	}

	inline static int32_t get_offset_of_isre_17() { return static_cast<int32_t>(offsetof(AIManager_t3650593381, ___isre_17)); }
	inline bool get_isre_17() const { return ___isre_17; }
	inline bool* get_address_of_isre_17() { return &___isre_17; }
	inline void set_isre_17(bool value)
	{
		___isre_17 = value;
	}

	inline static int32_t get_offset_of_ismsb_18() { return static_cast<int32_t>(offsetof(AIManager_t3650593381, ___ismsb_18)); }
	inline bool get_ismsb_18() const { return ___ismsb_18; }
	inline bool* get_address_of_ismsb_18() { return &___ismsb_18; }
	inline void set_ismsb_18(bool value)
	{
		___ismsb_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
