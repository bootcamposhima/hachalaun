﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_636475144.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Char>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m274731848_gshared (KeyValuePair_2_t636475144 * __this, Il2CppObject * ___key0, Il2CppChar ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m274731848(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t636475144 *, Il2CppObject *, Il2CppChar, const MethodInfo*))KeyValuePair_2__ctor_m274731848_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Char>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2659847968_gshared (KeyValuePair_2_t636475144 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m2659847968(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t636475144 *, const MethodInfo*))KeyValuePair_2_get_Key_m2659847968_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Char>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1114790369_gshared (KeyValuePair_2_t636475144 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m1114790369(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t636475144 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Key_m1114790369_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Char>::get_Value()
extern "C"  Il2CppChar KeyValuePair_2_get_Value_m199928900_gshared (KeyValuePair_2_t636475144 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m199928900(__this, method) ((  Il2CppChar (*) (KeyValuePair_2_t636475144 *, const MethodInfo*))KeyValuePair_2_get_Value_m199928900_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Char>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2690372961_gshared (KeyValuePair_2_t636475144 * __this, Il2CppChar ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m2690372961(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t636475144 *, Il2CppChar, const MethodInfo*))KeyValuePair_2_set_Value_m2690372961_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Char>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2050642311_gshared (KeyValuePair_2_t636475144 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m2050642311(__this, method) ((  String_t* (*) (KeyValuePair_2_t636475144 *, const MethodInfo*))KeyValuePair_2_ToString_m2050642311_gshared)(__this, method)
