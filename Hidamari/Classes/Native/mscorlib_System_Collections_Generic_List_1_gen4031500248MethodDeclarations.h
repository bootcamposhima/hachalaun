﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1244034627MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::.ctor()
#define List_1__ctor_m983277857(__this, method) ((  void (*) (List_1_t4031500248 *, const MethodInfo*))List_1__ctor_m3048469268_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m2240247355(__this, ___collection0, method) ((  void (*) (List_1_t4031500248 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1160795371_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::.ctor(System.Int32)
#define List_1__ctor_m4165883273(__this, ___capacity0, method) ((  void (*) (List_1_t4031500248 *, int32_t, const MethodInfo*))List_1__ctor_m3643386469_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::.cctor()
#define List_1__cctor_m4104461941(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3826137881_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2645734794(__this, method) ((  Il2CppObject* (*) (List_1_t4031500248 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2808422246_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m204961804(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t4031500248 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m4034025648_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m489804039(__this, method) ((  Il2CppObject * (*) (List_1_t4031500248 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1841330603_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m3954125898(__this, ___item0, method) ((  int32_t (*) (List_1_t4031500248 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3794749222_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m3043478210(__this, ___item0, method) ((  bool (*) (List_1_t4031500248 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2659633254_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m2609655202(__this, ___item0, method) ((  int32_t (*) (List_1_t4031500248 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3431692926_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m1772074189(__this, ___index0, ___item1, method) ((  void (*) (List_1_t4031500248 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2067529129_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m1772956475(__this, ___item0, method) ((  void (*) (List_1_t4031500248 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1644145887_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1252962371(__this, method) ((  bool (*) (List_1_t4031500248 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1299706087_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1494711642(__this, method) ((  bool (*) (List_1_t4031500248 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3867536694_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m18851398(__this, method) ((  Il2CppObject * (*) (List_1_t4031500248 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m4244374434_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m617034673(__this, method) ((  bool (*) (List_1_t4031500248 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m432946261_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m3106312488(__this, method) ((  bool (*) (List_1_t4031500248 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2961826820_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m2001935949(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t4031500248 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3985478825_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m2770198884(__this, ___index0, ___value1, method) ((  void (*) (List_1_t4031500248 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3234554688_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::Add(T)
#define List_1_Add_m677638673(__this, ___item0, method) ((  void (*) (List_1_t4031500248 *, ListenerPrefix_t2663314696 *, const MethodInfo*))List_1_Add_m642669291_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m1253648898(__this, ___newCount0, method) ((  void (*) (List_1_t4031500248 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m4122600870_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m3023108352(__this, ___collection0, method) ((  void (*) (List_1_t4031500248 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2478449828_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m2284080576(__this, ___enumerable0, method) ((  void (*) (List_1_t4031500248 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1739422052_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m429066455(__this, ___collection0, method) ((  void (*) (List_1_t4031500248 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2229151411_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::AsReadOnly()
#define List_1_AsReadOnly_m3805933170(__this, method) ((  ReadOnlyCollection_1_t4220392232 * (*) (List_1_t4031500248 *, const MethodInfo*))List_1_AsReadOnly_m769820182_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::Clear()
#define List_1_Clear_m1017770083(__this, method) ((  void (*) (List_1_t4031500248 *, const MethodInfo*))List_1_Clear_m454602559_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::Contains(T)
#define List_1_Contains_m2590128401(__this, ___item0, method) ((  bool (*) (List_1_t4031500248 *, ListenerPrefix_t2663314696 *, const MethodInfo*))List_1_Contains_m4186092781_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m2939562359(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t4031500248 *, ListenerPrefixU5BU5D_t3459952089*, int32_t, const MethodInfo*))List_1_CopyTo_m3988356635_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::Find(System.Predicate`1<T>)
#define List_1_Find_m711421073(__this, ___match0, method) ((  ListenerPrefix_t2663314696 * (*) (List_1_t4031500248 *, Predicate_1_t2274371579 *, const MethodInfo*))List_1_Find_m3379773421_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m3440225100(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2274371579 *, const MethodInfo*))List_1_CheckMatch_m3390394152_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m3875830897(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t4031500248 *, int32_t, int32_t, Predicate_1_t2274371579 *, const MethodInfo*))List_1_GetIndex_m4275988045_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::GetEnumerator()
#define List_1_GetEnumerator_m1910600292(__this, method) ((  Enumerator_t4051173018  (*) (List_1_t4031500248 *, const MethodInfo*))List_1_GetEnumerator_m2326457258_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::IndexOf(T)
#define List_1_IndexOf_m3823066427(__this, ___item0, method) ((  int32_t (*) (List_1_t4031500248 *, ListenerPrefix_t2663314696 *, const MethodInfo*))List_1_IndexOf_m1752303327_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m1730451662(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t4031500248 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3807054194_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m2685929543(__this, ___index0, method) ((  void (*) (List_1_t4031500248 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3734723819_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::Insert(System.Int32,T)
#define List_1_Insert_m4167971246(__this, ___index0, ___item1, method) ((  void (*) (List_1_t4031500248 *, int32_t, ListenerPrefix_t2663314696 *, const MethodInfo*))List_1_Insert_m3427163986_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m2335902627(__this, ___collection0, method) ((  void (*) (List_1_t4031500248 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2905071175_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::Remove(T)
#define List_1_Remove_m4198762828(__this, ___item0, method) ((  bool (*) (List_1_t4031500248 *, ListenerPrefix_t2663314696 *, const MethodInfo*))List_1_Remove_m2747911208_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m2111092414(__this, ___match0, method) ((  int32_t (*) (List_1_t4031500248 *, Predicate_1_t2274371579 *, const MethodInfo*))List_1_RemoveAll_m2933443938_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m385533803(__this, ___index0, method) ((  void (*) (List_1_t4031500248 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1301016856_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::Reverse()
#define List_1_Reverse_m487193208(__this, method) ((  void (*) (List_1_t4031500248 *, const MethodInfo*))List_1_Reverse_m449081940_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::Sort()
#define List_1_Sort_m2987923498(__this, method) ((  void (*) (List_1_t4031500248 *, const MethodInfo*))List_1_Sort_m1168641486_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m203376893(__this, ___comparison0, method) ((  void (*) (List_1_t4031500248 *, Comparison_1_t1379675883 *, const MethodInfo*))List_1_Sort_m4192185249_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::ToArray()
#define List_1_ToArray_m3398261751(__this, method) ((  ListenerPrefixU5BU5D_t3459952089* (*) (List_1_t4031500248 *, const MethodInfo*))List_1_ToArray_m238588755_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::TrimExcess()
#define List_1_TrimExcess_m3952799811(__this, method) ((  void (*) (List_1_t4031500248 *, const MethodInfo*))List_1_TrimExcess_m2451380967_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::get_Capacity()
#define List_1_get_Capacity_m3729276907(__this, method) ((  int32_t (*) (List_1_t4031500248 *, const MethodInfo*))List_1_get_Capacity_m543520655_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m2758805012(__this, ___value0, method) ((  void (*) (List_1_t4031500248 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1332789688_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::get_Count()
#define List_1_get_Count_m2827934807(__this, method) ((  int32_t (*) (List_1_t4031500248 *, const MethodInfo*))List_1_get_Count_m2599103100_gshared)(__this, method)
// T System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::get_Item(System.Int32)
#define List_1_get_Item_m3147397726(__this, ___index0, method) ((  ListenerPrefix_t2663314696 * (*) (List_1_t4031500248 *, int32_t, const MethodInfo*))List_1_get_Item_m2771401372_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<WebSocketSharp.Net.ListenerPrefix>::set_Item(System.Int32,T)
#define List_1_set_Item_m25476869(__this, ___index0, ___value1, method) ((  void (*) (List_1_t4031500248 *, int32_t, ListenerPrefix_t2663314696 *, const MethodInfo*))List_1_set_Item_m1074271145_gshared)(__this, ___index0, ___value1, method)
