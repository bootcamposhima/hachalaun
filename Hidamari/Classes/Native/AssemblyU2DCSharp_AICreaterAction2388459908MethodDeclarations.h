﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AICreaterAction
struct AICreaterAction_t2388459908;

#include "codegen/il2cpp-codegen.h"

// System.Void AICreaterAction::.ctor()
extern "C"  void AICreaterAction__ctor_m1538931303 (AICreaterAction_t2388459908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICreaterAction::Start()
extern "C"  void AICreaterAction_Start_m486069095 (AICreaterAction_t2388459908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICreaterAction::Update()
extern "C"  void AICreaterAction_Update_m2189092230 (AICreaterAction_t2388459908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICreaterAction::ActionChoice(System.Int32)
extern "C"  void AICreaterAction_ActionChoice_m1320016645 (AICreaterAction_t2388459908 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICreaterAction::ActionStart()
extern "C"  void AICreaterAction_ActionStart_m3092427089 (AICreaterAction_t2388459908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICreaterAction::ActionStop()
extern "C"  void AICreaterAction_ActionStop_m931452501 (AICreaterAction_t2388459908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICreaterAction::Defoultpos()
extern "C"  void AICreaterAction_Defoultpos_m3549152958 (AICreaterAction_t2388459908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
