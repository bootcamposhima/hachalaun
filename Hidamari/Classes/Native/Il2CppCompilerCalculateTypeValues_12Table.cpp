﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityP2935569768.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityP1707327472.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ServerContex5810494.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ServerRec1063474890.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Validation925859732.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslClient2101301545.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslCipher1471718687.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslHandsh3344762007.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslServer1149375137.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslStreamB657895919.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslStream2363113060.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsCipher3923560144.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsClient3675869755.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsExcept1031360050.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsServer3702625715.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsStream89550253.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3167042548.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake2623608376.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3359671455.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_617688751.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake4004312584.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3760597130.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake2038752746.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3687755450.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3135543351.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3188160136.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3787353090.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake1453856114.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake2604642324.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake1910642754.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_124657119.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_646324024.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_187975082.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake1051859738.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3194723818.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake2642511719.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake2500664744.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_214731042.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_466963106.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3853443396.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake1417611122.h"
#include "Mono_Security_Mono_Math_Prime_PrimalityTest2979531601.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Certifica3090555431.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Certifica3353735195.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Certificat604475832.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_PrivateKe3559239239.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E3053238933.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U2435538904.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U1676616792.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U3379220377.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U3379220410.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U3379220447.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U3379220505.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U3379220348.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U3379220352.h"
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U3988332409.h"
#include "System_Core_U3CModuleU3E86524790.h"
#include "System_Core_System_Runtime_CompilerServices_Extens2299149759.h"
#include "System_Core_Locale2281372282.h"
#include "System_Core_System_MonoTODOAttribute2091695241.h"
#include "System_Core_Mono_Security_Cryptography_KeyBuilder373726640.h"
#include "System_Core_Mono_Security_Cryptography_SymmetricTra131863657.h"
#include "System_Core_System_Linq_Check10677726.h"
#include "System_Core_System_Linq_Enumerable839044124.h"
#include "System_Core_System_Security_Cryptography_Aes2466798581.h"
#include "System_Core_System_Security_Cryptography_AesManaged23175804.h"
#include "System_Core_System_Security_Cryptography_AesTransf1787635017.h"
#include "System_Core_System_Action3771233898.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E3053238933.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U241676615769.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U241676615732.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U241676616792.h"
#include "System_Core_U3CPrivateImplementationDetailsU3E_U24A435478332.h"
#include "UnityEngine_U3CModuleU3E86524790.h"
#include "UnityEngine_UnityEngine_AssetBundleCreateRequest1416890373.h"
#include "UnityEngine_UnityEngine_AssetBundleCreateRequest_Co561206607.h"
#include "UnityEngine_UnityEngine_AssetBundleRequest2154290273.h"
#include "UnityEngine_UnityEngine_AssetBundle2070959688.h"
#include "UnityEngine_UnityEngine_SendMessageOptions3856946179.h"
#include "UnityEngine_UnityEngine_RuntimePlatform3050318497.h"
#include "UnityEngine_UnityEngine_LogType4286006228.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1615819279.h"
#include "UnityEngine_UnityEngine_WaitForFixedUpdate2130080621.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame2372756133.h"
#include "UnityEngine_UnityEngine_CustomYieldInstruction2666549910.h"
#include "UnityEngine_UnityEngine_Coroutine3621161934.h"
#include "UnityEngine_UnityEngine_ScriptableObject2970544072.h"
#include "UnityEngine_UnityEngine_UnhandledExceptionHandler1700300692.h"
#include "UnityEngine_UnityEngine_CursorLockMode1155278888.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1200 = { sizeof (SecurityParameters_t2935569768), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1200[3] = 
{
	SecurityParameters_t2935569768::get_offset_of_cipher_0(),
	SecurityParameters_t2935569768::get_offset_of_clientWriteMAC_1(),
	SecurityParameters_t2935569768::get_offset_of_serverWriteMAC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1201 = { sizeof (SecurityProtocolType_t1707327472)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1201[5] = 
{
	SecurityProtocolType_t1707327472::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1202 = { sizeof (ServerContext_t5810494), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1202[3] = 
{
	ServerContext_t5810494::get_offset_of_sslStream_30(),
	ServerContext_t5810494::get_offset_of_request_client_certificate_31(),
	ServerContext_t5810494::get_offset_of_clientCertificateRequired_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1203 = { sizeof (ServerRecordProtocol_t1063474890), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1204 = { sizeof (ValidationResult_t925859732), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1204[2] = 
{
	ValidationResult_t925859732::get_offset_of_trusted_0(),
	ValidationResult_t925859732::get_offset_of_error_code_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1205 = { sizeof (SslClientStream_t2101301545), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1205[4] = 
{
	SslClientStream_t2101301545::get_offset_of_ServerCertValidation_16(),
	SslClientStream_t2101301545::get_offset_of_ClientCertSelection_17(),
	SslClientStream_t2101301545::get_offset_of_PrivateKeySelection_18(),
	SslClientStream_t2101301545::get_offset_of_ServerCertValidation2_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1206 = { sizeof (SslCipherSuite_t1471718687), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1206[3] = 
{
	SslCipherSuite_t1471718687::get_offset_of_pad1_21(),
	SslCipherSuite_t1471718687::get_offset_of_pad2_22(),
	SslCipherSuite_t1471718687::get_offset_of_header_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1207 = { sizeof (SslHandshakeHash_t3344762007), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1207[8] = 
{
	SslHandshakeHash_t3344762007::get_offset_of_md5_4(),
	SslHandshakeHash_t3344762007::get_offset_of_sha_5(),
	SslHandshakeHash_t3344762007::get_offset_of_hashing_6(),
	SslHandshakeHash_t3344762007::get_offset_of_secret_7(),
	SslHandshakeHash_t3344762007::get_offset_of_innerPadMD5_8(),
	SslHandshakeHash_t3344762007::get_offset_of_outerPadMD5_9(),
	SslHandshakeHash_t3344762007::get_offset_of_innerPadSHA_10(),
	SslHandshakeHash_t3344762007::get_offset_of_outerPadSHA_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1208 = { sizeof (SslServerStream_t1149375137), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1208[3] = 
{
	SslServerStream_t1149375137::get_offset_of_ClientCertValidation_16(),
	SslServerStream_t1149375137::get_offset_of_PrivateKeySelection_17(),
	SslServerStream_t1149375137::get_offset_of_ClientCertValidation2_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1209 = { sizeof (SslStreamBase_t657895919), -1, sizeof(SslStreamBase_t657895919_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1209[15] = 
{
	0,
	SslStreamBase_t657895919_StaticFields::get_offset_of_record_processing_2(),
	SslStreamBase_t657895919::get_offset_of_innerStream_3(),
	SslStreamBase_t657895919::get_offset_of_inputBuffer_4(),
	SslStreamBase_t657895919::get_offset_of_context_5(),
	SslStreamBase_t657895919::get_offset_of_protocol_6(),
	SslStreamBase_t657895919::get_offset_of_ownsStream_7(),
	SslStreamBase_t657895919::get_offset_of_disposed_8(),
	SslStreamBase_t657895919::get_offset_of_checkCertRevocationStatus_9(),
	SslStreamBase_t657895919::get_offset_of_negotiate_10(),
	SslStreamBase_t657895919::get_offset_of_read_11(),
	SslStreamBase_t657895919::get_offset_of_write_12(),
	SslStreamBase_t657895919::get_offset_of_negotiationComplete_13(),
	SslStreamBase_t657895919::get_offset_of_recbuf_14(),
	SslStreamBase_t657895919::get_offset_of_recordStream_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1210 = { sizeof (InternalAsyncResult_t2363113060), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1210[12] = 
{
	InternalAsyncResult_t2363113060::get_offset_of_locker_0(),
	InternalAsyncResult_t2363113060::get_offset_of__userCallback_1(),
	InternalAsyncResult_t2363113060::get_offset_of__userState_2(),
	InternalAsyncResult_t2363113060::get_offset_of__asyncException_3(),
	InternalAsyncResult_t2363113060::get_offset_of_handle_4(),
	InternalAsyncResult_t2363113060::get_offset_of_completed_5(),
	InternalAsyncResult_t2363113060::get_offset_of__bytesRead_6(),
	InternalAsyncResult_t2363113060::get_offset_of__fromWrite_7(),
	InternalAsyncResult_t2363113060::get_offset_of__proceedAfterHandshake_8(),
	InternalAsyncResult_t2363113060::get_offset_of__buffer_9(),
	InternalAsyncResult_t2363113060::get_offset_of__offset_10(),
	InternalAsyncResult_t2363113060::get_offset_of__count_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1211 = { sizeof (TlsCipherSuite_t3923560144), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1211[2] = 
{
	TlsCipherSuite_t3923560144::get_offset_of_header_21(),
	TlsCipherSuite_t3923560144::get_offset_of_headerLock_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1212 = { sizeof (TlsClientSettings_t3675869755), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1212[4] = 
{
	TlsClientSettings_t3675869755::get_offset_of_targetHost_0(),
	TlsClientSettings_t3675869755::get_offset_of_certificates_1(),
	TlsClientSettings_t3675869755::get_offset_of_clientCertificate_2(),
	TlsClientSettings_t3675869755::get_offset_of_certificateRSA_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1213 = { sizeof (TlsException_t1031360050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1213[1] = 
{
	TlsException_t1031360050::get_offset_of_alert_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1214 = { sizeof (TlsServerSettings_t3702625715), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1214[8] = 
{
	TlsServerSettings_t3702625715::get_offset_of_certificates_0(),
	TlsServerSettings_t3702625715::get_offset_of_certificateRSA_1(),
	TlsServerSettings_t3702625715::get_offset_of_rsaParameters_2(),
	TlsServerSettings_t3702625715::get_offset_of_signedParams_3(),
	TlsServerSettings_t3702625715::get_offset_of_distinguisedNames_4(),
	TlsServerSettings_t3702625715::get_offset_of_serverKeyExchange_5(),
	TlsServerSettings_t3702625715::get_offset_of_certificateRequest_6(),
	TlsServerSettings_t3702625715::get_offset_of_certificateTypes_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1215 = { sizeof (TlsStream_t89550253), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1215[4] = 
{
	TlsStream_t89550253::get_offset_of_canRead_1(),
	TlsStream_t89550253::get_offset_of_canWrite_2(),
	TlsStream_t89550253::get_offset_of_buffer_3(),
	TlsStream_t89550253::get_offset_of_temp_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1216 = { sizeof (ClientCertificateType_t3167042548)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1216[6] = 
{
	ClientCertificateType_t3167042548::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1217 = { sizeof (HandshakeMessage_t2623608376), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1217[4] = 
{
	HandshakeMessage_t2623608376::get_offset_of_context_5(),
	HandshakeMessage_t2623608376::get_offset_of_handshakeType_6(),
	HandshakeMessage_t2623608376::get_offset_of_contentType_7(),
	HandshakeMessage_t2623608376::get_offset_of_cache_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1218 = { sizeof (HandshakeType_t3359671455)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1218[12] = 
{
	HandshakeType_t3359671455::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1219 = { sizeof (TlsClientCertificate_t617688751), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1219[2] = 
{
	TlsClientCertificate_t617688751::get_offset_of_clientCertSelected_9(),
	TlsClientCertificate_t617688751::get_offset_of_clientCert_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1220 = { sizeof (TlsClientCertificateVerify_t4004312584), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1221 = { sizeof (TlsClientFinished_t3760597130), -1, sizeof(TlsClientFinished_t3760597130_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1221[1] = 
{
	TlsClientFinished_t3760597130_StaticFields::get_offset_of_Ssl3Marker_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1222 = { sizeof (TlsClientHello_t2038752746), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1222[1] = 
{
	TlsClientHello_t2038752746::get_offset_of_random_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1223 = { sizeof (TlsClientKeyExchange_t3687755450), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1224 = { sizeof (TlsServerCertificate_t3135543351), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1224[1] = 
{
	TlsServerCertificate_t3135543351::get_offset_of_certificates_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1225 = { sizeof (TlsServerCertificateRequest_t3188160136), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1225[2] = 
{
	TlsServerCertificateRequest_t3188160136::get_offset_of_certificateTypes_9(),
	TlsServerCertificateRequest_t3188160136::get_offset_of_distinguisedNames_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1226 = { sizeof (TlsServerFinished_t3787353090), -1, sizeof(TlsServerFinished_t3787353090_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1226[1] = 
{
	TlsServerFinished_t3787353090_StaticFields::get_offset_of_Ssl3Marker_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1227 = { sizeof (TlsServerHello_t1453856114), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1227[4] = 
{
	TlsServerHello_t1453856114::get_offset_of_compressionMethod_9(),
	TlsServerHello_t1453856114::get_offset_of_random_10(),
	TlsServerHello_t1453856114::get_offset_of_sessionId_11(),
	TlsServerHello_t1453856114::get_offset_of_cipherSuite_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1228 = { sizeof (TlsServerHelloDone_t2604642324), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1229 = { sizeof (TlsServerKeyExchange_t1910642754), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1229[2] = 
{
	TlsServerKeyExchange_t1910642754::get_offset_of_rsaParams_9(),
	TlsServerKeyExchange_t1910642754::get_offset_of_signedParams_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1230 = { sizeof (TlsClientCertificate_t124657119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1230[1] = 
{
	TlsClientCertificate_t124657119::get_offset_of_clientCertificates_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1231 = { sizeof (TlsClientCertificateVerify_t646324024), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1232 = { sizeof (TlsClientFinished_t187975082), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1233 = { sizeof (TlsClientHello_t1051859738), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1233[4] = 
{
	TlsClientHello_t1051859738::get_offset_of_random_9(),
	TlsClientHello_t1051859738::get_offset_of_sessionId_10(),
	TlsClientHello_t1051859738::get_offset_of_cipherSuites_11(),
	TlsClientHello_t1051859738::get_offset_of_compressionMethods_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1234 = { sizeof (TlsClientKeyExchange_t3194723818), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1235 = { sizeof (TlsServerCertificate_t2642511719), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1236 = { sizeof (TlsServerCertificateRequest_t2500664744), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1237 = { sizeof (TlsServerFinished_t214731042), -1, sizeof(TlsServerFinished_t214731042_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1237[1] = 
{
	TlsServerFinished_t214731042_StaticFields::get_offset_of_Ssl3Marker_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1238 = { sizeof (TlsServerHello_t466963106), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1238[2] = 
{
	TlsServerHello_t466963106::get_offset_of_unixTime_9(),
	TlsServerHello_t466963106::get_offset_of_random_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1239 = { sizeof (TlsServerHelloDone_t3853443396), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1240 = { sizeof (TlsServerKeyExchange_t1417611122), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1241 = { sizeof (PrimalityTest_t2979531602), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1242 = { sizeof (CertificateValidationCallback_t3090555431), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1243 = { sizeof (CertificateValidationCallback2_t3353735195), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1244 = { sizeof (CertificateSelectionCallback_t604475832), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1245 = { sizeof (PrivateKeySelectionCallback_t3559239239), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1246 = { sizeof (U3CPrivateImplementationDetailsU3E_t3053238935), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3053238935_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1246[16] = 
{
	U3CPrivateImplementationDetailsU3E_t3053238935_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
	U3CPrivateImplementationDetailsU3E_t3053238935_StaticFields::get_offset_of_U24U24fieldU2D5_1(),
	U3CPrivateImplementationDetailsU3E_t3053238935_StaticFields::get_offset_of_U24U24fieldU2D6_2(),
	U3CPrivateImplementationDetailsU3E_t3053238935_StaticFields::get_offset_of_U24U24fieldU2D7_3(),
	U3CPrivateImplementationDetailsU3E_t3053238935_StaticFields::get_offset_of_U24U24fieldU2D8_4(),
	U3CPrivateImplementationDetailsU3E_t3053238935_StaticFields::get_offset_of_U24U24fieldU2D9_5(),
	U3CPrivateImplementationDetailsU3E_t3053238935_StaticFields::get_offset_of_U24U24fieldU2D11_6(),
	U3CPrivateImplementationDetailsU3E_t3053238935_StaticFields::get_offset_of_U24U24fieldU2D12_7(),
	U3CPrivateImplementationDetailsU3E_t3053238935_StaticFields::get_offset_of_U24U24fieldU2D13_8(),
	U3CPrivateImplementationDetailsU3E_t3053238935_StaticFields::get_offset_of_U24U24fieldU2D14_9(),
	U3CPrivateImplementationDetailsU3E_t3053238935_StaticFields::get_offset_of_U24U24fieldU2D15_10(),
	U3CPrivateImplementationDetailsU3E_t3053238935_StaticFields::get_offset_of_U24U24fieldU2D16_11(),
	U3CPrivateImplementationDetailsU3E_t3053238935_StaticFields::get_offset_of_U24U24fieldU2D17_12(),
	U3CPrivateImplementationDetailsU3E_t3053238935_StaticFields::get_offset_of_U24U24fieldU2D21_13(),
	U3CPrivateImplementationDetailsU3E_t3053238935_StaticFields::get_offset_of_U24U24fieldU2D22_14(),
	U3CPrivateImplementationDetailsU3E_t3053238935_StaticFields::get_offset_of_U24U24fieldU2D23_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1247 = { sizeof (U24ArrayTypeU243132_t435538905)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU243132_t435538905_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1248 = { sizeof (U24ArrayTypeU24256_t1676616793)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24256_t1676616793_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1249 = { sizeof (U24ArrayTypeU2420_t3379220378)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2420_t3379220378_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1250 = { sizeof (U24ArrayTypeU2432_t3379220411)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2432_t3379220411_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1251 = { sizeof (U24ArrayTypeU2448_t3379220448)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2448_t3379220448_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1252 = { sizeof (U24ArrayTypeU2464_t3379220506)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2464_t3379220506_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1253 = { sizeof (U24ArrayTypeU2412_t3379220350)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t3379220350_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1254 = { sizeof (U24ArrayTypeU2416_t3379220353)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2416_t3379220353_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1255 = { sizeof (U24ArrayTypeU244_t3988332409)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU244_t3988332409_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1256 = { sizeof (U3CModuleU3E_t86524793), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1257 = { sizeof (ExtensionAttribute_t2299149759), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1258 = { sizeof (Locale_t2281372285), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1259 = { sizeof (MonoTODOAttribute_t2091695243), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1260 = { sizeof (KeyBuilder_t373726642), -1, sizeof(KeyBuilder_t373726642_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1260[1] = 
{
	KeyBuilder_t373726642_StaticFields::get_offset_of_rng_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1261 = { sizeof (SymmetricTransform_t131863658), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1261[12] = 
{
	SymmetricTransform_t131863658::get_offset_of_algo_0(),
	SymmetricTransform_t131863658::get_offset_of_encrypt_1(),
	SymmetricTransform_t131863658::get_offset_of_BlockSizeByte_2(),
	SymmetricTransform_t131863658::get_offset_of_temp_3(),
	SymmetricTransform_t131863658::get_offset_of_temp2_4(),
	SymmetricTransform_t131863658::get_offset_of_workBuff_5(),
	SymmetricTransform_t131863658::get_offset_of_workout_6(),
	SymmetricTransform_t131863658::get_offset_of_FeedBackByte_7(),
	SymmetricTransform_t131863658::get_offset_of_FeedBackIter_8(),
	SymmetricTransform_t131863658::get_offset_of_m_disposed_9(),
	SymmetricTransform_t131863658::get_offset_of_lastBlock_10(),
	SymmetricTransform_t131863658::get_offset_of__rng_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1262 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1262[14] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1263 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1263[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1264 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1264[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1265 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1265[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1266 = { sizeof (Check_t10677726), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1267 = { sizeof (Enumerable_t839044124), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1268 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1268[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1269 = { sizeof (Aes_t2466798581), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1270 = { sizeof (AesManaged_t23175804), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1271 = { sizeof (AesTransform_t1787635017), -1, sizeof(AesTransform_t1787635017_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1271[14] = 
{
	AesTransform_t1787635017::get_offset_of_expandedKey_12(),
	AesTransform_t1787635017::get_offset_of_Nk_13(),
	AesTransform_t1787635017::get_offset_of_Nr_14(),
	AesTransform_t1787635017_StaticFields::get_offset_of_Rcon_15(),
	AesTransform_t1787635017_StaticFields::get_offset_of_SBox_16(),
	AesTransform_t1787635017_StaticFields::get_offset_of_iSBox_17(),
	AesTransform_t1787635017_StaticFields::get_offset_of_T0_18(),
	AesTransform_t1787635017_StaticFields::get_offset_of_T1_19(),
	AesTransform_t1787635017_StaticFields::get_offset_of_T2_20(),
	AesTransform_t1787635017_StaticFields::get_offset_of_T3_21(),
	AesTransform_t1787635017_StaticFields::get_offset_of_iT0_22(),
	AesTransform_t1787635017_StaticFields::get_offset_of_iT1_23(),
	AesTransform_t1787635017_StaticFields::get_offset_of_iT2_24(),
	AesTransform_t1787635017_StaticFields::get_offset_of_iT3_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1272 = { sizeof (Action_t3771233898), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1273 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1274 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1275 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1276 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1277 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1278 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1279 = { sizeof (U3CPrivateImplementationDetailsU3E_t3053238936), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1279[12] = 
{
	U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
	U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields::get_offset_of_U24U24fieldU2D1_1(),
	U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields::get_offset_of_U24U24fieldU2D2_2(),
	U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields::get_offset_of_U24U24fieldU2D3_3(),
	U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields::get_offset_of_U24U24fieldU2D4_4(),
	U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields::get_offset_of_U24U24fieldU2D5_5(),
	U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields::get_offset_of_U24U24fieldU2D6_6(),
	U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields::get_offset_of_U24U24fieldU2D7_7(),
	U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields::get_offset_of_U24U24fieldU2D8_8(),
	U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields::get_offset_of_U24U24fieldU2D9_9(),
	U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields::get_offset_of_U24U24fieldU2D10_10(),
	U3CPrivateImplementationDetailsU3E_t3053238936_StaticFields::get_offset_of_U24U24fieldU2D11_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1280 = { sizeof (U24ArrayTypeU24136_t1676615770)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24136_t1676615770_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1281 = { sizeof (U24ArrayTypeU24120_t1676615733)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24120_t1676615733_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1282 = { sizeof (U24ArrayTypeU24256_t1676616794)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24256_t1676616794_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1283 = { sizeof (U24ArrayTypeU241024_t435478333)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU241024_t435478333_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1284 = { sizeof (U3CModuleU3E_t86524794), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1285 = { sizeof (AssetBundleCreateRequest_t1416890373), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1286 = { sizeof (CompatibilityCheck_t561206607)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1286[6] = 
{
	CompatibilityCheck_t561206607::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1287 = { sizeof (AssetBundleRequest_t2154290273), sizeof(AssetBundleRequest_t2154290273_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1288 = { sizeof (AssetBundle_t2070959688), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1289 = { sizeof (SendMessageOptions_t3856946179)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1289[3] = 
{
	SendMessageOptions_t3856946179::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1290 = { sizeof (RuntimePlatform_t3050318497)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1290[32] = 
{
	RuntimePlatform_t3050318497::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1291 = { sizeof (LogType_t4286006228)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1291[6] = 
{
	LogType_t4286006228::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1292 = { sizeof (WaitForSeconds_t1615819279), sizeof(WaitForSeconds_t1615819279_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1292[1] = 
{
	WaitForSeconds_t1615819279::get_offset_of_m_Seconds_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1293 = { sizeof (WaitForFixedUpdate_t2130080621), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1294 = { sizeof (WaitForEndOfFrame_t2372756133), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1295 = { sizeof (CustomYieldInstruction_t2666549910), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1296 = { sizeof (Coroutine_t3621161934), sizeof(Coroutine_t3621161934_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1296[1] = 
{
	Coroutine_t3621161934::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1297 = { sizeof (ScriptableObject_t2970544072), sizeof(ScriptableObject_t2970544072_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1298 = { sizeof (UnhandledExceptionHandler_t1700300692), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1299 = { sizeof (CursorLockMode_t1155278888)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1299[4] = 
{
	CursorLockMode_t1155278888::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
