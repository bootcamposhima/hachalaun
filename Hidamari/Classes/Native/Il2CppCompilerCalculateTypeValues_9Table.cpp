﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System___Il2CppComObject3942315138.h"
#include "System_U3CModuleU3E86524790.h"
#include "System_Locale2281372282.h"
#include "System_System_MonoTODOAttribute2091695241.h"
#include "System_System_Collections_Specialized_HybridDictio3032146074.h"
#include "System_System_Collections_Specialized_ListDictiona2682732540.h"
#include "System_System_Collections_Specialized_ListDictiona2044266038.h"
#include "System_System_Collections_Specialized_ListDictiona3271093914.h"
#include "System_System_Collections_Specialized_NameObjectCo1023199937.h"
#include "System_System_Collections_Specialized_NameObjectCo1625138937.h"
#include "System_System_Collections_Specialized_NameObjectCo4030006590.h"
#include "System_System_Collections_Specialized_NameObjectCo1246329035.h"
#include "System_System_Collections_Specialized_NameValueCol2791941106.h"
#include "System_System_ComponentModel_EditorBrowsableAttrib1665593056.h"
#include "System_System_ComponentModel_EditorBrowsableState1963658837.h"
#include "System_System_ComponentModel_TypeConverter1753450284.h"
#include "System_System_ComponentModel_TypeConverterAttribut1738187778.h"
#include "System_System_ComponentModel_Win32Exception819808416.h"
#include "System_System_Diagnostics_Stopwatch3420517611.h"
#include "System_System_IO_Compression_CompressionMode1453657991.h"
#include "System_System_IO_Compression_DeflateStream2030147241.h"
#include "System_System_IO_Compression_DeflateStream_Unmanag2055733333.h"
#include "System_System_IO_Compression_DeflateStream_ReadMet1873379884.h"
#include "System_System_IO_Compression_DeflateStream_WriteMe3250749483.h"
#include "System_System_Net_Security_AuthenticatedStream3871465409.h"
#include "System_System_Net_Security_AuthenticationLevel4161053470.h"
#include "System_System_Net_Security_SslPolicyErrors3099591579.h"
#include "System_System_Net_Security_SslStream490807902.h"
#include "System_System_Net_Security_SslStream_U3CBeginAuthe3681700014.h"
#include "System_System_Net_Security_SslStream_U3CBeginAuthe4280659511.h"
#include "System_System_Net_Sockets_AddressFamily3770679850.h"
#include "System_System_Net_Sockets_IPv6MulticastOption1586710008.h"
#include "System_System_Net_Sockets_LingerOption3290254502.h"
#include "System_System_Net_Sockets_MulticastOption979356607.h"
#include "System_System_Net_Sockets_NetworkStream3953762560.h"
#include "System_System_Net_Sockets_ProtocolType3327388960.h"
#include "System_System_Net_Sockets_SelectMode3812195181.h"
#include "System_System_Net_Sockets_Socket2157335841.h"
#include "System_System_Net_Sockets_Socket_SocketOperation4113109398.h"
#include "System_System_Net_Sockets_Socket_SocketAsyncResult753587752.h"
#include "System_System_Net_Sockets_Socket_Worker3140563676.h"
#include "System_System_Net_Sockets_Socket_SocketAsyncCall742231849.h"
#include "System_System_Net_Sockets_SocketAsyncEventArgs939217660.h"
#include "System_System_Net_Sockets_SocketAsyncOperation2571062732.h"
#include "System_System_Net_Sockets_SocketError4204345479.h"
#include "System_System_Net_Sockets_SocketException3119490894.h"
#include "System_System_Net_Sockets_SocketFlags4205073670.h"
#include "System_System_Net_Sockets_SocketOptionLevel2476940110.h"
#include "System_System_Net_Sockets_SocketOptionName1841276065.h"
#include "System_System_Net_Sockets_SocketShutdown1229168279.h"
#include "System_System_Net_Sockets_SocketType1204660219.h"
#include "System_System_Net_Sockets_TcpClient838416830.h"
#include "System_System_Net_Sockets_TcpClient_Properties1406833876.h"
#include "System_System_Net_DefaultCertificatePolicy3264712578.h"
#include "System_System_Net_Dns3289571299.h"
#include "System_System_Net_EndPoint1026786191.h"
#include "System_System_Net_FileWebRequest1151586641.h"
#include "System_System_Net_FileWebRequestCreator1433256879.h"
#include "System_System_Net_FtpRequestCreator3707472729.h"
#include "System_System_Net_FtpWebRequest3084461655.h"
#include "System_System_Net_GlobalProxySelection183150427.h"
#include "System_System_Net_HttpRequestCreator1200682847.h"
#include "System_System_Net_HttpRequestHeader2474110222.h"
#include "System_System_Net_HttpResponseHeader1795944176.h"
#include "System_System_Net_HttpVersion3218902506.h"
#include "System_System_Net_HttpWebRequest3949036893.h"
#include "System_System_Net_IPAddress3525271463.h"
#include "System_System_Net_IPEndPoint2123960758.h"
#include "System_System_Net_IPHostEntry737820957.h"
#include "System_System_Net_IPv6Address1083476711.h"
#include "System_System_Net_ProtocolViolationException574056316.h"
#include "System_System_Net_SecurityProtocolType2898870124.h"
#include "System_System_Net_ServicePoint4193060341.h"
#include "System_System_Net_ServicePointManager165502476.h"
#include "System_System_Net_ServicePointManager_SPKey1069823253.h"
#include "System_System_Net_SocketAddress4232434619.h"
#include "System_System_Net_WebException2331648373.h"
#include "System_System_Net_WebExceptionStatus2952522055.h"
#include "System_System_Net_WebHeaderCollection1958609721.h"
#include "System_System_Net_WebProxy2877839764.h"
#include "System_System_Net_WebRequest51806901.h"
#include "System_System_Net_WebResponse3238378095.h"
#include "System_System_Security_Authentication_SslProtocols1694761299.h"
#include "System_System_Security_Cryptography_X509Certificat1258989979.h"
#include "System_System_Security_Cryptography_X509Certificat1182884468.h"
#include "System_System_Security_Cryptography_X509Certificat3209511796.h"
#include "System_System_Security_Cryptography_X509Certificat1427767882.h"
#include "System_System_Security_Cryptography_X509Certificate815951664.h"
#include "System_System_Security_Cryptography_X509Certificat2073611205.h"
#include "System_System_Security_Cryptography_X509Certificate134105807.h"
#include "System_System_Security_Cryptography_X509Certificate160474609.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize900 = { sizeof (Il2CppComObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize901 = { sizeof (U3CModuleU3E_t86524791), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize902 = { sizeof (Locale_t2281372283), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize903 = { sizeof (MonoTODOAttribute_t2091695242), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable903[1] = 
{
	MonoTODOAttribute_t2091695242::get_offset_of_comment_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize904 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable904[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize905 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable905[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize906 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable906[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize907 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable907[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize908 = { sizeof (HybridDictionary_t3032146074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable908[3] = 
{
	HybridDictionary_t3032146074::get_offset_of_caseInsensitive_0(),
	HybridDictionary_t3032146074::get_offset_of_hashtable_1(),
	HybridDictionary_t3032146074::get_offset_of_list_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize909 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize910 = { sizeof (ListDictionary_t2682732540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable910[4] = 
{
	ListDictionary_t2682732540::get_offset_of_count_0(),
	ListDictionary_t2682732540::get_offset_of_version_1(),
	ListDictionary_t2682732540::get_offset_of_head_2(),
	ListDictionary_t2682732540::get_offset_of_comparer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize911 = { sizeof (DictionaryNode_t2044266038), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable911[3] = 
{
	DictionaryNode_t2044266038::get_offset_of_key_0(),
	DictionaryNode_t2044266038::get_offset_of_value_1(),
	DictionaryNode_t2044266038::get_offset_of_next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize912 = { sizeof (DictionaryNodeEnumerator_t3271093914), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable912[4] = 
{
	DictionaryNodeEnumerator_t3271093914::get_offset_of_dict_0(),
	DictionaryNodeEnumerator_t3271093914::get_offset_of_isAtStart_1(),
	DictionaryNodeEnumerator_t3271093914::get_offset_of_current_2(),
	DictionaryNodeEnumerator_t3271093914::get_offset_of_version_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize913 = { sizeof (NameObjectCollectionBase_t1023199937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable913[10] = 
{
	NameObjectCollectionBase_t1023199937::get_offset_of_m_ItemsContainer_0(),
	NameObjectCollectionBase_t1023199937::get_offset_of_m_NullKeyItem_1(),
	NameObjectCollectionBase_t1023199937::get_offset_of_m_ItemsArray_2(),
	NameObjectCollectionBase_t1023199937::get_offset_of_m_hashprovider_3(),
	NameObjectCollectionBase_t1023199937::get_offset_of_m_comparer_4(),
	NameObjectCollectionBase_t1023199937::get_offset_of_m_defCapacity_5(),
	NameObjectCollectionBase_t1023199937::get_offset_of_m_readonly_6(),
	NameObjectCollectionBase_t1023199937::get_offset_of_infoCopy_7(),
	NameObjectCollectionBase_t1023199937::get_offset_of_keyscoll_8(),
	NameObjectCollectionBase_t1023199937::get_offset_of_equality_comparer_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize914 = { sizeof (_Item_t1625138937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable914[2] = 
{
	_Item_t1625138937::get_offset_of_key_0(),
	_Item_t1625138937::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize915 = { sizeof (_KeysEnumerator_t4030006590), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable915[2] = 
{
	_KeysEnumerator_t4030006590::get_offset_of_m_collection_0(),
	_KeysEnumerator_t4030006590::get_offset_of_m_position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize916 = { sizeof (KeysCollection_t1246329035), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable916[1] = 
{
	KeysCollection_t1246329035::get_offset_of_m_collection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize917 = { sizeof (NameValueCollection_t2791941106), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable917[2] = 
{
	NameValueCollection_t2791941106::get_offset_of_cachedAllKeys_10(),
	NameValueCollection_t2791941106::get_offset_of_cachedAll_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize918 = { sizeof (EditorBrowsableAttribute_t1665593056), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable918[1] = 
{
	EditorBrowsableAttribute_t1665593056::get_offset_of_state_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize919 = { sizeof (EditorBrowsableState_t1963658837)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable919[4] = 
{
	EditorBrowsableState_t1963658837::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize920 = { sizeof (TypeConverter_t1753450284), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize921 = { sizeof (TypeConverterAttribute_t1738187778), -1, sizeof(TypeConverterAttribute_t1738187778_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable921[2] = 
{
	TypeConverterAttribute_t1738187778_StaticFields::get_offset_of_Default_0(),
	TypeConverterAttribute_t1738187778::get_offset_of_converter_type_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize922 = { sizeof (Win32Exception_t819808416), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable922[1] = 
{
	Win32Exception_t819808416::get_offset_of_native_error_code_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize923 = { sizeof (Stopwatch_t3420517611), -1, sizeof(Stopwatch_t3420517611_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable923[5] = 
{
	Stopwatch_t3420517611_StaticFields::get_offset_of_Frequency_0(),
	Stopwatch_t3420517611_StaticFields::get_offset_of_IsHighResolution_1(),
	Stopwatch_t3420517611::get_offset_of_elapsed_2(),
	Stopwatch_t3420517611::get_offset_of_started_3(),
	Stopwatch_t3420517611::get_offset_of_is_running_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize924 = { sizeof (CompressionMode_t1453657991)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable924[3] = 
{
	CompressionMode_t1453657991::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize925 = { sizeof (DeflateStream_t2030147241), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable925[8] = 
{
	DeflateStream_t2030147241::get_offset_of_base_stream_1(),
	DeflateStream_t2030147241::get_offset_of_mode_2(),
	DeflateStream_t2030147241::get_offset_of_leaveOpen_3(),
	DeflateStream_t2030147241::get_offset_of_disposed_4(),
	DeflateStream_t2030147241::get_offset_of_feeder_5(),
	DeflateStream_t2030147241::get_offset_of_z_stream_6(),
	DeflateStream_t2030147241::get_offset_of_io_buffer_7(),
	DeflateStream_t2030147241::get_offset_of_data_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize926 = { sizeof (UnmanagedReadOrWrite_t2055733333), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize927 = { sizeof (ReadMethod_t1873379884), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize928 = { sizeof (WriteMethod_t3250749483), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize929 = { sizeof (AuthenticatedStream_t3871465409), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable929[2] = 
{
	AuthenticatedStream_t3871465409::get_offset_of_innerStream_1(),
	AuthenticatedStream_t3871465409::get_offset_of_leaveStreamOpen_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize930 = { sizeof (AuthenticationLevel_t4161053470)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable930[4] = 
{
	AuthenticationLevel_t4161053470::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize931 = { sizeof (SslPolicyErrors_t3099591579)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable931[5] = 
{
	SslPolicyErrors_t3099591579::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize932 = { sizeof (SslStream_t490807902), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable932[3] = 
{
	SslStream_t490807902::get_offset_of_ssl_stream_3(),
	SslStream_t490807902::get_offset_of_validation_callback_4(),
	SslStream_t490807902::get_offset_of_selection_callback_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize933 = { sizeof (U3CBeginAuthenticateAsClientU3Ec__AnonStorey7_t3681700014), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable933[2] = 
{
	U3CBeginAuthenticateAsClientU3Ec__AnonStorey7_t3681700014::get_offset_of_clientCertificates_0(),
	U3CBeginAuthenticateAsClientU3Ec__AnonStorey7_t3681700014::get_offset_of_U3CU3Ef__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize934 = { sizeof (U3CBeginAuthenticateAsServerU3Ec__AnonStorey8_t4280659511), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable934[2] = 
{
	U3CBeginAuthenticateAsServerU3Ec__AnonStorey8_t4280659511::get_offset_of_serverCertificate_0(),
	U3CBeginAuthenticateAsServerU3Ec__AnonStorey8_t4280659511::get_offset_of_U3CU3Ef__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize935 = { sizeof (AddressFamily_t3770679850)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable935[32] = 
{
	AddressFamily_t3770679850::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize936 = { sizeof (IPv6MulticastOption_t1586710008), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize937 = { sizeof (LingerOption_t3290254502), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable937[2] = 
{
	LingerOption_t3290254502::get_offset_of_enabled_0(),
	LingerOption_t3290254502::get_offset_of_seconds_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize938 = { sizeof (MulticastOption_t979356607), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize939 = { sizeof (NetworkStream_t3953762560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable939[6] = 
{
	NetworkStream_t3953762560::get_offset_of_access_1(),
	NetworkStream_t3953762560::get_offset_of_socket_2(),
	NetworkStream_t3953762560::get_offset_of_owns_socket_3(),
	NetworkStream_t3953762560::get_offset_of_readable_4(),
	NetworkStream_t3953762560::get_offset_of_writeable_5(),
	NetworkStream_t3953762560::get_offset_of_disposed_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize940 = { sizeof (ProtocolType_t3327388960)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable940[26] = 
{
	ProtocolType_t3327388960::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize941 = { sizeof (SelectMode_t3812195181)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable941[4] = 
{
	SelectMode_t3812195181::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize942 = { sizeof (Socket_t2157335841), -1, sizeof(Socket_t2157335841_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable942[23] = 
{
	Socket_t2157335841::get_offset_of_readQ_0(),
	Socket_t2157335841::get_offset_of_writeQ_1(),
	Socket_t2157335841::get_offset_of_islistening_2(),
	Socket_t2157335841::get_offset_of_useoverlappedIO_3(),
	Socket_t2157335841::get_offset_of_MinListenPort_4(),
	Socket_t2157335841::get_offset_of_MaxListenPort_5(),
	Socket_t2157335841_StaticFields::get_offset_of_ipv4Supported_6(),
	Socket_t2157335841_StaticFields::get_offset_of_ipv6Supported_7(),
	Socket_t2157335841::get_offset_of_linger_timeout_8(),
	Socket_t2157335841::get_offset_of_socket_9(),
	Socket_t2157335841::get_offset_of_address_family_10(),
	Socket_t2157335841::get_offset_of_socket_type_11(),
	Socket_t2157335841::get_offset_of_protocol_type_12(),
	Socket_t2157335841::get_offset_of_blocking_13(),
	Socket_t2157335841::get_offset_of_blocking_thread_14(),
	Socket_t2157335841::get_offset_of_isbound_15(),
	Socket_t2157335841_StaticFields::get_offset_of_current_bind_count_16(),
	Socket_t2157335841::get_offset_of_max_bind_count_17(),
	Socket_t2157335841::get_offset_of_connected_18(),
	Socket_t2157335841::get_offset_of_closed_19(),
	Socket_t2157335841::get_offset_of_disposed_20(),
	Socket_t2157335841::get_offset_of_seed_endpoint_21(),
	Socket_t2157335841_StaticFields::get_offset_of_check_socket_policy_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize943 = { sizeof (SocketOperation_t4113109398)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable943[15] = 
{
	SocketOperation_t4113109398::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize944 = { sizeof (SocketAsyncResult_t753587752), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable944[25] = 
{
	SocketAsyncResult_t753587752::get_offset_of_Sock_0(),
	SocketAsyncResult_t753587752::get_offset_of_handle_1(),
	SocketAsyncResult_t753587752::get_offset_of_state_2(),
	SocketAsyncResult_t753587752::get_offset_of_callback_3(),
	SocketAsyncResult_t753587752::get_offset_of_waithandle_4(),
	SocketAsyncResult_t753587752::get_offset_of_delayedException_5(),
	SocketAsyncResult_t753587752::get_offset_of_EndPoint_6(),
	SocketAsyncResult_t753587752::get_offset_of_Buffer_7(),
	SocketAsyncResult_t753587752::get_offset_of_Offset_8(),
	SocketAsyncResult_t753587752::get_offset_of_Size_9(),
	SocketAsyncResult_t753587752::get_offset_of_SockFlags_10(),
	SocketAsyncResult_t753587752::get_offset_of_AcceptSocket_11(),
	SocketAsyncResult_t753587752::get_offset_of_Addresses_12(),
	SocketAsyncResult_t753587752::get_offset_of_Port_13(),
	SocketAsyncResult_t753587752::get_offset_of_Buffers_14(),
	SocketAsyncResult_t753587752::get_offset_of_ReuseSocket_15(),
	SocketAsyncResult_t753587752::get_offset_of_acc_socket_16(),
	SocketAsyncResult_t753587752::get_offset_of_total_17(),
	SocketAsyncResult_t753587752::get_offset_of_completed_sync_18(),
	SocketAsyncResult_t753587752::get_offset_of_completed_19(),
	SocketAsyncResult_t753587752::get_offset_of_blocking_20(),
	SocketAsyncResult_t753587752::get_offset_of_error_21(),
	SocketAsyncResult_t753587752::get_offset_of_operation_22(),
	SocketAsyncResult_t753587752::get_offset_of_ares_23(),
	SocketAsyncResult_t753587752::get_offset_of_EndCalled_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize945 = { sizeof (Worker_t3140563676), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable945[3] = 
{
	Worker_t3140563676::get_offset_of_result_0(),
	Worker_t3140563676::get_offset_of_requireSocketSecurity_1(),
	Worker_t3140563676::get_offset_of_send_so_far_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize946 = { sizeof (SocketAsyncCall_t742231849), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize947 = { sizeof (SocketAsyncEventArgs_t939217660), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable947[15] = 
{
	SocketAsyncEventArgs_t939217660::get_offset_of__bufferList_1(),
	SocketAsyncEventArgs_t939217660::get_offset_of_curSocket_2(),
	SocketAsyncEventArgs_t939217660::get_offset_of_Completed_3(),
	SocketAsyncEventArgs_t939217660::get_offset_of_U3CAcceptSocketU3Ek__BackingField_4(),
	SocketAsyncEventArgs_t939217660::get_offset_of_U3CBufferU3Ek__BackingField_5(),
	SocketAsyncEventArgs_t939217660::get_offset_of_U3CBytesTransferredU3Ek__BackingField_6(),
	SocketAsyncEventArgs_t939217660::get_offset_of_U3CCountU3Ek__BackingField_7(),
	SocketAsyncEventArgs_t939217660::get_offset_of_U3CDisconnectReuseSocketU3Ek__BackingField_8(),
	SocketAsyncEventArgs_t939217660::get_offset_of_U3CLastOperationU3Ek__BackingField_9(),
	SocketAsyncEventArgs_t939217660::get_offset_of_U3COffsetU3Ek__BackingField_10(),
	SocketAsyncEventArgs_t939217660::get_offset_of_U3CRemoteEndPointU3Ek__BackingField_11(),
	SocketAsyncEventArgs_t939217660::get_offset_of_U3CSendPacketsSendSizeU3Ek__BackingField_12(),
	SocketAsyncEventArgs_t939217660::get_offset_of_U3CSocketErrorU3Ek__BackingField_13(),
	SocketAsyncEventArgs_t939217660::get_offset_of_U3CSocketFlagsU3Ek__BackingField_14(),
	SocketAsyncEventArgs_t939217660::get_offset_of_U3CUserTokenU3Ek__BackingField_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize948 = { sizeof (SocketAsyncOperation_t2571062732)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable948[11] = 
{
	SocketAsyncOperation_t2571062732::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize949 = { sizeof (SocketError_t4204345479)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable949[48] = 
{
	SocketError_t4204345479::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize950 = { sizeof (SocketException_t3119490894), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize951 = { sizeof (SocketFlags_t4205073670)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable951[11] = 
{
	SocketFlags_t4205073670::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize952 = { sizeof (SocketOptionLevel_t2476940110)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable952[6] = 
{
	SocketOptionLevel_t2476940110::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize953 = { sizeof (SocketOptionName_t1841276065)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable953[44] = 
{
	SocketOptionName_t1841276065::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize954 = { sizeof (SocketShutdown_t1229168279)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable954[4] = 
{
	SocketShutdown_t1229168279::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize955 = { sizeof (SocketType_t1204660219)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable955[7] = 
{
	SocketType_t1204660219::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize956 = { sizeof (TcpClient_t838416830), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable956[11] = 
{
	TcpClient_t838416830::get_offset_of_stream_0(),
	TcpClient_t838416830::get_offset_of_active_1(),
	TcpClient_t838416830::get_offset_of_client_2(),
	TcpClient_t838416830::get_offset_of_disposed_3(),
	TcpClient_t838416830::get_offset_of_values_4(),
	TcpClient_t838416830::get_offset_of_recv_timeout_5(),
	TcpClient_t838416830::get_offset_of_send_timeout_6(),
	TcpClient_t838416830::get_offset_of_recv_buffer_size_7(),
	TcpClient_t838416830::get_offset_of_send_buffer_size_8(),
	TcpClient_t838416830::get_offset_of_linger_state_9(),
	TcpClient_t838416830::get_offset_of_no_delay_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize957 = { sizeof (Properties_t1406833876)+ sizeof (Il2CppObject), sizeof(uint32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable957[7] = 
{
	Properties_t1406833876::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize958 = { sizeof (DefaultCertificatePolicy_t3264712578), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize959 = { sizeof (Dns_t3289571299), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize960 = { sizeof (EndPoint_t1026786191), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize961 = { sizeof (FileWebRequest_t1151586641), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable961[9] = 
{
	FileWebRequest_t1151586641::get_offset_of_uri_6(),
	FileWebRequest_t1151586641::get_offset_of_webHeaders_7(),
	FileWebRequest_t1151586641::get_offset_of_connectionGroup_8(),
	FileWebRequest_t1151586641::get_offset_of_contentLength_9(),
	FileWebRequest_t1151586641::get_offset_of_fileAccess_10(),
	FileWebRequest_t1151586641::get_offset_of_method_11(),
	FileWebRequest_t1151586641::get_offset_of_proxy_12(),
	FileWebRequest_t1151586641::get_offset_of_preAuthenticate_13(),
	FileWebRequest_t1151586641::get_offset_of_timeout_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize962 = { sizeof (FileWebRequestCreator_t1433256879), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize963 = { sizeof (FtpRequestCreator_t3707472729), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize964 = { sizeof (FtpWebRequest_t3084461655), -1, sizeof(FtpWebRequest_t3084461655_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable964[11] = 
{
	FtpWebRequest_t3084461655::get_offset_of_requestUri_6(),
	FtpWebRequest_t3084461655::get_offset_of_proxy_7(),
	FtpWebRequest_t3084461655::get_offset_of_timeout_8(),
	FtpWebRequest_t3084461655::get_offset_of_rwTimeout_9(),
	FtpWebRequest_t3084461655::get_offset_of_binary_10(),
	FtpWebRequest_t3084461655::get_offset_of_usePassive_11(),
	FtpWebRequest_t3084461655::get_offset_of_method_12(),
	FtpWebRequest_t3084461655::get_offset_of_locker_13(),
	FtpWebRequest_t3084461655_StaticFields::get_offset_of_supportedCommands_14(),
	FtpWebRequest_t3084461655::get_offset_of_callback_15(),
	FtpWebRequest_t3084461655_StaticFields::get_offset_of_U3CU3Ef__amU24cache1C_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize965 = { sizeof (GlobalProxySelection_t183150427), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize966 = { sizeof (HttpRequestCreator_t1200682847), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize967 = { sizeof (HttpRequestHeader_t2474110222)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable967[42] = 
{
	HttpRequestHeader_t2474110222::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize968 = { sizeof (HttpResponseHeader_t1795944176)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable968[31] = 
{
	HttpResponseHeader_t1795944176::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize969 = { sizeof (HttpVersion_t3218902506), -1, sizeof(HttpVersion_t3218902506_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable969[2] = 
{
	HttpVersion_t3218902506_StaticFields::get_offset_of_Version10_0(),
	HttpVersion_t3218902506_StaticFields::get_offset_of_Version11_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize970 = { sizeof (HttpWebRequest_t3949036893), -1, sizeof(HttpWebRequest_t3949036893_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable970[24] = 
{
	HttpWebRequest_t3949036893::get_offset_of_requestUri_6(),
	HttpWebRequest_t3949036893::get_offset_of_actualUri_7(),
	HttpWebRequest_t3949036893::get_offset_of_hostChanged_8(),
	HttpWebRequest_t3949036893::get_offset_of_allowAutoRedirect_9(),
	HttpWebRequest_t3949036893::get_offset_of_allowBuffering_10(),
	HttpWebRequest_t3949036893::get_offset_of_certificates_11(),
	HttpWebRequest_t3949036893::get_offset_of_connectionGroup_12(),
	HttpWebRequest_t3949036893::get_offset_of_contentLength_13(),
	HttpWebRequest_t3949036893::get_offset_of_webHeaders_14(),
	HttpWebRequest_t3949036893::get_offset_of_keepAlive_15(),
	HttpWebRequest_t3949036893::get_offset_of_maxAutoRedirect_16(),
	HttpWebRequest_t3949036893::get_offset_of_mediaType_17(),
	HttpWebRequest_t3949036893::get_offset_of_method_18(),
	HttpWebRequest_t3949036893::get_offset_of_initialMethod_19(),
	HttpWebRequest_t3949036893::get_offset_of_pipelined_20(),
	HttpWebRequest_t3949036893::get_offset_of_version_21(),
	HttpWebRequest_t3949036893::get_offset_of_proxy_22(),
	HttpWebRequest_t3949036893::get_offset_of_sendChunked_23(),
	HttpWebRequest_t3949036893::get_offset_of_servicePoint_24(),
	HttpWebRequest_t3949036893::get_offset_of_timeout_25(),
	HttpWebRequest_t3949036893::get_offset_of_redirects_26(),
	HttpWebRequest_t3949036893::get_offset_of_locker_27(),
	HttpWebRequest_t3949036893_StaticFields::get_offset_of_defaultMaxResponseHeadersLength_28(),
	HttpWebRequest_t3949036893::get_offset_of_readWriteTimeout_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize971 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize972 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize973 = { sizeof (IPAddress_t3525271463), -1, sizeof(IPAddress_t3525271463_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable973[11] = 
{
	IPAddress_t3525271463::get_offset_of_m_Address_0(),
	IPAddress_t3525271463::get_offset_of_m_Family_1(),
	IPAddress_t3525271463::get_offset_of_m_Numbers_2(),
	IPAddress_t3525271463::get_offset_of_m_ScopeId_3(),
	IPAddress_t3525271463_StaticFields::get_offset_of_Any_4(),
	IPAddress_t3525271463_StaticFields::get_offset_of_Broadcast_5(),
	IPAddress_t3525271463_StaticFields::get_offset_of_Loopback_6(),
	IPAddress_t3525271463_StaticFields::get_offset_of_None_7(),
	IPAddress_t3525271463_StaticFields::get_offset_of_IPv6Any_8(),
	IPAddress_t3525271463_StaticFields::get_offset_of_IPv6Loopback_9(),
	IPAddress_t3525271463_StaticFields::get_offset_of_IPv6None_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize974 = { sizeof (IPEndPoint_t2123960758), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable974[2] = 
{
	IPEndPoint_t2123960758::get_offset_of_address_0(),
	IPEndPoint_t2123960758::get_offset_of_port_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize975 = { sizeof (IPHostEntry_t737820957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable975[3] = 
{
	IPHostEntry_t737820957::get_offset_of_addressList_0(),
	IPHostEntry_t737820957::get_offset_of_aliases_1(),
	IPHostEntry_t737820957::get_offset_of_hostName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize976 = { sizeof (IPv6Address_t1083476711), -1, sizeof(IPv6Address_t1083476711_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable976[5] = 
{
	IPv6Address_t1083476711::get_offset_of_address_0(),
	IPv6Address_t1083476711::get_offset_of_prefixLength_1(),
	IPv6Address_t1083476711::get_offset_of_scopeId_2(),
	IPv6Address_t1083476711_StaticFields::get_offset_of_Loopback_3(),
	IPv6Address_t1083476711_StaticFields::get_offset_of_Unspecified_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize977 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize978 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize979 = { sizeof (ProtocolViolationException_t574056316), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize980 = { sizeof (SecurityProtocolType_t2898870124)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable980[3] = 
{
	SecurityProtocolType_t2898870124::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize981 = { sizeof (ServicePoint_t4193060341), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable981[11] = 
{
	ServicePoint_t4193060341::get_offset_of_uri_0(),
	ServicePoint_t4193060341::get_offset_of_connectionLimit_1(),
	ServicePoint_t4193060341::get_offset_of_maxIdleTime_2(),
	ServicePoint_t4193060341::get_offset_of_currentConnections_3(),
	ServicePoint_t4193060341::get_offset_of_idleSince_4(),
	ServicePoint_t4193060341::get_offset_of_usesProxy_5(),
	ServicePoint_t4193060341::get_offset_of_sendContinue_6(),
	ServicePoint_t4193060341::get_offset_of_useConnect_7(),
	ServicePoint_t4193060341::get_offset_of_locker_8(),
	ServicePoint_t4193060341::get_offset_of_hostE_9(),
	ServicePoint_t4193060341::get_offset_of_useNagle_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize982 = { sizeof (ServicePointManager_t165502476), -1, sizeof(ServicePointManager_t165502476_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable982[10] = 
{
	ServicePointManager_t165502476_StaticFields::get_offset_of_servicePoints_0(),
	ServicePointManager_t165502476_StaticFields::get_offset_of_policy_1(),
	ServicePointManager_t165502476_StaticFields::get_offset_of_defaultConnectionLimit_2(),
	ServicePointManager_t165502476_StaticFields::get_offset_of_maxServicePointIdleTime_3(),
	ServicePointManager_t165502476_StaticFields::get_offset_of_maxServicePoints_4(),
	ServicePointManager_t165502476_StaticFields::get_offset_of__checkCRL_5(),
	ServicePointManager_t165502476_StaticFields::get_offset_of__securityProtocol_6(),
	ServicePointManager_t165502476_StaticFields::get_offset_of_expectContinue_7(),
	ServicePointManager_t165502476_StaticFields::get_offset_of_useNagle_8(),
	ServicePointManager_t165502476_StaticFields::get_offset_of_server_cert_cb_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize983 = { sizeof (SPKey_t1069823253), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable983[2] = 
{
	SPKey_t1069823253::get_offset_of_uri_0(),
	SPKey_t1069823253::get_offset_of_use_connect_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize984 = { sizeof (SocketAddress_t4232434619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable984[1] = 
{
	SocketAddress_t4232434619::get_offset_of_data_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize985 = { sizeof (WebException_t2331648373), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable985[2] = 
{
	WebException_t2331648373::get_offset_of_response_12(),
	WebException_t2331648373::get_offset_of_status_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize986 = { sizeof (WebExceptionStatus_t2952522055)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable986[22] = 
{
	WebExceptionStatus_t2952522055::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize987 = { sizeof (WebHeaderCollection_t1958609721), -1, sizeof(WebHeaderCollection_t1958609721_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable987[5] = 
{
	WebHeaderCollection_t1958609721_StaticFields::get_offset_of_restricted_12(),
	WebHeaderCollection_t1958609721_StaticFields::get_offset_of_multiValue_13(),
	WebHeaderCollection_t1958609721_StaticFields::get_offset_of_restricted_response_14(),
	WebHeaderCollection_t1958609721::get_offset_of_internallyCreated_15(),
	WebHeaderCollection_t1958609721_StaticFields::get_offset_of_allowed_chars_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize988 = { sizeof (WebProxy_t2877839764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable988[5] = 
{
	WebProxy_t2877839764::get_offset_of_address_0(),
	WebProxy_t2877839764::get_offset_of_bypassOnLocal_1(),
	WebProxy_t2877839764::get_offset_of_bypassList_2(),
	WebProxy_t2877839764::get_offset_of_credentials_3(),
	WebProxy_t2877839764::get_offset_of_useDefaultCredentials_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize989 = { sizeof (WebRequest_t51806901), -1, sizeof(WebRequest_t51806901_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable989[5] = 
{
	WebRequest_t51806901_StaticFields::get_offset_of_prefixes_1(),
	WebRequest_t51806901_StaticFields::get_offset_of_isDefaultWebProxySet_2(),
	WebRequest_t51806901_StaticFields::get_offset_of_defaultWebProxy_3(),
	WebRequest_t51806901::get_offset_of_authentication_level_4(),
	WebRequest_t51806901_StaticFields::get_offset_of_lockobj_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize990 = { sizeof (WebResponse_t3238378095), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize991 = { sizeof (SslProtocols_t1694761299)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable991[6] = 
{
	SslProtocols_t1694761299::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize992 = { sizeof (OpenFlags_t1258989979)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable992[6] = 
{
	OpenFlags_t1258989979::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize993 = { sizeof (PublicKey_t1182884468), -1, sizeof(PublicKey_t1182884468_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable993[5] = 
{
	PublicKey_t1182884468::get_offset_of__key_0(),
	PublicKey_t1182884468::get_offset_of__keyValue_1(),
	PublicKey_t1182884468::get_offset_of__params_2(),
	PublicKey_t1182884468::get_offset_of__oid_3(),
	PublicKey_t1182884468_StaticFields::get_offset_of_U3CU3Ef__switchU24map9_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize994 = { sizeof (StoreLocation_t3209511796)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable994[3] = 
{
	StoreLocation_t3209511796::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize995 = { sizeof (StoreName_t1427767882)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable995[9] = 
{
	StoreName_t1427767882::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize996 = { sizeof (X500DistinguishedName_t815951664), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable996[1] = 
{
	X500DistinguishedName_t815951664::get_offset_of_name_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize997 = { sizeof (X500DistinguishedNameFlags_t2073611205)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable997[11] = 
{
	X500DistinguishedNameFlags_t2073611205::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize998 = { sizeof (X509BasicConstraintsExtension_t134105807), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable998[6] = 
{
	0,
	0,
	X509BasicConstraintsExtension_t134105807::get_offset_of__certificateAuthority_6(),
	X509BasicConstraintsExtension_t134105807::get_offset_of__hasPathLengthConstraint_7(),
	X509BasicConstraintsExtension_t134105807::get_offset_of__pathLengthConstraint_8(),
	X509BasicConstraintsExtension_t134105807::get_offset_of__status_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize999 = { sizeof (X509Certificate2_t160474609), -1, sizeof(X509Certificate2_t160474609_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable999[13] = 
{
	X509Certificate2_t160474609::get_offset_of__archived_5(),
	X509Certificate2_t160474609::get_offset_of__extensions_6(),
	X509Certificate2_t160474609::get_offset_of__name_7(),
	X509Certificate2_t160474609::get_offset_of__serial_8(),
	X509Certificate2_t160474609::get_offset_of__publicKey_9(),
	X509Certificate2_t160474609::get_offset_of_issuer_name_10(),
	X509Certificate2_t160474609::get_offset_of_subject_name_11(),
	X509Certificate2_t160474609::get_offset_of_signature_algorithm_12(),
	X509Certificate2_t160474609::get_offset_of__cert_13(),
	X509Certificate2_t160474609_StaticFields::get_offset_of_empty_error_14(),
	X509Certificate2_t160474609_StaticFields::get_offset_of_commonName_15(),
	X509Certificate2_t160474609_StaticFields::get_offset_of_email_16(),
	X509Certificate2_t160474609_StaticFields::get_offset_of_signedData_17(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
