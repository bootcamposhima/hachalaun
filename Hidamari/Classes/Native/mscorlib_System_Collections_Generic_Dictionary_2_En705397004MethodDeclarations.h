﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2055017830MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Char>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1651977034(__this, ___dictionary0, method) ((  void (*) (Enumerator_t705397004 *, Dictionary_2_t3683040908 *, const MethodInfo*))Enumerator__ctor_m1734237432_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Char>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3032992599(__this, method) ((  Il2CppObject * (*) (Enumerator_t705397004 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m4171576169_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Char>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2417142571(__this, method) ((  void (*) (Enumerator_t705397004 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3218868413_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Char>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3114761396(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t705397004 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m174895814_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Char>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m475065075(__this, method) ((  Il2CppObject * (*) (Enumerator_t705397004 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2259713413_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Char>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2319440069(__this, method) ((  Il2CppObject * (*) (Enumerator_t705397004 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3674541783_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Char>::MoveNext()
#define Enumerator_MoveNext_m1404610071(__this, method) ((  bool (*) (Enumerator_t705397004 *, const MethodInfo*))Enumerator_MoveNext_m2277225129_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Char>::get_Current()
#define Enumerator_get_Current_m153001785(__this, method) ((  KeyValuePair_2_t3581821614  (*) (Enumerator_t705397004 *, const MethodInfo*))Enumerator_get_Current_m3086119271_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Char>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2481728804(__this, method) ((  String_t* (*) (Enumerator_t705397004 *, const MethodInfo*))Enumerator_get_CurrentKey_m1875117110_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Char>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2572285256(__this, method) ((  Il2CppChar (*) (Enumerator_t705397004 *, const MethodInfo*))Enumerator_get_CurrentValue_m3733999578_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Char>::Reset()
#define Enumerator_Reset_m3037361820(__this, method) ((  void (*) (Enumerator_t705397004 *, const MethodInfo*))Enumerator_Reset_m3137012554_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Char>::VerifyState()
#define Enumerator_VerifyState_m3543753125(__this, method) ((  void (*) (Enumerator_t705397004 *, const MethodInfo*))Enumerator_VerifyState_m2181903315_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Char>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m2382559181(__this, method) ((  void (*) (Enumerator_t705397004 *, const MethodInfo*))Enumerator_VerifyCurrent_m3609917051_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Char>::Dispose()
#define Enumerator_Dispose_m850376812(__this, method) ((  void (*) (Enumerator_t705397004 *, const MethodInfo*))Enumerator_Dispose_m2125451674_gshared)(__this, method)
