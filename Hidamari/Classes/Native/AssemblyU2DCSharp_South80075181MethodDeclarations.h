﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// South
struct South_t80075181;

#include "codegen/il2cpp-codegen.h"

// System.Void South::.ctor()
extern "C"  void South__ctor_m123044446 (South_t80075181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void South::Start()
extern "C"  void South_Start_m3365149534 (South_t80075181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void South::Awake()
extern "C"  void South_Awake_m360649665 (South_t80075181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void South::Update()
extern "C"  void South_Update_m1246272623 (South_t80075181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void South::FixedUpdate()
extern "C"  void South_FixedUpdate_m2688524633 (South_t80075181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void South::MoveSatrtPosition()
extern "C"  void South_MoveSatrtPosition_m3026898076 (South_t80075181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void South::MoveStart()
extern "C"  void South_MoveStart_m871040205 (South_t80075181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void South::ChangeMove()
extern "C"  void South_ChangeMove_m4168515271 (South_t80075181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void South::setCoordinate()
extern "C"  void South_setCoordinate_m235211158 (South_t80075181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void South::Shot()
extern "C"  void South_Shot_m1760455584 (South_t80075181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void South::Move()
extern "C"  void South_Move_m1595339447 (South_t80075181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void South::ShotParticle()
extern "C"  void South_ShotParticle_m1844846022 (South_t80075181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
