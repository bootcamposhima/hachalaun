﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TransitionAnimation/<Fadein>c__Iterator9
struct U3CFadeinU3Ec__Iterator9_t2485504041;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void TransitionAnimation/<Fadein>c__Iterator9::.ctor()
extern "C"  void U3CFadeinU3Ec__Iterator9__ctor_m1140896914 (U3CFadeinU3Ec__Iterator9_t2485504041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TransitionAnimation/<Fadein>c__Iterator9::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFadeinU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m62216074 (U3CFadeinU3Ec__Iterator9_t2485504041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TransitionAnimation/<Fadein>c__Iterator9::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFadeinU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m1074507550 (U3CFadeinU3Ec__Iterator9_t2485504041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TransitionAnimation/<Fadein>c__Iterator9::MoveNext()
extern "C"  bool U3CFadeinU3Ec__Iterator9_MoveNext_m1048681482 (U3CFadeinU3Ec__Iterator9_t2485504041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransitionAnimation/<Fadein>c__Iterator9::Dispose()
extern "C"  void U3CFadeinU3Ec__Iterator9_Dispose_m1083556943 (U3CFadeinU3Ec__Iterator9_t2485504041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransitionAnimation/<Fadein>c__Iterator9::Reset()
extern "C"  void U3CFadeinU3Ec__Iterator9_Reset_m3082297151 (U3CFadeinU3Ec__Iterator9_t2485504041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
