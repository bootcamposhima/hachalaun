﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator1A
struct U3CSplitHeaderValueU3Ec__Iterator1A_t3349152944;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.String>
struct IEnumerator_1_t1919096606;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator1A::.ctor()
extern "C"  void U3CSplitHeaderValueU3Ec__Iterator1A__ctor_m4246856171 (U3CSplitHeaderValueU3Ec__Iterator1A_t3349152944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator1A::System.Collections.Generic.IEnumerator<string>.get_Current()
extern "C"  String_t* U3CSplitHeaderValueU3Ec__Iterator1A_System_Collections_Generic_IEnumeratorU3CstringU3E_get_Current_m1344651245 (U3CSplitHeaderValueU3Ec__Iterator1A_t3349152944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator1A::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSplitHeaderValueU3Ec__Iterator1A_System_Collections_IEnumerator_get_Current_m1496360741 (U3CSplitHeaderValueU3Ec__Iterator1A_t3349152944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator1A::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CSplitHeaderValueU3Ec__Iterator1A_System_Collections_IEnumerable_GetEnumerator_m3940149926 (U3CSplitHeaderValueU3Ec__Iterator1A_t3349152944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.String> WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator1A::System.Collections.Generic.IEnumerable<string>.GetEnumerator()
extern "C"  Il2CppObject* U3CSplitHeaderValueU3Ec__Iterator1A_System_Collections_Generic_IEnumerableU3CstringU3E_GetEnumerator_m3309820592 (U3CSplitHeaderValueU3Ec__Iterator1A_t3349152944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator1A::MoveNext()
extern "C"  bool U3CSplitHeaderValueU3Ec__Iterator1A_MoveNext_m3232789265 (U3CSplitHeaderValueU3Ec__Iterator1A_t3349152944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator1A::Dispose()
extern "C"  void U3CSplitHeaderValueU3Ec__Iterator1A_Dispose_m908132200 (U3CSplitHeaderValueU3Ec__Iterator1A_t3349152944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator1A::Reset()
extern "C"  void U3CSplitHeaderValueU3Ec__Iterator1A_Reset_m1893289112 (U3CSplitHeaderValueU3Ec__Iterator1A_t3349152944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
