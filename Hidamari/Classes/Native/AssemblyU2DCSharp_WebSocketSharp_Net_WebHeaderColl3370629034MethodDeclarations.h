﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.Net.WebHeaderCollection/<ToString>c__AnonStorey27
struct U3CToStringU3Ec__AnonStorey27_t3370629034;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocketSharp.Net.WebHeaderCollection/<ToString>c__AnonStorey27::.ctor()
extern "C"  void U3CToStringU3Ec__AnonStorey27__ctor_m2447564337 (U3CToStringU3Ec__AnonStorey27_t3370629034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.WebHeaderCollection/<ToString>c__AnonStorey27::<>m__B(System.Int32)
extern "C"  void U3CToStringU3Ec__AnonStorey27_U3CU3Em__B_m3999878203 (U3CToStringU3Ec__AnonStorey27_t3370629034 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
