﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t4260760469;
// WebSocketSharp.Net.HttpListenerResponse
struct HttpListenerResponse_t1992878431;
// System.IO.Stream
struct Stream_t1561764144;

#include "mscorlib_System_IO_Stream1561764144.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.ResponseStream
struct  ResponseStream_t1796293571  : public Stream_t1561764144
{
public:
	// System.Boolean WebSocketSharp.Net.ResponseStream::_disposed
	bool ____disposed_2;
	// System.Boolean WebSocketSharp.Net.ResponseStream::_ignoreErrors
	bool ____ignoreErrors_3;
	// WebSocketSharp.Net.HttpListenerResponse WebSocketSharp.Net.ResponseStream::_response
	HttpListenerResponse_t1992878431 * ____response_4;
	// System.IO.Stream WebSocketSharp.Net.ResponseStream::_stream
	Stream_t1561764144 * ____stream_5;
	// System.Boolean WebSocketSharp.Net.ResponseStream::_trailerSent
	bool ____trailerSent_6;

public:
	inline static int32_t get_offset_of__disposed_2() { return static_cast<int32_t>(offsetof(ResponseStream_t1796293571, ____disposed_2)); }
	inline bool get__disposed_2() const { return ____disposed_2; }
	inline bool* get_address_of__disposed_2() { return &____disposed_2; }
	inline void set__disposed_2(bool value)
	{
		____disposed_2 = value;
	}

	inline static int32_t get_offset_of__ignoreErrors_3() { return static_cast<int32_t>(offsetof(ResponseStream_t1796293571, ____ignoreErrors_3)); }
	inline bool get__ignoreErrors_3() const { return ____ignoreErrors_3; }
	inline bool* get_address_of__ignoreErrors_3() { return &____ignoreErrors_3; }
	inline void set__ignoreErrors_3(bool value)
	{
		____ignoreErrors_3 = value;
	}

	inline static int32_t get_offset_of__response_4() { return static_cast<int32_t>(offsetof(ResponseStream_t1796293571, ____response_4)); }
	inline HttpListenerResponse_t1992878431 * get__response_4() const { return ____response_4; }
	inline HttpListenerResponse_t1992878431 ** get_address_of__response_4() { return &____response_4; }
	inline void set__response_4(HttpListenerResponse_t1992878431 * value)
	{
		____response_4 = value;
		Il2CppCodeGenWriteBarrier(&____response_4, value);
	}

	inline static int32_t get_offset_of__stream_5() { return static_cast<int32_t>(offsetof(ResponseStream_t1796293571, ____stream_5)); }
	inline Stream_t1561764144 * get__stream_5() const { return ____stream_5; }
	inline Stream_t1561764144 ** get_address_of__stream_5() { return &____stream_5; }
	inline void set__stream_5(Stream_t1561764144 * value)
	{
		____stream_5 = value;
		Il2CppCodeGenWriteBarrier(&____stream_5, value);
	}

	inline static int32_t get_offset_of__trailerSent_6() { return static_cast<int32_t>(offsetof(ResponseStream_t1796293571, ____trailerSent_6)); }
	inline bool get__trailerSent_6() const { return ____trailerSent_6; }
	inline bool* get_address_of__trailerSent_6() { return &____trailerSent_6; }
	inline void set__trailerSent_6(bool value)
	{
		____trailerSent_6 = value;
	}
};

struct ResponseStream_t1796293571_StaticFields
{
public:
	// System.Byte[] WebSocketSharp.Net.ResponseStream::_crlf
	ByteU5BU5D_t4260760469* ____crlf_1;

public:
	inline static int32_t get_offset_of__crlf_1() { return static_cast<int32_t>(offsetof(ResponseStream_t1796293571_StaticFields, ____crlf_1)); }
	inline ByteU5BU5D_t4260760469* get__crlf_1() const { return ____crlf_1; }
	inline ByteU5BU5D_t4260760469** get_address_of__crlf_1() { return &____crlf_1; }
	inline void set__crlf_1(ByteU5BU5D_t4260760469* value)
	{
		____crlf_1 = value;
		Il2CppCodeGenWriteBarrier(&____crlf_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
