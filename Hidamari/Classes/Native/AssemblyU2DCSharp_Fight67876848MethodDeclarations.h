﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Fight
struct Fight_t67876848;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_FixedID820738927.h"

// System.Void Fight::.ctor()
extern "C"  void Fight__ctor_m84017019 (Fight_t67876848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fight::Start()
extern "C"  void Fight_Start_m3326122107 (Fight_t67876848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fight::Awake()
extern "C"  void Fight_Awake_m321622238 (Fight_t67876848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fight::Update()
extern "C"  void Fight_Update_m36422386 (Fight_t67876848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Fight::get_Attack()
extern "C"  bool Fight_get_Attack_m4242587718 (Fight_t67876848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fight::set_Attack(System.Boolean)
extern "C"  void Fight_set_Attack_m1041019109 (Fight_t67876848 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Fight::get_Room()
extern "C"  String_t* Fight_get_Room_m3229038026 (Fight_t67876848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fight::set_Room(System.String)
extern "C"  void Fight_set_Room_m1626682785 (Fight_t67876848 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FixedID Fight::get_EnemyFID()
extern "C"  FixedID_t820738927  Fight_get_EnemyFID_m3918231303 (Fight_t67876848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fight::set_EnemyFID(FixedID)
extern "C"  void Fight_set_EnemyFID_m1463124612 (Fight_t67876848 * __this, FixedID_t820738927  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Fight::get_EnemyID()
extern "C"  String_t* Fight_get_EnemyID_m1959288182 (Fight_t67876848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Fight::get_Enemyname()
extern "C"  String_t* Fight_get_Enemyname_m2768442470 (Fight_t67876848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fight::Des()
extern "C"  void Fight_Des_m3515429419 (Fight_t67876848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
