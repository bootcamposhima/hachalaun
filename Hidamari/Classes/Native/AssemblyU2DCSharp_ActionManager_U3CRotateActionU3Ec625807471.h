﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ActionManager/<RotateAction>c__Iterator7
struct  U3CRotateActionU3Ec__Iterator7_t625807471  : public Il2CppObject
{
public:
	// UnityEngine.Vector3 ActionManager/<RotateAction>c__Iterator7::rotate
	Vector3_t4282066566  ___rotate_0;
	// UnityEngine.GameObject ActionManager/<RotateAction>c__Iterator7::_ob
	GameObject_t3674682005 * ____ob_1;
	// UnityEngine.Vector3 ActionManager/<RotateAction>c__Iterator7::<p>__0
	Vector3_t4282066566  ___U3CpU3E__0_2;
	// System.Single ActionManager/<RotateAction>c__Iterator7::deltatime
	float ___deltatime_3;
	// System.Single ActionManager/<RotateAction>c__Iterator7::<spdx>__1
	float ___U3CspdxU3E__1_4;
	// System.Single ActionManager/<RotateAction>c__Iterator7::<spdy>__2
	float ___U3CspdyU3E__2_5;
	// System.Single ActionManager/<RotateAction>c__Iterator7::<spdz>__3
	float ___U3CspdzU3E__3_6;
	// UnityEngine.Vector3 ActionManager/<RotateAction>c__Iterator7::<mp>__4
	Vector3_t4282066566  ___U3CmpU3E__4_7;
	// System.Int32 ActionManager/<RotateAction>c__Iterator7::$PC
	int32_t ___U24PC_8;
	// System.Object ActionManager/<RotateAction>c__Iterator7::$current
	Il2CppObject * ___U24current_9;
	// UnityEngine.Vector3 ActionManager/<RotateAction>c__Iterator7::<$>rotate
	Vector3_t4282066566  ___U3CU24U3Erotate_10;
	// UnityEngine.GameObject ActionManager/<RotateAction>c__Iterator7::<$>_ob
	GameObject_t3674682005 * ___U3CU24U3E_ob_11;
	// System.Single ActionManager/<RotateAction>c__Iterator7::<$>deltatime
	float ___U3CU24U3Edeltatime_12;

public:
	inline static int32_t get_offset_of_rotate_0() { return static_cast<int32_t>(offsetof(U3CRotateActionU3Ec__Iterator7_t625807471, ___rotate_0)); }
	inline Vector3_t4282066566  get_rotate_0() const { return ___rotate_0; }
	inline Vector3_t4282066566 * get_address_of_rotate_0() { return &___rotate_0; }
	inline void set_rotate_0(Vector3_t4282066566  value)
	{
		___rotate_0 = value;
	}

	inline static int32_t get_offset_of__ob_1() { return static_cast<int32_t>(offsetof(U3CRotateActionU3Ec__Iterator7_t625807471, ____ob_1)); }
	inline GameObject_t3674682005 * get__ob_1() const { return ____ob_1; }
	inline GameObject_t3674682005 ** get_address_of__ob_1() { return &____ob_1; }
	inline void set__ob_1(GameObject_t3674682005 * value)
	{
		____ob_1 = value;
		Il2CppCodeGenWriteBarrier(&____ob_1, value);
	}

	inline static int32_t get_offset_of_U3CpU3E__0_2() { return static_cast<int32_t>(offsetof(U3CRotateActionU3Ec__Iterator7_t625807471, ___U3CpU3E__0_2)); }
	inline Vector3_t4282066566  get_U3CpU3E__0_2() const { return ___U3CpU3E__0_2; }
	inline Vector3_t4282066566 * get_address_of_U3CpU3E__0_2() { return &___U3CpU3E__0_2; }
	inline void set_U3CpU3E__0_2(Vector3_t4282066566  value)
	{
		___U3CpU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_deltatime_3() { return static_cast<int32_t>(offsetof(U3CRotateActionU3Ec__Iterator7_t625807471, ___deltatime_3)); }
	inline float get_deltatime_3() const { return ___deltatime_3; }
	inline float* get_address_of_deltatime_3() { return &___deltatime_3; }
	inline void set_deltatime_3(float value)
	{
		___deltatime_3 = value;
	}

	inline static int32_t get_offset_of_U3CspdxU3E__1_4() { return static_cast<int32_t>(offsetof(U3CRotateActionU3Ec__Iterator7_t625807471, ___U3CspdxU3E__1_4)); }
	inline float get_U3CspdxU3E__1_4() const { return ___U3CspdxU3E__1_4; }
	inline float* get_address_of_U3CspdxU3E__1_4() { return &___U3CspdxU3E__1_4; }
	inline void set_U3CspdxU3E__1_4(float value)
	{
		___U3CspdxU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CspdyU3E__2_5() { return static_cast<int32_t>(offsetof(U3CRotateActionU3Ec__Iterator7_t625807471, ___U3CspdyU3E__2_5)); }
	inline float get_U3CspdyU3E__2_5() const { return ___U3CspdyU3E__2_5; }
	inline float* get_address_of_U3CspdyU3E__2_5() { return &___U3CspdyU3E__2_5; }
	inline void set_U3CspdyU3E__2_5(float value)
	{
		___U3CspdyU3E__2_5 = value;
	}

	inline static int32_t get_offset_of_U3CspdzU3E__3_6() { return static_cast<int32_t>(offsetof(U3CRotateActionU3Ec__Iterator7_t625807471, ___U3CspdzU3E__3_6)); }
	inline float get_U3CspdzU3E__3_6() const { return ___U3CspdzU3E__3_6; }
	inline float* get_address_of_U3CspdzU3E__3_6() { return &___U3CspdzU3E__3_6; }
	inline void set_U3CspdzU3E__3_6(float value)
	{
		___U3CspdzU3E__3_6 = value;
	}

	inline static int32_t get_offset_of_U3CmpU3E__4_7() { return static_cast<int32_t>(offsetof(U3CRotateActionU3Ec__Iterator7_t625807471, ___U3CmpU3E__4_7)); }
	inline Vector3_t4282066566  get_U3CmpU3E__4_7() const { return ___U3CmpU3E__4_7; }
	inline Vector3_t4282066566 * get_address_of_U3CmpU3E__4_7() { return &___U3CmpU3E__4_7; }
	inline void set_U3CmpU3E__4_7(Vector3_t4282066566  value)
	{
		___U3CmpU3E__4_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CRotateActionU3Ec__Iterator7_t625807471, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CRotateActionU3Ec__Iterator7_t625807471, ___U24current_9)); }
	inline Il2CppObject * get_U24current_9() const { return ___U24current_9; }
	inline Il2CppObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(Il2CppObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_9, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Erotate_10() { return static_cast<int32_t>(offsetof(U3CRotateActionU3Ec__Iterator7_t625807471, ___U3CU24U3Erotate_10)); }
	inline Vector3_t4282066566  get_U3CU24U3Erotate_10() const { return ___U3CU24U3Erotate_10; }
	inline Vector3_t4282066566 * get_address_of_U3CU24U3Erotate_10() { return &___U3CU24U3Erotate_10; }
	inline void set_U3CU24U3Erotate_10(Vector3_t4282066566  value)
	{
		___U3CU24U3Erotate_10 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3E_ob_11() { return static_cast<int32_t>(offsetof(U3CRotateActionU3Ec__Iterator7_t625807471, ___U3CU24U3E_ob_11)); }
	inline GameObject_t3674682005 * get_U3CU24U3E_ob_11() const { return ___U3CU24U3E_ob_11; }
	inline GameObject_t3674682005 ** get_address_of_U3CU24U3E_ob_11() { return &___U3CU24U3E_ob_11; }
	inline void set_U3CU24U3E_ob_11(GameObject_t3674682005 * value)
	{
		___U3CU24U3E_ob_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3E_ob_11, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Edeltatime_12() { return static_cast<int32_t>(offsetof(U3CRotateActionU3Ec__Iterator7_t625807471, ___U3CU24U3Edeltatime_12)); }
	inline float get_U3CU24U3Edeltatime_12() const { return ___U3CU24U3Edeltatime_12; }
	inline float* get_address_of_U3CU24U3Edeltatime_12() { return &___U3CU24U3Edeltatime_12; }
	inline void set_U3CU24U3Edeltatime_12(float value)
	{
		___U3CU24U3Edeltatime_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
