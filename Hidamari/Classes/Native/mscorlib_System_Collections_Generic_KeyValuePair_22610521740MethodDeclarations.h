﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_924606671MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<WebSocketSharp.CompressionMethod,System.IO.Stream>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2619053622(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2610521740 *, uint8_t, Stream_t1561764144 *, const MethodInfo*))KeyValuePair_2__ctor_m200737057_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<WebSocketSharp.CompressionMethod,System.IO.Stream>::get_Key()
#define KeyValuePair_2_get_Key_m3131943026(__this, method) ((  uint8_t (*) (KeyValuePair_2_t2610521740 *, const MethodInfo*))KeyValuePair_2_get_Key_m2143879591_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<WebSocketSharp.CompressionMethod,System.IO.Stream>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3796908595(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2610521740 *, uint8_t, const MethodInfo*))KeyValuePair_2_set_Key_m3037882472_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<WebSocketSharp.CompressionMethod,System.IO.Stream>::get_Value()
#define KeyValuePair_2_get_Value_m3571917590(__this, method) ((  Stream_t1561764144 * (*) (KeyValuePair_2_t2610521740 *, const MethodInfo*))KeyValuePair_2_get_Value_m1606892327_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<WebSocketSharp.CompressionMethod,System.IO.Stream>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1657617587(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2610521740 *, Stream_t1561764144 *, const MethodInfo*))KeyValuePair_2_set_Value_m2410891368_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<WebSocketSharp.CompressionMethod,System.IO.Stream>::ToString()
#define KeyValuePair_2_ToString_m1188292981(__this, method) ((  String_t* (*) (KeyValuePair_2_t2610521740 *, const MethodInfo*))KeyValuePair_2_ToString_m2079672442_gshared)(__this, method)
