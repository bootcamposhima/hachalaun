﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// TransitionAnimation
struct TransitionAnimation_t4286760335;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TransitionAnimation/<Fadeout>c__IteratorA
struct  U3CFadeoutU3Ec__IteratorA_t2464685144  : public Il2CppObject
{
public:
	// System.Single TransitionAnimation/<Fadeout>c__IteratorA::<time>__0
	float ___U3CtimeU3E__0_0;
	// System.Single TransitionAnimation/<Fadeout>c__IteratorA::interval
	float ___interval_1;
	// System.Int32 TransitionAnimation/<Fadeout>c__IteratorA::$PC
	int32_t ___U24PC_2;
	// System.Object TransitionAnimation/<Fadeout>c__IteratorA::$current
	Il2CppObject * ___U24current_3;
	// System.Single TransitionAnimation/<Fadeout>c__IteratorA::<$>interval
	float ___U3CU24U3Einterval_4;
	// TransitionAnimation TransitionAnimation/<Fadeout>c__IteratorA::<>f__this
	TransitionAnimation_t4286760335 * ___U3CU3Ef__this_5;

public:
	inline static int32_t get_offset_of_U3CtimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CFadeoutU3Ec__IteratorA_t2464685144, ___U3CtimeU3E__0_0)); }
	inline float get_U3CtimeU3E__0_0() const { return ___U3CtimeU3E__0_0; }
	inline float* get_address_of_U3CtimeU3E__0_0() { return &___U3CtimeU3E__0_0; }
	inline void set_U3CtimeU3E__0_0(float value)
	{
		___U3CtimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_interval_1() { return static_cast<int32_t>(offsetof(U3CFadeoutU3Ec__IteratorA_t2464685144, ___interval_1)); }
	inline float get_interval_1() const { return ___interval_1; }
	inline float* get_address_of_interval_1() { return &___interval_1; }
	inline void set_interval_1(float value)
	{
		___interval_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CFadeoutU3Ec__IteratorA_t2464685144, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CFadeoutU3Ec__IteratorA_t2464685144, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Einterval_4() { return static_cast<int32_t>(offsetof(U3CFadeoutU3Ec__IteratorA_t2464685144, ___U3CU24U3Einterval_4)); }
	inline float get_U3CU24U3Einterval_4() const { return ___U3CU24U3Einterval_4; }
	inline float* get_address_of_U3CU24U3Einterval_4() { return &___U3CU24U3Einterval_4; }
	inline void set_U3CU24U3Einterval_4(float value)
	{
		___U3CU24U3Einterval_4 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_5() { return static_cast<int32_t>(offsetof(U3CFadeoutU3Ec__IteratorA_t2464685144, ___U3CU3Ef__this_5)); }
	inline TransitionAnimation_t4286760335 * get_U3CU3Ef__this_5() const { return ___U3CU3Ef__this_5; }
	inline TransitionAnimation_t4286760335 ** get_address_of_U3CU3Ef__this_5() { return &___U3CU3Ef__this_5; }
	inline void set_U3CU3Ef__this_5(TransitionAnimation_t4286760335 * value)
	{
		___U3CU3Ef__this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
