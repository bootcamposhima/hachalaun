﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va746493984MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m4176221666(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2090755500 *, Dictionary_2_t3390149787 *, const MethodInfo*))ValueCollection__ctor_m4177258586_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2636571152(__this, ___item0, method) ((  void (*) (ValueCollection_t2090755500 *, HttpConnection_t602292776 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1528225944_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m939546329(__this, method) ((  void (*) (ValueCollection_t2090755500 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2827278689_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3954171414(__this, ___item0, method) ((  bool (*) (ValueCollection_t2090755500 *, HttpConnection_t602292776 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2015709838_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1274381371(__this, ___item0, method) ((  bool (*) (ValueCollection_t2090755500 *, HttpConnection_t602292776 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3578506931_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1842235367(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2090755500 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4041606511_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m2384395421(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2090755500 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1709700389_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1628917208(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2090755500 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2843387488_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2229347273(__this, method) ((  bool (*) (ValueCollection_t2090755500 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m290885697_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1064308521(__this, method) ((  bool (*) (ValueCollection_t2090755500 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1930063777_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1331991637(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2090755500 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1874108365_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m2019226025(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2090755500 *, HttpConnectionU5BU5D_t1726474297*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1735386657_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>::GetEnumerator()
#define ValueCollection_GetEnumerator_m3980560716(__this, method) ((  Enumerator_t1321983195  (*) (ValueCollection_t2090755500 *, const MethodInfo*))ValueCollection_GetEnumerator_m1204216004_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>::get_Count()
#define ValueCollection_get_Count_m70107759(__this, method) ((  int32_t (*) (ValueCollection_t2090755500 *, const MethodInfo*))ValueCollection_get_Count_m2709231847_gshared)(__this, method)
