﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TransitionAnimation/<SceneAnimation>c__Iterator8
struct U3CSceneAnimationU3Ec__Iterator8_t732764625;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void TransitionAnimation/<SceneAnimation>c__Iterator8::.ctor()
extern "C"  void U3CSceneAnimationU3Ec__Iterator8__ctor_m4119537642 (U3CSceneAnimationU3Ec__Iterator8_t732764625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TransitionAnimation/<SceneAnimation>c__Iterator8::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSceneAnimationU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2088404530 (U3CSceneAnimationU3Ec__Iterator8_t732764625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TransitionAnimation/<SceneAnimation>c__Iterator8::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSceneAnimationU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m471018950 (U3CSceneAnimationU3Ec__Iterator8_t732764625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TransitionAnimation/<SceneAnimation>c__Iterator8::MoveNext()
extern "C"  bool U3CSceneAnimationU3Ec__Iterator8_MoveNext_m3016250802 (U3CSceneAnimationU3Ec__Iterator8_t732764625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransitionAnimation/<SceneAnimation>c__Iterator8::Dispose()
extern "C"  void U3CSceneAnimationU3Ec__Iterator8_Dispose_m3109077415 (U3CSceneAnimationU3Ec__Iterator8_t732764625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransitionAnimation/<SceneAnimation>c__Iterator8::Reset()
extern "C"  void U3CSceneAnimationU3Ec__Iterator8_Reset_m1765970583 (U3CSceneAnimationU3Ec__Iterator8_t732764625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
