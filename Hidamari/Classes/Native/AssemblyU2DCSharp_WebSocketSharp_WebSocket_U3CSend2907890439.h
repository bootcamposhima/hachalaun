﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Action`1<System.Boolean>
struct Action_1_t872614854;
// WebSocketSharp.WebSocket
struct WebSocket_t1342580397;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocket/<SendAsync>c__AnonStorey2E
struct  U3CSendAsyncU3Ec__AnonStorey2E_t2907890439  : public Il2CppObject
{
public:
	// System.String WebSocketSharp.WebSocket/<SendAsync>c__AnonStorey2E::msg
	String_t* ___msg_0;
	// System.Int32 WebSocketSharp.WebSocket/<SendAsync>c__AnonStorey2E::length
	int32_t ___length_1;
	// System.Action`1<System.Boolean> WebSocketSharp.WebSocket/<SendAsync>c__AnonStorey2E::completed
	Action_1_t872614854 * ___completed_2;
	// WebSocketSharp.WebSocket WebSocketSharp.WebSocket/<SendAsync>c__AnonStorey2E::<>f__this
	WebSocket_t1342580397 * ___U3CU3Ef__this_3;

public:
	inline static int32_t get_offset_of_msg_0() { return static_cast<int32_t>(offsetof(U3CSendAsyncU3Ec__AnonStorey2E_t2907890439, ___msg_0)); }
	inline String_t* get_msg_0() const { return ___msg_0; }
	inline String_t** get_address_of_msg_0() { return &___msg_0; }
	inline void set_msg_0(String_t* value)
	{
		___msg_0 = value;
		Il2CppCodeGenWriteBarrier(&___msg_0, value);
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(U3CSendAsyncU3Ec__AnonStorey2E_t2907890439, ___length_1)); }
	inline int32_t get_length_1() const { return ___length_1; }
	inline int32_t* get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(int32_t value)
	{
		___length_1 = value;
	}

	inline static int32_t get_offset_of_completed_2() { return static_cast<int32_t>(offsetof(U3CSendAsyncU3Ec__AnonStorey2E_t2907890439, ___completed_2)); }
	inline Action_1_t872614854 * get_completed_2() const { return ___completed_2; }
	inline Action_1_t872614854 ** get_address_of_completed_2() { return &___completed_2; }
	inline void set_completed_2(Action_1_t872614854 * value)
	{
		___completed_2 = value;
		Il2CppCodeGenWriteBarrier(&___completed_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_3() { return static_cast<int32_t>(offsetof(U3CSendAsyncU3Ec__AnonStorey2E_t2907890439, ___U3CU3Ef__this_3)); }
	inline WebSocket_t1342580397 * get_U3CU3Ef__this_3() const { return ___U3CU3Ef__this_3; }
	inline WebSocket_t1342580397 ** get_address_of_U3CU3Ef__this_3() { return &___U3CU3Ef__this_3; }
	inline void set_U3CU3Ef__this_3(WebSocket_t1342580397 * value)
	{
		___U3CU3Ef__this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
