﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSONObject/GetFieldResponse
struct GetFieldResponse_t2312868493;
// System.Object
struct Il2CppObject;
// JSONObject
struct JSONObject_t1752376903;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_JSONObject1752376903.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void JSONObject/GetFieldResponse::.ctor(System.Object,System.IntPtr)
extern "C"  void GetFieldResponse__ctor_m4162696676 (GetFieldResponse_t2312868493 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject/GetFieldResponse::Invoke(JSONObject)
extern "C"  void GetFieldResponse_Invoke_m194478999 (GetFieldResponse_t2312868493 * __this, JSONObject_t1752376903 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult JSONObject/GetFieldResponse::BeginInvoke(JSONObject,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetFieldResponse_BeginInvoke_m2545021624 (GetFieldResponse_t2312868493 * __this, JSONObject_t1752376903 * ___obj0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject/GetFieldResponse::EndInvoke(System.IAsyncResult)
extern "C"  void GetFieldResponse_EndInvoke_m1365228020 (GetFieldResponse_t2312868493 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
