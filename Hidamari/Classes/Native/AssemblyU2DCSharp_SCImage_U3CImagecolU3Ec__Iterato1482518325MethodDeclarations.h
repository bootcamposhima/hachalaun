﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SCImage/<Imagecol>c__Iterator16
struct U3CImagecolU3Ec__Iterator16_t1482518325;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void SCImage/<Imagecol>c__Iterator16::.ctor()
extern "C"  void U3CImagecolU3Ec__Iterator16__ctor_m559993814 (U3CImagecolU3Ec__Iterator16_t1482518325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SCImage/<Imagecol>c__Iterator16::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CImagecolU3Ec__Iterator16_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1456265532 (U3CImagecolU3Ec__Iterator16_t1482518325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SCImage/<Imagecol>c__Iterator16::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CImagecolU3Ec__Iterator16_System_Collections_IEnumerator_get_Current_m1508955344 (U3CImagecolU3Ec__Iterator16_t1482518325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SCImage/<Imagecol>c__Iterator16::MoveNext()
extern "C"  bool U3CImagecolU3Ec__Iterator16_MoveNext_m2545660510 (U3CImagecolU3Ec__Iterator16_t1482518325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCImage/<Imagecol>c__Iterator16::Dispose()
extern "C"  void U3CImagecolU3Ec__Iterator16_Dispose_m1181426323 (U3CImagecolU3Ec__Iterator16_t1482518325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SCImage/<Imagecol>c__Iterator16::Reset()
extern "C"  void U3CImagecolU3Ec__Iterator16_Reset_m2501394051 (U3CImagecolU3Ec__Iterator16_t1482518325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
