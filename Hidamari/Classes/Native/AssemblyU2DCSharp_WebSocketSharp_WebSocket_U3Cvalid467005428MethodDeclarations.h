﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.WebSocket/<validateSecWebSocketProtocolHeader>c__AnonStorey2C
struct U3CvalidateSecWebSocketProtocolHeaderU3Ec__AnonStorey2C_t467005428;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void WebSocketSharp.WebSocket/<validateSecWebSocketProtocolHeader>c__AnonStorey2C::.ctor()
extern "C"  void U3CvalidateSecWebSocketProtocolHeaderU3Ec__AnonStorey2C__ctor_m2077357351 (U3CvalidateSecWebSocketProtocolHeaderU3Ec__AnonStorey2C_t467005428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket/<validateSecWebSocketProtocolHeader>c__AnonStorey2C::<>m__13(System.String)
extern "C"  bool U3CvalidateSecWebSocketProtocolHeaderU3Ec__AnonStorey2C_U3CU3Em__13_m253440292 (U3CvalidateSecWebSocketProtocolHeaderU3Ec__AnonStorey2C_t467005428 * __this, String_t* ___protocol0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
