﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t2362096582;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.AndroidJavaObject::.cctor()
extern "C"  void AndroidJavaObject__cctor_m2323169220 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AndroidJavaObject::Dispose()
extern "C"  void AndroidJavaObject_Dispose_m962165510 (AndroidJavaObject_t2362096582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AndroidJavaObject::Finalize()
extern "C"  void AndroidJavaObject_Finalize_m3018091193 (AndroidJavaObject_t2362096582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AndroidJavaObject::Dispose(System.Boolean)
extern "C"  void AndroidJavaObject_Dispose_m4080102909 (AndroidJavaObject_t2362096582 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AndroidJavaObject::_Dispose()
extern "C"  void AndroidJavaObject__Dispose_m3642455739 (AndroidJavaObject_t2362096582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
