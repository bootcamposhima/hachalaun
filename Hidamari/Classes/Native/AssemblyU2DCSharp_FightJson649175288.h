﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType1744280289.h"
#include "AssemblyU2DCSharp_AIDatas4008896193.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FightJson
struct  FightJson_t649175288 
{
public:
	// System.Int32 FightJson::num
	int32_t ___num_0;
	// System.Int32 FightJson::hp
	int32_t ___hp_1;
	// AIDatas FightJson::aidatas
	AIDatas_t4008896193  ___aidatas_2;

public:
	inline static int32_t get_offset_of_num_0() { return static_cast<int32_t>(offsetof(FightJson_t649175288, ___num_0)); }
	inline int32_t get_num_0() const { return ___num_0; }
	inline int32_t* get_address_of_num_0() { return &___num_0; }
	inline void set_num_0(int32_t value)
	{
		___num_0 = value;
	}

	inline static int32_t get_offset_of_hp_1() { return static_cast<int32_t>(offsetof(FightJson_t649175288, ___hp_1)); }
	inline int32_t get_hp_1() const { return ___hp_1; }
	inline int32_t* get_address_of_hp_1() { return &___hp_1; }
	inline void set_hp_1(int32_t value)
	{
		___hp_1 = value;
	}

	inline static int32_t get_offset_of_aidatas_2() { return static_cast<int32_t>(offsetof(FightJson_t649175288, ___aidatas_2)); }
	inline AIDatas_t4008896193  get_aidatas_2() const { return ___aidatas_2; }
	inline AIDatas_t4008896193 * get_address_of_aidatas_2() { return &___aidatas_2; }
	inline void set_aidatas_2(AIDatas_t4008896193  value)
	{
		___aidatas_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: FightJson
struct FightJson_t649175288_marshaled_pinvoke
{
	int32_t ___num_0;
	int32_t ___hp_1;
	AIDatas_t4008896193_marshaled_pinvoke ___aidatas_2;
};
// Native definition for marshalling of: FightJson
struct FightJson_t649175288_marshaled_com
{
	int32_t ___num_0;
	int32_t ___hp_1;
	AIDatas_t4008896193_marshaled_com ___aidatas_2;
};
