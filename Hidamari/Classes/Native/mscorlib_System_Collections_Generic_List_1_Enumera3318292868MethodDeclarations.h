﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<AIData>
struct List_1_t3298620098;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3318292868.h"
#include "AssemblyU2DCSharp_AIData1930434546.h"

// System.Void System.Collections.Generic.List`1/Enumerator<AIData>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3522216069_gshared (Enumerator_t3318292868 * __this, List_1_t3298620098 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m3522216069(__this, ___l0, method) ((  void (*) (Enumerator_t3318292868 *, List_1_t3298620098 *, const MethodInfo*))Enumerator__ctor_m3522216069_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<AIData>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1634686125_gshared (Enumerator_t3318292868 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1634686125(__this, method) ((  void (*) (Enumerator_t3318292868 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1634686125_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<AIData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3088820259_gshared (Enumerator_t3318292868 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3088820259(__this, method) ((  Il2CppObject * (*) (Enumerator_t3318292868 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3088820259_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<AIData>::Dispose()
extern "C"  void Enumerator_Dispose_m1232994218_gshared (Enumerator_t3318292868 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1232994218(__this, method) ((  void (*) (Enumerator_t3318292868 *, const MethodInfo*))Enumerator_Dispose_m1232994218_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<AIData>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3203783139_gshared (Enumerator_t3318292868 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m3203783139(__this, method) ((  void (*) (Enumerator_t3318292868 *, const MethodInfo*))Enumerator_VerifyState_m3203783139_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<AIData>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2991611485_gshared (Enumerator_t3318292868 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2991611485(__this, method) ((  bool (*) (Enumerator_t3318292868 *, const MethodInfo*))Enumerator_MoveNext_m2991611485_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<AIData>::get_Current()
extern "C"  AIData_t1930434546  Enumerator_get_Current_m3207885500_gshared (Enumerator_t3318292868 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3207885500(__this, method) ((  AIData_t1930434546  (*) (Enumerator_t3318292868 *, const MethodInfo*))Enumerator_get_Current_m3207885500_gshared)(__this, method)
