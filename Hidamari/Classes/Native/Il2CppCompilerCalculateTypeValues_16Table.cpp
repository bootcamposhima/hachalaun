﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules2889237774.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleA1657757719.h"
#include "UnityEngine_UnityEngineInternal_GenericStack931085639.h"
#include "UnityEngine_UnityEngineInternal_NetFxCoreExtension2541795172.h"
#include "UnityEngine_UnityEngine_Events_UnityAction594794173.h"
#include "LitJson_U3CModuleU3E86524790.h"
#include "LitJson_LitJson_JsonType1715515030.h"
#include "LitJson_LitJson_JsonMockWrapper721699511.h"
#include "LitJson_LitJson_JsonData1715015430.h"
#include "LitJson_LitJson_OrderedDictionaryEnumerator3526912157.h"
#include "LitJson_LitJson_JsonException3617621405.h"
#include "LitJson_LitJson_PropertyMetadata4066634616.h"
#include "LitJson_LitJson_ArrayMetadata4058342910.h"
#include "LitJson_LitJson_ObjectMetadata2009294498.h"
#include "LitJson_LitJson_ExporterFunc3330360473.h"
#include "LitJson_LitJson_ImporterFunc2138319818.h"
#include "LitJson_LitJson_WrapperFactory3264289803.h"
#include "LitJson_LitJson_JsonMapper863513565.h"
#include "LitJson_LitJson_JsonToken621800391.h"
#include "LitJson_LitJson_JsonReader1009895007.h"
#include "LitJson_LitJson_Condition853519089.h"
#include "LitJson_LitJson_WriterContext3060158226.h"
#include "LitJson_LitJson_JsonWriter1165300239.h"
#include "LitJson_LitJson_FsmContext3936467683.h"
#include "LitJson_LitJson_Lexer3664372066.h"
#include "LitJson_LitJson_Lexer_StateHandler3261942315.h"
#include "LitJson_LitJson_ParserToken608116400.h"
#include "LitJson_U3CPrivateImplementationDetailsU3EU7Bd7db52637124422.h"
#include "LitJson_U3CPrivateImplementationDetailsU3EU7Bd7db52565356614.h"
#include "LitJson_U3CPrivateImplementationDetailsU3EU7Bd7db52216643725.h"
#include "UnityEngine_UI_U3CModuleU3E86524790.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventHandl1938870832.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSyste2276120119.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigge672607302.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigger95600550.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigge849715470.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg3843052064.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve2704060668.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_MoveDirect2840182460.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycasterM2104732159.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastRes3762661364.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviou2511441271.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_AxisEventD3355659985.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_AbstractEv1322796528.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD2054899105.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1848751023.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEven384979233.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve2820857440.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseInputMod15847059.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp3771003169.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp2039702646.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp3613479957.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp4082623702.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_Standalone1096194655.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_Standalone1573608962.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_TouchInputM894675487.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseRaycas2327671059.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_Physics2DRa935388869.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PhysicsRay1170055767.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorT723277650.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorT678919098.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Color4283662044.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Float2711705593.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_FloatT493535356.h"
#include "UnityEngine_UI_UnityEngine_UI_AnimationTriggers115197445.h"
#include "UnityEngine_UI_UnityEngine_UI_Button3896396478.h"
#include "UnityEngine_UI_UnityEngine_UI_Button_ButtonClicked2796375743.h"
#include "UnityEngine_UI_UnityEngine_UI_Button_U3COnFinishSu3118111730.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdate2847075725.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdateRegistry192658922.h"
#include "UnityEngine_UI_UnityEngine_UI_ColorBlock508458230.h"
#include "UnityEngine_UI_UnityEngine_UI_DefaultControls4097465341.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1600 = { sizeof (TypeInferenceRules_t2889237774)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1600[5] = 
{
	TypeInferenceRules_t2889237774::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1601 = { sizeof (TypeInferenceRuleAttribute_t1657757719), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1601[1] = 
{
	TypeInferenceRuleAttribute_t1657757719::get_offset_of__rule_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1602 = { sizeof (GenericStack_t931085639), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1603 = { sizeof (NetFxCoreExtensions_t2541795172), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1604 = { sizeof (UnityAction_t594794173), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1605 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1606 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1607 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1608 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1609 = { sizeof (U3CModuleU3E_t86524795), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1610 = { sizeof (JsonType_t1715515030)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1610[9] = 
{
	JsonType_t1715515030::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1611 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1612 = { sizeof (JsonMockWrapper_t721699511), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1613 = { sizeof (JsonData_t1715015430), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1613[10] = 
{
	JsonData_t1715015430::get_offset_of_inst_array_0(),
	JsonData_t1715015430::get_offset_of_inst_boolean_1(),
	JsonData_t1715015430::get_offset_of_inst_double_2(),
	JsonData_t1715015430::get_offset_of_inst_int_3(),
	JsonData_t1715015430::get_offset_of_inst_long_4(),
	JsonData_t1715015430::get_offset_of_inst_object_5(),
	JsonData_t1715015430::get_offset_of_inst_string_6(),
	JsonData_t1715015430::get_offset_of_json_7(),
	JsonData_t1715015430::get_offset_of_type_8(),
	JsonData_t1715015430::get_offset_of_object_list_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1614 = { sizeof (OrderedDictionaryEnumerator_t3526912157), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1614[1] = 
{
	OrderedDictionaryEnumerator_t3526912157::get_offset_of_list_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1615 = { sizeof (JsonException_t3617621405), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1616 = { sizeof (PropertyMetadata_t4066634616)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1616[3] = 
{
	PropertyMetadata_t4066634616::get_offset_of_Info_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropertyMetadata_t4066634616::get_offset_of_IsField_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropertyMetadata_t4066634616::get_offset_of_Type_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1617 = { sizeof (ArrayMetadata_t4058342910)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1617[3] = 
{
	ArrayMetadata_t4058342910::get_offset_of_element_type_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ArrayMetadata_t4058342910::get_offset_of_is_array_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ArrayMetadata_t4058342910::get_offset_of_is_list_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1618 = { sizeof (ObjectMetadata_t2009294498)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1618[3] = 
{
	ObjectMetadata_t2009294498::get_offset_of_element_type_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObjectMetadata_t2009294498::get_offset_of_is_dictionary_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObjectMetadata_t2009294498::get_offset_of_properties_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1619 = { sizeof (ExporterFunc_t3330360473), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1620 = { sizeof (ImporterFunc_t2138319818), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1621 = { sizeof (WrapperFactory_t3264289803), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1622 = { sizeof (JsonMapper_t863513565), -1, sizeof(JsonMapper_t863513565_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1622[38] = 
{
	JsonMapper_t863513565_StaticFields::get_offset_of_max_nesting_depth_0(),
	JsonMapper_t863513565_StaticFields::get_offset_of_datetime_format_1(),
	JsonMapper_t863513565_StaticFields::get_offset_of_base_exporters_table_2(),
	JsonMapper_t863513565_StaticFields::get_offset_of_custom_exporters_table_3(),
	JsonMapper_t863513565_StaticFields::get_offset_of_base_importers_table_4(),
	JsonMapper_t863513565_StaticFields::get_offset_of_custom_importers_table_5(),
	JsonMapper_t863513565_StaticFields::get_offset_of_array_metadata_6(),
	JsonMapper_t863513565_StaticFields::get_offset_of_array_metadata_lock_7(),
	JsonMapper_t863513565_StaticFields::get_offset_of_conv_ops_8(),
	JsonMapper_t863513565_StaticFields::get_offset_of_conv_ops_lock_9(),
	JsonMapper_t863513565_StaticFields::get_offset_of_object_metadata_10(),
	JsonMapper_t863513565_StaticFields::get_offset_of_object_metadata_lock_11(),
	JsonMapper_t863513565_StaticFields::get_offset_of_type_properties_12(),
	JsonMapper_t863513565_StaticFields::get_offset_of_type_properties_lock_13(),
	JsonMapper_t863513565_StaticFields::get_offset_of_static_writer_14(),
	JsonMapper_t863513565_StaticFields::get_offset_of_static_writer_lock_15(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_16(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_17(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_18(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_19(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_20(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_21(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_22(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_23(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache8_24(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache9_25(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cacheA_26(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cacheB_27(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cacheC_28(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cacheD_29(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cacheE_30(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cacheF_31(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache10_32(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache11_33(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache12_34(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache13_35(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache14_36(),
	JsonMapper_t863513565_StaticFields::get_offset_of_U3CU3Ef__amU24cache15_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1623 = { sizeof (JsonToken_t621800391)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1623[13] = 
{
	JsonToken_t621800391::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1624 = { sizeof (JsonReader_t1009895007), -1, sizeof(JsonReader_t1009895007_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1624[15] = 
{
	JsonReader_t1009895007_StaticFields::get_offset_of_parse_table_0(),
	JsonReader_t1009895007::get_offset_of_automaton_stack_1(),
	JsonReader_t1009895007::get_offset_of_current_input_2(),
	JsonReader_t1009895007::get_offset_of_current_symbol_3(),
	JsonReader_t1009895007::get_offset_of_end_of_json_4(),
	JsonReader_t1009895007::get_offset_of_end_of_input_5(),
	JsonReader_t1009895007::get_offset_of_lexer_6(),
	JsonReader_t1009895007::get_offset_of_parser_in_string_7(),
	JsonReader_t1009895007::get_offset_of_parser_return_8(),
	JsonReader_t1009895007::get_offset_of_read_started_9(),
	JsonReader_t1009895007::get_offset_of_reader_10(),
	JsonReader_t1009895007::get_offset_of_reader_is_owned_11(),
	JsonReader_t1009895007::get_offset_of_skip_non_members_12(),
	JsonReader_t1009895007::get_offset_of_token_value_13(),
	JsonReader_t1009895007::get_offset_of_token_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1625 = { sizeof (Condition_t853519089)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1625[6] = 
{
	Condition_t853519089::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1626 = { sizeof (WriterContext_t3060158226), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1626[5] = 
{
	WriterContext_t3060158226::get_offset_of_Count_0(),
	WriterContext_t3060158226::get_offset_of_InArray_1(),
	WriterContext_t3060158226::get_offset_of_InObject_2(),
	WriterContext_t3060158226::get_offset_of_ExpectingValue_3(),
	WriterContext_t3060158226::get_offset_of_Padding_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1627 = { sizeof (JsonWriter_t1165300239), -1, sizeof(JsonWriter_t1165300239_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1627[11] = 
{
	JsonWriter_t1165300239_StaticFields::get_offset_of_number_format_0(),
	JsonWriter_t1165300239::get_offset_of_context_1(),
	JsonWriter_t1165300239::get_offset_of_ctx_stack_2(),
	JsonWriter_t1165300239::get_offset_of_has_reached_end_3(),
	JsonWriter_t1165300239::get_offset_of_hex_seq_4(),
	JsonWriter_t1165300239::get_offset_of_indentation_5(),
	JsonWriter_t1165300239::get_offset_of_indent_value_6(),
	JsonWriter_t1165300239::get_offset_of_inst_string_builder_7(),
	JsonWriter_t1165300239::get_offset_of_pretty_print_8(),
	JsonWriter_t1165300239::get_offset_of_validate_9(),
	JsonWriter_t1165300239::get_offset_of_writer_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1628 = { sizeof (FsmContext_t3936467683), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1628[4] = 
{
	FsmContext_t3936467683::get_offset_of_Return_0(),
	FsmContext_t3936467683::get_offset_of_NextState_1(),
	FsmContext_t3936467683::get_offset_of_L_2(),
	FsmContext_t3936467683::get_offset_of_StateStack_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1629 = { sizeof (Lexer_t3664372066), -1, sizeof(Lexer_t3664372066_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1629[42] = 
{
	Lexer_t3664372066_StaticFields::get_offset_of_fsm_return_table_0(),
	Lexer_t3664372066_StaticFields::get_offset_of_fsm_handler_table_1(),
	Lexer_t3664372066::get_offset_of_allow_comments_2(),
	Lexer_t3664372066::get_offset_of_allow_single_quoted_strings_3(),
	Lexer_t3664372066::get_offset_of_end_of_input_4(),
	Lexer_t3664372066::get_offset_of_fsm_context_5(),
	Lexer_t3664372066::get_offset_of_input_buffer_6(),
	Lexer_t3664372066::get_offset_of_input_char_7(),
	Lexer_t3664372066::get_offset_of_reader_8(),
	Lexer_t3664372066::get_offset_of_state_9(),
	Lexer_t3664372066::get_offset_of_string_buffer_10(),
	Lexer_t3664372066::get_offset_of_string_value_11(),
	Lexer_t3664372066::get_offset_of_token_12(),
	Lexer_t3664372066::get_offset_of_unichar_13(),
	Lexer_t3664372066_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_14(),
	Lexer_t3664372066_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_15(),
	Lexer_t3664372066_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_16(),
	Lexer_t3664372066_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_17(),
	Lexer_t3664372066_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_18(),
	Lexer_t3664372066_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_19(),
	Lexer_t3664372066_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_20(),
	Lexer_t3664372066_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_21(),
	Lexer_t3664372066_StaticFields::get_offset_of_U3CU3Ef__mgU24cache8_22(),
	Lexer_t3664372066_StaticFields::get_offset_of_U3CU3Ef__mgU24cache9_23(),
	Lexer_t3664372066_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheA_24(),
	Lexer_t3664372066_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheB_25(),
	Lexer_t3664372066_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheC_26(),
	Lexer_t3664372066_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheD_27(),
	Lexer_t3664372066_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheE_28(),
	Lexer_t3664372066_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheF_29(),
	Lexer_t3664372066_StaticFields::get_offset_of_U3CU3Ef__mgU24cache10_30(),
	Lexer_t3664372066_StaticFields::get_offset_of_U3CU3Ef__mgU24cache11_31(),
	Lexer_t3664372066_StaticFields::get_offset_of_U3CU3Ef__mgU24cache12_32(),
	Lexer_t3664372066_StaticFields::get_offset_of_U3CU3Ef__mgU24cache13_33(),
	Lexer_t3664372066_StaticFields::get_offset_of_U3CU3Ef__mgU24cache14_34(),
	Lexer_t3664372066_StaticFields::get_offset_of_U3CU3Ef__mgU24cache15_35(),
	Lexer_t3664372066_StaticFields::get_offset_of_U3CU3Ef__mgU24cache16_36(),
	Lexer_t3664372066_StaticFields::get_offset_of_U3CU3Ef__mgU24cache17_37(),
	Lexer_t3664372066_StaticFields::get_offset_of_U3CU3Ef__mgU24cache18_38(),
	Lexer_t3664372066_StaticFields::get_offset_of_U3CU3Ef__mgU24cache19_39(),
	Lexer_t3664372066_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1A_40(),
	Lexer_t3664372066_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1B_41(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1630 = { sizeof (StateHandler_t3261942315), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1631 = { sizeof (ParserToken_t608116400)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1631[20] = 
{
	ParserToken_t608116400::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1632 = { sizeof (U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422), -1, sizeof(U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1632[13] = 
{
	U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422_StaticFields::get_offset_of_U24fieldU2D0_0(),
	U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422_StaticFields::get_offset_of_U24fieldU2D1_1(),
	U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422_StaticFields::get_offset_of_U24fieldU2D2_2(),
	U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422_StaticFields::get_offset_of_U24fieldU2D3_3(),
	U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422_StaticFields::get_offset_of_U24fieldU2D4_4(),
	U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422_StaticFields::get_offset_of_U24fieldU2D5_5(),
	U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422_StaticFields::get_offset_of_U24fieldU2D6_6(),
	U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422_StaticFields::get_offset_of_U24fieldU2D7_7(),
	U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422_StaticFields::get_offset_of_U24fieldU2D8_8(),
	U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422_StaticFields::get_offset_of_U24fieldU2D9_9(),
	U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422_StaticFields::get_offset_of_U24fieldU2DA_10(),
	U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422_StaticFields::get_offset_of_U24fieldU2DB_11(),
	U3CPrivateImplementationDetailsU3EU7Bd7db51aeU2Dd199U2D4277U2Da2a4U2D4bdb68cd3ac9U7D_t2637124422_StaticFields::get_offset_of_U24fieldU2DC_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1633 = { sizeof (U24ArrayTypeU3D12_t2565356614)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t2565356614_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1634 = { sizeof (U24ArrayTypeU3D112_t2216643725)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D112_t2216643725_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1635 = { sizeof (U3CModuleU3E_t86524796), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1636 = { sizeof (EventHandle_t1938870832)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1636[3] = 
{
	EventHandle_t1938870832::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1637 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1638 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1639 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1640 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1641 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1642 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1643 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1644 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1645 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1646 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1647 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1648 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1649 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1650 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1651 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1652 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1653 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1654 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1655 = { sizeof (EventSystem_t2276120119), -1, sizeof(EventSystem_t2276120119_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1655[10] = 
{
	EventSystem_t2276120119::get_offset_of_m_SystemInputModules_2(),
	EventSystem_t2276120119::get_offset_of_m_CurrentInputModule_3(),
	EventSystem_t2276120119::get_offset_of_m_FirstSelected_4(),
	EventSystem_t2276120119::get_offset_of_m_sendNavigationEvents_5(),
	EventSystem_t2276120119::get_offset_of_m_DragThreshold_6(),
	EventSystem_t2276120119::get_offset_of_m_CurrentSelected_7(),
	EventSystem_t2276120119::get_offset_of_m_SelectionGuard_8(),
	EventSystem_t2276120119::get_offset_of_m_DummyData_9(),
	EventSystem_t2276120119_StaticFields::get_offset_of_s_RaycastComparer_10(),
	EventSystem_t2276120119_StaticFields::get_offset_of_U3CcurrentU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1656 = { sizeof (EventTrigger_t672607302), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1656[2] = 
{
	EventTrigger_t672607302::get_offset_of_m_Delegates_2(),
	EventTrigger_t672607302::get_offset_of_delegates_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1657 = { sizeof (TriggerEvent_t95600550), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1658 = { sizeof (Entry_t849715470), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1658[2] = 
{
	Entry_t849715470::get_offset_of_eventID_0(),
	Entry_t849715470::get_offset_of_callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1659 = { sizeof (EventTriggerType_t3843052064)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1659[18] = 
{
	EventTriggerType_t3843052064::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1660 = { sizeof (ExecuteEvents_t2704060668), -1, sizeof(ExecuteEvents_t2704060668_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1660[20] = 
{
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_PointerEnterHandler_0(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_PointerExitHandler_1(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_PointerDownHandler_2(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_PointerUpHandler_3(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_PointerClickHandler_4(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_InitializePotentialDragHandler_5(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_BeginDragHandler_6(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_DragHandler_7(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_EndDragHandler_8(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_DropHandler_9(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_ScrollHandler_10(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_UpdateSelectedHandler_11(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_SelectHandler_12(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_DeselectHandler_13(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_MoveHandler_14(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_SubmitHandler_15(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_CancelHandler_16(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_HandlerListPool_17(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_s_InternalTransformList_18(),
	ExecuteEvents_t2704060668_StaticFields::get_offset_of_U3CU3Ef__amU24cache13_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1661 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1662 = { sizeof (MoveDirection_t2840182460)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1662[6] = 
{
	MoveDirection_t2840182460::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1663 = { sizeof (RaycasterManager_t2104732159), -1, sizeof(RaycasterManager_t2104732159_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1663[1] = 
{
	RaycasterManager_t2104732159_StaticFields::get_offset_of_s_Raycasters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1664 = { sizeof (RaycastResult_t3762661364)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1664[10] = 
{
	RaycastResult_t3762661364::get_offset_of_m_GameObject_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3762661364::get_offset_of_module_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3762661364::get_offset_of_distance_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3762661364::get_offset_of_index_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3762661364::get_offset_of_depth_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3762661364::get_offset_of_sortingLayer_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3762661364::get_offset_of_sortingOrder_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3762661364::get_offset_of_worldPosition_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3762661364::get_offset_of_worldNormal_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3762661364::get_offset_of_screenPosition_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1665 = { sizeof (UIBehaviour_t2511441271), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1666 = { sizeof (AxisEventData_t3355659985), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1666[2] = 
{
	AxisEventData_t3355659985::get_offset_of_U3CmoveVectorU3Ek__BackingField_2(),
	AxisEventData_t3355659985::get_offset_of_U3CmoveDirU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1667 = { sizeof (AbstractEventData_t1322796528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1667[1] = 
{
	AbstractEventData_t1322796528::get_offset_of_m_Used_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1668 = { sizeof (BaseEventData_t2054899105), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1668[1] = 
{
	BaseEventData_t2054899105::get_offset_of_m_EventSystem_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1669 = { sizeof (PointerEventData_t1848751023), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1669[21] = 
{
	PointerEventData_t1848751023::get_offset_of_m_PointerPress_2(),
	PointerEventData_t1848751023::get_offset_of_hovered_3(),
	PointerEventData_t1848751023::get_offset_of_U3CpointerEnterU3Ek__BackingField_4(),
	PointerEventData_t1848751023::get_offset_of_U3ClastPressU3Ek__BackingField_5(),
	PointerEventData_t1848751023::get_offset_of_U3CrawPointerPressU3Ek__BackingField_6(),
	PointerEventData_t1848751023::get_offset_of_U3CpointerDragU3Ek__BackingField_7(),
	PointerEventData_t1848751023::get_offset_of_U3CpointerCurrentRaycastU3Ek__BackingField_8(),
	PointerEventData_t1848751023::get_offset_of_U3CpointerPressRaycastU3Ek__BackingField_9(),
	PointerEventData_t1848751023::get_offset_of_U3CeligibleForClickU3Ek__BackingField_10(),
	PointerEventData_t1848751023::get_offset_of_U3CpointerIdU3Ek__BackingField_11(),
	PointerEventData_t1848751023::get_offset_of_U3CpositionU3Ek__BackingField_12(),
	PointerEventData_t1848751023::get_offset_of_U3CdeltaU3Ek__BackingField_13(),
	PointerEventData_t1848751023::get_offset_of_U3CpressPositionU3Ek__BackingField_14(),
	PointerEventData_t1848751023::get_offset_of_U3CworldPositionU3Ek__BackingField_15(),
	PointerEventData_t1848751023::get_offset_of_U3CworldNormalU3Ek__BackingField_16(),
	PointerEventData_t1848751023::get_offset_of_U3CclickTimeU3Ek__BackingField_17(),
	PointerEventData_t1848751023::get_offset_of_U3CclickCountU3Ek__BackingField_18(),
	PointerEventData_t1848751023::get_offset_of_U3CscrollDeltaU3Ek__BackingField_19(),
	PointerEventData_t1848751023::get_offset_of_U3CuseDragThresholdU3Ek__BackingField_20(),
	PointerEventData_t1848751023::get_offset_of_U3CdraggingU3Ek__BackingField_21(),
	PointerEventData_t1848751023::get_offset_of_U3CbuttonU3Ek__BackingField_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1670 = { sizeof (InputButton_t384979233)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1670[4] = 
{
	InputButton_t384979233::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1671 = { sizeof (FramePressState_t2820857440)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1671[5] = 
{
	FramePressState_t2820857440::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1672 = { sizeof (BaseInputModule_t15847059), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1672[4] = 
{
	BaseInputModule_t15847059::get_offset_of_m_RaycastResultCache_2(),
	BaseInputModule_t15847059::get_offset_of_m_AxisEventData_3(),
	BaseInputModule_t15847059::get_offset_of_m_EventSystem_4(),
	BaseInputModule_t15847059::get_offset_of_m_BaseEventData_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1673 = { sizeof (PointerInputModule_t3771003169), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1673[6] = 
{
	0,
	0,
	0,
	0,
	PointerInputModule_t3771003169::get_offset_of_m_PointerData_10(),
	PointerInputModule_t3771003169::get_offset_of_m_MouseState_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1674 = { sizeof (ButtonState_t2039702646), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1674[2] = 
{
	ButtonState_t2039702646::get_offset_of_m_Button_0(),
	ButtonState_t2039702646::get_offset_of_m_EventData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1675 = { sizeof (MouseState_t3613479957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1675[1] = 
{
	MouseState_t3613479957::get_offset_of_m_TrackedButtons_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1676 = { sizeof (MouseButtonEventData_t4082623702), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1676[2] = 
{
	MouseButtonEventData_t4082623702::get_offset_of_buttonState_0(),
	MouseButtonEventData_t4082623702::get_offset_of_buttonData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1677 = { sizeof (StandaloneInputModule_t1096194655), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1677[12] = 
{
	StandaloneInputModule_t1096194655::get_offset_of_m_PrevActionTime_12(),
	StandaloneInputModule_t1096194655::get_offset_of_m_LastMoveVector_13(),
	StandaloneInputModule_t1096194655::get_offset_of_m_ConsecutiveMoveCount_14(),
	StandaloneInputModule_t1096194655::get_offset_of_m_LastMousePosition_15(),
	StandaloneInputModule_t1096194655::get_offset_of_m_MousePosition_16(),
	StandaloneInputModule_t1096194655::get_offset_of_m_HorizontalAxis_17(),
	StandaloneInputModule_t1096194655::get_offset_of_m_VerticalAxis_18(),
	StandaloneInputModule_t1096194655::get_offset_of_m_SubmitButton_19(),
	StandaloneInputModule_t1096194655::get_offset_of_m_CancelButton_20(),
	StandaloneInputModule_t1096194655::get_offset_of_m_InputActionsPerSecond_21(),
	StandaloneInputModule_t1096194655::get_offset_of_m_RepeatDelay_22(),
	StandaloneInputModule_t1096194655::get_offset_of_m_ForceModuleActive_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1678 = { sizeof (InputMode_t1573608962)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1678[3] = 
{
	InputMode_t1573608962::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1679 = { sizeof (TouchInputModule_t894675487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1679[3] = 
{
	TouchInputModule_t894675487::get_offset_of_m_LastMousePosition_12(),
	TouchInputModule_t894675487::get_offset_of_m_MousePosition_13(),
	TouchInputModule_t894675487::get_offset_of_m_ForceModuleActive_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1680 = { sizeof (BaseRaycaster_t2327671059), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1681 = { sizeof (Physics2DRaycaster_t935388869), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1682 = { sizeof (PhysicsRaycaster_t1170055767), -1, sizeof(PhysicsRaycaster_t1170055767_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1682[4] = 
{
	0,
	PhysicsRaycaster_t1170055767::get_offset_of_m_EventCamera_3(),
	PhysicsRaycaster_t1170055767::get_offset_of_m_EventMask_4(),
	PhysicsRaycaster_t1170055767_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1683 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1684 = { sizeof (ColorTween_t723277650)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1684[6] = 
{
	ColorTween_t723277650::get_offset_of_m_Target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t723277650::get_offset_of_m_StartColor_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t723277650::get_offset_of_m_TargetColor_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t723277650::get_offset_of_m_TweenMode_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t723277650::get_offset_of_m_Duration_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t723277650::get_offset_of_m_IgnoreTimeScale_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1685 = { sizeof (ColorTweenMode_t678919098)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1685[4] = 
{
	ColorTweenMode_t678919098::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1686 = { sizeof (ColorTweenCallback_t4283662044), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1687 = { sizeof (FloatTween_t2711705593)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1687[5] = 
{
	FloatTween_t2711705593::get_offset_of_m_Target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatTween_t2711705593::get_offset_of_m_StartValue_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatTween_t2711705593::get_offset_of_m_TargetValue_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatTween_t2711705593::get_offset_of_m_Duration_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatTween_t2711705593::get_offset_of_m_IgnoreTimeScale_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1688 = { sizeof (FloatTweenCallback_t493535356), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1689 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1689[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1690 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1690[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1691 = { sizeof (AnimationTriggers_t115197445), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1691[8] = 
{
	0,
	0,
	0,
	0,
	AnimationTriggers_t115197445::get_offset_of_m_NormalTrigger_4(),
	AnimationTriggers_t115197445::get_offset_of_m_HighlightedTrigger_5(),
	AnimationTriggers_t115197445::get_offset_of_m_PressedTrigger_6(),
	AnimationTriggers_t115197445::get_offset_of_m_DisabledTrigger_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1692 = { sizeof (Button_t3896396478), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1692[1] = 
{
	Button_t3896396478::get_offset_of_m_OnClick_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1693 = { sizeof (ButtonClickedEvent_t2796375743), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1694 = { sizeof (U3COnFinishSubmitU3Ec__Iterator1_t3118111730), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1694[5] = 
{
	U3COnFinishSubmitU3Ec__Iterator1_t3118111730::get_offset_of_U3CfadeTimeU3E__0_0(),
	U3COnFinishSubmitU3Ec__Iterator1_t3118111730::get_offset_of_U3CelapsedTimeU3E__1_1(),
	U3COnFinishSubmitU3Ec__Iterator1_t3118111730::get_offset_of_U24PC_2(),
	U3COnFinishSubmitU3Ec__Iterator1_t3118111730::get_offset_of_U24current_3(),
	U3COnFinishSubmitU3Ec__Iterator1_t3118111730::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1695 = { sizeof (CanvasUpdate_t2847075725)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1695[7] = 
{
	CanvasUpdate_t2847075725::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1696 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1697 = { sizeof (CanvasUpdateRegistry_t192658922), -1, sizeof(CanvasUpdateRegistry_t192658922_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1697[6] = 
{
	CanvasUpdateRegistry_t192658922_StaticFields::get_offset_of_s_Instance_0(),
	CanvasUpdateRegistry_t192658922::get_offset_of_m_PerformingLayoutUpdate_1(),
	CanvasUpdateRegistry_t192658922::get_offset_of_m_PerformingGraphicUpdate_2(),
	CanvasUpdateRegistry_t192658922::get_offset_of_m_LayoutRebuildQueue_3(),
	CanvasUpdateRegistry_t192658922::get_offset_of_m_GraphicRebuildQueue_4(),
	CanvasUpdateRegistry_t192658922_StaticFields::get_offset_of_s_SortLayoutFunction_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1698 = { sizeof (ColorBlock_t508458230)+ sizeof (Il2CppObject), sizeof(ColorBlock_t508458230_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1698[6] = 
{
	ColorBlock_t508458230::get_offset_of_m_NormalColor_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorBlock_t508458230::get_offset_of_m_HighlightedColor_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorBlock_t508458230::get_offset_of_m_PressedColor_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorBlock_t508458230::get_offset_of_m_DisabledColor_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorBlock_t508458230::get_offset_of_m_ColorMultiplier_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorBlock_t508458230::get_offset_of_m_FadeDuration_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1699 = { sizeof (DefaultControls_t4097465341), -1, sizeof(DefaultControls_t4097465341_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1699[9] = 
{
	0,
	0,
	0,
	DefaultControls_t4097465341_StaticFields::get_offset_of_s_ThickElementSize_3(),
	DefaultControls_t4097465341_StaticFields::get_offset_of_s_ThinElementSize_4(),
	DefaultControls_t4097465341_StaticFields::get_offset_of_s_ImageElementSize_5(),
	DefaultControls_t4097465341_StaticFields::get_offset_of_s_DefaultSelectableColor_6(),
	DefaultControls_t4097465341_StaticFields::get_offset_of_s_PanelColor_7(),
	DefaultControls_t4097465341_StaticFields::get_offset_of_s_TextColor_8(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
