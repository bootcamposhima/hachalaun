﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Func`2<System.Object,WebSocketSharp.Net.AuthenticationSchemes>
struct Func_2_t3498845318;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_Authenticatio3190130368.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void System.Func`2<System.Object,WebSocketSharp.Net.AuthenticationSchemes>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m3049940406_gshared (Func_2_t3498845318 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Func_2__ctor_m3049940406(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t3498845318 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m3049940406_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.Object,WebSocketSharp.Net.AuthenticationSchemes>::Invoke(T)
extern "C"  int32_t Func_2_Invoke_m2795163504_gshared (Func_2_t3498845318 * __this, Il2CppObject * ___arg10, const MethodInfo* method);
#define Func_2_Invoke_m2795163504(__this, ___arg10, method) ((  int32_t (*) (Func_2_t3498845318 *, Il2CppObject *, const MethodInfo*))Func_2_Invoke_m2795163504_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.Object,WebSocketSharp.Net.AuthenticationSchemes>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_2_BeginInvoke_m1692252707_gshared (Func_2_t3498845318 * __this, Il2CppObject * ___arg10, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define Func_2_BeginInvoke_m1692252707(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t3498845318 *, Il2CppObject *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m1692252707_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.Object,WebSocketSharp.Net.AuthenticationSchemes>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Func_2_EndInvoke_m2564979300_gshared (Func_2_t3498845318 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Func_2_EndInvoke_m2564979300(__this, ___result0, method) ((  int32_t (*) (Func_2_t3498845318 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m2564979300_gshared)(__this, ___result0, method)
