﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Factory
struct Factory_t572770538;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "AssemblyU2DCSharp_Charctor1500651146.h"
#include "AssemblyU2DCSharp_AIData1930434546.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Enemy
struct  Enemy_t67100520  : public Charctor_t1500651146
{
public:
	// AIData Enemy::_aidata
	AIData_t1930434546  ____aidata_10;
	// System.Int32 Enemy::_ainum
	int32_t ____ainum_11;
	// Factory Enemy::f
	Factory_t572770538 * ___f_12;
	// System.Boolean Enemy::isStart
	bool ___isStart_13;
	// System.Single Enemy::hayasa
	float ___hayasa_14;
	// System.Single Enemy::_bullettechnich
	float ____bullettechnich_15;
	// UnityEngine.GameObject Enemy::shotPar
	GameObject_t3674682005 * ___shotPar_16;
	// System.Boolean Enemy::isEnd
	bool ___isEnd_17;

public:
	inline static int32_t get_offset_of__aidata_10() { return static_cast<int32_t>(offsetof(Enemy_t67100520, ____aidata_10)); }
	inline AIData_t1930434546  get__aidata_10() const { return ____aidata_10; }
	inline AIData_t1930434546 * get_address_of__aidata_10() { return &____aidata_10; }
	inline void set__aidata_10(AIData_t1930434546  value)
	{
		____aidata_10 = value;
	}

	inline static int32_t get_offset_of__ainum_11() { return static_cast<int32_t>(offsetof(Enemy_t67100520, ____ainum_11)); }
	inline int32_t get__ainum_11() const { return ____ainum_11; }
	inline int32_t* get_address_of__ainum_11() { return &____ainum_11; }
	inline void set__ainum_11(int32_t value)
	{
		____ainum_11 = value;
	}

	inline static int32_t get_offset_of_f_12() { return static_cast<int32_t>(offsetof(Enemy_t67100520, ___f_12)); }
	inline Factory_t572770538 * get_f_12() const { return ___f_12; }
	inline Factory_t572770538 ** get_address_of_f_12() { return &___f_12; }
	inline void set_f_12(Factory_t572770538 * value)
	{
		___f_12 = value;
		Il2CppCodeGenWriteBarrier(&___f_12, value);
	}

	inline static int32_t get_offset_of_isStart_13() { return static_cast<int32_t>(offsetof(Enemy_t67100520, ___isStart_13)); }
	inline bool get_isStart_13() const { return ___isStart_13; }
	inline bool* get_address_of_isStart_13() { return &___isStart_13; }
	inline void set_isStart_13(bool value)
	{
		___isStart_13 = value;
	}

	inline static int32_t get_offset_of_hayasa_14() { return static_cast<int32_t>(offsetof(Enemy_t67100520, ___hayasa_14)); }
	inline float get_hayasa_14() const { return ___hayasa_14; }
	inline float* get_address_of_hayasa_14() { return &___hayasa_14; }
	inline void set_hayasa_14(float value)
	{
		___hayasa_14 = value;
	}

	inline static int32_t get_offset_of__bullettechnich_15() { return static_cast<int32_t>(offsetof(Enemy_t67100520, ____bullettechnich_15)); }
	inline float get__bullettechnich_15() const { return ____bullettechnich_15; }
	inline float* get_address_of__bullettechnich_15() { return &____bullettechnich_15; }
	inline void set__bullettechnich_15(float value)
	{
		____bullettechnich_15 = value;
	}

	inline static int32_t get_offset_of_shotPar_16() { return static_cast<int32_t>(offsetof(Enemy_t67100520, ___shotPar_16)); }
	inline GameObject_t3674682005 * get_shotPar_16() const { return ___shotPar_16; }
	inline GameObject_t3674682005 ** get_address_of_shotPar_16() { return &___shotPar_16; }
	inline void set_shotPar_16(GameObject_t3674682005 * value)
	{
		___shotPar_16 = value;
		Il2CppCodeGenWriteBarrier(&___shotPar_16, value);
	}

	inline static int32_t get_offset_of_isEnd_17() { return static_cast<int32_t>(offsetof(Enemy_t67100520, ___isEnd_17)); }
	inline bool get_isEnd_17() const { return ___isEnd_17; }
	inline bool* get_address_of_isEnd_17() { return &___isEnd_17; }
	inline void set_isEnd_17(bool value)
	{
		___isEnd_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
