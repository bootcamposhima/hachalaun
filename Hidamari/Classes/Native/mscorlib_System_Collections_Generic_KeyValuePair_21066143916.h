﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Net.IPAddress
struct IPAddress_t3525271463;
// System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>
struct Dictionary_2_t3185352818;

#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Net.IPAddress,System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>>
struct  KeyValuePair_2_t1066143916 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	IPAddress_t3525271463 * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	Dictionary_2_t3185352818 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1066143916, ___key_0)); }
	inline IPAddress_t3525271463 * get_key_0() const { return ___key_0; }
	inline IPAddress_t3525271463 ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(IPAddress_t3525271463 * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier(&___key_0, value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1066143916, ___value_1)); }
	inline Dictionary_2_t3185352818 * get_value_1() const { return ___value_1; }
	inline Dictionary_2_t3185352818 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Dictionary_2_t3185352818 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier(&___value_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
