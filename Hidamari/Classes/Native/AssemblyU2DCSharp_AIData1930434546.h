﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AIData
struct  AIData_t1930434546 
{
public:
	// System.Int32 AIData::_charactornumber
	int32_t ____charactornumber_0;
	// System.Int32 AIData::_direction
	int32_t ____direction_1;
	// System.Int32[] AIData::_bullet
	Int32U5BU5D_t3230847821* ____bullet_2;
	// System.Int32[] AIData::_position
	Int32U5BU5D_t3230847821* ____position_3;

public:
	inline static int32_t get_offset_of__charactornumber_0() { return static_cast<int32_t>(offsetof(AIData_t1930434546, ____charactornumber_0)); }
	inline int32_t get__charactornumber_0() const { return ____charactornumber_0; }
	inline int32_t* get_address_of__charactornumber_0() { return &____charactornumber_0; }
	inline void set__charactornumber_0(int32_t value)
	{
		____charactornumber_0 = value;
	}

	inline static int32_t get_offset_of__direction_1() { return static_cast<int32_t>(offsetof(AIData_t1930434546, ____direction_1)); }
	inline int32_t get__direction_1() const { return ____direction_1; }
	inline int32_t* get_address_of__direction_1() { return &____direction_1; }
	inline void set__direction_1(int32_t value)
	{
		____direction_1 = value;
	}

	inline static int32_t get_offset_of__bullet_2() { return static_cast<int32_t>(offsetof(AIData_t1930434546, ____bullet_2)); }
	inline Int32U5BU5D_t3230847821* get__bullet_2() const { return ____bullet_2; }
	inline Int32U5BU5D_t3230847821** get_address_of__bullet_2() { return &____bullet_2; }
	inline void set__bullet_2(Int32U5BU5D_t3230847821* value)
	{
		____bullet_2 = value;
		Il2CppCodeGenWriteBarrier(&____bullet_2, value);
	}

	inline static int32_t get_offset_of__position_3() { return static_cast<int32_t>(offsetof(AIData_t1930434546, ____position_3)); }
	inline Int32U5BU5D_t3230847821* get__position_3() const { return ____position_3; }
	inline Int32U5BU5D_t3230847821** get_address_of__position_3() { return &____position_3; }
	inline void set__position_3(Int32U5BU5D_t3230847821* value)
	{
		____position_3 = value;
		Il2CppCodeGenWriteBarrier(&____position_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: AIData
struct AIData_t1930434546_marshaled_pinvoke
{
	int32_t ____charactornumber_0;
	int32_t ____direction_1;
	int32_t* ____bullet_2;
	int32_t* ____position_3;
};
// Native definition for marshalling of: AIData
struct AIData_t1930434546_marshaled_com
{
	int32_t ____charactornumber_0;
	int32_t ____direction_1;
	int32_t* ____bullet_2;
	int32_t* ____position_3;
};
