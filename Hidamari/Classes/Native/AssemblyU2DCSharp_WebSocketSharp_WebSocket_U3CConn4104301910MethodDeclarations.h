﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.WebSocket/<ConnectAsync>c__AnonStorey2D
struct U3CConnectAsyncU3Ec__AnonStorey2D_t4104301910;
// System.IAsyncResult
struct IAsyncResult_t2754620036;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocketSharp.WebSocket/<ConnectAsync>c__AnonStorey2D::.ctor()
extern "C"  void U3CConnectAsyncU3Ec__AnonStorey2D__ctor_m2906769413 (U3CConnectAsyncU3Ec__AnonStorey2D_t4104301910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket/<ConnectAsync>c__AnonStorey2D::<>m__14(System.IAsyncResult)
extern "C"  void U3CConnectAsyncU3Ec__AnonStorey2D_U3CU3Em__14_m19811154 (U3CConnectAsyncU3Ec__AnonStorey2D_t4104301910 * __this, Il2CppObject * ___ar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
