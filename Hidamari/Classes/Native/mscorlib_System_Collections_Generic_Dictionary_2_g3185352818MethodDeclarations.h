﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g4168079610MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::.ctor()
#define Dictionary_2__ctor_m1048523930(__this, method) ((  void (*) (Dictionary_2_t3185352818 *, const MethodInfo*))Dictionary_2__ctor_m1859298524_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m2620997850(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t3185352818 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3610002771_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m3957247893(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t3185352818 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m256662076_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::.ctor(System.Int32)
#define Dictionary_2__ctor_m3198172852(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t3185352818 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m3273912365_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m4063314824(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t3185352818 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1930793793_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m3947596233(__this, ___capacity0, ___comparer1, method) ((  void (*) (Dictionary_2_t3185352818 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2352173552_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m1477217444(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3185352818 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m1549788189_gshared)(__this, ___info0, ___context1, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m1931693947(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t3185352818 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m2843055522_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m1971301930(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3185352818 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m2020057553_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m3825786343(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3185352818 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m2894265824_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m1427618920(__this, ___key0, method) ((  void (*) (Dictionary_2_t3185352818 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m127000079_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m94310089(__this, method) ((  bool (*) (Dictionary_2_t3185352818 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m4062325186_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1923731899(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3185352818 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m4065571764_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3350415821(__this, method) ((  bool (*) (Dictionary_2_t3185352818 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m840305542_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1236014334(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t3185352818 *, KeyValuePair_2_t3084133524 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2185230117_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m392731720(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3185352818 *, KeyValuePair_2_t3084133524 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3713378305_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3799581026(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3185352818 *, KeyValuePair_2U5BU5D_t1781068445*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m4220340169_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m998323565(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3185352818 *, KeyValuePair_2_t3084133524 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3330268006_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m3431004353(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3185352818 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m323672040_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m2031688592(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3185352818 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m464503287_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1923990407(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3185352818 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1289662318_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2887531476(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3185352818 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3187662523_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::get_Count()
#define Dictionary_2_get_Count_m487905068(__this, method) ((  int32_t (*) (Dictionary_2_t3185352818 *, const MethodInfo*))Dictionary_2_get_Count_m655926012_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::get_Item(TKey)
#define Dictionary_2_get_Item_m3797018570(__this, ___key0, method) ((  EndPointListener_t3188089579 * (*) (Dictionary_2_t3185352818 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m542157067_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m252971019(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3185352818 *, int32_t, EndPointListener_t3188089579 *, const MethodInfo*))Dictionary_2_set_Item_m3219597724_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m2381808987(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t3185352818 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m3161627732_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m2323969980(__this, ___size0, method) ((  void (*) (Dictionary_2_t3185352818 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m3089254883_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m1336777400(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3185352818 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m3741359263_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m2871916620(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t3084133524  (*) (Il2CppObject * /* static, unused */, int32_t, EndPointListener_t3188089579 *, const MethodInfo*))Dictionary_2_make_pair_m2338171699_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m4036710898(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, EndPointListener_t3188089579 *, const MethodInfo*))Dictionary_2_pick_key_m1394751787_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m1224165454(__this /* static, unused */, ___key0, ___value1, method) ((  EndPointListener_t3188089579 * (*) (Il2CppObject * /* static, unused */, int32_t, EndPointListener_t3188089579 *, const MethodInfo*))Dictionary_2_pick_value_m1281047495_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m838343191(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3185352818 *, KeyValuePair_2U5BU5D_t1781068445*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m2503627344_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::Resize()
#define Dictionary_2_Resize_m2184666805(__this, method) ((  void (*) (Dictionary_2_t3185352818 *, const MethodInfo*))Dictionary_2_Resize_m1861476060_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::Add(TKey,TValue)
#define Dictionary_2_Add_m1180753458(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3185352818 *, int32_t, EndPointListener_t3188089579 *, const MethodInfo*))Dictionary_2_Add_m2232043353_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::Clear()
#define Dictionary_2_Clear_m2600993294(__this, method) ((  void (*) (Dictionary_2_t3185352818 *, const MethodInfo*))Dictionary_2_Clear_m3560399111_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m968586205(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3185352818 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m2612169713_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m4114815672(__this, ___value0, method) ((  bool (*) (Dictionary_2_t3185352818 *, EndPointListener_t3188089579 *, const MethodInfo*))Dictionary_2_ContainsValue_m454328177_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m1960374273(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3185352818 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m3426598522_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m719122691(__this, ___sender0, method) ((  void (*) (Dictionary_2_t3185352818 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m3983879210_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::Remove(TKey)
#define Dictionary_2_Remove_m2585425131(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3185352818 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m183515743_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m893518609(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t3185352818 *, int32_t, EndPointListener_t3188089579 **, const MethodInfo*))Dictionary_2_TryGetValue_m2515559242_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::get_Keys()
#define Dictionary_2_get_Keys_m1337909802(__this, method) ((  KeyCollection_t517144973 * (*) (Dictionary_2_t3185352818 *, const MethodInfo*))Dictionary_2_get_Keys_m4120714641_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::get_Values()
#define Dictionary_2_get_Values_m1687097414(__this, method) ((  ValueCollection_t1885958531 * (*) (Dictionary_2_t3185352818 *, const MethodInfo*))Dictionary_2_get_Values_m1815086189_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m3486569805(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t3185352818 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m844610694_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m3831446889(__this, ___value0, method) ((  EndPointListener_t3188089579 * (*) (Dictionary_2_t3185352818 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m3888328930_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m211743003(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t3185352818 *, KeyValuePair_2_t3084133524 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m139391042_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m3031260078(__this, method) ((  Enumerator_t207708914  (*) (Dictionary_2_t3185352818 *, const MethodInfo*))Dictionary_2_GetEnumerator_m3720989159_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m1119098021(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, int32_t, EndPointListener_t3188089579 *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m2937104030_gshared)(__this /* static, unused */, ___key0, ___value1, method)
