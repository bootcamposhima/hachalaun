﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>
struct Dictionary_2_t1025825965;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2343149357.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_924606671.h"
#include "AssemblyU2DCSharp_WebSocketSharp_CompressionMethod2226596781.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3563270077_gshared (Enumerator_t2343149357 * __this, Dictionary_2_t1025825965 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m3563270077(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2343149357 *, Dictionary_2_t1025825965 *, const MethodInfo*))Enumerator__ctor_m3563270077_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1341511502_gshared (Enumerator_t2343149357 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1341511502(__this, method) ((  Il2CppObject * (*) (Enumerator_t2343149357 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1341511502_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2086888664_gshared (Enumerator_t2343149357 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2086888664(__this, method) ((  void (*) (Enumerator_t2343149357 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2086888664_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t1751606614  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2409560079_gshared (Enumerator_t2343149357 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2409560079(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t2343149357 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2409560079_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m195944874_gshared (Enumerator_t2343149357 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m195944874(__this, method) ((  Il2CppObject * (*) (Enumerator_t2343149357 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m195944874_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m372899260_gshared (Enumerator_t2343149357 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m372899260(__this, method) ((  Il2CppObject * (*) (Enumerator_t2343149357 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m372899260_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2059000200_gshared (Enumerator_t2343149357 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2059000200(__this, method) ((  bool (*) (Enumerator_t2343149357 *, const MethodInfo*))Enumerator_MoveNext_m2059000200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t924606671  Enumerator_get_Current_m2916729652_gshared (Enumerator_t2343149357 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2916729652(__this, method) ((  KeyValuePair_2_t924606671  (*) (Enumerator_t2343149357 *, const MethodInfo*))Enumerator_get_Current_m2916729652_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.Object>::get_CurrentKey()
extern "C"  uint8_t Enumerator_get_CurrentKey_m4242206481_gshared (Enumerator_t2343149357 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m4242206481(__this, method) ((  uint8_t (*) (Enumerator_t2343149357 *, const MethodInfo*))Enumerator_get_CurrentKey_m4242206481_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m2318948881_gshared (Enumerator_t2343149357 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m2318948881(__this, method) ((  Il2CppObject * (*) (Enumerator_t2343149357 *, const MethodInfo*))Enumerator_get_CurrentValue_m2318948881_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m619019919_gshared (Enumerator_t2343149357 * __this, const MethodInfo* method);
#define Enumerator_Reset_m619019919(__this, method) ((  void (*) (Enumerator_t2343149357 *, const MethodInfo*))Enumerator_Reset_m619019919_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m889022296_gshared (Enumerator_t2343149357 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m889022296(__this, method) ((  void (*) (Enumerator_t2343149357 *, const MethodInfo*))Enumerator_VerifyState_m889022296_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.Object>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m2396806336_gshared (Enumerator_t2343149357 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m2396806336(__this, method) ((  void (*) (Enumerator_t2343149357 *, const MethodInfo*))Enumerator_VerifyCurrent_m2396806336_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<WebSocketSharp.CompressionMethod,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m401117087_gshared (Enumerator_t2343149357 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m401117087(__this, method) ((  void (*) (Enumerator_t2343149357 *, const MethodInfo*))Enumerator_Dispose_m401117087_gshared)(__this, method)
