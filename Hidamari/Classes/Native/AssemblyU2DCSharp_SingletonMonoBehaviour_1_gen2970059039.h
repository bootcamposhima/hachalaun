﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// TextLodaer
struct TextLodaer_t3639884186;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SingletonMonoBehaviour`1<TextLodaer>
struct  SingletonMonoBehaviour_1_t2970059039  : public MonoBehaviour_t667441552
{
public:

public:
};

struct SingletonMonoBehaviour_1_t2970059039_StaticFields
{
public:
	// T SingletonMonoBehaviour`1::instance
	TextLodaer_t3639884186 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(SingletonMonoBehaviour_1_t2970059039_StaticFields, ___instance_2)); }
	inline TextLodaer_t3639884186 * get_instance_2() const { return ___instance_2; }
	inline TextLodaer_t3639884186 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(TextLodaer_t3639884186 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
