﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.Net.ReadBufferState
struct ReadBufferState_t1290870949;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// WebSocketSharp.Net.HttpStreamAsyncResult
struct HttpStreamAsyncResult_t303264603;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_HttpStreamAsyn303264603.h"

// System.Void WebSocketSharp.Net.ReadBufferState::.ctor(System.Byte[],System.Int32,System.Int32,WebSocketSharp.Net.HttpStreamAsyncResult)
extern "C"  void ReadBufferState__ctor_m411392228 (ReadBufferState_t1290870949 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, HttpStreamAsyncResult_t303264603 * ___asyncResult3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.Net.HttpStreamAsyncResult WebSocketSharp.Net.ReadBufferState::get_AsyncResult()
extern "C"  HttpStreamAsyncResult_t303264603 * ReadBufferState_get_AsyncResult_m2846895106 (ReadBufferState_t1290870949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.ReadBufferState::set_AsyncResult(WebSocketSharp.Net.HttpStreamAsyncResult)
extern "C"  void ReadBufferState_set_AsyncResult_m2908769889 (ReadBufferState_t1290870949 * __this, HttpStreamAsyncResult_t303264603 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] WebSocketSharp.Net.ReadBufferState::get_Buffer()
extern "C"  ByteU5BU5D_t4260760469* ReadBufferState_get_Buffer_m1343911707 (ReadBufferState_t1290870949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.ReadBufferState::set_Buffer(System.Byte[])
extern "C"  void ReadBufferState_set_Buffer_m323394372 (ReadBufferState_t1290870949 * __this, ByteU5BU5D_t4260760469* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 WebSocketSharp.Net.ReadBufferState::get_Count()
extern "C"  int32_t ReadBufferState_get_Count_m286857618 (ReadBufferState_t1290870949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.ReadBufferState::set_Count(System.Int32)
extern "C"  void ReadBufferState_set_Count_m3212932541 (ReadBufferState_t1290870949 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 WebSocketSharp.Net.ReadBufferState::get_InitialCount()
extern "C"  int32_t ReadBufferState_get_InitialCount_m983564522 (ReadBufferState_t1290870949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.ReadBufferState::set_InitialCount(System.Int32)
extern "C"  void ReadBufferState_set_InitialCount_m3804384289 (ReadBufferState_t1290870949 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 WebSocketSharp.Net.ReadBufferState::get_Offset()
extern "C"  int32_t ReadBufferState_get_Offset_m4213714386 (ReadBufferState_t1290870949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.ReadBufferState::set_Offset(System.Int32)
extern "C"  void ReadBufferState_set_Offset_m36012425 (ReadBufferState_t1290870949 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
