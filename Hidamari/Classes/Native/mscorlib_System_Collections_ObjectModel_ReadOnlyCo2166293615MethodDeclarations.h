﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo1432926611MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.ListenerAsyncResult>::.ctor(System.Collections.Generic.IList`1<T>)
#define ReadOnlyCollection_1__ctor_m884628231(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t2166293615 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1366664402_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.ListenerAsyncResult>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1079751025(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t2166293615 *, ListenerAsyncResult_t609216079 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2541166012_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.ListenerAsyncResult>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m1119234425(__this, method) ((  void (*) (ReadOnlyCollection_1_t2166293615 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3473426062_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.ListenerAsyncResult>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1151458072(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t2166293615 *, int32_t, ListenerAsyncResult_t609216079 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3496388003_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.ListenerAsyncResult>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2914894630(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t2166293615 *, ListenerAsyncResult_t609216079 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m348744375_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.ListenerAsyncResult>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3320278238(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2166293615 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1370240873_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.ListenerAsyncResult>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m751567108(__this, ___index0, method) ((  ListenerAsyncResult_t609216079 * (*) (ReadOnlyCollection_1_t2166293615 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3534609325_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.ListenerAsyncResult>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m259241455(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2166293615 *, int32_t, ListenerAsyncResult_t609216079 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3174042042_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.ListenerAsyncResult>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m825574313(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2166293615 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2459576056_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.ListenerAsyncResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1735353398(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2166293615 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1945557633_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.ListenerAsyncResult>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3073804229(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2166293615 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3330065468_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.ListenerAsyncResult>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1377553048(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2166293615 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1628967861_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.ListenerAsyncResult>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m1514851140(__this, method) ((  void (*) (ReadOnlyCollection_1_t2166293615 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m514207119_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.ListenerAsyncResult>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m1992705448(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2166293615 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m736178103_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.ListenerAsyncResult>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2730816752(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2166293615 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3658311565_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.ListenerAsyncResult>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m3145422435(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2166293615 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m2823806264_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.ListenerAsyncResult>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m3118247397(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t2166293615 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m2498539760_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.ListenerAsyncResult>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3761744499(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2166293615 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1730676936_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.ListenerAsyncResult>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3939979444(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2166293615 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1373829189_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.ListenerAsyncResult>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2367639334(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2166293615 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m918746289_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.ListenerAsyncResult>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m29184791(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2166293615 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m932754534_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.ListenerAsyncResult>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m316402946(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2166293615 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2423760339_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.ListenerAsyncResult>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m34344749(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2166293615 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3512499704_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.ListenerAsyncResult>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m4002903418(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2166293615 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m4167408399_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.ListenerAsyncResult>::Contains(T)
#define ReadOnlyCollection_1_Contains_m4201377387(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2166293615 *, ListenerAsyncResult_t609216079 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m687553276_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.ListenerAsyncResult>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m819791777(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2166293615 *, ListenerAsyncResultU5BU5D_t759038870*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m475587820_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.ListenerAsyncResult>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m536223042(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t2166293615 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m809369055_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.ListenerAsyncResult>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m1741358381(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2166293615 *, ListenerAsyncResult_t609216079 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m817393776_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.ListenerAsyncResult>::get_Count()
#define ReadOnlyCollection_1_get_Count_m2259809774(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t2166293615 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m3681678091_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.ListenerAsyncResult>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m2851394756(__this, ___index0, method) ((  ListenerAsyncResult_t609216079 * (*) (ReadOnlyCollection_1_t2166293615 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m2421641197_gshared)(__this, ___index0, method)
