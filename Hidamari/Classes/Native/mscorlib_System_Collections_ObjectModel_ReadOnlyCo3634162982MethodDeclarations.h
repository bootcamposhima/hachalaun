﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo1432926611MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.Cookie>::.ctor(System.Collections.Generic.IList`1<T>)
#define ReadOnlyCollection_1__ctor_m3672087024(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t3634162982 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1366664402_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.Cookie>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3263517658(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t3634162982 *, Cookie_t2077085446 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2541166012_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.Cookie>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m96523312(__this, method) ((  void (*) (ReadOnlyCollection_1_t3634162982 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3473426062_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.Cookie>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m2994097729(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t3634162982 *, int32_t, Cookie_t2077085446 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3496388003_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.Cookie>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3688032985(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t3634162982 *, Cookie_t2077085446 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m348744375_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.Cookie>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m867950599(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3634162982 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1370240873_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.Cookie>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1910816203(__this, ___index0, method) ((  Cookie_t2077085446 * (*) (ReadOnlyCollection_1_t3634162982 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3534609325_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.Cookie>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1509425880(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3634162982 *, int32_t, Cookie_t2077085446 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3174042042_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.Cookie>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3788020886(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3634162982 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2459576056_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.Cookie>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m197203103(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3634162982 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1945557633_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.Cookie>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1665792218(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3634162982 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3330065468_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.Cookie>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m107384023(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3634162982 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1628967861_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.Cookie>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m2087739309(__this, method) ((  void (*) (ReadOnlyCollection_1_t3634162982 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m514207119_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.Cookie>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m2830791381(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3634162982 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m736178103_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.Cookie>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3545661359(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3634162982 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3658311565_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.Cookie>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m137709658(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3634162982 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m2823806264_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.Cookie>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1806554766(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t3634162982 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m2498539760_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.Cookie>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1753978602(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3634162982 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1730676936_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.Cookie>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m418150503(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3634162982 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1373829189_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.Cookie>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m918076115(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3634162982 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m918746289_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.Cookie>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2498897412(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3634162982 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m932754534_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.Cookie>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2474281077(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3634162982 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2423760339_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.Cookie>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m2868462490(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3634162982 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3512499704_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.Cookie>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m4103914929(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3634162982 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m4167408399_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.Cookie>::Contains(T)
#define ReadOnlyCollection_1_Contains_m433128734(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3634162982 *, Cookie_t2077085446 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m687553276_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.Cookie>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m172889354(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3634162982 *, CookieU5BU5D_t4193117731*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m475587820_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.Cookie>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m3724844545(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t3634162982 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m809369055_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.Cookie>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m2846311182(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3634162982 *, Cookie_t2077085446 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m817393776_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.Cookie>::get_Count()
#define ReadOnlyCollection_1_get_Count_m2153608237(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t3634162982 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m3681678091_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<WebSocketSharp.Net.Cookie>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m3598444811(__this, ___index0, method) ((  Cookie_t2077085446 * (*) (ReadOnlyCollection_1_t3634162982 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m2421641197_gshared)(__this, ___index0, method)
