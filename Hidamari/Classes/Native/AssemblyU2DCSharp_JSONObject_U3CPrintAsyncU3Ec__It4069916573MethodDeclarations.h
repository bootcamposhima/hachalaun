﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSONObject/<PrintAsync>c__Iterator18
struct U3CPrintAsyncU3Ec__Iterator18_t4069916573;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.String>
struct IEnumerator_1_t1919096606;

#include "codegen/il2cpp-codegen.h"

// System.Void JSONObject/<PrintAsync>c__Iterator18::.ctor()
extern "C"  void U3CPrintAsyncU3Ec__Iterator18__ctor_m49572766 (U3CPrintAsyncU3Ec__Iterator18_t4069916573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSONObject/<PrintAsync>c__Iterator18::System.Collections.Generic.IEnumerator<string>.get_Current()
extern "C"  String_t* U3CPrintAsyncU3Ec__Iterator18_System_Collections_Generic_IEnumeratorU3CstringU3E_get_Current_m1772489050 (U3CPrintAsyncU3Ec__Iterator18_t4069916573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JSONObject/<PrintAsync>c__Iterator18::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPrintAsyncU3Ec__Iterator18_System_Collections_IEnumerator_get_Current_m3308089874 (U3CPrintAsyncU3Ec__Iterator18_t4069916573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator JSONObject/<PrintAsync>c__Iterator18::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CPrintAsyncU3Ec__Iterator18_System_Collections_IEnumerable_GetEnumerator_m2654096275 (U3CPrintAsyncU3Ec__Iterator18_t4069916573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.String> JSONObject/<PrintAsync>c__Iterator18::System.Collections.Generic.IEnumerable<string>.GetEnumerator()
extern "C"  Il2CppObject* U3CPrintAsyncU3Ec__Iterator18_System_Collections_Generic_IEnumerableU3CstringU3E_GetEnumerator_m3690829213 (U3CPrintAsyncU3Ec__Iterator18_t4069916573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSONObject/<PrintAsync>c__Iterator18::MoveNext()
extern "C"  bool U3CPrintAsyncU3Ec__Iterator18_MoveNext_m2248569726 (U3CPrintAsyncU3Ec__Iterator18_t4069916573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject/<PrintAsync>c__Iterator18::Dispose()
extern "C"  void U3CPrintAsyncU3Ec__Iterator18_Dispose_m293070939 (U3CPrintAsyncU3Ec__Iterator18_t4069916573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject/<PrintAsync>c__Iterator18::Reset()
extern "C"  void U3CPrintAsyncU3Ec__Iterator18_Reset_m1990973003 (U3CPrintAsyncU3Ec__Iterator18_t4069916573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
