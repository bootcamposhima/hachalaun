﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.HandshakeBase
struct HandshakeBase_t1248407470;
// System.Version
struct Version_t763695022;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t2791941106;
// System.String
struct String_t;
// System.Text.Encoding
struct Encoding_t2012439129;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Version763695022.h"
#include "System_System_Collections_Specialized_NameValueCol2791941106.h"
#include "mscorlib_System_String7231557.h"

// System.Void WebSocketSharp.HandshakeBase::.ctor(System.Version,System.Collections.Specialized.NameValueCollection)
extern "C"  void HandshakeBase__ctor_m221550618 (HandshakeBase_t1248407470 * __this, Version_t763695022 * ___version0, NameValueCollection_t2791941106 * ___headers1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.HandshakeBase::get_EntityBody()
extern "C"  String_t* HandshakeBase_get_EntityBody_m1889311511 (HandshakeBase_t1248407470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Specialized.NameValueCollection WebSocketSharp.HandshakeBase::get_Headers()
extern "C"  NameValueCollection_t2791941106 * HandshakeBase_get_Headers_m3632488899 (HandshakeBase_t1248407470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Version WebSocketSharp.HandshakeBase::get_ProtocolVersion()
extern "C"  Version_t763695022 * HandshakeBase_get_ProtocolVersion_m3150344207 (HandshakeBase_t1248407470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding WebSocketSharp.HandshakeBase::getEncoding(System.String)
extern "C"  Encoding_t2012439129 * HandshakeBase_getEncoding_m1341803429 (Il2CppObject * __this /* static, unused */, String_t* ___contentType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] WebSocketSharp.HandshakeBase::ToByteArray()
extern "C"  ByteU5BU5D_t4260760469* HandshakeBase_ToByteArray_m3444172392 (HandshakeBase_t1248407470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
