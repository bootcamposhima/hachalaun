﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HPManager/<DamagetAnimation2>c__Iterator13
struct U3CDamagetAnimation2U3Ec__Iterator13_t529218454;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void HPManager/<DamagetAnimation2>c__Iterator13::.ctor()
extern "C"  void U3CDamagetAnimation2U3Ec__Iterator13__ctor_m1555342789 (U3CDamagetAnimation2U3Ec__Iterator13_t529218454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HPManager/<DamagetAnimation2>c__Iterator13::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDamagetAnimation2U3Ec__Iterator13_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3456292599 (U3CDamagetAnimation2U3Ec__Iterator13_t529218454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HPManager/<DamagetAnimation2>c__Iterator13::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDamagetAnimation2U3Ec__Iterator13_System_Collections_IEnumerator_get_Current_m4011305099 (U3CDamagetAnimation2U3Ec__Iterator13_t529218454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HPManager/<DamagetAnimation2>c__Iterator13::MoveNext()
extern "C"  bool U3CDamagetAnimation2U3Ec__Iterator13_MoveNext_m43441399 (U3CDamagetAnimation2U3Ec__Iterator13_t529218454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HPManager/<DamagetAnimation2>c__Iterator13::Dispose()
extern "C"  void U3CDamagetAnimation2U3Ec__Iterator13_Dispose_m4229051586 (U3CDamagetAnimation2U3Ec__Iterator13_t529218454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HPManager/<DamagetAnimation2>c__Iterator13::Reset()
extern "C"  void U3CDamagetAnimation2U3Ec__Iterator13_Reset_m3496743026 (U3CDamagetAnimation2U3Ec__Iterator13_t529218454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
