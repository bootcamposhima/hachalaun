﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSONObject/<StringifyAsync>c__Iterator19
struct U3CStringifyAsyncU3Ec__Iterator19_t3659654720;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;

#include "codegen/il2cpp-codegen.h"

// System.Void JSONObject/<StringifyAsync>c__Iterator19::.ctor()
extern "C"  void U3CStringifyAsyncU3Ec__Iterator19__ctor_m2271182939 (U3CStringifyAsyncU3Ec__Iterator19_t3659654720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JSONObject/<StringifyAsync>c__Iterator19::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStringifyAsyncU3Ec__Iterator19_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1813370721 (U3CStringifyAsyncU3Ec__Iterator19_t3659654720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object JSONObject/<StringifyAsync>c__Iterator19::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStringifyAsyncU3Ec__Iterator19_System_Collections_IEnumerator_get_Current_m1266466037 (U3CStringifyAsyncU3Ec__Iterator19_t3659654720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator JSONObject/<StringifyAsync>c__Iterator19::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CStringifyAsyncU3Ec__Iterator19_System_Collections_IEnumerable_GetEnumerator_m1310009398 (U3CStringifyAsyncU3Ec__Iterator19_t3659654720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Object> JSONObject/<StringifyAsync>c__Iterator19::System.Collections.Generic.IEnumerable<object>.GetEnumerator()
extern "C"  Il2CppObject* U3CStringifyAsyncU3Ec__Iterator19_System_Collections_Generic_IEnumerableU3CobjectU3E_GetEnumerator_m3603786880 (U3CStringifyAsyncU3Ec__Iterator19_t3659654720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSONObject/<StringifyAsync>c__Iterator19::MoveNext()
extern "C"  bool U3CStringifyAsyncU3Ec__Iterator19_MoveNext_m856051617 (U3CStringifyAsyncU3Ec__Iterator19_t3659654720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject/<StringifyAsync>c__Iterator19::Dispose()
extern "C"  void U3CStringifyAsyncU3Ec__Iterator19_Dispose_m661701080 (U3CStringifyAsyncU3Ec__Iterator19_t3659654720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSONObject/<StringifyAsync>c__Iterator19::Reset()
extern "C"  void U3CStringifyAsyncU3Ec__Iterator19_Reset_m4212583176 (U3CStringifyAsyncU3Ec__Iterator19_t3659654720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
