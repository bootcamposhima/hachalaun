﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen2352633852.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ActionManager
struct  ActionManager_t3022458999  : public SingletonMonoBehaviour_1_t2352633852
{
public:
	// System.Collections.IEnumerator ActionManager::_moveforever
	Il2CppObject * ____moveforever_3;

public:
	inline static int32_t get_offset_of__moveforever_3() { return static_cast<int32_t>(offsetof(ActionManager_t3022458999, ____moveforever_3)); }
	inline Il2CppObject * get__moveforever_3() const { return ____moveforever_3; }
	inline Il2CppObject ** get_address_of__moveforever_3() { return &____moveforever_3; }
	inline void set__moveforever_3(Il2CppObject * value)
	{
		____moveforever_3 = value;
		Il2CppCodeGenWriteBarrier(&____moveforever_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
