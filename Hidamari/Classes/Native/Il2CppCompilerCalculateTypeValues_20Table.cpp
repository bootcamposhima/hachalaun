﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_QueryStringCol580001633.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_ReadBufferSta1290870949.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_RequestStream2929193945.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_ResponseStrea1796293571.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_Security_SslS3623155758.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_WebHeaderColle288332393.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_WebHeaderColle832026752.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_WebHeaderColl1884298844.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_WebHeaderColl3370629034.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_WebSockets_Ht1074545506.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_WebSockets_Ht3255887036.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_WebSockets_Tc3782393199.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_WebSockets_Tc3823346890.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_WebSockets_Web763725542.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Opcode3782140426.h"
#include "AssemblyU2DCSharp_WebSocketSharp_PayloadData39926750.h"
#include "AssemblyU2DCSharp_WebSocketSharp_PayloadData_U3CGet303122823.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Rsv3262172379.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocket1342580397.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocket_U3CU3Ec__90658481.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocket_U3Cclose239095580.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocket_U3CsendA861700891.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocket_U3CsendA861700899.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocket_U3Cstart802946016.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocket_U3Cvalid467005428.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocket_U3CConn4104301910.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocket_U3CSend2907890439.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocketExceptio2311987812.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocketFrame778194306.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocketFrame_U34155755117.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocketFrame_U3C681710991.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocketFrame_U31917037014.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocketFrame_U31758512846.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocketState790259878.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocketStream4103435597.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocketStream_U3526000246.h"
#include "AssemblyU2DCSharp_Test2603186.h"
#include "AssemblyU2DCSharp_TestJson3212538042.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3053238933.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3988332413.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3379220352.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { sizeof (QueryStringCollection_t580001633), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { sizeof (ReadBufferState_t1290870949), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2001[5] = 
{
	ReadBufferState_t1290870949::get_offset_of_U3CAsyncResultU3Ek__BackingField_0(),
	ReadBufferState_t1290870949::get_offset_of_U3CBufferU3Ek__BackingField_1(),
	ReadBufferState_t1290870949::get_offset_of_U3CCountU3Ek__BackingField_2(),
	ReadBufferState_t1290870949::get_offset_of_U3CInitialCountU3Ek__BackingField_3(),
	ReadBufferState_t1290870949::get_offset_of_U3COffsetU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { sizeof (RequestStream_t2929193945), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2002[6] = 
{
	RequestStream_t2929193945::get_offset_of__buffer_1(),
	RequestStream_t2929193945::get_offset_of__disposed_2(),
	RequestStream_t2929193945::get_offset_of__length_3(),
	RequestStream_t2929193945::get_offset_of__offset_4(),
	RequestStream_t2929193945::get_offset_of__remainingBody_5(),
	RequestStream_t2929193945::get_offset_of__stream_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { sizeof (ResponseStream_t1796293571), -1, sizeof(ResponseStream_t1796293571_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2003[6] = 
{
	ResponseStream_t1796293571_StaticFields::get_offset_of__crlf_1(),
	ResponseStream_t1796293571::get_offset_of__disposed_2(),
	ResponseStream_t1796293571::get_offset_of__ignoreErrors_3(),
	ResponseStream_t1796293571::get_offset_of__response_4(),
	ResponseStream_t1796293571::get_offset_of__stream_5(),
	ResponseStream_t1796293571::get_offset_of__trailerSent_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { sizeof (SslStream_t3623155758), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2005 = { sizeof (WebHeaderCollection_t288332393), -1, sizeof(WebHeaderCollection_t288332393_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2005[3] = 
{
	WebHeaderCollection_t288332393_StaticFields::get_offset_of__headers_12(),
	WebHeaderCollection_t288332393::get_offset_of__internallyCreated_13(),
	WebHeaderCollection_t288332393::get_offset_of__state_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2006 = { sizeof (U3CToStringMultiValueU3Ec__AnonStorey25_t832026752), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2006[3] = 
{
	U3CToStringMultiValueU3Ec__AnonStorey25_t832026752::get_offset_of_response_0(),
	U3CToStringMultiValueU3Ec__AnonStorey25_t832026752::get_offset_of_buff_1(),
	U3CToStringMultiValueU3Ec__AnonStorey25_t832026752::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2007 = { sizeof (U3CGetObjectDataU3Ec__AnonStorey26_t1884298844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2007[3] = 
{
	U3CGetObjectDataU3Ec__AnonStorey26_t1884298844::get_offset_of_serializationInfo_0(),
	U3CGetObjectDataU3Ec__AnonStorey26_t1884298844::get_offset_of_count_1(),
	U3CGetObjectDataU3Ec__AnonStorey26_t1884298844::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2008 = { sizeof (U3CToStringU3Ec__AnonStorey27_t3370629034), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2008[2] = 
{
	U3CToStringU3Ec__AnonStorey27_t3370629034::get_offset_of_buff_0(),
	U3CToStringU3Ec__AnonStorey27_t3370629034::get_offset_of_U3CU3Ef__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2009 = { sizeof (HttpListenerWebSocketContext_t1074545506), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2009[2] = 
{
	HttpListenerWebSocketContext_t1074545506::get_offset_of__context_0(),
	HttpListenerWebSocketContext_t1074545506::get_offset_of__websocket_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2010 = { sizeof (U3CU3Ec__Iterator1C_t3255887036), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2010[7] = 
{
	U3CU3Ec__Iterator1C_t3255887036::get_offset_of_U3CprotocolsU3E__0_0(),
	U3CU3Ec__Iterator1C_t3255887036::get_offset_of_U3CU24s_136U3E__1_1(),
	U3CU3Ec__Iterator1C_t3255887036::get_offset_of_U3CU24s_137U3E__2_2(),
	U3CU3Ec__Iterator1C_t3255887036::get_offset_of_U3CprotocolU3E__3_3(),
	U3CU3Ec__Iterator1C_t3255887036::get_offset_of_U24PC_4(),
	U3CU3Ec__Iterator1C_t3255887036::get_offset_of_U24current_5(),
	U3CU3Ec__Iterator1C_t3255887036::get_offset_of_U3CU3Ef__this_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2011 = { sizeof (TcpListenerWebSocketContext_t3782393199), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2011[9] = 
{
	TcpListenerWebSocketContext_t3782393199::get_offset_of__client_0(),
	TcpListenerWebSocketContext_t3782393199::get_offset_of__cookies_1(),
	TcpListenerWebSocketContext_t3782393199::get_offset_of__queryString_2(),
	TcpListenerWebSocketContext_t3782393199::get_offset_of__request_3(),
	TcpListenerWebSocketContext_t3782393199::get_offset_of__secure_4(),
	TcpListenerWebSocketContext_t3782393199::get_offset_of__stream_5(),
	TcpListenerWebSocketContext_t3782393199::get_offset_of__uri_6(),
	TcpListenerWebSocketContext_t3782393199::get_offset_of__user_7(),
	TcpListenerWebSocketContext_t3782393199::get_offset_of__websocket_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2012 = { sizeof (U3CU3Ec__Iterator1D_t3823346890), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2012[7] = 
{
	U3CU3Ec__Iterator1D_t3823346890::get_offset_of_U3CprotocolsU3E__0_0(),
	U3CU3Ec__Iterator1D_t3823346890::get_offset_of_U3CU24s_138U3E__1_1(),
	U3CU3Ec__Iterator1D_t3823346890::get_offset_of_U3CU24s_139U3E__2_2(),
	U3CU3Ec__Iterator1D_t3823346890::get_offset_of_U3CprotocolU3E__3_3(),
	U3CU3Ec__Iterator1D_t3823346890::get_offset_of_U24PC_4(),
	U3CU3Ec__Iterator1D_t3823346890::get_offset_of_U24current_5(),
	U3CU3Ec__Iterator1D_t3823346890::get_offset_of_U3CU3Ef__this_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2013 = { sizeof (WebSocketContext_t763725542), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2014 = { sizeof (Opcode_t3782140426)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2014[7] = 
{
	Opcode_t3782140426::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2015 = { sizeof (PayloadData_t39926750), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2015[4] = 
{
	0,
	PayloadData_t39926750::get_offset_of__applicationData_1(),
	PayloadData_t39926750::get_offset_of__extensionData_2(),
	PayloadData_t39926750::get_offset_of__masked_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2016 = { sizeof (U3CGetEnumeratorU3Ec__Iterator1E_t303122823), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2016[9] = 
{
	U3CGetEnumeratorU3Ec__Iterator1E_t303122823::get_offset_of_U3CU24s_140U3E__0_0(),
	U3CGetEnumeratorU3Ec__Iterator1E_t303122823::get_offset_of_U3CU24s_141U3E__1_1(),
	U3CGetEnumeratorU3Ec__Iterator1E_t303122823::get_offset_of_U3CbU3E__2_2(),
	U3CGetEnumeratorU3Ec__Iterator1E_t303122823::get_offset_of_U3CU24s_142U3E__3_3(),
	U3CGetEnumeratorU3Ec__Iterator1E_t303122823::get_offset_of_U3CU24s_143U3E__4_4(),
	U3CGetEnumeratorU3Ec__Iterator1E_t303122823::get_offset_of_U3CbU3E__5_5(),
	U3CGetEnumeratorU3Ec__Iterator1E_t303122823::get_offset_of_U24PC_6(),
	U3CGetEnumeratorU3Ec__Iterator1E_t303122823::get_offset_of_U24current_7(),
	U3CGetEnumeratorU3Ec__Iterator1E_t303122823::get_offset_of_U3CU3Ef__this_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2017 = { sizeof (Rsv_t3262172379)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2017[3] = 
{
	Rsv_t3262172379::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2018 = { sizeof (WebSocket_t1342580397), -1, sizeof(WebSocket_t1342580397_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2018[38] = 
{
	0,
	0,
	0,
	WebSocket_t1342580397::get_offset_of__authChallenge_3(),
	WebSocket_t1342580397::get_offset_of__base64Key_4(),
	WebSocket_t1342580397::get_offset_of__certValidationCallback_5(),
	WebSocket_t1342580397::get_offset_of__client_6(),
	WebSocket_t1342580397::get_offset_of__closeContext_7(),
	WebSocket_t1342580397::get_offset_of__compression_8(),
	WebSocket_t1342580397::get_offset_of__context_9(),
	WebSocket_t1342580397::get_offset_of__cookies_10(),
	WebSocket_t1342580397::get_offset_of__credentials_11(),
	WebSocket_t1342580397::get_offset_of__extensions_12(),
	WebSocket_t1342580397::get_offset_of__exitReceiving_13(),
	WebSocket_t1342580397::get_offset_of__forConn_14(),
	WebSocket_t1342580397::get_offset_of__forEvent_15(),
	WebSocket_t1342580397::get_offset_of__forMessageEventQueue_16(),
	WebSocket_t1342580397::get_offset_of__forSend_17(),
	WebSocket_t1342580397::get_offset_of__handshakeRequestChecker_18(),
	WebSocket_t1342580397::get_offset_of__logger_19(),
	WebSocket_t1342580397::get_offset_of__messageEventQueue_20(),
	WebSocket_t1342580397::get_offset_of__nonceCount_21(),
	WebSocket_t1342580397::get_offset_of__origin_22(),
	WebSocket_t1342580397::get_offset_of__customHeaders_23(),
	WebSocket_t1342580397::get_offset_of__preAuth_24(),
	WebSocket_t1342580397::get_offset_of__protocol_25(),
	WebSocket_t1342580397::get_offset_of__protocols_26(),
	WebSocket_t1342580397::get_offset_of__readyState_27(),
	WebSocket_t1342580397::get_offset_of__receivePong_28(),
	WebSocket_t1342580397::get_offset_of__secure_29(),
	WebSocket_t1342580397::get_offset_of__stream_30(),
	WebSocket_t1342580397::get_offset_of__tcpClient_31(),
	WebSocket_t1342580397::get_offset_of__uri_32(),
	WebSocket_t1342580397::get_offset_of_OnClose_33(),
	WebSocket_t1342580397::get_offset_of_OnError_34(),
	WebSocket_t1342580397::get_offset_of_OnMessage_35(),
	WebSocket_t1342580397::get_offset_of_OnOpen_36(),
	WebSocket_t1342580397_StaticFields::get_offset_of_U3CU3Ef__amU24cache22_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2019 = { sizeof (U3CU3Ec__Iterator1F_t90658481), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2019[6] = 
{
	U3CU3Ec__Iterator1F_t90658481::get_offset_of_U3CU24s_145U3E__0_0(),
	U3CU3Ec__Iterator1F_t90658481::get_offset_of_U3CcookieU3E__1_1(),
	U3CU3Ec__Iterator1F_t90658481::get_offset_of_U3CU24s_146U3E__2_2(),
	U3CU3Ec__Iterator1F_t90658481::get_offset_of_U24PC_3(),
	U3CU3Ec__Iterator1F_t90658481::get_offset_of_U24current_4(),
	U3CU3Ec__Iterator1F_t90658481::get_offset_of_U3CU3Ef__this_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2020 = { sizeof (U3CcloseAsyncU3Ec__AnonStorey28_t239095580), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2020[1] = 
{
	U3CcloseAsyncU3Ec__AnonStorey28_t239095580::get_offset_of_closer_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2021 = { sizeof (U3CsendAsyncU3Ec__AnonStorey29_t861700891), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2021[3] = 
{
	U3CsendAsyncU3Ec__AnonStorey29_t861700891::get_offset_of_sender_0(),
	U3CsendAsyncU3Ec__AnonStorey29_t861700891::get_offset_of_completed_1(),
	U3CsendAsyncU3Ec__AnonStorey29_t861700891::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2022 = { sizeof (U3CsendAsyncU3Ec__AnonStorey2A_t861700899), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2022[3] = 
{
	U3CsendAsyncU3Ec__AnonStorey2A_t861700899::get_offset_of_sender_0(),
	U3CsendAsyncU3Ec__AnonStorey2A_t861700899::get_offset_of_completed_1(),
	U3CsendAsyncU3Ec__AnonStorey2A_t861700899::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2023 = { sizeof (U3CstartReceivingU3Ec__AnonStorey2B_t802946016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2023[2] = 
{
	U3CstartReceivingU3Ec__AnonStorey2B_t802946016::get_offset_of_receive_0(),
	U3CstartReceivingU3Ec__AnonStorey2B_t802946016::get_offset_of_U3CU3Ef__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2024 = { sizeof (U3CvalidateSecWebSocketProtocolHeaderU3Ec__AnonStorey2C_t467005428), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2024[1] = 
{
	U3CvalidateSecWebSocketProtocolHeaderU3Ec__AnonStorey2C_t467005428::get_offset_of_value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2025 = { sizeof (U3CConnectAsyncU3Ec__AnonStorey2D_t4104301910), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2025[2] = 
{
	U3CConnectAsyncU3Ec__AnonStorey2D_t4104301910::get_offset_of_connector_0(),
	U3CConnectAsyncU3Ec__AnonStorey2D_t4104301910::get_offset_of_U3CU3Ef__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2026 = { sizeof (U3CSendAsyncU3Ec__AnonStorey2E_t2907890439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2026[4] = 
{
	U3CSendAsyncU3Ec__AnonStorey2E_t2907890439::get_offset_of_msg_0(),
	U3CSendAsyncU3Ec__AnonStorey2E_t2907890439::get_offset_of_length_1(),
	U3CSendAsyncU3Ec__AnonStorey2E_t2907890439::get_offset_of_completed_2(),
	U3CSendAsyncU3Ec__AnonStorey2E_t2907890439::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2027 = { sizeof (WebSocketException_t2311987812), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2027[1] = 
{
	WebSocketException_t2311987812::get_offset_of_U3CCodeU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2028 = { sizeof (WebSocketFrame_t778194306), -1, sizeof(WebSocketFrame_t778194306_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2028[11] = 
{
	WebSocketFrame_t778194306::get_offset_of__extPayloadLength_0(),
	WebSocketFrame_t778194306::get_offset_of__fin_1(),
	WebSocketFrame_t778194306::get_offset_of__mask_2(),
	WebSocketFrame_t778194306::get_offset_of__maskingKey_3(),
	WebSocketFrame_t778194306::get_offset_of__opcode_4(),
	WebSocketFrame_t778194306::get_offset_of__payloadData_5(),
	WebSocketFrame_t778194306::get_offset_of__payloadLength_6(),
	WebSocketFrame_t778194306::get_offset_of__rsv1_7(),
	WebSocketFrame_t778194306::get_offset_of__rsv2_8(),
	WebSocketFrame_t778194306::get_offset_of__rsv3_9(),
	WebSocketFrame_t778194306_StaticFields::get_offset_of_EmptyUnmaskPingData_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2029 = { sizeof (U3CGetEnumeratorU3Ec__Iterator20_t4155755117), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2029[6] = 
{
	U3CGetEnumeratorU3Ec__Iterator20_t4155755117::get_offset_of_U3CU24s_168U3E__0_0(),
	U3CGetEnumeratorU3Ec__Iterator20_t4155755117::get_offset_of_U3CU24s_169U3E__1_1(),
	U3CGetEnumeratorU3Ec__Iterator20_t4155755117::get_offset_of_U3CbU3E__2_2(),
	U3CGetEnumeratorU3Ec__Iterator20_t4155755117::get_offset_of_U24PC_3(),
	U3CGetEnumeratorU3Ec__Iterator20_t4155755117::get_offset_of_U24current_4(),
	U3CGetEnumeratorU3Ec__Iterator20_t4155755117::get_offset_of_U3CU3Ef__this_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2030 = { sizeof (U3CdumpU3Ec__AnonStorey2F_t681710991), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2030[2] = 
{
	U3CdumpU3Ec__AnonStorey2F_t681710991::get_offset_of_output_0(),
	U3CdumpU3Ec__AnonStorey2F_t681710991::get_offset_of_lineFmt_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2031 = { sizeof (U3CdumpU3Ec__AnonStorey30_t1917037014), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2031[2] = 
{
	U3CdumpU3Ec__AnonStorey30_t1917037014::get_offset_of_lineCnt_0(),
	U3CdumpU3Ec__AnonStorey30_t1917037014::get_offset_of_U3CU3Ef__refU2447_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2032 = { sizeof (U3CParseAsyncU3Ec__AnonStorey31_t1758512846), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2032[3] = 
{
	U3CParseAsyncU3Ec__AnonStorey31_t1758512846::get_offset_of_stream_0(),
	U3CParseAsyncU3Ec__AnonStorey31_t1758512846::get_offset_of_unmask_1(),
	U3CParseAsyncU3Ec__AnonStorey31_t1758512846::get_offset_of_completed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2033 = { sizeof (WebSocketState_t790259878)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2033[5] = 
{
	WebSocketState_t790259878::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2034 = { sizeof (WebSocketStream_t4103435597), -1, sizeof(WebSocketStream_t4103435597_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2034[5] = 
{
	0,
	WebSocketStream_t4103435597::get_offset_of__forWrite_1(),
	WebSocketStream_t4103435597::get_offset_of__innerStream_2(),
	WebSocketStream_t4103435597::get_offset_of__secure_3(),
	WebSocketStream_t4103435597_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2035 = { sizeof (U3CreadHandshakeHeadersU3Ec__AnonStorey32_t3526000246), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2035[2] = 
{
	U3CreadHandshakeHeadersU3Ec__AnonStorey32_t3526000246::get_offset_of_buff_0(),
	U3CreadHandshakeHeadersU3Ec__AnonStorey32_t3526000246::get_offset_of_count_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2036 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2036[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2037 = { sizeof (Test_t2603186), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2038 = { sizeof (TestJson_t3212538042)+ sizeof (Il2CppObject), sizeof(TestJson_t3212538042_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2038[1] = 
{
	TestJson_t3212538042::get_offset_of_num_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2039 = { sizeof (U3CPrivateImplementationDetailsU3E_t3053238938), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3053238938_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2039[3] = 
{
	U3CPrivateImplementationDetailsU3E_t3053238938_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
	U3CPrivateImplementationDetailsU3E_t3053238938_StaticFields::get_offset_of_U24U24fieldU2D1_1(),
	U3CPrivateImplementationDetailsU3E_t3053238938_StaticFields::get_offset_of_U24U24fieldU2D2_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2040 = { sizeof (U24ArrayTypeU248_t3988332414)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU248_t3988332414_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2041 = { sizeof (U24ArrayTypeU2416_t3379220354)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2416_t3379220354_marshaled_pinvoke), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
