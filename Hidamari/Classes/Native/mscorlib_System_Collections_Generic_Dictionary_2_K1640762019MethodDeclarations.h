﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>
struct Dictionary_2_t1025825965;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1640762019.h"
#include "AssemblyU2DCSharp_WebSocketSharp_CompressionMethod2226596781.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<WebSocketSharp.CompressionMethod,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1521796523_gshared (Enumerator_t1640762019 * __this, Dictionary_2_t1025825965 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m1521796523(__this, ___host0, method) ((  void (*) (Enumerator_t1640762019 *, Dictionary_2_t1025825965 *, const MethodInfo*))Enumerator__ctor_m1521796523_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1182102112_gshared (Enumerator_t1640762019 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1182102112(__this, method) ((  Il2CppObject * (*) (Enumerator_t1640762019 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1182102112_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m642004522_gshared (Enumerator_t1640762019 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m642004522(__this, method) ((  void (*) (Enumerator_t1640762019 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m642004522_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<WebSocketSharp.CompressionMethod,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m435199245_gshared (Enumerator_t1640762019 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m435199245(__this, method) ((  void (*) (Enumerator_t1640762019 *, const MethodInfo*))Enumerator_Dispose_m435199245_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<WebSocketSharp.CompressionMethod,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m656682586_gshared (Enumerator_t1640762019 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m656682586(__this, method) ((  bool (*) (Enumerator_t1640762019 *, const MethodInfo*))Enumerator_MoveNext_m656682586_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<WebSocketSharp.CompressionMethod,System.Object>::get_Current()
extern "C"  uint8_t Enumerator_get_Current_m49632638_gshared (Enumerator_t1640762019 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m49632638(__this, method) ((  uint8_t (*) (Enumerator_t1640762019 *, const MethodInfo*))Enumerator_get_Current_m49632638_gshared)(__this, method)
