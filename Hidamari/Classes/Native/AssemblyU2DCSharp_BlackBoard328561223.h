﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>
struct Dictionary_2_t200133079;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Vector3>
struct Dictionary_2_t807517640;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1974256870;
// System.Collections.Generic.Dictionary`2<System.String,System.Single>
struct Dictionary_2_t817370046;
// System.Collections.Generic.Dictionary`2<System.String,System.Boolean>
struct Dictionary_2_t1297217088;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BlackBoard
struct  BlackBoard_t328561223  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject BlackBoard::_parent
	GameObject_t3674682005 * ____parent_2;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> BlackBoard::_gameobject
	Dictionary_2_t200133079 * ____gameobject_3;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Vector3> BlackBoard::_vector3
	Dictionary_2_t807517640 * ____vector3_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> BlackBoard::_integer
	Dictionary_2_t1974256870 * ____integer_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.Single> BlackBoard::_fraction
	Dictionary_2_t817370046 * ____fraction_6;
	// System.Collections.Generic.Dictionary`2<System.String,System.Boolean> BlackBoard::_jude
	Dictionary_2_t1297217088 * ____jude_7;

public:
	inline static int32_t get_offset_of__parent_2() { return static_cast<int32_t>(offsetof(BlackBoard_t328561223, ____parent_2)); }
	inline GameObject_t3674682005 * get__parent_2() const { return ____parent_2; }
	inline GameObject_t3674682005 ** get_address_of__parent_2() { return &____parent_2; }
	inline void set__parent_2(GameObject_t3674682005 * value)
	{
		____parent_2 = value;
		Il2CppCodeGenWriteBarrier(&____parent_2, value);
	}

	inline static int32_t get_offset_of__gameobject_3() { return static_cast<int32_t>(offsetof(BlackBoard_t328561223, ____gameobject_3)); }
	inline Dictionary_2_t200133079 * get__gameobject_3() const { return ____gameobject_3; }
	inline Dictionary_2_t200133079 ** get_address_of__gameobject_3() { return &____gameobject_3; }
	inline void set__gameobject_3(Dictionary_2_t200133079 * value)
	{
		____gameobject_3 = value;
		Il2CppCodeGenWriteBarrier(&____gameobject_3, value);
	}

	inline static int32_t get_offset_of__vector3_4() { return static_cast<int32_t>(offsetof(BlackBoard_t328561223, ____vector3_4)); }
	inline Dictionary_2_t807517640 * get__vector3_4() const { return ____vector3_4; }
	inline Dictionary_2_t807517640 ** get_address_of__vector3_4() { return &____vector3_4; }
	inline void set__vector3_4(Dictionary_2_t807517640 * value)
	{
		____vector3_4 = value;
		Il2CppCodeGenWriteBarrier(&____vector3_4, value);
	}

	inline static int32_t get_offset_of__integer_5() { return static_cast<int32_t>(offsetof(BlackBoard_t328561223, ____integer_5)); }
	inline Dictionary_2_t1974256870 * get__integer_5() const { return ____integer_5; }
	inline Dictionary_2_t1974256870 ** get_address_of__integer_5() { return &____integer_5; }
	inline void set__integer_5(Dictionary_2_t1974256870 * value)
	{
		____integer_5 = value;
		Il2CppCodeGenWriteBarrier(&____integer_5, value);
	}

	inline static int32_t get_offset_of__fraction_6() { return static_cast<int32_t>(offsetof(BlackBoard_t328561223, ____fraction_6)); }
	inline Dictionary_2_t817370046 * get__fraction_6() const { return ____fraction_6; }
	inline Dictionary_2_t817370046 ** get_address_of__fraction_6() { return &____fraction_6; }
	inline void set__fraction_6(Dictionary_2_t817370046 * value)
	{
		____fraction_6 = value;
		Il2CppCodeGenWriteBarrier(&____fraction_6, value);
	}

	inline static int32_t get_offset_of__jude_7() { return static_cast<int32_t>(offsetof(BlackBoard_t328561223, ____jude_7)); }
	inline Dictionary_2_t1297217088 * get__jude_7() const { return ____jude_7; }
	inline Dictionary_2_t1297217088 ** get_address_of__jude_7() { return &____jude_7; }
	inline void set__jude_7(Dictionary_2_t1297217088 * value)
	{
		____jude_7 = value;
		Il2CppCodeGenWriteBarrier(&____jude_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
