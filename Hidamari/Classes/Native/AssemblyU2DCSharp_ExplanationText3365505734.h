﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExplanationText
struct  ExplanationText_t3365505734  : public MonoBehaviour_t667441552
{
public:
	// System.Int32 ExplanationText::selectButton
	int32_t ___selectButton_2;
	// UnityEngine.GameObject ExplanationText::_Go
	GameObject_t3674682005 * ____Go_3;
	// UnityEngine.GameObject ExplanationText::_SCimage
	GameObject_t3674682005 * ____SCimage_4;

public:
	inline static int32_t get_offset_of_selectButton_2() { return static_cast<int32_t>(offsetof(ExplanationText_t3365505734, ___selectButton_2)); }
	inline int32_t get_selectButton_2() const { return ___selectButton_2; }
	inline int32_t* get_address_of_selectButton_2() { return &___selectButton_2; }
	inline void set_selectButton_2(int32_t value)
	{
		___selectButton_2 = value;
	}

	inline static int32_t get_offset_of__Go_3() { return static_cast<int32_t>(offsetof(ExplanationText_t3365505734, ____Go_3)); }
	inline GameObject_t3674682005 * get__Go_3() const { return ____Go_3; }
	inline GameObject_t3674682005 ** get_address_of__Go_3() { return &____Go_3; }
	inline void set__Go_3(GameObject_t3674682005 * value)
	{
		____Go_3 = value;
		Il2CppCodeGenWriteBarrier(&____Go_3, value);
	}

	inline static int32_t get_offset_of__SCimage_4() { return static_cast<int32_t>(offsetof(ExplanationText_t3365505734, ____SCimage_4)); }
	inline GameObject_t3674682005 * get__SCimage_4() const { return ____SCimage_4; }
	inline GameObject_t3674682005 ** get_address_of__SCimage_4() { return &____SCimage_4; }
	inline void set__SCimage_4(GameObject_t3674682005 * value)
	{
		____SCimage_4 = value;
		Il2CppCodeGenWriteBarrier(&____SCimage_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
