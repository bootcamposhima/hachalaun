﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.WebSocket/<SendAsync>c__AnonStorey2E
struct U3CSendAsyncU3Ec__AnonStorey2E_t2907890439;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Exception
struct Exception_t3991598821;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Exception3991598821.h"

// System.Void WebSocketSharp.WebSocket/<SendAsync>c__AnonStorey2E::.ctor()
extern "C"  void U3CSendAsyncU3Ec__AnonStorey2E__ctor_m1765214276 (U3CSendAsyncU3Ec__AnonStorey2E_t2907890439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket/<SendAsync>c__AnonStorey2E::<>m__15(System.Byte[])
extern "C"  void U3CSendAsyncU3Ec__AnonStorey2E_U3CU3Em__15_m1497102392 (U3CSendAsyncU3Ec__AnonStorey2E_t2907890439 * __this, ByteU5BU5D_t4260760469* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket/<SendAsync>c__AnonStorey2E::<>m__16(System.Exception)
extern "C"  void U3CSendAsyncU3Ec__AnonStorey2E_U3CU3Em__16_m2671865762 (U3CSendAsyncU3Ec__AnonStorey2E_t2907890439 * __this, Exception_t3991598821 * ___ex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
