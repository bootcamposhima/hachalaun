﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Char>
struct ShimEnumerator_t453472465;
// System.Collections.Generic.Dictionary`2<System.Object,System.Char>
struct Dictionary_2_t737694438;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Char>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m940277905_gshared (ShimEnumerator_t453472465 * __this, Dictionary_2_t737694438 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m940277905(__this, ___host0, method) ((  void (*) (ShimEnumerator_t453472465 *, Dictionary_2_t737694438 *, const MethodInfo*))ShimEnumerator__ctor_m940277905_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Char>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2990912240_gshared (ShimEnumerator_t453472465 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m2990912240(__this, method) ((  bool (*) (ShimEnumerator_t453472465 *, const MethodInfo*))ShimEnumerator_MoveNext_m2990912240_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Char>::get_Entry()
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m3509625892_gshared (ShimEnumerator_t453472465 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m3509625892(__this, method) ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t453472465 *, const MethodInfo*))ShimEnumerator_get_Entry_m3509625892_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Char>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m1660212351_gshared (ShimEnumerator_t453472465 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m1660212351(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t453472465 *, const MethodInfo*))ShimEnumerator_get_Key_m1660212351_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Char>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m3079638865_gshared (ShimEnumerator_t453472465 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m3079638865(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t453472465 *, const MethodInfo*))ShimEnumerator_get_Value_m3079638865_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Char>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m1725173145_gshared (ShimEnumerator_t453472465 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m1725173145(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t453472465 *, const MethodInfo*))ShimEnumerator_get_Current_m1725173145_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Char>::Reset()
extern "C"  void ShimEnumerator_Reset_m2618806627_gshared (ShimEnumerator_t453472465 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m2618806627(__this, method) ((  void (*) (ShimEnumerator_t453472465 *, const MethodInfo*))ShimEnumerator_Reset_m2618806627_gshared)(__this, method)
