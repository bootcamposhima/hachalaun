﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DemoPlay
struct DemoPlay_t921155543;

#include "codegen/il2cpp-codegen.h"

// System.Void DemoPlay::.ctor()
extern "C"  void DemoPlay__ctor_m1325721764 (DemoPlay_t921155543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoPlay::Start()
extern "C"  void DemoPlay_Start_m272859556 (DemoPlay_t921155543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoPlay::Awake()
extern "C"  void DemoPlay_Awake_m1563326983 (DemoPlay_t921155543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoPlay::Update()
extern "C"  void DemoPlay_Update_m4169531113 (DemoPlay_t921155543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoPlay::StopDemoPlay()
extern "C"  void DemoPlay_StopDemoPlay_m2432179385 (DemoPlay_t921155543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoPlay::PlayStart()
extern "C"  void DemoPlay_PlayStart_m2199782224 (DemoPlay_t921155543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DemoPlay::AddPlayerComporment()
extern "C"  void DemoPlay_AddPlayerComporment_m3644717460 (DemoPlay_t921155543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
