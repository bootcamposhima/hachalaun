﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_Direction1041377119.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Bullet
struct  Bullet_t2000900386  : public MonoBehaviour_t667441552
{
public:
	// System.Single Bullet::_spd
	float ____spd_2;
	// System.Int32 Bullet::_power
	int32_t ____power_3;
	// System.Boolean Bullet::isflyweight
	bool ___isflyweight_4;
	// Direction Bullet::_direction
	int32_t ____direction_5;
	// System.Single Bullet::gameSpeed
	float ___gameSpeed_6;
	// System.Single Bullet::_technique
	float ____technique_7;
	// System.Boolean Bullet::isStop
	bool ___isStop_8;

public:
	inline static int32_t get_offset_of__spd_2() { return static_cast<int32_t>(offsetof(Bullet_t2000900386, ____spd_2)); }
	inline float get__spd_2() const { return ____spd_2; }
	inline float* get_address_of__spd_2() { return &____spd_2; }
	inline void set__spd_2(float value)
	{
		____spd_2 = value;
	}

	inline static int32_t get_offset_of__power_3() { return static_cast<int32_t>(offsetof(Bullet_t2000900386, ____power_3)); }
	inline int32_t get__power_3() const { return ____power_3; }
	inline int32_t* get_address_of__power_3() { return &____power_3; }
	inline void set__power_3(int32_t value)
	{
		____power_3 = value;
	}

	inline static int32_t get_offset_of_isflyweight_4() { return static_cast<int32_t>(offsetof(Bullet_t2000900386, ___isflyweight_4)); }
	inline bool get_isflyweight_4() const { return ___isflyweight_4; }
	inline bool* get_address_of_isflyweight_4() { return &___isflyweight_4; }
	inline void set_isflyweight_4(bool value)
	{
		___isflyweight_4 = value;
	}

	inline static int32_t get_offset_of__direction_5() { return static_cast<int32_t>(offsetof(Bullet_t2000900386, ____direction_5)); }
	inline int32_t get__direction_5() const { return ____direction_5; }
	inline int32_t* get_address_of__direction_5() { return &____direction_5; }
	inline void set__direction_5(int32_t value)
	{
		____direction_5 = value;
	}

	inline static int32_t get_offset_of_gameSpeed_6() { return static_cast<int32_t>(offsetof(Bullet_t2000900386, ___gameSpeed_6)); }
	inline float get_gameSpeed_6() const { return ___gameSpeed_6; }
	inline float* get_address_of_gameSpeed_6() { return &___gameSpeed_6; }
	inline void set_gameSpeed_6(float value)
	{
		___gameSpeed_6 = value;
	}

	inline static int32_t get_offset_of__technique_7() { return static_cast<int32_t>(offsetof(Bullet_t2000900386, ____technique_7)); }
	inline float get__technique_7() const { return ____technique_7; }
	inline float* get_address_of__technique_7() { return &____technique_7; }
	inline void set__technique_7(float value)
	{
		____technique_7 = value;
	}

	inline static int32_t get_offset_of_isStop_8() { return static_cast<int32_t>(offsetof(Bullet_t2000900386, ___isStop_8)); }
	inline bool get_isStop_8() const { return ___isStop_8; }
	inline bool* get_address_of_isStop_8() { return &___isStop_8; }
	inline void set_isStop_8(bool value)
	{
		___isStop_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
