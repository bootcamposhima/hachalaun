﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey22
struct U3CReadBytesAsyncU3Ec__AnonStorey22_t3077023315;
// System.IAsyncResult
struct IAsyncResult_t2754620036;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey22::.ctor()
extern "C"  void U3CReadBytesAsyncU3Ec__AnonStorey22__ctor_m2552391592 (U3CReadBytesAsyncU3Ec__AnonStorey22_t3077023315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey22::<>m__2(System.IAsyncResult)
extern "C"  void U3CReadBytesAsyncU3Ec__AnonStorey22_U3CU3Em__2_m221656512 (U3CReadBytesAsyncU3Ec__AnonStorey22_t3077023315 * __this, Il2CppObject * ___ar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
