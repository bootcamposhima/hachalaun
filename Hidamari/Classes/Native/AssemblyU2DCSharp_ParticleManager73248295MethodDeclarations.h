﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ParticleManager
struct ParticleManager_t73248295;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void ParticleManager::.ctor()
extern "C"  void ParticleManager__ctor_m2289964324 (ParticleManager_t73248295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ParticleManager::Start()
extern "C"  void ParticleManager_Start_m1237102116 (ParticleManager_t73248295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ParticleManager::Update()
extern "C"  void ParticleManager_Update_m3996279401 (ParticleManager_t73248295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ParticleManager::Awake()
extern "C"  void ParticleManager_Awake_m2527569543 (ParticleManager_t73248295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject ParticleManager::SoriteParticleStart(UnityEngine.Vector3)
extern "C"  GameObject_t3674682005 * ParticleManager_SoriteParticleStart_m137554282 (ParticleManager_t73248295 * __this, Vector3_t4282066566  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject ParticleManager::AdmissionParticleStart(UnityEngine.Vector3)
extern "C"  GameObject_t3674682005 * ParticleManager_AdmissionParticleStart_m3838078235 (ParticleManager_t73248295 * __this, Vector3_t4282066566  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject ParticleManager::LossParticleStart(UnityEngine.Vector3)
extern "C"  GameObject_t3674682005 * ParticleManager_LossParticleStart_m233175049 (ParticleManager_t73248295 * __this, Vector3_t4282066566  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject ParticleManager::WinParticleStart(UnityEngine.Vector3)
extern "C"  GameObject_t3674682005 * ParticleManager_WinParticleStart_m573086894 (ParticleManager_t73248295 * __this, Vector3_t4282066566  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject ParticleManager::CharChangeParticleStart(UnityEngine.Vector3)
extern "C"  GameObject_t3674682005 * ParticleManager_CharChangeParticleStart_m3782077260 (ParticleManager_t73248295 * __this, Vector3_t4282066566  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject ParticleManager::DamegeParticleStart(UnityEngine.Vector3)
extern "C"  GameObject_t3674682005 * ParticleManager_DamegeParticleStart_m3539078361 (ParticleManager_t73248295 * __this, Vector3_t4282066566  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject ParticleManager::BloodParticleStart(UnityEngine.Vector3)
extern "C"  GameObject_t3674682005 * ParticleManager_BloodParticleStart_m3907307980 (ParticleManager_t73248295 * __this, Vector3_t4282066566  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject ParticleManager::ShotParticleStart(UnityEngine.Vector3)
extern "C"  GameObject_t3674682005 * ParticleManager_ShotParticleStart_m3278722592 (ParticleManager_t73248295 * __this, Vector3_t4282066566  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject ParticleManager::ExplosionsParticleStart(UnityEngine.Vector3)
extern "C"  GameObject_t3674682005 * ParticleManager_ExplosionsParticleStart_m1720486564 (ParticleManager_t73248295 * __this, Vector3_t4282066566  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ParticleManager::ParticleStop(UnityEngine.GameObject)
extern "C"  void ParticleManager_ParticleStop_m3909569472 (ParticleManager_t73248295 * __this, GameObject_t3674682005 * ___par0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
