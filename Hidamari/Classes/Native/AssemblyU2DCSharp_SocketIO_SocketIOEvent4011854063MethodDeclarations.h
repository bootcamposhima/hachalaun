﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SocketIO.SocketIOEvent
struct SocketIOEvent_t4011854063;
// System.String
struct String_t;
// JSONObject
struct JSONObject_t1752376903;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSONObject1752376903.h"

// System.Void SocketIO.SocketIOEvent::.ctor(System.String)
extern "C"  void SocketIOEvent__ctor_m359575091 (SocketIOEvent_t4011854063 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIO.SocketIOEvent::.ctor(System.String,JSONObject)
extern "C"  void SocketIOEvent__ctor_m3718452940 (SocketIOEvent_t4011854063 * __this, String_t* ___name0, JSONObject_t1752376903 * ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SocketIO.SocketIOEvent::get_name()
extern "C"  String_t* SocketIOEvent_get_name_m2477078092 (SocketIOEvent_t4011854063 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIO.SocketIOEvent::set_name(System.String)
extern "C"  void SocketIOEvent_set_name_m3920890821 (SocketIOEvent_t4011854063 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSONObject SocketIO.SocketIOEvent::get_data()
extern "C"  JSONObject_t1752376903 * SocketIOEvent_get_data_m1144384222 (SocketIOEvent_t4011854063 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIO.SocketIOEvent::set_data(JSONObject)
extern "C"  void SocketIOEvent_set_data_m1335247509 (SocketIOEvent_t4011854063 * __this, JSONObject_t1752376903 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SocketIO.SocketIOEvent::ToString()
extern "C"  String_t* SocketIOEvent_ToString_m922670340 (SocketIOEvent_t4011854063 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
