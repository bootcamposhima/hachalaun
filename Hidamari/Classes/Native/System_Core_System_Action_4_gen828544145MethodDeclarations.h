﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_4_gen1730810465MethodDeclarations.h"

// System.Void System.Action`4<System.String,System.String,System.String,System.String>::.ctor(System.Object,System.IntPtr)
#define Action_4__ctor_m2666262812(__this, ___object0, ___method1, method) ((  void (*) (Action_4_t828544145 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_4__ctor_m620347252_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`4<System.String,System.String,System.String,System.String>::Invoke(T1,T2,T3,T4)
#define Action_4_Invoke_m4047132558(__this, ___arg10, ___arg21, ___arg32, ___arg43, method) ((  void (*) (Action_4_t828544145 *, String_t*, String_t*, String_t*, String_t*, const MethodInfo*))Action_4_Invoke_m157716918_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, method)
// System.IAsyncResult System.Action`4<System.String,System.String,System.String,System.String>::BeginInvoke(T1,T2,T3,T4,System.AsyncCallback,System.Object)
#define Action_4_BeginInvoke_m1091731583(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___callback4, ___object5, method) ((  Il2CppObject * (*) (Action_4_t828544145 *, String_t*, String_t*, String_t*, String_t*, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Action_4_BeginInvoke_m1414752311_gshared)(__this, ___arg10, ___arg21, ___arg32, ___arg43, ___callback4, ___object5, method)
// System.Void System.Action`4<System.String,System.String,System.String,System.String>::EndInvoke(System.IAsyncResult)
#define Action_4_EndInvoke_m1438813756(__this, ___result0, method) ((  void (*) (Action_4_t828544145 *, Il2CppObject *, const MethodInfo*))Action_4_EndInvoke_m2556365700_gshared)(__this, ___result0, method)
