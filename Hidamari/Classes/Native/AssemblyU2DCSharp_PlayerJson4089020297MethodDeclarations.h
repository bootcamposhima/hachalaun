﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayerJson
struct PlayerJson_t4089020297;
struct PlayerJson_t4089020297_marshaled_pinvoke;
struct PlayerJson_t4089020297_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PlayerJson4089020297.h"
#include "AssemblyU2DCSharp_MoveJson4254904441.h"
#include "AssemblyU2DCSharp_HitJson2591263483.h"

// System.Void PlayerJson::.ctor(MoveJson)
extern "C"  void PlayerJson__ctor_m1707919577 (PlayerJson_t4089020297 * __this, MoveJson_t4254904441  ___m0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerJson::.ctor(HitJson)
extern "C"  void PlayerJson__ctor_m343967077 (PlayerJson_t4089020297 * __this, HitJson_t2591263483  ___h0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerJson::.ctor(System.Int32)
extern "C"  void PlayerJson__ctor_m2920742275 (PlayerJson_t4089020297 * __this, int32_t ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct PlayerJson_t4089020297;
struct PlayerJson_t4089020297_marshaled_pinvoke;

extern "C" void PlayerJson_t4089020297_marshal_pinvoke(const PlayerJson_t4089020297& unmarshaled, PlayerJson_t4089020297_marshaled_pinvoke& marshaled);
extern "C" void PlayerJson_t4089020297_marshal_pinvoke_back(const PlayerJson_t4089020297_marshaled_pinvoke& marshaled, PlayerJson_t4089020297& unmarshaled);
extern "C" void PlayerJson_t4089020297_marshal_pinvoke_cleanup(PlayerJson_t4089020297_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct PlayerJson_t4089020297;
struct PlayerJson_t4089020297_marshaled_com;

extern "C" void PlayerJson_t4089020297_marshal_com(const PlayerJson_t4089020297& unmarshaled, PlayerJson_t4089020297_marshaled_com& marshaled);
extern "C" void PlayerJson_t4089020297_marshal_com_back(const PlayerJson_t4089020297_marshaled_com& marshaled, PlayerJson_t4089020297& unmarshaled);
extern "C" void PlayerJson_t4089020297_marshal_com_cleanup(PlayerJson_t4089020297_marshaled_com& marshaled);
