﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SocketIO.SocketIOException
struct SocketIOException_t1682042276;
// System.String
struct String_t;
// System.Exception
struct Exception_t3991598821;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Exception3991598821.h"

// System.Void SocketIO.SocketIOException::.ctor()
extern "C"  void SocketIOException__ctor_m94167066 (SocketIOException_t1682042276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIO.SocketIOException::.ctor(System.String)
extern "C"  void SocketIOException__ctor_m3476502824 (SocketIOException_t1682042276 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIO.SocketIOException::.ctor(System.String,System.Exception)
extern "C"  void SocketIOException__ctor_m3934322958 (SocketIOException_t1682042276 * __this, String_t* ___message0, Exception_t3991598821 * ___innerException1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
