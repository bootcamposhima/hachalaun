﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.HandshakeRequest
struct HandshakeRequest_t1037477780;
// System.Version
struct Version_t763695022;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t2791941106;
// System.String
struct String_t;
// WebSocketSharp.Net.AuthenticationResponse
struct AuthenticationResponse_t2112712571;
// WebSocketSharp.Net.CookieCollection
struct CookieCollection_t1136277956;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Version763695022.h"
#include "System_System_Collections_Specialized_NameValueCol2791941106.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_CookieCollect1136277956.h"

// System.Void WebSocketSharp.HandshakeRequest::.ctor(System.Version,System.Collections.Specialized.NameValueCollection)
extern "C"  void HandshakeRequest__ctor_m205823408 (HandshakeRequest_t1037477780 * __this, Version_t763695022 * ___version0, NameValueCollection_t2791941106 * ___headers1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.HandshakeRequest::.ctor(System.String)
extern "C"  void HandshakeRequest__ctor_m4024060442 (HandshakeRequest_t1037477780 * __this, String_t* ___pathAndQuery0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.Net.AuthenticationResponse WebSocketSharp.HandshakeRequest::get_AuthResponse()
extern "C"  AuthenticationResponse_t2112712571 * HandshakeRequest_get_AuthResponse_m2705114864 (HandshakeRequest_t1037477780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.Net.CookieCollection WebSocketSharp.HandshakeRequest::get_Cookies()
extern "C"  CookieCollection_t1136277956 * HandshakeRequest_get_Cookies_m1812597953 (HandshakeRequest_t1037477780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.HandshakeRequest::get_HttpMethod()
extern "C"  String_t* HandshakeRequest_get_HttpMethod_m3863883883 (HandshakeRequest_t1037477780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.HandshakeRequest::get_IsWebSocketRequest()
extern "C"  bool HandshakeRequest_get_IsWebSocketRequest_m2145881123 (HandshakeRequest_t1037477780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.HandshakeRequest::get_RequestUri()
extern "C"  String_t* HandshakeRequest_get_RequestUri_m586885183 (HandshakeRequest_t1037477780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.HandshakeRequest WebSocketSharp.HandshakeRequest::Parse(System.String[])
extern "C"  HandshakeRequest_t1037477780 * HandshakeRequest_Parse_m2674338295 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t4054002952* ___headerParts0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.HandshakeRequest::SetCookies(WebSocketSharp.Net.CookieCollection)
extern "C"  void HandshakeRequest_SetCookies_m1719690283 (HandshakeRequest_t1037477780 * __this, CookieCollection_t1136277956 * ___cookies0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.HandshakeRequest::ToString()
extern "C"  String_t* HandshakeRequest_ToString_m1818022469 (HandshakeRequest_t1037477780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
