﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_Enumerat3401177016MethodDeclarations.h"

// System.Void System.Collections.Generic.Queue`1/Enumerator<WebSocketSharp.MessageEventArgs>::.ctor(System.Collections.Generic.Queue`1<T>)
#define Enumerator__ctor_m2745249250(__this, ___q0, method) ((  void (*) (Enumerator_t3562273681 *, Queue_1_t2273188169 *, const MethodInfo*))Enumerator__ctor_m3846579967_gshared)(__this, ___q0, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<WebSocketSharp.MessageEventArgs>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1412237597(__this, method) ((  void (*) (Enumerator_t3562273681 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2396412922_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1/Enumerator<WebSocketSharp.MessageEventArgs>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m27017939(__this, method) ((  Il2CppObject * (*) (Enumerator_t3562273681 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m280731440_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<WebSocketSharp.MessageEventArgs>::Dispose()
#define Enumerator_Dispose_m459339066(__this, method) ((  void (*) (Enumerator_t3562273681 *, const MethodInfo*))Enumerator_Dispose_m3069945149_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Queue`1/Enumerator<WebSocketSharp.MessageEventArgs>::MoveNext()
#define Enumerator_MoveNext_m502125793(__this, method) ((  bool (*) (Enumerator_t3562273681 *, const MethodInfo*))Enumerator_MoveNext_m262168254_gshared)(__this, method)
// T System.Collections.Generic.Queue`1/Enumerator<WebSocketSharp.MessageEventArgs>::get_Current()
#define Enumerator_get_Current_m1614509452(__this, method) ((  MessageEventArgs_t36945740 * (*) (Enumerator_t3562273681 *, const MethodInfo*))Enumerator_get_Current_m3381813839_gshared)(__this, method)
