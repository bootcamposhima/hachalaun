﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// WebSocketSharp.Net.AuthenticationChallenge
struct AuthenticationChallenge_t1782907061;
// System.Net.Security.RemoteCertificateValidationCallback
struct RemoteCertificateValidationCallback_t1894914657;
// System.Action
struct Action_t3771233898;
// WebSocketSharp.Net.WebSockets.WebSocketContext
struct WebSocketContext_t763725542;
// WebSocketSharp.Net.CookieCollection
struct CookieCollection_t1136277956;
// WebSocketSharp.Net.NetworkCredential
struct NetworkCredential_t1204099087;
// System.Threading.AutoResetEvent
struct AutoResetEvent_t874642578;
// System.Object
struct Il2CppObject;
// System.Func`2<WebSocketSharp.Net.WebSockets.WebSocketContext,System.String>
struct Func_2_t636757868;
// WebSocketSharp.Logger
struct Logger_t3695440972;
// System.Collections.Generic.Queue`1<WebSocketSharp.MessageEventArgs>
struct Queue_1_t2273188169;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t2791941106;
// System.String[]
struct StringU5BU5D_t4054002952;
// WebSocketSharp.WebSocketStream
struct WebSocketStream_t4103435597;
// System.Net.Sockets.TcpClient
struct TcpClient_t838416830;
// System.Uri
struct Uri_t1116831938;
// System.EventHandler`1<WebSocketSharp.CloseEventArgs>
struct EventHandler_1_t2615270477;
// System.EventHandler`1<WebSocketSharp.ErrorEventArgs>
struct EventHandler_1_t569145917;
// System.EventHandler`1<WebSocketSharp.MessageEventArgs>
struct EventHandler_1_t181896286;
// System.EventHandler
struct EventHandler_t2463957060;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_WebSocketSharp_CompressionMethod2226596781.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocketState790259878.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocket
struct  WebSocket_t1342580397  : public Il2CppObject
{
public:
	// WebSocketSharp.Net.AuthenticationChallenge WebSocketSharp.WebSocket::_authChallenge
	AuthenticationChallenge_t1782907061 * ____authChallenge_3;
	// System.String WebSocketSharp.WebSocket::_base64Key
	String_t* ____base64Key_4;
	// System.Net.Security.RemoteCertificateValidationCallback WebSocketSharp.WebSocket::_certValidationCallback
	RemoteCertificateValidationCallback_t1894914657 * ____certValidationCallback_5;
	// System.Boolean WebSocketSharp.WebSocket::_client
	bool ____client_6;
	// System.Action WebSocketSharp.WebSocket::_closeContext
	Action_t3771233898 * ____closeContext_7;
	// WebSocketSharp.CompressionMethod WebSocketSharp.WebSocket::_compression
	uint8_t ____compression_8;
	// WebSocketSharp.Net.WebSockets.WebSocketContext WebSocketSharp.WebSocket::_context
	WebSocketContext_t763725542 * ____context_9;
	// WebSocketSharp.Net.CookieCollection WebSocketSharp.WebSocket::_cookies
	CookieCollection_t1136277956 * ____cookies_10;
	// WebSocketSharp.Net.NetworkCredential WebSocketSharp.WebSocket::_credentials
	NetworkCredential_t1204099087 * ____credentials_11;
	// System.String WebSocketSharp.WebSocket::_extensions
	String_t* ____extensions_12;
	// System.Threading.AutoResetEvent WebSocketSharp.WebSocket::_exitReceiving
	AutoResetEvent_t874642578 * ____exitReceiving_13;
	// System.Object WebSocketSharp.WebSocket::_forConn
	Il2CppObject * ____forConn_14;
	// System.Object WebSocketSharp.WebSocket::_forEvent
	Il2CppObject * ____forEvent_15;
	// System.Object WebSocketSharp.WebSocket::_forMessageEventQueue
	Il2CppObject * ____forMessageEventQueue_16;
	// System.Object WebSocketSharp.WebSocket::_forSend
	Il2CppObject * ____forSend_17;
	// System.Func`2<WebSocketSharp.Net.WebSockets.WebSocketContext,System.String> WebSocketSharp.WebSocket::_handshakeRequestChecker
	Func_2_t636757868 * ____handshakeRequestChecker_18;
	// WebSocketSharp.Logger modreq(System.Runtime.CompilerServices.IsVolatile) WebSocketSharp.WebSocket::_logger
	Logger_t3695440972 * ____logger_19;
	// System.Collections.Generic.Queue`1<WebSocketSharp.MessageEventArgs> WebSocketSharp.WebSocket::_messageEventQueue
	Queue_1_t2273188169 * ____messageEventQueue_20;
	// System.UInt32 WebSocketSharp.WebSocket::_nonceCount
	uint32_t ____nonceCount_21;
	// System.String WebSocketSharp.WebSocket::_origin
	String_t* ____origin_22;
	// System.Collections.Specialized.NameValueCollection WebSocketSharp.WebSocket::_customHeaders
	NameValueCollection_t2791941106 * ____customHeaders_23;
	// System.Boolean WebSocketSharp.WebSocket::_preAuth
	bool ____preAuth_24;
	// System.String WebSocketSharp.WebSocket::_protocol
	String_t* ____protocol_25;
	// System.String[] WebSocketSharp.WebSocket::_protocols
	StringU5BU5D_t4054002952* ____protocols_26;
	// WebSocketSharp.WebSocketState modreq(System.Runtime.CompilerServices.IsVolatile) WebSocketSharp.WebSocket::_readyState
	uint16_t ____readyState_27;
	// System.Threading.AutoResetEvent WebSocketSharp.WebSocket::_receivePong
	AutoResetEvent_t874642578 * ____receivePong_28;
	// System.Boolean WebSocketSharp.WebSocket::_secure
	bool ____secure_29;
	// WebSocketSharp.WebSocketStream WebSocketSharp.WebSocket::_stream
	WebSocketStream_t4103435597 * ____stream_30;
	// System.Net.Sockets.TcpClient WebSocketSharp.WebSocket::_tcpClient
	TcpClient_t838416830 * ____tcpClient_31;
	// System.Uri WebSocketSharp.WebSocket::_uri
	Uri_t1116831938 * ____uri_32;
	// System.EventHandler`1<WebSocketSharp.CloseEventArgs> WebSocketSharp.WebSocket::OnClose
	EventHandler_1_t2615270477 * ___OnClose_33;
	// System.EventHandler`1<WebSocketSharp.ErrorEventArgs> WebSocketSharp.WebSocket::OnError
	EventHandler_1_t569145917 * ___OnError_34;
	// System.EventHandler`1<WebSocketSharp.MessageEventArgs> WebSocketSharp.WebSocket::OnMessage
	EventHandler_1_t181896286 * ___OnMessage_35;
	// System.EventHandler WebSocketSharp.WebSocket::OnOpen
	EventHandler_t2463957060 * ___OnOpen_36;

public:
	inline static int32_t get_offset_of__authChallenge_3() { return static_cast<int32_t>(offsetof(WebSocket_t1342580397, ____authChallenge_3)); }
	inline AuthenticationChallenge_t1782907061 * get__authChallenge_3() const { return ____authChallenge_3; }
	inline AuthenticationChallenge_t1782907061 ** get_address_of__authChallenge_3() { return &____authChallenge_3; }
	inline void set__authChallenge_3(AuthenticationChallenge_t1782907061 * value)
	{
		____authChallenge_3 = value;
		Il2CppCodeGenWriteBarrier(&____authChallenge_3, value);
	}

	inline static int32_t get_offset_of__base64Key_4() { return static_cast<int32_t>(offsetof(WebSocket_t1342580397, ____base64Key_4)); }
	inline String_t* get__base64Key_4() const { return ____base64Key_4; }
	inline String_t** get_address_of__base64Key_4() { return &____base64Key_4; }
	inline void set__base64Key_4(String_t* value)
	{
		____base64Key_4 = value;
		Il2CppCodeGenWriteBarrier(&____base64Key_4, value);
	}

	inline static int32_t get_offset_of__certValidationCallback_5() { return static_cast<int32_t>(offsetof(WebSocket_t1342580397, ____certValidationCallback_5)); }
	inline RemoteCertificateValidationCallback_t1894914657 * get__certValidationCallback_5() const { return ____certValidationCallback_5; }
	inline RemoteCertificateValidationCallback_t1894914657 ** get_address_of__certValidationCallback_5() { return &____certValidationCallback_5; }
	inline void set__certValidationCallback_5(RemoteCertificateValidationCallback_t1894914657 * value)
	{
		____certValidationCallback_5 = value;
		Il2CppCodeGenWriteBarrier(&____certValidationCallback_5, value);
	}

	inline static int32_t get_offset_of__client_6() { return static_cast<int32_t>(offsetof(WebSocket_t1342580397, ____client_6)); }
	inline bool get__client_6() const { return ____client_6; }
	inline bool* get_address_of__client_6() { return &____client_6; }
	inline void set__client_6(bool value)
	{
		____client_6 = value;
	}

	inline static int32_t get_offset_of__closeContext_7() { return static_cast<int32_t>(offsetof(WebSocket_t1342580397, ____closeContext_7)); }
	inline Action_t3771233898 * get__closeContext_7() const { return ____closeContext_7; }
	inline Action_t3771233898 ** get_address_of__closeContext_7() { return &____closeContext_7; }
	inline void set__closeContext_7(Action_t3771233898 * value)
	{
		____closeContext_7 = value;
		Il2CppCodeGenWriteBarrier(&____closeContext_7, value);
	}

	inline static int32_t get_offset_of__compression_8() { return static_cast<int32_t>(offsetof(WebSocket_t1342580397, ____compression_8)); }
	inline uint8_t get__compression_8() const { return ____compression_8; }
	inline uint8_t* get_address_of__compression_8() { return &____compression_8; }
	inline void set__compression_8(uint8_t value)
	{
		____compression_8 = value;
	}

	inline static int32_t get_offset_of__context_9() { return static_cast<int32_t>(offsetof(WebSocket_t1342580397, ____context_9)); }
	inline WebSocketContext_t763725542 * get__context_9() const { return ____context_9; }
	inline WebSocketContext_t763725542 ** get_address_of__context_9() { return &____context_9; }
	inline void set__context_9(WebSocketContext_t763725542 * value)
	{
		____context_9 = value;
		Il2CppCodeGenWriteBarrier(&____context_9, value);
	}

	inline static int32_t get_offset_of__cookies_10() { return static_cast<int32_t>(offsetof(WebSocket_t1342580397, ____cookies_10)); }
	inline CookieCollection_t1136277956 * get__cookies_10() const { return ____cookies_10; }
	inline CookieCollection_t1136277956 ** get_address_of__cookies_10() { return &____cookies_10; }
	inline void set__cookies_10(CookieCollection_t1136277956 * value)
	{
		____cookies_10 = value;
		Il2CppCodeGenWriteBarrier(&____cookies_10, value);
	}

	inline static int32_t get_offset_of__credentials_11() { return static_cast<int32_t>(offsetof(WebSocket_t1342580397, ____credentials_11)); }
	inline NetworkCredential_t1204099087 * get__credentials_11() const { return ____credentials_11; }
	inline NetworkCredential_t1204099087 ** get_address_of__credentials_11() { return &____credentials_11; }
	inline void set__credentials_11(NetworkCredential_t1204099087 * value)
	{
		____credentials_11 = value;
		Il2CppCodeGenWriteBarrier(&____credentials_11, value);
	}

	inline static int32_t get_offset_of__extensions_12() { return static_cast<int32_t>(offsetof(WebSocket_t1342580397, ____extensions_12)); }
	inline String_t* get__extensions_12() const { return ____extensions_12; }
	inline String_t** get_address_of__extensions_12() { return &____extensions_12; }
	inline void set__extensions_12(String_t* value)
	{
		____extensions_12 = value;
		Il2CppCodeGenWriteBarrier(&____extensions_12, value);
	}

	inline static int32_t get_offset_of__exitReceiving_13() { return static_cast<int32_t>(offsetof(WebSocket_t1342580397, ____exitReceiving_13)); }
	inline AutoResetEvent_t874642578 * get__exitReceiving_13() const { return ____exitReceiving_13; }
	inline AutoResetEvent_t874642578 ** get_address_of__exitReceiving_13() { return &____exitReceiving_13; }
	inline void set__exitReceiving_13(AutoResetEvent_t874642578 * value)
	{
		____exitReceiving_13 = value;
		Il2CppCodeGenWriteBarrier(&____exitReceiving_13, value);
	}

	inline static int32_t get_offset_of__forConn_14() { return static_cast<int32_t>(offsetof(WebSocket_t1342580397, ____forConn_14)); }
	inline Il2CppObject * get__forConn_14() const { return ____forConn_14; }
	inline Il2CppObject ** get_address_of__forConn_14() { return &____forConn_14; }
	inline void set__forConn_14(Il2CppObject * value)
	{
		____forConn_14 = value;
		Il2CppCodeGenWriteBarrier(&____forConn_14, value);
	}

	inline static int32_t get_offset_of__forEvent_15() { return static_cast<int32_t>(offsetof(WebSocket_t1342580397, ____forEvent_15)); }
	inline Il2CppObject * get__forEvent_15() const { return ____forEvent_15; }
	inline Il2CppObject ** get_address_of__forEvent_15() { return &____forEvent_15; }
	inline void set__forEvent_15(Il2CppObject * value)
	{
		____forEvent_15 = value;
		Il2CppCodeGenWriteBarrier(&____forEvent_15, value);
	}

	inline static int32_t get_offset_of__forMessageEventQueue_16() { return static_cast<int32_t>(offsetof(WebSocket_t1342580397, ____forMessageEventQueue_16)); }
	inline Il2CppObject * get__forMessageEventQueue_16() const { return ____forMessageEventQueue_16; }
	inline Il2CppObject ** get_address_of__forMessageEventQueue_16() { return &____forMessageEventQueue_16; }
	inline void set__forMessageEventQueue_16(Il2CppObject * value)
	{
		____forMessageEventQueue_16 = value;
		Il2CppCodeGenWriteBarrier(&____forMessageEventQueue_16, value);
	}

	inline static int32_t get_offset_of__forSend_17() { return static_cast<int32_t>(offsetof(WebSocket_t1342580397, ____forSend_17)); }
	inline Il2CppObject * get__forSend_17() const { return ____forSend_17; }
	inline Il2CppObject ** get_address_of__forSend_17() { return &____forSend_17; }
	inline void set__forSend_17(Il2CppObject * value)
	{
		____forSend_17 = value;
		Il2CppCodeGenWriteBarrier(&____forSend_17, value);
	}

	inline static int32_t get_offset_of__handshakeRequestChecker_18() { return static_cast<int32_t>(offsetof(WebSocket_t1342580397, ____handshakeRequestChecker_18)); }
	inline Func_2_t636757868 * get__handshakeRequestChecker_18() const { return ____handshakeRequestChecker_18; }
	inline Func_2_t636757868 ** get_address_of__handshakeRequestChecker_18() { return &____handshakeRequestChecker_18; }
	inline void set__handshakeRequestChecker_18(Func_2_t636757868 * value)
	{
		____handshakeRequestChecker_18 = value;
		Il2CppCodeGenWriteBarrier(&____handshakeRequestChecker_18, value);
	}

	inline static int32_t get_offset_of__logger_19() { return static_cast<int32_t>(offsetof(WebSocket_t1342580397, ____logger_19)); }
	inline Logger_t3695440972 * get__logger_19() const { return ____logger_19; }
	inline Logger_t3695440972 ** get_address_of__logger_19() { return &____logger_19; }
	inline void set__logger_19(Logger_t3695440972 * value)
	{
		____logger_19 = value;
		Il2CppCodeGenWriteBarrier(&____logger_19, value);
	}

	inline static int32_t get_offset_of__messageEventQueue_20() { return static_cast<int32_t>(offsetof(WebSocket_t1342580397, ____messageEventQueue_20)); }
	inline Queue_1_t2273188169 * get__messageEventQueue_20() const { return ____messageEventQueue_20; }
	inline Queue_1_t2273188169 ** get_address_of__messageEventQueue_20() { return &____messageEventQueue_20; }
	inline void set__messageEventQueue_20(Queue_1_t2273188169 * value)
	{
		____messageEventQueue_20 = value;
		Il2CppCodeGenWriteBarrier(&____messageEventQueue_20, value);
	}

	inline static int32_t get_offset_of__nonceCount_21() { return static_cast<int32_t>(offsetof(WebSocket_t1342580397, ____nonceCount_21)); }
	inline uint32_t get__nonceCount_21() const { return ____nonceCount_21; }
	inline uint32_t* get_address_of__nonceCount_21() { return &____nonceCount_21; }
	inline void set__nonceCount_21(uint32_t value)
	{
		____nonceCount_21 = value;
	}

	inline static int32_t get_offset_of__origin_22() { return static_cast<int32_t>(offsetof(WebSocket_t1342580397, ____origin_22)); }
	inline String_t* get__origin_22() const { return ____origin_22; }
	inline String_t** get_address_of__origin_22() { return &____origin_22; }
	inline void set__origin_22(String_t* value)
	{
		____origin_22 = value;
		Il2CppCodeGenWriteBarrier(&____origin_22, value);
	}

	inline static int32_t get_offset_of__customHeaders_23() { return static_cast<int32_t>(offsetof(WebSocket_t1342580397, ____customHeaders_23)); }
	inline NameValueCollection_t2791941106 * get__customHeaders_23() const { return ____customHeaders_23; }
	inline NameValueCollection_t2791941106 ** get_address_of__customHeaders_23() { return &____customHeaders_23; }
	inline void set__customHeaders_23(NameValueCollection_t2791941106 * value)
	{
		____customHeaders_23 = value;
		Il2CppCodeGenWriteBarrier(&____customHeaders_23, value);
	}

	inline static int32_t get_offset_of__preAuth_24() { return static_cast<int32_t>(offsetof(WebSocket_t1342580397, ____preAuth_24)); }
	inline bool get__preAuth_24() const { return ____preAuth_24; }
	inline bool* get_address_of__preAuth_24() { return &____preAuth_24; }
	inline void set__preAuth_24(bool value)
	{
		____preAuth_24 = value;
	}

	inline static int32_t get_offset_of__protocol_25() { return static_cast<int32_t>(offsetof(WebSocket_t1342580397, ____protocol_25)); }
	inline String_t* get__protocol_25() const { return ____protocol_25; }
	inline String_t** get_address_of__protocol_25() { return &____protocol_25; }
	inline void set__protocol_25(String_t* value)
	{
		____protocol_25 = value;
		Il2CppCodeGenWriteBarrier(&____protocol_25, value);
	}

	inline static int32_t get_offset_of__protocols_26() { return static_cast<int32_t>(offsetof(WebSocket_t1342580397, ____protocols_26)); }
	inline StringU5BU5D_t4054002952* get__protocols_26() const { return ____protocols_26; }
	inline StringU5BU5D_t4054002952** get_address_of__protocols_26() { return &____protocols_26; }
	inline void set__protocols_26(StringU5BU5D_t4054002952* value)
	{
		____protocols_26 = value;
		Il2CppCodeGenWriteBarrier(&____protocols_26, value);
	}

	inline static int32_t get_offset_of__readyState_27() { return static_cast<int32_t>(offsetof(WebSocket_t1342580397, ____readyState_27)); }
	inline uint16_t get__readyState_27() const { return ____readyState_27; }
	inline uint16_t* get_address_of__readyState_27() { return &____readyState_27; }
	inline void set__readyState_27(uint16_t value)
	{
		____readyState_27 = value;
	}

	inline static int32_t get_offset_of__receivePong_28() { return static_cast<int32_t>(offsetof(WebSocket_t1342580397, ____receivePong_28)); }
	inline AutoResetEvent_t874642578 * get__receivePong_28() const { return ____receivePong_28; }
	inline AutoResetEvent_t874642578 ** get_address_of__receivePong_28() { return &____receivePong_28; }
	inline void set__receivePong_28(AutoResetEvent_t874642578 * value)
	{
		____receivePong_28 = value;
		Il2CppCodeGenWriteBarrier(&____receivePong_28, value);
	}

	inline static int32_t get_offset_of__secure_29() { return static_cast<int32_t>(offsetof(WebSocket_t1342580397, ____secure_29)); }
	inline bool get__secure_29() const { return ____secure_29; }
	inline bool* get_address_of__secure_29() { return &____secure_29; }
	inline void set__secure_29(bool value)
	{
		____secure_29 = value;
	}

	inline static int32_t get_offset_of__stream_30() { return static_cast<int32_t>(offsetof(WebSocket_t1342580397, ____stream_30)); }
	inline WebSocketStream_t4103435597 * get__stream_30() const { return ____stream_30; }
	inline WebSocketStream_t4103435597 ** get_address_of__stream_30() { return &____stream_30; }
	inline void set__stream_30(WebSocketStream_t4103435597 * value)
	{
		____stream_30 = value;
		Il2CppCodeGenWriteBarrier(&____stream_30, value);
	}

	inline static int32_t get_offset_of__tcpClient_31() { return static_cast<int32_t>(offsetof(WebSocket_t1342580397, ____tcpClient_31)); }
	inline TcpClient_t838416830 * get__tcpClient_31() const { return ____tcpClient_31; }
	inline TcpClient_t838416830 ** get_address_of__tcpClient_31() { return &____tcpClient_31; }
	inline void set__tcpClient_31(TcpClient_t838416830 * value)
	{
		____tcpClient_31 = value;
		Il2CppCodeGenWriteBarrier(&____tcpClient_31, value);
	}

	inline static int32_t get_offset_of__uri_32() { return static_cast<int32_t>(offsetof(WebSocket_t1342580397, ____uri_32)); }
	inline Uri_t1116831938 * get__uri_32() const { return ____uri_32; }
	inline Uri_t1116831938 ** get_address_of__uri_32() { return &____uri_32; }
	inline void set__uri_32(Uri_t1116831938 * value)
	{
		____uri_32 = value;
		Il2CppCodeGenWriteBarrier(&____uri_32, value);
	}

	inline static int32_t get_offset_of_OnClose_33() { return static_cast<int32_t>(offsetof(WebSocket_t1342580397, ___OnClose_33)); }
	inline EventHandler_1_t2615270477 * get_OnClose_33() const { return ___OnClose_33; }
	inline EventHandler_1_t2615270477 ** get_address_of_OnClose_33() { return &___OnClose_33; }
	inline void set_OnClose_33(EventHandler_1_t2615270477 * value)
	{
		___OnClose_33 = value;
		Il2CppCodeGenWriteBarrier(&___OnClose_33, value);
	}

	inline static int32_t get_offset_of_OnError_34() { return static_cast<int32_t>(offsetof(WebSocket_t1342580397, ___OnError_34)); }
	inline EventHandler_1_t569145917 * get_OnError_34() const { return ___OnError_34; }
	inline EventHandler_1_t569145917 ** get_address_of_OnError_34() { return &___OnError_34; }
	inline void set_OnError_34(EventHandler_1_t569145917 * value)
	{
		___OnError_34 = value;
		Il2CppCodeGenWriteBarrier(&___OnError_34, value);
	}

	inline static int32_t get_offset_of_OnMessage_35() { return static_cast<int32_t>(offsetof(WebSocket_t1342580397, ___OnMessage_35)); }
	inline EventHandler_1_t181896286 * get_OnMessage_35() const { return ___OnMessage_35; }
	inline EventHandler_1_t181896286 ** get_address_of_OnMessage_35() { return &___OnMessage_35; }
	inline void set_OnMessage_35(EventHandler_1_t181896286 * value)
	{
		___OnMessage_35 = value;
		Il2CppCodeGenWriteBarrier(&___OnMessage_35, value);
	}

	inline static int32_t get_offset_of_OnOpen_36() { return static_cast<int32_t>(offsetof(WebSocket_t1342580397, ___OnOpen_36)); }
	inline EventHandler_t2463957060 * get_OnOpen_36() const { return ___OnOpen_36; }
	inline EventHandler_t2463957060 ** get_address_of_OnOpen_36() { return &___OnOpen_36; }
	inline void set_OnOpen_36(EventHandler_t2463957060 * value)
	{
		___OnOpen_36 = value;
		Il2CppCodeGenWriteBarrier(&___OnOpen_36, value);
	}
};

struct WebSocket_t1342580397_StaticFields
{
public:
	// System.Func`2<WebSocketSharp.Net.WebSockets.WebSocketContext,System.String> WebSocketSharp.WebSocket::<>f__am$cache22
	Func_2_t636757868 * ___U3CU3Ef__amU24cache22_37;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache22_37() { return static_cast<int32_t>(offsetof(WebSocket_t1342580397_StaticFields, ___U3CU3Ef__amU24cache22_37)); }
	inline Func_2_t636757868 * get_U3CU3Ef__amU24cache22_37() const { return ___U3CU3Ef__amU24cache22_37; }
	inline Func_2_t636757868 ** get_address_of_U3CU3Ef__amU24cache22_37() { return &___U3CU3Ef__amU24cache22_37; }
	inline void set_U3CU3Ef__amU24cache22_37(Func_2_t636757868 * value)
	{
		___U3CU3Ef__amU24cache22_37 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache22_37, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
