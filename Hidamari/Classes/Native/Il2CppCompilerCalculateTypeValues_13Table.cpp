﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Cursor2745727898.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter3570684786.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter1820874799.h"
#include "UnityEngine_UnityEngine_Renderer3076687687.h"
#include "UnityEngine_UnityEngine_InternalDrawTextureArgumen4047764288.h"
#include "UnityEngine_UnityEngine_Graphics3672240399.h"
#include "UnityEngine_UnityEngine_Screen3187157168.h"
#include "UnityEngine_UnityEngine_RectOffset3056157787.h"
#include "UnityEngine_UnityEngine_GUIElement3775428101.h"
#include "UnityEngine_UnityEngine_GUILayer2983897946.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"
#include "UnityEngine_UnityEngine_CullingGroupEvent2820176033.h"
#include "UnityEngine_UnityEngine_CullingGroup1868862003.h"
#include "UnityEngine_UnityEngine_CullingGroup_StateChanged2578300556.h"
#include "UnityEngine_UnityEngine_Gradient3661184436.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboard_Interna705488572.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboardType2604324130.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboard1858258760.h"
#include "UnityEngine_UnityEngine_Gizmos2849394813.h"
#include "UnityEngine_UnityEngine_LayerMask3236759763.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Color32598853688.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"
#include "UnityEngine_UnityEngine_Ray3134616544.h"
#include "UnityEngine_UnityEngine_Plane4206452690.h"
#include "UnityEngine_UnityEngineInternal_MathfInternal4096243933.h"
#include "UnityEngine_UnityEngine_Mathf4203372500.h"
#include "UnityEngine_UnityEngine_Keyframe4079056114.h"
#include "UnityEngine_UnityEngine_AnimationCurve3667593487.h"
#include "UnityEngine_UnityEngine_Mesh4241756145.h"
#include "UnityEngine_UnityEngine_Mesh_InternalShaderChannel2277052758.h"
#include "UnityEngine_UnityEngine_Mesh_InternalVertexChannel2290942609.h"
#include "UnityEngine_UnityEngine_NetworkPlayer3231273765.h"
#include "UnityEngine_UnityEngine_NetworkViewID3400394436.h"
#include "UnityEngine_UnityEngine_NetworkView3656680617.h"
#include "UnityEngine_UnityEngine_BitStream239125475.h"
#include "UnityEngine_UnityEngine_RPC3134615963.h"
#include "UnityEngine_UnityEngine_HostData3270478838.h"
#include "UnityEngine_UnityEngine_NetworkMessageInfo3807997963.h"
#include "UnityEngine_UnityEngine_DrivenTransformProperties624110065.h"
#include "UnityEngine_UnityEngine_DrivenRectTransformTracker4185719096.h"
#include "UnityEngine_UnityEngine_RectTransform972643934.h"
#include "UnityEngine_UnityEngine_RectTransform_Edge1676794651.h"
#include "UnityEngine_UnityEngine_RectTransform_Axis1676694783.h"
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDriven779639188.h"
#include "UnityEngine_UnityEngine_ResourceRequest3731857623.h"
#include "UnityEngine_UnityEngine_Resources2918352667.h"
#include "UnityEngine_UnityEngine_SerializePrivateVariables2835496234.h"
#include "UnityEngine_UnityEngine_SerializeField3754825534.h"
#include "UnityEngine_UnityEngine_Shader3191267369.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"
#include "UnityEngine_UnityEngine_SortingLayer3376264497.h"
#include "UnityEngine_UnityEngine_Sprite3199167241.h"
#include "UnityEngine_UnityEngine_SpriteRenderer2548470764.h"
#include "UnityEngine_UnityEngine_Sprites_DataUtility1448936472.h"
#include "UnityEngine_UnityEngine_Hash128346790303.h"
#include "UnityEngine_UnityEngine_WWW3134621005.h"
#include "UnityEngine_UnityEngine_WWWForm461342257.h"
#include "UnityEngine_UnityEngine_WWWTranscoder609724394.h"
#include "UnityEngine_UnityEngine_UnityString3369712284.h"
#include "UnityEngine_UnityEngine_AsyncOperation3699081103.h"
#include "UnityEngine_UnityEngine_Application2856536070.h"
#include "UnityEngine_UnityEngine_Application_LogCallback2984951347.h"
#include "UnityEngine_UnityEngine_Behaviour200106419.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "UnityEngine_UnityEngine_Camera_CameraCallback1945583101.h"
#include "UnityEngine_UnityEngine_DebugLogHandler2406589519.h"
#include "UnityEngine_UnityEngine_Debug4195163081.h"
#include "UnityEngine_UnityEngine_Display1321072632.h"
#include "UnityEngine_UnityEngine_Display_DisplaysUpdatedDele581305515.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_TouchPhase1567063616.h"
#include "UnityEngine_UnityEngine_IMECompositionMode3300198960.h"
#include "UnityEngine_UnityEngine_TouchType970257423.h"
#include "UnityEngine_UnityEngine_Touch4210255029.h"
#include "UnityEngine_UnityEngine_Input4200062272.h"
#include "UnityEngine_UnityEngine_HideFlags1436803931.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "UnityEngine_UnityEngine_Component3501516275.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "UnityEngine_UnityEngine_Transform_Enumerator3875554846.h"
#include "UnityEngine_UnityEngine_Time4241968337.h"
#include "UnityEngine_UnityEngine_Random3156561159.h"
#include "UnityEngine_UnityEngine_YieldInstruction2048002629.h"
#include "UnityEngine_UnityEngine_AndroidJavaObject2362096582.h"
#include "UnityEngine_UnityEngine_AndroidJavaClass1816259147.h"
#include "UnityEngine_UnityEngine_AndroidJNI3901305722.h"
#include "UnityEngine_UnityEngine_iOS_ADBannerView2392257822.h"
#include "UnityEngine_UnityEngine_iOS_ADBannerView_BannerWas3510840818.h"
#include "UnityEngine_UnityEngine_iOS_ADBannerView_BannerWas3604098244.h"
#include "UnityEngine_UnityEngine_iOS_ADBannerView_BannerFai2244784594.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1300 = { sizeof (Cursor_t2745727898), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1301 = { sizeof (GameCenterPlatform_t3570684786), -1, sizeof(GameCenterPlatform_t3570684786_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1301[15] = 
{
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_s_AuthenticateCallback_0(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_s_FriendsCallback_1(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_s_AchievementDescriptionLoaderCallback_2(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_s_AchievementLoaderCallback_3(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_s_ProgressCallback_4(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_s_ScoreCallback_5(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_s_ScoreLoaderCallback_6(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_s_LeaderboardCallback_7(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_s_UsersCallback_8(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_s_adCache_9(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_s_friends_10(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_s_users_11(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_s_ResetAchievements_12(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_m_LocalUser_13(),
	GameCenterPlatform_t3570684786_StaticFields::get_offset_of_m_GcBoards_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1302 = { sizeof (GcLeaderboard_t1820874799), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1302[2] = 
{
	GcLeaderboard_t1820874799::get_offset_of_m_InternalLeaderboard_0(),
	GcLeaderboard_t1820874799::get_offset_of_m_GenericLeaderboard_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1303 = { sizeof (Renderer_t3076687687), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1304 = { sizeof (InternalDrawTextureArguments_t4047764288)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1304[9] = 
{
	InternalDrawTextureArguments_t4047764288::get_offset_of_screenRect_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalDrawTextureArguments_t4047764288::get_offset_of_texture_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalDrawTextureArguments_t4047764288::get_offset_of_sourceRect_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalDrawTextureArguments_t4047764288::get_offset_of_leftBorder_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalDrawTextureArguments_t4047764288::get_offset_of_rightBorder_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalDrawTextureArguments_t4047764288::get_offset_of_topBorder_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalDrawTextureArguments_t4047764288::get_offset_of_bottomBorder_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalDrawTextureArguments_t4047764288::get_offset_of_color_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalDrawTextureArguments_t4047764288::get_offset_of_mat_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1305 = { sizeof (Graphics_t3672240399), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1306 = { sizeof (Screen_t3187157168), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1307 = { sizeof (RectOffset_t3056157787), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1307[2] = 
{
	RectOffset_t3056157787::get_offset_of_m_Ptr_0(),
	RectOffset_t3056157787::get_offset_of_m_SourceStyle_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1308 = { sizeof (GUIElement_t3775428101), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1309 = { sizeof (GUILayer_t2983897946), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1310 = { sizeof (Texture_t2526458961), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1311 = { sizeof (Texture2D_t3884108195), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1312 = { sizeof (RenderTexture_t1963041563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1313 = { sizeof (CullingGroupEvent_t2820176033)+ sizeof (Il2CppObject), sizeof(CullingGroupEvent_t2820176033_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1313[3] = 
{
	CullingGroupEvent_t2820176033::get_offset_of_m_Index_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CullingGroupEvent_t2820176033::get_offset_of_m_PrevState_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CullingGroupEvent_t2820176033::get_offset_of_m_ThisState_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1314 = { sizeof (CullingGroup_t1868862003), sizeof(CullingGroup_t1868862003_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1314[2] = 
{
	CullingGroup_t1868862003::get_offset_of_m_Ptr_0(),
	CullingGroup_t1868862003::get_offset_of_m_OnStateChanged_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1315 = { sizeof (StateChanged_t2578300556), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1316 = { sizeof (Gradient_t3661184436), sizeof(Gradient_t3661184436_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1316[1] = 
{
	Gradient_t3661184436::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1317 = { sizeof (TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572)+ sizeof (Il2CppObject), sizeof(TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1317[5] = 
{
	TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572::get_offset_of_keyboardType_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572::get_offset_of_autocorrection_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572::get_offset_of_multiline_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572::get_offset_of_secure_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TouchScreenKeyboard_InternalConstructorHelperArguments_t705488572::get_offset_of_alert_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1318 = { sizeof (TouchScreenKeyboardType_t2604324130)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1318[10] = 
{
	TouchScreenKeyboardType_t2604324130::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1319 = { sizeof (TouchScreenKeyboard_t1858258760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1319[1] = 
{
	TouchScreenKeyboard_t1858258760::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1320 = { sizeof (Gizmos_t2849394813), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1321 = { sizeof (LayerMask_t3236759763)+ sizeof (Il2CppObject), sizeof(LayerMask_t3236759763_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1321[1] = 
{
	LayerMask_t3236759763::get_offset_of_m_Mask_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1322 = { sizeof (Vector2_t4282066565)+ sizeof (Il2CppObject), sizeof(Vector2_t4282066565_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1322[3] = 
{
	0,
	Vector2_t4282066565::get_offset_of_x_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector2_t4282066565::get_offset_of_y_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1323 = { sizeof (Vector3_t4282066566)+ sizeof (Il2CppObject), sizeof(Vector3_t4282066566_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1323[4] = 
{
	0,
	Vector3_t4282066566::get_offset_of_x_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector3_t4282066566::get_offset_of_y_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector3_t4282066566::get_offset_of_z_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1324 = { sizeof (Color32_t598853688)+ sizeof (Il2CppObject), sizeof(Color32_t598853688_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1324[4] = 
{
	Color32_t598853688::get_offset_of_r_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color32_t598853688::get_offset_of_g_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color32_t598853688::get_offset_of_b_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color32_t598853688::get_offset_of_a_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1325 = { sizeof (Quaternion_t1553702882)+ sizeof (Il2CppObject), sizeof(Quaternion_t1553702882_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1325[4] = 
{
	Quaternion_t1553702882::get_offset_of_x_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Quaternion_t1553702882::get_offset_of_y_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Quaternion_t1553702882::get_offset_of_z_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Quaternion_t1553702882::get_offset_of_w_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1326 = { sizeof (Rect_t4241904616)+ sizeof (Il2CppObject), sizeof(Rect_t4241904616_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1326[4] = 
{
	Rect_t4241904616::get_offset_of_m_XMin_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Rect_t4241904616::get_offset_of_m_YMin_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Rect_t4241904616::get_offset_of_m_Width_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Rect_t4241904616::get_offset_of_m_Height_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1327 = { sizeof (Matrix4x4_t1651859333)+ sizeof (Il2CppObject), sizeof(Matrix4x4_t1651859333_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1327[16] = 
{
	Matrix4x4_t1651859333::get_offset_of_m00_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m10_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m20_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m30_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m01_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m11_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m21_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m31_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m02_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m12_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m22_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m32_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m03_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m13_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m23_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Matrix4x4_t1651859333::get_offset_of_m33_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1328 = { sizeof (Bounds_t2711641849)+ sizeof (Il2CppObject), sizeof(Bounds_t2711641849_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1328[2] = 
{
	Bounds_t2711641849::get_offset_of_m_Center_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Bounds_t2711641849::get_offset_of_m_Extents_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1329 = { sizeof (Vector4_t4282066567)+ sizeof (Il2CppObject), sizeof(Vector4_t4282066567_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1329[5] = 
{
	0,
	Vector4_t4282066567::get_offset_of_x_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector4_t4282066567::get_offset_of_y_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector4_t4282066567::get_offset_of_z_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector4_t4282066567::get_offset_of_w_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1330 = { sizeof (Ray_t3134616544)+ sizeof (Il2CppObject), sizeof(Ray_t3134616544_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1330[2] = 
{
	Ray_t3134616544::get_offset_of_m_Origin_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Ray_t3134616544::get_offset_of_m_Direction_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1331 = { sizeof (Plane_t4206452690)+ sizeof (Il2CppObject), sizeof(Plane_t4206452690_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1331[2] = 
{
	Plane_t4206452690::get_offset_of_m_Normal_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Plane_t4206452690::get_offset_of_m_Distance_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1332 = { sizeof (MathfInternal_t4096243933)+ sizeof (Il2CppObject), sizeof(MathfInternal_t4096243933_marshaled_pinvoke), sizeof(MathfInternal_t4096243933_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1332[3] = 
{
	MathfInternal_t4096243933_StaticFields::get_offset_of_FloatMinNormal_0(),
	MathfInternal_t4096243933_StaticFields::get_offset_of_FloatMinDenormal_1(),
	MathfInternal_t4096243933_StaticFields::get_offset_of_IsFlushToZeroEnabled_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1333 = { sizeof (Mathf_t4203372500)+ sizeof (Il2CppObject), sizeof(Mathf_t4203372500_marshaled_pinvoke), sizeof(Mathf_t4203372500_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1333[1] = 
{
	Mathf_t4203372500_StaticFields::get_offset_of_Epsilon_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1334 = { sizeof (Keyframe_t4079056114)+ sizeof (Il2CppObject), sizeof(Keyframe_t4079056114_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1334[4] = 
{
	Keyframe_t4079056114::get_offset_of_m_Time_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Keyframe_t4079056114::get_offset_of_m_Value_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Keyframe_t4079056114::get_offset_of_m_InTangent_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Keyframe_t4079056114::get_offset_of_m_OutTangent_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1335 = { sizeof (AnimationCurve_t3667593487), sizeof(AnimationCurve_t3667593487_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1335[1] = 
{
	AnimationCurve_t3667593487::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1336 = { sizeof (Mesh_t4241756145), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1337 = { sizeof (InternalShaderChannel_t2277052758)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1337[9] = 
{
	InternalShaderChannel_t2277052758::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1338 = { sizeof (InternalVertexChannelType_t2290942609)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1338[3] = 
{
	InternalVertexChannelType_t2290942609::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1339 = { sizeof (NetworkPlayer_t3231273765)+ sizeof (Il2CppObject), sizeof(NetworkPlayer_t3231273765_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1339[1] = 
{
	NetworkPlayer_t3231273765::get_offset_of_index_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1340 = { sizeof (NetworkViewID_t3400394436)+ sizeof (Il2CppObject), sizeof(NetworkViewID_t3400394436_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1340[3] = 
{
	NetworkViewID_t3400394436::get_offset_of_a_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NetworkViewID_t3400394436::get_offset_of_b_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NetworkViewID_t3400394436::get_offset_of_c_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1341 = { sizeof (NetworkView_t3656680617), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1342 = { sizeof (BitStream_t239125475), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1342[1] = 
{
	BitStream_t239125475::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1343 = { sizeof (RPC_t3134615963), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1344 = { sizeof (HostData_t3270478838), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1344[10] = 
{
	HostData_t3270478838::get_offset_of_m_Nat_0(),
	HostData_t3270478838::get_offset_of_m_GameType_1(),
	HostData_t3270478838::get_offset_of_m_GameName_2(),
	HostData_t3270478838::get_offset_of_m_ConnectedPlayers_3(),
	HostData_t3270478838::get_offset_of_m_PlayerLimit_4(),
	HostData_t3270478838::get_offset_of_m_IP_5(),
	HostData_t3270478838::get_offset_of_m_Port_6(),
	HostData_t3270478838::get_offset_of_m_PasswordProtected_7(),
	HostData_t3270478838::get_offset_of_m_Comment_8(),
	HostData_t3270478838::get_offset_of_m_GUID_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1345 = { sizeof (NetworkMessageInfo_t3807997963)+ sizeof (Il2CppObject), sizeof(NetworkMessageInfo_t3807997963_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1345[3] = 
{
	NetworkMessageInfo_t3807997963::get_offset_of_m_TimeStamp_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NetworkMessageInfo_t3807997963::get_offset_of_m_Sender_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NetworkMessageInfo_t3807997963::get_offset_of_m_ViewID_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1346 = { sizeof (DrivenTransformProperties_t624110065)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1346[26] = 
{
	DrivenTransformProperties_t624110065::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1347 = { sizeof (DrivenRectTransformTracker_t4185719096)+ sizeof (Il2CppObject), sizeof(DrivenRectTransformTracker_t4185719096_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1348 = { sizeof (RectTransform_t972643934), -1, sizeof(RectTransform_t972643934_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1348[1] = 
{
	RectTransform_t972643934_StaticFields::get_offset_of_reapplyDrivenProperties_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1349 = { sizeof (Edge_t1676794651)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1349[5] = 
{
	Edge_t1676794651::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1350 = { sizeof (Axis_t1676694783)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1350[3] = 
{
	Axis_t1676694783::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1351 = { sizeof (ReapplyDrivenProperties_t779639188), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1352 = { sizeof (ResourceRequest_t3731857623), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1352[2] = 
{
	ResourceRequest_t3731857623::get_offset_of_m_Path_1(),
	ResourceRequest_t3731857623::get_offset_of_m_Type_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1353 = { sizeof (Resources_t2918352667), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1354 = { sizeof (SerializePrivateVariables_t2835496234), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1355 = { sizeof (SerializeField_t3754825534), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1356 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1357 = { sizeof (Shader_t3191267369), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1358 = { sizeof (Material_t3870600107), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1359 = { sizeof (SortingLayer_t3376264497)+ sizeof (Il2CppObject), sizeof(SortingLayer_t3376264497_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1359[1] = 
{
	SortingLayer_t3376264497::get_offset_of_m_Id_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1360 = { sizeof (Sprite_t3199167241), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1361 = { sizeof (SpriteRenderer_t2548470764), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1362 = { sizeof (DataUtility_t1448936472), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1363 = { sizeof (Hash128_t346790303)+ sizeof (Il2CppObject), sizeof(Hash128_t346790303_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1363[4] = 
{
	Hash128_t346790303::get_offset_of_m_u32_0_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Hash128_t346790303::get_offset_of_m_u32_1_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Hash128_t346790303::get_offset_of_m_u32_2_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Hash128_t346790303::get_offset_of_m_u32_3_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1364 = { sizeof (WWW_t3134621005), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1365 = { sizeof (WWWForm_t461342257), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1365[6] = 
{
	WWWForm_t461342257::get_offset_of_formData_0(),
	WWWForm_t461342257::get_offset_of_fieldNames_1(),
	WWWForm_t461342257::get_offset_of_fileNames_2(),
	WWWForm_t461342257::get_offset_of_types_3(),
	WWWForm_t461342257::get_offset_of_boundary_4(),
	WWWForm_t461342257::get_offset_of_containsFiles_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1366 = { sizeof (WWWTranscoder_t609724394), -1, sizeof(WWWTranscoder_t609724394_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1366[8] = 
{
	WWWTranscoder_t609724394_StaticFields::get_offset_of_ucHexChars_0(),
	WWWTranscoder_t609724394_StaticFields::get_offset_of_lcHexChars_1(),
	WWWTranscoder_t609724394_StaticFields::get_offset_of_urlEscapeChar_2(),
	WWWTranscoder_t609724394_StaticFields::get_offset_of_urlSpace_3(),
	WWWTranscoder_t609724394_StaticFields::get_offset_of_urlForbidden_4(),
	WWWTranscoder_t609724394_StaticFields::get_offset_of_qpEscapeChar_5(),
	WWWTranscoder_t609724394_StaticFields::get_offset_of_qpSpace_6(),
	WWWTranscoder_t609724394_StaticFields::get_offset_of_qpForbidden_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1367 = { sizeof (UnityString_t3369712284), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1368 = { sizeof (AsyncOperation_t3699081103), sizeof(AsyncOperation_t3699081103_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1368[1] = 
{
	AsyncOperation_t3699081103::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1369 = { sizeof (Application_t2856536070), -1, sizeof(Application_t2856536070_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1369[2] = 
{
	Application_t2856536070_StaticFields::get_offset_of_s_LogCallbackHandler_0(),
	Application_t2856536070_StaticFields::get_offset_of_s_LogCallbackHandlerThreaded_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1370 = { sizeof (LogCallback_t2984951347), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1371 = { sizeof (Behaviour_t200106419), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1372 = { sizeof (Camera_t2727095145), -1, sizeof(Camera_t2727095145_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1372[3] = 
{
	Camera_t2727095145_StaticFields::get_offset_of_onPreCull_2(),
	Camera_t2727095145_StaticFields::get_offset_of_onPreRender_3(),
	Camera_t2727095145_StaticFields::get_offset_of_onPostRender_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1373 = { sizeof (CameraCallback_t1945583101), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1374 = { sizeof (DebugLogHandler_t2406589519), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1375 = { sizeof (Debug_t4195163081), -1, sizeof(Debug_t4195163081_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1375[1] = 
{
	Debug_t4195163081_StaticFields::get_offset_of_s_Logger_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1376 = { sizeof (Display_t1321072632), -1, sizeof(Display_t1321072632_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1376[4] = 
{
	Display_t1321072632::get_offset_of_nativeDisplay_0(),
	Display_t1321072632_StaticFields::get_offset_of_displays_1(),
	Display_t1321072632_StaticFields::get_offset_of__mainDisplay_2(),
	Display_t1321072632_StaticFields::get_offset_of_onDisplaysUpdated_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1377 = { sizeof (DisplaysUpdatedDelegate_t581305515), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1378 = { sizeof (MonoBehaviour_t667441552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1379 = { sizeof (TouchPhase_t1567063616)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1379[6] = 
{
	TouchPhase_t1567063616::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1380 = { sizeof (IMECompositionMode_t3300198960)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1380[4] = 
{
	IMECompositionMode_t3300198960::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1381 = { sizeof (TouchType_t970257423)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1381[4] = 
{
	TouchType_t970257423::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1382 = { sizeof (Touch_t4210255029)+ sizeof (Il2CppObject), sizeof(Touch_t4210255029_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1382[14] = 
{
	Touch_t4210255029::get_offset_of_m_FingerId_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t4210255029::get_offset_of_m_Position_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t4210255029::get_offset_of_m_RawPosition_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t4210255029::get_offset_of_m_PositionDelta_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t4210255029::get_offset_of_m_TimeDelta_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t4210255029::get_offset_of_m_TapCount_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t4210255029::get_offset_of_m_Phase_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t4210255029::get_offset_of_m_Type_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t4210255029::get_offset_of_m_Pressure_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t4210255029::get_offset_of_m_maximumPossiblePressure_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t4210255029::get_offset_of_m_Radius_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t4210255029::get_offset_of_m_RadiusVariance_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t4210255029::get_offset_of_m_AltitudeAngle_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Touch_t4210255029::get_offset_of_m_AzimuthAngle_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1383 = { sizeof (Input_t4200062272), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1384 = { sizeof (HideFlags_t1436803931)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1384[10] = 
{
	HideFlags_t1436803931::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1385 = { sizeof (Object_t3071478659), sizeof(Object_t3071478659_marshaled_pinvoke), sizeof(Object_t3071478659_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1385[2] = 
{
	Object_t3071478659::get_offset_of_m_CachedPtr_0(),
	Object_t3071478659_StaticFields::get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1386 = { sizeof (Component_t3501516275), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1387 = { sizeof (GameObject_t3674682005), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1388 = { sizeof (Transform_t1659122786), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1389 = { sizeof (Enumerator_t3875554846), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1389[2] = 
{
	Enumerator_t3875554846::get_offset_of_outer_0(),
	Enumerator_t3875554846::get_offset_of_currentIndex_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1390 = { sizeof (Time_t4241968337), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1391 = { sizeof (Random_t3156561159), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1392 = { sizeof (YieldInstruction_t2048002629), sizeof(YieldInstruction_t2048002629_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1393 = { sizeof (AndroidJavaObject_t2362096582), -1, sizeof(AndroidJavaObject_t2362096582_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1393[5] = 
{
	AndroidJavaObject_t2362096582_StaticFields::get_offset_of_enableDebugPrints_0(),
	AndroidJavaObject_t2362096582::get_offset_of_m_disposed_1(),
	AndroidJavaObject_t2362096582::get_offset_of_m_jobject_2(),
	AndroidJavaObject_t2362096582::get_offset_of_m_jclass_3(),
	AndroidJavaObject_t2362096582_StaticFields::get_offset_of_s_JavaLangClass_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1394 = { sizeof (AndroidJavaClass_t1816259147), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1395 = { sizeof (AndroidJNI_t3901305722), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1396 = { sizeof (ADBannerView_t2392257822), -1, sizeof(ADBannerView_t2392257822_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1396[4] = 
{
	ADBannerView_t2392257822::get_offset_of__bannerView_0(),
	ADBannerView_t2392257822_StaticFields::get_offset_of_onBannerWasClicked_1(),
	ADBannerView_t2392257822_StaticFields::get_offset_of_onBannerWasLoaded_2(),
	ADBannerView_t2392257822_StaticFields::get_offset_of_onBannerFailedToLoad_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1397 = { sizeof (BannerWasClickedDelegate_t3510840818), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1398 = { sizeof (BannerWasLoadedDelegate_t3604098244), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1399 = { sizeof (BannerFailedToLoadDelegate_t2244784594), sizeof(Il2CppMethodPointer), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
