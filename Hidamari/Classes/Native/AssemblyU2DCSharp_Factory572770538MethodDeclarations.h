﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Factory
struct Factory_t572770538;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t747900261;
// UnityEngine.Sprite
struct Sprite_t3199167241;

#include "codegen/il2cpp-codegen.h"

// System.Void Factory::.ctor()
extern "C"  void Factory__ctor_m4075277761 (Factory_t572770538 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Factory::Start()
extern "C"  void Factory_Start_m3022415553 (Factory_t572770538 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Factory::Awake()
extern "C"  void Factory_Awake_m17915684 (Factory_t572770538 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Factory::Update()
extern "C"  void Factory_Update_m3506421100 (Factory_t572770538 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Factory::CreateBullet(System.Int32)
extern "C"  GameObject_t3674682005 * Factory_CreateBullet_m576246781 (Factory_t572770538 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Factory::CreateCharactor(System.Int32)
extern "C"  GameObject_t3674682005 * Factory_CreateCharactor_m3066266792 (Factory_t572770538 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Factory::getBullet(System.Collections.Generic.List`1<UnityEngine.GameObject>)
extern "C"  GameObject_t3674682005 * Factory_getBullet_m205084046 (Factory_t572770538 * __this, List_1_t747900261 * ___objects0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite Factory::GetCharactorSprite(System.Int32)
extern "C"  Sprite_t3199167241 * Factory_GetCharactorSprite_m1373062113 (Factory_t572770538 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite Factory::GetBulletSprite(System.Int32)
extern "C"  Sprite_t3199167241 * Factory_GetBulletSprite_m2525236430 (Factory_t572770538 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Factory::BulletisStop(System.Boolean)
extern "C"  void Factory_BulletisStop_m1808191848 (Factory_t572770538 * __this, bool ___stop0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Factory::BulletFlyweight(System.Boolean)
extern "C"  void Factory_BulletFlyweight_m2740473951 (Factory_t572770538 * __this, bool ___flag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Factory::Reset()
extern "C"  void Factory_Reset_m1721710702 (Factory_t572770538 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
