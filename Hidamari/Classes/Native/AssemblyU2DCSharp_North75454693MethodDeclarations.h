﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// North
struct North_t75454693;

#include "codegen/il2cpp-codegen.h"

// System.Void North::.ctor()
extern "C"  void North__ctor_m2263589414 (North_t75454693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void North::Start()
extern "C"  void North_Start_m1210727206 (North_t75454693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void North::Awake()
extern "C"  void North_Awake_m2501194633 (North_t75454693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void North::Update()
extern "C"  void North_Update_m3178657191 (North_t75454693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void North::FixedUpdate()
extern "C"  void North_FixedUpdate_m3428895521 (North_t75454693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void North::MoveSatrtPosition()
extern "C"  void North_MoveSatrtPosition_m3072819300 (North_t75454693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void North::MoveStart()
extern "C"  void North_MoveStart_m3093037205 (North_t75454693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void North::ChangeMove()
extern "C"  void North_ChangeMove_m35978239 (North_t75454693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void North::setCoordinate()
extern "C"  void North_setCoordinate_m3062030686 (North_t75454693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void North::Shot()
extern "C"  void North_Shot_m28390104 (North_t75454693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void North::Move()
extern "C"  void North_Move_m4158241263 (North_t75454693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void North::ShotParticle()
extern "C"  void North_ShotParticle_m3321507070 (North_t75454693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
