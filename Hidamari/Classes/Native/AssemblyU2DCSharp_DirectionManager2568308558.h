﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2761310900;

#include "AssemblyU2DCSharp_UIManager1861242489.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DirectionManager
struct  DirectionManager_t2568308558  : public UIManager_t1861242489
{
public:
	// UnityEngine.Vector3 DirectionManager::_thirdbuttonpos
	Vector3_t4282066566  ____thirdbuttonpos_7;
	// System.Single DirectionManager::_rotate
	float ____rotate_8;
	// System.Single DirectionManager::_rotatespd
	float ____rotatespd_9;
	// UnityEngine.GameObject DirectionManager::_DirectionStageCanvas
	GameObject_t3674682005 * ____DirectionStageCanvas_10;
	// UnityEngine.Sprite[] DirectionManager::_DirectionImage
	SpriteU5BU5D_t2761310900* ____DirectionImage_11;

public:
	inline static int32_t get_offset_of__thirdbuttonpos_7() { return static_cast<int32_t>(offsetof(DirectionManager_t2568308558, ____thirdbuttonpos_7)); }
	inline Vector3_t4282066566  get__thirdbuttonpos_7() const { return ____thirdbuttonpos_7; }
	inline Vector3_t4282066566 * get_address_of__thirdbuttonpos_7() { return &____thirdbuttonpos_7; }
	inline void set__thirdbuttonpos_7(Vector3_t4282066566  value)
	{
		____thirdbuttonpos_7 = value;
	}

	inline static int32_t get_offset_of__rotate_8() { return static_cast<int32_t>(offsetof(DirectionManager_t2568308558, ____rotate_8)); }
	inline float get__rotate_8() const { return ____rotate_8; }
	inline float* get_address_of__rotate_8() { return &____rotate_8; }
	inline void set__rotate_8(float value)
	{
		____rotate_8 = value;
	}

	inline static int32_t get_offset_of__rotatespd_9() { return static_cast<int32_t>(offsetof(DirectionManager_t2568308558, ____rotatespd_9)); }
	inline float get__rotatespd_9() const { return ____rotatespd_9; }
	inline float* get_address_of__rotatespd_9() { return &____rotatespd_9; }
	inline void set__rotatespd_9(float value)
	{
		____rotatespd_9 = value;
	}

	inline static int32_t get_offset_of__DirectionStageCanvas_10() { return static_cast<int32_t>(offsetof(DirectionManager_t2568308558, ____DirectionStageCanvas_10)); }
	inline GameObject_t3674682005 * get__DirectionStageCanvas_10() const { return ____DirectionStageCanvas_10; }
	inline GameObject_t3674682005 ** get_address_of__DirectionStageCanvas_10() { return &____DirectionStageCanvas_10; }
	inline void set__DirectionStageCanvas_10(GameObject_t3674682005 * value)
	{
		____DirectionStageCanvas_10 = value;
		Il2CppCodeGenWriteBarrier(&____DirectionStageCanvas_10, value);
	}

	inline static int32_t get_offset_of__DirectionImage_11() { return static_cast<int32_t>(offsetof(DirectionManager_t2568308558, ____DirectionImage_11)); }
	inline SpriteU5BU5D_t2761310900* get__DirectionImage_11() const { return ____DirectionImage_11; }
	inline SpriteU5BU5D_t2761310900** get_address_of__DirectionImage_11() { return &____DirectionImage_11; }
	inline void set__DirectionImage_11(SpriteU5BU5D_t2761310900* value)
	{
		____DirectionImage_11 = value;
		Il2CppCodeGenWriteBarrier(&____DirectionImage_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
