﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen2525087348.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StageManager
struct  StageManager_t3194912495  : public SingletonMonoBehaviour_1_t2525087348
{
public:
	// System.Boolean StageManager::is_play
	bool ___is_play_3;

public:
	inline static int32_t get_offset_of_is_play_3() { return static_cast<int32_t>(offsetof(StageManager_t3194912495, ___is_play_3)); }
	inline bool get_is_play_3() const { return ___is_play_3; }
	inline bool* get_address_of_is_play_3() { return &___is_play_3; }
	inline void set_is_play_3(bool value)
	{
		___is_play_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
