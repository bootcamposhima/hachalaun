﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SocketIO.Ack
struct Ack_t4240269559;
// System.Action`1<JSONObject>
struct Action_1_t2148193039;
// JSONObject
struct JSONObject_t1752376903;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSONObject1752376903.h"

// System.Void SocketIO.Ack::.ctor(System.Int32,System.Action`1<JSONObject>)
extern "C"  void Ack__ctor_m3356688195 (Ack_t4240269559 * __this, int32_t ___packetId0, Action_1_t2148193039 * ___action1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SocketIO.Ack::Invoke(JSONObject)
extern "C"  void Ack_Invoke_m3998143310 (Ack_t4240269559 * __this, JSONObject_t1752376903 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SocketIO.Ack::ToString()
extern "C"  String_t* Ack_ToString_m1526510284 (Ack_t4240269559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
