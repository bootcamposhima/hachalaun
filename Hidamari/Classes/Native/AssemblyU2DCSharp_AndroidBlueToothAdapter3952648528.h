﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t2362096582;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AndroidBlueToothAdapter
struct  AndroidBlueToothAdapter_t3952648528  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.AndroidJavaObject AndroidBlueToothAdapter::cls
	AndroidJavaObject_t2362096582 * ___cls_10;
	// System.Boolean AndroidBlueToothAdapter::serverFlag
	bool ___serverFlag_11;
	// System.Boolean AndroidBlueToothAdapter::retryRequest
	bool ___retryRequest_12;

public:
	inline static int32_t get_offset_of_cls_10() { return static_cast<int32_t>(offsetof(AndroidBlueToothAdapter_t3952648528, ___cls_10)); }
	inline AndroidJavaObject_t2362096582 * get_cls_10() const { return ___cls_10; }
	inline AndroidJavaObject_t2362096582 ** get_address_of_cls_10() { return &___cls_10; }
	inline void set_cls_10(AndroidJavaObject_t2362096582 * value)
	{
		___cls_10 = value;
		Il2CppCodeGenWriteBarrier(&___cls_10, value);
	}

	inline static int32_t get_offset_of_serverFlag_11() { return static_cast<int32_t>(offsetof(AndroidBlueToothAdapter_t3952648528, ___serverFlag_11)); }
	inline bool get_serverFlag_11() const { return ___serverFlag_11; }
	inline bool* get_address_of_serverFlag_11() { return &___serverFlag_11; }
	inline void set_serverFlag_11(bool value)
	{
		___serverFlag_11 = value;
	}

	inline static int32_t get_offset_of_retryRequest_12() { return static_cast<int32_t>(offsetof(AndroidBlueToothAdapter_t3952648528, ___retryRequest_12)); }
	inline bool get_retryRequest_12() const { return ___retryRequest_12; }
	inline bool* get_address_of_retryRequest_12() { return &___retryRequest_12; }
	inline void set_retryRequest_12(bool value)
	{
		___retryRequest_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
