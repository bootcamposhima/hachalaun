﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<AIData>
struct DefaultComparer_t438680323;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AIData1930434546.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<AIData>::.ctor()
extern "C"  void DefaultComparer__ctor_m1210721656_gshared (DefaultComparer_t438680323 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1210721656(__this, method) ((  void (*) (DefaultComparer_t438680323 *, const MethodInfo*))DefaultComparer__ctor_m1210721656_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<AIData>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m1862344447_gshared (DefaultComparer_t438680323 * __this, AIData_t1930434546  ___x0, AIData_t1930434546  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m1862344447(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t438680323 *, AIData_t1930434546 , AIData_t1930434546 , const MethodInfo*))DefaultComparer_Compare_m1862344447_gshared)(__this, ___x0, ___y1, method)
