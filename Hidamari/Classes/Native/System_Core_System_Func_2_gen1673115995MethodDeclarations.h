﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen184564025MethodDeclarations.h"

// System.Void System.Func`2<System.String[],WebSocketSharp.HandshakeResponse>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m2730669671(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t1673115995 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m3944524044_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<System.String[],WebSocketSharp.HandshakeResponse>::Invoke(T)
#define Func_2_Invoke_m1612897327(__this, ___arg10, method) ((  HandshakeResponse_t3229697822 * (*) (Func_2_t1673115995 *, StringU5BU5D_t4054002952*, const MethodInfo*))Func_2_Invoke_m1924616534_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<System.String[],WebSocketSharp.HandshakeResponse>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m4022922594(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t1673115995 *, StringU5BU5D_t4054002952*, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m4281703301_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<System.String[],WebSocketSharp.HandshakeResponse>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m1189096197(__this, ___result0, method) ((  HandshakeResponse_t3229697822 * (*) (Func_2_t1673115995 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m4118168638_gshared)(__this, ___result0, method)
