﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey2F
struct U3CdumpU3Ec__AnonStorey2F_t681710991;
// System.Action`4<System.String,System.String,System.String,System.String>
struct Action_4_t828544145;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey2F::.ctor()
extern "C"  void U3CdumpU3Ec__AnonStorey2F__ctor_m1441017532 (U3CdumpU3Ec__AnonStorey2F_t681710991 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action`4<System.String,System.String,System.String,System.String> WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey2F::<>m__19()
extern "C"  Action_4_t828544145 * U3CdumpU3Ec__AnonStorey2F_U3CU3Em__19_m1858461973 (U3CdumpU3Ec__AnonStorey2F_t681710991 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
