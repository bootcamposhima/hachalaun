﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FightJson
struct FightJson_t649175288;
struct FightJson_t649175288_marshaled_pinvoke;
struct FightJson_t649175288_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FightJson649175288.h"
#include "AssemblyU2DCSharp_AIDatas4008896193.h"

// System.Void FightJson::.ctor(System.Int32,AIDatas)
extern "C"  void FightJson__ctor_m3104630479 (FightJson_t649175288 * __this, int32_t ___n0, AIDatas_t4008896193  ___ad1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightJson::.ctor(System.Int32,System.Int32)
extern "C"  void FightJson__ctor_m3248488211 (FightJson_t649175288 * __this, int32_t ___n0, int32_t ___h1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightJson::.ctor(System.Int32)
extern "C"  void FightJson__ctor_m3147050052 (FightJson_t649175288 * __this, int32_t ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct FightJson_t649175288;
struct FightJson_t649175288_marshaled_pinvoke;

extern "C" void FightJson_t649175288_marshal_pinvoke(const FightJson_t649175288& unmarshaled, FightJson_t649175288_marshaled_pinvoke& marshaled);
extern "C" void FightJson_t649175288_marshal_pinvoke_back(const FightJson_t649175288_marshaled_pinvoke& marshaled, FightJson_t649175288& unmarshaled);
extern "C" void FightJson_t649175288_marshal_pinvoke_cleanup(FightJson_t649175288_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct FightJson_t649175288;
struct FightJson_t649175288_marshaled_com;

extern "C" void FightJson_t649175288_marshal_com(const FightJson_t649175288& unmarshaled, FightJson_t649175288_marshaled_com& marshaled);
extern "C" void FightJson_t649175288_marshal_com_back(const FightJson_t649175288_marshaled_com& marshaled, FightJson_t649175288& unmarshaled);
extern "C" void FightJson_t649175288_marshal_com_cleanup(FightJson_t649175288_marshaled_com& marshaled);
