﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Round
struct  Round_t79151470  : public MonoBehaviour_t667441552
{
public:
	// System.Int32 Round::NowRound
	int32_t ___NowRound_2;
	// UnityEngine.GameObject Round::_EnemyManager
	GameObject_t3674682005 * ____EnemyManager_3;
	// UnityEngine.GameObject Round::_DefenceManager
	GameObject_t3674682005 * ____DefenceManager_4;
	// UnityEngine.GameObject Round::_Result
	GameObject_t3674682005 * ____Result_5;
	// UnityEngine.GameObject Round::_HP
	GameObject_t3674682005 * ____HP_6;
	// UnityEngine.GameObject Round::_PlaySelect
	GameObject_t3674682005 * ____PlaySelect_7;
	// UnityEngine.GameObject Round::_RoundText
	GameObject_t3674682005 * ____RoundText_8;
	// UnityEngine.GameObject Round::_TernText
	GameObject_t3674682005 * ____TernText_9;
	// System.Boolean Round::isAttack
	bool ___isAttack_10;

public:
	inline static int32_t get_offset_of_NowRound_2() { return static_cast<int32_t>(offsetof(Round_t79151470, ___NowRound_2)); }
	inline int32_t get_NowRound_2() const { return ___NowRound_2; }
	inline int32_t* get_address_of_NowRound_2() { return &___NowRound_2; }
	inline void set_NowRound_2(int32_t value)
	{
		___NowRound_2 = value;
	}

	inline static int32_t get_offset_of__EnemyManager_3() { return static_cast<int32_t>(offsetof(Round_t79151470, ____EnemyManager_3)); }
	inline GameObject_t3674682005 * get__EnemyManager_3() const { return ____EnemyManager_3; }
	inline GameObject_t3674682005 ** get_address_of__EnemyManager_3() { return &____EnemyManager_3; }
	inline void set__EnemyManager_3(GameObject_t3674682005 * value)
	{
		____EnemyManager_3 = value;
		Il2CppCodeGenWriteBarrier(&____EnemyManager_3, value);
	}

	inline static int32_t get_offset_of__DefenceManager_4() { return static_cast<int32_t>(offsetof(Round_t79151470, ____DefenceManager_4)); }
	inline GameObject_t3674682005 * get__DefenceManager_4() const { return ____DefenceManager_4; }
	inline GameObject_t3674682005 ** get_address_of__DefenceManager_4() { return &____DefenceManager_4; }
	inline void set__DefenceManager_4(GameObject_t3674682005 * value)
	{
		____DefenceManager_4 = value;
		Il2CppCodeGenWriteBarrier(&____DefenceManager_4, value);
	}

	inline static int32_t get_offset_of__Result_5() { return static_cast<int32_t>(offsetof(Round_t79151470, ____Result_5)); }
	inline GameObject_t3674682005 * get__Result_5() const { return ____Result_5; }
	inline GameObject_t3674682005 ** get_address_of__Result_5() { return &____Result_5; }
	inline void set__Result_5(GameObject_t3674682005 * value)
	{
		____Result_5 = value;
		Il2CppCodeGenWriteBarrier(&____Result_5, value);
	}

	inline static int32_t get_offset_of__HP_6() { return static_cast<int32_t>(offsetof(Round_t79151470, ____HP_6)); }
	inline GameObject_t3674682005 * get__HP_6() const { return ____HP_6; }
	inline GameObject_t3674682005 ** get_address_of__HP_6() { return &____HP_6; }
	inline void set__HP_6(GameObject_t3674682005 * value)
	{
		____HP_6 = value;
		Il2CppCodeGenWriteBarrier(&____HP_6, value);
	}

	inline static int32_t get_offset_of__PlaySelect_7() { return static_cast<int32_t>(offsetof(Round_t79151470, ____PlaySelect_7)); }
	inline GameObject_t3674682005 * get__PlaySelect_7() const { return ____PlaySelect_7; }
	inline GameObject_t3674682005 ** get_address_of__PlaySelect_7() { return &____PlaySelect_7; }
	inline void set__PlaySelect_7(GameObject_t3674682005 * value)
	{
		____PlaySelect_7 = value;
		Il2CppCodeGenWriteBarrier(&____PlaySelect_7, value);
	}

	inline static int32_t get_offset_of__RoundText_8() { return static_cast<int32_t>(offsetof(Round_t79151470, ____RoundText_8)); }
	inline GameObject_t3674682005 * get__RoundText_8() const { return ____RoundText_8; }
	inline GameObject_t3674682005 ** get_address_of__RoundText_8() { return &____RoundText_8; }
	inline void set__RoundText_8(GameObject_t3674682005 * value)
	{
		____RoundText_8 = value;
		Il2CppCodeGenWriteBarrier(&____RoundText_8, value);
	}

	inline static int32_t get_offset_of__TernText_9() { return static_cast<int32_t>(offsetof(Round_t79151470, ____TernText_9)); }
	inline GameObject_t3674682005 * get__TernText_9() const { return ____TernText_9; }
	inline GameObject_t3674682005 ** get_address_of__TernText_9() { return &____TernText_9; }
	inline void set__TernText_9(GameObject_t3674682005 * value)
	{
		____TernText_9 = value;
		Il2CppCodeGenWriteBarrier(&____TernText_9, value);
	}

	inline static int32_t get_offset_of_isAttack_10() { return static_cast<int32_t>(offsetof(Round_t79151470, ___isAttack_10)); }
	inline bool get_isAttack_10() const { return ___isAttack_10; }
	inline bool* get_address_of_isAttack_10() { return &___isAttack_10; }
	inline void set_isAttack_10(bool value)
	{
		___isAttack_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
