﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<AIDatas>
struct List_1_t1082114449;
// System.Collections.Generic.IEnumerable`1<AIDatas>
struct IEnumerable_1_t3014841854;
// System.Collections.Generic.IEnumerator`1<AIDatas>
struct IEnumerator_1_t1625793946;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<AIDatas>
struct ICollection_1_t608518884;
// System.Collections.ObjectModel.ReadOnlyCollection`1<AIDatas>
struct ReadOnlyCollection_1_t1271006433;
// AIDatas[]
struct AIDatasU5BU5D_t2487978268;
// System.Predicate`1<AIDatas>
struct Predicate_1_t3619953076;
// System.Comparison`1<AIDatas>
struct Comparison_1_t2725257380;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_AIDatas4008896193.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1101787219.h"

// System.Void System.Collections.Generic.List`1<AIDatas>::.ctor()
extern "C"  void List_1__ctor_m3961798944_gshared (List_1_t1082114449 * __this, const MethodInfo* method);
#define List_1__ctor_m3961798944(__this, method) ((  void (*) (List_1_t1082114449 *, const MethodInfo*))List_1__ctor_m3961798944_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AIDatas>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m744117544_gshared (List_1_t1082114449 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m744117544(__this, ___collection0, method) ((  void (*) (List_1_t1082114449 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m744117544_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<AIDatas>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m182320584_gshared (List_1_t1082114449 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m182320584(__this, ___capacity0, method) ((  void (*) (List_1_t1082114449 *, int32_t, const MethodInfo*))List_1__ctor_m182320584_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<AIDatas>::.cctor()
extern "C"  void List_1__cctor_m1282156438_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1282156438(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m1282156438_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<AIDatas>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1293495177_gshared (List_1_t1082114449 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1293495177(__this, method) ((  Il2CppObject* (*) (List_1_t1082114449 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1293495177_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AIDatas>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m1868868141_gshared (List_1_t1082114449 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1868868141(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1082114449 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1868868141_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<AIDatas>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m916572392_gshared (List_1_t1082114449 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m916572392(__this, method) ((  Il2CppObject * (*) (List_1_t1082114449 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m916572392_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<AIDatas>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m234110089_gshared (List_1_t1082114449 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m234110089(__this, ___item0, method) ((  int32_t (*) (List_1_t1082114449 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m234110089_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<AIDatas>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m2906864995_gshared (List_1_t1082114449 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m2906864995(__this, ___item0, method) ((  bool (*) (List_1_t1082114449 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2906864995_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<AIDatas>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m4165011041_gshared (List_1_t1082114449 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m4165011041(__this, ___item0, method) ((  int32_t (*) (List_1_t1082114449 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m4165011041_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<AIDatas>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m953283084_gshared (List_1_t1082114449 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m953283084(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1082114449 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m953283084_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<AIDatas>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m2785366940_gshared (List_1_t1082114449 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m2785366940(__this, ___item0, method) ((  void (*) (List_1_t1082114449 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m2785366940_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<AIDatas>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3895987620_gshared (List_1_t1082114449 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3895987620(__this, method) ((  bool (*) (List_1_t1082114449 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3895987620_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<AIDatas>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m3279420185_gshared (List_1_t1082114449 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m3279420185(__this, method) ((  bool (*) (List_1_t1082114449 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3279420185_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<AIDatas>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m2566741381_gshared (List_1_t1082114449 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m2566741381(__this, method) ((  Il2CppObject * (*) (List_1_t1082114449 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m2566741381_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<AIDatas>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m3092345618_gshared (List_1_t1082114449 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m3092345618(__this, method) ((  bool (*) (List_1_t1082114449 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m3092345618_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<AIDatas>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m1939235239_gshared (List_1_t1082114449 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1939235239(__this, method) ((  bool (*) (List_1_t1082114449 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1939235239_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<AIDatas>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m2392850892_gshared (List_1_t1082114449 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m2392850892(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1082114449 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m2392850892_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<AIDatas>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m1890962147_gshared (List_1_t1082114449 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1890962147(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1082114449 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1890962147_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<AIDatas>::Add(T)
extern "C"  void List_1_Add_m3656159760_gshared (List_1_t1082114449 * __this, AIDatas_t4008896193  ___item0, const MethodInfo* method);
#define List_1_Add_m3656159760(__this, ___item0, method) ((  void (*) (List_1_t1082114449 *, AIDatas_t4008896193 , const MethodInfo*))List_1_Add_m3656159760_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<AIDatas>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m1596361955_gshared (List_1_t1082114449 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1596361955(__this, ___newCount0, method) ((  void (*) (List_1_t1082114449 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1596361955_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<AIDatas>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m87201761_gshared (List_1_t1082114449 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m87201761(__this, ___collection0, method) ((  void (*) (List_1_t1082114449 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m87201761_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<AIDatas>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m3643141281_gshared (List_1_t1082114449 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m3643141281(__this, ___enumerable0, method) ((  void (*) (List_1_t1082114449 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m3643141281_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<AIDatas>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m1435492694_gshared (List_1_t1082114449 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m1435492694(__this, ___collection0, method) ((  void (*) (List_1_t1082114449 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m1435492694_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<AIDatas>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t1271006433 * List_1_AsReadOnly_m133089683_gshared (List_1_t1082114449 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m133089683(__this, method) ((  ReadOnlyCollection_1_t1271006433 * (*) (List_1_t1082114449 *, const MethodInfo*))List_1_AsReadOnly_m133089683_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AIDatas>::Clear()
extern "C"  void List_1_Clear_m926727970_gshared (List_1_t1082114449 * __this, const MethodInfo* method);
#define List_1_Clear_m926727970(__this, method) ((  void (*) (List_1_t1082114449 *, const MethodInfo*))List_1_Clear_m926727970_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<AIDatas>::Contains(T)
extern "C"  bool List_1_Contains_m2412421584_gshared (List_1_t1082114449 * __this, AIDatas_t4008896193  ___item0, const MethodInfo* method);
#define List_1_Contains_m2412421584(__this, ___item0, method) ((  bool (*) (List_1_t1082114449 *, AIDatas_t4008896193 , const MethodInfo*))List_1_Contains_m2412421584_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<AIDatas>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m4155560088_gshared (List_1_t1082114449 * __this, AIDatasU5BU5D_t2487978268* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m4155560088(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1082114449 *, AIDatasU5BU5D_t2487978268*, int32_t, const MethodInfo*))List_1_CopyTo_m4155560088_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<AIDatas>::Find(System.Predicate`1<T>)
extern "C"  AIDatas_t4008896193  List_1_Find_m2733051152_gshared (List_1_t1082114449 * __this, Predicate_1_t3619953076 * ___match0, const MethodInfo* method);
#define List_1_Find_m2733051152(__this, ___match0, method) ((  AIDatas_t4008896193  (*) (List_1_t1082114449 *, Predicate_1_t3619953076 *, const MethodInfo*))List_1_Find_m2733051152_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<AIDatas>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m3558242635_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t3619953076 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m3558242635(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t3619953076 *, const MethodInfo*))List_1_CheckMatch_m3558242635_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<AIDatas>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m2929264_gshared (List_1_t1082114449 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t3619953076 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m2929264(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1082114449 *, int32_t, int32_t, Predicate_1_t3619953076 *, const MethodInfo*))List_1_GetIndex_m2929264_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<AIDatas>::GetEnumerator()
extern "C"  Enumerator_t1101787219  List_1_GetEnumerator_m3037671181_gshared (List_1_t1082114449 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m3037671181(__this, method) ((  Enumerator_t1101787219  (*) (List_1_t1082114449 *, const MethodInfo*))List_1_GetEnumerator_m3037671181_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<AIDatas>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m75992348_gshared (List_1_t1082114449 * __this, AIDatas_t4008896193  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m75992348(__this, ___item0, method) ((  int32_t (*) (List_1_t1082114449 *, AIDatas_t4008896193 , const MethodInfo*))List_1_IndexOf_m75992348_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<AIDatas>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m1457164015_gshared (List_1_t1082114449 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m1457164015(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1082114449 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1457164015_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<AIDatas>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m3901927272_gshared (List_1_t1082114449 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m3901927272(__this, ___index0, method) ((  void (*) (List_1_t1082114449 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3901927272_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<AIDatas>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m4093259023_gshared (List_1_t1082114449 * __this, int32_t ___index0, AIDatas_t4008896193  ___item1, const MethodInfo* method);
#define List_1_Insert_m4093259023(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1082114449 *, int32_t, AIDatas_t4008896193 , const MethodInfo*))List_1_Insert_m4093259023_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<AIDatas>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m2723182148_gshared (List_1_t1082114449 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m2723182148(__this, ___collection0, method) ((  void (*) (List_1_t1082114449 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2723182148_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<AIDatas>::Remove(T)
extern "C"  bool List_1_Remove_m4024276427_gshared (List_1_t1082114449 * __this, AIDatas_t4008896193  ___item0, const MethodInfo* method);
#define List_1_Remove_m4024276427(__this, ___item0, method) ((  bool (*) (List_1_t1082114449 *, AIDatas_t4008896193 , const MethodInfo*))List_1_Remove_m4024276427_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<AIDatas>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m2167887839_gshared (List_1_t1082114449 * __this, Predicate_1_t3619953076 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m2167887839(__this, ___match0, method) ((  int32_t (*) (List_1_t1082114449 *, Predicate_1_t3619953076 *, const MethodInfo*))List_1_RemoveAll_m2167887839_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<AIDatas>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m1967111893_gshared (List_1_t1082114449 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m1967111893(__this, ___index0, method) ((  void (*) (List_1_t1082114449 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1967111893_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<AIDatas>::Reverse()
extern "C"  void List_1_Reverse_m3190035831_gshared (List_1_t1082114449 * __this, const MethodInfo* method);
#define List_1_Reverse_m3190035831(__this, method) ((  void (*) (List_1_t1082114449 *, const MethodInfo*))List_1_Reverse_m3190035831_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AIDatas>::Sort()
extern "C"  void List_1_Sort_m2292249995_gshared (List_1_t1082114449 * __this, const MethodInfo* method);
#define List_1_Sort_m2292249995(__this, method) ((  void (*) (List_1_t1082114449 *, const MethodInfo*))List_1_Sort_m2292249995_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AIDatas>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m3133110174_gshared (List_1_t1082114449 * __this, Comparison_1_t2725257380 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m3133110174(__this, ___comparison0, method) ((  void (*) (List_1_t1082114449 *, Comparison_1_t2725257380 *, const MethodInfo*))List_1_Sort_m3133110174_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<AIDatas>::ToArray()
extern "C"  AIDatasU5BU5D_t2487978268* List_1_ToArray_m577365174_gshared (List_1_t1082114449 * __this, const MethodInfo* method);
#define List_1_ToArray_m577365174(__this, method) ((  AIDatasU5BU5D_t2487978268* (*) (List_1_t1082114449 *, const MethodInfo*))List_1_ToArray_m577365174_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AIDatas>::TrimExcess()
extern "C"  void List_1_TrimExcess_m2290516196_gshared (List_1_t1082114449 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m2290516196(__this, method) ((  void (*) (List_1_t1082114449 *, const MethodInfo*))List_1_TrimExcess_m2290516196_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<AIDatas>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m228824908_gshared (List_1_t1082114449 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m228824908(__this, method) ((  int32_t (*) (List_1_t1082114449 *, const MethodInfo*))List_1_get_Capacity_m228824908_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AIDatas>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m3101518069_gshared (List_1_t1082114449 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m3101518069(__this, ___value0, method) ((  void (*) (List_1_t1082114449 *, int32_t, const MethodInfo*))List_1_set_Capacity_m3101518069_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<AIDatas>::get_Count()
extern "C"  int32_t List_1_get_Count_m153591638_gshared (List_1_t1082114449 * __this, const MethodInfo* method);
#define List_1_get_Count_m153591638(__this, method) ((  int32_t (*) (List_1_t1082114449 *, const MethodInfo*))List_1_get_Count_m153591638_gshared)(__this, method)
// T System.Collections.Generic.List`1<AIDatas>::get_Item(System.Int32)
extern "C"  AIDatas_t4008896193  List_1_get_Item_m1162615295_gshared (List_1_t1082114449 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m1162615295(__this, ___index0, method) ((  AIDatas_t4008896193  (*) (List_1_t1082114449 *, int32_t, const MethodInfo*))List_1_get_Item_m1162615295_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<AIDatas>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m3375302162_gshared (List_1_t1082114449 * __this, int32_t ___index0, AIDatas_t4008896193  ___value1, const MethodInfo* method);
#define List_1_set_Item_m3375302162(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1082114449 *, int32_t, AIDatas_t4008896193 , const MethodInfo*))List_1_set_Item_m3375302162_gshared)(__this, ___index0, ___value1, method)
