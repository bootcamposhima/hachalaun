﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t2662109048;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2761310900;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Result
struct  Result_t2444407869  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject[] Result::player1
	GameObjectU5BU5D_t2662109048* ___player1_2;
	// UnityEngine.GameObject[] Result::player2
	GameObjectU5BU5D_t2662109048* ___player2_3;
	// UnityEngine.GameObject Result::roundCamera
	GameObject_t3674682005 * ___roundCamera_4;
	// UnityEngine.GameObject Result::canvas
	GameObject_t3674682005 * ___canvas_5;
	// UnityEngine.GameObject Result::i_result
	GameObject_t3674682005 * ___i_result_6;
	// UnityEngine.Sprite[] Result::sp_result
	SpriteU5BU5D_t2761310900* ___sp_result_7;
	// System.Int32 Result::_player1Win
	int32_t ____player1Win_8;
	// System.Int32 Result::_player2Win
	int32_t ____player2Win_9;
	// System.Int32 Result::_player1HP
	int32_t ____player1HP_10;
	// System.Int32 Result::_player2HP
	int32_t ____player2HP_11;
	// System.Boolean Result::isFight
	bool ___isFight_12;

public:
	inline static int32_t get_offset_of_player1_2() { return static_cast<int32_t>(offsetof(Result_t2444407869, ___player1_2)); }
	inline GameObjectU5BU5D_t2662109048* get_player1_2() const { return ___player1_2; }
	inline GameObjectU5BU5D_t2662109048** get_address_of_player1_2() { return &___player1_2; }
	inline void set_player1_2(GameObjectU5BU5D_t2662109048* value)
	{
		___player1_2 = value;
		Il2CppCodeGenWriteBarrier(&___player1_2, value);
	}

	inline static int32_t get_offset_of_player2_3() { return static_cast<int32_t>(offsetof(Result_t2444407869, ___player2_3)); }
	inline GameObjectU5BU5D_t2662109048* get_player2_3() const { return ___player2_3; }
	inline GameObjectU5BU5D_t2662109048** get_address_of_player2_3() { return &___player2_3; }
	inline void set_player2_3(GameObjectU5BU5D_t2662109048* value)
	{
		___player2_3 = value;
		Il2CppCodeGenWriteBarrier(&___player2_3, value);
	}

	inline static int32_t get_offset_of_roundCamera_4() { return static_cast<int32_t>(offsetof(Result_t2444407869, ___roundCamera_4)); }
	inline GameObject_t3674682005 * get_roundCamera_4() const { return ___roundCamera_4; }
	inline GameObject_t3674682005 ** get_address_of_roundCamera_4() { return &___roundCamera_4; }
	inline void set_roundCamera_4(GameObject_t3674682005 * value)
	{
		___roundCamera_4 = value;
		Il2CppCodeGenWriteBarrier(&___roundCamera_4, value);
	}

	inline static int32_t get_offset_of_canvas_5() { return static_cast<int32_t>(offsetof(Result_t2444407869, ___canvas_5)); }
	inline GameObject_t3674682005 * get_canvas_5() const { return ___canvas_5; }
	inline GameObject_t3674682005 ** get_address_of_canvas_5() { return &___canvas_5; }
	inline void set_canvas_5(GameObject_t3674682005 * value)
	{
		___canvas_5 = value;
		Il2CppCodeGenWriteBarrier(&___canvas_5, value);
	}

	inline static int32_t get_offset_of_i_result_6() { return static_cast<int32_t>(offsetof(Result_t2444407869, ___i_result_6)); }
	inline GameObject_t3674682005 * get_i_result_6() const { return ___i_result_6; }
	inline GameObject_t3674682005 ** get_address_of_i_result_6() { return &___i_result_6; }
	inline void set_i_result_6(GameObject_t3674682005 * value)
	{
		___i_result_6 = value;
		Il2CppCodeGenWriteBarrier(&___i_result_6, value);
	}

	inline static int32_t get_offset_of_sp_result_7() { return static_cast<int32_t>(offsetof(Result_t2444407869, ___sp_result_7)); }
	inline SpriteU5BU5D_t2761310900* get_sp_result_7() const { return ___sp_result_7; }
	inline SpriteU5BU5D_t2761310900** get_address_of_sp_result_7() { return &___sp_result_7; }
	inline void set_sp_result_7(SpriteU5BU5D_t2761310900* value)
	{
		___sp_result_7 = value;
		Il2CppCodeGenWriteBarrier(&___sp_result_7, value);
	}

	inline static int32_t get_offset_of__player1Win_8() { return static_cast<int32_t>(offsetof(Result_t2444407869, ____player1Win_8)); }
	inline int32_t get__player1Win_8() const { return ____player1Win_8; }
	inline int32_t* get_address_of__player1Win_8() { return &____player1Win_8; }
	inline void set__player1Win_8(int32_t value)
	{
		____player1Win_8 = value;
	}

	inline static int32_t get_offset_of__player2Win_9() { return static_cast<int32_t>(offsetof(Result_t2444407869, ____player2Win_9)); }
	inline int32_t get__player2Win_9() const { return ____player2Win_9; }
	inline int32_t* get_address_of__player2Win_9() { return &____player2Win_9; }
	inline void set__player2Win_9(int32_t value)
	{
		____player2Win_9 = value;
	}

	inline static int32_t get_offset_of__player1HP_10() { return static_cast<int32_t>(offsetof(Result_t2444407869, ____player1HP_10)); }
	inline int32_t get__player1HP_10() const { return ____player1HP_10; }
	inline int32_t* get_address_of__player1HP_10() { return &____player1HP_10; }
	inline void set__player1HP_10(int32_t value)
	{
		____player1HP_10 = value;
	}

	inline static int32_t get_offset_of__player2HP_11() { return static_cast<int32_t>(offsetof(Result_t2444407869, ____player2HP_11)); }
	inline int32_t get__player2HP_11() const { return ____player2HP_11; }
	inline int32_t* get_address_of__player2HP_11() { return &____player2HP_11; }
	inline void set__player2HP_11(int32_t value)
	{
		____player2HP_11 = value;
	}

	inline static int32_t get_offset_of_isFight_12() { return static_cast<int32_t>(offsetof(Result_t2444407869, ___isFight_12)); }
	inline bool get_isFight_12() const { return ___isFight_12; }
	inline bool* get_address_of_isFight_12() { return &___isFight_12; }
	inline void set_isFight_12(bool value)
	{
		___isFight_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
