﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "AssemblyU2DCSharp_UIManager1861242489.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharManager
struct  CharManager_t988180311  : public UIManager_t1861242489
{
public:
	// UnityEngine.GameObject CharManager::_button
	GameObject_t3674682005 * ____button_7;
	// UnityEngine.GameObject CharManager::_scroll
	GameObject_t3674682005 * ____scroll_8;
	// UnityEngine.GameObject CharManager::_content
	GameObject_t3674682005 * ____content_9;
	// UnityEngine.Vector3 CharManager::_scrollfirstpos
	Vector3_t4282066566  ____scrollfirstpos_10;
	// UnityEngine.Vector3 CharManager::_scrollsecpundpos
	Vector3_t4282066566  ____scrollsecpundpos_11;
	// UnityEngine.Vector3 CharManager::_scrolldestination
	Vector3_t4282066566  ____scrolldestination_12;
	// System.Single CharManager::_scrollspd
	float ____scrollspd_13;
	// System.Boolean CharManager::_scrollismove
	bool ____scrollismove_14;
	// System.Boolean CharManager::istop
	bool ___istop_15;

public:
	inline static int32_t get_offset_of__button_7() { return static_cast<int32_t>(offsetof(CharManager_t988180311, ____button_7)); }
	inline GameObject_t3674682005 * get__button_7() const { return ____button_7; }
	inline GameObject_t3674682005 ** get_address_of__button_7() { return &____button_7; }
	inline void set__button_7(GameObject_t3674682005 * value)
	{
		____button_7 = value;
		Il2CppCodeGenWriteBarrier(&____button_7, value);
	}

	inline static int32_t get_offset_of__scroll_8() { return static_cast<int32_t>(offsetof(CharManager_t988180311, ____scroll_8)); }
	inline GameObject_t3674682005 * get__scroll_8() const { return ____scroll_8; }
	inline GameObject_t3674682005 ** get_address_of__scroll_8() { return &____scroll_8; }
	inline void set__scroll_8(GameObject_t3674682005 * value)
	{
		____scroll_8 = value;
		Il2CppCodeGenWriteBarrier(&____scroll_8, value);
	}

	inline static int32_t get_offset_of__content_9() { return static_cast<int32_t>(offsetof(CharManager_t988180311, ____content_9)); }
	inline GameObject_t3674682005 * get__content_9() const { return ____content_9; }
	inline GameObject_t3674682005 ** get_address_of__content_9() { return &____content_9; }
	inline void set__content_9(GameObject_t3674682005 * value)
	{
		____content_9 = value;
		Il2CppCodeGenWriteBarrier(&____content_9, value);
	}

	inline static int32_t get_offset_of__scrollfirstpos_10() { return static_cast<int32_t>(offsetof(CharManager_t988180311, ____scrollfirstpos_10)); }
	inline Vector3_t4282066566  get__scrollfirstpos_10() const { return ____scrollfirstpos_10; }
	inline Vector3_t4282066566 * get_address_of__scrollfirstpos_10() { return &____scrollfirstpos_10; }
	inline void set__scrollfirstpos_10(Vector3_t4282066566  value)
	{
		____scrollfirstpos_10 = value;
	}

	inline static int32_t get_offset_of__scrollsecpundpos_11() { return static_cast<int32_t>(offsetof(CharManager_t988180311, ____scrollsecpundpos_11)); }
	inline Vector3_t4282066566  get__scrollsecpundpos_11() const { return ____scrollsecpundpos_11; }
	inline Vector3_t4282066566 * get_address_of__scrollsecpundpos_11() { return &____scrollsecpundpos_11; }
	inline void set__scrollsecpundpos_11(Vector3_t4282066566  value)
	{
		____scrollsecpundpos_11 = value;
	}

	inline static int32_t get_offset_of__scrolldestination_12() { return static_cast<int32_t>(offsetof(CharManager_t988180311, ____scrolldestination_12)); }
	inline Vector3_t4282066566  get__scrolldestination_12() const { return ____scrolldestination_12; }
	inline Vector3_t4282066566 * get_address_of__scrolldestination_12() { return &____scrolldestination_12; }
	inline void set__scrolldestination_12(Vector3_t4282066566  value)
	{
		____scrolldestination_12 = value;
	}

	inline static int32_t get_offset_of__scrollspd_13() { return static_cast<int32_t>(offsetof(CharManager_t988180311, ____scrollspd_13)); }
	inline float get__scrollspd_13() const { return ____scrollspd_13; }
	inline float* get_address_of__scrollspd_13() { return &____scrollspd_13; }
	inline void set__scrollspd_13(float value)
	{
		____scrollspd_13 = value;
	}

	inline static int32_t get_offset_of__scrollismove_14() { return static_cast<int32_t>(offsetof(CharManager_t988180311, ____scrollismove_14)); }
	inline bool get__scrollismove_14() const { return ____scrollismove_14; }
	inline bool* get_address_of__scrollismove_14() { return &____scrollismove_14; }
	inline void set__scrollismove_14(bool value)
	{
		____scrollismove_14 = value;
	}

	inline static int32_t get_offset_of_istop_15() { return static_cast<int32_t>(offsetof(CharManager_t988180311, ___istop_15)); }
	inline bool get_istop_15() const { return ___istop_15; }
	inline bool* get_address_of_istop_15() { return &___istop_15; }
	inline void set_istop_15(bool value)
	{
		___istop_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
