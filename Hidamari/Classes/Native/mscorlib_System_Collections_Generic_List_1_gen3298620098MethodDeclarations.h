﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<AIData>
struct List_1_t3298620098;
// System.Collections.Generic.IEnumerable`1<AIData>
struct IEnumerable_1_t936380207;
// System.Collections.Generic.IEnumerator`1<AIData>
struct IEnumerator_1_t3842299595;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<AIData>
struct ICollection_1_t2825024533;
// System.Collections.ObjectModel.ReadOnlyCollection`1<AIData>
struct ReadOnlyCollection_1_t3487512082;
// AIData[]
struct AIDataU5BU5D_t2017435655;
// System.Predicate`1<AIData>
struct Predicate_1_t1541491429;
// System.Comparison`1<AIData>
struct Comparison_1_t646795733;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_AIData1930434546.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3318292868.h"

// System.Void System.Collections.Generic.List`1<AIData>::.ctor()
extern "C"  void List_1__ctor_m481393309_gshared (List_1_t3298620098 * __this, const MethodInfo* method);
#define List_1__ctor_m481393309(__this, method) ((  void (*) (List_1_t3298620098 *, const MethodInfo*))List_1__ctor_m481393309_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AIData>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m848254681_gshared (List_1_t3298620098 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m848254681(__this, ___collection0, method) ((  void (*) (List_1_t3298620098 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m848254681_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<AIData>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m1764961390_gshared (List_1_t3298620098 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m1764961390(__this, ___capacity0, method) ((  void (*) (List_1_t3298620098 *, int32_t, const MethodInfo*))List_1__ctor_m1764961390_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<AIData>::.cctor()
extern "C"  void List_1__cctor_m1114990215_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1114990215(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m1114990215_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<AIData>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2226753392_gshared (List_1_t3298620098 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2226753392(__this, method) ((  Il2CppObject* (*) (List_1_t3298620098 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2226753392_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AIData>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m4042809886_gshared (List_1_t3298620098 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m4042809886(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3298620098 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m4042809886_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<AIData>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m430493229_gshared (List_1_t3298620098 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m430493229(__this, method) ((  Il2CppObject * (*) (List_1_t3298620098 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m430493229_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<AIData>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m1909549744_gshared (List_1_t3298620098 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1909549744(__this, ___item0, method) ((  int32_t (*) (List_1_t3298620098 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1909549744_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<AIData>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m831373072_gshared (List_1_t3298620098 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m831373072(__this, ___item0, method) ((  bool (*) (List_1_t3298620098 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m831373072_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<AIData>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m2952579336_gshared (List_1_t3298620098 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m2952579336(__this, ___item0, method) ((  int32_t (*) (List_1_t3298620098 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m2952579336_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<AIData>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m2390050683_gshared (List_1_t3298620098 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m2390050683(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3298620098 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2390050683_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<AIData>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m1125701069_gshared (List_1_t3298620098 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1125701069(__this, ___item0, method) ((  void (*) (List_1_t3298620098 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1125701069_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<AIData>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4104264465_gshared (List_1_t3298620098 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4104264465(__this, method) ((  bool (*) (List_1_t3298620098 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4104264465_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<AIData>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m2648735308_gshared (List_1_t3298620098 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m2648735308(__this, method) ((  bool (*) (List_1_t3298620098 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m2648735308_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<AIData>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m476950910_gshared (List_1_t3298620098 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m476950910(__this, method) ((  Il2CppObject * (*) (List_1_t3298620098 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m476950910_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<AIData>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m1616247423_gshared (List_1_t3298620098 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m1616247423(__this, method) ((  bool (*) (List_1_t3298620098 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m1616247423_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<AIData>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m3831281818_gshared (List_1_t3298620098 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m3831281818(__this, method) ((  bool (*) (List_1_t3298620098 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m3831281818_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<AIData>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m4148902789_gshared (List_1_t3298620098 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m4148902789(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t3298620098 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m4148902789_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<AIData>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m3940122770_gshared (List_1_t3298620098 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m3940122770(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3298620098 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3940122770_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<AIData>::Add(T)
extern "C"  void List_1_Add_m175754125_gshared (List_1_t3298620098 * __this, AIData_t1930434546  ___item0, const MethodInfo* method);
#define List_1_Add_m175754125(__this, ___item0, method) ((  void (*) (List_1_t3298620098 *, AIData_t1930434546 , const MethodInfo*))List_1_Add_m175754125_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<AIData>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m637686164_gshared (List_1_t3298620098 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m637686164(__this, ___newCount0, method) ((  void (*) (List_1_t3298620098 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m637686164_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<AIData>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m3458685074_gshared (List_1_t3298620098 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m3458685074(__this, ___collection0, method) ((  void (*) (List_1_t3298620098 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m3458685074_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<AIData>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m2719657298_gshared (List_1_t3298620098 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m2719657298(__this, ___enumerable0, method) ((  void (*) (List_1_t3298620098 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2719657298_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<AIData>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m2818553349_gshared (List_1_t3298620098 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m2818553349(__this, ___collection0, method) ((  void (*) (List_1_t3298620098 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2818553349_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<AIData>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t3487512082 * List_1_AsReadOnly_m2030609120_gshared (List_1_t3298620098 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m2030609120(__this, method) ((  ReadOnlyCollection_1_t3487512082 * (*) (List_1_t3298620098 *, const MethodInfo*))List_1_AsReadOnly_m2030609120_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AIData>::Clear()
extern "C"  void List_1_Clear_m2860998161_gshared (List_1_t3298620098 * __this, const MethodInfo* method);
#define List_1_Clear_m2860998161(__this, method) ((  void (*) (List_1_t3298620098 *, const MethodInfo*))List_1_Clear_m2860998161_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<AIData>::Contains(T)
extern "C"  bool List_1_Contains_m2326041603_gshared (List_1_t3298620098 * __this, AIData_t1930434546  ___item0, const MethodInfo* method);
#define List_1_Contains_m2326041603(__this, ___item0, method) ((  bool (*) (List_1_t3298620098 *, AIData_t1930434546 , const MethodInfo*))List_1_Contains_m2326041603_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<AIData>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m3479702921_gshared (List_1_t3298620098 * __this, AIDataU5BU5D_t2017435655* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m3479702921(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3298620098 *, AIDataU5BU5D_t2017435655*, int32_t, const MethodInfo*))List_1_CopyTo_m3479702921_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<AIData>::Find(System.Predicate`1<T>)
extern "C"  AIData_t1930434546  List_1_Find_m3093499293_gshared (List_1_t3298620098 * __this, Predicate_1_t1541491429 * ___match0, const MethodInfo* method);
#define List_1_Find_m3093499293(__this, ___match0, method) ((  AIData_t1930434546  (*) (List_1_t3298620098 *, Predicate_1_t1541491429 *, const MethodInfo*))List_1_Find_m3093499293_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<AIData>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m3924179834_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t1541491429 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m3924179834(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t1541491429 *, const MethodInfo*))List_1_CheckMatch_m3924179834_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<AIData>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m4207841367_gshared (List_1_t3298620098 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t1541491429 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m4207841367(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t3298620098 *, int32_t, int32_t, Predicate_1_t1541491429 *, const MethodInfo*))List_1_GetIndex_m4207841367_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<AIData>::GetEnumerator()
extern "C"  Enumerator_t3318292868  List_1_GetEnumerator_m4213316544_gshared (List_1_t3298620098 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m4213316544(__this, method) ((  Enumerator_t3318292868  (*) (List_1_t3298620098 *, const MethodInfo*))List_1_GetEnumerator_m4213316544_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<AIData>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m2470617621_gshared (List_1_t3298620098 * __this, AIData_t1930434546  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m2470617621(__this, ___item0, method) ((  int32_t (*) (List_1_t3298620098 *, AIData_t1930434546 , const MethodInfo*))List_1_IndexOf_m2470617621_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<AIData>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m637684448_gshared (List_1_t3298620098 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m637684448(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t3298620098 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m637684448_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<AIData>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m3226070105_gshared (List_1_t3298620098 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m3226070105(__this, ___index0, method) ((  void (*) (List_1_t3298620098 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3226070105_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<AIData>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m3784176192_gshared (List_1_t3298620098 * __this, int32_t ___index0, AIData_t1930434546  ___item1, const MethodInfo* method);
#define List_1_Insert_m3784176192(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3298620098 *, int32_t, AIData_t1930434546 , const MethodInfo*))List_1_Insert_m3784176192_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<AIData>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m18337461_gshared (List_1_t3298620098 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m18337461(__this, ___collection0, method) ((  void (*) (List_1_t3298620098 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m18337461_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<AIData>::Remove(T)
extern "C"  bool List_1_Remove_m2471171080_gshared (List_1_t3298620098 * __this, AIData_t1930434546  ___item0, const MethodInfo* method);
#define List_1_Remove_m2471171080(__this, ___item0, method) ((  bool (*) (List_1_t3298620098 *, AIData_t1930434546 , const MethodInfo*))List_1_Remove_m2471171080_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<AIData>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m2347334680_gshared (List_1_t3298620098 * __this, Predicate_1_t1541491429 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m2347334680(__this, ___match0, method) ((  int32_t (*) (List_1_t3298620098 *, Predicate_1_t1541491429 *, const MethodInfo*))List_1_RemoveAll_m2347334680_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<AIData>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m1658029062_gshared (List_1_t3298620098 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m1658029062(__this, ___index0, method) ((  void (*) (List_1_t3298620098 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1658029062_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<AIData>::Reverse()
extern "C"  void List_1_Reverse_m2302850214_gshared (List_1_t3298620098 * __this, const MethodInfo* method);
#define List_1_Reverse_m2302850214(__this, method) ((  void (*) (List_1_t3298620098 *, const MethodInfo*))List_1_Reverse_m2302850214_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AIData>::Sort()
extern "C"  void List_1_Sort_m2770287804_gshared (List_1_t3298620098 * __this, const MethodInfo* method);
#define List_1_Sort_m2770287804(__this, method) ((  void (*) (List_1_t3298620098 *, const MethodInfo*))List_1_Sort_m2770287804_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AIData>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m968676367_gshared (List_1_t3298620098 * __this, Comparison_1_t646795733 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m968676367(__this, ___comparison0, method) ((  void (*) (List_1_t3298620098 *, Comparison_1_t646795733 *, const MethodInfo*))List_1_Sort_m968676367_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<AIData>::ToArray()
extern "C"  AIDataU5BU5D_t2017435655* List_1_ToArray_m1044461183_gshared (List_1_t3298620098 * __this, const MethodInfo* method);
#define List_1_ToArray_m1044461183(__this, method) ((  AIDataU5BU5D_t2017435655* (*) (List_1_t3298620098 *, const MethodInfo*))List_1_ToArray_m1044461183_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AIData>::TrimExcess()
extern "C"  void List_1_TrimExcess_m3372539733_gshared (List_1_t3298620098 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m3372539733(__this, method) ((  void (*) (List_1_t3298620098 *, const MethodInfo*))List_1_TrimExcess_m3372539733_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<AIData>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m3999893445_gshared (List_1_t3298620098 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m3999893445(__this, method) ((  int32_t (*) (List_1_t3298620098 *, const MethodInfo*))List_1_get_Capacity_m3999893445_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<AIData>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m2142842278_gshared (List_1_t3298620098 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m2142842278(__this, ___value0, method) ((  void (*) (List_1_t3298620098 *, int32_t, const MethodInfo*))List_1_set_Capacity_m2142842278_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<AIData>::get_Count()
extern "C"  int32_t List_1_get_Count_m1248655279_gshared (List_1_t3298620098 * __this, const MethodInfo* method);
#define List_1_get_Count_m1248655279(__this, method) ((  int32_t (*) (List_1_t3298620098 *, const MethodInfo*))List_1_get_Count_m1248655279_gshared)(__this, method)
// T System.Collections.Generic.List`1<AIData>::get_Item(System.Int32)
extern "C"  AIData_t1930434546  List_1_get_Item_m3231359942_gshared (List_1_t3298620098 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m3231359942(__this, ___index0, method) ((  AIData_t1930434546  (*) (List_1_t3298620098 *, int32_t, const MethodInfo*))List_1_get_Item_m3231359942_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<AIData>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m565617431_gshared (List_1_t3298620098 * __this, int32_t ___index0, AIData_t1930434546  ___value1, const MethodInfo* method);
#define List_1_set_Item_m565617431(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3298620098 *, int32_t, AIData_t1930434546 , const MethodInfo*))List_1_set_Item_m565617431_gshared)(__this, ___index0, ___value1, method)
