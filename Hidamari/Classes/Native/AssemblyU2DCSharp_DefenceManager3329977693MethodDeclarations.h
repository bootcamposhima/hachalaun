﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DefenceManager
struct DefenceManager_t3329977693;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"

// System.Void DefenceManager::.ctor()
extern "C"  void DefenceManager__ctor_m3184728030 (DefenceManager_t3329977693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefenceManager::Start()
extern "C"  void DefenceManager_Start_m2131865822 (DefenceManager_t3329977693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefenceManager::Update()
extern "C"  void DefenceManager_Update_m1669183215 (DefenceManager_t3329977693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject DefenceManager::CreatDefenceCharctor(System.Int32,System.Boolean)
extern "C"  GameObject_t3674682005 * DefenceManager_CreatDefenceCharctor_m2051452192 (DefenceManager_t3329977693 * __this, int32_t ___charNum0, bool ___isAI1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefenceManager::PlayerStart()
extern "C"  void DefenceManager_PlayerStart_m3422140029 (DefenceManager_t3329977693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefenceManager::MoveStop()
extern "C"  void DefenceManager_MoveStop_m779001305 (DefenceManager_t3329977693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefenceManager::MoveReStart()
extern "C"  void DefenceManager_MoveReStart_m4043825690 (DefenceManager_t3329977693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DefenceManager::GetHP()
extern "C"  int32_t DefenceManager_GetHP_m894617228 (DefenceManager_t3329977693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefenceManager::DestroyCharctor()
extern "C"  void DefenceManager_DestroyCharctor_m2899120832 (DefenceManager_t3329977693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefenceManager::EndAction()
extern "C"  void DefenceManager_EndAction_m1233891885 (DefenceManager_t3329977693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefenceManager::SetGameSpeed(System.Single)
extern "C"  void DefenceManager_SetGameSpeed_m26052914 (DefenceManager_t3329977693 * __this, float ___speed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DefenceManager::GameOver()
extern "C"  void DefenceManager_GameOver_m2874251660 (DefenceManager_t3329977693 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
