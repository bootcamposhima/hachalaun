﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<AIDatas>
struct EqualityComparer_1_t3116733074;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.EqualityComparer`1<AIDatas>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m3568662048_gshared (EqualityComparer_1_t3116733074 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m3568662048(__this, method) ((  void (*) (EqualityComparer_1_t3116733074 *, const MethodInfo*))EqualityComparer_1__ctor_m3568662048_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<AIDatas>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m2772244877_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m2772244877(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m2772244877_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<AIDatas>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2804785657_gshared (EqualityComparer_1_t3116733074 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2804785657(__this, ___obj0, method) ((  int32_t (*) (EqualityComparer_1_t3116733074 *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2804785657_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<AIDatas>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2812299185_gshared (EqualityComparer_1_t3116733074 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2812299185(__this, ___x0, ___y1, method) ((  bool (*) (EqualityComparer_1_t3116733074 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2812299185_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<AIDatas>::get_Default()
extern "C"  EqualityComparer_1_t3116733074 * EqualityComparer_1_get_Default_m3404975562_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m3404975562(__this /* static, unused */, method) ((  EqualityComparer_1_t3116733074 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m3404975562_gshared)(__this /* static, unused */, method)
