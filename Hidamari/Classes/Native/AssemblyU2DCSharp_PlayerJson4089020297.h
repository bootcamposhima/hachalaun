﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType1744280289.h"
#include "AssemblyU2DCSharp_MoveJson4254904441.h"
#include "AssemblyU2DCSharp_HitJson2591263483.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerJson
struct  PlayerJson_t4089020297 
{
public:
	// System.Int32 PlayerJson::num
	int32_t ___num_0;
	// MoveJson PlayerJson::mj
	MoveJson_t4254904441  ___mj_1;
	// HitJson PlayerJson::hj
	HitJson_t2591263483  ___hj_2;

public:
	inline static int32_t get_offset_of_num_0() { return static_cast<int32_t>(offsetof(PlayerJson_t4089020297, ___num_0)); }
	inline int32_t get_num_0() const { return ___num_0; }
	inline int32_t* get_address_of_num_0() { return &___num_0; }
	inline void set_num_0(int32_t value)
	{
		___num_0 = value;
	}

	inline static int32_t get_offset_of_mj_1() { return static_cast<int32_t>(offsetof(PlayerJson_t4089020297, ___mj_1)); }
	inline MoveJson_t4254904441  get_mj_1() const { return ___mj_1; }
	inline MoveJson_t4254904441 * get_address_of_mj_1() { return &___mj_1; }
	inline void set_mj_1(MoveJson_t4254904441  value)
	{
		___mj_1 = value;
	}

	inline static int32_t get_offset_of_hj_2() { return static_cast<int32_t>(offsetof(PlayerJson_t4089020297, ___hj_2)); }
	inline HitJson_t2591263483  get_hj_2() const { return ___hj_2; }
	inline HitJson_t2591263483 * get_address_of_hj_2() { return &___hj_2; }
	inline void set_hj_2(HitJson_t2591263483  value)
	{
		___hj_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: PlayerJson
struct PlayerJson_t4089020297_marshaled_pinvoke
{
	int32_t ___num_0;
	MoveJson_t4254904441_marshaled_pinvoke ___mj_1;
	HitJson_t2591263483_marshaled_pinvoke ___hj_2;
};
// Native definition for marshalling of: PlayerJson
struct PlayerJson_t4089020297_marshaled_com
{
	int32_t ___num_0;
	MoveJson_t4254904441_marshaled_com ___mj_1;
	HitJson_t2591263483_marshaled_com ___hj_2;
};
