﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3363211663MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Net.IPAddress,System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m923238609(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2484686602 *, Dictionary_2_t1167363210 *, const MethodInfo*))Enumerator__ctor_m3920831137_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Net.IPAddress,System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m637917370(__this, method) ((  Il2CppObject * (*) (Enumerator_t2484686602 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3262087712_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Net.IPAddress,System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m278400836(__this, method) ((  void (*) (Enumerator_t2484686602 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2959141748_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Net.IPAddress,System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m481770875(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t2484686602 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2279524093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Net.IPAddress,System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1293558294(__this, method) ((  Il2CppObject * (*) (Enumerator_t2484686602 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1201448700_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Net.IPAddress,System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2912408360(__this, method) ((  Il2CppObject * (*) (Enumerator_t2484686602 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m294434446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Net.IPAddress,System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>>::MoveNext()
#define Enumerator_MoveNext_m745213428(__this, method) ((  bool (*) (Enumerator_t2484686602 *, const MethodInfo*))Enumerator_MoveNext_m217327200_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Net.IPAddress,System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>>::get_Current()
#define Enumerator_get_Current_m1620375368(__this, method) ((  KeyValuePair_2_t1066143916  (*) (Enumerator_t2484686602 *, const MethodInfo*))Enumerator_get_Current_m4240003024_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Net.IPAddress,System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m845492861(__this, method) ((  IPAddress_t3525271463 * (*) (Enumerator_t2484686602 *, const MethodInfo*))Enumerator_get_CurrentKey_m3062159917_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Net.IPAddress,System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m3758293629(__this, method) ((  Dictionary_2_t3185352818 * (*) (Enumerator_t2484686602 *, const MethodInfo*))Enumerator_get_CurrentValue_m592783249_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Net.IPAddress,System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>>::Reset()
#define Enumerator_Reset_m4277629347(__this, method) ((  void (*) (Enumerator_t2484686602 *, const MethodInfo*))Enumerator_Reset_m3001375603_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Net.IPAddress,System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>>::VerifyState()
#define Enumerator_VerifyState_m713669484(__this, method) ((  void (*) (Enumerator_t2484686602 *, const MethodInfo*))Enumerator_VerifyState_m4290054460_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Net.IPAddress,System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1386478548(__this, method) ((  void (*) (Enumerator_t2484686602 *, const MethodInfo*))Enumerator_VerifyCurrent_m2318603684_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Net.IPAddress,System.Collections.Generic.Dictionary`2<System.Int32,WebSocketSharp.Net.EndPointListener>>::Dispose()
#define Enumerator_Dispose_m3041529267(__this, method) ((  void (*) (Enumerator_t2484686602 *, const MethodInfo*))Enumerator_Dispose_m627360643_gshared)(__this, method)
