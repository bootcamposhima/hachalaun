﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Bullet
struct Bullet_t2000900386;
// UnityEngine.Collider
struct Collider_t2939674232;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Direction1041377119.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"

// System.Void Bullet::.ctor()
extern "C"  void Bullet__ctor_m4277156281 (Bullet_t2000900386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Bullet::get_Flyweight()
extern "C"  bool Bullet_get_Flyweight_m3218254373 (Bullet_t2000900386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Bullet::set_Flyweight(System.Boolean)
extern "C"  void Bullet_set_Flyweight_m1897660188 (Bullet_t2000900386 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Bullet::get_Stop()
extern "C"  bool Bullet_get_Stop_m3881172394 (Bullet_t2000900386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Bullet::set_Stop(System.Boolean)
extern "C"  void Bullet_set_Stop_m2962953569 (Bullet_t2000900386 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Bullet::get_technique()
extern "C"  float Bullet_get_technique_m1988930962 (Bullet_t2000900386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Bullet::set_direction(Direction)
extern "C"  void Bullet_set_direction_m2231111290 (Bullet_t2000900386 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Bullet::Shot()
extern "C"  void Bullet_Shot_m3418479845 (Bullet_t2000900386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Bullet::Move()
extern "C"  void Bullet_Move_m3253363708 (Bullet_t2000900386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Bullet::MoveDirection()
extern "C"  void Bullet_MoveDirection_m1124539397 (Bullet_t2000900386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Bullet::SetGameSpeed(System.Single)
extern "C"  void Bullet_SetGameSpeed_m279588749 (Bullet_t2000900386 * __this, float ___speed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Bullet::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void Bullet_OnTriggerEnter_m3148503135 (Bullet_t2000900386 * __this, Collider_t2939674232 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
