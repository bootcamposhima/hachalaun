﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// IDManager
struct IDManager_t1951742482;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SingletonMonoBehaviour`1<IDManager>
struct  SingletonMonoBehaviour_1_t1281917335  : public MonoBehaviour_t667441552
{
public:

public:
};

struct SingletonMonoBehaviour_1_t1281917335_StaticFields
{
public:
	// T SingletonMonoBehaviour`1::instance
	IDManager_t1951742482 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(SingletonMonoBehaviour_1_t1281917335_StaticFields, ___instance_2)); }
	inline IDManager_t1951742482 * get_instance_2() const { return ___instance_2; }
	inline IDManager_t1951742482 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(IDManager_t1951742482 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
