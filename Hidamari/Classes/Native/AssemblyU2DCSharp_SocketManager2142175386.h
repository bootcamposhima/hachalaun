﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// FightSocket
struct FightSocket_t1340804995;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SocketManager
struct  SocketManager_t2142175386  : public MonoBehaviour_t667441552
{
public:
	// FightSocket SocketManager::socket
	FightSocket_t1340804995 * ___socket_2;

public:
	inline static int32_t get_offset_of_socket_2() { return static_cast<int32_t>(offsetof(SocketManager_t2142175386, ___socket_2)); }
	inline FightSocket_t1340804995 * get_socket_2() const { return ___socket_2; }
	inline FightSocket_t1340804995 ** get_address_of_socket_2() { return &___socket_2; }
	inline void set_socket_2(FightSocket_t1340804995 * value)
	{
		___socket_2 = value;
		Il2CppCodeGenWriteBarrier(&___socket_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
