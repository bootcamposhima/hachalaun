﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SingletonMonoBehaviour`1<System.Object>
struct SingletonMonoBehaviour_1_t3500991224;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void SingletonMonoBehaviour`1<System.Object>::.ctor()
extern "C"  void SingletonMonoBehaviour_1__ctor_m2524367503_gshared (SingletonMonoBehaviour_1_t3500991224 * __this, const MethodInfo* method);
#define SingletonMonoBehaviour_1__ctor_m2524367503(__this, method) ((  void (*) (SingletonMonoBehaviour_1_t3500991224 *, const MethodInfo*))SingletonMonoBehaviour_1__ctor_m2524367503_gshared)(__this, method)
// T SingletonMonoBehaviour`1<System.Object>::get_Instance()
extern "C"  Il2CppObject * SingletonMonoBehaviour_1_get_Instance_m4273216084_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define SingletonMonoBehaviour_1_get_Instance_m4273216084(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SingletonMonoBehaviour_1_get_Instance_m4273216084_gshared)(__this /* static, unused */, method)
