﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SocketIO.Parser
struct Parser_t1660565463;
// SocketIO.SocketIOEvent
struct SocketIOEvent_t4011854063;
// JSONObject
struct JSONObject_t1752376903;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSONObject1752376903.h"

// System.Void SocketIO.Parser::.ctor()
extern "C"  void Parser__ctor_m3855138327 (Parser_t1660565463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SocketIO.SocketIOEvent SocketIO.Parser::Parse(JSONObject)
extern "C"  SocketIOEvent_t4011854063 * Parser_Parse_m196541778 (Parser_t1660565463 * __this, JSONObject_t1752376903 * ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
