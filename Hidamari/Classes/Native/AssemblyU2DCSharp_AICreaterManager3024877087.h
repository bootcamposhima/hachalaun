﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.Camera
struct Camera_t2727095145;
// AICreaterAction
struct AICreaterAction_t2388459908;
// Factory
struct Factory_t572770538;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_AIDatas4008896193.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AICreaterManager
struct  AICreaterManager_t3024877087  : public MonoBehaviour_t667441552
{
public:
	// AIDatas AICreaterManager::_aidatas
	AIDatas_t4008896193  ____aidatas_2;
	// UnityEngine.GameObject AICreaterManager::_aiscroll
	GameObject_t3674682005 * ____aiscroll_3;
	// UnityEngine.GameObject AICreaterManager::_bulletscroll
	GameObject_t3674682005 * ____bulletscroll_4;
	// UnityEngine.GameObject AICreaterManager::_charactorscroll
	GameObject_t3674682005 * ____charactorscroll_5;
	// UnityEngine.GameObject AICreaterManager::_addbutton
	GameObject_t3674682005 * ____addbutton_6;
	// UnityEngine.GameObject AICreaterManager::_panels
	GameObject_t3674682005 * ____panels_7;
	// UnityEngine.GameObject AICreaterManager::_arrows
	GameObject_t3674682005 * ____arrows_8;
	// UnityEngine.Camera AICreaterManager::_maincamera
	Camera_t2727095145 * ____maincamera_9;
	// UnityEngine.GameObject AICreaterManager::_directionobject
	GameObject_t3674682005 * ____directionobject_10;
	// UnityEngine.GameObject AICreaterManager::_node
	GameObject_t3674682005 * ____node_11;
	// AICreaterAction AICreaterManager::_aicreateaction
	AICreaterAction_t2388459908 * ____aicreateaction_12;
	// System.Int32 AICreaterManager::_direction
	int32_t ____direction_13;
	// System.Int32 AICreaterManager::_charactornum
	int32_t ____charactornum_14;
	// Factory AICreaterManager::_fc
	Factory_t572770538 * ____fc_15;
	// System.Int32 AICreaterManager::_topainum
	int32_t ____topainum_16;
	// System.Int32 AICreaterManager::_topbulletnum
	int32_t ____topbulletnum_17;
	// System.Int32 AICreaterManager::_toppositionnum
	int32_t ____toppositionnum_18;
	// System.Boolean AICreaterManager::_formation
	bool ____formation_19;
	// System.Int32 AICreaterManager::_count
	int32_t ____count_20;
	// System.Boolean AICreaterManager::_istop
	bool ____istop_21;
	// UnityEngine.GameObject AICreaterManager::_north
	GameObject_t3674682005 * ____north_22;
	// UnityEngine.GameObject AICreaterManager::_south
	GameObject_t3674682005 * ____south_23;
	// UnityEngine.GameObject AICreaterManager::_east
	GameObject_t3674682005 * ____east_24;
	// UnityEngine.GameObject AICreaterManager::_west
	GameObject_t3674682005 * ____west_25;

public:
	inline static int32_t get_offset_of__aidatas_2() { return static_cast<int32_t>(offsetof(AICreaterManager_t3024877087, ____aidatas_2)); }
	inline AIDatas_t4008896193  get__aidatas_2() const { return ____aidatas_2; }
	inline AIDatas_t4008896193 * get_address_of__aidatas_2() { return &____aidatas_2; }
	inline void set__aidatas_2(AIDatas_t4008896193  value)
	{
		____aidatas_2 = value;
	}

	inline static int32_t get_offset_of__aiscroll_3() { return static_cast<int32_t>(offsetof(AICreaterManager_t3024877087, ____aiscroll_3)); }
	inline GameObject_t3674682005 * get__aiscroll_3() const { return ____aiscroll_3; }
	inline GameObject_t3674682005 ** get_address_of__aiscroll_3() { return &____aiscroll_3; }
	inline void set__aiscroll_3(GameObject_t3674682005 * value)
	{
		____aiscroll_3 = value;
		Il2CppCodeGenWriteBarrier(&____aiscroll_3, value);
	}

	inline static int32_t get_offset_of__bulletscroll_4() { return static_cast<int32_t>(offsetof(AICreaterManager_t3024877087, ____bulletscroll_4)); }
	inline GameObject_t3674682005 * get__bulletscroll_4() const { return ____bulletscroll_4; }
	inline GameObject_t3674682005 ** get_address_of__bulletscroll_4() { return &____bulletscroll_4; }
	inline void set__bulletscroll_4(GameObject_t3674682005 * value)
	{
		____bulletscroll_4 = value;
		Il2CppCodeGenWriteBarrier(&____bulletscroll_4, value);
	}

	inline static int32_t get_offset_of__charactorscroll_5() { return static_cast<int32_t>(offsetof(AICreaterManager_t3024877087, ____charactorscroll_5)); }
	inline GameObject_t3674682005 * get__charactorscroll_5() const { return ____charactorscroll_5; }
	inline GameObject_t3674682005 ** get_address_of__charactorscroll_5() { return &____charactorscroll_5; }
	inline void set__charactorscroll_5(GameObject_t3674682005 * value)
	{
		____charactorscroll_5 = value;
		Il2CppCodeGenWriteBarrier(&____charactorscroll_5, value);
	}

	inline static int32_t get_offset_of__addbutton_6() { return static_cast<int32_t>(offsetof(AICreaterManager_t3024877087, ____addbutton_6)); }
	inline GameObject_t3674682005 * get__addbutton_6() const { return ____addbutton_6; }
	inline GameObject_t3674682005 ** get_address_of__addbutton_6() { return &____addbutton_6; }
	inline void set__addbutton_6(GameObject_t3674682005 * value)
	{
		____addbutton_6 = value;
		Il2CppCodeGenWriteBarrier(&____addbutton_6, value);
	}

	inline static int32_t get_offset_of__panels_7() { return static_cast<int32_t>(offsetof(AICreaterManager_t3024877087, ____panels_7)); }
	inline GameObject_t3674682005 * get__panels_7() const { return ____panels_7; }
	inline GameObject_t3674682005 ** get_address_of__panels_7() { return &____panels_7; }
	inline void set__panels_7(GameObject_t3674682005 * value)
	{
		____panels_7 = value;
		Il2CppCodeGenWriteBarrier(&____panels_7, value);
	}

	inline static int32_t get_offset_of__arrows_8() { return static_cast<int32_t>(offsetof(AICreaterManager_t3024877087, ____arrows_8)); }
	inline GameObject_t3674682005 * get__arrows_8() const { return ____arrows_8; }
	inline GameObject_t3674682005 ** get_address_of__arrows_8() { return &____arrows_8; }
	inline void set__arrows_8(GameObject_t3674682005 * value)
	{
		____arrows_8 = value;
		Il2CppCodeGenWriteBarrier(&____arrows_8, value);
	}

	inline static int32_t get_offset_of__maincamera_9() { return static_cast<int32_t>(offsetof(AICreaterManager_t3024877087, ____maincamera_9)); }
	inline Camera_t2727095145 * get__maincamera_9() const { return ____maincamera_9; }
	inline Camera_t2727095145 ** get_address_of__maincamera_9() { return &____maincamera_9; }
	inline void set__maincamera_9(Camera_t2727095145 * value)
	{
		____maincamera_9 = value;
		Il2CppCodeGenWriteBarrier(&____maincamera_9, value);
	}

	inline static int32_t get_offset_of__directionobject_10() { return static_cast<int32_t>(offsetof(AICreaterManager_t3024877087, ____directionobject_10)); }
	inline GameObject_t3674682005 * get__directionobject_10() const { return ____directionobject_10; }
	inline GameObject_t3674682005 ** get_address_of__directionobject_10() { return &____directionobject_10; }
	inline void set__directionobject_10(GameObject_t3674682005 * value)
	{
		____directionobject_10 = value;
		Il2CppCodeGenWriteBarrier(&____directionobject_10, value);
	}

	inline static int32_t get_offset_of__node_11() { return static_cast<int32_t>(offsetof(AICreaterManager_t3024877087, ____node_11)); }
	inline GameObject_t3674682005 * get__node_11() const { return ____node_11; }
	inline GameObject_t3674682005 ** get_address_of__node_11() { return &____node_11; }
	inline void set__node_11(GameObject_t3674682005 * value)
	{
		____node_11 = value;
		Il2CppCodeGenWriteBarrier(&____node_11, value);
	}

	inline static int32_t get_offset_of__aicreateaction_12() { return static_cast<int32_t>(offsetof(AICreaterManager_t3024877087, ____aicreateaction_12)); }
	inline AICreaterAction_t2388459908 * get__aicreateaction_12() const { return ____aicreateaction_12; }
	inline AICreaterAction_t2388459908 ** get_address_of__aicreateaction_12() { return &____aicreateaction_12; }
	inline void set__aicreateaction_12(AICreaterAction_t2388459908 * value)
	{
		____aicreateaction_12 = value;
		Il2CppCodeGenWriteBarrier(&____aicreateaction_12, value);
	}

	inline static int32_t get_offset_of__direction_13() { return static_cast<int32_t>(offsetof(AICreaterManager_t3024877087, ____direction_13)); }
	inline int32_t get__direction_13() const { return ____direction_13; }
	inline int32_t* get_address_of__direction_13() { return &____direction_13; }
	inline void set__direction_13(int32_t value)
	{
		____direction_13 = value;
	}

	inline static int32_t get_offset_of__charactornum_14() { return static_cast<int32_t>(offsetof(AICreaterManager_t3024877087, ____charactornum_14)); }
	inline int32_t get__charactornum_14() const { return ____charactornum_14; }
	inline int32_t* get_address_of__charactornum_14() { return &____charactornum_14; }
	inline void set__charactornum_14(int32_t value)
	{
		____charactornum_14 = value;
	}

	inline static int32_t get_offset_of__fc_15() { return static_cast<int32_t>(offsetof(AICreaterManager_t3024877087, ____fc_15)); }
	inline Factory_t572770538 * get__fc_15() const { return ____fc_15; }
	inline Factory_t572770538 ** get_address_of__fc_15() { return &____fc_15; }
	inline void set__fc_15(Factory_t572770538 * value)
	{
		____fc_15 = value;
		Il2CppCodeGenWriteBarrier(&____fc_15, value);
	}

	inline static int32_t get_offset_of__topainum_16() { return static_cast<int32_t>(offsetof(AICreaterManager_t3024877087, ____topainum_16)); }
	inline int32_t get__topainum_16() const { return ____topainum_16; }
	inline int32_t* get_address_of__topainum_16() { return &____topainum_16; }
	inline void set__topainum_16(int32_t value)
	{
		____topainum_16 = value;
	}

	inline static int32_t get_offset_of__topbulletnum_17() { return static_cast<int32_t>(offsetof(AICreaterManager_t3024877087, ____topbulletnum_17)); }
	inline int32_t get__topbulletnum_17() const { return ____topbulletnum_17; }
	inline int32_t* get_address_of__topbulletnum_17() { return &____topbulletnum_17; }
	inline void set__topbulletnum_17(int32_t value)
	{
		____topbulletnum_17 = value;
	}

	inline static int32_t get_offset_of__toppositionnum_18() { return static_cast<int32_t>(offsetof(AICreaterManager_t3024877087, ____toppositionnum_18)); }
	inline int32_t get__toppositionnum_18() const { return ____toppositionnum_18; }
	inline int32_t* get_address_of__toppositionnum_18() { return &____toppositionnum_18; }
	inline void set__toppositionnum_18(int32_t value)
	{
		____toppositionnum_18 = value;
	}

	inline static int32_t get_offset_of__formation_19() { return static_cast<int32_t>(offsetof(AICreaterManager_t3024877087, ____formation_19)); }
	inline bool get__formation_19() const { return ____formation_19; }
	inline bool* get_address_of__formation_19() { return &____formation_19; }
	inline void set__formation_19(bool value)
	{
		____formation_19 = value;
	}

	inline static int32_t get_offset_of__count_20() { return static_cast<int32_t>(offsetof(AICreaterManager_t3024877087, ____count_20)); }
	inline int32_t get__count_20() const { return ____count_20; }
	inline int32_t* get_address_of__count_20() { return &____count_20; }
	inline void set__count_20(int32_t value)
	{
		____count_20 = value;
	}

	inline static int32_t get_offset_of__istop_21() { return static_cast<int32_t>(offsetof(AICreaterManager_t3024877087, ____istop_21)); }
	inline bool get__istop_21() const { return ____istop_21; }
	inline bool* get_address_of__istop_21() { return &____istop_21; }
	inline void set__istop_21(bool value)
	{
		____istop_21 = value;
	}

	inline static int32_t get_offset_of__north_22() { return static_cast<int32_t>(offsetof(AICreaterManager_t3024877087, ____north_22)); }
	inline GameObject_t3674682005 * get__north_22() const { return ____north_22; }
	inline GameObject_t3674682005 ** get_address_of__north_22() { return &____north_22; }
	inline void set__north_22(GameObject_t3674682005 * value)
	{
		____north_22 = value;
		Il2CppCodeGenWriteBarrier(&____north_22, value);
	}

	inline static int32_t get_offset_of__south_23() { return static_cast<int32_t>(offsetof(AICreaterManager_t3024877087, ____south_23)); }
	inline GameObject_t3674682005 * get__south_23() const { return ____south_23; }
	inline GameObject_t3674682005 ** get_address_of__south_23() { return &____south_23; }
	inline void set__south_23(GameObject_t3674682005 * value)
	{
		____south_23 = value;
		Il2CppCodeGenWriteBarrier(&____south_23, value);
	}

	inline static int32_t get_offset_of__east_24() { return static_cast<int32_t>(offsetof(AICreaterManager_t3024877087, ____east_24)); }
	inline GameObject_t3674682005 * get__east_24() const { return ____east_24; }
	inline GameObject_t3674682005 ** get_address_of__east_24() { return &____east_24; }
	inline void set__east_24(GameObject_t3674682005 * value)
	{
		____east_24 = value;
		Il2CppCodeGenWriteBarrier(&____east_24, value);
	}

	inline static int32_t get_offset_of__west_25() { return static_cast<int32_t>(offsetof(AICreaterManager_t3024877087, ____west_25)); }
	inline GameObject_t3674682005 * get__west_25() const { return ____west_25; }
	inline GameObject_t3674682005 ** get_address_of__west_25() { return &____west_25; }
	inline void set__west_25(GameObject_t3674682005 * value)
	{
		____west_25 = value;
		Il2CppCodeGenWriteBarrier(&____west_25, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
