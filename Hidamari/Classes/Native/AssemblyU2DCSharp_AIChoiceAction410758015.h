﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// TransitionAnimation
struct TransitionAnimation_t4286760335;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AIChoiceAction
struct  AIChoiceAction_t410758015  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject AIChoiceAction::_createAIbutton
	GameObject_t3674682005 * ____createAIbutton_2;
	// UnityEngine.GameObject AIChoiceAction::_editingbutton
	GameObject_t3674682005 * ____editingbutton_3;
	// UnityEngine.GameObject AIChoiceAction::_east
	GameObject_t3674682005 * ____east_4;
	// UnityEngine.GameObject AIChoiceAction::_weat
	GameObject_t3674682005 * ____weat_5;
	// UnityEngine.GameObject AIChoiceAction::_south
	GameObject_t3674682005 * ____south_6;
	// UnityEngine.GameObject AIChoiceAction::_north
	GameObject_t3674682005 * ____north_7;
	// TransitionAnimation AIChoiceAction::_ta
	TransitionAnimation_t4286760335 * ____ta_8;
	// System.Int32 AIChoiceAction::_topnum
	int32_t ____topnum_9;

public:
	inline static int32_t get_offset_of__createAIbutton_2() { return static_cast<int32_t>(offsetof(AIChoiceAction_t410758015, ____createAIbutton_2)); }
	inline GameObject_t3674682005 * get__createAIbutton_2() const { return ____createAIbutton_2; }
	inline GameObject_t3674682005 ** get_address_of__createAIbutton_2() { return &____createAIbutton_2; }
	inline void set__createAIbutton_2(GameObject_t3674682005 * value)
	{
		____createAIbutton_2 = value;
		Il2CppCodeGenWriteBarrier(&____createAIbutton_2, value);
	}

	inline static int32_t get_offset_of__editingbutton_3() { return static_cast<int32_t>(offsetof(AIChoiceAction_t410758015, ____editingbutton_3)); }
	inline GameObject_t3674682005 * get__editingbutton_3() const { return ____editingbutton_3; }
	inline GameObject_t3674682005 ** get_address_of__editingbutton_3() { return &____editingbutton_3; }
	inline void set__editingbutton_3(GameObject_t3674682005 * value)
	{
		____editingbutton_3 = value;
		Il2CppCodeGenWriteBarrier(&____editingbutton_3, value);
	}

	inline static int32_t get_offset_of__east_4() { return static_cast<int32_t>(offsetof(AIChoiceAction_t410758015, ____east_4)); }
	inline GameObject_t3674682005 * get__east_4() const { return ____east_4; }
	inline GameObject_t3674682005 ** get_address_of__east_4() { return &____east_4; }
	inline void set__east_4(GameObject_t3674682005 * value)
	{
		____east_4 = value;
		Il2CppCodeGenWriteBarrier(&____east_4, value);
	}

	inline static int32_t get_offset_of__weat_5() { return static_cast<int32_t>(offsetof(AIChoiceAction_t410758015, ____weat_5)); }
	inline GameObject_t3674682005 * get__weat_5() const { return ____weat_5; }
	inline GameObject_t3674682005 ** get_address_of__weat_5() { return &____weat_5; }
	inline void set__weat_5(GameObject_t3674682005 * value)
	{
		____weat_5 = value;
		Il2CppCodeGenWriteBarrier(&____weat_5, value);
	}

	inline static int32_t get_offset_of__south_6() { return static_cast<int32_t>(offsetof(AIChoiceAction_t410758015, ____south_6)); }
	inline GameObject_t3674682005 * get__south_6() const { return ____south_6; }
	inline GameObject_t3674682005 ** get_address_of__south_6() { return &____south_6; }
	inline void set__south_6(GameObject_t3674682005 * value)
	{
		____south_6 = value;
		Il2CppCodeGenWriteBarrier(&____south_6, value);
	}

	inline static int32_t get_offset_of__north_7() { return static_cast<int32_t>(offsetof(AIChoiceAction_t410758015, ____north_7)); }
	inline GameObject_t3674682005 * get__north_7() const { return ____north_7; }
	inline GameObject_t3674682005 ** get_address_of__north_7() { return &____north_7; }
	inline void set__north_7(GameObject_t3674682005 * value)
	{
		____north_7 = value;
		Il2CppCodeGenWriteBarrier(&____north_7, value);
	}

	inline static int32_t get_offset_of__ta_8() { return static_cast<int32_t>(offsetof(AIChoiceAction_t410758015, ____ta_8)); }
	inline TransitionAnimation_t4286760335 * get__ta_8() const { return ____ta_8; }
	inline TransitionAnimation_t4286760335 ** get_address_of__ta_8() { return &____ta_8; }
	inline void set__ta_8(TransitionAnimation_t4286760335 * value)
	{
		____ta_8 = value;
		Il2CppCodeGenWriteBarrier(&____ta_8, value);
	}

	inline static int32_t get_offset_of__topnum_9() { return static_cast<int32_t>(offsetof(AIChoiceAction_t410758015, ____topnum_9)); }
	inline int32_t get__topnum_9() const { return ____topnum_9; }
	inline int32_t* get_address_of__topnum_9() { return &____topnum_9; }
	inline void set__topnum_9(int32_t value)
	{
		____topnum_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
