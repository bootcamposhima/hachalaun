﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SearchMoveTask
struct SearchMoveTask_t1050018526;
// BlackBoard
struct BlackBoard_t328561223;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BlackBoard328561223.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void SearchMoveTask::.ctor()
extern "C"  void SearchMoveTask__ctor_m2597462909 (SearchMoveTask_t1050018526 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SearchMoveTask::Start()
extern "C"  void SearchMoveTask_Start_m1544600701 (SearchMoveTask_t1050018526 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SearchMoveTask::Update()
extern "C"  void SearchMoveTask_Update_m643833648 (SearchMoveTask_t1050018526 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SearchMoveTask::PlayTask(BlackBoard)
extern "C"  bool SearchMoveTask_PlayTask_m1961227853 (SearchMoveTask_t1050018526 * __this, BlackBoard_t328561223 * ___blackboard0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SearchMoveTask::FlagJude(System.Int32,System.Int32)
extern "C"  bool SearchMoveTask_FlagJude_m3872925459 (SearchMoveTask_t1050018526 * __this, int32_t ___flag10, int32_t ___flag21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SearchMoveTask::getDnm(UnityEngine.Vector3,System.Int32&,System.Int32&)
extern "C"  void SearchMoveTask_getDnm_m3650684303 (SearchMoveTask_t1050018526 * __this, Vector3_t4282066566  ___pos0, int32_t* ___center1, int32_t* ___dnm2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SearchMoveTask::PlayerRotatin(System.Int32,BlackBoard)
extern "C"  void SearchMoveTask_PlayerRotatin_m4019265253 (SearchMoveTask_t1050018526 * __this, int32_t ___angle0, BlackBoard_t328561223 * ___blackboard1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
