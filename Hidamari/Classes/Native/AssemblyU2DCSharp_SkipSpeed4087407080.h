﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.Sprite
struct Sprite_t3199167241;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SkipSpeed
struct  SkipSpeed_t4087407080  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject SkipSpeed::playerEnemy
	GameObject_t3674682005 * ___playerEnemy_2;
	// UnityEngine.GameObject SkipSpeed::defenceManager
	GameObject_t3674682005 * ___defenceManager_3;
	// UnityEngine.Sprite SkipSpeed::sp1
	Sprite_t3199167241 * ___sp1_4;
	// UnityEngine.Sprite SkipSpeed::sp2
	Sprite_t3199167241 * ___sp2_5;
	// System.Int32 SkipSpeed::speed
	int32_t ___speed_6;

public:
	inline static int32_t get_offset_of_playerEnemy_2() { return static_cast<int32_t>(offsetof(SkipSpeed_t4087407080, ___playerEnemy_2)); }
	inline GameObject_t3674682005 * get_playerEnemy_2() const { return ___playerEnemy_2; }
	inline GameObject_t3674682005 ** get_address_of_playerEnemy_2() { return &___playerEnemy_2; }
	inline void set_playerEnemy_2(GameObject_t3674682005 * value)
	{
		___playerEnemy_2 = value;
		Il2CppCodeGenWriteBarrier(&___playerEnemy_2, value);
	}

	inline static int32_t get_offset_of_defenceManager_3() { return static_cast<int32_t>(offsetof(SkipSpeed_t4087407080, ___defenceManager_3)); }
	inline GameObject_t3674682005 * get_defenceManager_3() const { return ___defenceManager_3; }
	inline GameObject_t3674682005 ** get_address_of_defenceManager_3() { return &___defenceManager_3; }
	inline void set_defenceManager_3(GameObject_t3674682005 * value)
	{
		___defenceManager_3 = value;
		Il2CppCodeGenWriteBarrier(&___defenceManager_3, value);
	}

	inline static int32_t get_offset_of_sp1_4() { return static_cast<int32_t>(offsetof(SkipSpeed_t4087407080, ___sp1_4)); }
	inline Sprite_t3199167241 * get_sp1_4() const { return ___sp1_4; }
	inline Sprite_t3199167241 ** get_address_of_sp1_4() { return &___sp1_4; }
	inline void set_sp1_4(Sprite_t3199167241 * value)
	{
		___sp1_4 = value;
		Il2CppCodeGenWriteBarrier(&___sp1_4, value);
	}

	inline static int32_t get_offset_of_sp2_5() { return static_cast<int32_t>(offsetof(SkipSpeed_t4087407080, ___sp2_5)); }
	inline Sprite_t3199167241 * get_sp2_5() const { return ___sp2_5; }
	inline Sprite_t3199167241 ** get_address_of_sp2_5() { return &___sp2_5; }
	inline void set_sp2_5(Sprite_t3199167241 * value)
	{
		___sp2_5 = value;
		Il2CppCodeGenWriteBarrier(&___sp2_5, value);
	}

	inline static int32_t get_offset_of_speed_6() { return static_cast<int32_t>(offsetof(SkipSpeed_t4087407080, ___speed_6)); }
	inline int32_t get_speed_6() const { return ___speed_6; }
	inline int32_t* get_address_of_speed_6() { return &___speed_6; }
	inline void set_speed_6(int32_t value)
	{
		___speed_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
