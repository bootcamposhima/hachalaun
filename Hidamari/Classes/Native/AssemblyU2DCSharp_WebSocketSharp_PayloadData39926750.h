﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.PayloadData
struct  PayloadData_t39926750  : public Il2CppObject
{
public:
	// System.Byte[] WebSocketSharp.PayloadData::_applicationData
	ByteU5BU5D_t4260760469* ____applicationData_1;
	// System.Byte[] WebSocketSharp.PayloadData::_extensionData
	ByteU5BU5D_t4260760469* ____extensionData_2;
	// System.Boolean WebSocketSharp.PayloadData::_masked
	bool ____masked_3;

public:
	inline static int32_t get_offset_of__applicationData_1() { return static_cast<int32_t>(offsetof(PayloadData_t39926750, ____applicationData_1)); }
	inline ByteU5BU5D_t4260760469* get__applicationData_1() const { return ____applicationData_1; }
	inline ByteU5BU5D_t4260760469** get_address_of__applicationData_1() { return &____applicationData_1; }
	inline void set__applicationData_1(ByteU5BU5D_t4260760469* value)
	{
		____applicationData_1 = value;
		Il2CppCodeGenWriteBarrier(&____applicationData_1, value);
	}

	inline static int32_t get_offset_of__extensionData_2() { return static_cast<int32_t>(offsetof(PayloadData_t39926750, ____extensionData_2)); }
	inline ByteU5BU5D_t4260760469* get__extensionData_2() const { return ____extensionData_2; }
	inline ByteU5BU5D_t4260760469** get_address_of__extensionData_2() { return &____extensionData_2; }
	inline void set__extensionData_2(ByteU5BU5D_t4260760469* value)
	{
		____extensionData_2 = value;
		Il2CppCodeGenWriteBarrier(&____extensionData_2, value);
	}

	inline static int32_t get_offset_of__masked_3() { return static_cast<int32_t>(offsetof(PayloadData_t39926750, ____masked_3)); }
	inline bool get__masked_3() const { return ____masked_3; }
	inline bool* get_address_of__masked_3() { return &____masked_3; }
	inline void set__masked_3(bool value)
	{
		____masked_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
