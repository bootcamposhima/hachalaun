﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MatchingManager/<MatingTimeAction>c__Iterator11
struct U3CMatingTimeActionU3Ec__Iterator11_t3526127701;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MatchingManager/<MatingTimeAction>c__Iterator11::.ctor()
extern "C"  void U3CMatingTimeActionU3Ec__Iterator11__ctor_m107366070 (U3CMatingTimeActionU3Ec__Iterator11_t3526127701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MatchingManager/<MatingTimeAction>c__Iterator11::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMatingTimeActionU3Ec__Iterator11_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1404255324 (U3CMatingTimeActionU3Ec__Iterator11_t3526127701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MatchingManager/<MatingTimeAction>c__Iterator11::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMatingTimeActionU3Ec__Iterator11_System_Collections_IEnumerator_get_Current_m3108075504 (U3CMatingTimeActionU3Ec__Iterator11_t3526127701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MatchingManager/<MatingTimeAction>c__Iterator11::MoveNext()
extern "C"  bool U3CMatingTimeActionU3Ec__Iterator11_MoveNext_m3093019518 (U3CMatingTimeActionU3Ec__Iterator11_t3526127701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MatchingManager/<MatingTimeAction>c__Iterator11::Dispose()
extern "C"  void U3CMatingTimeActionU3Ec__Iterator11_Dispose_m4292828531 (U3CMatingTimeActionU3Ec__Iterator11_t3526127701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MatchingManager/<MatingTimeAction>c__Iterator11::Reset()
extern "C"  void U3CMatingTimeActionU3Ec__Iterator11_Reset_m2048766307 (U3CMatingTimeActionU3Ec__Iterator11_t3526127701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
