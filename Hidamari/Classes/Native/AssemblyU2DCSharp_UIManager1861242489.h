﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIManager
struct  UIManager_t1861242489  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Vector3 UIManager::_firstposbuttonpos
	Vector3_t4282066566  ____firstposbuttonpos_2;
	// UnityEngine.Vector3 UIManager::_secoundbuttonpos
	Vector3_t4282066566  ____secoundbuttonpos_3;
	// UnityEngine.Vector3 UIManager::_destination
	Vector3_t4282066566  ____destination_4;
	// System.Boolean UIManager::_ismove
	bool ____ismove_5;
	// System.Single UIManager::_time
	float ____time_6;

public:
	inline static int32_t get_offset_of__firstposbuttonpos_2() { return static_cast<int32_t>(offsetof(UIManager_t1861242489, ____firstposbuttonpos_2)); }
	inline Vector3_t4282066566  get__firstposbuttonpos_2() const { return ____firstposbuttonpos_2; }
	inline Vector3_t4282066566 * get_address_of__firstposbuttonpos_2() { return &____firstposbuttonpos_2; }
	inline void set__firstposbuttonpos_2(Vector3_t4282066566  value)
	{
		____firstposbuttonpos_2 = value;
	}

	inline static int32_t get_offset_of__secoundbuttonpos_3() { return static_cast<int32_t>(offsetof(UIManager_t1861242489, ____secoundbuttonpos_3)); }
	inline Vector3_t4282066566  get__secoundbuttonpos_3() const { return ____secoundbuttonpos_3; }
	inline Vector3_t4282066566 * get_address_of__secoundbuttonpos_3() { return &____secoundbuttonpos_3; }
	inline void set__secoundbuttonpos_3(Vector3_t4282066566  value)
	{
		____secoundbuttonpos_3 = value;
	}

	inline static int32_t get_offset_of__destination_4() { return static_cast<int32_t>(offsetof(UIManager_t1861242489, ____destination_4)); }
	inline Vector3_t4282066566  get__destination_4() const { return ____destination_4; }
	inline Vector3_t4282066566 * get_address_of__destination_4() { return &____destination_4; }
	inline void set__destination_4(Vector3_t4282066566  value)
	{
		____destination_4 = value;
	}

	inline static int32_t get_offset_of__ismove_5() { return static_cast<int32_t>(offsetof(UIManager_t1861242489, ____ismove_5)); }
	inline bool get__ismove_5() const { return ____ismove_5; }
	inline bool* get_address_of__ismove_5() { return &____ismove_5; }
	inline void set__ismove_5(bool value)
	{
		____ismove_5 = value;
	}

	inline static int32_t get_offset_of__time_6() { return static_cast<int32_t>(offsetof(UIManager_t1861242489, ____time_6)); }
	inline float get__time_6() const { return ____time_6; }
	inline float* get_address_of__time_6() { return &____time_6; }
	inline void set__time_6(float value)
	{
		____time_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
