﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_InteropServices_DispIdAttr3541616345.h"
#include "mscorlib_System_Runtime_InteropServices_ErrorWrapp3505718805.h"
#include "mscorlib_System_Runtime_InteropServices_ExternalEx3159215326.h"
#include "mscorlib_System_Runtime_InteropServices_GCHandle1812538030.h"
#include "mscorlib_System_Runtime_InteropServices_GCHandleTy1621876744.h"
#include "mscorlib_System_Runtime_InteropServices_InterfaceTy986726003.h"
#include "mscorlib_System_Runtime_InteropServices_Marshal87536056.h"
#include "mscorlib_System_Runtime_InteropServices_MarshalDir1970812184.h"
#include "mscorlib_System_Runtime_InteropServices_PreserveSi1347954145.h"
#include "mscorlib_System_Runtime_InteropServices_SafeHandle3340575423.h"
#include "mscorlib_System_Runtime_InteropServices_TypeLibImp4035097262.h"
#include "mscorlib_System_Runtime_InteropServices_TypeLibVer2572937673.h"
#include "mscorlib_System_Runtime_InteropServices_UnmanagedF1462594039.h"
#include "mscorlib_System_Runtime_InteropServices_UnmanagedTyp96484186.h"
#include "mscorlib_System_Runtime_Remoting_Activation_Activa1948190490.h"
#include "mscorlib_System_Runtime_Remoting_Activation_AppDoma184966104.h"
#include "mscorlib_System_Runtime_Remoting_Activation_Constru763282600.h"
#include "mscorlib_System_Runtime_Remoting_Activation_Contex4263139650.h"
#include "mscorlib_System_Runtime_Remoting_Activation_Remote1251920305.h"
#include "mscorlib_System_Runtime_Remoting_Activation_UrlAtt2914013939.h"
#include "mscorlib_System_Runtime_Remoting_ChannelInfo140347619.h"
#include "mscorlib_System_Runtime_Remoting_Channels_ChannelS3172382875.h"
#include "mscorlib_System_Runtime_Remoting_Channels_CrossApp4210664921.h"
#include "mscorlib_System_Runtime_Remoting_Channels_CrossApp4294171512.h"
#include "mscorlib_System_Runtime_Remoting_Channels_CrossApp4211119298.h"
#include "mscorlib_System_Runtime_Remoting_Channels_SinkProvi794892312.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_Context515654137.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_DynamicP3791941180.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_DynamicPr469376913.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_ContextCa781636285.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_ContextAt728408415.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_CrossCon2792143614.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_Synchroniz69789646.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_Synchroni994676909.h"
#include "mscorlib_System_Runtime_Remoting_Contexts_Synchron3512531509.h"
#include "mscorlib_System_Runtime_Remoting_Lifetime_Lease4190051248.h"
#include "mscorlib_System_Runtime_Remoting_Lifetime_Lease_Re1379218617.h"
#include "mscorlib_System_Runtime_Remoting_Lifetime_LeaseMan2601135431.h"
#include "mscorlib_System_Runtime_Remoting_Lifetime_LeaseSin1781043427.h"
#include "mscorlib_System_Runtime_Remoting_Lifetime_LeaseSta4015278955.h"
#include "mscorlib_System_Runtime_Remoting_Lifetime_LifetimeS590691611.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_ArgInfo4189099640.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_ArgInfo185197854.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_AsyncRe4124112563.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_ClientC1503526936.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_Constru2368686409.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_Construc743124799.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_EnvoyTe4146359051.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_Header1689611527.h"
#include "mscorlib_System_Runtime_Remoting_Messaging_Logical3949483042.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize400 = { sizeof (DispIdAttribute_t3541616345), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable400[1] = 
{
	DispIdAttribute_t3541616345::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize401 = { sizeof (ErrorWrapper_t3505718805), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable401[1] = 
{
	ErrorWrapper_t3505718805::get_offset_of_errorCode_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize402 = { sizeof (ExternalException_t3159215326), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize403 = { sizeof (GCHandle_t1812538030)+ sizeof (Il2CppObject), sizeof(GCHandle_t1812538030_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable403[1] = 
{
	GCHandle_t1812538030::get_offset_of_handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize404 = { sizeof (GCHandleType_t1621876744)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable404[5] = 
{
	GCHandleType_t1621876744::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize405 = { sizeof (InterfaceTypeAttribute_t986726003), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable405[1] = 
{
	InterfaceTypeAttribute_t986726003::get_offset_of_intType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize406 = { sizeof (Marshal_t87536056), -1, sizeof(Marshal_t87536056_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable406[2] = 
{
	Marshal_t87536056_StaticFields::get_offset_of_SystemMaxDBCSCharSize_0(),
	Marshal_t87536056_StaticFields::get_offset_of_SystemDefaultCharSize_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize407 = { sizeof (MarshalDirectiveException_t1970812184), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable407[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize408 = { sizeof (PreserveSigAttribute_t1347954145), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize409 = { sizeof (SafeHandle_t3340575423), sizeof(void*), 0, 0 };
extern const int32_t g_FieldOffsetTable409[4] = 
{
	SafeHandle_t3340575423::get_offset_of_handle_0(),
	SafeHandle_t3340575423::get_offset_of_invalid_handle_value_1(),
	SafeHandle_t3340575423::get_offset_of_refcount_2(),
	SafeHandle_t3340575423::get_offset_of_owns_handle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize410 = { sizeof (TypeLibImportClassAttribute_t4035097262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable410[1] = 
{
	TypeLibImportClassAttribute_t4035097262::get_offset_of__importClass_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize411 = { sizeof (TypeLibVersionAttribute_t2572937673), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable411[2] = 
{
	TypeLibVersionAttribute_t2572937673::get_offset_of_major_0(),
	TypeLibVersionAttribute_t2572937673::get_offset_of_minor_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize412 = { sizeof (UnmanagedFunctionPointerAttribute_t1462594039), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable412[1] = 
{
	UnmanagedFunctionPointerAttribute_t1462594039::get_offset_of_call_conv_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize413 = { sizeof (UnmanagedType_t96484186)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable413[36] = 
{
	UnmanagedType_t96484186::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize414 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize415 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize416 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize417 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize418 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize419 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize420 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize421 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize422 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize423 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize424 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize425 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize426 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize427 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize428 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize429 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize430 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize431 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize432 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize433 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize434 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize435 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize436 = { sizeof (ActivationServices_t1948190490), -1, sizeof(ActivationServices_t1948190490_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable436[1] = 
{
	ActivationServices_t1948190490_StaticFields::get_offset_of__constructionActivator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize437 = { sizeof (AppDomainLevelActivator_t184966104), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable437[2] = 
{
	AppDomainLevelActivator_t184966104::get_offset_of__activationUrl_0(),
	AppDomainLevelActivator_t184966104::get_offset_of__next_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize438 = { sizeof (ConstructionLevelActivator_t763282600), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize439 = { sizeof (ContextLevelActivator_t4263139650), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable439[1] = 
{
	ContextLevelActivator_t4263139650::get_offset_of_m_NextActivator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize440 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize441 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize442 = { sizeof (RemoteActivator_t1251920305), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize443 = { sizeof (UrlAttribute_t2914013939), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable443[1] = 
{
	UrlAttribute_t2914013939::get_offset_of_url_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize444 = { sizeof (ChannelInfo_t140347619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable444[1] = 
{
	ChannelInfo_t140347619::get_offset_of_channelData_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize445 = { sizeof (ChannelServices_t3172382875), -1, sizeof(ChannelServices_t3172382875_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable445[5] = 
{
	ChannelServices_t3172382875_StaticFields::get_offset_of_registeredChannels_0(),
	ChannelServices_t3172382875_StaticFields::get_offset_of_delayedClientChannels_1(),
	ChannelServices_t3172382875_StaticFields::get_offset_of__crossContextSink_2(),
	ChannelServices_t3172382875_StaticFields::get_offset_of_CrossContextUrl_3(),
	ChannelServices_t3172382875_StaticFields::get_offset_of_oldStartModeTypes_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize446 = { sizeof (CrossAppDomainData_t4210664921), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable446[3] = 
{
	CrossAppDomainData_t4210664921::get_offset_of__ContextID_0(),
	CrossAppDomainData_t4210664921::get_offset_of__DomainID_1(),
	CrossAppDomainData_t4210664921::get_offset_of__processGuid_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize447 = { sizeof (CrossAppDomainChannel_t4294171512), -1, sizeof(CrossAppDomainChannel_t4294171512_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable447[1] = 
{
	CrossAppDomainChannel_t4294171512_StaticFields::get_offset_of_s_lock_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize448 = { sizeof (CrossAppDomainSink_t4211119298), -1, sizeof(CrossAppDomainSink_t4211119298_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable448[3] = 
{
	CrossAppDomainSink_t4211119298_StaticFields::get_offset_of_s_sinks_0(),
	CrossAppDomainSink_t4211119298_StaticFields::get_offset_of_processMessageMethod_1(),
	CrossAppDomainSink_t4211119298::get_offset_of__domainID_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize449 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize450 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize451 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize452 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize453 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize454 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize455 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize456 = { sizeof (SinkProviderData_t794892312), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable456[3] = 
{
	SinkProviderData_t794892312::get_offset_of_sinkName_0(),
	SinkProviderData_t794892312::get_offset_of_children_1(),
	SinkProviderData_t794892312::get_offset_of_properties_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize457 = { sizeof (Context_t515654137), -1, sizeof(Context_t515654137_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable457[14] = 
{
	Context_t515654137::get_offset_of_domain_id_0(),
	Context_t515654137::get_offset_of_context_id_1(),
	Context_t515654137::get_offset_of_static_data_2(),
	Context_t515654137_StaticFields::get_offset_of_default_server_context_sink_3(),
	Context_t515654137::get_offset_of_server_context_sink_chain_4(),
	Context_t515654137::get_offset_of_client_context_sink_chain_5(),
	Context_t515654137::get_offset_of_datastore_6(),
	Context_t515654137::get_offset_of_context_properties_7(),
	Context_t515654137::get_offset_of_frozen_8(),
	Context_t515654137_StaticFields::get_offset_of_global_count_9(),
	Context_t515654137_StaticFields::get_offset_of_namedSlots_10(),
	Context_t515654137_StaticFields::get_offset_of_global_dynamic_properties_11(),
	Context_t515654137::get_offset_of_context_dynamic_properties_12(),
	Context_t515654137::get_offset_of_callback_object_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize458 = { sizeof (DynamicPropertyCollection_t3791941180), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable458[1] = 
{
	DynamicPropertyCollection_t3791941180::get_offset_of__properties_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize459 = { sizeof (DynamicPropertyReg_t469376913), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable459[2] = 
{
	DynamicPropertyReg_t469376913::get_offset_of_Property_0(),
	DynamicPropertyReg_t469376913::get_offset_of_Sink_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize460 = { sizeof (ContextCallbackObject_t781636285), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize461 = { sizeof (ContextAttribute_t728408415), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable461[1] = 
{
	ContextAttribute_t728408415::get_offset_of_AttributeName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize462 = { sizeof (CrossContextChannel_t2792143614), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize463 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize464 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize465 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize466 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize467 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize468 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize469 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize470 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize471 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize472 = { sizeof (SynchronizationAttribute_t69789646), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable472[5] = 
{
	SynchronizationAttribute_t69789646::get_offset_of__bReEntrant_1(),
	SynchronizationAttribute_t69789646::get_offset_of__flavor_2(),
	SynchronizationAttribute_t69789646::get_offset_of__lockCount_3(),
	SynchronizationAttribute_t69789646::get_offset_of__mutex_4(),
	SynchronizationAttribute_t69789646::get_offset_of__ownerThread_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize473 = { sizeof (SynchronizedClientContextSink_t994676909), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable473[2] = 
{
	SynchronizedClientContextSink_t994676909::get_offset_of__next_0(),
	SynchronizedClientContextSink_t994676909::get_offset_of__att_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize474 = { sizeof (SynchronizedServerContextSink_t3512531509), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable474[2] = 
{
	SynchronizedServerContextSink_t3512531509::get_offset_of__next_0(),
	SynchronizedServerContextSink_t3512531509::get_offset_of__att_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize475 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize476 = { sizeof (Lease_t4190051248), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable476[8] = 
{
	Lease_t4190051248::get_offset_of__leaseExpireTime_1(),
	Lease_t4190051248::get_offset_of__currentState_2(),
	Lease_t4190051248::get_offset_of__initialLeaseTime_3(),
	Lease_t4190051248::get_offset_of__renewOnCallTime_4(),
	Lease_t4190051248::get_offset_of__sponsorshipTimeout_5(),
	Lease_t4190051248::get_offset_of__sponsors_6(),
	Lease_t4190051248::get_offset_of__renewingSponsors_7(),
	Lease_t4190051248::get_offset_of__renewalDelegate_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize477 = { sizeof (RenewalDelegate_t1379218617), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize478 = { sizeof (LeaseManager_t2601135431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable478[2] = 
{
	LeaseManager_t2601135431::get_offset_of__objects_0(),
	LeaseManager_t2601135431::get_offset_of__timer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize479 = { sizeof (LeaseSink_t1781043427), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable479[1] = 
{
	LeaseSink_t1781043427::get_offset_of__nextSink_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize480 = { sizeof (LeaseState_t4015278955)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable480[6] = 
{
	LeaseState_t4015278955::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize481 = { sizeof (LifetimeServices_t590691611), -1, sizeof(LifetimeServices_t590691611_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable481[5] = 
{
	LifetimeServices_t590691611_StaticFields::get_offset_of__leaseManagerPollTime_0(),
	LifetimeServices_t590691611_StaticFields::get_offset_of__leaseTime_1(),
	LifetimeServices_t590691611_StaticFields::get_offset_of__renewOnCallTime_2(),
	LifetimeServices_t590691611_StaticFields::get_offset_of__sponsorshipTimeout_3(),
	LifetimeServices_t590691611_StaticFields::get_offset_of__leaseManager_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize482 = { sizeof (ArgInfoType_t4189099640)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable482[3] = 
{
	ArgInfoType_t4189099640::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize483 = { sizeof (ArgInfo_t185197854), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable483[3] = 
{
	ArgInfo_t185197854::get_offset_of__paramMap_0(),
	ArgInfo_t185197854::get_offset_of__inoutArgCount_1(),
	ArgInfo_t185197854::get_offset_of__method_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize484 = { sizeof (AsyncResult_t4124112563), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable484[15] = 
{
	AsyncResult_t4124112563::get_offset_of_async_state_0(),
	AsyncResult_t4124112563::get_offset_of_handle_1(),
	AsyncResult_t4124112563::get_offset_of_async_delegate_2(),
	AsyncResult_t4124112563::get_offset_of_data_3(),
	AsyncResult_t4124112563::get_offset_of_object_data_4(),
	AsyncResult_t4124112563::get_offset_of_sync_completed_5(),
	AsyncResult_t4124112563::get_offset_of_completed_6(),
	AsyncResult_t4124112563::get_offset_of_endinvoke_called_7(),
	AsyncResult_t4124112563::get_offset_of_async_callback_8(),
	AsyncResult_t4124112563::get_offset_of_current_9(),
	AsyncResult_t4124112563::get_offset_of_original_10(),
	AsyncResult_t4124112563::get_offset_of_gchandle_11(),
	AsyncResult_t4124112563::get_offset_of_call_message_12(),
	AsyncResult_t4124112563::get_offset_of_message_ctrl_13(),
	AsyncResult_t4124112563::get_offset_of_reply_message_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize485 = { sizeof (ClientContextTerminatorSink_t1503526936), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable485[1] = 
{
	ClientContextTerminatorSink_t1503526936::get_offset_of__context_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize486 = { sizeof (ConstructionCall_t2368686409), -1, sizeof(ConstructionCall_t2368686409_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable486[7] = 
{
	ConstructionCall_t2368686409::get_offset_of__activator_11(),
	ConstructionCall_t2368686409::get_offset_of__activationAttributes_12(),
	ConstructionCall_t2368686409::get_offset_of__contextProperties_13(),
	ConstructionCall_t2368686409::get_offset_of__activationType_14(),
	ConstructionCall_t2368686409::get_offset_of__activationTypeName_15(),
	ConstructionCall_t2368686409::get_offset_of__isContextOk_16(),
	ConstructionCall_t2368686409_StaticFields::get_offset_of_U3CU3Ef__switchU24map20_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize487 = { sizeof (ConstructionCallDictionary_t743124799), -1, sizeof(ConstructionCallDictionary_t743124799_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable487[3] = 
{
	ConstructionCallDictionary_t743124799_StaticFields::get_offset_of_InternalKeys_6(),
	ConstructionCallDictionary_t743124799_StaticFields::get_offset_of_U3CU3Ef__switchU24map23_7(),
	ConstructionCallDictionary_t743124799_StaticFields::get_offset_of_U3CU3Ef__switchU24map24_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize488 = { sizeof (EnvoyTerminatorSink_t4146359051), -1, sizeof(EnvoyTerminatorSink_t4146359051_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable488[1] = 
{
	EnvoyTerminatorSink_t4146359051_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize489 = { sizeof (Header_t1689611527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable489[4] = 
{
	Header_t1689611527::get_offset_of_HeaderNamespace_0(),
	Header_t1689611527::get_offset_of_MustUnderstand_1(),
	Header_t1689611527::get_offset_of_Name_2(),
	Header_t1689611527::get_offset_of_Value_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize490 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize491 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize492 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize493 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize494 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize495 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize496 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize497 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize498 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize499 = { sizeof (LogicalCallContext_t3949483042), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable499[2] = 
{
	LogicalCallContext_t3949483042::get_offset_of__data_0(),
	LogicalCallContext_t3949483042::get_offset_of__remotingData_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
