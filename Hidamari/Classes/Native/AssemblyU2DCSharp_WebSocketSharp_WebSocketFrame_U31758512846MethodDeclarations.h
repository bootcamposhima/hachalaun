﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.WebSocketFrame/<ParseAsync>c__AnonStorey31
struct U3CParseAsyncU3Ec__AnonStorey31_t1758512846;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocketSharp.WebSocketFrame/<ParseAsync>c__AnonStorey31::.ctor()
extern "C"  void U3CParseAsyncU3Ec__AnonStorey31__ctor_m332244573 (U3CParseAsyncU3Ec__AnonStorey31_t1758512846 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocketFrame/<ParseAsync>c__AnonStorey31::<>m__1A(System.Byte[])
extern "C"  void U3CParseAsyncU3Ec__AnonStorey31_U3CU3Em__1A_m283814771 (U3CParseAsyncU3Ec__AnonStorey31_t1758512846 * __this, ByteU5BU5D_t4260760469* ___header0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
