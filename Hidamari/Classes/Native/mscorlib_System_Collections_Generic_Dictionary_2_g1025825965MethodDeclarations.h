﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>
struct Dictionary_2_t1025825965;
// System.Collections.Generic.IEqualityComparer`1<WebSocketSharp.CompressionMethod>
struct IEqualityComparer_1_t3017631185;
// System.Collections.Generic.IDictionary`2<WebSocketSharp.CompressionMethod,System.Object>
struct IDictionary_2_t603699310;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<WebSocketSharp.CompressionMethod,System.Object>[]
struct KeyValuePair_2U5BU5D_t1219481366;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<WebSocketSharp.CompressionMethod,System.Object>>
struct IEnumerator_1_t2836471720;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.Generic.Dictionary`2/KeyCollection<WebSocketSharp.CompressionMethod,System.Object>
struct KeyCollection_t2652585416;
// System.Collections.Generic.Dictionary`2/ValueCollection<WebSocketSharp.CompressionMethod,System.Object>
struct ValueCollection_t4021398974;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_924606671.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_WebSocketSharp_CompressionMethod2226596781.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2343149357.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m578331081_gshared (Dictionary_2_t1025825965 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m578331081(__this, method) ((  void (*) (Dictionary_2_t1025825965 *, const MethodInfo*))Dictionary_2__ctor_m578331081_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m4167210432_gshared (Dictionary_2_t1025825965 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m4167210432(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t1025825965 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m4167210432_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m350230383_gshared (Dictionary_2_t1025825965 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m350230383(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t1025825965 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m350230383_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m4043108762_gshared (Dictionary_2_t1025825965 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m4043108762(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t1025825965 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m4043108762_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3277336558_gshared (Dictionary_2_t1025825965 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m3277336558(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t1025825965 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3277336558_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m221108899_gshared (Dictionary_2_t1025825965 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m221108899(__this, ___capacity0, ___comparer1, method) ((  void (*) (Dictionary_2_t1025825965 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m221108899_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m2100855690_gshared (Dictionary_2_t1025825965 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m2100855690(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1025825965 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m2100855690_gshared)(__this, ___info0, ___context1, method)
// System.Object System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m697864597_gshared (Dictionary_2_t1025825965 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m697864597(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t1025825965 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m697864597_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m1250982916_gshared (Dictionary_2_t1025825965 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m1250982916(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1025825965 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m1250982916_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m2953924557_gshared (Dictionary_2_t1025825965 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m2953924557(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1025825965 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m2953924557_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m3743645250_gshared (Dictionary_2_t1025825965 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m3743645250(__this, ___key0, method) ((  void (*) (Dictionary_2_t1025825965 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m3743645250_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3855196719_gshared (Dictionary_2_t1025825965 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3855196719(__this, method) ((  bool (*) (Dictionary_2_t1025825965 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3855196719_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1208937057_gshared (Dictionary_2_t1025825965 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1208937057(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1025825965 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1208937057_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1281339827_gshared (Dictionary_2_t1025825965 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1281339827(__this, method) ((  bool (*) (Dictionary_2_t1025825965 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1281339827_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1774297560_gshared (Dictionary_2_t1025825965 * __this, KeyValuePair_2_t924606671  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1774297560(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t1025825965 *, KeyValuePair_2_t924606671 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1774297560_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m111894574_gshared (Dictionary_2_t1025825965 * __this, KeyValuePair_2_t924606671  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m111894574(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1025825965 *, KeyValuePair_2_t924606671 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m111894574_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1473956796_gshared (Dictionary_2_t1025825965 * __this, KeyValuePair_2U5BU5D_t1219481366* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1473956796(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1025825965 *, KeyValuePair_2U5BU5D_t1219481366*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1473956796_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m609204947_gshared (Dictionary_2_t1025825965 * __this, KeyValuePair_2_t924606671  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m609204947(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1025825965 *, KeyValuePair_2_t924606671 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m609204947_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m2173092763_gshared (Dictionary_2_t1025825965 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m2173092763(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1025825965 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m2173092763_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3662793578_gshared (Dictionary_2_t1025825965 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3662793578(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1025825965 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3662793578_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2019002657_gshared (Dictionary_2_t1025825965 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2019002657(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1025825965 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2019002657_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1640124718_gshared (Dictionary_2_t1025825965 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1640124718(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1025825965 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1640124718_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m1476213609_gshared (Dictionary_2_t1025825965 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m1476213609(__this, method) ((  int32_t (*) (Dictionary_2_t1025825965 *, const MethodInfo*))Dictionary_2_get_Count_m1476213609_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m3026492030_gshared (Dictionary_2_t1025825965 * __this, uint8_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m3026492030(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t1025825965 *, uint8_t, const MethodInfo*))Dictionary_2_get_Item_m3026492030_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m3682960329_gshared (Dictionary_2_t1025825965 * __this, uint8_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m3682960329(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1025825965 *, uint8_t, Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m3682960329_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m1014673729_gshared (Dictionary_2_t1025825965 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m1014673729(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t1025825965 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m1014673729_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m3099731094_gshared (Dictionary_2_t1025825965 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m3099731094(__this, ___size0, method) ((  void (*) (Dictionary_2_t1025825965 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m3099731094_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m2319860242_gshared (Dictionary_2_t1025825965 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m2319860242(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1025825965 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m2319860242_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t924606671  Dictionary_2_make_pair_m3717872806_gshared (Il2CppObject * __this /* static, unused */, uint8_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m3717872806(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t924606671  (*) (Il2CppObject * /* static, unused */, uint8_t, Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m3717872806_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::pick_key(TKey,TValue)
extern "C"  uint8_t Dictionary_2_pick_key_m3156079448_gshared (Il2CppObject * __this /* static, unused */, uint8_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m3156079448(__this /* static, unused */, ___key0, ___value1, method) ((  uint8_t (*) (Il2CppObject * /* static, unused */, uint8_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_key_m3156079448_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m2116303796_gshared (Il2CppObject * __this /* static, unused */, uint8_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m2116303796(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, uint8_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m2116303796_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m3842523517_gshared (Dictionary_2_t1025825965 * __this, KeyValuePair_2U5BU5D_t1219481366* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m3842523517(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1025825965 *, KeyValuePair_2U5BU5D_t1219481366*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m3842523517_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m806190991_gshared (Dictionary_2_t1025825965 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m806190991(__this, method) ((  void (*) (Dictionary_2_t1025825965 *, const MethodInfo*))Dictionary_2_Resize_m806190991_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m2388493580_gshared (Dictionary_2_t1025825965 * __this, uint8_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m2388493580(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1025825965 *, uint8_t, Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m2388493580_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m2279431668_gshared (Dictionary_2_t1025825965 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m2279431668(__this, method) ((  void (*) (Dictionary_2_t1025825965 *, const MethodInfo*))Dictionary_2_Clear_m2279431668_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m843657118_gshared (Dictionary_2_t1025825965 * __this, uint8_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m843657118(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1025825965 *, uint8_t, const MethodInfo*))Dictionary_2_ContainsKey_m843657118_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m2032632990_gshared (Dictionary_2_t1025825965 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m2032632990(__this, ___value0, method) ((  bool (*) (Dictionary_2_t1025825965 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m2032632990_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m925540583_gshared (Dictionary_2_t1025825965 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m925540583(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1025825965 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m925540583_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m3514082269_gshared (Dictionary_2_t1025825965 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m3514082269(__this, ___sender0, method) ((  void (*) (Dictionary_2_t1025825965 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m3514082269_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m4137893522_gshared (Dictionary_2_t1025825965 * __this, uint8_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m4137893522(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1025825965 *, uint8_t, const MethodInfo*))Dictionary_2_Remove_m4137893522_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m4218620407_gshared (Dictionary_2_t1025825965 * __this, uint8_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m4218620407(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t1025825965 *, uint8_t, Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m4218620407_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::get_Keys()
extern "C"  KeyCollection_t2652585416 * Dictionary_2_get_Keys_m532320132_gshared (Dictionary_2_t1025825965 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m532320132(__this, method) ((  KeyCollection_t2652585416 * (*) (Dictionary_2_t1025825965 *, const MethodInfo*))Dictionary_2_get_Keys_m532320132_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::get_Values()
extern "C"  ValueCollection_t4021398974 * Dictionary_2_get_Values_m455074464_gshared (Dictionary_2_t1025825965 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m455074464(__this, method) ((  ValueCollection_t4021398974 * (*) (Dictionary_2_t1025825965 *, const MethodInfo*))Dictionary_2_get_Values_m455074464_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::ToTKey(System.Object)
extern "C"  uint8_t Dictionary_2_ToTKey_m2605938355_gshared (Dictionary_2_t1025825965 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m2605938355(__this, ___key0, method) ((  uint8_t (*) (Dictionary_2_t1025825965 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m2605938355_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m428617935_gshared (Dictionary_2_t1025825965 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m428617935(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t1025825965 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m428617935_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m1460067445_gshared (Dictionary_2_t1025825965 * __this, KeyValuePair_2_t924606671  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m1460067445(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t1025825965 *, KeyValuePair_2_t924606671 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m1460067445_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::GetEnumerator()
extern "C"  Enumerator_t2343149357  Dictionary_2_GetEnumerator_m711766612_gshared (Dictionary_2_t1025825965 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m711766612(__this, method) ((  Enumerator_t2343149357  (*) (Dictionary_2_t1025825965 *, const MethodInfo*))Dictionary_2_GetEnumerator_m711766612_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__0_m341211339_gshared (Il2CppObject * __this /* static, unused */, uint8_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m341211339(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, uint8_t, Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m341211339_gshared)(__this /* static, unused */, ___key0, ___value1, method)
