﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "AssemblyU2DCSharp_Task2599333.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Move
struct  Move_t2404337  : public Task_t2599333
{
public:
	// System.Boolean Move::istart
	bool ___istart_2;
	// UnityEngine.GameObject Move::movePar
	GameObject_t3674682005 * ___movePar_3;

public:
	inline static int32_t get_offset_of_istart_2() { return static_cast<int32_t>(offsetof(Move_t2404337, ___istart_2)); }
	inline bool get_istart_2() const { return ___istart_2; }
	inline bool* get_address_of_istart_2() { return &___istart_2; }
	inline void set_istart_2(bool value)
	{
		___istart_2 = value;
	}

	inline static int32_t get_offset_of_movePar_3() { return static_cast<int32_t>(offsetof(Move_t2404337, ___movePar_3)); }
	inline GameObject_t3674682005 * get_movePar_3() const { return ___movePar_3; }
	inline GameObject_t3674682005 ** get_address_of_movePar_3() { return &___movePar_3; }
	inline void set_movePar_3(GameObject_t3674682005 * value)
	{
		___movePar_3 = value;
		Il2CppCodeGenWriteBarrier(&___movePar_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
