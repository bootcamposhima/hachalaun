﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.Net.QueryStringCollection
struct QueryStringCollection_t580001633;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocketSharp.Net.QueryStringCollection::.ctor()
extern "C"  void QueryStringCollection__ctor_m3938208288 (QueryStringCollection_t580001633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.Net.QueryStringCollection::ToString()
extern "C"  String_t* QueryStringCollection_ToString_m2702326707 (QueryStringCollection_t580001633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
