﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ActionManager/<MoveAction>c__Iterator6
struct U3CMoveActionU3Ec__Iterator6_t173834552;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ActionManager/<MoveAction>c__Iterator6::.ctor()
extern "C"  void U3CMoveActionU3Ec__Iterator6__ctor_m1564893795 (U3CMoveActionU3Ec__Iterator6_t173834552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ActionManager/<MoveAction>c__Iterator6::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMoveActionU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3354059673 (U3CMoveActionU3Ec__Iterator6_t173834552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ActionManager/<MoveAction>c__Iterator6::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMoveActionU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m2191604013 (U3CMoveActionU3Ec__Iterator6_t173834552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ActionManager/<MoveAction>c__Iterator6::MoveNext()
extern "C"  bool U3CMoveActionU3Ec__Iterator6_MoveNext_m3213847961 (U3CMoveActionU3Ec__Iterator6_t173834552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActionManager/<MoveAction>c__Iterator6::Dispose()
extern "C"  void U3CMoveActionU3Ec__Iterator6_Dispose_m522666464 (U3CMoveActionU3Ec__Iterator6_t173834552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActionManager/<MoveAction>c__Iterator6::Reset()
extern "C"  void U3CMoveActionU3Ec__Iterator6_Reset_m3506294032 (U3CMoveActionU3Ec__Iterator6_t173834552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
