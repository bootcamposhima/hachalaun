﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// HPManager
struct HPManager_t3198084485;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HPManager/<DamagetAnimation>c__Iterator12
struct  U3CDamagetAnimationU3Ec__Iterator12_t1065517605  : public Il2CppObject
{
public:
	// UnityEngine.Vector2 HPManager/<DamagetAnimation>c__Iterator12::<size>__0
	Vector2_t4282066565  ___U3CsizeU3E__0_0;
	// UnityEngine.Vector2 HPManager/<DamagetAnimation>c__Iterator12::<s>__1
	Vector2_t4282066565  ___U3CsU3E__1_1;
	// System.Int32 HPManager/<DamagetAnimation>c__Iterator12::$PC
	int32_t ___U24PC_2;
	// System.Object HPManager/<DamagetAnimation>c__Iterator12::$current
	Il2CppObject * ___U24current_3;
	// HPManager HPManager/<DamagetAnimation>c__Iterator12::<>f__this
	HPManager_t3198084485 * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_U3CsizeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDamagetAnimationU3Ec__Iterator12_t1065517605, ___U3CsizeU3E__0_0)); }
	inline Vector2_t4282066565  get_U3CsizeU3E__0_0() const { return ___U3CsizeU3E__0_0; }
	inline Vector2_t4282066565 * get_address_of_U3CsizeU3E__0_0() { return &___U3CsizeU3E__0_0; }
	inline void set_U3CsizeU3E__0_0(Vector2_t4282066565  value)
	{
		___U3CsizeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CsU3E__1_1() { return static_cast<int32_t>(offsetof(U3CDamagetAnimationU3Ec__Iterator12_t1065517605, ___U3CsU3E__1_1)); }
	inline Vector2_t4282066565  get_U3CsU3E__1_1() const { return ___U3CsU3E__1_1; }
	inline Vector2_t4282066565 * get_address_of_U3CsU3E__1_1() { return &___U3CsU3E__1_1; }
	inline void set_U3CsU3E__1_1(Vector2_t4282066565  value)
	{
		___U3CsU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CDamagetAnimationU3Ec__Iterator12_t1065517605, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CDamagetAnimationU3Ec__Iterator12_t1065517605, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3CDamagetAnimationU3Ec__Iterator12_t1065517605, ___U3CU3Ef__this_4)); }
	inline HPManager_t3198084485 * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline HPManager_t3198084485 ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(HPManager_t3198084485 * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
