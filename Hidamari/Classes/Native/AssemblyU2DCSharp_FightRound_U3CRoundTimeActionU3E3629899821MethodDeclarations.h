﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FightRound/<RoundTimeAction>c__IteratorF
struct U3CRoundTimeActionU3Ec__IteratorF_t3629899821;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void FightRound/<RoundTimeAction>c__IteratorF::.ctor()
extern "C"  void U3CRoundTimeActionU3Ec__IteratorF__ctor_m273375502 (U3CRoundTimeActionU3Ec__IteratorF_t3629899821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FightRound/<RoundTimeAction>c__IteratorF::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CRoundTimeActionU3Ec__IteratorF_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1880064142 (U3CRoundTimeActionU3Ec__IteratorF_t3629899821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FightRound/<RoundTimeAction>c__IteratorF::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CRoundTimeActionU3Ec__IteratorF_System_Collections_IEnumerator_get_Current_m3745538082 (U3CRoundTimeActionU3Ec__IteratorF_t3629899821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightRound/<RoundTimeAction>c__IteratorF::MoveNext()
extern "C"  bool U3CRoundTimeActionU3Ec__IteratorF_MoveNext_m3831483918 (U3CRoundTimeActionU3Ec__IteratorF_t3629899821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightRound/<RoundTimeAction>c__IteratorF::Dispose()
extern "C"  void U3CRoundTimeActionU3Ec__IteratorF_Dispose_m619135435 (U3CRoundTimeActionU3Ec__IteratorF_t3629899821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightRound/<RoundTimeAction>c__IteratorF::Reset()
extern "C"  void U3CRoundTimeActionU3Ec__IteratorF_Reset_m2214775739 (U3CRoundTimeActionU3Ec__IteratorF_t3629899821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
