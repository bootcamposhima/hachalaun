﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Char>
struct ValueCollection_t3733267447;
// System.Collections.Generic.Dictionary`2<System.Object,System.Char>
struct Dictionary_2_t737694438;
// System.Collections.Generic.IEnumerator`1<System.Char>
struct IEnumerator_1_t479520291;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Char[]
struct CharU5BU5D_t3324145743;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2964495142.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Char>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m1935621361_gshared (ValueCollection_t3733267447 * __this, Dictionary_2_t737694438 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m1935621361(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t3733267447 *, Dictionary_2_t737694438 *, const MethodInfo*))ValueCollection__ctor_m1935621361_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Char>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m624631073_gshared (ValueCollection_t3733267447 * __this, Il2CppChar ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m624631073(__this, ___item0, method) ((  void (*) (ValueCollection_t3733267447 *, Il2CppChar, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m624631073_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Char>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3880174186_gshared (ValueCollection_t3733267447 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3880174186(__this, method) ((  void (*) (ValueCollection_t3733267447 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3880174186_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Char>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2845504293_gshared (ValueCollection_t3733267447 * __this, Il2CppChar ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2845504293(__this, ___item0, method) ((  bool (*) (ValueCollection_t3733267447 *, Il2CppChar, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2845504293_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Char>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1014010122_gshared (ValueCollection_t3733267447 * __this, Il2CppChar ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1014010122(__this, ___item0, method) ((  bool (*) (ValueCollection_t3733267447 *, Il2CppChar, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1014010122_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Char>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3008812856_gshared (ValueCollection_t3733267447 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3008812856(__this, method) ((  Il2CppObject* (*) (ValueCollection_t3733267447 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3008812856_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Char>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m1243932398_gshared (ValueCollection_t3733267447 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m1243932398(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3733267447 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1243932398_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Char>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m360125929_gshared (ValueCollection_t3733267447 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m360125929(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3733267447 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m360125929_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Char>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1120680152_gshared (ValueCollection_t3733267447 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1120680152(__this, method) ((  bool (*) (ValueCollection_t3733267447 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1120680152_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Char>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3355378360_gshared (ValueCollection_t3733267447 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3355378360(__this, method) ((  bool (*) (ValueCollection_t3733267447 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3355378360_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Char>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m2518357860_gshared (ValueCollection_t3733267447 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2518357860(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3733267447 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2518357860_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Char>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m2788789496_gshared (ValueCollection_t3733267447 * __this, CharU5BU5D_t3324145743* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m2788789496(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3733267447 *, CharU5BU5D_t3324145743*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m2788789496_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Char>::GetEnumerator()
extern "C"  Enumerator_t2964495142  ValueCollection_GetEnumerator_m533181211_gshared (ValueCollection_t3733267447 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m533181211(__this, method) ((  Enumerator_t2964495142  (*) (ValueCollection_t3733267447 *, const MethodInfo*))ValueCollection_GetEnumerator_m533181211_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Char>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m1062707326_gshared (ValueCollection_t3733267447 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m1062707326(__this, method) ((  int32_t (*) (ValueCollection_t3733267447 *, const MethodInfo*))ValueCollection_get_Count_m1062707326_gshared)(__this, method)
