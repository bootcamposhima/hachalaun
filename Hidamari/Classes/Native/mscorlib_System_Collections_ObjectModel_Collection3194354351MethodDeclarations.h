﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<AIDatas>
struct Collection_1_t3194354351;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// AIDatas[]
struct AIDatasU5BU5D_t2487978268;
// System.Collections.Generic.IEnumerator`1<AIDatas>
struct IEnumerator_1_t1625793946;
// System.Collections.Generic.IList`1<AIDatas>
struct IList_1_t2408576100;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_AIDatas4008896193.h"

// System.Void System.Collections.ObjectModel.Collection`1<AIDatas>::.ctor()
extern "C"  void Collection_1__ctor_m1005078340_gshared (Collection_1_t3194354351 * __this, const MethodInfo* method);
#define Collection_1__ctor_m1005078340(__this, method) ((  void (*) (Collection_1_t3194354351 *, const MethodInfo*))Collection_1__ctor_m1005078340_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AIDatas>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1692024247_gshared (Collection_1_t3194354351 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1692024247(__this, method) ((  bool (*) (Collection_1_t3194354351 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1692024247_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<AIDatas>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m348707456_gshared (Collection_1_t3194354351 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m348707456(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3194354351 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m348707456_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<AIDatas>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m885560443_gshared (Collection_1_t3194354351 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m885560443(__this, method) ((  Il2CppObject * (*) (Collection_1_t3194354351 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m885560443_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<AIDatas>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m1616245078_gshared (Collection_1_t3194354351 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m1616245078(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3194354351 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1616245078_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AIDatas>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m992729398_gshared (Collection_1_t3194354351 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m992729398(__this, ___value0, method) ((  bool (*) (Collection_1_t3194354351 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m992729398_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<AIDatas>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m636587182_gshared (Collection_1_t3194354351 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m636587182(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3194354351 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m636587182_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AIDatas>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m3434060249_gshared (Collection_1_t3194354351 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m3434060249(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3194354351 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m3434060249_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<AIDatas>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m837943983_gshared (Collection_1_t3194354351 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m837943983(__this, ___value0, method) ((  void (*) (Collection_1_t3194354351 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m837943983_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AIDatas>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m3546638950_gshared (Collection_1_t3194354351 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m3546638950(__this, method) ((  bool (*) (Collection_1_t3194354351 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m3546638950_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<AIDatas>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m1419920978_gshared (Collection_1_t3194354351 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1419920978(__this, method) ((  Il2CppObject * (*) (Collection_1_t3194354351 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1419920978_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AIDatas>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m411314469_gshared (Collection_1_t3194354351 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m411314469(__this, method) ((  bool (*) (Collection_1_t3194354351 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m411314469_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AIDatas>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m2684034356_gshared (Collection_1_t3194354351 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m2684034356(__this, method) ((  bool (*) (Collection_1_t3194354351 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m2684034356_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<AIDatas>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m4105038681_gshared (Collection_1_t3194354351 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m4105038681(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t3194354351 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m4105038681_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AIDatas>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m2210968432_gshared (Collection_1_t3194354351 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m2210968432(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3194354351 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m2210968432_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<AIDatas>::Add(T)
extern "C"  void Collection_1_Add_m992395451_gshared (Collection_1_t3194354351 * __this, AIDatas_t4008896193  ___item0, const MethodInfo* method);
#define Collection_1_Add_m992395451(__this, ___item0, method) ((  void (*) (Collection_1_t3194354351 *, AIDatas_t4008896193 , const MethodInfo*))Collection_1_Add_m992395451_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AIDatas>::Clear()
extern "C"  void Collection_1_Clear_m2706178927_gshared (Collection_1_t3194354351 * __this, const MethodInfo* method);
#define Collection_1_Clear_m2706178927(__this, method) ((  void (*) (Collection_1_t3194354351 *, const MethodInfo*))Collection_1_Clear_m2706178927_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<AIDatas>::ClearItems()
extern "C"  void Collection_1_ClearItems_m1513253619_gshared (Collection_1_t3194354351 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m1513253619(__this, method) ((  void (*) (Collection_1_t3194354351 *, const MethodInfo*))Collection_1_ClearItems_m1513253619_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AIDatas>::Contains(T)
extern "C"  bool Collection_1_Contains_m2114658845_gshared (Collection_1_t3194354351 * __this, AIDatas_t4008896193  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m2114658845(__this, ___item0, method) ((  bool (*) (Collection_1_t3194354351 *, AIDatas_t4008896193 , const MethodInfo*))Collection_1_Contains_m2114658845_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AIDatas>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m3482456043_gshared (Collection_1_t3194354351 * __this, AIDatasU5BU5D_t2487978268* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m3482456043(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3194354351 *, AIDatasU5BU5D_t2487978268*, int32_t, const MethodInfo*))Collection_1_CopyTo_m3482456043_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<AIDatas>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m1125754624_gshared (Collection_1_t3194354351 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m1125754624(__this, method) ((  Il2CppObject* (*) (Collection_1_t3194354351 *, const MethodInfo*))Collection_1_GetEnumerator_m1125754624_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<AIDatas>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m3379074735_gshared (Collection_1_t3194354351 * __this, AIDatas_t4008896193  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m3379074735(__this, ___item0, method) ((  int32_t (*) (Collection_1_t3194354351 *, AIDatas_t4008896193 , const MethodInfo*))Collection_1_IndexOf_m3379074735_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AIDatas>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m3216581922_gshared (Collection_1_t3194354351 * __this, int32_t ___index0, AIDatas_t4008896193  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m3216581922(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3194354351 *, int32_t, AIDatas_t4008896193 , const MethodInfo*))Collection_1_Insert_m3216581922_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<AIDatas>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m2822042197_gshared (Collection_1_t3194354351 * __this, int32_t ___index0, AIDatas_t4008896193  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m2822042197(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3194354351 *, int32_t, AIDatas_t4008896193 , const MethodInfo*))Collection_1_InsertItem_m2822042197_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AIDatas>::Remove(T)
extern "C"  bool Collection_1_Remove_m3907765592_gshared (Collection_1_t3194354351 * __this, AIDatas_t4008896193  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m3907765592(__this, ___item0, method) ((  bool (*) (Collection_1_t3194354351 *, AIDatas_t4008896193 , const MethodInfo*))Collection_1_Remove_m3907765592_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AIDatas>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m1090434792_gshared (Collection_1_t3194354351 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m1090434792(__this, ___index0, method) ((  void (*) (Collection_1_t3194354351 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1090434792_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AIDatas>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m2835439880_gshared (Collection_1_t3194354351 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m2835439880(__this, ___index0, method) ((  void (*) (Collection_1_t3194354351 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m2835439880_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<AIDatas>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m1489409196_gshared (Collection_1_t3194354351 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m1489409196(__this, method) ((  int32_t (*) (Collection_1_t3194354351 *, const MethodInfo*))Collection_1_get_Count_m1489409196_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<AIDatas>::get_Item(System.Int32)
extern "C"  AIDatas_t4008896193  Collection_1_get_Item_m3017345260_gshared (Collection_1_t3194354351 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m3017345260(__this, ___index0, method) ((  AIDatas_t4008896193  (*) (Collection_1_t3194354351 *, int32_t, const MethodInfo*))Collection_1_get_Item_m3017345260_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AIDatas>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m568370553_gshared (Collection_1_t3194354351 * __this, int32_t ___index0, AIDatas_t4008896193  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m568370553(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3194354351 *, int32_t, AIDatas_t4008896193 , const MethodInfo*))Collection_1_set_Item_m568370553_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<AIDatas>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m2246381536_gshared (Collection_1_t3194354351 * __this, int32_t ___index0, AIDatas_t4008896193  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m2246381536(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3194354351 *, int32_t, AIDatas_t4008896193 , const MethodInfo*))Collection_1_SetItem_m2246381536_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AIDatas>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m3939709551_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m3939709551(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m3939709551_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<AIDatas>::ConvertItem(System.Object)
extern "C"  AIDatas_t4008896193  Collection_1_ConvertItem_m4031537611_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m4031537611(__this /* static, unused */, ___item0, method) ((  AIDatas_t4008896193  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m4031537611_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<AIDatas>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m3605551915_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m3605551915(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m3605551915_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AIDatas>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m2928050933_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m2928050933(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m2928050933_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<AIDatas>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m641118154_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m641118154(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m641118154_gshared)(__this /* static, unused */, ___list0, method)
