﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// WebSocketSharp.Net.Cookie
struct Cookie_t2077085446;
// System.Object
struct Il2CppObject;
// WebSocketSharp.WebSocket
struct WebSocket_t1342580397;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocket/<>c__Iterator1F
struct  U3CU3Ec__Iterator1F_t90658481  : public Il2CppObject
{
public:
	// System.Collections.IEnumerator WebSocketSharp.WebSocket/<>c__Iterator1F::<$s_145>__0
	Il2CppObject * ___U3CU24s_145U3E__0_0;
	// WebSocketSharp.Net.Cookie WebSocketSharp.WebSocket/<>c__Iterator1F::<cookie>__1
	Cookie_t2077085446 * ___U3CcookieU3E__1_1;
	// System.Object WebSocketSharp.WebSocket/<>c__Iterator1F::<$s_146>__2
	Il2CppObject * ___U3CU24s_146U3E__2_2;
	// System.Int32 WebSocketSharp.WebSocket/<>c__Iterator1F::$PC
	int32_t ___U24PC_3;
	// WebSocketSharp.Net.Cookie WebSocketSharp.WebSocket/<>c__Iterator1F::$current
	Cookie_t2077085446 * ___U24current_4;
	// WebSocketSharp.WebSocket WebSocketSharp.WebSocket/<>c__Iterator1F::<>f__this
	WebSocket_t1342580397 * ___U3CU3Ef__this_5;

public:
	inline static int32_t get_offset_of_U3CU24s_145U3E__0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1F_t90658481, ___U3CU24s_145U3E__0_0)); }
	inline Il2CppObject * get_U3CU24s_145U3E__0_0() const { return ___U3CU24s_145U3E__0_0; }
	inline Il2CppObject ** get_address_of_U3CU24s_145U3E__0_0() { return &___U3CU24s_145U3E__0_0; }
	inline void set_U3CU24s_145U3E__0_0(Il2CppObject * value)
	{
		___U3CU24s_145U3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_145U3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CcookieU3E__1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1F_t90658481, ___U3CcookieU3E__1_1)); }
	inline Cookie_t2077085446 * get_U3CcookieU3E__1_1() const { return ___U3CcookieU3E__1_1; }
	inline Cookie_t2077085446 ** get_address_of_U3CcookieU3E__1_1() { return &___U3CcookieU3E__1_1; }
	inline void set_U3CcookieU3E__1_1(Cookie_t2077085446 * value)
	{
		___U3CcookieU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcookieU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CU24s_146U3E__2_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1F_t90658481, ___U3CU24s_146U3E__2_2)); }
	inline Il2CppObject * get_U3CU24s_146U3E__2_2() const { return ___U3CU24s_146U3E__2_2; }
	inline Il2CppObject ** get_address_of_U3CU24s_146U3E__2_2() { return &___U3CU24s_146U3E__2_2; }
	inline void set_U3CU24s_146U3E__2_2(Il2CppObject * value)
	{
		___U3CU24s_146U3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_146U3E__2_2, value);
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1F_t90658481, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1F_t90658481, ___U24current_4)); }
	inline Cookie_t2077085446 * get_U24current_4() const { return ___U24current_4; }
	inline Cookie_t2077085446 ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Cookie_t2077085446 * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1F_t90658481, ___U3CU3Ef__this_5)); }
	inline WebSocket_t1342580397 * get_U3CU3Ef__this_5() const { return ___U3CU3Ef__this_5; }
	inline WebSocket_t1342580397 ** get_address_of_U3CU3Ef__this_5() { return &___U3CU3Ef__this_5; }
	inline void set_U3CU3Ef__this_5(WebSocket_t1342580397 * value)
	{
		___U3CU3Ef__this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
