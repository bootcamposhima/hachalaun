﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TransitionAnimation/<Fadeout>c__IteratorA
struct U3CFadeoutU3Ec__IteratorA_t2464685144;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void TransitionAnimation/<Fadeout>c__IteratorA::.ctor()
extern "C"  void U3CFadeoutU3Ec__IteratorA__ctor_m4146314259 (U3CFadeoutU3Ec__IteratorA_t2464685144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TransitionAnimation/<Fadeout>c__IteratorA::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFadeoutU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2061439839 (U3CFadeoutU3Ec__IteratorA_t2464685144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TransitionAnimation/<Fadeout>c__IteratorA::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFadeoutU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m76133619 (U3CFadeoutU3Ec__IteratorA_t2464685144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TransitionAnimation/<Fadeout>c__IteratorA::MoveNext()
extern "C"  bool U3CFadeoutU3Ec__IteratorA_MoveNext_m3228038657 (U3CFadeoutU3Ec__IteratorA_t2464685144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransitionAnimation/<Fadeout>c__IteratorA::Dispose()
extern "C"  void U3CFadeoutU3Ec__IteratorA_Dispose_m3071602576 (U3CFadeoutU3Ec__IteratorA_t2464685144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TransitionAnimation/<Fadeout>c__IteratorA::Reset()
extern "C"  void U3CFadeoutU3Ec__IteratorA_Reset_m1792747200 (U3CFadeoutU3Ec__IteratorA_t2464685144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
