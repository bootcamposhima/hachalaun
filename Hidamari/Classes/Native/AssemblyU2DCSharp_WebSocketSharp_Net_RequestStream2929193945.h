﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.IO.Stream
struct Stream_t1561764144;

#include "mscorlib_System_IO_Stream1561764144.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.RequestStream
struct  RequestStream_t2929193945  : public Stream_t1561764144
{
public:
	// System.Byte[] WebSocketSharp.Net.RequestStream::_buffer
	ByteU5BU5D_t4260760469* ____buffer_1;
	// System.Boolean WebSocketSharp.Net.RequestStream::_disposed
	bool ____disposed_2;
	// System.Int32 WebSocketSharp.Net.RequestStream::_length
	int32_t ____length_3;
	// System.Int32 WebSocketSharp.Net.RequestStream::_offset
	int32_t ____offset_4;
	// System.Int64 WebSocketSharp.Net.RequestStream::_remainingBody
	int64_t ____remainingBody_5;
	// System.IO.Stream WebSocketSharp.Net.RequestStream::_stream
	Stream_t1561764144 * ____stream_6;

public:
	inline static int32_t get_offset_of__buffer_1() { return static_cast<int32_t>(offsetof(RequestStream_t2929193945, ____buffer_1)); }
	inline ByteU5BU5D_t4260760469* get__buffer_1() const { return ____buffer_1; }
	inline ByteU5BU5D_t4260760469** get_address_of__buffer_1() { return &____buffer_1; }
	inline void set__buffer_1(ByteU5BU5D_t4260760469* value)
	{
		____buffer_1 = value;
		Il2CppCodeGenWriteBarrier(&____buffer_1, value);
	}

	inline static int32_t get_offset_of__disposed_2() { return static_cast<int32_t>(offsetof(RequestStream_t2929193945, ____disposed_2)); }
	inline bool get__disposed_2() const { return ____disposed_2; }
	inline bool* get_address_of__disposed_2() { return &____disposed_2; }
	inline void set__disposed_2(bool value)
	{
		____disposed_2 = value;
	}

	inline static int32_t get_offset_of__length_3() { return static_cast<int32_t>(offsetof(RequestStream_t2929193945, ____length_3)); }
	inline int32_t get__length_3() const { return ____length_3; }
	inline int32_t* get_address_of__length_3() { return &____length_3; }
	inline void set__length_3(int32_t value)
	{
		____length_3 = value;
	}

	inline static int32_t get_offset_of__offset_4() { return static_cast<int32_t>(offsetof(RequestStream_t2929193945, ____offset_4)); }
	inline int32_t get__offset_4() const { return ____offset_4; }
	inline int32_t* get_address_of__offset_4() { return &____offset_4; }
	inline void set__offset_4(int32_t value)
	{
		____offset_4 = value;
	}

	inline static int32_t get_offset_of__remainingBody_5() { return static_cast<int32_t>(offsetof(RequestStream_t2929193945, ____remainingBody_5)); }
	inline int64_t get__remainingBody_5() const { return ____remainingBody_5; }
	inline int64_t* get_address_of__remainingBody_5() { return &____remainingBody_5; }
	inline void set__remainingBody_5(int64_t value)
	{
		____remainingBody_5 = value;
	}

	inline static int32_t get_offset_of__stream_6() { return static_cast<int32_t>(offsetof(RequestStream_t2929193945, ____stream_6)); }
	inline Stream_t1561764144 * get__stream_6() const { return ____stream_6; }
	inline Stream_t1561764144 ** get_address_of__stream_6() { return &____stream_6; }
	inline void set__stream_6(Stream_t1561764144 * value)
	{
		____stream_6 = value;
		Il2CppCodeGenWriteBarrier(&____stream_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
