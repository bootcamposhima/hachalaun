﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.Ext/<ContainsTwice>c__AnonStorey21
struct U3CContainsTwiceU3Ec__AnonStorey21_t3499061934;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocketSharp.Ext/<ContainsTwice>c__AnonStorey21::.ctor()
extern "C"  void U3CContainsTwiceU3Ec__AnonStorey21__ctor_m878820989 (U3CContainsTwiceU3Ec__AnonStorey21_t3499061934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.Ext/<ContainsTwice>c__AnonStorey21::<>m__1(System.Int32)
extern "C"  bool U3CContainsTwiceU3Ec__AnonStorey21_U3CU3Em__1_m590985194 (U3CContainsTwiceU3Ec__AnonStorey21_t3499061934 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
