﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AI
struct AI_t2088;
// System.String
struct String_t;
// System.Collections.Generic.List`1<AIDatas>
struct List_1_t1082114449;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AIDatas4008896193.h"
#include "mscorlib_System_String7231557.h"

// System.Void AI::.ctor()
extern "C"  void AI__ctor_m1099683187 (AI_t2088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AIDatas AI::LoadAIData(System.String)
extern "C"  AIDatas_t4008896193  AI_LoadAIData_m1461124263 (Il2CppObject * __this /* static, unused */, String_t* ___filename0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AIDatas AI::LoadAIData2(System.String)
extern "C"  AIDatas_t4008896193  AI_LoadAIData2_m246710505 (Il2CppObject * __this /* static, unused */, String_t* ___filename0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<AIDatas> AI::LoadAllAidatas()
extern "C"  List_1_t1082114449 * AI_LoadAllAidatas_m685334393 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> AI::LoadAllAidatasPath()
extern "C"  List_1_t1375417109 * AI_LoadAllAidatasPath_m372536941 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AIDatas AI::CreateAIData()
extern "C"  AIDatas_t4008896193  AI_CreateAIData_m458330065 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AIDatas AI::CreateJsonAIData(System.String)
extern "C"  AIDatas_t4008896193  AI_CreateJsonAIData_m1772068297 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AIDatas AI::Write(System.Int32,System.Int32,System.Int32[],System.Int32[],AIDatas)
extern "C"  AIDatas_t4008896193  AI_Write_m3845981139 (Il2CppObject * __this /* static, unused */, int32_t ___charactornumber0, int32_t ___direction1, Int32U5BU5D_t3230847821* ___bullet2, Int32U5BU5D_t3230847821* ___position3, AIDatas_t4008896193  ___ads4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AIDatas AI::Write(System.Int32,System.Int32,AIDatas)
extern "C"  AIDatas_t4008896193  AI_Write_m326662771 (Il2CppObject * __this /* static, unused */, int32_t ___charactornumber0, int32_t ___direction1, AIDatas_t4008896193  ___ads2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AI::FileWrite(System.String,AIDatas)
extern "C"  void AI_FileWrite_m2575970937 (Il2CppObject * __this /* static, unused */, String_t* ___name0, AIDatas_t4008896193  ___ads1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AI::FileWrite2(System.String,AIDatas)
extern "C"  void AI_FileWrite2_m3302162701 (Il2CppObject * __this /* static, unused */, String_t* ___name0, AIDatas_t4008896193  ___ads1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
