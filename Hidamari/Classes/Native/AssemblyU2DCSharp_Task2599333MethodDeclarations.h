﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Task
struct Task_t2599333;

#include "codegen/il2cpp-codegen.h"

// System.Void Task::.ctor()
extern "C"  void Task__ctor_m675015318 (Task_t2599333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Task::Start()
extern "C"  void Task_Start_m3917120406 (Task_t2599333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Task::Update()
extern "C"  void Task_Update_m1177500471 (Task_t2599333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
