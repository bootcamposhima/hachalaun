﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.WebSocket
struct WebSocket_t1342580397;
// WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext
struct HttpListenerWebSocketContext_t1074545506;
// System.String
struct String_t;
// WebSocketSharp.Logger
struct Logger_t3695440972;
// WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext
struct TcpListenerWebSocketContext_t3782393199;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.EventHandler`1<WebSocketSharp.CloseEventArgs>
struct EventHandler_1_t2615270477;
// System.EventHandler`1<WebSocketSharp.ErrorEventArgs>
struct EventHandler_1_t569145917;
// System.EventHandler`1<WebSocketSharp.MessageEventArgs>
struct EventHandler_1_t181896286;
// System.EventHandler
struct EventHandler_t2463957060;
// WebSocketSharp.Net.CookieCollection
struct CookieCollection_t1136277956;
// System.Func`2<WebSocketSharp.Net.WebSockets.WebSocketContext,System.String>
struct Func_2_t636757868;
// System.Collections.Generic.IEnumerable`1<WebSocketSharp.Net.Cookie>
struct IEnumerable_1_t1083031107;
// WebSocketSharp.Net.NetworkCredential
struct NetworkCredential_t1204099087;
// System.Net.Security.RemoteCertificateValidationCallback
struct RemoteCertificateValidationCallback_t1894914657;
// System.Uri
struct Uri_t1116831938;
// WebSocketSharp.WebSocketFrame
struct WebSocketFrame_t778194306;
// System.Exception
struct Exception_t3991598821;
// WebSocketSharp.Net.WebSockets.WebSocketContext
struct WebSocketContext_t763725542;
// WebSocketSharp.HandshakeResponse
struct HandshakeResponse_t3229697822;
// WebSocketSharp.PayloadData
struct PayloadData_t39926750;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Action
struct Action_t3771233898;
// System.IO.Stream
struct Stream_t1561764144;
// WebSocketSharp.HandshakeRequest
struct HandshakeRequest_t1037477780;
// WebSocketSharp.MessageEventArgs
struct MessageEventArgs_t36945740;
// System.Action`1<System.Boolean>
struct Action_1_t872614854;
// WebSocketSharp.CloseEventArgs
struct CloseEventArgs_t2470319931;
// System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Byte[]>
struct Dictionary_2_t1115770063;
// System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.IO.Stream>
struct Dictionary_2_t2711741034;
// System.IO.FileInfo
struct FileInfo_t3233670074;
// WebSocketSharp.Net.Cookie
struct Cookie_t2077085446;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_WebSockets_Ht1074545506.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Logger3695440972.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_WebSockets_Tc3782393199.h"
#include "mscorlib_System_EventHandler2463957060.h"
#include "AssemblyU2DCSharp_WebSocketSharp_CompressionMethod2226596781.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocketState790259878.h"
#include "System_System_Net_Security_RemoteCertificateValida1894914657.h"
#include "AssemblyU2DCSharp_WebSocketSharp_WebSocketFrame778194306.h"
#include "mscorlib_System_Exception3991598821.h"
#include "AssemblyU2DCSharp_WebSocketSharp_CloseStatusCode3936110621.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_WebSockets_Web763725542.h"
#include "AssemblyU2DCSharp_WebSocketSharp_HandshakeResponse3229697822.h"
#include "AssemblyU2DCSharp_WebSocketSharp_PayloadData39926750.h"
#include "System_Core_System_Action3771233898.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_HttpStatusCod1625451593.h"
#include "AssemblyU2DCSharp_WebSocketSharp_MessageEventArgs36945740.h"
#include "AssemblyU2DCSharp_WebSocketSharp_HandshakeRequest1037477780.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Opcode3782140426.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Mask3422653544.h"
#include "AssemblyU2DCSharp_WebSocketSharp_CloseEventArgs2470319931.h"
#include "mscorlib_System_IO_FileInfo3233670074.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_Cookie2077085446.h"

// System.Void WebSocketSharp.WebSocket::.ctor(WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext,System.String,WebSocketSharp.Logger)
extern "C"  void WebSocket__ctor_m3396704322 (WebSocket_t1342580397 * __this, HttpListenerWebSocketContext_t1074545506 * ___context0, String_t* ___protocol1, Logger_t3695440972 * ___logger2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::.ctor(WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext,System.String,WebSocketSharp.Logger)
extern "C"  void WebSocket__ctor_m496774313 (WebSocket_t1342580397 * __this, TcpListenerWebSocketContext_t3782393199 * ___context0, String_t* ___protocol1, Logger_t3695440972 * ___logger2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::.ctor(System.String,System.String[])
extern "C"  void WebSocket__ctor_m2306802077 (WebSocket_t1342580397 * __this, String_t* ___url0, StringU5BU5D_t4054002952* ___protocols1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::add_OnClose(System.EventHandler`1<WebSocketSharp.CloseEventArgs>)
extern "C"  void WebSocket_add_OnClose_m3939987016 (WebSocket_t1342580397 * __this, EventHandler_1_t2615270477 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::remove_OnClose(System.EventHandler`1<WebSocketSharp.CloseEventArgs>)
extern "C"  void WebSocket_remove_OnClose_m2678343347 (WebSocket_t1342580397 * __this, EventHandler_1_t2615270477 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::add_OnError(System.EventHandler`1<WebSocketSharp.ErrorEventArgs>)
extern "C"  void WebSocket_add_OnError_m215455816 (WebSocket_t1342580397 * __this, EventHandler_1_t569145917 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::remove_OnError(System.EventHandler`1<WebSocketSharp.ErrorEventArgs>)
extern "C"  void WebSocket_remove_OnError_m3248779443 (WebSocket_t1342580397 * __this, EventHandler_1_t569145917 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::add_OnMessage(System.EventHandler`1<WebSocketSharp.MessageEventArgs>)
extern "C"  void WebSocket_add_OnMessage_m1138713256 (WebSocket_t1342580397 * __this, EventHandler_1_t181896286 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::remove_OnMessage(System.EventHandler`1<WebSocketSharp.MessageEventArgs>)
extern "C"  void WebSocket_remove_OnMessage_m2623802771 (WebSocket_t1342580397 * __this, EventHandler_1_t181896286 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::add_OnOpen(System.EventHandler)
extern "C"  void WebSocket_add_OnOpen_m3674536951 (WebSocket_t1342580397 * __this, EventHandler_t2463957060 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::remove_OnOpen(System.EventHandler)
extern "C"  void WebSocket_remove_OnOpen_m2525126434 (WebSocket_t1342580397 * __this, EventHandler_t2463957060 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::System.IDisposable.Dispose()
extern "C"  void WebSocket_System_IDisposable_Dispose_m2593617632 (WebSocket_t1342580397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.Net.CookieCollection WebSocketSharp.WebSocket::get_CookieCollection()
extern "C"  CookieCollection_t1136277956 * WebSocket_get_CookieCollection_m1369077125 (WebSocket_t1342580397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Func`2<WebSocketSharp.Net.WebSockets.WebSocketContext,System.String> WebSocketSharp.WebSocket::get_CustomHandshakeRequestChecker()
extern "C"  Func_2_t636757868 * WebSocket_get_CustomHandshakeRequestChecker_m1227021650 (WebSocket_t1342580397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::set_CustomHandshakeRequestChecker(System.Func`2<WebSocketSharp.Net.WebSockets.WebSocketContext,System.String>)
extern "C"  void WebSocket_set_CustomHandshakeRequestChecker_m2971497793 (WebSocket_t1342580397 * __this, Func_2_t636757868 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::get_IsConnected()
extern "C"  bool WebSocket_get_IsConnected_m2653755679 (WebSocket_t1342580397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.CompressionMethod WebSocketSharp.WebSocket::get_Compression()
extern "C"  uint8_t WebSocket_get_Compression_m3949946905 (WebSocket_t1342580397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::set_Compression(WebSocketSharp.CompressionMethod)
extern "C"  void WebSocket_set_Compression_m1365773706 (WebSocket_t1342580397 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<WebSocketSharp.Net.Cookie> WebSocketSharp.WebSocket::get_Cookies()
extern "C"  Il2CppObject* WebSocket_get_Cookies_m1919387799 (WebSocket_t1342580397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.Net.NetworkCredential WebSocketSharp.WebSocket::get_Credentials()
extern "C"  NetworkCredential_t1204099087 * WebSocket_get_Credentials_m1933776316 (WebSocket_t1342580397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.WebSocket::get_Extensions()
extern "C"  String_t* WebSocket_get_Extensions_m3314700805 (WebSocket_t1342580397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::get_IsAlive()
extern "C"  bool WebSocket_get_IsAlive_m2079412323 (WebSocket_t1342580397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::get_IsSecure()
extern "C"  bool WebSocket_get_IsSecure_m3548243843 (WebSocket_t1342580397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.Logger WebSocketSharp.WebSocket::get_Log()
extern "C"  Logger_t3695440972 * WebSocket_get_Log_m257468864 (WebSocket_t1342580397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::set_Log(WebSocketSharp.Logger)
extern "C"  void WebSocket_set_Log_m1960622227 (WebSocket_t1342580397 * __this, Logger_t3695440972 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.WebSocket::get_Origin()
extern "C"  String_t* WebSocket_get_Origin_m4272001175 (WebSocket_t1342580397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::set_Origin(System.String)
extern "C"  void WebSocket_set_Origin_m1309570778 (WebSocket_t1342580397 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.WebSocket::get_Protocol()
extern "C"  String_t* WebSocket_get_Protocol_m4186218377 (WebSocket_t1342580397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::set_Protocol(System.String)
extern "C"  void WebSocket_set_Protocol_m1846032744 (WebSocket_t1342580397 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.WebSocketState WebSocketSharp.WebSocket::get_ReadyState()
extern "C"  uint16_t WebSocket_get_ReadyState_m719238074 (WebSocket_t1342580397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.Security.RemoteCertificateValidationCallback WebSocketSharp.WebSocket::get_ServerCertificateValidationCallback()
extern "C"  RemoteCertificateValidationCallback_t1894914657 * WebSocket_get_ServerCertificateValidationCallback_m1521164420 (WebSocket_t1342580397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::set_ServerCertificateValidationCallback(System.Net.Security.RemoteCertificateValidationCallback)
extern "C"  void WebSocket_set_ServerCertificateValidationCallback_m3368301391 (WebSocket_t1342580397 * __this, RemoteCertificateValidationCallback_t1894914657 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri WebSocketSharp.WebSocket::get_Url()
extern "C"  Uri_t1116831938 * WebSocket_get_Url_m2061398483 (WebSocket_t1342580397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::acceptCloseFrame(WebSocketSharp.WebSocketFrame)
extern "C"  bool WebSocket_acceptCloseFrame_m1331471019 (WebSocket_t1342580397 * __this, WebSocketFrame_t778194306 * ___frame0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::acceptDataFrame(WebSocketSharp.WebSocketFrame)
extern "C"  bool WebSocket_acceptDataFrame_m1452099421 (WebSocket_t1342580397 * __this, WebSocketFrame_t778194306 * ___frame0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::acceptException(System.Exception,System.String)
extern "C"  void WebSocket_acceptException_m1180715184 (WebSocket_t1342580397 * __this, Exception_t3991598821 * ___exception0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::acceptFragmentedFrame(WebSocketSharp.WebSocketFrame)
extern "C"  bool WebSocket_acceptFragmentedFrame_m496170018 (WebSocket_t1342580397 * __this, WebSocketFrame_t778194306 * ___frame0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::acceptFragments(WebSocketSharp.WebSocketFrame)
extern "C"  bool WebSocket_acceptFragments_m359488285 (WebSocket_t1342580397 * __this, WebSocketFrame_t778194306 * ___first0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::acceptFrame(WebSocketSharp.WebSocketFrame)
extern "C"  bool WebSocket_acceptFrame_m511668371 (WebSocket_t1342580397 * __this, WebSocketFrame_t778194306 * ___frame0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::acceptHandshake()
extern "C"  bool WebSocket_acceptHandshake_m303332664 (WebSocket_t1342580397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::acceptPingFrame(WebSocketSharp.WebSocketFrame)
extern "C"  bool WebSocket_acceptPingFrame_m3791361317 (WebSocket_t1342580397 * __this, WebSocketFrame_t778194306 * ___frame0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::acceptPongFrame(WebSocketSharp.WebSocketFrame)
extern "C"  bool WebSocket_acceptPongFrame_m2550227115 (WebSocket_t1342580397 * __this, WebSocketFrame_t778194306 * ___frame0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::acceptSecWebSocketExtensionsHeader(System.String)
extern "C"  void WebSocket_acceptSecWebSocketExtensionsHeader_m3976552830 (WebSocket_t1342580397 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::acceptUnsupportedFrame(WebSocketSharp.WebSocketFrame,WebSocketSharp.CloseStatusCode,System.String)
extern "C"  bool WebSocket_acceptUnsupportedFrame_m1827787566 (WebSocket_t1342580397 * __this, WebSocketFrame_t778194306 * ___frame0, uint16_t ___code1, String_t* ___reason2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.WebSocket::checkIfAvailable(System.String,System.Boolean,System.Boolean)
extern "C"  String_t* WebSocket_checkIfAvailable_m3972221078 (WebSocket_t1342580397 * __this, String_t* ___operation0, bool ___availableAsServer1, bool ___availableAsConnected2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.WebSocket::checkIfCanConnect()
extern "C"  String_t* WebSocket_checkIfCanConnect_m4087900537 (WebSocket_t1342580397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.WebSocket::checkIfValidHandshakeRequest(WebSocketSharp.Net.WebSockets.WebSocketContext)
extern "C"  String_t* WebSocket_checkIfValidHandshakeRequest_m199201705 (WebSocket_t1342580397 * __this, WebSocketContext_t763725542 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.WebSocket::checkIfValidHandshakeResponse(WebSocketSharp.HandshakeResponse)
extern "C"  String_t* WebSocket_checkIfValidHandshakeResponse_m1340903150 (WebSocket_t1342580397 * __this, HandshakeResponse_t3229697822 * ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::close(WebSocketSharp.CloseStatusCode,System.String,System.Boolean)
extern "C"  void WebSocket_close_m1268085240 (WebSocket_t1342580397 * __this, uint16_t ___code0, String_t* ___reason1, bool ___wait2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::close(WebSocketSharp.PayloadData,System.Boolean,System.Boolean)
extern "C"  void WebSocket_close_m707576104 (WebSocket_t1342580397 * __this, PayloadData_t39926750 * ___payload0, bool ___send1, bool ___wait2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::closeAsync(WebSocketSharp.PayloadData,System.Boolean,System.Boolean)
extern "C"  void WebSocket_closeAsync_m1147887708 (WebSocket_t1342580397 * __this, PayloadData_t39926750 * ___payload0, bool ___send1, bool ___wait2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::closeClientResources()
extern "C"  void WebSocket_closeClientResources_m4025636903 (WebSocket_t1342580397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::closeHandshake(System.Byte[],System.Int32,System.Action)
extern "C"  bool WebSocket_closeHandshake_m1631040221 (WebSocket_t1342580397 * __this, ByteU5BU5D_t4260760469* ___frame0, int32_t ___timeout1, Action_t3771233898 * ___release2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::closeServerResources()
extern "C"  void WebSocket_closeServerResources_m2248524207 (WebSocket_t1342580397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::concatenateFragmentsInto(System.IO.Stream)
extern "C"  bool WebSocket_concatenateFragmentsInto_m70739806 (WebSocket_t1342580397 * __this, Stream_t1561764144 * ___dest0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::connect()
extern "C"  bool WebSocket_connect_m1396814739 (WebSocket_t1342580397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.WebSocket::createExtensionsRequest()
extern "C"  String_t* WebSocket_createExtensionsRequest_m292258777 (WebSocket_t1342580397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.HandshakeRequest WebSocketSharp.WebSocket::createHandshakeRequest()
extern "C"  HandshakeRequest_t1037477780 * WebSocket_createHandshakeRequest_m2249726057 (WebSocket_t1342580397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.HandshakeResponse WebSocketSharp.WebSocket::createHandshakeResponse()
extern "C"  HandshakeResponse_t3229697822 * WebSocket_createHandshakeResponse_m4091239465 (WebSocket_t1342580397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.HandshakeResponse WebSocketSharp.WebSocket::createHandshakeResponse(WebSocketSharp.Net.HttpStatusCode)
extern "C"  HandshakeResponse_t3229697822 * WebSocket_createHandshakeResponse_m1053144262 (WebSocket_t1342580397 * __this, int32_t ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.MessageEventArgs WebSocketSharp.WebSocket::dequeueFromMessageEventQueue()
extern "C"  MessageEventArgs_t36945740 * WebSocket_dequeueFromMessageEventQueue_m4080620833 (WebSocket_t1342580397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::doHandshake()
extern "C"  bool WebSocket_doHandshake_m4093491669 (WebSocket_t1342580397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::enqueueToMessageEventQueue(WebSocketSharp.MessageEventArgs)
extern "C"  void WebSocket_enqueueToMessageEventQueue_m3930529079 (WebSocket_t1342580397 * __this, MessageEventArgs_t36945740 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::error(System.String)
extern "C"  void WebSocket_error_m149167037 (WebSocket_t1342580397 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::init()
extern "C"  void WebSocket_init_m2877534261 (WebSocket_t1342580397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::open()
extern "C"  void WebSocket_open_m3051031279 (WebSocket_t1342580397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.HandshakeResponse WebSocketSharp.WebSocket::receiveHandshakeResponse()
extern "C"  HandshakeResponse_t3229697822 * WebSocket_receiveHandshakeResponse_m1807057802 (WebSocket_t1342580397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::send(System.Byte[])
extern "C"  bool WebSocket_send_m1644284424 (WebSocket_t1342580397 * __this, ByteU5BU5D_t4260760469* ___frame0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::send(WebSocketSharp.HandshakeRequest)
extern "C"  void WebSocket_send_m346101410 (WebSocket_t1342580397 * __this, HandshakeRequest_t1037477780 * ___request0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::send(WebSocketSharp.HandshakeResponse)
extern "C"  bool WebSocket_send_m288551060 (WebSocket_t1342580397 * __this, HandshakeResponse_t3229697822 * ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::send(WebSocketSharp.WebSocketFrame)
extern "C"  bool WebSocket_send_m2219182912 (WebSocket_t1342580397 * __this, WebSocketFrame_t778194306 * ___frame0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::send(WebSocketSharp.Opcode,System.Byte[])
extern "C"  bool WebSocket_send_m2882270907 (WebSocket_t1342580397 * __this, uint8_t ___opcode0, ByteU5BU5D_t4260760469* ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::send(WebSocketSharp.Opcode,System.IO.Stream)
extern "C"  bool WebSocket_send_m2473628261 (WebSocket_t1342580397 * __this, uint8_t ___opcode0, Stream_t1561764144 * ___stream1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::sendAsync(WebSocketSharp.Opcode,System.Byte[],System.Action`1<System.Boolean>)
extern "C"  void WebSocket_sendAsync_m2588890200 (WebSocket_t1342580397 * __this, uint8_t ___opcode0, ByteU5BU5D_t4260760469* ___data1, Action_1_t872614854 * ___completed2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::sendAsync(WebSocketSharp.Opcode,System.IO.Stream,System.Action`1<System.Boolean>)
extern "C"  void WebSocket_sendAsync_m1251110818 (WebSocket_t1342580397 * __this, uint8_t ___opcode0, Stream_t1561764144 * ___stream1, Action_1_t872614854 * ___completed2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::sendFragmented(WebSocketSharp.Opcode,System.IO.Stream,WebSocketSharp.Mask,System.Boolean)
extern "C"  bool WebSocket_sendFragmented_m3902794260 (WebSocket_t1342580397 * __this, uint8_t ___opcode0, Stream_t1561764144 * ___stream1, uint8_t ___mask2, bool ___compressed3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.HandshakeResponse WebSocketSharp.WebSocket::sendHandshakeRequest()
extern "C"  HandshakeResponse_t3229697822 * WebSocket_sendHandshakeRequest_m556619861 (WebSocket_t1342580397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.HandshakeResponse WebSocketSharp.WebSocket::sendHandshakeRequest(WebSocketSharp.HandshakeRequest)
extern "C"  HandshakeResponse_t3229697822 * WebSocket_sendHandshakeRequest_m4214597114 (WebSocket_t1342580397 * __this, HandshakeRequest_t1037477780 * ___request0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::setClientStream()
extern "C"  void WebSocket_setClientStream_m1110293962 (WebSocket_t1342580397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::startReceiving()
extern "C"  void WebSocket_startReceiving_m3343765315 (WebSocket_t1342580397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::validateSecWebSocketAcceptHeader(System.String)
extern "C"  bool WebSocket_validateSecWebSocketAcceptHeader_m2593449224 (WebSocket_t1342580397 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::validateSecWebSocketExtensionsHeader(System.String)
extern "C"  bool WebSocket_validateSecWebSocketExtensionsHeader_m3169128316 (WebSocket_t1342580397 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::validateSecWebSocketKeyHeader(System.String)
extern "C"  bool WebSocket_validateSecWebSocketKeyHeader_m1076494873 (WebSocket_t1342580397 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::validateSecWebSocketProtocolHeader(System.String)
extern "C"  bool WebSocket_validateSecWebSocketProtocolHeader_m3777734264 (WebSocket_t1342580397 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::validateSecWebSocketVersionClientHeader(System.String)
extern "C"  bool WebSocket_validateSecWebSocketVersionClientHeader_m2132125493 (WebSocket_t1342580397 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::validateSecWebSocketVersionServerHeader(System.String)
extern "C"  bool WebSocket_validateSecWebSocketVersionServerHeader_m3224946365 (WebSocket_t1342580397 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::Close(WebSocketSharp.HandshakeResponse)
extern "C"  void WebSocket_Close_m320732264 (WebSocket_t1342580397 * __this, HandshakeResponse_t3229697822 * ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::Close(WebSocketSharp.Net.HttpStatusCode)
extern "C"  void WebSocket_Close_m4199074618 (WebSocket_t1342580397 * __this, int32_t ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::Close(WebSocketSharp.CloseEventArgs,System.Byte[],System.Int32)
extern "C"  void WebSocket_Close_m1253619585 (WebSocket_t1342580397 * __this, CloseEventArgs_t2470319931 * ___e0, ByteU5BU5D_t4260760469* ___frame1, int32_t ___timeout2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::ConnectAsServer()
extern "C"  void WebSocket_ConnectAsServer_m789659036 (WebSocket_t1342580397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.WebSocket::CreateBase64Key()
extern "C"  String_t* WebSocket_CreateBase64Key_m565389870 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.WebSocket::CreateResponseKey(System.String)
extern "C"  String_t* WebSocket_CreateResponseKey_m666974534 (Il2CppObject * __this /* static, unused */, String_t* ___base64Key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::Ping(System.Byte[],System.Int32)
extern "C"  bool WebSocket_Ping_m3619812057 (WebSocket_t1342580397 * __this, ByteU5BU5D_t4260760469* ___frame0, int32_t ___timeout1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::Send(WebSocketSharp.Opcode,System.Byte[],System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.Byte[]>)
extern "C"  void WebSocket_Send_m2771733563 (WebSocket_t1342580397 * __this, uint8_t ___opcode0, ByteU5BU5D_t4260760469* ___data1, Dictionary_2_t1115770063 * ___cache2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::Send(WebSocketSharp.Opcode,System.IO.Stream,System.Collections.Generic.Dictionary`2<WebSocketSharp.CompressionMethod,System.IO.Stream>)
extern "C"  void WebSocket_Send_m698872459 (WebSocket_t1342580397 * __this, uint8_t ___opcode0, Stream_t1561764144 * ___stream1, Dictionary_2_t2711741034 * ___cache2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::Close()
extern "C"  void WebSocket_Close_m3887126325 (WebSocket_t1342580397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::Close(System.UInt16)
extern "C"  void WebSocket_Close_m338211039 (WebSocket_t1342580397 * __this, uint16_t ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::Close(WebSocketSharp.CloseStatusCode)
extern "C"  void WebSocket_Close_m3195629129 (WebSocket_t1342580397 * __this, uint16_t ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::Close(System.UInt16,System.String)
extern "C"  void WebSocket_Close_m3721357019 (WebSocket_t1342580397 * __this, uint16_t ___code0, String_t* ___reason1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::Close(WebSocketSharp.CloseStatusCode,System.String)
extern "C"  void WebSocket_Close_m1852531397 (WebSocket_t1342580397 * __this, uint16_t ___code0, String_t* ___reason1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::CloseAsync()
extern "C"  void WebSocket_CloseAsync_m1380802217 (WebSocket_t1342580397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::CloseAsync(System.UInt16)
extern "C"  void WebSocket_CloseAsync_m350152171 (WebSocket_t1342580397 * __this, uint16_t ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::CloseAsync(WebSocketSharp.CloseStatusCode)
extern "C"  void WebSocket_CloseAsync_m3945427645 (WebSocket_t1342580397 * __this, uint16_t ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::CloseAsync(System.UInt16,System.String)
extern "C"  void WebSocket_CloseAsync_m189650663 (WebSocket_t1342580397 * __this, uint16_t ___code0, String_t* ___reason1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::CloseAsync(WebSocketSharp.CloseStatusCode,System.String)
extern "C"  void WebSocket_CloseAsync_m913411129 (WebSocket_t1342580397 * __this, uint16_t ___code0, String_t* ___reason1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::SetHeader(System.String,System.String)
extern "C"  void WebSocket_SetHeader_m50374962 (WebSocket_t1342580397 * __this, String_t* ___header0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::Connect()
extern "C"  void WebSocket_Connect_m3106463399 (WebSocket_t1342580397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::ConnectAsync()
extern "C"  void WebSocket_ConnectAsync_m1615474295 (WebSocket_t1342580397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::Ping()
extern "C"  bool WebSocket_Ping_m348145387 (WebSocket_t1342580397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::Ping(System.String)
extern "C"  bool WebSocket_Ping_m1968909815 (WebSocket_t1342580397 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::Send(System.Byte[])
extern "C"  void WebSocket_Send_m3742182428 (WebSocket_t1342580397 * __this, ByteU5BU5D_t4260760469* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::Send(System.IO.FileInfo)
extern "C"  void WebSocket_Send_m7785242 (WebSocket_t1342580397 * __this, FileInfo_t3233670074 * ___file0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::Send(System.String)
extern "C"  void WebSocket_Send_m1505020757 (WebSocket_t1342580397 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::SendAsync(System.Byte[],System.Action`1<System.Boolean>)
extern "C"  void WebSocket_SendAsync_m1734499269 (WebSocket_t1342580397 * __this, ByteU5BU5D_t4260760469* ___data0, Action_1_t872614854 * ___completed1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::SendAsync(System.IO.FileInfo,System.Action`1<System.Boolean>)
extern "C"  void WebSocket_SendAsync_m1663627723 (WebSocket_t1342580397 * __this, FileInfo_t3233670074 * ___file0, Action_1_t872614854 * ___completed1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::SendAsync(System.String,System.Action`1<System.Boolean>)
extern "C"  void WebSocket_SendAsync_m2295781118 (WebSocket_t1342580397 * __this, String_t* ___data0, Action_1_t872614854 * ___completed1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::SendAsync(System.IO.Stream,System.Int32,System.Action`1<System.Boolean>)
extern "C"  void WebSocket_SendAsync_m172934556 (WebSocket_t1342580397 * __this, Stream_t1561764144 * ___stream0, int32_t ___length1, Action_1_t872614854 * ___completed2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::SetCookie(WebSocketSharp.Net.Cookie)
extern "C"  void WebSocket_SetCookie_m3263691247 (WebSocket_t1342580397 * __this, Cookie_t2077085446 * ___cookie0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket::SetCredentials(System.String,System.String,System.Boolean)
extern "C"  void WebSocket_SetCredentials_m4000006718 (WebSocket_t1342580397 * __this, String_t* ___username0, String_t* ___password1, bool ___preAuth2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.WebSocket::<get_CustomHandshakeRequestChecker>m__C(WebSocketSharp.Net.WebSockets.WebSocketContext)
extern "C"  String_t* WebSocket_U3Cget_CustomHandshakeRequestCheckerU3Em__C_m6268665 (Il2CppObject * __this /* static, unused */, WebSocketContext_t763725542 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::<acceptHandshake>m__D(System.String)
extern "C"  bool WebSocket_U3CacceptHandshakeU3Em__D_m2493968471 (WebSocket_t1342580397 * __this, String_t* ___protocol0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket::<validateSecWebSocketExtensionsHeader>m__12(System.String)
extern "C"  bool WebSocket_U3CvalidateSecWebSocketExtensionsHeaderU3Em__12_m468975296 (WebSocket_t1342580397 * __this, String_t* ___extension0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
