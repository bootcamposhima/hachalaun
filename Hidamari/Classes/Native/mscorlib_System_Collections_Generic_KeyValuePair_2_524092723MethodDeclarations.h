﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<WebSocketSharp.Net.ListenerPrefix,WebSocketSharp.Net.HttpListener>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m4107943233(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t524092723 *, ListenerPrefix_t2663314696 *, HttpListener_t398944510 *, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<WebSocketSharp.Net.ListenerPrefix,WebSocketSharp.Net.HttpListener>::get_Key()
#define KeyValuePair_2_get_Key_m3151780103(__this, method) ((  ListenerPrefix_t2663314696 * (*) (KeyValuePair_2_t524092723 *, const MethodInfo*))KeyValuePair_2_get_Key_m3256475977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<WebSocketSharp.Net.ListenerPrefix,WebSocketSharp.Net.HttpListener>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3554809928(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t524092723 *, ListenerPrefix_t2663314696 *, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<WebSocketSharp.Net.ListenerPrefix,WebSocketSharp.Net.HttpListener>::get_Value()
#define KeyValuePair_2_get_Value_m539795051(__this, method) ((  HttpListener_t398944510 * (*) (KeyValuePair_2_t524092723 *, const MethodInfo*))KeyValuePair_2_get_Value_m3899079597_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<WebSocketSharp.Net.ListenerPrefix,WebSocketSharp.Net.HttpListener>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1567098952(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t524092723 *, HttpListener_t398944510 *, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<WebSocketSharp.Net.ListenerPrefix,WebSocketSharp.Net.HttpListener>::ToString()
#define KeyValuePair_2_ToString_m2629543296(__this, method) ((  String_t* (*) (KeyValuePair_2_t524092723 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
