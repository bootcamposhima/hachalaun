﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_gen2112091504MethodDeclarations.h"

// System.Void System.Collections.Generic.Queue`1<SocketIO.SocketIOEvent>::.ctor()
#define Queue_1__ctor_m2771211506(__this, method) ((  void (*) (Queue_1_t1953129196 *, const MethodInfo*))Queue_1__ctor_m3042804833_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<SocketIO.SocketIOEvent>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Queue_1_System_Collections_ICollection_CopyTo_m3462729457(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t1953129196 *, Il2CppArray *, int32_t, const MethodInfo*))Queue_1_System_Collections_ICollection_CopyTo_m3260144643_gshared)(__this, ___array0, ___idx1, method)
// System.Boolean System.Collections.Generic.Queue`1<SocketIO.SocketIOEvent>::System.Collections.ICollection.get_IsSynchronized()
#define Queue_1_System_Collections_ICollection_get_IsSynchronized_m112490433(__this, method) ((  bool (*) (Queue_1_t1953129196 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_IsSynchronized_m63917275_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1<SocketIO.SocketIOEvent>::System.Collections.ICollection.get_SyncRoot()
#define Queue_1_System_Collections_ICollection_get_SyncRoot_m2209308353(__this, method) ((  Il2CppObject * (*) (Queue_1_t1953129196 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_SyncRoot_m2093948217_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<SocketIO.SocketIOEvent>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2588240709(__this, method) ((  Il2CppObject* (*) (Queue_1_t1953129196 *, const MethodInfo*))Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m472615211_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<SocketIO.SocketIOEvent>::System.Collections.IEnumerable.GetEnumerator()
#define Queue_1_System_Collections_IEnumerable_GetEnumerator_m221660992(__this, method) ((  Il2CppObject * (*) (Queue_1_t1953129196 *, const MethodInfo*))Queue_1_System_Collections_IEnumerable_GetEnumerator_m3688614462_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<SocketIO.SocketIOEvent>::Clear()
#define Queue_1_Clear_m2470319582(__this, method) ((  void (*) (Queue_1_t1953129196 *, const MethodInfo*))Queue_1_Clear_m448938124_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<SocketIO.SocketIOEvent>::CopyTo(T[],System.Int32)
#define Queue_1_CopyTo_m1525172572(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t1953129196 *, SocketIOEventU5BU5D_t3010781814*, int32_t, const MethodInfo*))Queue_1_CopyTo_m3592753262_gshared)(__this, ___array0, ___idx1, method)
// T System.Collections.Generic.Queue`1<SocketIO.SocketIOEvent>::Dequeue()
#define Queue_1_Dequeue_m2749530642(__this, method) ((  SocketIOEvent_t4011854063 * (*) (Queue_1_t1953129196 *, const MethodInfo*))Queue_1_Dequeue_m102813934_gshared)(__this, method)
// T System.Collections.Generic.Queue`1<SocketIO.SocketIOEvent>::Peek()
#define Queue_1_Peek_m2169025099(__this, method) ((  SocketIOEvent_t4011854063 * (*) (Queue_1_t1953129196 *, const MethodInfo*))Queue_1_Peek_m3013356031_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<SocketIO.SocketIOEvent>::Enqueue(T)
#define Queue_1_Enqueue_m1559728201(__this, ___item0, method) ((  void (*) (Queue_1_t1953129196 *, SocketIOEvent_t4011854063 *, const MethodInfo*))Queue_1_Enqueue_m4079343671_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Queue`1<SocketIO.SocketIOEvent>::SetCapacity(System.Int32)
#define Queue_1_SetCapacity_m1903198430(__this, ___new_size0, method) ((  void (*) (Queue_1_t1953129196 *, int32_t, const MethodInfo*))Queue_1_SetCapacity_m1573690380_gshared)(__this, ___new_size0, method)
// System.Int32 System.Collections.Generic.Queue`1<SocketIO.SocketIOEvent>::get_Count()
#define Queue_1_get_Count_m387962408(__this, method) ((  int32_t (*) (Queue_1_t1953129196 *, const MethodInfo*))Queue_1_get_Count_m1429559317_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<SocketIO.SocketIOEvent>::GetEnumerator()
#define Queue_1_GetEnumerator_m696021554(__this, method) ((  Enumerator_t3242214708  (*) (Queue_1_t1953129196 *, const MethodInfo*))Queue_1_GetEnumerator_m3965043378_gshared)(__this, method)
