﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Int32[]
struct Int32U5BU5D_t3230847821;
// AIData
struct AIData_t1930434546;
struct AIData_t1930434546_marshaled_pinvoke;
struct AIData_t1930434546_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AIData1930434546.h"

// System.Void AIData::.ctor(System.Int32,System.Int32,System.Int32[],System.Int32[])
extern "C"  void AIData__ctor_m1371294973 (AIData_t1930434546 * __this, int32_t ___charactornumber0, int32_t ___direction1, Int32U5BU5D_t3230847821* ___bullet2, Int32U5BU5D_t3230847821* ___position3, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct AIData_t1930434546;
struct AIData_t1930434546_marshaled_pinvoke;

extern "C" void AIData_t1930434546_marshal_pinvoke(const AIData_t1930434546& unmarshaled, AIData_t1930434546_marshaled_pinvoke& marshaled);
extern "C" void AIData_t1930434546_marshal_pinvoke_back(const AIData_t1930434546_marshaled_pinvoke& marshaled, AIData_t1930434546& unmarshaled);
extern "C" void AIData_t1930434546_marshal_pinvoke_cleanup(AIData_t1930434546_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct AIData_t1930434546;
struct AIData_t1930434546_marshaled_com;

extern "C" void AIData_t1930434546_marshal_com(const AIData_t1930434546& unmarshaled, AIData_t1930434546_marshaled_com& marshaled);
extern "C" void AIData_t1930434546_marshal_com_back(const AIData_t1930434546_marshaled_com& marshaled, AIData_t1930434546& unmarshaled);
extern "C" void AIData_t1930434546_marshal_com_cleanup(AIData_t1930434546_marshaled_com& marshaled);
