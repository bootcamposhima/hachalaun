﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.Generic.List`1<AIDatas>
struct List_1_t1082114449;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlaySelect
struct  PlaySelect_t3562688880  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.GameObject PlaySelect::_SelectUICamera
	GameObject_t3674682005 * ____SelectUICamera_2;
	// UnityEngine.GameObject PlaySelect::_MainCamera
	GameObject_t3674682005 * ____MainCamera_3;
	// UnityEngine.GameObject PlaySelect::_UICamera
	GameObject_t3674682005 * ____UICamera_4;
	// UnityEngine.GameObject PlaySelect::_EnemyManager
	GameObject_t3674682005 * ____EnemyManager_5;
	// UnityEngine.GameObject PlaySelect::_DefenceManager
	GameObject_t3674682005 * ____DefenceManager_6;
	// UnityEngine.GameObject PlaySelect::_round
	GameObject_t3674682005 * ____round_7;
	// UnityEngine.GameObject PlaySelect::_StartText
	GameObject_t3674682005 * ____StartText_8;
	// UnityEngine.GameObject PlaySelect::_SkipSpeed
	GameObject_t3674682005 * ____SkipSpeed_9;
	// UnityEngine.GameObject PlaySelect::_NotSelectImage
	GameObject_t3674682005 * ____NotSelectImage_10;
	// UnityEngine.GameObject PlaySelect::_AIScroll
	GameObject_t3674682005 * ____AIScroll_11;
	// UnityEngine.GameObject PlaySelect::_Go
	GameObject_t3674682005 * ____Go_12;
	// UnityEngine.GameObject PlaySelect::waku1
	GameObject_t3674682005 * ___waku1_13;
	// UnityEngine.GameObject PlaySelect::waku2
	GameObject_t3674682005 * ___waku2_14;
	// UnityEngine.GameObject PlaySelect::_demoobject
	GameObject_t3674682005 * ____demoobject_15;
	// System.Collections.Generic.List`1<AIDatas> PlaySelect::list
	List_1_t1082114449 * ___list_16;
	// System.Int32 PlaySelect::_ai
	int32_t ____ai_17;
	// System.Int32 PlaySelect::_pl
	int32_t ____pl_18;
	// System.Boolean PlaySelect::_isAttack
	bool ____isAttack_19;
	// System.Boolean PlaySelect::_isFight
	bool ____isFight_20;
	// System.Boolean PlaySelect::_isStartOK
	bool ____isStartOK_21;
	// System.Boolean PlaySelect::_isStart
	bool ____isStart_22;
	// System.Boolean PlaySelect::_isChangeGameStart
	bool ____isChangeGameStart_23;
	// System.Boolean PlaySelect::_isChangeShow
	bool ____isChangeShow_24;
	// System.Single PlaySelect::rectX_s
	float ___rectX_s_25;
	// System.Single PlaySelect::rectX_m
	float ___rectX_m_26;
	// System.Single PlaySelect::s_rectX_m
	float ___s_rectX_m_27;
	// System.Single PlaySelect::s_rectX_s
	float ___s_rectX_s_28;
	// System.Single PlaySelect::s_view_m
	float ___s_view_m_29;
	// System.Boolean PlaySelect::_isFarst
	bool ____isFarst_30;
	// System.Collections.IEnumerator PlaySelect::ie
	Il2CppObject * ___ie_31;

public:
	inline static int32_t get_offset_of__SelectUICamera_2() { return static_cast<int32_t>(offsetof(PlaySelect_t3562688880, ____SelectUICamera_2)); }
	inline GameObject_t3674682005 * get__SelectUICamera_2() const { return ____SelectUICamera_2; }
	inline GameObject_t3674682005 ** get_address_of__SelectUICamera_2() { return &____SelectUICamera_2; }
	inline void set__SelectUICamera_2(GameObject_t3674682005 * value)
	{
		____SelectUICamera_2 = value;
		Il2CppCodeGenWriteBarrier(&____SelectUICamera_2, value);
	}

	inline static int32_t get_offset_of__MainCamera_3() { return static_cast<int32_t>(offsetof(PlaySelect_t3562688880, ____MainCamera_3)); }
	inline GameObject_t3674682005 * get__MainCamera_3() const { return ____MainCamera_3; }
	inline GameObject_t3674682005 ** get_address_of__MainCamera_3() { return &____MainCamera_3; }
	inline void set__MainCamera_3(GameObject_t3674682005 * value)
	{
		____MainCamera_3 = value;
		Il2CppCodeGenWriteBarrier(&____MainCamera_3, value);
	}

	inline static int32_t get_offset_of__UICamera_4() { return static_cast<int32_t>(offsetof(PlaySelect_t3562688880, ____UICamera_4)); }
	inline GameObject_t3674682005 * get__UICamera_4() const { return ____UICamera_4; }
	inline GameObject_t3674682005 ** get_address_of__UICamera_4() { return &____UICamera_4; }
	inline void set__UICamera_4(GameObject_t3674682005 * value)
	{
		____UICamera_4 = value;
		Il2CppCodeGenWriteBarrier(&____UICamera_4, value);
	}

	inline static int32_t get_offset_of__EnemyManager_5() { return static_cast<int32_t>(offsetof(PlaySelect_t3562688880, ____EnemyManager_5)); }
	inline GameObject_t3674682005 * get__EnemyManager_5() const { return ____EnemyManager_5; }
	inline GameObject_t3674682005 ** get_address_of__EnemyManager_5() { return &____EnemyManager_5; }
	inline void set__EnemyManager_5(GameObject_t3674682005 * value)
	{
		____EnemyManager_5 = value;
		Il2CppCodeGenWriteBarrier(&____EnemyManager_5, value);
	}

	inline static int32_t get_offset_of__DefenceManager_6() { return static_cast<int32_t>(offsetof(PlaySelect_t3562688880, ____DefenceManager_6)); }
	inline GameObject_t3674682005 * get__DefenceManager_6() const { return ____DefenceManager_6; }
	inline GameObject_t3674682005 ** get_address_of__DefenceManager_6() { return &____DefenceManager_6; }
	inline void set__DefenceManager_6(GameObject_t3674682005 * value)
	{
		____DefenceManager_6 = value;
		Il2CppCodeGenWriteBarrier(&____DefenceManager_6, value);
	}

	inline static int32_t get_offset_of__round_7() { return static_cast<int32_t>(offsetof(PlaySelect_t3562688880, ____round_7)); }
	inline GameObject_t3674682005 * get__round_7() const { return ____round_7; }
	inline GameObject_t3674682005 ** get_address_of__round_7() { return &____round_7; }
	inline void set__round_7(GameObject_t3674682005 * value)
	{
		____round_7 = value;
		Il2CppCodeGenWriteBarrier(&____round_7, value);
	}

	inline static int32_t get_offset_of__StartText_8() { return static_cast<int32_t>(offsetof(PlaySelect_t3562688880, ____StartText_8)); }
	inline GameObject_t3674682005 * get__StartText_8() const { return ____StartText_8; }
	inline GameObject_t3674682005 ** get_address_of__StartText_8() { return &____StartText_8; }
	inline void set__StartText_8(GameObject_t3674682005 * value)
	{
		____StartText_8 = value;
		Il2CppCodeGenWriteBarrier(&____StartText_8, value);
	}

	inline static int32_t get_offset_of__SkipSpeed_9() { return static_cast<int32_t>(offsetof(PlaySelect_t3562688880, ____SkipSpeed_9)); }
	inline GameObject_t3674682005 * get__SkipSpeed_9() const { return ____SkipSpeed_9; }
	inline GameObject_t3674682005 ** get_address_of__SkipSpeed_9() { return &____SkipSpeed_9; }
	inline void set__SkipSpeed_9(GameObject_t3674682005 * value)
	{
		____SkipSpeed_9 = value;
		Il2CppCodeGenWriteBarrier(&____SkipSpeed_9, value);
	}

	inline static int32_t get_offset_of__NotSelectImage_10() { return static_cast<int32_t>(offsetof(PlaySelect_t3562688880, ____NotSelectImage_10)); }
	inline GameObject_t3674682005 * get__NotSelectImage_10() const { return ____NotSelectImage_10; }
	inline GameObject_t3674682005 ** get_address_of__NotSelectImage_10() { return &____NotSelectImage_10; }
	inline void set__NotSelectImage_10(GameObject_t3674682005 * value)
	{
		____NotSelectImage_10 = value;
		Il2CppCodeGenWriteBarrier(&____NotSelectImage_10, value);
	}

	inline static int32_t get_offset_of__AIScroll_11() { return static_cast<int32_t>(offsetof(PlaySelect_t3562688880, ____AIScroll_11)); }
	inline GameObject_t3674682005 * get__AIScroll_11() const { return ____AIScroll_11; }
	inline GameObject_t3674682005 ** get_address_of__AIScroll_11() { return &____AIScroll_11; }
	inline void set__AIScroll_11(GameObject_t3674682005 * value)
	{
		____AIScroll_11 = value;
		Il2CppCodeGenWriteBarrier(&____AIScroll_11, value);
	}

	inline static int32_t get_offset_of__Go_12() { return static_cast<int32_t>(offsetof(PlaySelect_t3562688880, ____Go_12)); }
	inline GameObject_t3674682005 * get__Go_12() const { return ____Go_12; }
	inline GameObject_t3674682005 ** get_address_of__Go_12() { return &____Go_12; }
	inline void set__Go_12(GameObject_t3674682005 * value)
	{
		____Go_12 = value;
		Il2CppCodeGenWriteBarrier(&____Go_12, value);
	}

	inline static int32_t get_offset_of_waku1_13() { return static_cast<int32_t>(offsetof(PlaySelect_t3562688880, ___waku1_13)); }
	inline GameObject_t3674682005 * get_waku1_13() const { return ___waku1_13; }
	inline GameObject_t3674682005 ** get_address_of_waku1_13() { return &___waku1_13; }
	inline void set_waku1_13(GameObject_t3674682005 * value)
	{
		___waku1_13 = value;
		Il2CppCodeGenWriteBarrier(&___waku1_13, value);
	}

	inline static int32_t get_offset_of_waku2_14() { return static_cast<int32_t>(offsetof(PlaySelect_t3562688880, ___waku2_14)); }
	inline GameObject_t3674682005 * get_waku2_14() const { return ___waku2_14; }
	inline GameObject_t3674682005 ** get_address_of_waku2_14() { return &___waku2_14; }
	inline void set_waku2_14(GameObject_t3674682005 * value)
	{
		___waku2_14 = value;
		Il2CppCodeGenWriteBarrier(&___waku2_14, value);
	}

	inline static int32_t get_offset_of__demoobject_15() { return static_cast<int32_t>(offsetof(PlaySelect_t3562688880, ____demoobject_15)); }
	inline GameObject_t3674682005 * get__demoobject_15() const { return ____demoobject_15; }
	inline GameObject_t3674682005 ** get_address_of__demoobject_15() { return &____demoobject_15; }
	inline void set__demoobject_15(GameObject_t3674682005 * value)
	{
		____demoobject_15 = value;
		Il2CppCodeGenWriteBarrier(&____demoobject_15, value);
	}

	inline static int32_t get_offset_of_list_16() { return static_cast<int32_t>(offsetof(PlaySelect_t3562688880, ___list_16)); }
	inline List_1_t1082114449 * get_list_16() const { return ___list_16; }
	inline List_1_t1082114449 ** get_address_of_list_16() { return &___list_16; }
	inline void set_list_16(List_1_t1082114449 * value)
	{
		___list_16 = value;
		Il2CppCodeGenWriteBarrier(&___list_16, value);
	}

	inline static int32_t get_offset_of__ai_17() { return static_cast<int32_t>(offsetof(PlaySelect_t3562688880, ____ai_17)); }
	inline int32_t get__ai_17() const { return ____ai_17; }
	inline int32_t* get_address_of__ai_17() { return &____ai_17; }
	inline void set__ai_17(int32_t value)
	{
		____ai_17 = value;
	}

	inline static int32_t get_offset_of__pl_18() { return static_cast<int32_t>(offsetof(PlaySelect_t3562688880, ____pl_18)); }
	inline int32_t get__pl_18() const { return ____pl_18; }
	inline int32_t* get_address_of__pl_18() { return &____pl_18; }
	inline void set__pl_18(int32_t value)
	{
		____pl_18 = value;
	}

	inline static int32_t get_offset_of__isAttack_19() { return static_cast<int32_t>(offsetof(PlaySelect_t3562688880, ____isAttack_19)); }
	inline bool get__isAttack_19() const { return ____isAttack_19; }
	inline bool* get_address_of__isAttack_19() { return &____isAttack_19; }
	inline void set__isAttack_19(bool value)
	{
		____isAttack_19 = value;
	}

	inline static int32_t get_offset_of__isFight_20() { return static_cast<int32_t>(offsetof(PlaySelect_t3562688880, ____isFight_20)); }
	inline bool get__isFight_20() const { return ____isFight_20; }
	inline bool* get_address_of__isFight_20() { return &____isFight_20; }
	inline void set__isFight_20(bool value)
	{
		____isFight_20 = value;
	}

	inline static int32_t get_offset_of__isStartOK_21() { return static_cast<int32_t>(offsetof(PlaySelect_t3562688880, ____isStartOK_21)); }
	inline bool get__isStartOK_21() const { return ____isStartOK_21; }
	inline bool* get_address_of__isStartOK_21() { return &____isStartOK_21; }
	inline void set__isStartOK_21(bool value)
	{
		____isStartOK_21 = value;
	}

	inline static int32_t get_offset_of__isStart_22() { return static_cast<int32_t>(offsetof(PlaySelect_t3562688880, ____isStart_22)); }
	inline bool get__isStart_22() const { return ____isStart_22; }
	inline bool* get_address_of__isStart_22() { return &____isStart_22; }
	inline void set__isStart_22(bool value)
	{
		____isStart_22 = value;
	}

	inline static int32_t get_offset_of__isChangeGameStart_23() { return static_cast<int32_t>(offsetof(PlaySelect_t3562688880, ____isChangeGameStart_23)); }
	inline bool get__isChangeGameStart_23() const { return ____isChangeGameStart_23; }
	inline bool* get_address_of__isChangeGameStart_23() { return &____isChangeGameStart_23; }
	inline void set__isChangeGameStart_23(bool value)
	{
		____isChangeGameStart_23 = value;
	}

	inline static int32_t get_offset_of__isChangeShow_24() { return static_cast<int32_t>(offsetof(PlaySelect_t3562688880, ____isChangeShow_24)); }
	inline bool get__isChangeShow_24() const { return ____isChangeShow_24; }
	inline bool* get_address_of__isChangeShow_24() { return &____isChangeShow_24; }
	inline void set__isChangeShow_24(bool value)
	{
		____isChangeShow_24 = value;
	}

	inline static int32_t get_offset_of_rectX_s_25() { return static_cast<int32_t>(offsetof(PlaySelect_t3562688880, ___rectX_s_25)); }
	inline float get_rectX_s_25() const { return ___rectX_s_25; }
	inline float* get_address_of_rectX_s_25() { return &___rectX_s_25; }
	inline void set_rectX_s_25(float value)
	{
		___rectX_s_25 = value;
	}

	inline static int32_t get_offset_of_rectX_m_26() { return static_cast<int32_t>(offsetof(PlaySelect_t3562688880, ___rectX_m_26)); }
	inline float get_rectX_m_26() const { return ___rectX_m_26; }
	inline float* get_address_of_rectX_m_26() { return &___rectX_m_26; }
	inline void set_rectX_m_26(float value)
	{
		___rectX_m_26 = value;
	}

	inline static int32_t get_offset_of_s_rectX_m_27() { return static_cast<int32_t>(offsetof(PlaySelect_t3562688880, ___s_rectX_m_27)); }
	inline float get_s_rectX_m_27() const { return ___s_rectX_m_27; }
	inline float* get_address_of_s_rectX_m_27() { return &___s_rectX_m_27; }
	inline void set_s_rectX_m_27(float value)
	{
		___s_rectX_m_27 = value;
	}

	inline static int32_t get_offset_of_s_rectX_s_28() { return static_cast<int32_t>(offsetof(PlaySelect_t3562688880, ___s_rectX_s_28)); }
	inline float get_s_rectX_s_28() const { return ___s_rectX_s_28; }
	inline float* get_address_of_s_rectX_s_28() { return &___s_rectX_s_28; }
	inline void set_s_rectX_s_28(float value)
	{
		___s_rectX_s_28 = value;
	}

	inline static int32_t get_offset_of_s_view_m_29() { return static_cast<int32_t>(offsetof(PlaySelect_t3562688880, ___s_view_m_29)); }
	inline float get_s_view_m_29() const { return ___s_view_m_29; }
	inline float* get_address_of_s_view_m_29() { return &___s_view_m_29; }
	inline void set_s_view_m_29(float value)
	{
		___s_view_m_29 = value;
	}

	inline static int32_t get_offset_of__isFarst_30() { return static_cast<int32_t>(offsetof(PlaySelect_t3562688880, ____isFarst_30)); }
	inline bool get__isFarst_30() const { return ____isFarst_30; }
	inline bool* get_address_of__isFarst_30() { return &____isFarst_30; }
	inline void set__isFarst_30(bool value)
	{
		____isFarst_30 = value;
	}

	inline static int32_t get_offset_of_ie_31() { return static_cast<int32_t>(offsetof(PlaySelect_t3562688880, ___ie_31)); }
	inline Il2CppObject * get_ie_31() const { return ___ie_31; }
	inline Il2CppObject ** get_address_of_ie_31() { return &___ie_31; }
	inline void set_ie_31(Il2CppObject * value)
	{
		___ie_31 = value;
		Il2CppCodeGenWriteBarrier(&___ie_31, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
