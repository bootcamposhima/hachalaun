﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Char>
struct Dictionary_2_t737694438;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1352630492.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Char>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m289353290_gshared (Enumerator_t1352630492 * __this, Dictionary_2_t737694438 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m289353290(__this, ___host0, method) ((  void (*) (Enumerator_t1352630492 *, Dictionary_2_t737694438 *, const MethodInfo*))Enumerator__ctor_m289353290_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Char>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m41324823_gshared (Enumerator_t1352630492 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m41324823(__this, method) ((  Il2CppObject * (*) (Enumerator_t1352630492 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m41324823_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Char>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1760479275_gshared (Enumerator_t1352630492 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1760479275(__this, method) ((  void (*) (Enumerator_t1352630492 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1760479275_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Char>::Dispose()
extern "C"  void Enumerator_Dispose_m223247212_gshared (Enumerator_t1352630492 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m223247212(__this, method) ((  void (*) (Enumerator_t1352630492 *, const MethodInfo*))Enumerator_Dispose_m223247212_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Char>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1667874455_gshared (Enumerator_t1352630492 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1667874455(__this, method) ((  bool (*) (Enumerator_t1352630492 *, const MethodInfo*))Enumerator_MoveNext_m1667874455_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Char>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m4064068829_gshared (Enumerator_t1352630492 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m4064068829(__this, method) ((  Il2CppObject * (*) (Enumerator_t1352630492 *, const MethodInfo*))Enumerator_get_Current_m4064068829_gshared)(__this, method)
