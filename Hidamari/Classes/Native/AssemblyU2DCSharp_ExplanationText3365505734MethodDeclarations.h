﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ExplanationText
struct ExplanationText_t3365505734;

#include "codegen/il2cpp-codegen.h"

// System.Void ExplanationText::.ctor()
extern "C"  void ExplanationText__ctor_m762772325 (ExplanationText_t3365505734 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExplanationText::Start()
extern "C"  void ExplanationText_Start_m4004877413 (ExplanationText_t3365505734 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExplanationText::Update()
extern "C"  void ExplanationText_Update_m3897967688 (ExplanationText_t3365505734 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExplanationText::NormalSelect()
extern "C"  void ExplanationText_NormalSelect_m2792178498 (ExplanationText_t3365505734 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExplanationText::FightSelect()
extern "C"  void ExplanationText_FightSelect_m1678907695 (ExplanationText_t3365505734 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExplanationText::PracticeSelect()
extern "C"  void ExplanationText_PracticeSelect_m1103485974 (ExplanationText_t3365505734 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExplanationText::GoAnimation()
extern "C"  void ExplanationText_GoAnimation_m2294184831 (ExplanationText_t3365505734 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ExplanationText::GoSelect()
extern "C"  void ExplanationText_GoSelect_m463982915 (ExplanationText_t3365505734 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
