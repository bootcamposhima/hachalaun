﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2791238869.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_AIDatas4008896193.h"

// System.Void System.Array/InternalEnumerator`1<AIDatas>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m796802822_gshared (InternalEnumerator_1_t2791238869 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m796802822(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2791238869 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m796802822_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<AIDatas>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1637653722_gshared (InternalEnumerator_1_t2791238869 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1637653722(__this, method) ((  void (*) (InternalEnumerator_1_t2791238869 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1637653722_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<AIDatas>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2787797200_gshared (InternalEnumerator_1_t2791238869 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2787797200(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2791238869 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2787797200_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<AIDatas>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m906475613_gshared (InternalEnumerator_1_t2791238869 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m906475613(__this, method) ((  void (*) (InternalEnumerator_1_t2791238869 *, const MethodInfo*))InternalEnumerator_1_Dispose_m906475613_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<AIDatas>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3913430666_gshared (InternalEnumerator_1_t2791238869 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3913430666(__this, method) ((  bool (*) (InternalEnumerator_1_t2791238869 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3913430666_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<AIDatas>::get_Current()
extern "C"  AIDatas_t4008896193  InternalEnumerator_1_get_Current_m2745372463_gshared (InternalEnumerator_1_t2791238869 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2745372463(__this, method) ((  AIDatas_t4008896193  (*) (InternalEnumerator_1_t2791238869 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2745372463_gshared)(__this, method)
