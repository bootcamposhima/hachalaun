﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.AsyncCallback
struct AsyncCallback_t1369114871;
// System.Object
struct Il2CppObject;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t924017833;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Exception
struct Exception_t3991598821;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpStreamAsyncResult
struct  HttpStreamAsyncResult_t303264603  : public Il2CppObject
{
public:
	// System.AsyncCallback WebSocketSharp.Net.HttpStreamAsyncResult::_callback
	AsyncCallback_t1369114871 * ____callback_0;
	// System.Boolean WebSocketSharp.Net.HttpStreamAsyncResult::_completed
	bool ____completed_1;
	// System.Object WebSocketSharp.Net.HttpStreamAsyncResult::_state
	Il2CppObject * ____state_2;
	// System.Object WebSocketSharp.Net.HttpStreamAsyncResult::_sync
	Il2CppObject * ____sync_3;
	// System.Threading.ManualResetEvent WebSocketSharp.Net.HttpStreamAsyncResult::_waitHandle
	ManualResetEvent_t924017833 * ____waitHandle_4;
	// System.Byte[] WebSocketSharp.Net.HttpStreamAsyncResult::Buffer
	ByteU5BU5D_t4260760469* ___Buffer_5;
	// System.Int32 WebSocketSharp.Net.HttpStreamAsyncResult::Count
	int32_t ___Count_6;
	// System.Exception WebSocketSharp.Net.HttpStreamAsyncResult::Error
	Exception_t3991598821 * ___Error_7;
	// System.Int32 WebSocketSharp.Net.HttpStreamAsyncResult::Offset
	int32_t ___Offset_8;
	// System.Int32 WebSocketSharp.Net.HttpStreamAsyncResult::SyncRead
	int32_t ___SyncRead_9;

public:
	inline static int32_t get_offset_of__callback_0() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t303264603, ____callback_0)); }
	inline AsyncCallback_t1369114871 * get__callback_0() const { return ____callback_0; }
	inline AsyncCallback_t1369114871 ** get_address_of__callback_0() { return &____callback_0; }
	inline void set__callback_0(AsyncCallback_t1369114871 * value)
	{
		____callback_0 = value;
		Il2CppCodeGenWriteBarrier(&____callback_0, value);
	}

	inline static int32_t get_offset_of__completed_1() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t303264603, ____completed_1)); }
	inline bool get__completed_1() const { return ____completed_1; }
	inline bool* get_address_of__completed_1() { return &____completed_1; }
	inline void set__completed_1(bool value)
	{
		____completed_1 = value;
	}

	inline static int32_t get_offset_of__state_2() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t303264603, ____state_2)); }
	inline Il2CppObject * get__state_2() const { return ____state_2; }
	inline Il2CppObject ** get_address_of__state_2() { return &____state_2; }
	inline void set__state_2(Il2CppObject * value)
	{
		____state_2 = value;
		Il2CppCodeGenWriteBarrier(&____state_2, value);
	}

	inline static int32_t get_offset_of__sync_3() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t303264603, ____sync_3)); }
	inline Il2CppObject * get__sync_3() const { return ____sync_3; }
	inline Il2CppObject ** get_address_of__sync_3() { return &____sync_3; }
	inline void set__sync_3(Il2CppObject * value)
	{
		____sync_3 = value;
		Il2CppCodeGenWriteBarrier(&____sync_3, value);
	}

	inline static int32_t get_offset_of__waitHandle_4() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t303264603, ____waitHandle_4)); }
	inline ManualResetEvent_t924017833 * get__waitHandle_4() const { return ____waitHandle_4; }
	inline ManualResetEvent_t924017833 ** get_address_of__waitHandle_4() { return &____waitHandle_4; }
	inline void set__waitHandle_4(ManualResetEvent_t924017833 * value)
	{
		____waitHandle_4 = value;
		Il2CppCodeGenWriteBarrier(&____waitHandle_4, value);
	}

	inline static int32_t get_offset_of_Buffer_5() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t303264603, ___Buffer_5)); }
	inline ByteU5BU5D_t4260760469* get_Buffer_5() const { return ___Buffer_5; }
	inline ByteU5BU5D_t4260760469** get_address_of_Buffer_5() { return &___Buffer_5; }
	inline void set_Buffer_5(ByteU5BU5D_t4260760469* value)
	{
		___Buffer_5 = value;
		Il2CppCodeGenWriteBarrier(&___Buffer_5, value);
	}

	inline static int32_t get_offset_of_Count_6() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t303264603, ___Count_6)); }
	inline int32_t get_Count_6() const { return ___Count_6; }
	inline int32_t* get_address_of_Count_6() { return &___Count_6; }
	inline void set_Count_6(int32_t value)
	{
		___Count_6 = value;
	}

	inline static int32_t get_offset_of_Error_7() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t303264603, ___Error_7)); }
	inline Exception_t3991598821 * get_Error_7() const { return ___Error_7; }
	inline Exception_t3991598821 ** get_address_of_Error_7() { return &___Error_7; }
	inline void set_Error_7(Exception_t3991598821 * value)
	{
		___Error_7 = value;
		Il2CppCodeGenWriteBarrier(&___Error_7, value);
	}

	inline static int32_t get_offset_of_Offset_8() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t303264603, ___Offset_8)); }
	inline int32_t get_Offset_8() const { return ___Offset_8; }
	inline int32_t* get_address_of_Offset_8() { return &___Offset_8; }
	inline void set_Offset_8(int32_t value)
	{
		___Offset_8 = value;
	}

	inline static int32_t get_offset_of_SyncRead_9() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t303264603, ___SyncRead_9)); }
	inline int32_t get_SyncRead_9() const { return ___SyncRead_9; }
	inline int32_t* get_address_of_SyncRead_9() { return &___SyncRead_9; }
	inline void set_SyncRead_9(int32_t value)
	{
		___SyncRead_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
