﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIManager
struct UIManager_t1861242489;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UIManager::.ctor()
extern "C"  void UIManager__ctor_m3008954130 (UIManager_t1861242489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIManager::Start()
extern "C"  void UIManager_Start_m1956091922 (UIManager_t1861242489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIManager::Update()
extern "C"  void UIManager_Update_m515159611 (UIManager_t1861242489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIManager::UIMove(UnityEngine.GameObject,UnityEngine.Vector3,System.Single&)
extern "C"  bool UIManager_UIMove_m877261529 (UIManager_t1861242489 * __this, GameObject_t3674682005 * ____ob0, Vector3_t4282066566  ___destination1, float* ___spd2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
