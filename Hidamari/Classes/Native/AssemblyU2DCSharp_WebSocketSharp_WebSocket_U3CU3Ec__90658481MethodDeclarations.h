﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.WebSocket/<>c__Iterator1F
struct U3CU3Ec__Iterator1F_t90658481;
// WebSocketSharp.Net.Cookie
struct Cookie_t2077085446;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<WebSocketSharp.Net.Cookie>
struct IEnumerator_1_t3988950495;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocketSharp.WebSocket/<>c__Iterator1F::.ctor()
extern "C"  void U3CU3Ec__Iterator1F__ctor_m799211274 (U3CU3Ec__Iterator1F_t90658481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.Net.Cookie WebSocketSharp.WebSocket/<>c__Iterator1F::System.Collections.Generic.IEnumerator<WebSocketSharp.Net.Cookie>.get_Current()
extern "C"  Cookie_t2077085446 * U3CU3Ec__Iterator1F_System_Collections_Generic_IEnumeratorU3CWebSocketSharp_Net_CookieU3E_get_Current_m3316606067 (U3CU3Ec__Iterator1F_t90658481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object WebSocketSharp.WebSocket/<>c__Iterator1F::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CU3Ec__Iterator1F_System_Collections_IEnumerator_get_Current_m3517517734 (U3CU3Ec__Iterator1F_t90658481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator WebSocketSharp.WebSocket/<>c__Iterator1F::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CU3Ec__Iterator1F_System_Collections_IEnumerable_GetEnumerator_m4202140327 (U3CU3Ec__Iterator1F_t90658481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<WebSocketSharp.Net.Cookie> WebSocketSharp.WebSocket/<>c__Iterator1F::System.Collections.Generic.IEnumerable<WebSocketSharp.Net.Cookie>.GetEnumerator()
extern "C"  Il2CppObject* U3CU3Ec__Iterator1F_System_Collections_Generic_IEnumerableU3CWebSocketSharp_Net_CookieU3E_GetEnumerator_m2683495812 (U3CU3Ec__Iterator1F_t90658481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.WebSocket/<>c__Iterator1F::MoveNext()
extern "C"  bool U3CU3Ec__Iterator1F_MoveNext_m964271762 (U3CU3Ec__Iterator1F_t90658481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket/<>c__Iterator1F::Dispose()
extern "C"  void U3CU3Ec__Iterator1F_Dispose_m3436138695 (U3CU3Ec__Iterator1F_t90658481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.WebSocket/<>c__Iterator1F::Reset()
extern "C"  void U3CU3Ec__Iterator1F_Reset_m2740611511 (U3CU3Ec__Iterator1F_t90658481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
