﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CharData
struct CharData_t1499709504;
struct CharData_t1499709504_marshaled_pinvoke;
struct CharData_t1499709504_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CharData1499709504.h"

// System.Void CharData::.ctor(System.Int32,System.Single,System.Single)
extern "C"  void CharData__ctor_m1061663286 (CharData_t1499709504 * __this, int32_t ___hp0, float ___spd1, float ___technique2, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct CharData_t1499709504;
struct CharData_t1499709504_marshaled_pinvoke;

extern "C" void CharData_t1499709504_marshal_pinvoke(const CharData_t1499709504& unmarshaled, CharData_t1499709504_marshaled_pinvoke& marshaled);
extern "C" void CharData_t1499709504_marshal_pinvoke_back(const CharData_t1499709504_marshaled_pinvoke& marshaled, CharData_t1499709504& unmarshaled);
extern "C" void CharData_t1499709504_marshal_pinvoke_cleanup(CharData_t1499709504_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct CharData_t1499709504;
struct CharData_t1499709504_marshaled_com;

extern "C" void CharData_t1499709504_marshal_com(const CharData_t1499709504& unmarshaled, CharData_t1499709504_marshaled_com& marshaled);
extern "C" void CharData_t1499709504_marshal_com_back(const CharData_t1499709504_marshaled_com& marshaled, CharData_t1499709504& unmarshaled);
extern "C" void CharData_t1499709504_marshal_com_cleanup(CharData_t1499709504_marshaled_com& marshaled);
