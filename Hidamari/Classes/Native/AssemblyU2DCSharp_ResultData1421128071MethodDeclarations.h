﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResultData
struct ResultData_t1421128071;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "codegen/il2cpp-codegen.h"

// System.Void ResultData::.ctor()
extern "C"  void ResultData__ctor_m3951603444 (ResultData_t1421128071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultData::Start()
extern "C"  void ResultData_Start_m2898741236 (ResultData_t1421128071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultData::Update()
extern "C"  void ResultData_Update_m3967484569 (ResultData_t1421128071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ResultData::get_Retire()
extern "C"  bool ResultData_get_Retire_m4161786728 (ResultData_t1421128071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultData::set_Retire(System.Boolean)
extern "C"  void ResultData_set_Retire_m715067551 (ResultData_t1421128071 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ResultData::get_Disconnection()
extern "C"  bool ResultData_get_Disconnection_m4054582625 (ResultData_t1421128071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultData::set_Disconnection(System.Boolean)
extern "C"  void ResultData_set_Disconnection_m79538008 (ResultData_t1421128071 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultData::SetPlayerHP(System.Int32,System.Int32)
extern "C"  void ResultData_SetPlayerHP_m3215785737 (ResultData_t1421128071 * __this, int32_t ___HP0, int32_t ___nowRound1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultData::SetEnemyHP(System.Int32,System.Int32)
extern "C"  void ResultData_SetEnemyHP_m1414745096 (ResultData_t1421128071 * __this, int32_t ___HP0, int32_t ___nowRound1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultData::SetHP(System.Int32,System.Int32,System.Int32)
extern "C"  void ResultData_SetHP_m2078511341 (ResultData_t1421128071 * __this, int32_t ___player1HP0, int32_t ___player2HP1, int32_t ___nowRound2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ResultData::GetHP(System.Boolean)
extern "C"  Int32U5BU5D_t3230847821* ResultData_GetHP_m2220923131 (ResultData_t1421128071 * __this, bool ___isPlayer10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultData::MyDestroy()
extern "C"  void ResultData_MyDestroy_m1847615680 (ResultData_t1421128071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
