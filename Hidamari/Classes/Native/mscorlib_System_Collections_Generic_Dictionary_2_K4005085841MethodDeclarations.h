﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2660824325MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m487475195(__this, ___host0, method) ((  void (*) (Enumerator_t4005085841 *, Dictionary_2_t3390149787 *, const MethodInfo*))Enumerator__ctor_m2661607283_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2813773254(__this, method) ((  Il2CppObject * (*) (Enumerator_t4005085841 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2640325710_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3705922010(__this, method) ((  void (*) (Enumerator_t4005085841 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1606518626_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>::Dispose()
#define Enumerator_Dispose_m2850706269(__this, method) ((  void (*) (Enumerator_t4005085841 *, const MethodInfo*))Enumerator_Dispose_m2264940757_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>::MoveNext()
#define Enumerator_MoveNext_m2845480774(__this, method) ((  bool (*) (Enumerator_t4005085841 *, const MethodInfo*))Enumerator_MoveNext_m3041849038_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>::get_Current()
#define Enumerator_get_Current_m2716134478(__this, method) ((  HttpConnection_t602292776 * (*) (Enumerator_t4005085841 *, const MethodInfo*))Enumerator_get_Current_m3451690438_gshared)(__this, method)
