﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// SocketIO.SocketIOComponent
struct SocketIOComponent_t3095476818;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SingletonMonoBehaviour`1<SocketIO.SocketIOComponent>
struct  SingletonMonoBehaviour_1_t2425651671  : public MonoBehaviour_t667441552
{
public:

public:
};

struct SingletonMonoBehaviour_1_t2425651671_StaticFields
{
public:
	// T SingletonMonoBehaviour`1::instance
	SocketIOComponent_t3095476818 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(SingletonMonoBehaviour_1_t2425651671_StaticFields, ___instance_2)); }
	inline SocketIOComponent_t3095476818 * get_instance_2() const { return ___instance_2; }
	inline SocketIOComponent_t3095476818 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(SocketIOComponent_t3095476818 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
