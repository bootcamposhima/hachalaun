﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Result
struct Result_t2444407869;

#include "codegen/il2cpp-codegen.h"

// System.Void Result::.ctor()
extern "C"  void Result__ctor_m1541350654 (Result_t2444407869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Result::Start()
extern "C"  void Result_Start_m488488446 (Result_t2444407869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Result::Update()
extern "C"  void Result_Update_m2264092111 (Result_t2444407869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Result::ShowRoudnResult()
extern "C"  void Result_ShowRoudnResult_m720290750 (Result_t2444407869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Result::ShowResult()
extern "C"  void Result_ShowResult_m1558159296 (Result_t2444407869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Result::PushStageButton(System.Int32)
extern "C"  void Result_PushStageButton_m3429852547 (Result_t2444407869 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
