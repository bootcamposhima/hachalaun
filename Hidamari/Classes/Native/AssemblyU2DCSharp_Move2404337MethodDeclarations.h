﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Move
struct Move_t2404337;
// BlackBoard
struct BlackBoard_t328561223;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BlackBoard328561223.h"

// System.Void Move::.ctor()
extern "C"  void Move__ctor_m324221386 (Move_t2404337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Move::Start()
extern "C"  void Move_Start_m3566326474 (Move_t2404337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Move::Update()
extern "C"  void Move_Update_m3187790467 (Move_t2404337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Move::PlayTask(BlackBoard)
extern "C"  bool Move_PlayTask_m2151601696 (Move_t2404337 * __this, BlackBoard_t328561223 * ___blackboard0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Move::MoveAction(BlackBoard)
extern "C"  bool Move_MoveAction_m3705250926 (Move_t2404337 * __this, BlackBoard_t328561223 * ___blackboard0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Move::MoveParticle(BlackBoard)
extern "C"  void Move_MoveParticle_m2649125258 (Move_t2404337 * __this, BlackBoard_t328561223 * ___blackboard0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Move::StopMovePar()
extern "C"  void Move_StopMovePar_m221965014 (Move_t2404337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
