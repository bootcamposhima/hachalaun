﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FightRound/<ShowRetire>c__Iterator10
struct U3CShowRetireU3Ec__Iterator10_t4208130037;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void FightRound/<ShowRetire>c__Iterator10::.ctor()
extern "C"  void U3CShowRetireU3Ec__Iterator10__ctor_m2634896966 (U3CShowRetireU3Ec__Iterator10_t4208130037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FightRound/<ShowRetire>c__Iterator10::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CShowRetireU3Ec__Iterator10_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2343248086 (U3CShowRetireU3Ec__Iterator10_t4208130037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FightRound/<ShowRetire>c__Iterator10::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CShowRetireU3Ec__Iterator10_System_Collections_IEnumerator_get_Current_m2071302250 (U3CShowRetireU3Ec__Iterator10_t4208130037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightRound/<ShowRetire>c__Iterator10::MoveNext()
extern "C"  bool U3CShowRetireU3Ec__Iterator10_MoveNext_m4288260054 (U3CShowRetireU3Ec__Iterator10_t4208130037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightRound/<ShowRetire>c__Iterator10::Dispose()
extern "C"  void U3CShowRetireU3Ec__Iterator10_Dispose_m2298530051 (U3CShowRetireU3Ec__Iterator10_t4208130037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightRound/<ShowRetire>c__Iterator10::Reset()
extern "C"  void U3CShowRetireU3Ec__Iterator10_Reset_m281329907 (U3CShowRetireU3Ec__Iterator10_t4208130037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
