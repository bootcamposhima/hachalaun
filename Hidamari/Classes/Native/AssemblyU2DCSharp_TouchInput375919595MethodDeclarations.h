﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchInput
struct TouchInput_t375919595;

#include "codegen/il2cpp-codegen.h"

// System.Void TouchInput::.ctor()
extern "C"  void TouchInput__ctor_m3979590928 (TouchInput_t375919595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TouchInput TouchInput::get_GetInstance()
extern "C"  TouchInput_t375919595 * TouchInput_get_GetInstance_m1302823006 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchInput::Awake()
extern "C"  void TouchInput_Awake_m4217196147 (TouchInput_t375919595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchInput::Start()
extern "C"  void TouchInput_Start_m2926728720 (TouchInput_t375919595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchInput::Update()
extern "C"  void TouchInput_Update_m540129277 (TouchInput_t375919595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchInput::SetTouchActive(System.Boolean)
extern "C"  void TouchInput_SetTouchActive_m2490300494 (TouchInput_t375919595 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchInput::SetTouchNotActive(System.Boolean)
extern "C"  void TouchInput_SetTouchNotActive_m3835352033 (TouchInput_t375919595 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchInput::SceneTouchInput()
extern "C"  void TouchInput_SceneTouchInput_m3862149541 (TouchInput_t375919595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchInput::RaycastDetectionPC()
extern "C"  void TouchInput_RaycastDetectionPC_m3746588227 (TouchInput_t375919595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchInput::RaycastDetectionMobile()
extern "C"  void TouchInput_RaycastDetectionMobile_m2667430354 (TouchInput_t375919595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchInput::RaycastDetectionMobileWithLayerMask()
extern "C"  void TouchInput_RaycastDetectionMobileWithLayerMask_m4139300775 (TouchInput_t375919595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
