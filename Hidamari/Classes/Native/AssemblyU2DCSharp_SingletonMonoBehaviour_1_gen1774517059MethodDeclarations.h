﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen3500991224MethodDeclarations.h"

// System.Void SingletonMonoBehaviour`1<SoundManager>::.ctor()
#define SingletonMonoBehaviour_1__ctor_m4078300311(__this, method) ((  void (*) (SingletonMonoBehaviour_1_t1774517059 *, const MethodInfo*))SingletonMonoBehaviour_1__ctor_m2524367503_gshared)(__this, method)
// T SingletonMonoBehaviour`1<SoundManager>::get_Instance()
#define SingletonMonoBehaviour_1_get_Instance_m2019541994(__this /* static, unused */, method) ((  SoundManager_t2444342206 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))SingletonMonoBehaviour_1_get_Instance_m4273216084_gshared)(__this /* static, unused */, method)
