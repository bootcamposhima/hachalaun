﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2761310900;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SCImage
struct  SCImage_t2637275371  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Sprite[] SCImage::_Normal
	SpriteU5BU5D_t2761310900* ____Normal_2;
	// UnityEngine.Sprite[] SCImage::_Fight
	SpriteU5BU5D_t2761310900* ____Fight_3;
	// UnityEngine.Sprite[] SCImage::_Edit
	SpriteU5BU5D_t2761310900* ____Edit_4;

public:
	inline static int32_t get_offset_of__Normal_2() { return static_cast<int32_t>(offsetof(SCImage_t2637275371, ____Normal_2)); }
	inline SpriteU5BU5D_t2761310900* get__Normal_2() const { return ____Normal_2; }
	inline SpriteU5BU5D_t2761310900** get_address_of__Normal_2() { return &____Normal_2; }
	inline void set__Normal_2(SpriteU5BU5D_t2761310900* value)
	{
		____Normal_2 = value;
		Il2CppCodeGenWriteBarrier(&____Normal_2, value);
	}

	inline static int32_t get_offset_of__Fight_3() { return static_cast<int32_t>(offsetof(SCImage_t2637275371, ____Fight_3)); }
	inline SpriteU5BU5D_t2761310900* get__Fight_3() const { return ____Fight_3; }
	inline SpriteU5BU5D_t2761310900** get_address_of__Fight_3() { return &____Fight_3; }
	inline void set__Fight_3(SpriteU5BU5D_t2761310900* value)
	{
		____Fight_3 = value;
		Il2CppCodeGenWriteBarrier(&____Fight_3, value);
	}

	inline static int32_t get_offset_of__Edit_4() { return static_cast<int32_t>(offsetof(SCImage_t2637275371, ____Edit_4)); }
	inline SpriteU5BU5D_t2761310900* get__Edit_4() const { return ____Edit_4; }
	inline SpriteU5BU5D_t2761310900** get_address_of__Edit_4() { return &____Edit_4; }
	inline void set__Edit_4(SpriteU5BU5D_t2761310900* value)
	{
		____Edit_4 = value;
		Il2CppCodeGenWriteBarrier(&____Edit_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
