﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NormalRound
struct NormalRound_t1394582727;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AIDatas4008896193.h"

// System.Void NormalRound::.ctor()
extern "C"  void NormalRound__ctor_m75187844 (NormalRound_t1394582727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NormalRound::Start()
extern "C"  void NormalRound_Start_m3317292932 (NormalRound_t1394582727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NormalRound::Update()
extern "C"  void NormalRound_Update_m4057685257 (NormalRound_t1394582727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NormalRound::ChangeRound()
extern "C"  void NormalRound_ChangeRound_m197063456 (NormalRound_t1394582727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NormalRound::RoundStart()
extern "C"  void NormalRound_RoundStart_m243022196 (NormalRound_t1394582727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NormalRound::ChoiceStart()
extern "C"  void NormalRound_ChoiceStart_m1444312547 (NormalRound_t1394582727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NormalRound::ChoiceDone()
extern "C"  void NormalRound_ChoiceDone_m3907875587 (NormalRound_t1394582727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NormalRound::GameStart()
extern "C"  void NormalRound_GameStart_m3537764018 (NormalRound_t1394582727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NormalRound::NextGame()
extern "C"  void NormalRound_NextGame_m2572229381 (NormalRound_t1394582727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AIDatas NormalRound::createCPUAI()
extern "C"  AIDatas_t4008896193  NormalRound_createCPUAI_m1476309544 (NormalRound_t1394582727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NormalRound::DefenceCharRumd()
extern "C"  int32_t NormalRound_DefenceCharRumd_m2359283696 (NormalRound_t1394582727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NormalRound::ModeSelect(System.Int32)
extern "C"  void NormalRound_ModeSelect_m962173584 (NormalRound_t1394582727 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NormalRound::ModeSelectEnd()
extern "C"  void NormalRound_ModeSelectEnd_m4236110718 (NormalRound_t1394582727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
