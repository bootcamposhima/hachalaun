﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.Net.ListenerAsyncResult
struct ListenerAsyncResult_t609216079;
// System.AsyncCallback
struct AsyncCallback_t1369114871;
// System.Object
struct Il2CppObject;
// System.Threading.WaitHandle
struct WaitHandle_t1661568373;
// System.Exception
struct Exception_t3991598821;
// WebSocketSharp.Net.HttpListenerContext
struct HttpListenerContext_t3744659101;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_AsyncCallback1369114871.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Exception3991598821.h"
#include "AssemblyU2DCSharp_WebSocketSharp_Net_HttpListenerC3744659101.h"

// System.Void WebSocketSharp.Net.ListenerAsyncResult::.ctor(System.AsyncCallback,System.Object)
extern "C"  void ListenerAsyncResult__ctor_m1162667038 (ListenerAsyncResult_t609216079 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object WebSocketSharp.Net.ListenerAsyncResult::get_AsyncState()
extern "C"  Il2CppObject * ListenerAsyncResult_get_AsyncState_m3902670597 (ListenerAsyncResult_t609216079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.WaitHandle WebSocketSharp.Net.ListenerAsyncResult::get_AsyncWaitHandle()
extern "C"  WaitHandle_t1661568373 * ListenerAsyncResult_get_AsyncWaitHandle_m1813914847 (ListenerAsyncResult_t609216079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.Net.ListenerAsyncResult::get_CompletedSynchronously()
extern "C"  bool ListenerAsyncResult_get_CompletedSynchronously_m409838618 (ListenerAsyncResult_t609216079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.Net.ListenerAsyncResult::get_IsCompleted()
extern "C"  bool ListenerAsyncResult_get_IsCompleted_m3372987892 (ListenerAsyncResult_t609216079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.ListenerAsyncResult::invokeCallback(System.Object)
extern "C"  void ListenerAsyncResult_invokeCallback_m1054580645 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.ListenerAsyncResult::Complete(System.Exception)
extern "C"  void ListenerAsyncResult_Complete_m851649339 (ListenerAsyncResult_t609216079 * __this, Exception_t3991598821 * ___exception0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.ListenerAsyncResult::Complete(WebSocketSharp.Net.HttpListenerContext)
extern "C"  void ListenerAsyncResult_Complete_m3052208084 (ListenerAsyncResult_t609216079 * __this, HttpListenerContext_t3744659101 * ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.ListenerAsyncResult::Complete(WebSocketSharp.Net.HttpListenerContext,System.Boolean)
extern "C"  void ListenerAsyncResult_Complete_m1486958889 (ListenerAsyncResult_t609216079 * __this, HttpListenerContext_t3744659101 * ___context0, bool ___syncCompleted1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WebSocketSharp.Net.HttpListenerContext WebSocketSharp.Net.ListenerAsyncResult::GetContext()
extern "C"  HttpListenerContext_t3744659101 * ListenerAsyncResult_GetContext_m1737975889 (ListenerAsyncResult_t609216079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
