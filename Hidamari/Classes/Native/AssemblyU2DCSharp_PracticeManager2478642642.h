﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<AIDatas>
struct List_1_t1082114449;
// UnityEngine.Camera
struct Camera_t2727095145;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PracticeManager
struct  PracticeManager_t2478642642  : public MonoBehaviour_t667441552
{
public:
	// System.Collections.Generic.List`1<AIDatas> PracticeManager::_list
	List_1_t1082114449 * ____list_2;
	// UnityEngine.Camera PracticeManager::_uimaincamera
	Camera_t2727095145 * ____uimaincamera_3;
	// System.Collections.Generic.List`1<System.String> PracticeManager::fs
	List_1_t1375417109 * ___fs_4;
	// System.Int32 PracticeManager::_top
	int32_t ____top_5;

public:
	inline static int32_t get_offset_of__list_2() { return static_cast<int32_t>(offsetof(PracticeManager_t2478642642, ____list_2)); }
	inline List_1_t1082114449 * get__list_2() const { return ____list_2; }
	inline List_1_t1082114449 ** get_address_of__list_2() { return &____list_2; }
	inline void set__list_2(List_1_t1082114449 * value)
	{
		____list_2 = value;
		Il2CppCodeGenWriteBarrier(&____list_2, value);
	}

	inline static int32_t get_offset_of__uimaincamera_3() { return static_cast<int32_t>(offsetof(PracticeManager_t2478642642, ____uimaincamera_3)); }
	inline Camera_t2727095145 * get__uimaincamera_3() const { return ____uimaincamera_3; }
	inline Camera_t2727095145 ** get_address_of__uimaincamera_3() { return &____uimaincamera_3; }
	inline void set__uimaincamera_3(Camera_t2727095145 * value)
	{
		____uimaincamera_3 = value;
		Il2CppCodeGenWriteBarrier(&____uimaincamera_3, value);
	}

	inline static int32_t get_offset_of_fs_4() { return static_cast<int32_t>(offsetof(PracticeManager_t2478642642, ___fs_4)); }
	inline List_1_t1375417109 * get_fs_4() const { return ___fs_4; }
	inline List_1_t1375417109 ** get_address_of_fs_4() { return &___fs_4; }
	inline void set_fs_4(List_1_t1375417109 * value)
	{
		___fs_4 = value;
		Il2CppCodeGenWriteBarrier(&___fs_4, value);
	}

	inline static int32_t get_offset_of__top_5() { return static_cast<int32_t>(offsetof(PracticeManager_t2478642642, ____top_5)); }
	inline int32_t get__top_5() const { return ____top_5; }
	inline int32_t* get_address_of__top_5() { return &____top_5; }
	inline void set__top_5(int32_t value)
	{
		____top_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
