﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext/<>c__Iterator1C
struct U3CU3Ec__Iterator1C_t3255887036;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.String>
struct IEnumerator_1_t1919096606;

#include "codegen/il2cpp-codegen.h"

// System.Void WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext/<>c__Iterator1C::.ctor()
extern "C"  void U3CU3Ec__Iterator1C__ctor_m3017639775 (U3CU3Ec__Iterator1C_t3255887036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext/<>c__Iterator1C::System.Collections.Generic.IEnumerator<string>.get_Current()
extern "C"  String_t* U3CU3Ec__Iterator1C_System_Collections_Generic_IEnumeratorU3CstringU3E_get_Current_m1365713401 (U3CU3Ec__Iterator1C_t3255887036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext/<>c__Iterator1C::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CU3Ec__Iterator1C_System_Collections_IEnumerator_get_Current_m201165617 (U3CU3Ec__Iterator1C_t3255887036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext/<>c__Iterator1C::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CU3Ec__Iterator1C_System_Collections_IEnumerable_GetEnumerator_m4209078962 (U3CU3Ec__Iterator1C_t3255887036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.String> WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext/<>c__Iterator1C::System.Collections.Generic.IEnumerable<string>.GetEnumerator()
extern "C"  Il2CppObject* U3CU3Ec__Iterator1C_System_Collections_Generic_IEnumerableU3CstringU3E_GetEnumerator_m278477500 (U3CU3Ec__Iterator1C_t3255887036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext/<>c__Iterator1C::MoveNext()
extern "C"  bool U3CU3Ec__Iterator1C_MoveNext_m2943388957 (U3CU3Ec__Iterator1C_t3255887036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext/<>c__Iterator1C::Dispose()
extern "C"  void U3CU3Ec__Iterator1C_Dispose_m747182044 (U3CU3Ec__Iterator1C_t3255887036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext/<>c__Iterator1C::Reset()
extern "C"  void U3CU3Ec__Iterator1C_Reset_m664072716 (U3CU3Ec__Iterator1C_t3255887036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
