﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t747900261;
// System.Collections.Generic.List`1<System.Collections.IEnumerator>
struct List_1_t537793463;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "AssemblyU2DCSharp_SingletonMonoBehaviour_1_gen3616935188.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TransitionAnimation
struct  TransitionAnimation_t4286760335  : public SingletonMonoBehaviour_1_t3616935188
{
public:
	// UnityEngine.Texture2D TransitionAnimation::T_tex2D
	Texture2D_t3884108195 * ___T_tex2D_3;
	// System.Single TransitionAnimation::m_Alpha
	float ___m_Alpha_4;
	// System.Boolean TransitionAnimation::m_IsAnimation
	bool ___m_IsAnimation_5;
	// UnityEngine.Vector3 TransitionAnimation::color
	Vector3_t4282066566  ___color_6;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> TransitionAnimation::_list
	List_1_t747900261 * ____list_7;
	// System.Collections.Generic.List`1<System.Collections.IEnumerator> TransitionAnimation::_ilist
	List_1_t537793463 * ____ilist_8;
	// System.Collections.IEnumerator TransitionAnimation::_allscaleanimarion
	Il2CppObject * ____allscaleanimarion_9;
	// System.Single TransitionAnimation::_scale
	float ____scale_10;
	// System.Collections.IEnumerator TransitionAnimation::_moveaction
	Il2CppObject * ____moveaction_11;

public:
	inline static int32_t get_offset_of_T_tex2D_3() { return static_cast<int32_t>(offsetof(TransitionAnimation_t4286760335, ___T_tex2D_3)); }
	inline Texture2D_t3884108195 * get_T_tex2D_3() const { return ___T_tex2D_3; }
	inline Texture2D_t3884108195 ** get_address_of_T_tex2D_3() { return &___T_tex2D_3; }
	inline void set_T_tex2D_3(Texture2D_t3884108195 * value)
	{
		___T_tex2D_3 = value;
		Il2CppCodeGenWriteBarrier(&___T_tex2D_3, value);
	}

	inline static int32_t get_offset_of_m_Alpha_4() { return static_cast<int32_t>(offsetof(TransitionAnimation_t4286760335, ___m_Alpha_4)); }
	inline float get_m_Alpha_4() const { return ___m_Alpha_4; }
	inline float* get_address_of_m_Alpha_4() { return &___m_Alpha_4; }
	inline void set_m_Alpha_4(float value)
	{
		___m_Alpha_4 = value;
	}

	inline static int32_t get_offset_of_m_IsAnimation_5() { return static_cast<int32_t>(offsetof(TransitionAnimation_t4286760335, ___m_IsAnimation_5)); }
	inline bool get_m_IsAnimation_5() const { return ___m_IsAnimation_5; }
	inline bool* get_address_of_m_IsAnimation_5() { return &___m_IsAnimation_5; }
	inline void set_m_IsAnimation_5(bool value)
	{
		___m_IsAnimation_5 = value;
	}

	inline static int32_t get_offset_of_color_6() { return static_cast<int32_t>(offsetof(TransitionAnimation_t4286760335, ___color_6)); }
	inline Vector3_t4282066566  get_color_6() const { return ___color_6; }
	inline Vector3_t4282066566 * get_address_of_color_6() { return &___color_6; }
	inline void set_color_6(Vector3_t4282066566  value)
	{
		___color_6 = value;
	}

	inline static int32_t get_offset_of__list_7() { return static_cast<int32_t>(offsetof(TransitionAnimation_t4286760335, ____list_7)); }
	inline List_1_t747900261 * get__list_7() const { return ____list_7; }
	inline List_1_t747900261 ** get_address_of__list_7() { return &____list_7; }
	inline void set__list_7(List_1_t747900261 * value)
	{
		____list_7 = value;
		Il2CppCodeGenWriteBarrier(&____list_7, value);
	}

	inline static int32_t get_offset_of__ilist_8() { return static_cast<int32_t>(offsetof(TransitionAnimation_t4286760335, ____ilist_8)); }
	inline List_1_t537793463 * get__ilist_8() const { return ____ilist_8; }
	inline List_1_t537793463 ** get_address_of__ilist_8() { return &____ilist_8; }
	inline void set__ilist_8(List_1_t537793463 * value)
	{
		____ilist_8 = value;
		Il2CppCodeGenWriteBarrier(&____ilist_8, value);
	}

	inline static int32_t get_offset_of__allscaleanimarion_9() { return static_cast<int32_t>(offsetof(TransitionAnimation_t4286760335, ____allscaleanimarion_9)); }
	inline Il2CppObject * get__allscaleanimarion_9() const { return ____allscaleanimarion_9; }
	inline Il2CppObject ** get_address_of__allscaleanimarion_9() { return &____allscaleanimarion_9; }
	inline void set__allscaleanimarion_9(Il2CppObject * value)
	{
		____allscaleanimarion_9 = value;
		Il2CppCodeGenWriteBarrier(&____allscaleanimarion_9, value);
	}

	inline static int32_t get_offset_of__scale_10() { return static_cast<int32_t>(offsetof(TransitionAnimation_t4286760335, ____scale_10)); }
	inline float get__scale_10() const { return ____scale_10; }
	inline float* get_address_of__scale_10() { return &____scale_10; }
	inline void set__scale_10(float value)
	{
		____scale_10 = value;
	}

	inline static int32_t get_offset_of__moveaction_11() { return static_cast<int32_t>(offsetof(TransitionAnimation_t4286760335, ____moveaction_11)); }
	inline Il2CppObject * get__moveaction_11() const { return ____moveaction_11; }
	inline Il2CppObject ** get_address_of__moveaction_11() { return &____moveaction_11; }
	inline void set__moveaction_11(Il2CppObject * value)
	{
		____moveaction_11 = value;
		Il2CppCodeGenWriteBarrier(&____moveaction_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
